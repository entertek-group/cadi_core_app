# -------- WINDOWS : ANDROID STUDIO EMULATOR ------------

emulator emulator -list-avds

# emulator -avd <YOUR EMULATOR>
emulator -avd Pixel_3_API_33
emulator -avd Pixel_3_API_34

fvm flutter run 
(OR F5 for live reload)

# WINDOWS : ANDROID STUDIO EMULATOR
