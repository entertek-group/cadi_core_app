import 'package:cadi_core_app/redux/models/account/account_models.dart';

class AccountFactory {
  List<AccountPort> convertAccountPortDepotsToAccountPorts(
      List<AccountPortDepot> accountPortDepots) {
    List<AccountPort> accountPorts = [];

    for (var accountPortDepot in accountPortDepots) {
      int uniqueId = accountPortDepot.portId + accountPortDepot.accountId;
      var accountPort = AccountPort(
          portId: accountPortDepot.portId,
          unLocode: accountPortDepot.unLocode,
          accountId: accountPortDepot.accountId,
          uniqueId: uniqueId,
          accountPortDepotId: accountPortDepot.id);
      accountPorts.add(accountPort);
    }
    var seen = <int>{};

    List<AccountPort> uniqueList =
        accountPorts.where((element) => seen.add(element.uniqueId)).toList();

    return uniqueList;
  }
}
