import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';

class CadiEnumFactory {
  List<CadiEnum> createCargoMovementStates() {
    return <CadiEnum>[
      CadiEnum(id: CadiCargoMovementStatus.damaged, description: "Damaged"),
      CadiEnum(
          id: CadiCargoMovementStatus.goodOrder, description: "Good Order"),
      CadiEnum(
          id: CadiCargoMovementStatus.missingCargo,
          description: "Missing Cargo"),
      CadiEnum(id: CadiCargoMovementStatus.other, description: "Other")
    ];
  }

  List<CadiEnum> createRoadCargoMovementStates() {
    return <CadiEnum>[
      CadiEnum(
          id: CadiRoadCargoMovementStatus.damaged, description: "Damaged"),
      CadiEnum(
          id: CadiRoadCargoMovementStatus.goodOrder,
          description: "Good Order"),
      CadiEnum(
          id: CadiRoadCargoMovementStatus.missingCargo,
          description: "Missing Cargo"),
      CadiEnum(id: CadiRoadCargoMovementStatus.other, description: "Other")
    ];
  }

  List<CadiEnum> createCargoMovementRoles() {
    return <CadiEnum>[
      CadiEnum(
          id: CadiCargoMovementRole.freightUnload, description: "Discharge"),
      CadiEnum(id: CadiCargoMovementRole.freightLoad, description: "Load"),
      CadiEnum(id: CadiCargoMovementRole.gateIn, description: "Gate In"),
      CadiEnum(id: CadiCargoMovementRole.gateOut, description: "Gate Out")
    ];
  }

  List<CadiEnum> createVesselJourneyRoles() {
    return <CadiEnum>[
      CadiEnum(
          id: CadiPortEventRole.firstLine,
          description: getPortEventRoleDescription(CadiPortEventRole.firstLine)),
      CadiEnum(id: CadiPortEventRole.firstLift,
          description: getPortEventRoleDescription(CadiPortEventRole.firstLift)),
      CadiEnum(id: CadiPortEventRole.securingVessel,
          description: getPortEventRoleDescription(CadiPortEventRole.securingVessel)),
      CadiEnum(id: CadiPortEventRole.labourOn,
          description: getPortEventRoleDescription(CadiPortEventRole.labourOn)),
      CadiEnum(id: CadiPortEventRole.unLashing,
          description: getPortEventRoleDescription(CadiPortEventRole.unLashing)),
      CadiEnum(id: CadiPortEventRole.hatchLids,
          description: getPortEventRoleDescription(CadiPortEventRole.hatchLids)),
      CadiEnum(id: CadiPortEventRole.lashing,
          description: getPortEventRoleDescription(CadiPortEventRole.lashing)),
      CadiEnum(id: CadiPortEventRole.lastLift,
          description: getPortEventRoleDescription(CadiPortEventRole.lastLift)),
      CadiEnum(id: CadiPortEventRole.delay,
          description: getPortEventRoleDescription(CadiPortEventRole.delay)),
      CadiEnum(id: CadiPortEventRole.refuel,
          description: getPortEventRoleDescription(CadiPortEventRole.refuel))
    ];
  }

  List<CadiEnum> createRoadMovementRoles() {
    return <CadiEnum>[
      CadiEnum(
          id: CadiRoadMovementRole.pickup,
          description: "Pickup"),
      CadiEnum(
          id: CadiRoadMovementRole.delivery,
          description: "Delivery"),
    ];
  }

  List<CadiEnum> createCargoMovementShipments() {
    return <CadiEnum>[
      CadiEnum(
          id: CadiCargoMovementShipment.container, description: "Container"),
      CadiEnum(
          id: CadiCargoMovementShipment.breakBulk, description: "Break Bulk"),
      CadiEnum(id: CadiCargoMovementShipment.restow, description: "Restow"),
    ];
  }

  List<CadiEnum> createRoadMovementCargoTypes() {
    return <CadiEnum>[
      CadiEnum(
          id: CadiRoadMovementCargoType.container, description: "Container"),
      CadiEnum(
          id: CadiRoadMovementCargoType.breakBulk, description: "Break Bulk"),
      CadiEnum(
          id: CadiRoadMovementCargoType.fuel, description: "Fuel"),
    ];
  }

  List<CadiEnum> createPortCargoUpdateTypes() {
    return <CadiEnum>[
      CadiEnum(id: CadiPortCargoUpdateType.vgm, description: "Verified Gross Mass"),
    ];
  }

  List<CadiEnum> createPortCargoUpdateShipments() {
    return <CadiEnum>[
      CadiEnum(id: CadiPortCargoUpdateShipment.container, description: "Container"),
       CadiEnum(id: CadiPortCargoUpdateShipment.breakBulk, description: "Break Bulk"),
    ];
  }

  List<CadiEnum> createRestowStates() {
    return <CadiEnum>[
      CadiEnum(id: CadiRestowStatus.unloaded, description: "Unloaded"),
      CadiEnum(id: CadiRestowStatus.reloaded, description: "Reloaded"),
    ];
  }

  List<CadiEnum> createNoSealOptions() {
    return <CadiEnum>[
      CadiEnum(id: CadiNoSealOption.seal, description: "Seal Available"),
      CadiEnum(id: CadiNoSealOption.noSeal, description: "No Seal Number"),
      CadiEnum(id: CadiNoSealOption.padLocked, description: "Padlocked"),
    ];
  }

  List<CadiEnum> createReportFilterOptions() {
    return <CadiEnum>[
      CadiEnum(id: ReportFilterRole.all, description: "All"),
      CadiEnum(id: ReportFilterRole.loaded, description: "Loaded"),
      CadiEnum(id: ReportFilterRole.unloaded, description: "Unloaded"),
    ];
  }

  List<CadiEnum> createCurrencies() {
    return <CadiEnum>[
      CadiEnum(id: "aud", description: "Australian Dollar"),
      CadiEnum(id: "eur", description: "Euro"),
      CadiEnum(id: "gbp", description: "Pounds"),
      CadiEnum(id: "pgk", description: "PNG Kina"),
      CadiEnum(id: "usd", description: "US Dollar"),
      CadiEnum(id: "zar", description: "Rands"),
    ];
  }

  List<CadiEnum> createVolumeUnits() {
    return <CadiEnum>[
      CadiEnum(id: "kl", description: "Kilo Litre"),
      CadiEnum(id: "l", description: "Litre"),
      CadiEnum(id: "gl", description: "Gallon"),
    ];
  }

  List<CadiEnum> createDistanceTravelledUnits() {
    return <CadiEnum>[
      CadiEnum(id: "nm", description: "Nautical Miles"),
      CadiEnum(id: "hr", description: "Hours"),
    ];
  }

  List<CadiEnum> createWeightMeasurements() {
    return <CadiEnum>[
      CadiEnum(id: WeightUnit.tonne, description: "Tonnes"),
      CadiEnum(id: WeightUnit.kilogram, description: "Kilogram"),
      CadiEnum(id: WeightUnit.pound, description: "Pounds"),
    ];
  }
}
