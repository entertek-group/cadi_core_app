import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';
import 'package:cadi_core_app/ui/shared/components/picker.dart';

class UiFactory {
  List<PickerItem> convertToPicker<T>(
      List<T> items, String Function(T) labelCb) {
    return items
        .asMap()
        .map<int, PickerItem>(
            (index, item) => MapEntry(index, PickerItem(labelCb(item), index)))
        .values
        .toList();
  }

  List<DropdownItem> convertToDropdown<T, IDType>(
      List<T> items, String Function(T) labelCb, IDType Function(T) idCb) {
    return items
        .asMap()
        .map<int, DropdownItem>((index, item) =>
            MapEntry(index, DropdownItem(labelCb(item), index, idCb(item))))
        .values
        .toList();
  }
}
