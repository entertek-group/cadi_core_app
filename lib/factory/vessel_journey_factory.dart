import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/vessel/vessel.dart';

class VesselJourneyFactory {
  List<SailingSchedule> generateSailingScheduleFromVesselJourneySearch(
      String voyageNumber,
      String voyageDate,
      String unLocode,
      String vesselName) {
    List<SailingSchedule> sailingSchedules = [];

    Vessel vessel = Vessel(
        id: 0,
        vesselName: vesselName,
        vesselClass: "",
        vesselType: "",
        fuelType: "");

    SailingSchedule sailingSchedule = SailingSchedule(
        id: null,
        accountId: 0,
        userId: 0,
        voyageNumber: voyageNumber,
        vesselId: 0,
        estimatedDeparture: voyageDate,
        estimatedArrival: voyageDate,
        capacityUsed: 0.00,
        capacityUnit: "m3",
        distance: 0.00,
        distanceUnit: "km",
        avgSpeed: 0.00,
        avgSpeedUnit: "kts",
        createdAt: voyageDate,
        updatedAt: voyageDate,
        portOfDischarge: unLocode,
        portOfLoad: unLocode,
        vessel: vessel);

    sailingSchedules.add(sailingSchedule);

    return sailingSchedules;
  }

}
