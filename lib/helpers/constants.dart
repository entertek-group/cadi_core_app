class ConnectivityConstants {
  static const none = "none";
  static const wifi = "wifi";
  static const mobile = "mobile";
  static const ethernet = "ethernet";
}

class CargoMovementConstants {
  static const billOfLading = "BillOfLading";
  static const cargoContainer = "CargoContainer";
}

class HomeNavigationConstants {
  static const homeScreenIndex = 0;
  static const cargoMovementIndex = 1;
  static const portCargoUpdateIndex = 2;
  static const reportsIndex = 3;
  static const settingsScreenIndex = 4;
  static const emptySettingsScreenIndex = 1;
  static const portEventIndex = 1;
  static const vesselUpdateIndex = 2;
  static const calculatorHomeIndex = 3;
  static const roadMovementIndex = 1;
  static const roadUpdateIndex = 2;
  static const roadReportsIndex = 3;
}

class CadiJobType {
  static const billOfLading = "BillOfLading";
  static const booking = "Booking";
  static const other = "other";
}

class CadiCargoMovementStatus {
  static const damaged = "Damaged";
  static const goodOrder = "GoodOrder";
  static const missingCargo = "MissingCargo";
  static const other = "Other";
}

class CadiRoadCargoMovementStatus {
  static const damaged = "damaged";
  static const goodOrder = "good_order";
  static const missingCargo = "missing_cargo";
  static const other = "other";
}

class CadiCargoMovementRole {
  static const freightUnload = "FreightUnload";
  static const freightLoad = "FreightLoad";
  static const gateIn = "GateIn";
  static const gateOut = "GateOut";
}

class CadiRoadMovementRole {
  static const pickup = "Pickup";
  static const delivery = "Delivery";
}

class CadiPortEventRole {
  static const firstLine = "FirstLine";
  static const firstLift = "FirstLift";
  static const securingVessel = "SecuringVessel";
  static const labourOn = "LabourOn";
  static const unLashing = "UnLashing";
  static const hatchLids = "HatchLids";
  static const lashing = "Lashing";
  static const lastLift = "LastLift";
  static const delay = "Delay";
  static const refuel = "Refuel";
}

class CadiCargoMovementShipment {
  static const container = "Container";
  static const breakBulk = "BreakBulk";
  static const restow = "Restow";
}

class CadiRoadMovementCargoType {
  static const container = "container";
  static const breakBulk = "break_bulk";
  static const fuel = "fuel";
}

class CadiPortCargoUpdateShipment {
  static const container = "Container";
  static const breakBulk = "BreakBulk";
}

class CadiPortCargoUpdateType {
  static const vgm = "VGM";
}

class CadiRestowStatus {
  static const unloaded = "Unloaded";
  static const reloaded = "Reloaded";
}

class CadiNoSealOption {
  static const seal = "seal_available";
  static const noSeal = "no_seal";
  static const padLocked = "padlocked";
}

class CadiAppServices {
  static const portMovements = "PortMovement";
  static const portEvents = "PortEvent";
  static const roadMovements = "RoadMovement";
  static const roadEvents = "RoadEvent";
  static const unavailable = "unavailable";
}

class CargoUpdateQueueType {
  static const container = "Container";
  static const pack = "Pack";
}

class StorageType {
  static const cargoMovement = CadiAppServices.portMovements;
  static const cargoEvent = CadiAppServices.portEvents;
  static const roadMovement = CadiAppServices.roadMovements;
  static const roadEvent = CadiAppServices.roadEvents;
  static const avatar = "avatar";
}

class WeightUnit {
  static const tonne = "T";
  static const kilogram = "KG";
  static const pound = "LB";
}

class ReportFilterRole {
  static const all = "all";
  static const loaded = "loaded";
  static const unloaded = "unloaded";
}

class PortReportType {
  static const restow = "restow";
  static const shortLand = "shortLand";
  static const shortShip = "shortShip";
  static const movementStatus = "movementStatus";
}

String getReportDescriptionByRole(String role) {
  switch (role) {
    case "FreightUnload":
      return "Discharged";
    case "FreightLoad":
      return "Loaded";
    default:
      return "";
  }
}

String getAppServiceName(String service) {
  switch (service) {
    case CadiAppServices.portMovements:
      return "Port Movements";
    case CadiAppServices.portEvents:
      return "Port Events";
    case CadiAppServices.roadMovements:
      return "Road Movements";
    case CadiAppServices.roadEvents:
      return "Road Events";
    default:
      return "unavailable";
  }
}

String getShipmentTypeDisplayFormat(String shipment) {
  switch (shipment) {
    case CadiCargoMovementShipment.restow:
      return "Restow";
    case CadiCargoMovementShipment.container:
      return "Container";
    case CadiCargoMovementShipment.breakBulk:
      return "Break Bulk";
    default:
      return '';
  }
}

String getCargoMovementRoleDescription(String role) {
  switch (role) {
    case CadiCargoMovementRole.freightUnload:
      return "Discharge";
    case CadiCargoMovementRole.freightLoad:
      return "Load";
    case CadiCargoMovementRole.gateIn:
      return "Gate In";
    case CadiCargoMovementRole.gateOut:
      return "Gate Out";
    default:
      return "Discharge";
  }
}

String getPortCargoUpdateRoleDescription(String role) {
  switch (role) {
    case CadiPortCargoUpdateType.vgm:
      return "Verified Gross Mass";
    default:
      return "Verified Gross Mass";
  }
}

String getRoadMovementRoleDescription(String role) {
  switch (role) {
    case  CadiRoadMovementRole.delivery:
      return "Delivery";
    case CadiRoadMovementRole.pickup:
      return "Pickup";
    default:
      return "Delivery";
  }
}


String getPortEventRoleDescription(String role) {
  switch (role) {
    case "FirstLine":
      return "First Line";
    case "FirstLift":
      return "First Lift";
    case "SecuringVessel":
      return "Securing Vessel";
    case "LabourOn":
      return "Labour On";
    case "UnLashing":
      return "Unlashing";
    case "HatchLids":
        return "Hatch Lids";
    case "Lashing":
      return "Lashing";
    case "LastLift":
      return "Last Lift";
    case "Delay":
      return "Delay";
    case "Refuel":
      return "Refuel";
    default:
      return "Not Defined";
  }
}

String getReportTypeDescription(String reportType) {
  switch (reportType) {
    case "restow":
      return "Restow";
    case "shortShip":
      return "Short Ship";
    case "shortLand":
      return "Short Land";
    case "movementStatus":
      return "Movement Status";
    default:
      return "";
  }
}

String getRoadReportTypeDescription(String reportType) {
  switch (reportType) {
    case "in_transit":
      return "In Transit";
    case "ready_for_collection":
      return "Ready for Collection";

    default:
      return "";
  }
}
