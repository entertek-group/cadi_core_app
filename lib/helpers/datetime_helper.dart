import 'package:flutter/material.dart';

String getDateTimeWithOffset(DateTime now) {
  Duration offset = now.timeZoneOffset;
  String dateTime = now.toIso8601String();
  // - or -
  // String dateTime = intl.DateFormat("yyyy-MM-dd'T'HH:mm:ss").format(now);
  String utcHourOffset = (offset.isNegative ? '-' : '+') +
      offset.inHours.abs().toString().padLeft(2, '0');
  String utcMinuteOffset = (offset.inMinutes - offset.inHours * 60)
      .toString().padLeft(2, '0');
  String dateTimeWithOffset = '$dateTime$utcHourOffset:$utcMinuteOffset';
  return dateTimeWithOffset;
}

extension DateTimeExtension on DateTime {
  DateTime applied(TimeOfDay time) {
    return DateTime(year, month, day, time.hour, time.minute);
  }
}
