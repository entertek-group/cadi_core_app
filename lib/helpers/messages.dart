class WarningMessages {
  static const containerOverrideMessage = "Container number does not meet international "
      "format (4 letters, 7 numbers). Are you sure you want to override format?";
}