import 'dart:io';

import 'package:cadi_core_app/redux/models/models.dart';
import 'package:flutter/widgets.dart';
import 'global_helper.dart';

Map<String, String> authHeaders(Token token) => {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${token.token}'
    };

const Map<String, String> jsonHeaders = {
  'Content-type': 'application/json',
  'Accept': 'application/json'
};


var urlBase = [
  if (isDevelopmentMode && Platform.isAndroid)
    "http://10.0.2.2:5002/api"
    else if (isDevelopmentMode)
    "http://localhost:5002/api"
    else
    "https://lake.cadicore.com/api" //uat
][0];

var  urlBaseImage = [
  if (isDevelopmentMode && Platform.isAndroid)
    "http://10.0.2.2:5002/api/v1/blobs/image"
  else if (isDevelopmentMode)
    "http://localhost:5002/api/v1/blobs/image"
  else
  "https://lake.cadicore.com/api/v1/blobs/image" //uat
][0];

String getImageUrl(String url) {
  return "$urlBaseImage/$url";
}

String standardRoute(String route) {
  debugPrint(urlBase);
  return urlBase + route;
}

const urlContactUs = "https://www.cadisystems.com/contact-us";
const urlDataPolicy = "https://www.cadisystems.com/data-policy";
const urlFaqs = "https://www.cadisystems.com/faqs";
const urlTermsOfUse = "https://www.cadisystems.com/terms-of-use";
const urlPrivacyStatement = "https://www.cadisystems.com/privacy-statement";
