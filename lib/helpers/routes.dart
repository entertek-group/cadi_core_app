//#welcome
const welcomeScreen = '/welcome/vm';
//#home
const homeScreen = '/home';
//#setting
const settingIndexScreen = '/setting_index';
const settingIndexScreenBuilder = '/setting_index/vm';
//#login
const loginScreen = '/login';
const logoutScreen = '/logout';
//#Password Reset
const passwordResetScreen = '/password_reset/vm';
//#loaders
const loaderLogin = '/loader/login';
//#cargo
const cargoHomeScreen = '/cargo_home';
const cargoMovementHomeScreenBuilder = '/cargo_movement_home/vm';

//#Port Cargo Update
const portCargoUpdateHomeScreenBuilder = '/port_cargo_update_home/vm';
const portCargoUpdateFreightSearchScreenBuilder = '/port_cargo_update_freight_search/vm';
const portCargoUpdateContainerScreenBuilder = '/port_cargo_update_container/vm';
const portCargoUpdateStatusScreenBuilder = '/port_cargo_update_status/vm';
const portCargoUpdateSummaryScreenBuilder = '/port_cargo_update_summary/vm';
const portCargoUpdateCompletionScreenBuilder = '/port_cargo_update_completion/vm';



// const cargoGateSearchcreenBuilder = '/cargo_gate_search/vm';
const cargoMovementFreightSearchScreenBuilder = '/cargo_movement_freight_search/vm';
// const cargoIndexScreen = '/cargo_index';
// const cargoIndexScreenBuilder = '/cargo_index/vm';
// const cargoContainerSelectScreen = '/cargo_container_select';
// const cargoContainerSelectScreenBuilder = '/cargo_container_select/vm';
// const cargoCargoMoveEventScreenBuilder = '/cargo_move_event/vm';
const cargoMovementContainerScreenBuilder = '/cargo_movement_container/vm';
const cargoMovementStatusScreenBuilder = '/cargo_movement_status/vm';
const cargoMovementSummaryScreenBuilder = '/cargo_movement_summary/vm';
const cargoMovementEirGenerationScreenBuilder = '/cargo_movement_eir_generation/vm';
const cargoUpdateSummaryScreenBuilder = '/cargo_update_summary/vm';
const cargoMovementVehicleDetailsBuilder = '/cargo_movement_vehicles_details/vm';
const cargoMovementCompletionScreenBuilder = '/cargo_movement_completion/vm';
//#Cargo Update
const cargoUpdateSearchScreenBuilder = '/cargo_update_search/vm';
const cargoUpdateCompletionScreenBuilder = '/cargo_update_completion/vm';

//#Road Movement
const roadMovementHomeScreenBuilder = '/road_movement_home/vm';
const roadMovementFreightSearchScreenBuilder = '/road_movement_freight_search/vm';
const roadMovementContainerScreenBuilder = '/road_movement_container/vm';
const roadMovementStatusScreenBuilder = '/road_movement_status/vm';
const roadMovementSummaryScreenBuilder = '/road_movement_summary/vm';
const roadMovementCompletionScreenBuilder = '/road_movement_completion/vm';
const roadMovementSignatureScreenBuilder = '/road_movement_signature/vm';

//#Road Cargo Update
const roadUpdateSearchScreenBuilder = '/road_update_search/vm';
const roadUpdateSummaryScreenBuilder = '/road_update_summary/vm';

//Road Reports
const roadReportsHomePageScreenBuilder = '/road_report/vm';
const roadReportsHomeBuilder = '/road_report/road_report_home_screen/vm';
const roadReportSearchBuilder = '/road_report/road_report_search/vm';
const roadReportShowAllBuilder = 'road_report/road_report_show_all/vm';
const roadReportResultsBuilder = 'road_report/road_report_results/vm';

//Truck Journeys
const truckJourneyIndexScreenBuilder = '/truck_journeys/truck_journey_index/vm';
const truckJourneySearchBuilder = '/truck_journeys/truck_journey_search/vm';
const truckJourneyResultsBuilder = '/truck_journeys/truck_journey_results/vm';
const truckJourneySummaryScreenBuilder =
    'truck_journey_summary/truck_journey_summary/vm';
const truckJourneyFoundBuilder = 'truck_journey_found/vm';
const truckJourneyNewBuilder = '/truck_journeys/truck_journey_new/vm';
const truckJourneyCompletionScreenBuilder = '/truck_journey_completion/vm';
const truckJourneyRefuelBuilder = '/truck_journeys/truck_journey_refuel/vm';

//Truck Journey Update
const truckJourneyUpdateIndexBuilder = '/truck_journey_update/vm';
const truckJourneyUpdateResultsBuilder =
    '/truck_journey_update/truck_journey_update_results/vm';
const truckJourneyUpdateSummaryScreenBuilder =
    '/truck_journey_update/truck_journey_update_summary/vm';
const truckJourneyUpdateCompletionScreenBuilder =
    '/truck_journey_update_completion/vm';
const truckJourneyUpdateFoundBuilder = 'truck_journey_update_found/vm';

//#Help Support
const helpSupportIndexScreenBuilder = '/help_support_index/vm';
//#Account Settings
const accountSettingsIndexScreenBuilder = '/account_settings_index/vm';
const changePasswordScreenBuilder = '/account_settings_index/change_password';

//#User Profile
const userProfileIndexScreenBuilder = '/user_profile/vm';

//CargoReports
const cargoReportHomeBuilder = '/cargo_report/cargo_report_home_screen/vm';
const cargoReportSearchBuilder = '/cargo_report/cargo_report_search/vm';
const cargoReportShowAllBuilder = '/cargo_report/cargo_report_show_all/vm';
const cargoReportResultsBuilder = '/cargo_report/cargo_report_results/vm';
const cargoMovementReportsBuilder = '/cargo_report/cargo_movement_report/vm';

//Port Events
const portEventIndexScreenBuilder =
    '/port_event_index/vm';
const portEventCaptureBuilder = '/port_event_capture/vm';
const portEventSummaryScreenBuilder =
    'port_event_summary/vm';
const portEventCompletionScreenBuilder = '/port_event_completion/vm';
const portEventRefuelBuilder = 'port_event_refuel/vm';

//Cargo Event Update
const cargoEventUpdateIndexBuilder = '/cargo_event_update/vm';
const cargoEventUpdateResultsBuilder = '/cargo_event_update_results/vm';
const cargoEventUpdateSummaryScreenBuilder = '/cargo_event_update_summary/vm';
const cargoEventUpdateCompletionScreenBuilder = '/cargo_event_update_completion/vm';

//Calculator
const cargoEventCalculatorHomeBuilder = '/cargo_event_calculator/vm';
