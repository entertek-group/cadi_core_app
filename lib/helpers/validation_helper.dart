import 'package:cadi_core_app/ui/shared/components/app/error_snack.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

bool isContainerInputValid(BuildContext context, String value,
    [bool showErrorSnack = false]) {
  String letterPart = value.replaceAll(RegExp(r'\d'), '');
  String numberPart = value.replaceAll(RegExp(r'[A-Za-z]'), '');

  if (value.trim() == "") {
    if (showErrorSnack) {
      errorSnack(context, 'Please input a container number');
    }
    return false;
  } else if (letterPart.length != 4 || numberPart.length != 7) {
    if (showErrorSnack) {
      errorSnack(context,
          'Please validate the container number (4 letters, 7 numbers)');
    }

    return false;
  } else {
    return true;
  }
}

class StowagePositionFormatter extends TextInputFormatter
{
  final String sample;
  final String separator;

  StowagePositionFormatter({
    required this.sample,
    required this.separator
});

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.isNotEmpty)
      {
        if (newValue.text.length > oldValue.text.length)
          {
            if (newValue.text.length > sample.length) return oldValue;
            if (newValue.text.length < sample.length && sample[newValue.text.length - 1] == separator)
              {
                return TextEditingValue(
                  text: '${oldValue.text}$separator${newValue.text.substring(newValue.text.length - 1)}',
                  selection: TextSelection.collapsed(offset: newValue.selection.end + 1)
                );
              }
          }
      }
    return newValue;
  }

}
