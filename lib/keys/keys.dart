class LoginKeys {
  static const String emailKey = '__login__email__';
  static const String passwordKey = '__login__password__';
}

class PasswordResetKeys {
  static const String emailKey = '__login__email__';
}

class CargoUpdateKeys {
  static const String searchBolKey = '__temp1__';
  static const String searchVesselKey = '__input_search_vessel__';
  static const String searchVoyageKey = '__input_search_voyage__';
  static const String inputContainerOneKey = '__input_container__one__';
  static const String inputContainerTwoKey = '__input_container__two__';
  static const String inputContainerThreeKey = '__input_container__three__';
  static const String inputContainerFourKey = '__input_container__four__';
}

class VesselUpdateKeys {
  static const String searchVesselKey = '__vessel_search_vessel__';
  static const String searchVoyageKey = '__vessel_search_voyage__';
  static const String selectPortKey = '__vessel_select_port__';
  static const String selectCarrierKey = '__vessel_select_carrier__';
  static const String selectRoleKey = '__vessel_select_role__';
  static const String selectAccountPortDepotKey =
      '__vessel_select_account_port_depot__';
  static const String searchVoyageDateKey = '__vessel_update_voyage_date__';
}

class CargoKeys {
  static const String selectAccountPortDepotKey =
      '__cargo_select_account_port_depot__';
  static const String selectPortKey = '__cargo_select_port__';
  static const String selectCarrierKey = '__cargo_select_carrier__';
  static const String selectStatusKey = '__cargo_select_status__';
  static const String selectRoleKey = '__cargo_select_role__';
  static const String selectCargoTypeKey = '__cargo_select_cargo_type__';
  static const String selectCraneKey = '__cargo_select_crane__';
  static const String selectContainerTypeKey =
      '__cargo_select_container_type__';
  static const String selectJobPackTypeKey = '__cargo_select_job_pack_type__';
  static const String commentKey = '__cargo_comment__';

  static const String searchBolKey = '__cargo_temp1__';
  static const String searchContainerKey = '__cargo_temp2__';
  static const String searchVesselKey = '__cargo_search_vessel__';
  static const String searchVoyageKey = '__cargo_search_voyage__';

  static const String inputContainerOneKey = '__cargo_input_container__one__';
  static const String inputContainerTwoKey = '__cargo_input_container__two__';
  static const String inputContainerThreeKey =
      '__cargo_input_container__three__';
  static const String inputContainerFourKey = '__cargo_input_container__four__';
  static const String inputContainerStowagePositionKey =
      '__cargo_input_container__stowage__position__';

  static const String truckRegistrationKey = '__cargo_truck_registration__';
  static const String truckDriverIdKey = '__cargo_truck_driver_id__';

  static const String inputRestowKey = '__cargo_input_restow__';
}

class RoadMovementKeys {
  static const String selectAccountPortDepotKey =
      '__truck_cargo_select_account_port_depot__';
  static const String selectPortKey = '__truck_cargo_select_port__';
  static const String selectVehicleKey = '__truck_cargo_select_vehicle__';
  static const String selectVehicleDriverKey = '__truck_cargo_select_vehicle_driver__';
  static const String selectVehicleOperatorKey = '__truck_cargo_select_vehicle_operator__';
  static const String selectStatusKey = '__truck_cargo_select_status__';
  static const String selectRoleKey = '__truck_cargo_select_role__';
  static const String commentKey = '__truck_cargo_comment__';
  static const String selectTruckTypeKey = '__truck_cargo_select_cargo_type__';
  static const String selectContainerTypeKey =
      '__truck_cargo_select_container_type__';
  static const String selectJobPackTypeKey =
      '__truck_cargo_select_job_pack_type__';

  // static const String searchBolKey = '__cargo_temp1__';
  static const String searchContainerKey = '__truck_cargo_temp2__';
  static const String searchEtaKey = '__truck_cargo_search_eta__';
  static const String searchLegNumberKey = '__truck_cargo_search_leg_number__';
  static const String searchWaybillKey = '__truck_cargo_search_waybill__';

  static const String inputContainerNumberKey =
      '__truck_cargo_input_container__number__';
  static const String inputContainerTwoKey =
      '__truck_cargo_input_container__two__';
  static const String inputContainerThreeKey =
      '__truck_cargo_input_container__three__';
  static const String selectCargoTypeKey = '__truck_select_cargo_type__';
  static const String inputSignatoryFullNameKey = '__truck_cargo_input_signatory__';
}

class RoadUpdateKeys {
  static const String searchVoyageDateKey = '__truck_update_voyage_date__';
}

class TruckJourneyKeys {
  static const String selectAccountPortDepotKey =
      '__truck_journey_select_account_port_depot__';
  static const String selectLocationKey = '__truck_journey_select_location__';
  static const String selectPortKey = '__truck_journey_select_port__';
  static const String selectCarrierKey = '__truck_journey_select_carrier__';
  static const String selectRoleKey = '__truck_journey_select_role__';
  // static const String selectVesselClassKey =
  //     '__truck_journey_select_vessel_class__';
  // static const String selectFuelTypeKey = '__truck_journey_select_fuel_type__';
  static const String selectCurrencyKey = '__truck_journey_select_currency__';

  static const String searchVesseNameKey =
      '__truck_journey_search_vessel_name__';
  static const String searchVoyageNumberKey = '__truck_journey_voyage_number__';
  static const String searchVoyageDateKey = '__truck_journey_voyage_date__';

  // static const String inputVesselNameKey =
  //     '__truck_journey_search_vessel_name_input__';
  // static const String inputVoyageNumberKey =
  //     '__truck_journey_voyage_number_input__';
  // static const String inputJourneyAtKey =
  //     '__truck_journey_journey_date_input__';
  // static const String inputNewDateKey = '__truck_journey_new_date_input__';

  static const String inputRefuelDateKey =
      '__truck_journey_refuel_date_input__';

  static const String inputRefuelVolumeKey =
      '__truck_journey_refuel_volume_input__';

  static const String inputRefuelPriceKey =
      '__truck_journey_refuel_price_input__';

  static const String inputRefuelTruckHoursKey =
      '__truck_journey_refuel_truck_hours_input__';

  static const String inputRefuelCommentKey =
      '__truck_journey_refuel_comment_input__';
}

class CargoEventKeys {
  static const String selectAccountPortDepotKey =
      '__cargo_event_select_account_port_depot__';
  static const String selectLocationKey = '__cargo_event_select_location__';
  static const String selectPortKey = '__cargo_event_select_port__';
  static const String selectCarrierKey = '__cargo_event_select_carrier__';
  static const String selectRoleKey = '__cargo_event_select_role__';
  static const String selectVesselClassKey =
      '__cargo_event_select_vessel_class__';


  static const String vesselNameKey =
      '__cargo_event_vessel_name__';
  static const String voyageNumberKey = '__cargo_event_voyage_number__';
  static const String startDateKey = '__cargo_event_start_date__';
  static const String endDateKey = '__cargo_event_end_date__';
  static const String searchCommentKey = '__cargo_event_comment_input__';
  static const String selectBerthKey = '__cargo_event_select_berth__';

  static const String inputVesselNameKey =
      '__cargo_event_search_vessel_name_input__';
  static const String inputVoyageNumberKey =
      '__cargo_event_voyage_number_input__';
  static const String inputJourneyAtKey = '__cargo_event_journey_date_input__';
  static const String inputNewDateKey = '__cargo_event_new_date_input__';


  static const String inputVolumeKey =
      '__port_event_refuel_volume_input__';

  static const String inputPriceKey =
      '__port_event_refuel_price_input__';

  static const String inputDistanceTravelledKey =
      '__port_event_refuel_vessel_hours_input__';
  static const String inputRefuelCommentKey =
      '__port_event_refuel_comment_input__';
  static const String selectFuelTypeKey = '__port_event_select_fuel_type__';
  static const String selectCurrencyKey = '__port_event_select_currency__';
  static const String selectVolumeUnitKey = '__port_event_select_volume_unit__';
  static const String selectDistanceTravelledUnityKey = '__port_event_select_distance_travelled__';
}

class UserProfileKeys {
  static const String emailKey = '__user_profile__email__';
  static const String nameKey = '__user_profile__name__';
  static const String mobileKey = '__user_profile__mobile__';
}

class ChangePasswordKeys {
  static const String oldPasswordKey = '__change_password_old_password__';
  static const String newPasswordKey = '__change_password_new_password__';
  static const String confirmPasswordKey =
      '__change_password_confirm_password__';
}

class ReportKeys {
  static const String inputFilterkey = '__input_filter__';
}
