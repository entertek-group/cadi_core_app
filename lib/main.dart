import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/reducers/app_reducer.dart';

import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_logging/redux_logging.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
//#Redux Library
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:cadi_core_app/redux/middleware/middleware.dart';
//#Ui
import 'package:cadi_core_app/ui/ui.dart';

//#Persisted Storage
import 'package:redux_persist/redux_persist.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';

import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'theme/theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Create Persistor
  final persistor = Persistor<AppState>(
    storage: FlutterStorage(), // Or use other engines
    serializer:
        JsonSerializer<AppState>(AppState.fromJson), // Or use other serializers
  );

  //Look at using transform to get around updates! https://github.com/Cretezy/redux_persist/tree/master/packages/redux_persist
  print(persistor.toString());

  // Load initial state
  AppState? initialState;
  try {
    initialState = await persistor.load();
  } catch (e) {
    print('Error loading initialState: $e');
  }

  final store = Store<AppState>(appReducer,
      initialState: initialState ?? AppState.initial(), //?? new AppState()
      middleware: [
        ...createAuthMiddleware(),
        ...createCargoMovementMiddleware(),
        ...createPortCargoUpdateMiddleware(),
        ...createAccountMiddleware(),
        ...createReferenceMiddleware(),
        ...createReportMiddleware(),
        ...createUiMiddleware(),
        ...createPortEventMiddleware(),
        ...createStorageMiddleware(),
        ...createRoadMovementMiddleware(),
        ...createConnectivityMiddleware(),
        const NavigationMiddleware<AppState>().call,
        LoggingMiddleware.printer().call,
        persistor.createMiddleware()
      ]);

  String title = 'CaDi Core';

  runApp(MyApp(
    store: store,
    title: title,
  ));
}

// ignore: must_be_immutable
class MyApp extends StatefulWidget {
  final Store<AppState> store;
  final String title;
  const MyApp({super.key, required this.store, required this.title});

  @override
  State<MyApp> createState() => _MyAppState(store: store, title: title);
}

class _MyAppState extends State<MyApp> {
  final Store<AppState> store;
  final String title;
  _MyAppState({required this.store, required this.title});

  @override
  void initState() {
    widget.store.dispatch(StopLoading());
    widget.store.dispatch(StopProcessingQueue());
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: widget.store,
      child: PlatformApp(
        debugShowCheckedModeBanner: false,
        material: (_, __) => MaterialAppData(theme: materialThemeData),
        cupertino: (_, __) => CupertinoAppData(theme: cupertinoTheme),
        title: title,
        navigatorKey: NavigatorHolder.navigatorKey,
        localizationsDelegates: const [DefaultMaterialLocalizations.delegate],
        routes: {
          InitScreen.route: (context) => const InitScreen(),
          WelcomeScreen.route: (context) => const WelcomeScreen(),
          LoginScreen.route: (context) => const LoginScreen(),
          LoginLoaderScreen.route: (context) => const LoginLoaderScreen(),
          HomeScreen.route: (context) => const HomeScreen(),
          CargoMovementFreightSearchScreenBuilder.route: (context) =>
              const CargoMovementFreightSearchScreenBuilder(),
          CargoMovementContainerScreenBuilder.route: (context) =>
              const CargoMovementContainerScreenBuilder(),
          CargoMovementStatusScreenBuilder.route: (context) =>
              const CargoMovementStatusScreenBuilder(),
          CargoMovementVehicleDetailsScreenBuilder.route: (context) =>
              const CargoMovementVehicleDetailsScreenBuilder(),
          CargoMovementSummaryScreenBuilder.route: (context) =>
              const CargoMovementSummaryScreenBuilder(),
          CargoMovementEirGenerationScreenBuilder.route: (context) =>
              const CargoMovementEirGenerationScreenBuilder(),
          CargoMovementCompletionScreenBuilder.route: (context) =>
              const CargoMovementCompletionScreenBuilder(),
          PasswordResetScreen.route: (context) => const PasswordResetScreen(),
          HelpSupportIndexScreenBuilder.route: (context) =>
              const HelpSupportIndexScreenBuilder(),
          AccountSettingsIndexScreenBuilder.route: (context) =>
              const AccountSettingsIndexScreenBuilder(),
          UserProfileScreenBuilder.route: (context) =>
              const UserProfileScreenBuilder(),
          ChangePasswordScreenBuilder.route: (context) =>
              const ChangePasswordScreenBuilder(),
          PortCargoUpdateHomeScreenBuilder.route: (context) =>
              const PortCargoUpdateHomeScreenBuilder(),
          PortCargoUpdateFreightSearchScreenBuilder.route: (context) =>
              const PortCargoUpdateFreightSearchScreenBuilder(),
          PortCargoUpdateContainerScreenBuilder.route: (context) =>
              const PortCargoUpdateContainerScreenBuilder(),
          PortCargoUpdateStatusScreenBuilder.route: (context) =>
              const PortCargoUpdateStatusScreenBuilder(),
          PortCargoUpdateSummaryScreenBuilder.route: (context) =>
              const PortCargoUpdateSummaryScreenBuilder(),
          PortCargoUpdateCompletionScreenBuilder.route: (context) =>
              const PortCargoUpdateCompletionScreenBuilder(),
          CargoReportHomeBuilder.route: (context) =>
              const CargoReportHomeBuilder(),
          CargoReportSearchBuilder.route: (context) =>
              const CargoReportSearchBuilder(),
          CargoReportResultsBuilder.route: (context) =>
              const CargoReportResultsBuilder(),
          CargoMovementReportBuilder.route: (context) =>
              const CargoMovementReportBuilder(),
          PortEventIndexScreenBuilder.route: (context) =>
              const PortEventIndexScreenBuilder(),
          PortEventCaptureBuilder.route: (context) =>
              const PortEventCaptureBuilder(),
          PortEventSummaryScreenBuilder.route: (context) =>
              const PortEventSummaryScreenBuilder(),
          PortEventCompletionScreenBuilder.route: (context) =>
              const PortEventCompletionScreenBuilder(),
          PortEventRefuelBuilder.route: (context) =>
              const PortEventRefuelBuilder(),
          RoadMovementFreightSearchScreenBuilder.route: (context) =>
              const RoadMovementFreightSearchScreenBuilder(),
          RoadMovementContainerScreenBuilder.route: (context) =>
              const RoadMovementContainerScreenBuilder(),
          RoadMovementStatusScreenBuilder.route: (context) =>
              const RoadMovementStatusScreenBuilder(),
          RoadMovementSummaryScreenBuilder.route: (context) =>
              const RoadMovementSummaryScreenBuilder(),
          RoadMovementCompletionScreenBuilder.route: (context) =>
              const RoadMovementCompletionScreenBuilder(),
          RoadMovementSignatureScreenBuilder.route: (context) =>
              const RoadMovementSignatureScreenBuilder(),
        },
      ),
    );
  }
}
