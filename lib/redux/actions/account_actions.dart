import 'package:cadi_core_app/redux/models/models.dart';
import 'dart:async';

class GetAccounts {
  final Completer completer;
  GetAccounts(this.completer);
}

class GetAccountsSuccess {
  final AccountList accountList;
  final Account account;

  GetAccountsSuccess(this.accountList, this.account);

  @override
  String toString() {
    return 'GetAccounts{AccountList: $accountList, Account: $account}';
  }
}

class GetAccountsError {
  final String error;

  GetAccountsError(this.error);

  @override
  String toString() {
    return 'GetAccounts{Error: $error}';
  }
}

class SetSelectedAccount {
  final Account account;

  SetSelectedAccount(this.account);

  @override
  String toString() {
    return 'SetAccount{Account: $account}';
  }
}

class FilterByAccountSelection {
  final Account account;
  final List<AccountPortDepot> filteredAccountPortDepots;
  final List<AccountPort> filteredAccountPorts;
  final List<AccountContainerType> filteredAccountContainerTypes;
  final List<AccountCarrier> filteredAccountCarriers;
  final List<AccountVessel> filteredAccountVessels;
  final List<AccountVehicleDriver> filteredAccountVehicleDrivers;
  final List<AccountVehicleDriver> filteredAccountVehicleOperators;
  final List<AccountDocumentType> filteredAccountDocumentTypes;
  final List<AccountCrane> filteredAccountCranes;
  final List<AccountVehicle> filteredAccountVehicles;
  final List<AccountBerth> filteredAccountBerths;

  FilterByAccountSelection(
      this.account,
      this.filteredAccountPortDepots,
      this.filteredAccountPorts,
      this.filteredAccountContainerTypes,
      this.filteredAccountCarriers,
      this.filteredAccountVessels,
      this.filteredAccountVehicleDrivers,
      this.filteredAccountVehicleOperators,
      this.filteredAccountDocumentTypes,
      this.filteredAccountCranes,
      this.filteredAccountVehicles,
      this.filteredAccountBerths
  );

  @override
  String toString() {
    return 'FilterByAccountSelection{account" $account, filteredAccountPortDepots: $filteredAccountPortDepots,'
        'filteredAccountPorts: $filteredAccountPorts,'
        'filteredAccountContainerTypes: $filteredAccountContainerTypes,'
        'filteredAccountCarriers: $filteredAccountCarriers,'
        'filteredAccountVessels: $filteredAccountVessels,'
        'filteredAccountVehicleDrivers: $filteredAccountVehicleDrivers,'
        'filteredAccountVehicleOperators: $filteredAccountVehicleOperators,'
        'filteredAccountDocumentTypes: $filteredAccountDocumentTypes,'
        'filteredAccountCranes: $filteredAccountCranes,'
        'filteredAccountVehicles: $filteredAccountVehicles,'
        'filteredAccountBerths: $filteredAccountBerths}';
  }
}

class GetAccountPortDepots {
  final Completer completer;
  GetAccountPortDepots(this.completer);
}

class GetAccountPortDepotsSuccess {
  final List<AccountPortDepot> accountPortDepots;
  final List<AccountPort> accountPorts;

  GetAccountPortDepotsSuccess(this.accountPortDepots, this.accountPorts);

  @override
  String toString() {
    return 'GetAccountPortDepotsSuccess{accountPortDepots: $accountPortDepots, accountPorts: $accountPorts}';
  }
}

class GetAccountPortDepotsError {
  final String error;

  GetAccountPortDepotsError(this.error);

  @override
  String toString() {
    return 'GetAccountPortDepots{Error: $error}';
  }
}

class SetActiveService {
  final String activeService;

  SetActiveService(this.activeService);

  @override
  String toString() {
    return 'SetActiveService{activeService: $activeService}';
  }
}

class GetAccountContainerTypes {
  final Completer completer;
  GetAccountContainerTypes(this.completer);
}

class GetAccountContainerTypesSuccess {
  final List<AccountContainerType> accountContainerTypes;

  GetAccountContainerTypesSuccess(this.accountContainerTypes);

  @override
  String toString() {
    return 'GetAccountContainerTypesSuccess{accountContainerTypes: $accountContainerTypes}';
  }
}

class GetAccountContainerTypesError {
  final String error;

  GetAccountContainerTypesError(this.error);

  @override
  String toString() {
    return 'GetAccountContainerTypes{Error: $error}';
  }
}

class FilterContainerTypesByAccountSelect {
  final List<AccountContainerType> filteredAccountContainerTypes;

  FilterContainerTypesByAccountSelect(this.filteredAccountContainerTypes);

  @override
  String toString() {
    return 'FilterContainerTypesByAccountSelect{filteredAccountContainerTypes: $filteredAccountContainerTypes}';
  }
}

class GetAccountVehicles{
  final Completer completer;
  GetAccountVehicles(this.completer);
}

class GetAccountVehiclesSuccess {
  final List<AccountVehicle> accountVehicles;

  GetAccountVehiclesSuccess(this.accountVehicles);

  @override
  String toString() {
    return 'GetAccountVehiclesSuccess{accountVehicles: $accountVehicles}';
  }
}

class GetAccountVehiclesError {
  final String error;

  GetAccountVehiclesError(this.error);

  @override
  String toString() {
    return 'GetAccountVehicles{Error: $error}';
  }
}


class GetAccountVehicleDrivers {
  final Completer completer;
  GetAccountVehicleDrivers(this.completer);
}

class GetAccountVehicleDriversSuccess {
  final List<AccountVehicleDriver> accountVehicleDrivers;

  GetAccountVehicleDriversSuccess(this.accountVehicleDrivers);

  @override
  String toString() {
    return 'GetAccountVehicleDriversSuccess{accountVehicleDrivers: $accountVehicleDrivers}';
  }
}

class GetAccountVehicleDriversError {
  final String error;

  GetAccountVehicleDriversError(this.error);

  @override
  String toString() {
    return 'GetAccountVehicleDrivers{Error: $error}';
  }
}

class GetAccountVehicleOperators {
  final Completer completer;
  GetAccountVehicleOperators(this.completer);
}

class GetAccountVehicleOperatorsSuccess {
  final List<AccountVehicleDriver> accountVehicleOperators;

  GetAccountVehicleOperatorsSuccess(this.accountVehicleOperators);

  @override
  String toString() {
    return 'GetAccountVehicleOperatorsSuccess{accountVehicleOperators: $accountVehicleOperators}';
  }
}

class GetAccountVehicleOperatorsError {
  final String error;

  GetAccountVehicleOperatorsError(this.error);

  @override
  String toString() {
    return 'GetAccountVehicleOperators{Error: $error}';
  }
}

class GetAccountDocumentTypes {
  final Completer completer;
  GetAccountDocumentTypes(this.completer);
}

class GetAccountDocumentTypesSuccess {
  final List<AccountDocumentType> accountDocumentTypes;

  GetAccountDocumentTypesSuccess(this.accountDocumentTypes);

  @override
  String toString() {
    return 'GetAccountDocumentTypesSuccess{accountDocumentTypes: $accountDocumentTypes}';
  }
}

class GetAccountDocumentTypesError {
  final String error;

  GetAccountDocumentTypesError(this.error);

  @override
  String toString() {
    return 'GetAccountDocumentTypes{Error: $error}';
  }
}

class GetAccountCarriers {
  final Completer completer;
  GetAccountCarriers(this.completer);
}

class GetAccountCarriersSuccess {
  final List<AccountCarrier> accountCarriers;

  GetAccountCarriersSuccess(this.accountCarriers);

  @override
  String toString() {
    return 'GetAccountCarriers{accountCarrier: $accountCarriers}';
  }
}

class GetAccountCarriersError {
  final String error;

  GetAccountCarriersError(this.error);

  @override
  String toString() {
    return 'GetAccountCarriers{Error: $error}';
  }
}

class FilterCarriersByAccountSelect {
  final List<AccountCarrier> filteredAccountCarriers;

  FilterCarriersByAccountSelect(this.filteredAccountCarriers);

  @override
  String toString() {
    return 'FilterCarriersByAccountSelects{filteredAccountCarriers: $filteredAccountCarriers}';
  }
}

class GetAccountVessels {
  final Completer completer;
  GetAccountVessels(this.completer);
}

class GetAccountVesselsSuccess {
  final List<AccountVessel> accountVessels;

  GetAccountVesselsSuccess(this.accountVessels);

  @override
  String toString() {
    return 'GetAccountVessels{accountVessels: $accountVessels}';
  }
}

class GetAccountVesselsError {
  final String error;

  GetAccountVesselsError(this.error);

  @override
  String toString() {
    return 'GetAccountVessels{Error: $error}';
  }
}

// class FilterVesselsByAccountSelect {
//   final List<AccountVessel> filteredAccountVessels;
//
//   FilterVesselsByAccountSelect(this.filteredAccountVessels);
//
//   @override
//   String toString() {
//     return 'FilterVesselsByAccountSelects{filteredAccountVessels: $filteredAccountVessels}';
//   }
// }

class GetAccountCranes {
  final Completer completer;
  GetAccountCranes(this.completer);
}

class GetAccountCranesSuccess {
  final List<AccountCrane> accountCranes;

  GetAccountCranesSuccess(this.accountCranes);

  @override
  String toString() {
    return 'GetAccountCranes{accountCranes: $accountCranes}';
  }
}

class GetAccountCranesError {
  final String error;

  GetAccountCranesError(this.error);

  @override
  String toString() {
    return 'GetAccountCranes{Error: $error}';
  }
}

class FilterCranesByAccountSelect {
  final List<AccountCrane> filteredAccountCranes;

  FilterCranesByAccountSelect(this.filteredAccountCranes);

  @override
  String toString() {
    return 'FilterCranesByAccountSelects{filteredAccountCranes: $filteredAccountCranes}';
  }
}

class GetAccountBerths {
  final Completer completer;
  GetAccountBerths(this.completer);
}

class GetAccountBerthsSuccess {
  final List<AccountBerth> accountBerths;

  GetAccountBerthsSuccess(this.accountBerths);

  @override
  String toString() {
    return 'GetAccountBerths{accountBerths: $accountBerths}';
  }
}

class GetAccountBerthsError {
  final String error;

  GetAccountBerthsError(this.error);

  @override
  String toString() {
    return 'GetAccountBerths{Error: $error}';
  }
}
