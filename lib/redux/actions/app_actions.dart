class PersistData {}

class PersistUI {}

class ToggleSettingsAutoDisplayNextDropdown {}

class StartLoading {}

class StopLoading {}

class StartDownloading {}

class StopDownloading {}

class StartSubmitting {}

class StopSubmitting {}

class StopShowingWelcome {}

class SetHomeIndex {
  final int index;

  SetHomeIndex(this.index);

  @override
  String toString() {
    return 'SetHomeIndex: $index}';
  }
}

class SetProcessingIndex {
  final int processingIndex;

  SetProcessingIndex(this.processingIndex);
}

class SetProcessingTotal {
  final int processingTotal;

  SetProcessingTotal(this.processingTotal);
}

class CompleteSignOut {
  // @override
  // String toString() {
  //   return 'AppState{Triggered Complete Signout}';
  // }
}
