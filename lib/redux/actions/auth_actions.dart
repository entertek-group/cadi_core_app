
import 'package:flutter/widgets.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'app_actions.dart';
import 'dart:async';

class LoadStateRequest {
  final BuildContext context;
  LoadStateRequest(this.context);
}

class Authenticate implements StartSubmitting {
  final Completer completer;
  final String email;
  final String password;
  Authenticate(this.completer, this.email, this.password);
}

class AuthenticateSuccess implements StopSubmitting {
  final Token token;
  final String email;

  AuthenticateSuccess(this.token, this.email);

  @override
  String toString() {
    return 'Authenticate{Token: $token, Email: $email}';
  }
}

class AuthenticateError implements StopSubmitting {
  final String error;

  AuthenticateError(this.error);

  @override
  String toString() {
    return 'Authenticate{Error: $error}';
  }
}

class GetMyUser {
  final Completer completer;
  GetMyUser(this.completer);
}

class GetMyUserSuccess {
  final User user;

  GetMyUserSuccess(this.user);

  @override
  String toString() {
    return 'GetMyUser{User: $user}';
  }
}

class GetMyUserError {
  final String error;

  GetMyUserError(this.error);

  @override
  String toString() {
    return 'GetMyUser{Error: $error}';
  }
}

class UserLoginSuccess implements StopLoading {}

class SignOutSuccess implements StopLoading {
  final Token token;
  final User user;

  SignOutSuccess(this.token, this.user);

  @override
  String toString() {
    return 'SignOutSuccess{Token: $token, User: $user}';
  }
}

class ClearAuthError {}

class TokenExpired {}

class UpdateUser implements StartSubmitting {
  final Completer completer;
  final String firstName;
  final String lastName;
  final String email;
  final String mobileNumber;

  UpdateUser(this.completer, this.firstName, this.lastName, this.email, this.mobileNumber);
}

class UpdateUserSuccess implements StopSubmitting {
  final User user;

  UpdateUserSuccess(this.user);

  @override
  String toString() {
    return 'UpdateUser{User: $user}';
  }
}

class UpdateUserError implements StopSubmitting {
  final String error;

  UpdateUserError(this.error);

  @override
  String toString() {
    return 'UpdateUser{Error: $error}';
  }
}

class UpdateUserPassword implements StartSubmitting {
  final Completer completer;
  final String currentPassword;
  final String password;
  final String passwordConfirmation;
  UpdateUserPassword(this.completer, this.currentPassword, this.password,
      this.passwordConfirmation);
}

class ForgetPassword implements StartLoading {
  final Completer completer;
  final String email;

  ForgetPassword(this.completer, this.email);
}

class ForgetPasswordSuccess implements StopLoading {}

class ForgetPasswordError implements StopLoading {
  final String error;

  ForgetPasswordError(this.error);

  @override
  String toString() {
    return 'ForgetPasswordError{Error: $error}';
  }
}

class UpdateUserPasswordSuccess implements StopSubmitting {
  final User user;

  UpdateUserPasswordSuccess(this.user);

  @override
  String toString() {
    return 'UpdateUserPassword{User: $user}';
  }
}

class UpdateUserPasswordError implements StopSubmitting {
  final String error;

  UpdateUserPasswordError(this.error);

  @override
  String toString() {
    return 'UpdateUserPassword{Error: $error}';
  }
}

class UpdateUserAvatar implements StartSubmitting {
  Completer completer;
  final String image;
  final String imageName;

  UpdateUserAvatar(this.completer, this.image, this.imageName);
}

class UpdateUserAvatarSuccess implements StopSubmitting {
  final User user;

  UpdateUserAvatarSuccess(this.user);

  @override
  String toString() {
    return 'UpdateUserAvatar{User: $user}';
  }
}

class UpdateUserAvatarError implements StopSubmitting {
  final String error;

  UpdateUserAvatarError(this.error);

  @override
  String toString() {
    return 'UpdateUserAvatar{Error: $error}';
  }
}
