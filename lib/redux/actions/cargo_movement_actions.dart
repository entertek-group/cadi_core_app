import 'package:cadi_core_app/redux/models/image/image_create.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'dart:async';

class UpdateCargoContainerNumber {
  final String containerNumber;

  UpdateCargoContainerNumber(this.containerNumber);
}

class UpdateCargoContainerWeight {
  final num containerWeight;

  UpdateCargoContainerWeight(this.containerWeight);
}

class UpdateCargoStowagePosition {
  final String stowagePosition;

  UpdateCargoStowagePosition(this.stowagePosition);
}

class UpdateCargoPackWeight {
  final num packWeight;

  UpdateCargoPackWeight(this.packWeight);
}

class UpdateCargoContainerWeightUnit {
  final String containerWeightUnit;

  UpdateCargoContainerWeightUnit(this.containerWeightUnit);
}

class UpdateCargoPackWeightUnit {
  final String packWeightUnit;

  UpdateCargoPackWeightUnit(this.packWeightUnit);
}

class UpdateCargoSealNumber1 {
  final String sealNumber1;

  UpdateCargoSealNumber1(this.sealNumber1);
}

class UpdateCargoMovementAt {
  final String movementAt;

  UpdateCargoMovementAt(this.movementAt);
}

class SetCargoContainerVariables {
  final String shipment;
  final String inputOne;
  final String inputTwo;
  final String inputThree;
  final String inputStowagePosition;
  final String selectedCargoType;
  final String selectedContainerType;
  final String selectedJobPackType;

  SetCargoContainerVariables(
      this.shipment,
      this.inputOne,
      this.inputTwo,
      this.inputThree,
      this.inputStowagePosition,
      this.selectedCargoType,
      this.selectedContainerType,
      this.selectedJobPackType);
}

class SetCargoStatusVariables {
  final String status;
  final String inputComment;
  final String inputRestow;
  List<ImageCreate>? attachedImages;
  final String? eir;  

  SetCargoStatusVariables(
      this.status, this.inputComment, this.inputRestow, this.attachedImages, this.eir);
}

class SetCargoMovementEirVariables {
  final String? signatoryFullName;
  final ImageCreate? signature;
  final String? consigneeOrConsignorName;
  final String? deliveryDocketNumber;
  final String? portOfLoadOrDischarge;

  SetCargoMovementEirVariables(
    this.signatoryFullName, this.signature, this.consigneeOrConsignorName, this.deliveryDocketNumber, this.portOfLoadOrDischarge);
}

class SetCargoFreightVariables {
  final String vesselName;
  final String voyageNumber;
  final int? craneId;

  SetCargoFreightVariables(this.vesselName, this.voyageNumber, this.craneId);
}

class SetCargoHomeVariables {
  final int accountPortDepot;
  final int carrierId;
  final String role;
  final int portId;

  SetCargoHomeVariables(
      this.accountPortDepot, this.carrierId, this.role, this.portId);
}

class SetCargoTruckDriverVariables {
  final String truckRegistration;
  final String truckDriverId;
  final String truckCompany;

  SetCargoTruckDriverVariables(
      this.truckRegistration, this.truckDriverId, this.truckCompany);
}

class SetAccountPortDepot {
  final int accountPortDepotId;

  SetAccountPortDepot(this.accountPortDepotId);
}

class UpdateCargoMovementCreate {
  final CargoMovementCreate cargoMovementCreate;

  UpdateCargoMovementCreate(this.cargoMovementCreate);

  @override
  String toString() {
    return 'UpdateCargoMovementCreate{cargoMovementCreate: $cargoMovementCreate}';
  }
}

class ResetCargoMovementCreate {}

class QueueCargoMovement {
  final CargoMovementCreate cargoMovementCreate;

  QueueCargoMovement(this.cargoMovementCreate);

  @override
  String toString() {
    return 'QueueCargoMovement{CargoMovementCreate: $cargoMovementCreate}';
  }
}

class UpdateQueueCargoMovement {
  final List<CargoCreateQueue> cargoCreateQueue;

  UpdateQueueCargoMovement(this.cargoCreateQueue);

  @override
  String toString() {
    return 'UpdateQueueCargoMovement{CargoCreateQueue: $cargoCreateQueue}';
  }
}

class RemoveCargoMovementFromQueue {
  final CargoCreateQueue cargoCreateQueue;

  RemoveCargoMovementFromQueue(this.cargoCreateQueue);

  @override
  String toString() {
    return 'RemoveCargoMovementFromQueue{CargoCreateQueue: $cargoCreateQueue}';
  }
}

class WriteCargoMovement {
  final Completer completer;
  final String uniqueId;
  final CargoMovementCreate cargoMovementCreate;

  WriteCargoMovement(this.completer, this.uniqueId, this.cargoMovementCreate);
}

class UploadCargoMovementImage {
  final Completer completer;
  final FileStore fileStore;

  UploadCargoMovementImage(this.completer, this.fileStore);
}

class FlagQueued {}

class UnFlagQueued {}

class StartProcessingQueue {}

class StopProcessingQueue {}
