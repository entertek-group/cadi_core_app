class SetConnectivity {
  final String connectionStatus;
  final String statusAt;

  SetConnectivity(this.connectionStatus, this.statusAt);

  @override
  String toString() {
    return 'SetConnectivity{ConnectionStatus: $connectionStatus, StatusAt: $statusAt}';
  }
}

class UpdateConnectivity {
  final String connectionStatus;
  final String statusAt;

  UpdateConnectivity(this.connectionStatus, this.statusAt);

  @override
  String toString() {
    return 'UpdateConnectivity{ConnectionStatus: $connectionStatus, StatusAt: $statusAt}';
  }

}

class EnableDataSync {}

class DisableDataSync {}
