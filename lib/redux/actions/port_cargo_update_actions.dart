import 'package:cadi_core_app/redux/models/models.dart';
import 'dart:async';

class UpdatePortCargoUpdateContainerNumber {
  final String containerNumber;

  UpdatePortCargoUpdateContainerNumber(this.containerNumber);
}

class UpdatePortCargoUpdateContainerWeight {
  final num containerWeight;

  UpdatePortCargoUpdateContainerWeight(this.containerWeight);
}

class UpdatePortCargoUpdatePackWeight {
  final num packWeight;

  UpdatePortCargoUpdatePackWeight(this.packWeight);
}

class UpdatePortCargoUpdateContainerWeightUnit {
  final String containerWeightUnit;

  UpdatePortCargoUpdateContainerWeightUnit(this.containerWeightUnit);
}

class UpdatePortCargoUpdatePackWeightUnit {
  final String packWeightUnit;

  UpdatePortCargoUpdatePackWeightUnit(this.packWeightUnit);
}

class UpdatePortCargoUpdateSealNumber1 {
  final String sealNumber1;

  UpdatePortCargoUpdateSealNumber1(this.sealNumber1);
}

class UpdatePortCargoUpdateAt {
  final String updateAt;

  UpdatePortCargoUpdateAt(this.updateAt);
}

class SetPortCargoUpdateContainerVariables {
  final String shipment;
  final String inputOne;
  final String inputTwo;
  final num grossWeight;
  final String weightUnit;
  final String selectedJobPackType;

  SetPortCargoUpdateContainerVariables(
      this.shipment,
      this.inputOne,
      this.inputTwo,
      this.grossWeight,
      this.weightUnit,
      this.selectedJobPackType);
}

class SetPortCargoUpdateStatusVariables {
  final String inputComment;
  SetPortCargoUpdateStatusVariables(this.inputComment);
}

class SetPortCargoUpdateFreightVariables {
  final String vesselName;
  final String voyageNumber;

  SetPortCargoUpdateFreightVariables(this.vesselName, this.voyageNumber);
}

class SetPortCargoUpdateHomeVariables {
  final int accountPortDepot;
  final int carrierId;
  final String role;

  SetPortCargoUpdateHomeVariables(
      this.accountPortDepot, this.carrierId, this.role);
}

class SetPortCargoUpdateAccountPortDepot {
  final int accountPortDepotId;

  SetPortCargoUpdateAccountPortDepot(this.accountPortDepotId);
}

class UpdatePortCargoUpdateCreate {
  final PortCargoUpdateCreate portCargoUpdateCreate;

  UpdatePortCargoUpdateCreate(this.portCargoUpdateCreate);

  @override
  String toString() {
    return 'UpdatePortCargoUpdateCreate{portCargoUpdateCreate: $portCargoUpdateCreate}';
  }
}

class ResetPortCargoUpdateCreate {}

class QueuePortCargoUpdate {
  final PortCargoUpdateCreate portCargoUpdateCreate;

  QueuePortCargoUpdate(this.portCargoUpdateCreate);

  @override
  String toString() {
    return 'QueuePortCargoUpdate{PortCargoUpdateCreate: $portCargoUpdateCreate}';
  }
}

class UpdateQueuePortCargoUpdate {
  final List<PortCargoUpdateQueue> portCargoUpdateQueue;

  UpdateQueuePortCargoUpdate(this.portCargoUpdateQueue);

  @override
  String toString() {
    return 'UpdateQueuePortCargoUpdate{PortCargoUpdateQueue: $portCargoUpdateQueue}';
  }
}

class RemovePortCargoUpdateFromQueue {
  final PortCargoUpdateQueue portCargoUpdateQueue;

  RemovePortCargoUpdateFromQueue(this.portCargoUpdateQueue);

  @override
  String toString() {
    return 'RemovePortCargoUpdateFromQueue{PortCargoUpdateQueue: $portCargoUpdateQueue}';
  }
}

class WritePortCargoUpdate {
  final Completer completer;
  final String uniqueId;
  final PortCargoUpdateCreate portCargoUpdateCreate;

  WritePortCargoUpdate(
      this.completer, this.uniqueId, this.portCargoUpdateCreate);
}

class FlagPortCargoUpdateQueued {}

class UnFlagPortCargoUpdateQueued {}

class StartPortCargoUpdateProcessingQueue {}

class StopPortCargoUpdateProcessingQueue {}
