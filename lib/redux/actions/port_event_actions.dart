import 'dart:async';
import 'package:cadi_core_app/redux/models/image/image_create.dart';
import 'package:cadi_core_app/redux/models/port_event/port_event_models.dart';

class QueuePortEvent {
  final PortEventCreate portEventCreate;

  QueuePortEvent(this.portEventCreate);

  @override
  String toString() {
    return 'QueuePortEvent{PortEvent: $portEventCreate}';
  }
}

class UpdateQueuePortEvent {
  final List<PortEventCreateQueue> portEventCreateQueue;

  UpdateQueuePortEvent(this.portEventCreateQueue);

  @override
  String toString() {
    return 'UpdateQueuePortEvent{PortEventCreateQueue: $portEventCreateQueue}';
  }
}

class RemovePortEventFromQueue {
  final PortEventCreateQueue portEventCreateQueue;

  RemovePortEventFromQueue(this.portEventCreateQueue);

  @override
  String toString() {
    return 'RemovePortEventFromQueue{PortEventCreateQueue: $portEventCreateQueue}';
  }
}

class UpdatePortEventEventAt {
  final String eventAt;

  UpdatePortEventEventAt(this.eventAt);
}

class UpdatePortEventEventCompleteAt {
  final String? eventCompleteAt;

  UpdatePortEventEventCompleteAt(this.eventCompleteAt);
}

class WritePortEvent {
  final Completer completer;
  final String uniqueId;
  final PortEventCreate portEventCreate;

  WritePortEvent(this.completer, this.uniqueId, this.portEventCreate);
}

class ResetPortEvent {}

class SetPortEventIndexVariables {
  final int accountPortLocationId;
  final int carrierId;
  final String role;

  SetPortEventIndexVariables(
      this.accountPortLocationId, this.carrierId, this.role);
}

class SetPortEventCaptureVariables {
  final String vesselName;
  final String voyageNumber;
  final int? accountPortBerthId;
  final String comment;
  final String eventAt;
  final String eventCompleteAt;
  final List<ImageCreate>? images;

  SetPortEventCaptureVariables(this.vesselName, this.voyageNumber, this.accountPortBerthId, this.comment, this.eventAt, this.eventCompleteAt, this.images);

}

class SetPortEventRefuelVariables {
  final int fuelTypeId;
  final num volume;
  final String volumeUnit;
  final num price;
  final String priceCurrency;
  final num distanceTravelled;
  final String distanceTravelledUnit;

  SetPortEventRefuelVariables(this.fuelTypeId, this.volume, this.volumeUnit, this.price, this.priceCurrency, this.distanceTravelled, this.distanceTravelledUnit);

}

class UpdatePortEventCreate {
  final PortEventCreate portEventCreate;

  UpdatePortEventCreate(this.portEventCreate);

  @override
  String toString() {
    return 'UpdatePortEventCreate{PortEvent: $portEventCreate}';
  }
}

class FlagPortEventQueued {}

class UnFlagPortEventQueued {}

class StartPortEventProcessingQueue {}

class StopPortEventProcessingQueue {}