import 'package:cadi_core_app/redux/models/models.dart';
import 'dart:async';

class GetVesselClasses {
  final Completer completer;
  GetVesselClasses(this.completer);
}

class GetVesselClassesSuccess {
  final List<CadiGeneric> vesselClasses;

  GetVesselClassesSuccess(this.vesselClasses);

  @override
  String toString() {
    return 'GetVesselClasses{vesselClasses: $vesselClasses}';
  }
}

class GetVesselClassesError {
  final String error;

  GetVesselClassesError(this.error);

  @override
  String toString() {
    return 'GetVesselClasses{Error: $error}';
  }
}

class GetFuelTypes {
  final Completer completer;
  GetFuelTypes(this.completer);
}

class GetFuelTypesSuccess {
  final List<FuelType> fuelTypes;

  GetFuelTypesSuccess(this.fuelTypes);

  @override
  String toString() {
    return 'GetFuelTypes{fuelTypes: $fuelTypes}';
  }
}

class GetFuelTypesError {
  final String error;

  GetFuelTypesError(this.error);

  @override
  String toString() {
    return 'GetFuelTypes{Error: $error}';
  }
}

class GetCargoTypes {
  final Completer completer;
  GetCargoTypes(this.completer);
}

class GetCargoTypesSuccess {
  final List<CadiGeneric> cargoTypes;

  GetCargoTypesSuccess(this.cargoTypes);

  @override
  String toString() {
    return 'GetCargoTypes{cargoTypes: $cargoTypes}';
  }
}

class GetCargoTypesError {
  final String error;

  GetCargoTypesError(this.error);

  @override
  String toString() {
    return 'GetCargoTypes{Error: $error}';
  }
}

class GetJobPackTypes {
  final Completer completer;
  GetJobPackTypes(this.completer);
}

class GetJobPackTypesSuccess {
  final List<CadiGeneric> jobPackTypes;

  GetJobPackTypesSuccess(this.jobPackTypes);

  @override
  String toString() {
    return 'GetJobPackTypes{jobPackTypes: $jobPackTypes}';
  }
}

class GetJobPackTypesError {
  final String error;

  GetJobPackTypesError(this.error);

  @override
  String toString() {
    return 'GetJobPackTypes{Error: $error}';
  }
}