import 'dart:async';

import 'package:cadi_core_app/redux/models/models.dart';

import 'app_actions.dart';

class GetPortReport implements StartLoading
{
  final Completer completer;
  final String unLocode;
  final String vesselName;
  final String voyageNumber;
  final String reportType;

  GetPortReport (this.completer, this.unLocode, this.vesselName, this.voyageNumber, this.reportType);
}

class GetPortReportSuccess implements StopLoading {
  final PortReportSummary portReportSummary;

  GetPortReportSuccess(this.portReportSummary);

  @override
  String toString() {
    return 'GetPortReportSuccess{cargoMovements: $portReportSummary}';
  }
}

class GetPortReportError implements StopLoading {
  final String error;

  GetPortReportError(this.error);

  @override
  String toString() {
    return 'GetPortReport{Error: $error}';
  }
}

class GetPortMovementReportSuccess implements StopLoading {
  final PortMovementReportSummary portMovementReportSummary;

  GetPortMovementReportSuccess(this.portMovementReportSummary);

  @override
  String toString() {
    return 'GetPortMovementReportSuccess{cargoMovementMovements: $portMovementReportSummary}';
  }
}

class ResetShortReports {}

class ResetReportCargoMovements {}

class FilterReportCargoMovementsByRole {
  final String role;

  FilterReportCargoMovementsByRole(this.role);

  @override
  String toString() {
    return 'FilterReportCargoMovementsByRole{role: $role}';
  }
}

class FilterReportShortDataByRole {
  final String role;

  FilterReportShortDataByRole(this.role);

  @override
  String toString() {
    return 'FilterReportShortDataByRole{role: $role}';
  }
}

class SetReportType {
  final String reportType;

  SetReportType(this.reportType);

  @override
  String toString() {
    return 'SetReportType(reportType: $reportType)';
  }
}
