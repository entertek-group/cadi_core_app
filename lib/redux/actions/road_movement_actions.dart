import 'package:cadi_core_app/redux/models/cargo_data/container_data.dart';
import 'package:cadi_core_app/redux/models/image/image_create.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'dart:async';

class UpdateRoadContainerNumber {
  final String previousContainerNumber;
  final String containerNumber;

  UpdateRoadContainerNumber(this.previousContainerNumber, this.containerNumber);
}

class SetRoadContainerVariables {
  final List<ContainerData>? containers;
  final bool isBreakBulk;

  SetRoadContainerVariables(this.containers, this.isBreakBulk);
}

class SetAccountVehicleDriverId {
  final int accountVehicleDriverId;

  SetAccountVehicleDriverId(this.accountVehicleDriverId);
}

class SetRoadStatusVariables {
  final String status;
  final String inputComment;
  final List<ImageCreate>? attachedImages;

  SetRoadStatusVariables(this.status, this.inputComment, this.attachedImages);
}

class SetRoadFreightVariables {
  final String waybill;
  final String legNumber;
  final String? eta;
  final List<ContainerData>? containers;
  final bool isBreakBulk;
  final List<ImageCreate>? attachedDocuments;

  SetRoadFreightVariables(this.waybill, this.legNumber, this.eta,
      this.containers, this.isBreakBulk, this.attachedDocuments);
}

class SetRoadHomeVariables {
  final int accountPortDepot;
  final int? vehicleId;
  final int? vehicleDriverId;
  final int? vehicleOperatorId;
  final String role;

  SetRoadHomeVariables(this.accountPortDepot, this.vehicleId,
      this.vehicleDriverId, this.vehicleOperatorId, this.role);
}

class SetRoadDriverVariables {
  final String vehicleRegistration;
  final String vehicleDriverName;

  SetRoadDriverVariables(this.vehicleRegistration, this.vehicleDriverName);
}

class SetRoadAccountPortDepot {
  final int accountPortDepotId;

  SetRoadAccountPortDepot(this.accountPortDepotId);
}

class UpdateRoadMovementCreate {
  final RoadMovementCreate roadMovementCreate;

  UpdateRoadMovementCreate(this.roadMovementCreate);

  @override
  String toString() {
    return 'UpdateRoadMovementCreate{roadMovementCreate: $roadMovementCreate}';
  }
}

class ResetRoadMovementCreate {}

class QueueRoadMovement {
  final RoadMovementCreate roadMovementCreate;

  QueueRoadMovement(this.roadMovementCreate);

  @override
  String toString() {
    return 'QueueRoadMovement{RoadMovementCreate: $roadMovementCreate}';
  }
}

class UpdateQueueRoadMovement {
  final List<RoadMovementCreateQueue> roadMovementCreateQueue;

  UpdateQueueRoadMovement(this.roadMovementCreateQueue);

  @override
  String toString() {
    return 'UpdateQueueRoadMovement{RoadMovementCreateQueue: $roadMovementCreateQueue}';
  }
}

class RemoveRoadMovementFromQueue {
  final RoadMovementCreateQueue roadMovementCreateQueue;

  RemoveRoadMovementFromQueue(this.roadMovementCreateQueue);

  @override
  String toString() {
    return 'RemoveRoadMovementFromQueue{RoadMovementCreateQueue: $roadMovementCreateQueue}';
  }
}

class WriteRoadMovement {
  final Completer completer;
  final String uniqueId;
  final RoadMovementCreate roadMovementCreate;

  WriteRoadMovement(this.completer, this.uniqueId, this.roadMovementCreate);
}

class UploadRoadMovementImage {
  final Completer completer;
  final FileStore fileStore;

  UploadRoadMovementImage(this.completer, this.fileStore);
}

class SetRoadMovementSignature {
  final String? signatoryFullName;
  final ImageCreate? signature;
  final List<ImageCreate>? attachedDocuments;

  SetRoadMovementSignature(
      this.signatoryFullName, this.signature, this.attachedDocuments);
}

class AddRoadMovementImages {
  final List<ImageCreate> roadImages;

  AddRoadMovementImages(this.roadImages);
}

class ClearRoadMovementImages {
  ClearRoadMovementImages();
}

class FlagRoadMovementQueued {}

class UnFlagRoadMovementQueued {}

class StartRoadMovementProcessingQueue {}

class StopRoadMovementProcessingQueue {}

class RemoveItemsFromQueue {
  final List<RoadMovementCreateQueue> items;

  RemoveItemsFromQueue(this.items);
}
