import 'dart:io';

import 'package:cadi_core_app/redux/models/models.dart';

class AddFileToStorage {
  final FileStore fileStore;

  AddFileToStorage(this.fileStore);

  @override
  String toString() {
    return 'AddFileToStorage{FileStore: $fileStore}';
  }
}

class RemoveFileFromStorage {
  final String uniqueId;
  final String storageType;

  RemoveFileFromStorage(this.uniqueId, this.storageType);

  @override
  String toString() {
    return 'RemoveFileFromStorage{uniqueId: $uniqueId, storageType: $storageType}';
  }
}

class UpdateFileStores {
  final List<FileStore> fileStores;

  UpdateFileStores(this.fileStores);

  @override
  String toString() {
    return 'UpdateFileStores{fileStores: $fileStores}';
  }
}

class StoreTemporaryFile {
  final File file;
  final String storageType;

  StoreTemporaryFile(this.file, this.storageType);

  @override
  String toString() {
    return 'StoreTemporaryFile{File: $file, storageType: $storageType}';
  }
}

class MoveTempStorageToFileStores {
  final String uniqueId;

  MoveTempStorageToFileStores(this.uniqueId);

  @override
  String toString() {
    return 'MoveTempStorageToFileStores{UniqueId: $uniqueId}';
  }
}

class UpdateApiIdInFileStores {
  final String uniqueId;
  final String storageType;
  final int apiId;

  UpdateApiIdInFileStores(this.uniqueId, this.storageType, this.apiId);

  @override
  String toString() {
    return 'UpdateApiIdInFileStoress{UniqueId: $uniqueId, storageType: $storageType, apiId: $apiId}';
  }
}

class ResetTempStorage {}

class CompleteTempStorageReset {}

class UpdateTempStorage {
  final FileStore fileStore;

  UpdateTempStorage(this.fileStore);

  @override
  String toString() {
    return 'UpdateTempStorage{FileStore: $fileStore}';
  }
}
