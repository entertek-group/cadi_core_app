import 'package:cadi_core_app/redux/models/models.dart';

import '../models/ui_screen/ui_screen_models.dart';

class UiDropDownAutoContinueOn {}

class UiDropDownAutoContinueOff {}

class UiAutoSubmitPageOn {}

class UiAutoSubmitPageOff {}

class UiCargoHomeSetPortIndex {
  final int selectedPortIndex;

  UiCargoHomeSetPortIndex(this.selectedPortIndex);

  @override
  String toString() {
    return 'UiCargoHomeSetPortIndex{selectedPortIndex: $selectedPortIndex}';
  }
}

class UiCargoHomeSetLocationIndex {
  final int selectedLocationIndex;

  UiCargoHomeSetLocationIndex(this.selectedLocationIndex);

  @override
  String toString() {
    return 'UiCargoHomeSetLocationIndex{selectedLocationIndex: $selectedLocationIndex}';
  }
}

class UiCargoHomeSetCarrierIndex {
  final int selectedCarrierIndex;

  UiCargoHomeSetCarrierIndex(this.selectedCarrierIndex);

  @override
  String toString() {
    return 'UiCargoHomeSetCarrierIndex{selectedCarrierIndex: $selectedCarrierIndex}';
  }
}

class UiCargoUpdateSearchSetVariables {
  final String searchType;
  final String containerNumber;
  final String vesselName;
  final String voyageNumber;
  final String jobNumber;

  UiCargoUpdateSearchSetVariables(this.searchType, this.containerNumber,
      this.vesselName, this.voyageNumber, this.jobNumber);

  @override
  String toString() {
    return 'UiCargoUpdateSearch{searchType: $searchType, $containerNumber, vesselName: $vesselName, voyageNumber: $voyageNumber, jobNumber: $jobNumber}';
  }
}

class UiCargoUpdateSearchUpdate {
  final UiCargoUpdateSearch uiCargoUpdateSearch;

  UiCargoUpdateSearchUpdate(this.uiCargoUpdateSearch);

  @override
  String toString() {
    return 'UiCargoUpdateSearch{uiCargoUpdateSearch: $uiCargoUpdateSearch}';
  }
}

class UiCargoUpdateSearchReset {}

class UiCargoHomeSetRoleIndex {
  final int selectedRoleIndex;

  UiCargoHomeSetRoleIndex(this.selectedRoleIndex);

  @override
  String toString() {
    return 'UiCargoHomeSetRoleIndex{selectedRoleIndex: $selectedRoleIndex}';
  }
}

class UiPortCargoUpdateSetRole {
  final String portCargoUpdateRole;

  UiPortCargoUpdateSetRole(this.portCargoUpdateRole);

  @override
  String toString() {
    return 'UiPortCargoUpdateSetRole{portCargoUpdateRole: $portCargoUpdateRole}';
  }
}

class UiPortCargoUpdateSetPortIndex {
  final int selectedPortIndex;

  UiPortCargoUpdateSetPortIndex(this.selectedPortIndex);

  @override
  String toString() {
    return 'UiPortCargoUpdateSetPortIndex{selectedPortIndex: $selectedPortIndex}';
  }
}

class UiPortCargoUpdateSetLocationIndex {
  final int selectedLocationIndex;

  UiPortCargoUpdateSetLocationIndex(this.selectedLocationIndex);

  @override
  String toString() {
    return 'UiPortCargoUpdateSetLocationIndex{selectedLocationIndex: $selectedLocationIndex}';
  }
}

class UiPortCargoUpdateSetCarrierIndex {
  final int selectedCarrierIndex;

  UiPortCargoUpdateSetCarrierIndex(this.selectedCarrierIndex);

  @override
  String toString() {
    return 'UiPortCargoUpdateSetCarrierIndex{selectedCarrierIndex: $selectedCarrierIndex}';
  }
}

class UiPortCargoUpdateSetRoleIndex {
  final int selectedRoleIndex;

  UiPortCargoUpdateSetRoleIndex(this.selectedRoleIndex);

  @override
  String toString() {
    return 'UiPortCargoUpdateSetRoleIndex{selectedRoleIndex: $selectedRoleIndex}';
  }
}

class UiPortCargoUpdateHomeReset {}

class UiPortCargoUpdateHomeUpdate {
  final UiPortCargoUpdateHome uiPortCargoUpdateHome;

  UiPortCargoUpdateHomeUpdate(this.uiPortCargoUpdateHome);

  @override
  String toString() {
    return 'UiPortCargoUpdateHomeUpdate{uiPortCargoUpdateHome: $uiPortCargoUpdateHome}';
  }
}

class UiPortCargoUpdateFreightSearchSetVesselName {
  final String vesselName;

  UiPortCargoUpdateFreightSearchSetVesselName(this.vesselName);

  @override
  String toString() {
    return 'UiPortCargoUpdateFreightSearchSetVesselName{vesselName: $vesselName}';
  }
}

class UiPortCargoUpdateFreightSearchSetVoyageNumber {
  final String voyageNumber;

  UiPortCargoUpdateFreightSearchSetVoyageNumber(this.voyageNumber);

  @override
  String toString() {
    return 'UiPortCargoUpdateFreightSearchSetVoyageNumber{voyageNumber: $voyageNumber}';
  }
}

class UiPortCargoUpdateFreightSearchReset {}

class UiPortCargoUpdateFreightSearchUpdate {
  final UiPortCargoUpdateFreightSearch uiPortCargoUpdateFreightSearch;

  UiPortCargoUpdateFreightSearchUpdate(this.uiPortCargoUpdateFreightSearch);

  @override
  String toString() {
    return 'UiPortCargoUpdateFreightSearchUpdate{uiPortCargoUpdateFreightSearch: $uiPortCargoUpdateFreightSearch}';
  }
}

class UiCargoHomeReset {}

class UiCargoHomeUpdate {
  final UiCargoHome uiCargoHome;

  UiCargoHomeUpdate(this.uiCargoHome);

  @override
  String toString() {
    return 'UiCargoHomeUpdate{uiCargoHome: $uiCargoHome}';
  }
}

class UiCargoTruckDetailsReset {}

class UiCargoTruckDetailsUpdate {
  final UiCargoTruckDetails uiCargoTruckDetails;

  UiCargoTruckDetailsUpdate(this.uiCargoTruckDetails);

  @override
  String toString() {
    return 'UiCargoTruckDetailsUpdate{uiCargoTruckDetail: $uiCargoTruckDetails}';
  }
}

class UiCargoSetRole {
  final String cargoRole;

  UiCargoSetRole(this.cargoRole);

  @override
  String toString() {
    return 'UiCargoSetRole{cargoRole: $cargoRole}';
  }
}

class UiCargoHomeFreightSearchSetVesselName {
  final String vesselName;

  UiCargoHomeFreightSearchSetVesselName(this.vesselName);

  @override
  String toString() {
    return 'UiCargoHomeFreightSearchSetVesselName{vesselName: $vesselName}';
  }
}

class UiCargoHomeFreightSearchSetVoyageNumber {
  final String voyageNumber;

  UiCargoHomeFreightSearchSetVoyageNumber(this.voyageNumber);

  @override
  String toString() {
    return 'UiCargoHomeFreightSearchSetVoyageNumber{voyageNumber: $voyageNumber}';
  }
}

class UiCargoHomeFreightSelectCrane {
  final int craneId;

  UiCargoHomeFreightSelectCrane(this.craneId);

  @override
  String toString() {
    return 'UiCargoHomeFreightSelectCrane{craneId: $craneId}';
  }
}

class UiCargoFreightSearchReset {}

class UiCargoFreightSearchUpdate {
  final UiCargoFreightSearch uiCargoFreightSearch;

  UiCargoFreightSearchUpdate(this.uiCargoFreightSearch);

  @override
  String toString() {
    return 'UiCargoFreightSearchUpdate{uiCargoFreightSearch: $uiCargoFreightSearch}';
  }
}

class UiCargoHomeTruckDetailsSetTruckRegistration {
  final String truckRegistration;

  UiCargoHomeTruckDetailsSetTruckRegistration(this.truckRegistration);

  @override
  String toString() {
    return 'UiCargoHomeTruckDetailsSetTruckRegistration{truckRegistration: $truckRegistration}';
  }
}

class UiCargoHomeTruckDetailsSetTruckDriverId {
  final String truckDriverId;

  UiCargoHomeTruckDetailsSetTruckDriverId(this.truckDriverId);

  @override
  String toString() {
    return 'UiCargoHomeTruckDetailsSetTruckDriverId{truckDriverId: $truckDriverId}';
  }
}

class UiCargoReportSetReportType {
  final String reportType;

  UiCargoReportSetReportType(this.reportType);

  @override
  String toString() {
    return 'UiCargoReportSetReportType{reportType: $reportType}';
  }
}

class UiCargoReportSetPortIndex {
  final int selectedPortIndex;

  UiCargoReportSetPortIndex(this.selectedPortIndex);

  @override
  String toString() {
    return 'UiCargoReportSetPortIndex{selectedPortIndex: $selectedPortIndex}';
  }
}

class UiCargoReportSetLocationIndex {
  final int selectedLocationIndex;

  UiCargoReportSetLocationIndex(this.selectedLocationIndex);

  @override
  String toString() {
    return 'UiCargoReportSetLocationIndex{selectedLocationIndex: $selectedLocationIndex}';
  }
}

class UiCargoReportSetSailingSchedule {
  final String vesselName;
  final String voyageNumber;

  UiCargoReportSetSailingSchedule(this.vesselName, this.voyageNumber);

  @override
  String toString() {
    return 'UiCargoReportSetSailingSchedule{vesselName: $vesselName, voyageNumber: $voyageNumber}';
  }
}

class UiCargoReportSearchUpdate {
  final UiCargoReportSearch uiCargoReportSearch;

  UiCargoReportSearchUpdate(this.uiCargoReportSearch);

  @override
  String toString() {
    return 'UiCargoReportSearch{uiCargoReportSearch: $uiCargoReportSearch}';
  }
}

class UiCargoReportSearchReset {}

class UiPortEventIndexSetPortIndex {
  final int selectedPortIndex;

  UiPortEventIndexSetPortIndex(this.selectedPortIndex);

  @override
  String toString() {
    return 'UiPortEventIndexSetPortIndex{selectedPortIndex: $selectedPortIndex}';
  }
}

class UiPortEventIndexSetLocationIndex {
  final int selectedLocationIndex;

  UiPortEventIndexSetLocationIndex(this.selectedLocationIndex);

  @override
  String toString() {
    return 'UiPortEventIndexSetLocationIndex{selectedLocationIndex: $selectedLocationIndex}';
  }
}

class UiPortEventIndexSetCarrierIndex {
  final int selectedCarrierIndex;

  UiPortEventIndexSetCarrierIndex(this.selectedCarrierIndex);

  @override
  String toString() {
    return 'UiPortEventIndexSetCarrierIndex{selectedCarrierIndex: $selectedCarrierIndex}';
  }
}

class UiPortEventIndexSetRoleIndex {
  final int selectedRoleIndex;

  UiPortEventIndexSetRoleIndex(this.selectedRoleIndex);

  @override
  String toString() {
    return 'UiPortEventIndexSetRoleIndex{selectedRoleIndex: $selectedRoleIndex}';
  }
}

class UiPortEventIndexUpdate {
  final UiPortEventIndex uiPortEventIndex;

  UiPortEventIndexUpdate(this.uiPortEventIndex);
  @override
  String toString() {
    return 'UiPortEventIndexUpdate{uiPortEventIndex: $uiPortEventIndex}';
  }
}

class UiPortEventIndexReset {}

//Work Down from here

class UiPortEventSetRole {
  final String portEventRole;

  UiPortEventSetRole(this.portEventRole);

  @override
  String toString() {
    return 'UiPortEventSetRole{portEventRole: $portEventRole}';
  }
}

class UiPortMovementCaptureSetVesselName {
  final String vesselName;

  UiPortMovementCaptureSetVesselName(this.vesselName);

  @override
  String toString() {
    return 'UiPortMovementCaptureSetVesselName{vesselName: $vesselName}';
  }
}

class UiPortMovementCaptureSetVoyageNumber {
  final String voyageNumber;

  UiPortMovementCaptureSetVoyageNumber(this.voyageNumber);

  @override
  String toString() {
    return 'UiPortMovementCaptureSetVoyageNumber{voyageNumber: $voyageNumber}';
  }
}

class UiPortMovementCaptureReset {}

class UiPortEventCaptureSetVariables {
  final String vesselName;
  final String voyageNumber;
  final int berthIndex;

  UiPortEventCaptureSetVariables(
      this.vesselName, this.voyageNumber, this.berthIndex);

  @override
  String toString() {
    return 'UiPortEventCaptureSetVariables{vesselName: $vesselName, voyageNumber: $voyageNumber, berthIndex: $berthIndex}';
  }
}

class UiPortEventCaptureUpdate {
  final UiPortEventCapture uiPortEventCapture;

  UiPortEventCaptureUpdate(this.uiPortEventCapture);

  @override
  String toString() {
    return 'UiPortEventCaptureUpdate{uiPortEventCapture: $uiPortEventCapture}';
  }
}

class UiVesselUpdateIndexSetVesselName {
  final String vesselName;

  UiVesselUpdateIndexSetVesselName(this.vesselName);

  @override
  String toString() {
    return 'UiVesselUpdateIndexSetVesselName{vesselName: $vesselName}';
  }
}

class UiVesselUpdateIndexSetVoyageNumber {
  final String voyageNumber;

  UiVesselUpdateIndexSetVoyageNumber(this.voyageNumber);

  @override
  String toString() {
    return 'UiVesselUpdateIndexSetVoyageNumber{voyageNumber: $voyageNumber}';
  }
}

class UiVesselUpdateIndexSetVoyageDate {
  final String voyageDate;

  UiVesselUpdateIndexSetVoyageDate(this.voyageDate);

  @override
  String toString() {
    return 'UiVesselUpdateIndexSetVoyageDate{voyageDate: $voyageDate}';
  }
}

class UiVesselUpdateIndexSetPortIndex {
  final int selectedPortIndex;

  UiVesselUpdateIndexSetPortIndex(this.selectedPortIndex);

  @override
  String toString() {
    return 'UiVesselUpdateIndexSetPortIndex{selectedPortIndex: $selectedPortIndex}';
  }
}

class UiVesselUpdateIndexSetLocationIndex {
  final int selectedLocationIndex;

  UiVesselUpdateIndexSetLocationIndex(this.selectedLocationIndex);

  @override
  String toString() {
    return 'UiVesselUpdateIndexSetLocationIndex{selectedLocationIndex: $selectedLocationIndex}';
  }
}

class UiVesselUpdateIndexReset {}

class UiVesselUpdateIndexUpdate {
  final UiVesselUpdateIndex uiVesselUpdateIndex;

  UiVesselUpdateIndexUpdate(this.uiVesselUpdateIndex);

  @override
  String toString() {
    return 'UiVesselUpdateIndexUpdate{uiVesselUpdateIndex: $uiVesselUpdateIndex}';
  }
}

class UiVehicleDriverSetIndex {
  final int selectedVehicleDriverIndex;

  UiVehicleDriverSetIndex(this.selectedVehicleDriverIndex);

  @override
  String toString() {
    return 'UiVehicleDriverSetIndex{selectedRoleIndex: $selectedVehicleDriverIndex}';
  }
}

/// **************************
/// #ROAD MOVEMENTS
///***************************/

class UiRoadMovementSetRole {
  final String roadMovementRole;

  UiRoadMovementSetRole(this.roadMovementRole);

  @override
  String toString() {
    return 'UiRoadMovementSetRole{roadMovementRole: $roadMovementRole}';
  }
}

class SetUiRoadMovementHome {
  final UiRoadMovementHome uiRoadMovementHome;

  SetUiRoadMovementHome(this.uiRoadMovementHome);

  @override
  String toString() {
    return 'SetUiRoadMovementHome{uiRoadMovementHome: $uiRoadMovementHome}';
  }
}

class UiRoadMovementHomeSetPortIndex {
  final int selectedPortIndex;

  UiRoadMovementHomeSetPortIndex(this.selectedPortIndex);

  @override
  String toString() {
    return 'UiRoadMovementHomeSetPortIndex{selectedPortIndex: $selectedPortIndex}';
  }
}

class UiRoadMovementHomeSetLocationIndex {
  final int selectedLocationIndex;

  UiRoadMovementHomeSetLocationIndex(this.selectedLocationIndex);

  @override
  String toString() {
    return 'UiRoadMovementHomeSetLocationIndex{selectedLocationIndex: $selectedLocationIndex}';
  }
}

class UiRoadMovementHomeSetRoleIndex {
  final int selectedRoleIndex;

  UiRoadMovementHomeSetRoleIndex(this.selectedRoleIndex);

  @override
  String toString() {
    return 'UiRoadMovementHomeSetRoleIndex{selectedRoleIndex: $selectedRoleIndex}';
  }
}

class UiRoadMovementHomeSetVehicleIndex {
  final int selectedVehicleIndex;

  UiRoadMovementHomeSetVehicleIndex(this.selectedVehicleIndex);

  @override
  String toString() {
    return 'UiRoadMovementHomeSetVehicleIndex{selectedVehicleIndex: $selectedVehicleIndex}';
  }
}

class UiRoadMovementHomeSetVehicleDriverIndex {
  final int selectedVehicleDriverIndex;

  UiRoadMovementHomeSetVehicleDriverIndex(this.selectedVehicleDriverIndex);

  @override
  String toString() {
    return 'UiRoadMovementHomeSetVehicleDriverIndex{selectedVehicleDriverIndex: $selectedVehicleDriverIndex}';
  }
}

class UiRoadMovementHomeSetVehicleOperatorIndex {
  final int selectedVehicleOperatorIndex;

  UiRoadMovementHomeSetVehicleOperatorIndex(this.selectedVehicleOperatorIndex);

  @override
  String toString() {
    return 'UiRoadMovementHomeSetVehicleOperatorIndex{selectedVehicleOperatorIndex: $selectedVehicleOperatorIndex}';
  }
}

class SetUiRoadMovementSearch {
  final UiRoadMovementSearch uiRoadMovementSearch;

  SetUiRoadMovementSearch(this.uiRoadMovementSearch);

  @override
  String toString() {
    return 'SetUiRoadMovementSearch{uiRoadMovementSearch: $uiRoadMovementSearch}';
  }
}

class UiRoadMovementSearchSetWaybill {
  final String waybill;

  UiRoadMovementSearchSetWaybill(this.waybill);

  @override
  String toString() {
    return 'UiRoadMovementSearchSetWaybill{waybill: $waybill}';
  }
}

class UiRoadMovementSearchSetLegNumber {
  final String legNumber;

  UiRoadMovementSearchSetLegNumber(this.legNumber);

  @override
  String toString() {
    return 'UiRoadMovementSearchSetLegNumber{legNumber: $legNumber}';
  }
}

class UiRoadMovementSearchSetEta {
  final String? eta;

  UiRoadMovementSearchSetEta(this.eta);

  @override
  String toString() {
    return 'UiRoadMovementSearchSetEta{legNumber: $eta}';
  }
}
