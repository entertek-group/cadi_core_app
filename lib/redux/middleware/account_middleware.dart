import 'package:cadi_core_app/factory/account_factory.dart';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/models/core_api_response.dart';
import 'package:cadi_core_app/redux/models/list_converters.dart';
import 'package:cadi_core_app/redux/state/account_state.dart';
import 'package:collection/src/iterable_extensions.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:cadi_core_app/services/rest_service.dart';
import 'dart:convert';

//helpers
import 'package:cadi_core_app/helpers/helpers.dart';

List<Middleware<AppState>> createAccountMiddleware() {
  final getAccounts = _getAccountsMiddleware();
  final getAccountPortDepots = _getAccountPortDepots();
  final setSelectedAccount = _setSelectedAccount();
  final getAccountContainerTypes = _getAccountContainerTypes();
  final getAccountCarriers = _getAccountCarriers();
  final getAccountVessels = _getAccountVessels();
  final getAccountCranes = _getAccountCranes();
  final getAccountBerths = _getAccountBerths();
  final getAccountDocumentTypes = _getAccountDocumentTypes();
  final getAccountVehicleDrivers = _getAccountVehicleDrivers();
  final getAccountVehicleOperators = _getAccountVehicleOperators();
  final getAccountVehicles = _getAccountVehicles();

  return [
    TypedMiddleware<AppState, GetAccounts>(getAccounts).call,
    TypedMiddleware<AppState, GetAccountPortDepots>(getAccountPortDepots).call,
    TypedMiddleware<AppState, SetSelectedAccount>(setSelectedAccount).call,
    TypedMiddleware<AppState, GetAccountContainerTypes>(
            getAccountContainerTypes)
        .call,
    TypedMiddleware<AppState, GetAccountCarriers>(getAccountCarriers).call,
    TypedMiddleware<AppState, GetAccountVessels>(getAccountVessels).call,
    TypedMiddleware<AppState, GetAccountCranes>(getAccountCranes).call,
    TypedMiddleware<AppState, GetAccountBerths>(getAccountBerths).call,
    TypedMiddleware<AppState, GetAccountVehicles>(getAccountVehicles).call,
    TypedMiddleware<AppState, GetAccountDocumentTypes>(getAccountDocumentTypes)
        .call,
    TypedMiddleware<AppState, GetAccountVehicleDrivers>(
            getAccountVehicleDrivers)
        .call,
    TypedMiddleware<AppState, GetAccountVehicleOperators>(
            getAccountVehicleOperators)
        .call,
  ];
}

Middleware<AppState> _getAccountsMiddleware() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);
    if (action is GetAccounts) {
      restService
          .get(standardRoute("/v1/account_users/my_accounts"),
              headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;

        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 200) {
            final accounts = AccountList.fromJson(coreResponse.result);
            final account = accounts.accounts.first;
            store.dispatch(GetAccountsSuccess(accounts, account));
          } else {
            store.dispatch(GetAccountsError(coreResponse.message));
          }
          action.completer.complete(null);
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else {
            store
                .dispatch(GetAccountsError("Could not retrieve account users"));
          }
          action.completer.complete(null);
        }
      }).catchError((dynamic error) {
        store.dispatch(GetAccountsError(error.toString()));
        action.completer.complete(null);
      });
    }
    next(action);
  };
}

Middleware<AppState> _getAccountPortDepots() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);
    if (action is GetAccountPortDepots) {
      restService
          .get(standardRoute("/v1/app/account_locations"),
              headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;

        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 200) {
            List<AccountPortDepot> accountPortDepots =
                converterAccountPortDepots(
                    coreResponse.result as List<dynamic>);

            List<AccountPort> accountPorts = AccountFactory()
                .convertAccountPortDepotsToAccountPorts(accountPortDepots);

            store.dispatch(
                GetAccountPortDepotsSuccess(accountPortDepots, accountPorts));
          } else {
            store.dispatch(GetAccountPortDepotsError(coreResponse.message));
          }
          action.completer.complete(null);
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else {
            store.dispatch(GetAccountPortDepotsError(
                "Could not retrieve account port depots"));
          }
          action.completer.complete(null);
        }
      }).catchError((dynamic error) {
        store.dispatch(GetAccountPortDepotsError(error.toString()));
        print("$error");
        action.completer.complete(null);
      });
    }
    next(action);
  };
}

Middleware<AppState> _setSelectedAccount() {
  return (Store store, action, NextDispatcher next) async {
    AccountState accountState = store.state.accountState;
    if (action is SetSelectedAccount) {
      List<AccountPortDepot> accountPortDepots = accountState.accountPortDepots;

      List<AccountPortDepot> filteredAccountPortDepots = accountPortDepots
          .where((element) => element.accountId == action.account.id)
          .toList();

      List<AccountCarrier> filteredAccountCarriers = store
          .state.accountState.accountCarriers
          .where((element) => element.accountId == action.account.id)
          .toList();

      List<AccountVessel> filteredAccountVessels = store
          .state.accountState.accountVessels
          .where((element) => element.accountId == action.account.id)
          .toList();

      List<AccountCrane> filteredAccountCranes = store
          .state.accountState.accountCranes
          .where((element) => element.accountId == action.account.id)
          .toList();

      List<AccountBerth> filteredAccountBerths = store
          .state.accountState.accountBerths
          .where((element) => element.accountId == action.account.id)
          .toList();

      List<AccountVehicleDriver> filteredAccountVehicleDrivers = store
          .state.accountState.accountVehicleDrivers
          .where((element) => element.accountId == action.account.id)
          .toList();

      List<AccountVehicleDriver> filteredAccountVehicleOperators = store
          .state.accountState.accountVehicleOperators
          .where((element) => element.accountId == action.account.id)
          .toList();

      List<AccountVehicle> filteredAccountVehicles = store
          .state.accountState.accountVehicles
          .where((element) => element.accountId == action.account.id)
          .toList();

      List<AccountDocumentType> filteredAccountDocumentTypes = store
          .state.accountState.accountDocumentTypes
          .where((element) => element.accountId == action.account.id)
          .toList();

      List<AccountContainerType> filteredAccountContainerTypes = store
          .state.accountState.accountContainerTypes
          .where((element) => element.accountId == action.account.id)
          .toList();

      List<AccountPort> filteredAccountPorts = accountState.accountPorts
          .where((element) => element.accountId == action.account.id)
          .toList();

      Account? activeAccount = accountState.accountList?.accounts
          .firstWhere((element) => element.id == action.account.id);

      //#Cargo movements take priority.
      if (activeAccount != null) {
        if (activeAccount.activeServices.firstWhereOrNull(
                (element) => element == CadiAppServices.portMovements) !=
            null) {
          store.dispatch(SetActiveService(CadiAppServices.portMovements));
        } else if (activeAccount.activeServices.firstWhereOrNull(
                (element) => element == CadiAppServices.portEvents) !=
            null) {
          store.dispatch(SetActiveService(CadiAppServices.portEvents));
        } else if (activeAccount.activeServices.firstWhereOrNull(
                (element) => element == CadiAppServices.roadMovements) !=
            null) {
          store.dispatch(SetActiveService(CadiAppServices.roadMovements));
        } else if (activeAccount.activeServices.firstWhereOrNull(
                (element) => element == CadiAppServices.roadEvents) !=
            null) {
          store.dispatch(SetActiveService(CadiAppServices.roadEvents));
        } else {
          store.dispatch(SetActiveService(CadiAppServices.unavailable));
        }
      } else {
        store.dispatch(SetActiveService(CadiAppServices.unavailable));
      }
      store.dispatch(UiCargoHomeReset());
      //set new account
      store.dispatch(FilterByAccountSelection(
          action.account,
          filteredAccountPortDepots,
          filteredAccountPorts,
          filteredAccountContainerTypes,
          filteredAccountCarriers,
          filteredAccountVessels,
          filteredAccountVehicleDrivers,
          filteredAccountVehicleOperators,
          filteredAccountDocumentTypes,
          filteredAccountCranes,
          filteredAccountVehicles,
          filteredAccountBerths));
    }
    next(action);
  };
}

Middleware<AppState> _getAccountContainerTypes() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);

    if (action is GetAccountContainerTypes) {
      restService
          .get(standardRoute("/v1/app/account_container_types"),
              headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 200) {
            List<AccountContainerType> containerTypes =
                converterAccountContainerTypes(coreResponse.result);
            store.dispatch(GetAccountContainerTypesSuccess(containerTypes));
            action.completer.complete(ApiResponse(success: true, result: ""));
          } else {
            store.dispatch(GetAccountContainerTypesError(coreResponse.message));
            action.completer.complete(
                ApiResponse(success: false, result: "No records found"));
          }
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else if (statusCode == 404) {
            store.dispatch(GetAccountContainerTypesError("No records found"));
          } else {
            store.dispatch(GetAccountContainerTypesError(
                "No record found matching search criteria"));
          }
          action.completer.complete(
              ApiResponse(success: false, result: "No records found"));
        }
      }).catchError((dynamic error) {
        store.dispatch(GetAccountContainerTypesError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}

Middleware<AppState> _getAccountCarriers() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);

    if (action is GetAccountCarriers) {
      restService
          .get(standardRoute("/v1/app/account_carriers"),
              headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 200) {
            List<AccountCarrier> carriers =
                converterAccountCarriers(coreResponse.result);
            store.dispatch(GetAccountCarriersSuccess(carriers));

            action.completer.complete(ApiResponse(success: true, result: ""));
          } else {
            store.dispatch(GetAccountCarriersError(coreResponse.message));
            action.completer.complete(
                ApiResponse(success: false, result: "No records found"));
          }
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else if (statusCode == 404) {
            store.dispatch(GetAccountCarriersError("No records found"));
          } else {
            store.dispatch(GetAccountCarriersError(
                "No record found matching search criteria"));
          }
          action.completer.complete(
              ApiResponse(success: false, result: "No records found"));
        }
      }).catchError((dynamic error) {
        store.dispatch(GetAccountCarriersError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}

Middleware<AppState> _getAccountVessels() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);

    if (action is GetAccountVessels) {
      restService
          .get(standardRoute("/v1/app/account_vessels"), headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 200) {
            List<AccountVessel> vessels =
                converterAccountVessels(coreResponse.result);
            store.dispatch(GetAccountVesselsSuccess(vessels));

            action.completer.complete(ApiResponse(success: true, result: ""));
          } else {
            store.dispatch(GetAccountVesselsError(coreResponse.message));
            action.completer.complete(
                ApiResponse(success: false, result: "No records found"));
          }
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else if (statusCode == 404) {
            store.dispatch(GetAccountVesselsError("No records found"));
          } else {
            store.dispatch(GetAccountVesselsError(
                "No record found matching search criteria"));
          }
          action.completer.complete(
              ApiResponse(success: false, result: "No records found"));
        }
      }).catchError((dynamic error) {
        store.dispatch(GetAccountVesselsError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}

Middleware<AppState> _getAccountVehicles() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);

    if (action is GetAccountVehicles) {
      restService
          .get(standardRoute("/v1/app/account_vehicles"),
              headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 200) {
            List<AccountVehicle> vehicles =
                converterAccountVehicles(coreResponse.result);
            store.dispatch(GetAccountVehiclesSuccess(vehicles));

            action.completer.complete(ApiResponse(success: true, result: ""));
          } else {
            store.dispatch(GetAccountVehiclesError(coreResponse.message));
            action.completer.complete(
                ApiResponse(success: false, result: "No records found"));
          }
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else if (statusCode == 404) {
            store.dispatch(GetAccountVehiclesError("No records found"));
          } else {
            store.dispatch(GetAccountVehiclesError(
                "No record found matching search criteria"));
          }
          action.completer.complete(
              ApiResponse(success: false, result: "No records found"));
        }
      }).catchError((dynamic error) {
        store.dispatch(GetAccountVehiclesError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}

Middleware<AppState> _getAccountCranes() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);

    if (action is GetAccountCranes) {
      restService
          .get(standardRoute("/v1/app/account_cranes"), headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 200) {
            List<AccountCrane> cranes =
                converterAccountCranes(coreResponse.result);
            store.dispatch(GetAccountCranesSuccess(cranes));

            action.completer.complete(ApiResponse(success: true, result: ""));
          } else {
            store.dispatch(GetAccountCranesError(coreResponse.message));
            action.completer.complete(
                ApiResponse(success: false, result: "No records found"));
          }
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else if (statusCode == 404) {
            store.dispatch(GetAccountCranesError("No records found"));
          } else {
            store.dispatch(GetAccountCranesError(
                "No record found matching search criteria"));
          }
          action.completer.complete(
              ApiResponse(success: false, result: "No records found"));
        }
      }).catchError((dynamic error) {
        store.dispatch(GetAccountCranesError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}

Middleware<AppState> _getAccountBerths() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);

    if (action is GetAccountBerths) {
      restService
          .get(standardRoute("/v1/app/account_berths"), headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 200) {
            List<AccountBerth> berths =
                converterAccountBerths(coreResponse.result);
            store.dispatch(GetAccountBerthsSuccess(berths));

            action.completer.complete(ApiResponse(success: true, result: ""));
          } else {
            store.dispatch(GetAccountBerthsError(coreResponse.message));
            action.completer.complete(
                ApiResponse(success: false, result: "No records found"));
          }
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else if (statusCode == 404) {
            store.dispatch(GetAccountBerthsError("No records found"));
          } else {
            store.dispatch(GetAccountBerthsError(
                "No record found matching search criteria"));
          }
          action.completer.complete(
              ApiResponse(success: false, result: "No records found"));
        }
      }).catchError((dynamic error) {
        store.dispatch(GetAccountBerthsError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}

Middleware<AppState> _getAccountVehicleDrivers() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);

    if (action is GetAccountVehicleDrivers) {
      restService
          .get(standardRoute("/v1/app/account_vehicle_drivers"),
              headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 200) {
            List<AccountVehicleDriver> drivers =
                converterAccountVehicleDrivers(coreResponse.result);
            store.dispatch(GetAccountVehicleDriversSuccess(drivers));

            action.completer.complete(ApiResponse(success: true, result: ""));
          } else {
            store.dispatch(GetAccountVehicleDriversError(coreResponse.message));
            action.completer.complete(
                ApiResponse(success: false, result: "No records found"));
          }
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else if (statusCode == 404) {
            store.dispatch(GetAccountVehicleDriversError("No records found"));
          } else {
            store.dispatch(GetAccountVehicleDriversError(
                "No record found matching search criteria"));
          }
          action.completer.complete(
              ApiResponse(success: false, result: "No records found"));
        }
      }).catchError((dynamic error) {
        store.dispatch(GetAccountVehicleDriversError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}

Middleware<AppState> _getAccountVehicleOperators() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);

    if (action is GetAccountVehicleOperators) {
      restService
          .get(standardRoute("/v1/app/account_vehicle_operators"),
              headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 200) {
            List<AccountVehicleDriver> operators =
                converterAccountVehicleDrivers(coreResponse.result);
            store.dispatch(GetAccountVehicleOperatorsSuccess(operators));

            action.completer.complete(ApiResponse(success: true, result: ""));
          } else {
            store.dispatch(
                GetAccountVehicleOperatorsError(coreResponse.message));
            action.completer.complete(
                ApiResponse(success: false, result: "No records found"));
          }
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else if (statusCode == 404) {
            store.dispatch(GetAccountVehicleOperatorsError("No records found"));
          } else {
            store.dispatch(GetAccountVehicleOperatorsError(
                "No record found matching search criteria"));
          }
          action.completer.complete(
              ApiResponse(success: false, result: "No records found"));
        }
      }).catchError((dynamic error) {
        store.dispatch(GetAccountVehicleOperatorsError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}

Middleware<AppState> _getAccountDocumentTypes() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);

    if (action is GetAccountDocumentTypes) {
      restService
          .get(standardRoute("/v1/app/account_document_types"),
              headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 200) {
            List<AccountDocumentType> documentTypes =
                converterAccountDocumentTypes(coreResponse.result);
            store.dispatch(GetAccountDocumentTypesSuccess(documentTypes));

            action.completer.complete(ApiResponse(success: true, result: ""));
          } else {
            store.dispatch(GetAccountDocumentTypesError(coreResponse.message));
            action.completer.complete(
                ApiResponse(success: false, result: "No records found"));
          }
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else if (statusCode == 404) {
            store.dispatch(GetAccountDocumentTypesError("No records found"));
          } else {
            store.dispatch(GetAccountDocumentTypesError(
                "No record found matching search criteria"));
          }
          action.completer.complete(
              ApiResponse(success: false, result: "No records found"));
        }
      }).catchError((dynamic error) {
        store.dispatch(GetAccountDocumentTypesError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}
