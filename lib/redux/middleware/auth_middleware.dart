import 'package:cadi_core_app/redux/models/core_api_response.dart';
import 'package:cadi_core_app/ui/ui.dart';
import 'package:redux/redux.dart';

import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';

import 'package:cadi_core_app/services/rest_service.dart';
import 'dart:convert';

import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
//helpers
import 'package:cadi_core_app/helpers/helpers.dart';

List<Middleware<AppState>> createAuthMiddleware() {
  final stateRequest = _createStateRequest();
  final logIn = _createLogInMiddleware();
  final getMyUser = _getMyUserMiddleware();
  final updateUser = _updateUserMiddleware();
  final updateUserPassword = _updateUserPasswordMiddleware();
  final forgetPassword = _forgetPasswordMiddleware();
  final updateUserAvatar = _updateUserAvatarMiddleware();

  return [
    TypedMiddleware<AppState, LoadStateRequest>(stateRequest).call,
    TypedMiddleware<AppState, Authenticate>(logIn).call,
    TypedMiddleware<AppState, GetMyUser>(getMyUser).call,
    TypedMiddleware<AppState, UpdateUser>(updateUser).call,
    TypedMiddleware<AppState, UpdateUserPassword>(updateUserPassword).call,
    TypedMiddleware<AppState, ForgetPassword>(forgetPassword).call,
    TypedMiddleware<AppState, UpdateUserAvatar>(updateUserAvatar).call,
  ];
}

Middleware<AppState> _createStateRequest() {
  return (Store store, action, NextDispatcher next) async {
    final authenticated = store.state.authState.isAuthenticated;
    if (authenticated) {
      store.dispatch(NavigateToAction.replace(HomeScreen.route));
    } else {
      if (store.state.welcomeShown) {
        store.dispatch(NavigateToAction.replace(LoginScreen.route));
      } else {
        store.dispatch(NavigateToAction.replace(WelcomeScreen.route));
      }
    }
    next(action);
  };
}

Middleware<AppState> _createLogInMiddleware() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    if (action is Authenticate) {
      restService
          .post(standardRoute("/v1/authentication/login"),
              headers: jsonHeaders,
              body: json
                  .encode({"email": action.email, "password": action.password}))
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        //Add Tokens
        if (statusCode == 200) {
          Token token = Token.fromJson(body['accessToken']);
          store.dispatch(AuthenticateSuccess(token, action.email));
          action.completer.complete(null);
        } else {
          store.dispatch(
              AuthenticateError("Incorrect email / password. Try again."));
          action.completer.complete(null);
        }
      }).catchError((dynamic error) {
        store.dispatch(
            AuthenticateError(error.toString())); //_error_helper.bad_connection
        action.completer.complete(null);
      });
    }
    next(action);
  };
}

Middleware<AppState> _updateUserMiddleware() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final existingUser = store.state.authState.user;
    final submitHeaders = authHeaders(existingToken);
    if (action is UpdateUser) {
      restService
          .put(standardRoute("/v1/authentication/user"),
              headers: submitHeaders,
              body: json.encode({
                "id": existingUser.id,
                "email": action.email,
                "fullName": '${action.firstName} ${action.lastName}',
                "firstName": action.firstName,
                "lastName": action.lastName,
                "username": existingUser.username,
                "timezone": existingUser.timezone,
                "mobileNumber": action.mobileNumber,
                "roles": [],
              }))
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;

        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 202) {
            final user = User.fromJson(coreResponse.result);
            store.dispatch(UpdateUserSuccess(user));
          } else {
            store.dispatch(UpdateUserError(coreResponse.message));
          }
          action.completer.complete(null);
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else {
            store.dispatch(UpdateUserError("Could not save user"));
          }
          action.completer.complete(null);
        }
      }).catchError((dynamic error) {
        store.dispatch(UpdateUserError(error.toString()));
        action.completer.complete(null);
      });
    }
    next(action);
  };
}

Middleware<AppState> _updateUserAvatarMiddleware() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final existingUser = store.state.authState.user;
    final submitHeaders = authHeaders(existingToken);
    if (action is UpdateUserAvatar) {
      restService
          .put(standardRoute("/v1/authentication/user"),
              headers: submitHeaders,
              body: json.encode({
                "id": existingUser.id,
                "email": existingUser.email,
                "fullName": existingUser.name,
                "firstName": existingUser.firstName,
                "lastName": existingUser.lastName,
                "username": existingUser.username,
                "timezone": existingUser.timezone,
                "mobileNumber": existingUser.mobileNumber,
                "roles": [],
                "imageBlob": {
                  "name": action.imageName,
                  "base64Content": action.image,
                }
              }))
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;

        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 202) {
            final user = User.fromJson(coreResponse.result);
            store.dispatch(UpdateUserAvatarSuccess(user));
          } else {
            store.dispatch(UpdateUserAvatarError(coreResponse.message));
          }
          action.completer.complete(null);
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else {
            store.dispatch(UpdateUserAvatarError("Could not save user"));
          }
          action.completer.complete(
              ApiResponse(success: false, result: "Could not update user"));
        }
      }).catchError((dynamic error) {
        store.dispatch(UpdateUserAvatarError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}

Middleware<AppState> _updateUserPasswordMiddleware() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final existingUser = store.state.authState.user;
    final submitHeaders = authHeaders(existingToken);
    if (action is UpdateUserPassword) {
      restService
          .put(standardRoute("/v1/authentication/password"),
              headers: submitHeaders,
              body: json.encode({
                "userId": existingUser.id,
                "currentPassword": action.currentPassword,
                "newPassword": action.password,
                "confirmPassword": action.passwordConfirmation
              }))
          .then((dynamic res) {
        final body = decoder.convert(res.body);

        final statusCode = res.statusCode;

        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 202) {
            store.dispatch(UpdateUserPasswordSuccess(existingUser));
          } else {
            store.dispatch(UpdateUserPasswordError(coreResponse.message));
          }
          action.completer.complete(null);
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else {
            store.dispatch(UpdateUserPasswordError(
                "Could not update password, try again."));
          }
          action.completer.complete(null);
        }
      }).catchError((dynamic error) {
        store.dispatch(UpdateUserPasswordError(error.toString()));
        action.completer.complete(null);
      });
    }
    next(action);
  };
}

Middleware<AppState> _forgetPasswordMiddleware() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    if (action is ForgetPassword) {
      restService
          .get(
              standardRoute(
                  "/v1/authentication/send_reset_link/${action.email}"),
              headers: jsonHeaders)
          .then((dynamic res) {
        final statusCode = res.statusCode;
        final body = decoder.convert(res.body);

        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 200) {
            action.completer.complete(ApiResponse(
                success: true,
                result: "Password reset email sent. Please check your email."));
            store.dispatch(ForgetPasswordSuccess());
          } else {
            store.dispatch(ForgetPasswordError(coreResponse.message));
            action.completer.complete(ApiResponse(
                success: false, result: "Could not reset password"));
          }
        } else {
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else {
            store.dispatch(
                ForgetPasswordError("Could not update password, try again."));
          }
          action.completer.complete(
              ApiResponse(success: false, result: "Could not reset password"));
        }
      }).catchError((dynamic error) {
        store.dispatch(ForgetPasswordError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}

Middleware<AppState> _getMyUserMiddleware() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);
    if (action is GetMyUser) {
      restService
          .get(standardRoute("/v1/app/me"), headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;

        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);

          if (coreResponse.statusCode == 200) {
            final user = User.fromJson(coreResponse.result);
            store.dispatch(GetMyUserSuccess(user));
          } else {
            store.dispatch(GetMyUserError(coreResponse.message));
          }
          action.completer.complete(null);
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else {
            store.dispatch(GetMyUserError("Could not retrieve user"));
          }
          action.completer.complete(null);
        }
      }).catchError((dynamic error) {
        store.dispatch(GetMyUserError(error.toString()));
        action.completer.complete(null);
      });
    }
    next(action);
  };
}

// Middleware<AppState> _signOutUserMiddleware() {
//   return (Store store, action, NextDispatcher next) async {
//     if (action is AddBolCargoMovement) {
//       // var token = Token.initial();
//       // var user = User.initial();

//       // // print(cargoMovement.toString());
//       // store.dispatch(SignOutSuccess(token, user));
//       store.dispatch(CompleteSignOut());
//     }
//     next(action);
//   };
// }
