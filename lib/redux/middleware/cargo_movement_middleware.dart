import 'dart:io';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/models/core_api_response.dart';
import 'package:redux/redux.dart';

import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:cadi_core_app/services/rest_service.dart';
import 'dart:convert';
//helpers
import 'package:cadi_core_app/helpers/helpers.dart';

List<Middleware<AppState>> createCargoMovementMiddleware() {
  final queueCargoMovement = _queueCargoMovement();
  final setCargoHomeVariables = _setCargoHomeVariables();
  final setCargoContainerVariables = _setCargoContainerVariables();
  final setCargoStatusVariables = _setCargoStatusVariables();
  final setAccountPortDepot = _setAccountPortDepot();
  final setCargoTruckDriverVariables = _setCargoTruckDriverVariables();
  final updateCargoContainerNumber = _updateCargoContainerNumber();
  final updateCargoContainerWeight = _updateCargoContainerWeight();
  final updateCargoStowagePosition = _updateCargoStowagePosition();
  final updateCargoPackWeight = _updateCargoPackWeight();
  final updateCargoContainerWeightUnit = _updateCargoContainerWeightUnit();
  final updateCargoPackWeightUnit = _updateCargoPackWeightUnit();

  final updateCargoSealNumber1 = _updateCargoSealNumber1();
  final updateCargoMovementAt = _updateCargoMovementAt();
  final writeCargoMovement = _writeCargoMovement();
  final setCargoFreightVariables = _setCargoFreighteVariables();
  final uploadCargoMovementImage = _uploadCargoMovementImage();
  final setCargoMovementEirVariables = _setCargoMovementEirVariables();

  return [
    TypedMiddleware<AppState, QueueCargoMovement>(queueCargoMovement).call,
    TypedMiddleware<AppState, SetCargoHomeVariables>(setCargoHomeVariables)
        .call,
    TypedMiddleware<AppState, SetCargoContainerVariables>(
            setCargoContainerVariables)
        .call,
    TypedMiddleware<AppState, SetCargoFreightVariables>(
            setCargoFreightVariables)
        .call,
    TypedMiddleware<AppState, SetCargoTruckDriverVariables>(
            setCargoTruckDriverVariables)
        .call,
    TypedMiddleware<AppState, SetCargoStatusVariables>(setCargoStatusVariables)
        .call,
    TypedMiddleware<AppState, UpdateCargoContainerNumber>(
            updateCargoContainerNumber)
        .call,
    TypedMiddleware<AppState, UpdateCargoContainerWeight>(
            updateCargoContainerWeight)
        .call,
    TypedMiddleware<AppState, UpdateCargoStowagePosition>(
            updateCargoStowagePosition)
        .call,
    TypedMiddleware<AppState, UpdateCargoPackWeight>(updateCargoPackWeight)
        .call,
    TypedMiddleware<AppState, UpdateCargoContainerWeightUnit>(
            updateCargoContainerWeightUnit)
        .call,
    TypedMiddleware<AppState, UpdateCargoPackWeightUnit>(
            updateCargoPackWeightUnit)
        .call,
    TypedMiddleware<AppState, UpdateCargoSealNumber1>(updateCargoSealNumber1)
        .call,
    TypedMiddleware<AppState, UpdateCargoMovementAt>(updateCargoMovementAt)
        .call,
    TypedMiddleware<AppState, SetAccountPortDepot>(setAccountPortDepot).call,
    TypedMiddleware<AppState, WriteCargoMovement>(writeCargoMovement).call,
    TypedMiddleware<AppState, UploadCargoMovementImage>(
            uploadCargoMovementImage)
        .call,
    TypedMiddleware<AppState, SetCargoMovementEirVariables>(
            setCargoMovementEirVariables)
        .call,
  ];
}

Middleware<AppState> _writeCargoMovement() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    if (action is WriteCargoMovement) {
      final existingToken = store.state.authState.token;
      final selectedAccountId = store.state.accountState.account != null
          ? store.state.accountState.account.id
          : 0;
      final submitHeaders = authHeaders(existingToken);

      var cargoMovement = action.cargoMovementCreate.copyWith(
          applicationUserId: store.state.authState.user.id,
          accountId: selectedAccountId);

      // Remove jobContainerId and jobId if they are 0
      if (cargoMovement.sailingJobContainerId == 0) {
        cargoMovement = cargoMovement.copyWith(sailingJobContainerId: null);
      }
      if (cargoMovement.sailingJobId == 0) {
        cargoMovement = cargoMovement.copyWith(sailingJobId: null);
      }

      restService
          .post(standardRoute("/v1/$selectedAccountId/port_movements"),
              headers: submitHeaders, body: json.encode(cargoMovement))
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 201) {
            if (action.uniqueId != "") {
              List<CargoCreateQueue> cargoQueue =
                  store.state.cargoMovementState.cargoCreateQueue;
              cargoQueue.removeWhere((x) => x.uniqueId == action.uniqueId);
              store.dispatch(UpdateQueueCargoMovement(cargoQueue));
              action.completer.complete(ApiResponse(success: true, result: ""));
            }
          } else {
            ApiResponse apiResponse =
                ApiResponse(success: false, result: coreResponse.message);
            action.completer.complete(apiResponse);
          }
        } else {
          ApiResponse apiResponse = ApiResponse(
              success: false,
              result: "Could not create cargo movement $statusCode");
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          }
          action.completer.complete(apiResponse);
        }
      }).catchError((dynamic error) {
        ApiResponse apiResponse =
            ApiResponse(success: false, result: error.toString());
        action.completer.complete(apiResponse);
      });
    }
    next(action);
  };
}

Middleware<AppState> _queueCargoMovement() {
  return (Store store, action, NextDispatcher next) async {
    final now = DateTime.now();
    if (action is QueueCargoMovement) {
      List<CargoCreateQueue> cargoQueue =
          store.state.cargoMovementState.cargoCreateQueue ??
              <CargoCreateQueue>[];

      FileStore tempStorage = store.state.storageState.tempStorage;

      CargoMovementCreate updatedCargoMovement =
          action.cargoMovementCreate.movementAt == ""
              ? action.cargoMovementCreate
                  .copyWith(movementAt: getDateTimeWithOffset(DateTime.now()))
              : action.cargoMovementCreate;

      var newCargoQueue = CargoCreateQueue(
          uniqueId: now.microsecondsSinceEpoch.toString(),
          cargoMovement: updatedCargoMovement,
          complete: false,
          createdAt: DateTime.now().toString(),
          error: "");

      cargoQueue.add(newCargoQueue);
      store.dispatch(UpdateQueueCargoMovement(cargoQueue));
      store.dispatch(FlagQueued());
      if (tempStorage.filePath != "" &&
          tempStorage.storageType == StorageType.cargoMovement) {
        store.dispatch(MoveTempStorageToFileStores(newCargoQueue.uniqueId));
      }
    }
    next(action);
  };
}

Middleware<AppState> _setCargoHomeVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetCargoHomeVariables) {
      CargoMovementCreate cargoMovementCreate =
          store.state.cargoMovementState.cargoMovementCreate.copyWith(
              accountPortLocationId: action.accountPortDepot,
              carrierId: action.carrierId,
              role: action.role,
              portId: action.portId);
      store.dispatch(UpdateCargoMovementCreate(cargoMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setCargoFreighteVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetCargoFreightVariables) {
      CargoMovementCreate cargoMovementCreate =
          store.state.cargoMovementState.cargoMovementCreate.copyWith(
              vesselName: action.vesselName,
              voyageNumber: action.voyageNumber,
              accountPortCraneId: action.craneId);
      store.dispatch(UpdateCargoMovementCreate(cargoMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setCargoStatusVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetCargoStatusVariables) {
      CargoMovementCreate cargoMovementCreate =
          store.state.cargoMovementState.cargoMovementCreate.copyWith(
              state: action.status,
              comment: action.inputComment,
              images: action.attachedImages,
              restowLocation: action.inputRestow,
              eir: action.eir);
      store.dispatch(UpdateCargoMovementCreate(cargoMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updateCargoContainerNumber() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdateCargoContainerNumber) {
      CargoMovementCreate cargoMovementCreate =
          store.state.cargoMovementState.cargoMovementCreate.copyWith(
              containerData: store
                  .state.cargoMovementState.cargoMovementCreate.containerData
                  .copyWith(containerNumber: action.containerNumber));
      store.dispatch(UpdateCargoMovementCreate(cargoMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updateCargoContainerWeight() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdateCargoContainerWeight) {
      CargoMovementCreate cargoMovementCreate =
          store.state.cargoMovementState.cargoMovementCreate.copyWith(
              containerData: store
                  .state.cargoMovementState.cargoMovementCreate.containerData
                  .copyWith(containerWeight: action.containerWeight));
      store.dispatch(UpdateCargoMovementCreate(cargoMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updateCargoStowagePosition() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdateCargoStowagePosition) {
      CargoMovementCreate cargoMovementCreate = store
          .state.cargoMovementState.cargoMovementCreate
          .copyWith(stowagePosition: action.stowagePosition);
      store.dispatch(UpdateCargoMovementCreate(cargoMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updateCargoPackWeight() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdateCargoPackWeight) {
      CargoMovementCreate cargoMovementCreate =
          store.state.cargoMovementState.cargoMovementCreate.copyWith(
              packData: store
                  .state.cargoMovementState.cargoMovementCreate.packData
                  .copyWith(packWeight: action.packWeight));

      store.dispatch(UpdateCargoMovementCreate(cargoMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updateCargoContainerWeightUnit() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdateCargoContainerWeightUnit) {
      CargoMovementCreate cargoMovementCreate =
          store.state.cargoMovementState.cargoMovementCreate.copyWith(
              containerData: store
                  .state.cargoMovementState.cargoMovementCreate.containerData
                  .copyWith(containerWeightUnit: action.containerWeightUnit));
      store.dispatch(UpdateCargoMovementCreate(cargoMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updateCargoPackWeightUnit() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdateCargoPackWeightUnit) {
      CargoMovementCreate cargoMovementCreate =
          store.state.cargoMovementState.cargoMovementCreate.copyWith(
              packData: store
                  .state.cargoMovementState.cargoMovementCreate.packData
                  .copyWith(packWeightUnit: action.packWeightUnit));
      store.dispatch(UpdateCargoMovementCreate(cargoMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updateCargoSealNumber1() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdateCargoSealNumber1) {
      CargoMovementCreate cargoMovementCreate =
          store.state.cargoMovementState.cargoMovementCreate.copyWith(
              containerData: store
                  .state.cargoMovementState.cargoMovementCreate.containerData
                  .copyWith(sealNumber1: action.sealNumber1));
      store.dispatch(UpdateCargoMovementCreate(cargoMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updateCargoMovementAt() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdateCargoMovementAt) {
      CargoMovementCreate cargoMovementCreate = store
          .state.cargoMovementState.cargoMovementCreate
          .copyWith(movementAt: action.movementAt);
      store.dispatch(UpdateCargoMovementCreate(cargoMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setCargoMovementEirVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetCargoMovementEirVariables) {
      CargoMovementCreate cargoMovementCreate =
          store.state.cargoMovementState.cargoMovementCreate.copyWith(
              signatoryFullName: action.signatoryFullName,
              signature: action.signature,
              consigneeOrConsignorName: action.consigneeOrConsignorName,
              deliveryDocketNumber: action.deliveryDocketNumber,
              portOfLoadOrDischarge: action.portOfLoadOrDischarge);
      store.dispatch(UpdateCargoMovementCreate(cargoMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setAccountPortDepot() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetAccountPortDepot) {
      CargoMovementCreate cargoMovementCreate = store
          .state.cargoMovementState.cargoMovementCreate
          .copyWith(accountPortLocationId: action.accountPortDepotId);
      store.dispatch(UpdateCargoMovementCreate(cargoMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setCargoTruckDriverVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetCargoTruckDriverVariables) {
      CargoMovementCreate cargoMovementCreate =
          store.state.cargoMovementState.cargoMovementCreate.copyWith(
              truckRegistration: action.truckRegistration,
              truckDriverId: action.truckDriverId,
              truckCompany: action.truckCompany);
      store.dispatch(UpdateCargoMovementCreate(cargoMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setCargoContainerVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetCargoContainerVariables) {
      CargoMovementCreate cargoMovementCreate;

      switch (action.shipment) {
        case CadiCargoMovementShipment.container:
        case CadiCargoMovementShipment.restow:
          cargoMovementCreate =
              store.state.cargoMovementState.cargoMovementCreate.copyWith(
            shipment: action.shipment,
            stowagePosition: action.inputStowagePosition,
            containerData: store
                .state.cargoMovementState.cargoMovementCreate.containerData
                .copyWith(
              containerNumber: action.inputOne,
              sealNumber1: action.inputTwo,
              sealNumber2: action.inputThree,
              containerMode: action.selectedCargoType,
              packingMode: action.selectedContainerType,
            ),
          );
          break;
        case CadiCargoMovementShipment.breakBulk:
          cargoMovementCreate =
              store.state.cargoMovementState.cargoMovementCreate.copyWith(
                  shipment: action.shipment,
                  stowagePosition: action.inputStowagePosition,
                  packData: store
                      .state.cargoMovementState.cargoMovementCreate.packData
                      .copyWith(
                    packSequenceNumber: "${action.inputOne}_${action.inputTwo}",
                    packingMode: action.selectedJobPackType,
                  ));
          break;
        default:
          cargoMovementCreate =
              store.state.cargoMovementState.cargoMovementCreate.copyWith(
            shipment: action.shipment,
            stowagePosition: action.inputStowagePosition,
            containerData: store
                .state.cargoMovementState.cargoMovementCreate.containerData
                .copyWith(
              containerNumber: action.inputOne,
              sealNumber1: action.inputTwo,
              sealNumber2: action.inputThree,
              containerMode: action.selectedCargoType,
              packingMode: action.selectedContainerType,
            ),
          );
          break;
      }
      store.dispatch(UpdateCargoMovementCreate(cargoMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _uploadCargoMovementImage() {
  RestService restService = RestService();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);
    final selectedAccountId =
        store.state.accountState.account != null //todo here
            ? store.state.accountState.account.id
            : 0;
    if (action is UploadCargoMovementImage) {
      File image = File(action.fileStore.filePath);
      restService
          .upload(
              standardRoute(
                  "/v1/accounts/$selectedAccountId/cargo_movements/${action.fileStore.apiId}"),
              image,
              "cargo_image",
              headers: submitHeaders)
          .then((dynamic res) {
        //final body = _decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 202) {
          action.completer.complete(ApiResponse(success: true, result: ""));
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          }
          // else {
          //   // store.dispatch(UpdateUserAvatarError("Could not save user"));
          // }
          action.completer.complete(
              ApiResponse(success: false, result: "Could not upload image"));
        }
      }).catchError((dynamic error) {
        // store.dispatch(UpdateUserAvatarError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}
