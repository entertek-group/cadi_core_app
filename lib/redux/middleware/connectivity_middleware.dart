import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/services/data_processing_service.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';

List<Middleware<AppState>> createConnectivityMiddleware() {
  final setConnectivity = _setConnectivity();

  return [
    TypedMiddleware<AppState, SetConnectivity>(setConnectivity).call,
  ];
}

Middleware<AppState> _setConnectivity() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetConnectivity) {

      bool cargoMovementQueued = store.state.cargoMovementState.queued;
      bool roadMovementQueued = store.state.roadMovementState.queued;
      bool portEventQueued = store.state.portEventState.queued;
      bool portCargoUpdateQueued = store.state.portCargoUpdateState.queued;

      if (action.connectionStatus == ConnectivityConstants.mobile ||
          action.connectionStatus == ConnectivityConstants.wifi) {

        //Run all queues
        if (cargoMovementQueued) {
          processCargoMovementQueue(store);
        }


        if (roadMovementQueued) {
            processRoadMovementQueue(store);
          }

        if (portEventQueued) {
          processPortEventQueue(store);
        }

        if(portCargoUpdateQueued) {
          processPortCargoUpdateQueue(store);
        }

      }

      store.dispatch(UpdateConnectivity(action.connectionStatus, action.statusAt));
    }
    next(action);
  };
}