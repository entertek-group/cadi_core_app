import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/models/core_api_response.dart';
import 'package:redux/redux.dart';

import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:cadi_core_app/services/rest_service.dart';
import 'dart:convert';
//helpers
import 'package:cadi_core_app/helpers/helpers.dart';

List<Middleware<AppState>> createPortCargoUpdateMiddleware() {
  final queuePortCargoUpdate = _queuePortCargoUpdate();
  final setPortCargoUpdateHomeVariables = _setPortCargoUpdateHomeVariables();
  final setPortCargoUpdateContainerVariables =
      _setPortCargoUpdateContainerVariables();
  final setPortCargoUpdateStatusVariables =
      _setPortCargoUpdateStatusVariables();
  final setPortCargoUpdateAccountPortDepot =
      _setPortCargoUpdateAccountPortDepot();
  final updatePortCargoUpdateContainerNumber =
      _updatePortCargoUpdateContainerNumber();
  final updatePortCargoUpdateContainerWeight =
      _updatePortCargoUpdateContainerWeight();
  final updatePortCargoUpdatePackWeight = _updatePortCargoUpdatePackWeight();
  final updatePortCargoUpdateContainerWeightUnit =
      _updatePortCargoUpdateContainerWeightUnit();
  final updatePortCargoUpdatePackWeightUnit =
      _updatePortCargoUpdatePackWeightUnit();
  final updatePortCargoUpdateSealNumber1 = _updatePortCargoUpdateSealNumber1();
  final updatePortCargoUpdateEventAt = _updatePortCargoUpdateEventAt();
  final writePortCargoUpdate = _writePortCargoUpdate();
  final setPortCargoUpdateFreightVariables =
      _setPortCargoUpdateFreightVariables();

  return [
    TypedMiddleware<AppState, QueuePortCargoUpdate>(queuePortCargoUpdate).call,
    TypedMiddleware<AppState, SetPortCargoUpdateHomeVariables>(
            setPortCargoUpdateHomeVariables)
        .call,
    TypedMiddleware<AppState, SetPortCargoUpdateContainerVariables>(
            setPortCargoUpdateContainerVariables)
        .call,
    TypedMiddleware<AppState, SetPortCargoUpdateFreightVariables>(
            setPortCargoUpdateFreightVariables)
        .call,
    TypedMiddleware<AppState, SetPortCargoUpdateStatusVariables>(
            setPortCargoUpdateStatusVariables)
        .call,
    TypedMiddleware<AppState, UpdatePortCargoUpdateContainerNumber>(
            updatePortCargoUpdateContainerNumber)
        .call,
    TypedMiddleware<AppState, UpdatePortCargoUpdateContainerWeight>(
            updatePortCargoUpdateContainerWeight)
        .call,
    TypedMiddleware<AppState, UpdatePortCargoUpdatePackWeight>(
            updatePortCargoUpdatePackWeight)
        .call,
    TypedMiddleware<AppState, UpdatePortCargoUpdateContainerWeightUnit>(
            updatePortCargoUpdateContainerWeightUnit)
        .call,
    TypedMiddleware<AppState, UpdatePortCargoUpdatePackWeightUnit>(
            updatePortCargoUpdatePackWeightUnit)
        .call,
    TypedMiddleware<AppState, UpdatePortCargoUpdateSealNumber1>(
            updatePortCargoUpdateSealNumber1)
        .call,
    TypedMiddleware<AppState, UpdatePortCargoUpdateAt>(
            updatePortCargoUpdateEventAt)
        .call,
    TypedMiddleware<AppState, SetPortCargoUpdateAccountPortDepot>(
            setPortCargoUpdateAccountPortDepot)
        .call,
    TypedMiddleware<AppState, WritePortCargoUpdate>(writePortCargoUpdate).call,
  ];
}

Middleware<AppState> _writePortCargoUpdate() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    if (action is WritePortCargoUpdate) {
      final existingToken = store.state.authState.token;
      final selectedAccountId = store.state.accountState.account != null
          ? store.state.accountState.account.id
          : 0;
      final submitHeaders = authHeaders(existingToken);

      var portCargoUpdate = action.portCargoUpdateCreate.copyWith(
          applicationUserId: store.state.authState.user.id,
          accountId: selectedAccountId);

      // Remove jobContainerId and jobId if they are 0
      if (portCargoUpdate.sailingJobContainerId == 0) {
        portCargoUpdate = portCargoUpdate.copyWith(sailingJobContainerId: null);
      }
      if (portCargoUpdate.sailingJobId == 0) {
        portCargoUpdate = portCargoUpdate.copyWith(sailingJobId: null);
      }

      restService
          .post(standardRoute("/v1/$selectedAccountId/port_cargo_updates"),
              headers: submitHeaders, body: json.encode(portCargoUpdate))
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 201) {
            if (action.uniqueId != "") {
              List<PortCargoUpdateQueue> portCargoUpdateQueue =
                  store.state.portCargoUpdateState.portCargoUpdateQueue;
              portCargoUpdateQueue
                  .removeWhere((x) => x.uniqueId == action.uniqueId);
              store.dispatch(UpdateQueuePortCargoUpdate(portCargoUpdateQueue));
              action.completer.complete(ApiResponse(success: true, result: ""));
            }
          } else {
            ApiResponse apiResponse =
                ApiResponse(success: false, result: coreResponse.message);
            action.completer.complete(apiResponse);
          }
        } else {
          ApiResponse apiResponse = ApiResponse(
              success: false,
              result: "Could not create cargo update $statusCode");
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          }
          action.completer.complete(apiResponse);
        }
      }).catchError((dynamic error) {
        ApiResponse apiResponse =
            ApiResponse(success: false, result: error.toString());
        action.completer.complete(apiResponse);
      });
    }
    next(action);
  };
}

Middleware<AppState> _queuePortCargoUpdate() {
  return (Store store, action, NextDispatcher next) async {
    final now = DateTime.now();
    if (action is QueuePortCargoUpdate) {
      List<PortCargoUpdateQueue> portCargoQueue =
          store.state.portCargoUpdateState.portCargoUpdateQueue ??
              <PortCargoUpdateQueue>[];

      FileStore tempStorage = store.state.storageState.tempStorage;

      PortCargoUpdateCreate updatedPortCargoUpdate =
          action.portCargoUpdateCreate.updateAt == ""
              ? action.portCargoUpdateCreate
                  .copyWith(updateAt: getDateTimeWithOffset(DateTime.now()))
              : action.portCargoUpdateCreate;

      var newPortCargoQueue = PortCargoUpdateQueue(
          uniqueId: now.microsecondsSinceEpoch.toString(),
          portCargoUpdateCreate: updatedPortCargoUpdate,
          complete: false,
          createdAt: DateTime.now().toString(),
          error: "");

      portCargoQueue.add(newPortCargoQueue);
      store.dispatch(UpdateQueuePortCargoUpdate(portCargoQueue));
      store.dispatch(FlagPortCargoUpdateQueued());
      if (tempStorage.filePath != "" &&
          tempStorage.storageType == StorageType.cargoMovement) {
        store.dispatch(MoveTempStorageToFileStores(newPortCargoQueue.uniqueId));
      }
    }
    next(action);
  };
}

Middleware<AppState> _setPortCargoUpdateHomeVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetPortCargoUpdateHomeVariables) {
      PortCargoUpdateCreate portCargoUpdateCreate =
          store.state.portCargoUpdateState.portCargoUpdateCreate.copyWith(
              accountPortLocationId: action.accountPortDepot,
              carrierId: action.carrierId,
              role: action.role);
      store.dispatch(UpdatePortCargoUpdateCreate(portCargoUpdateCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setPortCargoUpdateFreightVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetPortCargoUpdateFreightVariables) {
      PortCargoUpdateCreate portCargoUpdateCreate =
          store.state.portCargoUpdateState.portCargoUpdateCreate.copyWith(
              vesselName: action.vesselName, voyageNumber: action.voyageNumber);
      store.dispatch(UpdatePortCargoUpdateCreate(portCargoUpdateCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setPortCargoUpdateStatusVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetPortCargoUpdateStatusVariables) {
      PortCargoUpdateCreate portCargoUpdateCreate = store
          .state.portCargoUpdateState.portCargoUpdateCreate
          .copyWith(comment: action.inputComment);
      store.dispatch(UpdatePortCargoUpdateCreate(portCargoUpdateCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updatePortCargoUpdateContainerNumber() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdatePortCargoUpdateContainerNumber) {
      PortCargoUpdateCreate portCargoUpdateCreate =
          store.state.portCargoUpdateState.portCargoUpdateCreate.copyWith(
              containerData: store.state.portCargoUpdateState
                  .portCargoUpdateCreate.containerData
                  .copyWith(containerNumber: action.containerNumber));
      store.dispatch(UpdatePortCargoUpdateCreate(portCargoUpdateCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updatePortCargoUpdateContainerWeight() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdatePortCargoUpdateContainerWeight) {
      PortCargoUpdateCreate portCargoUpdateCreate =
          store.state.portCargoUpdateState.portCargoUpdateCreate.copyWith(
              containerData: store.state.portCargoUpdateState
                  .portCargoUpdateCreate.containerData
                  .copyWith(containerWeight: action.containerWeight));
      store.dispatch(UpdatePortCargoUpdateCreate(portCargoUpdateCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updatePortCargoUpdatePackWeight() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdatePortCargoUpdatePackWeight) {
      PortCargoUpdateCreate portCargoUpdateCreate =
          store.state.portCargoUpdateState.portCargoUpdateCreate.copyWith(
              packData: store
                  .state.portCargoUpdateState.portCargoUpdateCreate.packData
                  .copyWith(packWeight: action.packWeight));

      store.dispatch(UpdatePortCargoUpdateCreate(portCargoUpdateCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updatePortCargoUpdateContainerWeightUnit() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdatePortCargoUpdateContainerWeightUnit) {
      PortCargoUpdateCreate portCargoUpdateCreate =
          store.state.portCargoUpdateState.portCargoUpdateCreate.copyWith(
              containerData: store.state.portCargoUpdateState
                  .portCargoUpdateCreate.containerData
                  .copyWith(containerWeightUnit: action.containerWeightUnit));
      store.dispatch(UpdatePortCargoUpdateCreate(portCargoUpdateCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updatePortCargoUpdatePackWeightUnit() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdatePortCargoUpdatePackWeightUnit) {
      PortCargoUpdateCreate portCargoUpdateCreate =
          store.state.portCargoUpdateState.portCargoUpdateCreate.copyWith(
              packData: store
                  .state.portCargoUpdateState.portCargoUpdateCreate.packData
                  .copyWith(packWeightUnit: action.packWeightUnit));
      store.dispatch(UpdatePortCargoUpdateCreate(portCargoUpdateCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updatePortCargoUpdateSealNumber1() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdatePortCargoUpdateSealNumber1) {
      PortCargoUpdateCreate portCargoUpdateCreate =
          store.state.portCargoUpdateState.portCargoUpdateCreate.copyWith(
              containerData: store.state.portCargoUpdateState
                  .portCargoUpdateCreate.containerData
                  .copyWith(sealNumber1: action.sealNumber1));
      store.dispatch(UpdatePortCargoUpdateCreate(portCargoUpdateCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updatePortCargoUpdateEventAt() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdatePortCargoUpdateAt) {
      PortCargoUpdateCreate portCargoUpdateCreate = store
          .state.portCargoUpdateState.portCargoUpdateCreate
          .copyWith(updateAt: action.updateAt);
      store.dispatch(UpdatePortCargoUpdateCreate(portCargoUpdateCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setPortCargoUpdateAccountPortDepot() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetPortCargoUpdateAccountPortDepot) {
      PortCargoUpdateCreate portCargoUpdateCreate = store
          .state.portCargoUpdateState.portCargoUpdateCreate
          .copyWith(accountPortLocationId: action.accountPortDepotId);
      store.dispatch(UpdatePortCargoUpdateCreate(portCargoUpdateCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setPortCargoUpdateContainerVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetPortCargoUpdateContainerVariables) {
      PortCargoUpdateCreate portCargoUpdateCreate;

      switch (action.shipment) {
        case CadiCargoMovementShipment.container:
          portCargoUpdateCreate =
              store.state.portCargoUpdateState.portCargoUpdateCreate.copyWith(
            shipment: action.shipment,
            billOfLading: action.inputOne,
            containerData: store
                .state.portCargoUpdateState.portCargoUpdateCreate.containerData
                .copyWith(
              containerNumber: action.inputTwo,
              containerWeight: action.grossWeight,
              containerWeightUnit: action.weightUnit,
            ),
          );
          break;
        case CadiCargoMovementShipment.breakBulk:
          portCargoUpdateCreate =
              store.state.portCargoUpdateState.portCargoUpdateCreate.copyWith(
                  shipment: action.shipment,
                  billOfLading: action.inputOne,
                  packData: store
                      .state.portCargoUpdateState.portCargoUpdateCreate.packData
                      .copyWith(
                    packSequenceNumber: "${action.inputOne}_${action.inputTwo}",
                    packingMode: action.selectedJobPackType,
                    packWeight: action.grossWeight,
                    packWeightUnit: action.weightUnit,
                  ));
          break;
        default:
          portCargoUpdateCreate =
              store.state.portCargoUpdateState.portCargoUpdateCreate.copyWith(
            shipment: action.shipment,
            billOfLading: action.inputTwo,
            containerData: store
                .state.portCargoUpdateState.portCargoUpdateCreate.containerData
                .copyWith(
              containerNumber: action.inputOne,
              containerWeight: action.grossWeight,
              containerWeightUnit: action.weightUnit,
            ),
          );
          break;
      }
      store.dispatch(UpdatePortCargoUpdateCreate(portCargoUpdateCreate));
    }
    next(action);
  };
}
