import 'package:cadi_core_app/redux/actions/auth_actions.dart';
import 'package:cadi_core_app/redux/actions/port_event_actions.dart';
import 'package:cadi_core_app/redux/models/core_api_response.dart';
import 'package:cadi_core_app/redux/models/port_event/port_event_models.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:cadi_core_app/services/rest_service.dart';
import 'dart:convert';
import 'package:cadi_core_app/helpers/helpers.dart';

List<Middleware<AppState>> createPortEventMiddleware() {
  return [
    TypedMiddleware<AppState, SetPortEventIndexVariables>(_setPortEventIndexVariables()).call,
    TypedMiddleware<AppState, SetPortEventCaptureVariables>(_setPortEventCaptureVariables()).call,
    TypedMiddleware<AppState, SetPortEventRefuelVariables>(_setPortEventRefuelVariables()).call,
    TypedMiddleware<AppState, QueuePortEvent>(_queuePortEvent()).call,
    TypedMiddleware<AppState, UpdatePortEventEventAt>(_updatePortEventEventAt()).call,
    TypedMiddleware<AppState, UpdatePortEventEventCompleteAt>(_updatePortEventEventCompleteAt()).call,
    TypedMiddleware<AppState, WritePortEvent>(_writePortEvent()).call,
  ];
}

Middleware<AppState> _setPortEventIndexVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetPortEventIndexVariables) {
      PortEventCreate portEventCreate = store.state.portEventState.portEventCreate.copyWith(
          accountPortLocationId: action.accountPortLocationId,
          carrierId: action.carrierId,
          role: action.role);
      store.dispatch(UpdatePortEventCreate(portEventCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setPortEventCaptureVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetPortEventCaptureVariables) {
      PortEventCreate portEventCreate = store.state.portEventState.portEventCreate.copyWith(
          vesselName: action.vesselName,
          voyageNumber: action.voyageNumber,
          accountPortBerthId: action.accountPortBerthId,
          comment: action.comment,
          eventAt: action.eventAt,
          eventCompleteAt: action.eventCompleteAt,
          images: action.images
      );
      store.dispatch(UpdatePortEventCreate(portEventCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setPortEventRefuelVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetPortEventRefuelVariables) {
      var refuelData = RefuelData(
          volume: action.volume,
          volumeUnit: action.volumeUnit,
          price: action.price,
          priceCurrency: action.priceCurrency,
          distanceTravelled: action.distanceTravelled,
          distanceTravelledUnit: action.distanceTravelledUnit);

      PortEventCreate portEventCreate = store.state.portEventState.portEventCreate.copyWith(
          fuelTypeId: action.fuelTypeId,
          refuelData: refuelData
      );
      store.dispatch(UpdatePortEventCreate(portEventCreate));
    }
    next(action);
  };
}

Middleware<AppState> _queuePortEvent() {
  return (Store store, action, NextDispatcher next) async {
    final now = DateTime.now();
    if (action is QueuePortEvent) {
      List<PortEventCreateQueue> portEventQueue =
          store.state.portEventState.portEventCreateQueue ?? <PortEventCreateQueue>[];

      var newPortEventQueue = PortEventCreateQueue(
          uniqueId: now.microsecondsSinceEpoch.toString(),
          portEventCreate: action.portEventCreate,
          complete: false,
          createdAt: DateTime.now().toString(),
          error: "");

      portEventQueue.add(newPortEventQueue);
      store.dispatch(UpdateQueuePortEvent(portEventQueue));
      store.dispatch(FlagPortEventQueued());
    }
    next(action);
  };
}

Middleware<AppState> _updatePortEventEventAt() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdatePortEventEventAt) {
      PortEventCreate portEventCreate = store.state.portEventState.portEventCreate.copyWith(
          eventAt: action.eventAt
      );
      store.dispatch(UpdatePortEventCreate(portEventCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updatePortEventEventCompleteAt() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdatePortEventEventCompleteAt) {
      PortEventCreate portEventCreate = store.state.portEventState.portEventCreate.copyWith(
          eventCompleteAt: action.eventCompleteAt
      );
      store.dispatch(UpdatePortEventCreate(portEventCreate));
    }
    next(action);
  };
}

Middleware<AppState> _writePortEvent() {
  return (Store store, action, NextDispatcher next) async {
    RestService restService = RestService();
    const JsonDecoder decoder = JsonDecoder();
    if (action is WritePortEvent) {
      final existingToken = store.state.authState.token;
      final selectedAccountId = store.state.accountState.account != null
          ? store.state.accountState.account.id
          : 0;
      final submitHeaders = authHeaders(existingToken);

      var portEvent = action.portEventCreate
          .copyWith(applicationUserId: store.state.authState.user.id,
          accountId: selectedAccountId);

      restService
          .post(
          standardRoute("/v1/$selectedAccountId/port_events"),
          headers: submitHeaders,
          body: json.encode(portEvent))
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if(coreResponse.statusCode == 201){
            if (action.uniqueId != "") {
              List<PortEventCreateQueue> portEventQueue =
                  store.state.portEventState.portEventCreateQueue;
              portEventQueue.removeWhere((x) => x.uniqueId == action.uniqueId);
              store.dispatch(UpdateQueuePortEvent(portEventQueue));
              action.completer.complete(ApiResponse(success: true, result: ""));
            }
          } else {
            ApiResponse apiResponse =
            ApiResponse(success: false, result: coreResponse.message);
            action.completer.complete(apiResponse);
          }
        } else {
          ApiResponse apiResponse = ApiResponse(
              success: false,
              result: "Could not create port event $statusCode");
          /*
          Should start removing unwritable objects from queue after
          n amount of retries.
           */
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          }
          action.completer.complete(apiResponse);
        }
      }).catchError((dynamic error) {
        ApiResponse apiResponse =
        ApiResponse(success: false, result: error.toString());
        action.completer.complete(apiResponse);
      });
    }
    next(action);
  };
}
