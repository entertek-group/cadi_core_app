import 'package:cadi_core_app/redux/models/core_api_response.dart';
import 'package:cadi_core_app/redux/models/list_converters.dart';
import 'package:redux/redux.dart';

import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:cadi_core_app/services/rest_service.dart';
import 'dart:convert';
//helpers
import 'package:cadi_core_app/helpers/helpers.dart';

List<Middleware<AppState>> createReferenceMiddleware() {
  final getVesselClasses = _getVesselClasses();
  final getFuelTypes = _getFuelTypes();
  final getCargoTypes = _getCargoTypes();
  final getJobPackTypes = _getJobPackTypes();

  return [
    TypedMiddleware<AppState, GetVesselClasses>(getVesselClasses).call,
    TypedMiddleware<AppState, GetFuelTypes>(getFuelTypes).call,
    TypedMiddleware<AppState, GetCargoTypes>(getCargoTypes).call,
    TypedMiddleware<AppState, GetJobPackTypes>(getJobPackTypes).call,
  ];
}

Middleware<AppState> _getVesselClasses() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);

    if (action is GetVesselClasses) {
      restService
          .get(standardRoute("/v1/vessel_classes"), headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          List<CadiGeneric> vesselClasses = converterCadiGeneric(body);

          store.dispatch(GetVesselClassesSuccess(vesselClasses));

          action.completer.complete(ApiResponse(success: true, result: ""));
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else if (statusCode == 404) {
            store.dispatch(GetVesselClassesError("No records found"));
          } else {
            store.dispatch(GetVesselClassesError(
                "No record found matching search criteria"));
          }
          action.completer.complete(
              ApiResponse(success: false, result: "No records found"));
        }
      }).catchError((dynamic error) {
        store.dispatch(GetVesselClassesError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}

Middleware<AppState> _getFuelTypes() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);

    if (action is GetFuelTypes) {
      restService
          .get(standardRoute("/v1/app/fuel_types"), headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if(coreResponse.statusCode == 200){
            List<FuelType> fuelTypes = converterFuelType(coreResponse.result);

            store.dispatch(GetFuelTypesSuccess(fuelTypes));

            action.completer.complete(ApiResponse(success: true, result: ""));
          }else {
            store.dispatch(GetFuelTypesError(coreResponse.message));
            action.completer.complete(
                ApiResponse(success: false, result: "No records found"));
          }
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else if (statusCode == 404) {
            store.dispatch(GetFuelTypesError("No records found"));
          } else {
            store.dispatch(
                GetFuelTypesError("No record found matching search criteria"));
          }
          action.completer.complete(
              ApiResponse(success: false, result: "No records found"));
        }
      }).catchError((dynamic error) {
        store.dispatch(GetFuelTypesError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}

Middleware<AppState> _getCargoTypes() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);

    if (action is GetCargoTypes) {
      restService
          .get(standardRoute("/v1/app/cargo_types"), headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if(coreResponse.statusCode == 200){
            List<CadiGeneric> cargoTypes = converterCadiGeneric(coreResponse.result);

            store.dispatch(GetCargoTypesSuccess(cargoTypes));

            action.completer.complete(ApiResponse(success: true, result: ""));
          }else {
            store.dispatch(GetCargoTypesError(coreResponse.message));
            action.completer.complete(
                ApiResponse(success: false, result: "No records found"));
          }
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else if (statusCode == 404) {
            store.dispatch(GetCargoTypesError("No records found"));
          } else {
            store.dispatch(
                GetCargoTypesError("No record found matching search criteria"));
          }
          action.completer.complete(
              ApiResponse(success: false, result: "No records found"));
        }
      }).catchError((dynamic error) {
        store.dispatch(GetCargoTypesError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}

Middleware<AppState> _getJobPackTypes() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);

    if (action is GetJobPackTypes) {
      restService
          .get(standardRoute("/v1/app/pack_types"), headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 200) {
            List<CadiGeneric> jobPackTypes = converterCadiGeneric(coreResponse.result);

            store.dispatch(GetJobPackTypesSuccess(jobPackTypes));

            action.completer.complete(ApiResponse(success: true, result: ""));
          } else {
            store.dispatch(GetJobPackTypesError(
                coreResponse.message));
            action.completer.complete(
                ApiResponse(success: false, result: "No records found"));
          }

        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          } else if (statusCode == 404) {
            store.dispatch(GetJobPackTypesError("No records found"));
          } else {
            store.dispatch(GetJobPackTypesError(
                "No record found matching search criteria"));
          }
          action.completer.complete(
              ApiResponse(success: false, result: "No records found"));
        }
      }).catchError((dynamic error) {
        store.dispatch(GetJobPackTypesError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}
