import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/actions/auth_actions.dart';
import 'package:cadi_core_app/redux/models/core_api_response.dart';
import 'package:redux/redux.dart';

import 'package:cadi_core_app/redux/actions/report_actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:cadi_core_app/services/rest_service.dart';
import 'dart:convert';
//helpers
import 'package:cadi_core_app/helpers/helpers.dart';

List<Middleware<AppState>> createReportMiddleware() {
  final getPortReport = _getPortReport();
  // final filterReportShortDataByRole =
  // _filterReportShortDataByRole();

  return [
    TypedMiddleware<AppState, GetPortReport>(getPortReport).call,
    // TypedMiddleware<AppState, FilterReportShortDataByRole>(
    //     filterReportShortDataByRole),
  ];
}

Middleware<AppState> _getPortReport() {
  RestService restService = RestService();
  const JsonDecoder decoder = JsonDecoder();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);
    final selectedAccountId =
        store.state.accountState.account != null //todo here
            ? store.state.accountState.account.id
            : 0;

    if (action is GetPortReport) {
      final unLocode = action.unLocode;
      final vesselName = action.vesselName;
      final voyageNumber = action.voyageNumber;

      var reportUrl = "";
      if (action.reportType == PortReportType.restow)
        {
          reportUrl = "restow";
        }
      else if (action.reportType == PortReportType.shortShip)
        {
          reportUrl = "short_ship";
        }
      else if (action.reportType == PortReportType.shortLand)
        {
          reportUrl = "short_land";
        }
      else if (action.reportType == PortReportType.movementStatus)
        {
          reportUrl = "movement_states";
        }
      print("report_url");
      print(reportUrl);
      print("/v1/$selectedAccountId/port_reports/$reportUrl?vesselName=$vesselName&voyageNumber=$voyageNumber&unLoCode=$unLocode");
      restService
          .get(
              standardRoute(
                  "/v1/$selectedAccountId/port_reports/$reportUrl?vesselName=$vesselName&voyageNumber=$voyageNumber&unLoCode=$unLocode"),
              headers: submitHeaders)
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if(coreResponse.statusCode == 200){

            if (action.reportType == PortReportType.movementStatus) {
              print("setting port movement report summary");
              var portMovementReportSummary = PortMovementReportSummary.fromJson(coreResponse.result);
              store.dispatch(GetPortMovementReportSuccess(portMovementReportSummary));
            } else {
              var portReportSummary = PortReportSummary.fromJson(coreResponse.result);
              store.dispatch(GetPortReportSuccess(portReportSummary));
            }


            action.completer.complete(ApiResponse(success: true, result: ""));
          }else{
            action.completer.complete(ApiResponse(
                success: false, result: "No records found matching criteria"));
            store.dispatch(
                GetPortReportError("No records found matching criteria"));
          }
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
            action.completer.complete(null);
            store.dispatch(GetPortReportError("Token expired"));
          } else if (statusCode == 404) {
            action.completer.complete(ApiResponse(
                success: false, result: "No records found matching criteria"));
            store.dispatch(
                GetPortReportError("No records found matching criteria"));
          }
        }
      }).catchError((dynamic error) {
        store.dispatch(GetPortReportError(error.toString()));
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}
//
// Middleware<AppState> _filterReportShortDataByRole() {
//   return (Store store, action, NextDispatcher next) async {
//     ReportState _reportState = store.state.reportState;
//     String _vesselName = _reportState.filterVesselName;
//     if (action is FilterReportShortDataByRole) {
//       switch (action.role) {
//         case ReportFilterRole.all:
//           List<ShortReport> filteredShortReportData = _reportState
//               .shortReportData
//               .where((element) => element.vesselName == _vesselName)
//               .toList();
//           store.dispatch(SetFilteredShortReport(filteredShortReportData));
//           break;
//         case ReportFilterRole.loaded:
//           List<ShortReport> filteredShortReportData = _reportState
//               .shortReportData
//               .where((element) =>
//           element.vesselName == _vesselName &&
//               element.role == CadiCargoMovementRole.freightLoad)
//               .toList();
//           store.dispatch(SetFilteredShortReport(filteredShortReportData));
//           break;
//         case ReportFilterRole.unloaded:
//           List<ShortReport> filteredShortReportData = _reportState
//               .shortReportData
//               .where((element) =>
//           element.vesselName == _vesselName &&
//               element.role == CadiCargoMovementRole.freightUnload)
//               .toList();
//           store.dispatch(SetFilteredShortReport(filteredShortReportData));
//           break;
//
//         default:
//           List<ShortReport> filteredShortReportData = _reportState
//               .shortReportData
//               .where((element) => element.vesselName == _vesselName)
//               .toList();
//           store.dispatch(SetFilteredShortReport(filteredShortReportData));
//           break;
//       }
//     }
//     next(action);
//   };
// }

