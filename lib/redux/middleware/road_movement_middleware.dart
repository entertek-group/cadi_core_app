import 'dart:io';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/models/cargo_data/container_data.dart';
import 'package:cadi_core_app/redux/models/core_api_response.dart';
import 'package:redux/redux.dart';

import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:cadi_core_app/services/rest_service.dart';
import 'dart:convert';
//helpers
import 'package:cadi_core_app/helpers/helpers.dart';

List<Middleware<AppState>> createRoadMovementMiddleware() {
  final setRoadHomeVariables = _setRoadHomeVariables();
  final setRoadContainerVariables = _setRoadContainerVariables();
  final setRoadStatusVariables = _setRoadStatusVariables();
  final setRoadAccountPortDepot = _setRoadAccountPortDepot();
  final setAccountVehicleDriverId = _setAccountVehicleDriverId();
  final updateRoadContainerNumber = _updateRoadContainerNumber();
  final setRoadFreightVariables = _setRoadFreightVariables();
  final queueRoadMovement = _queueRoadMovement();
  final writeRoadMovement = _writeRoadMovement();
  final uploadRoadMovementImage = _uploadRoadMovementImage();
  final setRoadMovementSignature = _setRoadMovementSignature();

  return [
    TypedMiddleware<AppState, SetRoadHomeVariables>(setRoadHomeVariables).call,
    TypedMiddleware<AppState, SetRoadContainerVariables>(
            setRoadContainerVariables)
        .call,
    TypedMiddleware<AppState, SetRoadFreightVariables>(setRoadFreightVariables)
        .call,
    TypedMiddleware<AppState, SetAccountVehicleDriverId>(
            setAccountVehicleDriverId)
        .call,
    TypedMiddleware<AppState, SetRoadStatusVariables>(setRoadStatusVariables)
        .call,
    TypedMiddleware<AppState, UpdateRoadContainerNumber>(
            updateRoadContainerNumber)
        .call,
    TypedMiddleware<AppState, SetRoadAccountPortDepot>(setRoadAccountPortDepot)
        .call,
    TypedMiddleware<AppState, QueueRoadMovement>(queueRoadMovement).call,
    TypedMiddleware<AppState, WriteRoadMovement>(writeRoadMovement).call,
    TypedMiddleware<AppState, UploadRoadMovementImage>(uploadRoadMovementImage)
        .call,
    TypedMiddleware<AppState, SetRoadMovementSignature>(
            setRoadMovementSignature)
        .call,
  ];
}

Middleware<AppState> _setRoadHomeVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetRoadHomeVariables) {
      RoadMovementCreate roadMovementCreate =
          store.state.roadMovementState.roadMovementCreate.copyWith(
              accountPortLocationId: action.accountPortDepot,
              accountVehicleId: action.vehicleId,
              accountVehicleDriverId: action.vehicleDriverId,
              accountVehicleOperatorId: action.vehicleOperatorId,
              role: action.role);
      store.dispatch(UpdateRoadMovementCreate(roadMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setRoadContainerVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetRoadContainerVariables) {
      RoadMovementCreate roadMovementCreate =
          store.state.roadMovementState.roadMovementCreate.copyWith(
              containerData: action.containers,
              isBreakBulk: action.isBreakBulk);
      store.dispatch(UpdateRoadMovementCreate(roadMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setRoadFreightVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetRoadFreightVariables) {
      RoadMovementCreate roadMovementCreate =
          store.state.roadMovementState.roadMovementCreate.copyWith(
              waybillNumber: action.waybill,
              legNumber: action.legNumber,
              expectedArrivalAt: action.eta,
              containerData: action.containers,
              isBreakBulk: action.isBreakBulk,
              documents: action.attachedDocuments);
      store.dispatch(UpdateRoadMovementCreate(roadMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setAccountVehicleDriverId() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetAccountVehicleDriverId) {
      RoadMovementCreate roadMovementCreate =
          store.state.roadMovementState.roadMovementCreate.copyWith(
        accountVehicleDriverId: action.accountVehicleDriverId,
      );
      store.dispatch(UpdateRoadMovementCreate(roadMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setRoadStatusVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetRoadStatusVariables) {
      RoadMovementCreate roadMovementCreate =
          store.state.roadMovementState.roadMovementCreate.copyWith(
              state: action.status,
              comment: action.inputComment,
              images: action.attachedImages);
      store.dispatch(UpdateRoadMovementCreate(roadMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _updateRoadContainerNumber() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdateRoadContainerNumber) {
      List<ContainerData> updatedContainers = <ContainerData>[];
      List<ContainerData> containers =
          store.state.roadMovementState.roadMovementCreate.containerData;

      print("fokit");

      for (var container in containers) {
        if (container.containerNumber == action.previousContainerNumber) {
          var updateContainer =
              container.copyWith(containerNumber: action.containerNumber);
          updatedContainers.add(updateContainer);
        } else {
          updatedContainers.add(container);
        }
      }

      RoadMovementCreate roadMovementCreate = store
          .state.roadMovementState.roadMovementCreate
          .copyWith(containerData: updatedContainers);
      store.dispatch(UpdateRoadMovementCreate(roadMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setRoadAccountPortDepot() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetRoadAccountPortDepot) {
      RoadMovementCreate roadMovementCreate = store
          .state.roadMovementState.roadMovementCreate
          .copyWith(accountPortLocationId: action.accountPortDepotId);
      store.dispatch(UpdateRoadMovementCreate(roadMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _setRoadMovementSignature() {
  return (Store store, action, NextDispatcher next) async {
    if (action is SetRoadMovementSignature) {
      RoadMovementCreate roadMovementCreate =
          store.state.roadMovementState.roadMovementCreate.copyWith(
              signatoryFullName: action.signatoryFullName,
              signature: action.signature,
              documents: action.attachedDocuments);
      store.dispatch(UpdateRoadMovementCreate(roadMovementCreate));
    }
    next(action);
  };
}

Middleware<AppState> _writeRoadMovement() {
  return (Store store, action, NextDispatcher next) async {
    RestService restService = RestService();
    const JsonDecoder decoder = JsonDecoder();
    if (action is WriteRoadMovement) {
      final existingToken = store.state.authState.token;
      final selectedAccountId = store.state.accountState.account != null
          ? store.state.accountState.account.id
          : 0;
      final submitHeaders = authHeaders(existingToken);

      var roadMovement = action.roadMovementCreate.copyWith(
          applicationUserId: store.state.authState.user.id,
          accountId: selectedAccountId);

      restService
          .post(standardRoute("/v1/$selectedAccountId/road_movements"),
              headers: submitHeaders, body: json.encode(roadMovement))
          .then((dynamic res) {
        final body = decoder.convert(res.body);
        final statusCode = res.statusCode;
        if (statusCode == 200) {
          var coreResponse = CoreApiResponse.fromJson(body);
          if (coreResponse.statusCode == 201) {
            if (action.uniqueId != "") {
              List<RoadMovementCreateQueue> roadQueue =
                  store.state.roadMovementState.roadMovementCreateQueue;
              roadQueue.removeWhere((x) => x.uniqueId == action.uniqueId);
              store.dispatch(UpdateQueueRoadMovement(roadQueue));
              action.completer.complete(ApiResponse(success: true, result: ""));
            }
          } else {
            ApiResponse apiResponse =
                ApiResponse(success: false, result: coreResponse.message);
            action.completer.complete(apiResponse);
          }
        } else {
          ApiResponse apiResponse = ApiResponse(
              success: false,
              result: "Could not create road movement $statusCode");
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          }
          action.completer.complete(apiResponse);
        }
      }).catchError((dynamic error) {
        ApiResponse apiResponse =
            ApiResponse(success: false, result: error.toString());
        action.completer.complete(apiResponse);
      });
    }
    next(action);
  };
}

Middleware<AppState> _queueRoadMovement() {
  return (Store store, action, NextDispatcher next) async {
    final now = DateTime.now();
    if (action is QueueRoadMovement) {
      List<RoadMovementCreateQueue> roadQueue =
          store.state.roadMovementState.roadMovementCreateQueue ??
              <RoadMovementCreateQueue>[];

      RoadMovementCreate roadMovementCreate = action.roadMovementCreate
          .copyWith(movementAt: getDateTimeWithOffset(DateTime.now()));

      FileStore tempStorage = store.state.storageState.tempStorage;

      var newRoadQueue = RoadMovementCreateQueue(
          uniqueId: now.microsecondsSinceEpoch.toString(),
          roadMovement: roadMovementCreate,
          complete: false,
          createdAt: DateTime.now().toString(),
          error: "");

      roadQueue.add(newRoadQueue);
      store.dispatch(UpdateQueueRoadMovement(roadQueue));
      store.dispatch(FlagRoadMovementQueued());
      if (tempStorage.filePath != "" &&
          tempStorage.storageType == StorageType.roadMovement) {
        store.dispatch(MoveTempStorageToFileStores(newRoadQueue.uniqueId));
      }
    }
    next(action);
  };
}

Middleware<AppState> _uploadRoadMovementImage() {
  RestService restService = RestService();
  return (Store store, action, NextDispatcher next) async {
    final existingToken = store.state.authState.token;
    final submitHeaders = authHeaders(existingToken);
    final selectedAccountId = store.state.accountState.account != null
        ? store.state.accountState.account.id
        : 0;
    if (action is UploadRoadMovementImage) {
      File image = File(action.fileStore.filePath);
      restService
          .upload(
              standardRoute(
                  "/v1/accounts/$selectedAccountId/road_movements/${action.fileStore.apiId}"),
              image,
              "road_image",
              headers: submitHeaders)
          .then((dynamic res) {
        final statusCode = res.statusCode;
        if (statusCode == 202) {
          action.completer.complete(ApiResponse(success: true, result: ""));
        } else {
          //Logout here
          if (statusCode == 401) {
            store.dispatch(TokenExpired());
          }
          action.completer.complete(
              ApiResponse(success: false, result: "Could not upload image"));
        }
      }).catchError((dynamic error) {
        action.completer
            .complete(ApiResponse(success: false, result: error.toString()));
      });
    }
    next(action);
  };
}
