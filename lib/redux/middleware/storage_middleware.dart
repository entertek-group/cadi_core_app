import 'dart:io';

import 'package:cadi_core_app/redux/actions/storage_actions.dart';
import 'package:redux/redux.dart';
import 'package:collection/collection.dart';

import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:path_provider/path_provider.dart';

List<Middleware<AppState>> createStorageMiddleware() {
  final addFileToStorage = _addFileToStorage();
  final removeFileFromStorage = _removeFileFromStorage();
  final storeTemporaryFile = _storeTemporaryFile();
  final resetTempStorage = _resetTempStorage();
  final moveTempStorageToFileStores = _moveTempStorageToFileStores();
  final updateApiIdInFileStores = _updateApiIdInFileStores();

  return [
    TypedMiddleware<AppState, AddFileToStorage>(addFileToStorage).call,
    TypedMiddleware<AppState, RemoveFileFromStorage>(removeFileFromStorage).call,
    TypedMiddleware<AppState, StoreTemporaryFile>(storeTemporaryFile).call,
    TypedMiddleware<AppState, ResetTempStorage>(resetTempStorage).call,
    TypedMiddleware<AppState, MoveTempStorageToFileStores>(
        moveTempStorageToFileStores).call,
    TypedMiddleware<AppState, UpdateApiIdInFileStores>(updateApiIdInFileStores).call,
  ];
}

Middleware<AppState> _addFileToStorage() {
  return (Store store, action, NextDispatcher next) async {
    if (action is AddFileToStorage) {
      List<FileStore> fileStores = store.state.storageState.fileStores;

      fileStores.add(action.fileStore);
      // print(cargoMovement.toString());
      store.dispatch(UpdateFileStores(fileStores));
    }
    next(action);
  };
}

Middleware<AppState> _removeFileFromStorage() {
  return (Store store, action, NextDispatcher next) async {
    if (action is RemoveFileFromStorage) {
      List<FileStore> fileStores = store.state.storageState.fileStores;

      FileStore? fileToRemove = fileStores.firstWhereOrNull((x) =>
          x.uniqueId == action.uniqueId && x.storageType == action.storageType);

      if (fileToRemove != null) {
        fileStores.remove(fileToRemove);
        // fileStores.removeWhere((x) =>
        //     x.uniqueId == action.uniqueId &&
        //     x.storageType == action.storageType);
        try {
          await File(fileToRemove.filePath).delete();
        } on Exception catch (_) {
          print('never reached');
        }
      }
      // print(cargoMovement.toString());
      store.dispatch(UpdateFileStores(fileStores));
    }
    next(action);
  };
}

Middleware<AppState> _updateApiIdInFileStores() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UpdateApiIdInFileStores) {
      List<FileStore> fileStores = store.state.storageState.fileStores;

      FileStore? fileToUpdate = fileStores.firstWhereOrNull((x) =>
          x.uniqueId == action.uniqueId && x.storageType == action.storageType);

      if (fileToUpdate != null) {
        try {
          var newFile = fileToUpdate.copyWith(apiId: action.apiId);
          fileStores[fileStores.indexWhere((x) =>
              x.uniqueId == action.uniqueId &&
              x.storageType == action.storageType)] = newFile;
          store.dispatch(UpdateFileStores(fileStores));
        } on Exception catch (_) {
          print('never reached');
        }
      }
    }
    next(action);
  };
}

Middleware<AppState> _storeTemporaryFile() {
  return (Store store, action, NextDispatcher next) async {
    if (action is StoreTemporaryFile) {
      final directory = await _getApplicationDirectory();
      final now = DateTime.now();
      File workingFile = action.file;
      var fileName = now.microsecondsSinceEpoch.toString();
      var filePath = '$directory/$fileName';
      var fileBytes = await workingFile.readAsBytes();

      File tempFile = File(filePath);
      tempFile.writeAsBytes(fileBytes);

      final fileStore = FileStore(
          fileName: fileName,
          filePath: filePath,
          fileType: "",
          storageType: action.storageType,
          uniqueId: "",
          apiId: null);
      store.dispatch(UpdateTempStorage(fileStore));
    }
    next(action);
  };
}

Middleware<AppState> _resetTempStorage() {
  return (Store store, action, NextDispatcher next) async {
    if (action is ResetTempStorage) {
      FileStore tempStorage = store.state.storageState.tempStorage;

      if (tempStorage.filePath != "") {
        try {
          await File(tempStorage.filePath).delete();
        } on Exception catch (_) {
          print('never reached');
        }
      }
      store.dispatch(CompleteTempStorageReset());
    }
    next(action);
  };
}

Middleware<AppState> _moveTempStorageToFileStores() {
  return (Store store, action, NextDispatcher next) async {
    if (action is MoveTempStorageToFileStores) {
      FileStore tempStorage = store.state.storageState.tempStorage
          .copyWith(uniqueId: action.uniqueId);

      List<FileStore> fileStores = store.state.storageState.fileStores;

      fileStores.add(tempStorage);

      store.dispatch(UpdateFileStores(fileStores));
      store.dispatch(CompleteTempStorageReset());
    }
    next(action);
  };
}

Future _getApplicationDirectory() async {
  Directory directory = await getApplicationDocumentsDirectory();
  return directory.path;
}
