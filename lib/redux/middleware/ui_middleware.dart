import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:redux/redux.dart';

import '../models/ui_screen/ui_screen_models.dart';

List<Middleware<AppState>> createUiMiddleware() {
  //Port Movements
  final uiCargoHomeSetPortIndex = _uiCargoHomeSetPortIndex();
  final uiCargoHomeSetLocationIndex = _uiCargoHomeSetLocationIndex();
  final uiCargoHomeSetCarrierIndex = _uiCargoHomeSetCarrierIndex();
  final uiCargoHomeSetRoleIndex = _uiCargoHomeSetRoleIndex();
  final uiCargoHomeFreightSearchSetVesselName =
      _uiCargoHomeFreightSearchSetVesselName();
  final uiCargoHomeFreightSearchSetVoyageNumber =
      _uiCargoHomeFreightSearchSetVoyageNumber();
  final uiCargoHomeFreightSelectCrane =
  _uiCargoHomeFreightSelectCrane();
  final uiCargoHomeTruckDetailsSetTruckRegistration =
      _uiCargoHomeTruckDetailsSetTruckRegistration();
  final uiCargoHomeTruckDetailsSetTruckDriverId =
      _uiCargoHomeTruckDetailsSetTruckDriverId();
  final uiCargoUpdateSearchSetVariables = _uiCargoUpdateSearchSetVariables();
  final uiCargoReportSetReportType = _uiCargoReportSetReportType();
  final uiCargoReportSetPortIndex = _uiCargoReportSetPortIndex();
  final uiCargoReportSetLocationIndex = _uiCargoReportSetLocationIndex();
  final uiCargoReportSetSailingSchedule = _uiCargoReportSetSailingSchedule();
  final uiCargoReportSearchReset = _uiCargoReportSearchReset();

  //Port Cargo Updates
  final uiPortCargoUpdateSetPortIndex = _uiPortCargoUpdateSetPortIndex();
  final uiPortCargoUpdateSetLocationIndex = _uiPortCargoUpdateSetLocationIndex();
  final uiPortCargoUpdateSetCarrierIndex = _uiPortCargoUpdateSetCarrierIndex();
  final uiPortCargoUpdateSetRoleIndex = _uiPortCargoUpdateSetRoleIndex();
  final uiPortCargoUpdateFreightSearchSetVesselName = _uiPortCargoUpdateFreightSearchSetVesselName();
  final uiPortCargoUpdateFreightSearchSetVoyageNumber = _uiPortCargoUpdateFreightSearchSetVoyageNumber();

  //Road Movements
  final uiRoadMovementHomeSetPortIndex = _uiRoadMovementHomeSetPortIndex();
  final uiRoadMovementHomeSetLocationIndex = _uiRoadMovementHomeSetLocationIndex();
  final uiRoadMovementHomeSetRoleIndex = _uiRoadMovementHomeSetRoleIndex();
  final uiRoadMovementHomeSetVehicleIndex = _uiRoadMovementHomeSetVehicleIndex();
  final uiRoadMovementHomeSetVehicleDriverIndex = _uiRoadMovementHomeSetVehicleDriverIndex();
  final uiRoadMovementHomeSetVehicleOperatorIndex = _uiRoadMovementHomeSetVehicleOperatorIndex();

  final uiRoadMovementSearchSetWaybill = _uiRoadMovementSearchSetWaybill();
  final uiRoadMovementSearchSetLegNumber = _uiRoadMovementSearchSetLegNumber();
  final uiRoadMovementSearchSetEta = _uiRoadMovementSearchSetEta();

  //Port Events
  final uiPortEventIndexSetPortIndex = _uiPortEventIndexSetPortIndex();
  final uiPortEventIndexSetLocationIndex =
      _uiPortEventIndexSetLocationIndex();
  final uiPortEventIndexSetCarrierIndex =
      _uiPortEventIndexSetCarrierIndex();
  final uiPortEventIndexSetRoleIndex = _uiPortEventIndexSetRoleIndex();
  final uiPortEventCaptureSetVariables = _uiPortEventCaptureSetVariables();
  final uiVesselUpdateIndexSetVesselName = _uiVesselUpdateIndexSetVesselName();
  final uiVesselUpdateIndexSetVoyageNumber =
      _uiVesselUpdateIndexSetVoyageNumber();
  final uiVesselUpdateIndexSetVoyageDate = _uiVesselUpdateIndexSetVoyageDate();
  final uiVesselUpdateIndexSetPortIndex = _uiVesselUpdateIndexSetPortIndex();
  final uiVesselUpdateIndexSetLocationIndex =
      _uiVesselUpdateIndexSetLocationIndex();

  return [
    TypedMiddleware<AppState, UiCargoHomeSetPortIndex>(uiCargoHomeSetPortIndex).call,
    TypedMiddleware<AppState, UiCargoHomeSetLocationIndex>(
        uiCargoHomeSetLocationIndex).call,
    TypedMiddleware<AppState, UiCargoHomeSetCarrierIndex>(
        uiCargoHomeSetCarrierIndex).call,
    TypedMiddleware<AppState, UiCargoHomeSetRoleIndex>(uiCargoHomeSetRoleIndex).call,
    TypedMiddleware<AppState, UiCargoHomeFreightSearchSetVesselName>(
        uiCargoHomeFreightSearchSetVesselName).call,
    TypedMiddleware<AppState, UiCargoHomeFreightSearchSetVoyageNumber>(
        uiCargoHomeFreightSearchSetVoyageNumber).call,
    TypedMiddleware<AppState, UiCargoHomeFreightSelectCrane>(
        uiCargoHomeFreightSelectCrane).call,
    TypedMiddleware<AppState, UiCargoHomeTruckDetailsSetTruckRegistration>(
        uiCargoHomeTruckDetailsSetTruckRegistration).call,
    TypedMiddleware<AppState, UiCargoHomeTruckDetailsSetTruckDriverId>(
        uiCargoHomeTruckDetailsSetTruckDriverId).call,
    TypedMiddleware<AppState, UiCargoUpdateSearchSetVariables>(
        uiCargoUpdateSearchSetVariables).call,
    TypedMiddleware<AppState, UiCargoReportSetReportType>(
        uiCargoReportSetReportType).call,
    TypedMiddleware<AppState, UiCargoReportSetPortIndex>(
        uiCargoReportSetPortIndex).call,
    TypedMiddleware<AppState, UiCargoReportSetLocationIndex>(
        uiCargoReportSetLocationIndex).call,
    TypedMiddleware<AppState, UiCargoReportSetSailingSchedule>(
        uiCargoReportSetSailingSchedule).call,
    TypedMiddleware<AppState, UiCargoReportSearchReset>(
        uiCargoReportSearchReset).call,
    TypedMiddleware<AppState, UiPortCargoUpdateSetPortIndex>(
        uiPortCargoUpdateSetPortIndex).call,
    TypedMiddleware<AppState, UiPortCargoUpdateSetLocationIndex>(
        uiPortCargoUpdateSetLocationIndex).call,
    TypedMiddleware<AppState, UiPortCargoUpdateSetCarrierIndex>(
        uiPortCargoUpdateSetCarrierIndex).call,
    TypedMiddleware<AppState, UiPortCargoUpdateSetRoleIndex>(
        uiPortCargoUpdateSetRoleIndex).call,
    TypedMiddleware<AppState, UiPortCargoUpdateFreightSearchSetVesselName>(
        uiPortCargoUpdateFreightSearchSetVesselName).call,
    TypedMiddleware<AppState, UiPortCargoUpdateFreightSearchSetVoyageNumber>(
        uiPortCargoUpdateFreightSearchSetVoyageNumber).call,
    TypedMiddleware<AppState, UiPortEventIndexSetPortIndex>(
        uiPortEventIndexSetPortIndex).call,
    TypedMiddleware<AppState, UiPortEventIndexSetLocationIndex>(
        uiPortEventIndexSetLocationIndex).call,
    TypedMiddleware<AppState, UiPortEventIndexSetCarrierIndex>(
        uiPortEventIndexSetCarrierIndex).call,
    TypedMiddleware<AppState, UiPortEventIndexSetRoleIndex>(
        uiPortEventIndexSetRoleIndex).call,
    TypedMiddleware<AppState, UiPortEventCaptureSetVariables>(
        uiPortEventCaptureSetVariables
    ).call,
    TypedMiddleware<AppState, UiVesselUpdateIndexSetVesselName>(
        uiVesselUpdateIndexSetVesselName).call,
    TypedMiddleware<AppState, UiVesselUpdateIndexSetVoyageNumber>(
        uiVesselUpdateIndexSetVoyageNumber).call,
    TypedMiddleware<AppState, UiVesselUpdateIndexSetVoyageDate>(
        uiVesselUpdateIndexSetVoyageDate).call,
    TypedMiddleware<AppState, UiVesselUpdateIndexSetPortIndex>(
        uiVesselUpdateIndexSetPortIndex).call,
    TypedMiddleware<AppState, UiVesselUpdateIndexSetLocationIndex>(
        uiVesselUpdateIndexSetLocationIndex).call,
    //ROAD MOVEMENTS
    TypedMiddleware<AppState, UiRoadMovementHomeSetPortIndex>(
        uiRoadMovementHomeSetPortIndex).call,
    TypedMiddleware<AppState, UiRoadMovementHomeSetLocationIndex>(
        uiRoadMovementHomeSetLocationIndex).call,
    TypedMiddleware<AppState, UiRoadMovementHomeSetRoleIndex>(
        uiRoadMovementHomeSetRoleIndex).call,
    TypedMiddleware<AppState, UiRoadMovementHomeSetVehicleIndex>(
        uiRoadMovementHomeSetVehicleIndex).call,
    TypedMiddleware<AppState, UiRoadMovementHomeSetVehicleDriverIndex>(
        uiRoadMovementHomeSetVehicleDriverIndex).call,
    TypedMiddleware<AppState, UiRoadMovementHomeSetVehicleOperatorIndex>(
        uiRoadMovementHomeSetVehicleOperatorIndex).call,

    TypedMiddleware<AppState, UiRoadMovementSearchSetWaybill>(
        uiRoadMovementSearchSetWaybill).call,
    TypedMiddleware<AppState, UiRoadMovementSearchSetLegNumber>(
        uiRoadMovementSearchSetLegNumber).call,
    TypedMiddleware<AppState, UiRoadMovementSearchSetEta>(
        uiRoadMovementSearchSetEta).call,

  ];
}

Middleware<AppState> _uiCargoHomeSetPortIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiCargoHomeSetPortIndex) {
      UiCargoHome uiCargoHome = store.state.uiState.uiCargoHome
          .copyWith(selectedPortIndex: action.selectedPortIndex);
      store.dispatch(UiCargoHomeUpdate(uiCargoHome));
    }
    next(action);
  };
}

Middleware<AppState> _uiCargoHomeSetLocationIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiCargoHomeSetLocationIndex) {
      UiCargoHome uiCargoHome = store.state.uiState.uiCargoHome
          .copyWith(selectedLocationIndex: action.selectedLocationIndex);
      store.dispatch(UiCargoHomeUpdate(uiCargoHome));
    }
    next(action);
  };
}

Middleware<AppState> _uiCargoHomeSetCarrierIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiCargoHomeSetCarrierIndex) {
      UiCargoHome uiCargoHome = store.state.uiState.uiCargoHome
          .copyWith(selectedCarrierIndex: action.selectedCarrierIndex);
      store.dispatch(UiCargoHomeUpdate(uiCargoHome));
    }
    next(action);
  };
}

Middleware<AppState> _uiCargoHomeSetRoleIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiCargoHomeSetRoleIndex) {
      UiCargoHome uiCargoHome = store.state.uiState.uiCargoHome
          .copyWith(selectedRoleIndex: action.selectedRoleIndex);
      store.dispatch(UiCargoHomeUpdate(uiCargoHome));
    }
    next(action);
  };
}

Middleware<AppState> _uiCargoHomeFreightSearchSetVesselName() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiCargoHomeFreightSearchSetVesselName) {
      UiCargoFreightSearch uiCargoFreightSearch = store
          .state.uiState.uiCargoFreightSearch
          .copyWith(vesselName: action.vesselName);
      store.dispatch(UiCargoFreightSearchUpdate(uiCargoFreightSearch));
    }
    next(action);
  };
}

Middleware<AppState> _uiCargoHomeFreightSearchSetVoyageNumber() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiCargoHomeFreightSearchSetVoyageNumber) {
      UiCargoFreightSearch uiCargoFreightSearch = store
          .state.uiState.uiCargoFreightSearch
          .copyWith(voyageNumber: action.voyageNumber);
      store.dispatch(UiCargoFreightSearchUpdate(uiCargoFreightSearch));
    }
    next(action);
  };
}

Middleware<AppState> _uiCargoHomeFreightSelectCrane() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiCargoHomeFreightSelectCrane) {
      UiCargoFreightSearch uiCargoFreightSearch = store
          .state.uiState.uiCargoFreightSearch
          .copyWith(selectedCraneIndex: action.craneId);
      store.dispatch(UiCargoFreightSearchUpdate(uiCargoFreightSearch));
    }
    next(action);
  };
}

Middleware<AppState> _uiCargoHomeTruckDetailsSetTruckRegistration() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiCargoHomeTruckDetailsSetTruckRegistration) {
      UiCargoTruckDetails uiCargoTruckDetails = store
          .state.uiState.uiCargoTruckDetails
          .copyWith(truckRegistration: action.truckRegistration);
      store.dispatch(UiCargoTruckDetailsUpdate(uiCargoTruckDetails));
    }
    next(action);
  };
}

Middleware<AppState> _uiCargoHomeTruckDetailsSetTruckDriverId() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiCargoHomeTruckDetailsSetTruckDriverId) {
      UiCargoTruckDetails uiCargoTruckDetails = store
          .state.uiState.uiCargoTruckDetails
          .copyWith(truckDriverId: action.truckDriverId);
      store.dispatch(UiCargoTruckDetailsUpdate(uiCargoTruckDetails));
    }
    next(action);
  };
}

Middleware<AppState> _uiCargoUpdateSearchSetVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiCargoUpdateSearchSetVariables) {
      UiCargoUpdateSearch uiCargoUpdateSearch =
          store.state.uiState.uiCargoUpdateSearch.copyWith(
              searchType: action.searchType,
              containerNumber: action.containerNumber,
              vesselName: action.vesselName,
              voyageNumber: action.voyageNumber,
              jobNumber: action.jobNumber);
      store.dispatch(UiCargoUpdateSearchUpdate(uiCargoUpdateSearch));
    }
    next(action);
  };
}

Middleware<AppState> _uiCargoReportSetReportType() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiCargoReportSetReportType) {
      UiCargoReportSearch uiCargoReportSearch = store
          .state.uiState.uiCargoReportSearch
          .copyWith(reportType: action.reportType);
      store.dispatch(UiCargoReportSearchUpdate(uiCargoReportSearch));
    }
    next(action);
  };
}

Middleware<AppState> _uiCargoReportSetPortIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiCargoReportSetPortIndex) {
      UiCargoReportSearch uiCargoReportSearch = store
          .state.uiState.uiCargoReportSearch
          .copyWith(selectedPortIndex: action.selectedPortIndex);
      store.dispatch(UiCargoReportSearchUpdate(uiCargoReportSearch));
    }
    next(action);
  };
}

Middleware<AppState> _uiCargoReportSetLocationIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiCargoReportSetLocationIndex) {
      UiCargoReportSearch uiCargoReportSearch = store
          .state.uiState.uiCargoReportSearch
          .copyWith(selectedLocationIndex: action.selectedLocationIndex);
      store.dispatch(UiCargoReportSearchUpdate(uiCargoReportSearch));
    }
    next(action);
  };
}

Middleware<AppState> _uiCargoReportSetSailingSchedule() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiCargoReportSetSailingSchedule) {
      UiCargoReportSearch uiCargoReportSearch =
          store.state.uiState.uiCargoReportSearch.copyWith(
              vesselName: action.vesselName, voyageNumber: action.voyageNumber);
      store.dispatch(UiCargoReportSearchUpdate(uiCargoReportSearch));
    }
    next(action);
  };
}

Middleware<AppState> _uiCargoReportSearchReset() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiCargoReportSearchReset) {
      UiCargoReportSearch uiCargoReportSearch = UiCargoReportSearch.initial();
      store.dispatch(UiCargoReportSearchUpdate(uiCargoReportSearch));
    }
    next(action);
  };
}


Middleware<AppState> _uiPortCargoUpdateSetPortIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiPortCargoUpdateSetPortIndex) {
      UiPortCargoUpdateHome uiPortCargoUpdateHome = store.state.uiState.uiPortCargoUpdateHome.copyWith(selectedPortIndex: action.selectedPortIndex);
      store.dispatch(UiPortCargoUpdateHomeUpdate(uiPortCargoUpdateHome));
    }
    next(action);
  };
}


Middleware<AppState> _uiPortCargoUpdateSetLocationIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiPortCargoUpdateSetLocationIndex) {
      UiPortCargoUpdateHome uiPortCargoUpdateHome = store.state.uiState.uiPortCargoUpdateHome.copyWith(selectedLocationIndex: action.selectedLocationIndex);
      store.dispatch(UiPortCargoUpdateHomeUpdate(uiPortCargoUpdateHome));
    }
    next(action);
  };
}

Middleware<AppState> _uiPortCargoUpdateSetCarrierIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiPortCargoUpdateSetCarrierIndex) {
      UiPortCargoUpdateHome uiPortCargoUpdateHome = store.state.uiState.uiPortCargoUpdateHome.copyWith(selectedCarrierIndex: action.selectedCarrierIndex);
      store.dispatch(UiPortCargoUpdateHomeUpdate(uiPortCargoUpdateHome));
    }
    next(action);
  };
}

Middleware<AppState> _uiPortCargoUpdateSetRoleIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiPortCargoUpdateSetRoleIndex) {
      UiPortCargoUpdateHome uiPortCargoUpdateHome = store.state.uiState.uiPortCargoUpdateHome.copyWith(selectedRoleIndex: action.selectedRoleIndex);
      store.dispatch(UiPortCargoUpdateHomeUpdate(uiPortCargoUpdateHome));
    }
    next(action);
  };
}

Middleware<AppState> _uiPortCargoUpdateFreightSearchSetVesselName() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiPortCargoUpdateFreightSearchSetVesselName) {
      UiPortCargoUpdateFreightSearch uiPortCargoUpdateFreightSearch = store.state.uiState.uiPortCargoUpdateFreightSearch.copyWith(vesselName: action.vesselName);
      store.dispatch(UiPortCargoUpdateFreightSearchUpdate(uiPortCargoUpdateFreightSearch));
    }
    next(action);
  };
}

Middleware<AppState> _uiPortCargoUpdateFreightSearchSetVoyageNumber() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiPortCargoUpdateFreightSearchSetVoyageNumber) {
      UiPortCargoUpdateFreightSearch uiPortCargoUpdateFreightSearch = store.state.uiState.uiPortCargoUpdateFreightSearch.copyWith(voyageNumber: action.voyageNumber);
      store.dispatch(UiPortCargoUpdateFreightSearchUpdate(uiPortCargoUpdateFreightSearch));
    }
    next(action);
  };
}

Middleware<AppState> _uiPortEventIndexSetPortIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiPortEventIndexSetPortIndex) {
      UiPortEventIndex uiPortEventIndex = store
          .state.uiState.uiPortEventIndex
          .copyWith(selectedPortIndex: action.selectedPortIndex);
      store.dispatch(UiPortEventIndexUpdate(uiPortEventIndex));
    }
    next(action);
  };
}

Middleware<AppState> _uiPortEventIndexSetLocationIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiPortEventIndexSetLocationIndex) {
      UiPortEventIndex uiPortEventIndex = store
          .state.uiState.uiPortEventIndex
          .copyWith(selectedLocationIndex: action.selectedLocationIndex);
      store.dispatch(UiPortEventIndexUpdate(uiPortEventIndex));
    }
    next(action);
  };
}

Middleware<AppState> _uiPortEventIndexSetCarrierIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiPortEventIndexSetCarrierIndex) {
      UiPortEventIndex uiPortEventIndex = store
          .state.uiState.uiPortEventIndex
          .copyWith(selectedCarrierIndex: action.selectedCarrierIndex);
      store.dispatch(UiPortEventIndexUpdate(uiPortEventIndex));
    }
    next(action);
  };
}

Middleware<AppState> _uiPortEventIndexSetRoleIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiPortEventIndexSetRoleIndex) {
      UiPortEventIndex uiPortEventIndex = store
          .state.uiState.uiPortEventIndex
          .copyWith(selectedRoleIndex: action.selectedRoleIndex);
      store.dispatch(UiPortEventIndexUpdate(uiPortEventIndex));
    }
    next(action);
  };
}

Middleware<AppState> _uiPortEventCaptureSetVariables() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiPortEventCaptureSetVariables) {
      UiPortEventCapture uiPortEventCapture = store
          .state.uiState.uiPortEventCapture
          .copyWith(
              vesselName: action.vesselName,
              voyageNumber: action.voyageNumber,
              berthIndex: action.berthIndex,
          );
      store.dispatch(UiPortEventCaptureUpdate(uiPortEventCapture));
    }
    next(action);
  };
}

Middleware<AppState> _uiVesselUpdateIndexSetVesselName() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiVesselUpdateIndexSetVesselName) {
      UiVesselUpdateIndex uiVesselUpdateIndex = store
          .state.uiState.uiVesselUpdateIndex
          .copyWith(vesselName: action.vesselName);
      store.dispatch(UiVesselUpdateIndexUpdate(uiVesselUpdateIndex));
    }
    next(action);
  };
}

Middleware<AppState> _uiVesselUpdateIndexSetVoyageNumber() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiVesselUpdateIndexSetVoyageNumber) {
      UiVesselUpdateIndex uiVesselUpdateIndex = store
          .state.uiState.uiVesselUpdateIndex
          .copyWith(voyageNumber: action.voyageNumber);
      store.dispatch(UiVesselUpdateIndexUpdate(uiVesselUpdateIndex));
    }
    next(action);
  };
}

Middleware<AppState> _uiVesselUpdateIndexSetVoyageDate() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiVesselUpdateIndexSetVoyageDate) {
      UiVesselUpdateIndex uiVesselUpdateIndex = store
          .state.uiState.uiVesselUpdateIndex
          .copyWith(voyageDate: action.voyageDate);
      store.dispatch(UiVesselUpdateIndexUpdate(uiVesselUpdateIndex));
    }
    next(action);
  };
}

Middleware<AppState> _uiVesselUpdateIndexSetPortIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiVesselUpdateIndexSetPortIndex) {
      UiVesselUpdateIndex uiVesselUpdateIndex = store
          .state.uiState.uiVesselUpdateIndex
          .copyWith(selectedPortIndex: action.selectedPortIndex);
      store.dispatch(UiVesselUpdateIndexUpdate(uiVesselUpdateIndex));
    }
    next(action);
  };
}

Middleware<AppState> _uiVesselUpdateIndexSetLocationIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiVesselUpdateIndexSetLocationIndex) {
      UiVesselUpdateIndex uiVesselUpdateIndex = store
          .state.uiState.uiVesselUpdateIndex
          .copyWith(selectedLocationIndex: action.selectedLocationIndex);
      store.dispatch(UiVesselUpdateIndexUpdate(uiVesselUpdateIndex));
    }
    next(action);
  };
}

Middleware<AppState> _uiRoadMovementHomeSetPortIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiRoadMovementHomeSetPortIndex) {
      UiRoadMovementHome uiRoadMovementHome = store
          .state.uiState.uiRoadMovementHome
          .copyWith(selectedPortIndex: action.selectedPortIndex);
      store.dispatch(SetUiRoadMovementHome(uiRoadMovementHome));
    }
    next(action);
  };
}

Middleware<AppState> _uiRoadMovementHomeSetLocationIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiRoadMovementHomeSetLocationIndex) {
      UiRoadMovementHome uiRoadMovementHome = store
          .state.uiState.uiRoadMovementHome
          .copyWith(selectedLocationIndex: action.selectedLocationIndex);
      store.dispatch(SetUiRoadMovementHome(uiRoadMovementHome));
    }
    next(action);
  };
}

Middleware<AppState> _uiRoadMovementHomeSetRoleIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiRoadMovementHomeSetRoleIndex) {
      UiRoadMovementHome uiRoadMovementHome = store
          .state.uiState.uiRoadMovementHome
          .copyWith(selectedRoleIndex: action.selectedRoleIndex);
      store.dispatch(SetUiRoadMovementHome(uiRoadMovementHome));
    }
    next(action);
  };
}

Middleware<AppState> _uiRoadMovementHomeSetVehicleIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiRoadMovementHomeSetVehicleIndex) {
      UiRoadMovementHome uiRoadMovementHome = store
          .state.uiState.uiRoadMovementHome
          .copyWith(selectedVehicleIndex: action.selectedVehicleIndex);
      store.dispatch(SetUiRoadMovementHome(uiRoadMovementHome));
    }
    next(action);
  };
}

Middleware<AppState> _uiRoadMovementHomeSetVehicleDriverIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiRoadMovementHomeSetVehicleDriverIndex) {
      UiRoadMovementHome uiRoadMovementHome = store
          .state.uiState.uiRoadMovementHome
          .copyWith(selectedVehicleDriverIndex: action.selectedVehicleDriverIndex);
      store.dispatch(SetUiRoadMovementHome(uiRoadMovementHome));
    }
    next(action);
  };
}

Middleware<AppState> _uiRoadMovementHomeSetVehicleOperatorIndex() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiRoadMovementHomeSetVehicleOperatorIndex) {
      UiRoadMovementHome uiRoadMovementHome = store
          .state.uiState.uiRoadMovementHome
          .copyWith(selectedVehicleOperatorIndex: action.selectedVehicleOperatorIndex);
      store.dispatch(SetUiRoadMovementHome(uiRoadMovementHome));
    }
    next(action);
  };
}

Middleware<AppState> _uiRoadMovementSearchSetWaybill() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiRoadMovementSearchSetWaybill) {
      UiRoadMovementSearch uiRoadMovementSearch = store
          .state.uiState.uiRoadMovementSearch
          .copyWith(waybill: action.waybill);
      store.dispatch(SetUiRoadMovementSearch(uiRoadMovementSearch));
    }
    next(action);
  };
}

Middleware<AppState> _uiRoadMovementSearchSetLegNumber() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiRoadMovementSearchSetLegNumber) {
      UiRoadMovementSearch uiRoadMovementSearch = store
          .state.uiState.uiRoadMovementSearch
          .copyWith(legNumber: action.legNumber);
      store.dispatch(SetUiRoadMovementSearch(uiRoadMovementSearch));
    }
    next(action);
  };
}

Middleware<AppState> _uiRoadMovementSearchSetEta() {
  return (Store store, action, NextDispatcher next) async {
    if (action is UiRoadMovementSearchSetEta) {
      UiRoadMovementSearch uiRoadMovementSearch = store
          .state.uiState.uiRoadMovementSearch
          .copyWith(eta: action.eta);
      store.dispatch(SetUiRoadMovementSearch(uiRoadMovementSearch));
    }
    next(action);
  };
}

