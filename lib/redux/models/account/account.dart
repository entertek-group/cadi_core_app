import '../list_converters.dart';

class Account {
  final int id;
  final String name;
  final List<String> roles;
  final List<String> activeServices;
  //final AccountData accountData;

  Account(
      {required this.id,
      required this.name,
      required this.roles,
      required this.activeServices
      //required this.accountData
      });

  factory Account.initial() {
    return Account(
        id: 0,
        name: "",
        roles: [""],
        activeServices: [""]
        // accountData: AccountData.initial()
        );
  }

  Account copyWith(
      {int? id,
      String? name,
      List<String>? roles,
      List<String>? activeServices
      //AccountData? accountData
      }) {
    return Account(
      id: id ?? this.id,
      name: name ?? this.name,
      roles: roles ?? this.roles,
      activeServices: activeServices ?? this.activeServices,
      //accountData: accountData ?? this.accountData
    );
  }

  Account.fromJson(dynamic json)
      : id = json['accountId'] as int,
        name = json['accountName'],
        roles = json["roles"] != null
            ? convertedActiveServices(json["roles"])
            : [""],
        activeServices = json["serviceKeys"] != null
            ? convertedActiveServices(json["serviceKeys"])
            : [""];

  Map toJson() => {
        'accountId': id,
        'accountName': name,
        'roles': roles,
        'serviceKeys': activeServices
      };
}
