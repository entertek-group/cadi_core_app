class AccountBerth {
  final int id;
  final int accountId;
  final int portId;
  final String name;
  final String providerReference;
  final String unLoCode;
  final bool active;

  AccountBerth(
      {
        required this.id,
        required this.accountId,
        required this.portId,
        required this.name,
        required this.providerReference,
        required this.unLoCode,
        required this.active,
      });

  factory AccountBerth.initial() {
    return AccountBerth(
        id: 0,
        accountId: 0,
        portId: 0,
        name: "",
        providerReference: "",
        unLoCode: "",
        active: false);
  }

  AccountBerth copyWith(
      {
        int? id,
        int? accountId,
        int? portId,
        String? name,
        String? providerReference,
        String? unLoCode,
        bool? active}) {
    return AccountBerth(
        id: id ?? this.id,
        accountId: accountId ?? this.accountId,
        portId: portId ?? this.portId,
        name: name ?? this.name,
        providerReference: providerReference ?? this.providerReference,
        unLoCode: unLoCode ?? this.unLoCode,
        active: active ?? this.active);
  }

  AccountBerth.fromJson(Map json)
      : id = json['id'] as int,
        accountId = json['accountId'] as int,
        portId = json['portId'] as int,
        name = json['name'] ?? "",
        providerReference = json['providerReference'] ?? "",
        unLoCode = json['unLoCode'] ?? "",
        active = json['isActive'] as bool;
  Map toJson() => {
        'id': id,
        'accountId': accountId,
        'portId': portId,
        'name': name,
        'providerReference': providerReference,
        'unLoCode': unLoCode,
        'isActive': active
      };
}
