class AccountCarrier {
  final int id;
  final int accountId;
  final int carrierId;
  final String carrierName;
  final bool active;

  AccountCarrier(
      {
        required this.id,
        required this.accountId,
        required this.carrierId,
        required this.carrierName,
        required this.active
      });

  factory AccountCarrier.initial() {
    return AccountCarrier(
        id: 0,
        accountId: 0,
        carrierId: 0,
        carrierName: "",
        active: false);
  }

  AccountCarrier copyWith(
      {int? id,
      int? accountId,
      int? carrierId,
      String? carrierName,
      bool? active}) {
    return AccountCarrier(
        id: id ?? this.id,
        accountId: accountId ?? this.accountId,
        carrierId: carrierId ?? this.carrierId,
        carrierName: carrierName ?? this.carrierName,
        active: active ?? this.active);
  }

  AccountCarrier.fromJson(Map json)
      : id = json['id'] as int,
        accountId = json['accountId'] as int,
        carrierId = json['carrierId'] as int,
        carrierName = json['carrierName'] ?? "",
        active = json['isActive'] as bool;
  Map toJson() => {
        'id': id,
        'accountId': accountId,
        'carrierId': carrierId,
        'carrierName': carrierName,
        'isActive': active
      };
}
