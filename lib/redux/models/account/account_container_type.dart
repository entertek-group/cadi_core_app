class AccountContainerType {
  final int id;
  final int accountId;
  final int containerTypeId;
  final int sortOrder;
  final String code;
  final String description;
  final bool active;

  AccountContainerType({
    required this.id,
    required this.accountId,
    required this.containerTypeId,
    required this.sortOrder,
    required this.code,
    required this.description,
    required this.active
  });

  factory AccountContainerType.initial() {
    return AccountContainerType(
      id: 0,
      accountId: 0,
      containerTypeId: 0,
      sortOrder: 0,
      code: "",
      description: "",
      active: false,
    );
  }

  AccountContainerType copyWith({
    int? id,
    int? accountId,
    int? containerTypeId,
    int? sortOrder,
    String? code,
    String? description,
    bool? active
  }) {
    return AccountContainerType(
      id: id ?? this.id,
      accountId: accountId ?? this.accountId,
      containerTypeId: containerTypeId ?? this.containerTypeId,
      sortOrder: sortOrder ?? this.sortOrder,
      code: code ?? this.code,
      description: description ?? this.description,
      active: active ?? this.active
    );
  }

  AccountContainerType.fromJson(Map json)
      : id = json['id'] as int,
        accountId = json['accountId'] as int,
        containerTypeId = json['containerTypeId'] as int,
        sortOrder = json['sortOrder'] as int,
        code = json['code'] ?? "",
        description = json['description'] ?? "",
        active = json['isActive'] as bool
  ;
  Map toJson() => {
        'id': id,
        'accountId': accountId,
        'containerTypeId': containerTypeId,
        'sortOrder': sortOrder,
        'code': code,
        'description': description,
        'isActive': active,
      };
}