class AccountCrane {
  final int id;
  final int accountId;
  final int portId;
  final String name;
  final String providerReference;
  final String unLoCode;
  final bool active;

  AccountCrane(
      {
        required this.id,
        required this.accountId,
        required this.portId,
        required this.name,
        required this.providerReference,
        required this.unLoCode,
        required this.active,
      });

  factory AccountCrane.initial() {
    return AccountCrane(
        id: 0,
        accountId: 0,
        portId: 0,
        name: "",
        providerReference: "",
        unLoCode: "",
        active: false);
  }

  AccountCrane copyWith(
      {
        int? id,
        int? accountId,
        int? portId,
        String? name,
        String? providerReference,
        String? unLoCode,
        bool? active}) {
    return AccountCrane(
        id: id ?? this.id,
        accountId: accountId ?? this.accountId,
        portId: portId ?? this.portId,
        name: name ?? this.name,
        providerReference: providerReference ?? this.providerReference,
        unLoCode: unLoCode ?? this.unLoCode,
        active: active ?? this.active);
  }

  AccountCrane.fromJson(Map json)
      : id = json['id'] as int,
        accountId = json['accountId'] as int,
        portId = json['portId'] as int,
        name = json['name'] ?? "",
        providerReference = json['providerReference'] ?? "",
        unLoCode = json['unLoCode'] ?? "",
        active = json['isActive'] as bool;
  Map toJson() => {
        'id': id,
        'accountId': accountId,
        'portId': portId,
        'name': name,
        'providerReference': providerReference,
        'unLoCode': unLoCode,
        'isActive': active
      };
}
