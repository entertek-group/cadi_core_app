class AccountData {
  final String organisationNo;

  AccountData({required this.organisationNo});

  factory AccountData.initial() {
    return AccountData(organisationNo: "");
  }

  AccountData copyWith({String? organisationNo}) {
    return AccountData(organisationNo: organisationNo ?? this.organisationNo);
  }

  AccountData.fromJson(Map json) : organisationNo = json['organisation_no'];

  Map toJson() => {'organisation_no': organisationNo};
}
