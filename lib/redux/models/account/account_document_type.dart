class AccountDocumentType {
  final int id;
  final int accountId;
  final int accountServiceId;
  final String code;
  final String description;
  final String serviceKey;

  AccountDocumentType({
    required this.id,
    required this.accountId,
    required this.accountServiceId,
    required this.code,
    required this.description,
    required this.serviceKey,
  });

  factory AccountDocumentType.initial() {
    return AccountDocumentType(
      id: 0,
      accountId: 0,
      accountServiceId: 0,
      code: "",
      description: "",
      serviceKey: "",
    );
  }

  AccountDocumentType copyWith(
      {
        int? id,
        int? accountId,
        int? accountServiceId,
        String? code,
        String? description,
        String? serviceKey
      }) {
    return AccountDocumentType(
      id: id ?? this.id,
      accountId: accountId ?? this.accountId,
      accountServiceId: accountServiceId ?? this.accountServiceId,
      code: code ?? this.code,
      description: description ?? this.description,
      serviceKey: serviceKey ?? this.serviceKey,
    );
  }

  AccountDocumentType.fromJson(Map json)
      : id = json['id'] as int,
        accountId = json['accountId'] as int,
        accountServiceId = json['accountServiceId'] as int,
        code = json['code'] ?? "",
        description = json['description'] ?? "",
        serviceKey = json['serviceKey'] ?? "";
  Map toJson() => {
        'id': id,
        'accountId': accountId,
        'accountServiceId': accountServiceId,
        'code': code,
        'description': description,
        'serviceKey': serviceKey,
      };
}
