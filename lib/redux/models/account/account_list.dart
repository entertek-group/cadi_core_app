import 'account.dart';

class AccountList {
  List<Account> accounts;

  AccountList({
    required this.accounts,
  });

  factory AccountList.initial() {
    List<Account> accounts = [];
    accounts.add(Account.initial());
    return AccountList(accounts: accounts);
  }

  AccountList copyWith({List<Account>? accounts}) {
    return AccountList(accounts: accounts ?? this.accounts);
  }

  factory AccountList.fromJson(List<dynamic> parsedJson) {
    List<Account> accounts = <Account>[];
    accounts = parsedJson
        .map((i) => Account.fromJson(i as Map<String, dynamic>))
        .toList();
    return AccountList(accounts: accounts);
  }

  dynamic toJson() => {'accounts': accounts};
}
