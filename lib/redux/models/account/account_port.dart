class AccountPort {
  final int portId;
  final int accountId;
  final String unLocode;
  final int uniqueId;
  final int accountPortDepotId;

  AccountPort(
      {required this.portId,
      required this.unLocode,
      required this.accountId,
      required this.uniqueId,
      required this.accountPortDepotId});

  factory AccountPort.initial() {
    return AccountPort(
        portId: 0,
        unLocode: "",
        accountId: 0,
        uniqueId: 0,
        accountPortDepotId: 0);
  }

  AccountPort copyWith(
      {int? portId,
      String? unLocode,
      int? accountId,
      int? uniqueId,
      int? accountPortDepotId}) {
    return AccountPort(
        portId: portId ?? this.portId,
        unLocode: unLocode ?? this.unLocode,
        accountId: accountId ?? this.accountId,
        uniqueId: uniqueId ?? this.uniqueId,
        accountPortDepotId: accountPortDepotId ?? this.accountPortDepotId);
  }

  AccountPort.fromJson(Map json)
      : portId = json['portId'] as int,
        unLocode = json['unLoCode'] ?? "",
        accountId = json['accountId'] as int,
        uniqueId = json['unique_id'] as int,
        accountPortDepotId = json['id'] as int;

  Map toJson() => {
        'portId': portId,
        'unLoCode': unLocode,
        'accountId': accountId,
        'unique_id': uniqueId,
        'id': accountPortDepotId
      };
}
