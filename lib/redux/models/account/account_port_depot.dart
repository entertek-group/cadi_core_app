class AccountPortDepot {
  final int id;
  final int accountId;
  final int portId;
  final String depotName;
  final String addressType;
  final String unLocode;
  final bool active;
  final String createdAt;
  final String updatedAt;

  AccountPortDepot({
    required this.id,
    required this.accountId,
    required this.portId,
    required this.depotName,
    required this.addressType,
    required this.unLocode,
    required this.active,
    required this.createdAt,
    required this.updatedAt,
  });

  factory AccountPortDepot.initial() {
    return AccountPortDepot(
      id: 0,
      accountId: 0,
      portId: 0,
      depotName: "",
      addressType: "",
      unLocode: "",
      active: false,
      createdAt: "",
      updatedAt: "",
    );
  }

  AccountPortDepot copyWith({
    int? id,
    int? accountId,
    int? portId,
    String? depotName,
    String? addressType,
    String? unLocode,
    bool? active,
    String? createdAt,
    String? updatedAt,
  }) {
    return AccountPortDepot(
      id: id ?? this.id,
      accountId: accountId ?? this.accountId,
      portId: portId ?? this.portId,
      depotName: depotName ?? this.depotName,
      addressType: addressType ?? this.addressType,
      unLocode: unLocode ?? this.unLocode,
      active: active ?? this.active,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  AccountPortDepot.fromJson(Map json)
      : id = json['id'] as int,
        accountId = json['accountId'] as int,
        portId = json['portId'] as int,
        depotName = json['name'] ?? "",
        addressType = json['addressType'] ?? "",
        active = json['isActive'] as bool,
        unLocode = json['unLoCode'] ?? "",
        createdAt = json['createdAt'] ?? "",
        updatedAt = json['updatedAt'] ?? "";

  Map toJson() => {
        'id': id,
        'accountId': accountId,
        'portId': portId,
        'name': depotName,
        'addressType': addressType,
        'unLoCode': unLocode,
        'isActive': active,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
      };
}
