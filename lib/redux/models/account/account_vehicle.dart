class AccountVehicle {
  final int id;
  final int accountId;
  final String make;
  final String model;
  final String registrationPlate;
  final String vinNumber;
  final String year;
  final String color;

  AccountVehicle({
    required this.id,
    required this.accountId,
    required this.make,
    required this.model,
    required this.registrationPlate,
    required this.vinNumber,
    required this.year,
    required this.color
  });

  factory AccountVehicle.initial() {
    return AccountVehicle(
      id: 0,
      accountId: 0,
      make: "",
      model: "",
      registrationPlate: "",
      vinNumber: "",
      year: "",
      color: ""
    );
  }

  AccountVehicle copyWith(
      {
        int? id,
        int? accountId,
        String? make,
        String? model,
        String? registrationPlate,
        String? vinNumber,
        String? year,
        String? color
      }) {
    return AccountVehicle(
      id: id ?? this.id,
      accountId: accountId ?? this.accountId,
      make: make ?? this.make,
      model: model ?? this.model,
      registrationPlate: registrationPlate ?? this.registrationPlate,
      vinNumber: vinNumber ?? this.vinNumber,
      year: year ?? this.year,
      color: color ?? this.color
    );
  }

  AccountVehicle.fromJson(Map json)
      : id = json['id'] as int,
        accountId = json['accountId'] as int,
        make = json['make'] ?? "",
        model = json['model'] ?? "",
        registrationPlate = json['registrationPlate'] ?? "",
        vinNumber = json['vinNumber'] ?? "",
        year = json['year'] ?? "",
        color = json['color'] ?? ""

  ;
  Map toJson() => {
        'id': id,
        'accountId': accountId,
        'make': make,
        'model': model,
        'registrationPlate': registrationPlate,
        'vinNumber': vinNumber,
        'year': year,
        'color': color
      };
}
