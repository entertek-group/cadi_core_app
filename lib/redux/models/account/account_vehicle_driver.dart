class AccountVehicleDriver {
  final int id;
  final int accountId;
  final String firstName;
  final String lastName;
  final String fullName;

  AccountVehicleDriver({
    required this.id,
    required this.accountId,
    required this.firstName,
    required this.lastName,
    required this.fullName,
  });

  factory AccountVehicleDriver.initial() {
    return AccountVehicleDriver(
      id: 0,
      accountId: 0,
      firstName: "",
      lastName: "",
      fullName: "",
    );
  }

  AccountVehicleDriver copyWith(
      {
        int? id,
        int? accountId,
        String? firstName,
        String? lastName,
        String? fullName
      }) {
    return AccountVehicleDriver(
      id: id ?? this.id,
      accountId: accountId ?? this.accountId,
      firstName: firstName ?? this.firstName,
      lastName: lastName ?? this.lastName,
      fullName: fullName ?? this.fullName,
    );
  }

  AccountVehicleDriver.fromJson(Map json)
      : id = json['id'] as int,
        accountId = json['accountId'] as int,
        firstName = json['firstName'] ?? "",
        lastName = json['lastName'] ?? "",
        fullName = json['fullName'] ?? "";
  Map toJson() => {
        'id': id,
        'accountId': accountId,
        'firstName': firstName,
        'lastName': lastName,
        'fullName': fullName,
      };
}
