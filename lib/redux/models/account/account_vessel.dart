class AccountVessel {
  final int id;
  final int accountId;
  final int vesselId;
  final String vesselName;
  final bool active;

  AccountVessel(
      {
        required this.id,
        required this.accountId,
        required this.vesselId,
        required this.vesselName,
        required this.active
      });

  factory AccountVessel.initial() {
    return AccountVessel(
        id: 0,
        accountId: 0,
        vesselId: 0,
        vesselName: "",
        active: false);
  }

  AccountVessel copyWith(
      {
        int? id,
        int? accountId,
        int? vesselId,
        String? vesselName,
        bool? active
      }) {
    return AccountVessel(
        id: id ?? this.id,
        accountId: accountId ?? this.accountId,
        vesselId: vesselId ?? this.vesselId,
        vesselName: vesselName ?? this.vesselName,
        active: active ?? this.active);
  }

  AccountVessel.fromJson(Map json)
      : id = json['id'] as int,
        accountId = json['accountId'] as int,
        vesselId = json['vesselId'] as int,
        vesselName = json['vesselName'] ?? "",
        active = json['isActive'] as bool;
  Map toJson() => {
        'id': id,
        'accountId': accountId,
        'vesselId': vesselId,
        'vesselName': vesselName,
        'isActive': active
      };
}
