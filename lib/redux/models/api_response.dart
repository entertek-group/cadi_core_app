class ApiResponse {
  final bool success;
  final String result;

  ApiResponse({required this.success, required this.result});

  factory ApiResponse.initial() {
    return ApiResponse(success: false, result: "");
  }

  ApiResponse copyWith({bool? success, String? result}) {
    return ApiResponse(
        success: success ?? this.success, result: result ?? this.result);
  }

  ApiResponse.fromJson(Map json)
      : success = json['success'] as bool,
        result = json['result'];

  Map toJson() => {'success': success, 'result': result};
}
