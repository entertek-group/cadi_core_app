class ContainerData {
  final String sealNumber1;
  final String sealNumber2;
  final String sealNumber3;
  final String packingMode;
  final String containerCode;
  final String containerTransportMode;
  final String containerMode;
  final String containerNumber;
  final num containerWeight;
  final String containerWeightUnit;
  final num containerVolume;
  final String containerVolumeUnit;
  final String containerCommodityCode;


  ContainerData({
      required this.sealNumber1,
      required this.sealNumber2,
      required this.sealNumber3,
      required this.packingMode,
      required this.containerCode,
      required this.containerTransportMode,
      required this.containerMode,
      required this.containerNumber,
      required this.containerWeight,
      required this.containerWeightUnit,
      required this.containerVolume,
      required this.containerVolumeUnit,
      required this.containerCommodityCode
  });

  factory ContainerData.initial() {
    return ContainerData(
        sealNumber1: "",
        sealNumber2: "",
        sealNumber3: "",
        packingMode: "20GP",
        containerCode: "",
        containerTransportMode: "Sea",
        containerMode: "",
        containerNumber: "",
        containerWeight: 0.0,
        containerWeightUnit: "kg",
        containerVolume: 0.0,
        containerVolumeUnit: "m3",
        containerCommodityCode: "GEN"
    );
  }

  ContainerData copyWith({
      String? sealNumber1,
      String? sealNumber2,
      String? sealNumber3,
      String? packingMode,
      String? containerCode,
      String? containerTransportMode,
      String? containerMode,
      String? containerNumber,
      num? containerWeight,
      String? containerWeightUnit,
      num? containerVolume,
      String? containerVolumeUnit,
      String? containerCommodityCode
  }) {
    return ContainerData(
      sealNumber1: sealNumber1 ?? this.sealNumber1,
      sealNumber2: sealNumber2 ?? this.sealNumber2,
      sealNumber3: sealNumber3 ?? this.sealNumber3,
      packingMode: packingMode ?? this.packingMode,
      containerCode: containerCode ?? this.containerCode,
      containerTransportMode:
          containerTransportMode ?? this.containerTransportMode,
      containerMode: containerMode ?? this.containerMode,
      containerNumber: containerNumber ?? this.containerNumber,
      containerWeight: containerWeight ?? this.containerWeight,
      containerWeightUnit: containerWeightUnit ?? this.containerWeightUnit,
      containerVolume: containerVolume ?? this.containerVolume,
      containerVolumeUnit: containerVolumeUnit ?? this.containerVolumeUnit,
      containerCommodityCode:
          containerCommodityCode ?? this.containerCommodityCode
    );
  }

  ContainerData.fromJson(Map json)
      :
        sealNumber1 = json['sealNumber1'] ?? "",
        sealNumber2 = json['sealNumber2'] ?? "",
        sealNumber3 = json['sealNumber3'] ?? "",
        containerCode = json['containerCode'] ?? "",
        containerMode = json['containerMode'] ?? "",
        containerNumber = json['containerNumber'] ?? "",
        containerTransportMode = json['transportMode'] ?? "",
        containerWeight = json['weight'] as dynamic,
        containerWeightUnit = json['weightUnit'] ?? "",
        containerVolume = json['volume'] as dynamic,
        containerVolumeUnit = json['volumeUnit'] ?? "",
        packingMode = json['packingMode'] ?? "",
        containerCommodityCode = json['commodityCode'] ?? "";

  Map toJson() => {
        'sealNumber1': sealNumber1,
        'sealNumber2': sealNumber2,
        'sealNumber3': sealNumber3,
        'containerCode': containerCode,
        'containerMode': containerMode,
        'containerNumber': containerNumber,
        'transportMode': containerTransportMode,
        'weight': containerWeight,
        'weightUnit': containerWeightUnit,
        'volume': containerVolume,
        'volumeUnit': containerVolumeUnit,
        'packingMode': packingMode,
        'commodityCode': containerCommodityCode
      };
}