class PackData {

  final String packSequenceNumber;
  final num packUnits;
  final String transportMode;
  final num packWeight;
  final String packWeightUnit;
  final num packVolume;
  final String packVolumeUnit;
  final String commodityCode;
  final String packingMode;

  PackData({
      required this.packSequenceNumber,
      required this.packUnits,
      required this.transportMode,
      required this.packWeight,
      required this.packWeightUnit,
      required this.packVolume,
      required this.packVolumeUnit,
      required this.commodityCode,
      required this.packingMode
  });

  factory PackData.initial() {
    return PackData(
        packSequenceNumber: "",
        packUnits: 0.0,
        transportMode: "Sea",
        packWeight: 0.0,
        packWeightUnit: "kg",
        packVolume: 0.0,
        packVolumeUnit: "m3",
        packingMode: "PLT",
        commodityCode: "GEN",
    );
  }

  PackData copyWith(
      {
        String? packSequenceNumber,
        num? packUnits,
        String? transportMode,
        num? packWeight,
        String? packWeightUnit,
        num? packVolume,
        String? packVolumeUnit,
        String? packingMode,
        String? commodityCode
      }) {
    return PackData(
      packSequenceNumber: packSequenceNumber ?? this.packSequenceNumber,
      packUnits: packUnits ?? this.packUnits,
      transportMode: transportMode ?? this.transportMode,
      packWeight: packWeight ?? this.packWeight,
      packWeightUnit: packWeightUnit ?? this.packWeightUnit,
      packVolume: packVolume ?? this.packVolume,
      packVolumeUnit: packVolumeUnit ?? this.packVolumeUnit,
      packingMode: packingMode ?? this.packingMode,
      commodityCode: commodityCode ?? this.commodityCode,
    );
  }

  PackData.fromJson(Map json)
      :
        packSequenceNumber = json['packSequenceNumber'] ?? "",
        packUnits = json['units'] as num,
        transportMode = json['transportMode'],
        packWeight = json['weight'] as num,
        packWeightUnit = json['weightUnit'] ?? "",
        packVolume = json['volume'] as num,
        packVolumeUnit = json['volumeUnit'] ?? "",
        packingMode = json['packingMode'] ?? "",
        commodityCode = json['commodityCode'] ?? "";

  Map toJson() => {
        'packSequenceNumber': packSequenceNumber,
        'units': packUnits,
        'transportMode': transportMode,
        'weight': packWeight,
        'weightUnit': packWeightUnit,
        'volume': packVolume,
        'volumeUnit': packVolumeUnit,
        'packingMode': packingMode,
        'commodityCode': commodityCode
      };
}
