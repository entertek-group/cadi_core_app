import 'cargo_movement_create.dart';

class CargoCreateQueue {
  final String uniqueId;
  final CargoMovementCreate cargoMovement;
  final bool complete;
  final String createdAt;
  final String error;

  CargoCreateQueue(
      {required this.uniqueId,
      required this.cargoMovement,
      required this.complete,
      required this.createdAt,
      required this.error});

  factory CargoCreateQueue.initial() {
    return CargoCreateQueue(
        uniqueId: "",
        cargoMovement: CargoMovementCreate.initial(),
        complete: true,
        createdAt: DateTime.now().toString(),
        error: "");
  }

  CargoCreateQueue copyWith(
      {String? uniqueId,
      CargoMovementCreate? cargoMovement,
      bool? complete,
      String? createdAt,
      String? error}) {
    return CargoCreateQueue(
        uniqueId: uniqueId ?? this.uniqueId,
        cargoMovement: cargoMovement ?? this.cargoMovement,
        complete: complete ?? this.complete,
        createdAt: createdAt ?? this.createdAt,
        error: error ?? this.error);
  }

  CargoCreateQueue.fromJson(Map json)
      : uniqueId = json['unique_id'],
        cargoMovement = json['cargo_movement_create'] != null
            ? CargoMovementCreate.fromJson(json['cargo_movement_create'])
            : CargoMovementCreate.initial(),
        complete = json['complete'] as bool,
        createdAt = json['createdAt'],
        error = json['error'];

  Map toJson() => {
        'unique_id': uniqueId,
        'cargo_movement_create': cargoMovement,
        'complete': complete,
        'createdAt': createdAt,
        'error': error
      };
}
