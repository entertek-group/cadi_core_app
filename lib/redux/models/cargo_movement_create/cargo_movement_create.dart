import 'package:cadi_core_app/redux/models/cargo_data/container_data.dart';
import 'package:cadi_core_app/redux/models/cargo_data/pack_data.dart';
import 'package:cadi_core_app/redux/models/image/image_create.dart';

import '../list_converters.dart';

class CargoMovementCreate {
  final int accountId;
  final int applicationUserId;
  final int accountPortLocationId;
  final int? accountPortCraneId;
  final int? portId; //tracking portId for crane selection
  final int carrierId;
  final int? vesselVoyageId;
  final int? sailingJobId;
  final int? sailingJobContainerId;
  final int? sailingJobPackId;
  final String? eir;
  final String? comment;
  final String vesselName;
  final String voyageNumber;
  final String? restowState;
  final String? restowLocation;
  final String? stowagePosition;
  final String? truckRegistration;
  final String? truckCompany;
  final String? truckDriverId;
  final String role;
  final String state;
  final String shipment;
  final ContainerData containerData;
  final PackData packData;
  final String movementAt;
  final List<ImageCreate>? images;
  final ImageCreate? signature;
  final String? signatoryFullName;
  final String? consigneeOrConsignorName;
  final String? deliveryDocketNumber;
  final String? portOfLoadOrDischarge;

  CargoMovementCreate({
    required this.accountId,
    required this.applicationUserId,
    required this.accountPortLocationId,
    this.accountPortCraneId,
    this.portId,
    required this.carrierId,
    this.vesselVoyageId,
    this.sailingJobId,
    this.sailingJobContainerId,
    this.sailingJobPackId,
    this.eir,
    this.comment,
    required this.vesselName,
    required this.voyageNumber,
    this.restowState,
    this.restowLocation,
    this.stowagePosition,
    this.truckRegistration,
    this.truckCompany,
    this.truckDriverId,
    required this.role,
    required this.state,
    required this.shipment,
    required this.containerData,
    required this.packData,
    required this.movementAt,
    this.images,
    this.signature,
    this.signatoryFullName,
    this.consigneeOrConsignorName,
    this.deliveryDocketNumber,
    this.portOfLoadOrDischarge,
  });

  factory CargoMovementCreate.initial() {
    return CargoMovementCreate(
      accountId: 0,
      applicationUserId: 0,
      accountPortLocationId: 0,
      accountPortCraneId: null,
      portId: null,
      carrierId: 0,
      vesselVoyageId: null,
      sailingJobId: null,
      sailingJobContainerId: null,
      sailingJobPackId: null,
      eir: null,
      comment: null,
      vesselName: "",
      voyageNumber: "",
      restowState: null,
      restowLocation: null,
      stowagePosition: null,
      truckRegistration: null,
      truckCompany: null,
      truckDriverId: null,
      role: "",
      state: "",
      shipment: "",
      containerData: ContainerData.initial(),
      packData: PackData.initial(),
      movementAt: "",
      images: null,
      signature: null,
      signatoryFullName: null,
      consigneeOrConsignorName: null,
      deliveryDocketNumber: null,
      portOfLoadOrDischarge: null,
    );
  }

  CargoMovementCreate copyWith({
    int? accountId,
    int? applicationUserId,
    int? accountPortLocationId,
    int? accountPortCraneId,
    int? carrierId,
    int? portId,
    int? vesselVoyageId,
    int? sailingJobId,
    int? sailingJobContainerId,
    int? sailingJobPackId,
    String? eir,
    String? comment,
    String? vesselName,
    String? voyageNumber,
    String? restowState,
    String? restowLocation,
    String? stowagePosition,
    String? truckRegistration,
    String? truckCompany,
    String? truckDriverId,
    String? role,
    String? state,
    String? shipment,
    ContainerData? containerData,
    PackData? packData,
    String? movementAt,
    List<ImageCreate>? images,
    ImageCreate? signature,
    String? signatoryFullName,
    String? consigneeOrConsignorName,
    String? deliveryDocketNumber,
    String? portOfLoadOrDischarge,
  }) {
    return CargoMovementCreate(
      accountId: accountId ?? this.accountId,
      applicationUserId: applicationUserId ?? this.applicationUserId,
      accountPortLocationId:
          accountPortLocationId ?? this.accountPortLocationId,
      accountPortCraneId: accountPortCraneId ?? this.accountPortCraneId,
      carrierId: carrierId ?? this.carrierId,
      portId: portId ?? this.portId,
      vesselVoyageId: vesselVoyageId ?? this.vesselVoyageId,
      sailingJobId: sailingJobId ?? this.sailingJobId,
      sailingJobContainerId:
          sailingJobContainerId ?? this.sailingJobContainerId,
      sailingJobPackId: sailingJobPackId ?? this.sailingJobPackId,
      eir: eir ?? this.eir,
      comment: comment ?? this.comment,
      vesselName: vesselName ?? this.vesselName,
      voyageNumber: voyageNumber ?? this.voyageNumber,
      restowState: restowState ?? this.restowState,
      restowLocation: restowLocation ?? this.restowLocation,
      stowagePosition: stowagePosition ?? this.stowagePosition,
      truckRegistration: truckRegistration ?? this.truckRegistration,
      truckCompany: truckCompany ?? this.truckCompany,
      truckDriverId: truckDriverId ?? this.truckDriverId,
      role: role ?? this.role,
      state: state ?? this.state,
      shipment: shipment ?? this.shipment,
      containerData: containerData ?? this.containerData,
      packData: packData ?? this.packData,
      movementAt: movementAt ?? this.movementAt,
      images: images ?? this.images,
      signature: signature ?? this.signature,
      signatoryFullName: signatoryFullName ?? this.signatoryFullName,
      consigneeOrConsignorName:
          consigneeOrConsignorName ?? this.consigneeOrConsignorName,
      deliveryDocketNumber: deliveryDocketNumber ?? this.deliveryDocketNumber,
      portOfLoadOrDischarge:
          portOfLoadOrDischarge ?? this.portOfLoadOrDischarge,
    );
  }

  CargoMovementCreate.fromJson(Map<String, dynamic> json)
      : accountId = json['accountId'] as int,
        applicationUserId = json['applicationUserId'] as int,
        accountPortLocationId = json['accountPortLocationId'] as int,
        accountPortCraneId = json['accountPortCraneId'] as int?,
        carrierId = json['carrierId'] as int,
        vesselVoyageId = json['vesselVoyageId'] as int?,
        portId = json['portId'] as int?,
        sailingJobId = json['sailingJobId'] as int?,
        sailingJobContainerId = json['sailingJobContainerId'] as int?,
        sailingJobPackId = json['sailingJobPackId'] as int?,
        eir = json['eir'] as String?,
        comment = json['comment'] as String?,
        vesselName = json['vesselName'] as String,
        voyageNumber = json['voyageNumber'] as String,
        restowState = json['restowState'] as String?,
        restowLocation = json['restowLocation'] as String?,
        stowagePosition = json['stowagePosition'] as String?,
        truckRegistration = json['truckRegistration'] as String?,
        truckDriverId = json['truckDriverId'] as String?,
        truckCompany = json['truckCompany'] as String?,
        role = json['role'] as String,
        state = json['state'] as String,
        shipment = json['shipment'] as String,
        containerData = ContainerData.fromJson(json['containerData']),
        packData = PackData.fromJson(json['packData']),
        movementAt = json['movementAt'] as String,
        images = json['images'] != null
            ? converterImageCreate(json['images'])
            : null,
        signature = json['signature'] != null
            ? ImageCreate.fromJson(json['signature'])
            : ImageCreate.initial(),
        signatoryFullName = json['signatoryFullName'] as String?,
        consigneeOrConsignorName = json['consigneeOrConsignorName'] as String?,
        deliveryDocketNumber = json['deliveryDocketNumber'] as String?,
        portOfLoadOrDischarge = json['portOfLoadOrDischarge'] as String?;

  Map<String, dynamic> toJson() => {
        'accountId': accountId,
        'applicationUserId': applicationUserId,
        'accountPortLocationId': accountPortLocationId,
        'accountPortCraneId': accountPortCraneId,
        'carrierId': carrierId,
        'vesselVoyageId': vesselVoyageId,
        'sailingJobId': sailingJobId,
        'portId': portId,
        'sailingJobContainerId': sailingJobContainerId,
        'sailingJobPackId': sailingJobPackId,
        'eir': eir,
        'comment': comment,
        'vesselName': vesselName,
        'voyageNumber': voyageNumber,
        'restowState': restowState,
        'restowLocation': restowLocation,
        'stowagePosition': stowagePosition,
        'truckRegistration': truckRegistration,
        'truckDriverId': truckDriverId,
        'truckCompany': truckCompany,
        'role': role,
        'state': state,
        'shipment': shipment,
        'containerData': containerData.toJson(),
        'packData': packData.toJson(),
        'movementAt': movementAt,
        'images': images?.map((image) => image.toJson()).toList(),
        'signature': signature?.toJson(),
        'signatoryFullName': signatoryFullName,
        'consigneeOrConsignorName': consigneeOrConsignorName,
        'deliveryDocketNumber': deliveryDocketNumber,
        'portOfLoadOrDischarge': portOfLoadOrDischarge,
      };
}
