class CoreApiResponse {
  final String version;
  final int statusCode;
  final bool isError;
  final String message;
  final String? responseException;
  final dynamic result;

  CoreApiResponse({
    required this.version,
    required this.statusCode,
    required this.isError,
    required this.message,
    required this.responseException,
    required this.result
  });

  CoreApiResponse.fromJson(Map json)
      : version = json['version'],
        statusCode = json['statusCode'] as int,
        isError = json['isError'] as bool,
        message = json['message'],
        responseException = json['responseException'],
        result = json['result'] as dynamic;
}
