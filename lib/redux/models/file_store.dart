class FileStore {
  String fileName;
  String filePath;
  String fileType;
  String storageType;
  String uniqueId;
  dynamic apiId;

  FileStore(
      {required this.fileName,
      required this.filePath,
      required this.fileType,
      required this.storageType,
      required this.uniqueId,
      required this.apiId});

  FileStore copyWith(
      {String? fileName,
      String? filePath,
      String? fileType,
      String? storageType,
      String? uniqueId,
      dynamic apiId}) {
    return FileStore(
        fileName: fileName ?? this.fileName,
        filePath: filePath ?? this.filePath,
        fileType: fileType ?? this.fileType,
        storageType: storageType ?? this.storageType,
        uniqueId: uniqueId ?? this.uniqueId,
        apiId: apiId ?? this.apiId);
  }

  FileStore.fromJson(Map<String, dynamic> json)
      : fileName = json['fileName'],
        filePath = json['filePath'],
        fileType = json['fileType'],
        storageType = json['storageType'],
        uniqueId = json['uniqueId'],
        apiId = json['apiId'] as dynamic;

  Map<String, dynamic> toJson() => {
        'fileName': fileName,
        'filePath': filePath,
        'fileType': fileType,
        'storageType': storageType,
        'uniqueId': uniqueId,
        'apiId': apiId
      };
}
