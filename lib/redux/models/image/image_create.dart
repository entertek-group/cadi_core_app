
class ImageCreate {

  final String name;
  final String base64Content;
  final String? kind;

  ImageCreate(
      {
        required this.name,
        required this.base64Content,
        required this.kind
      });

  factory ImageCreate.initial() {
    return ImageCreate(
      name: "",
      base64Content: "",
      kind: null
    );
  }

  ImageCreate copyWith({
    String? name,
    String? base64Content,
    String? kind
  }) {
    return ImageCreate(
        name: name ?? this.name,
        base64Content: base64Content ?? this.base64Content,
        kind: kind ?? this.kind
    );
  }

  ImageCreate.fromJson(Map json)
      : name = json['name'] ?? "",
        base64Content = json['base64Content'] ?? "",
        kind = json['kind'] as dynamic
  ;

  Map toJson() => {
    'name': name,
    'base64Content': base64Content,
    'kind': kind
  };
}
