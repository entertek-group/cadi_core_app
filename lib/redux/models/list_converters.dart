import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/port_event/port_event_create_queue.dart';
import 'package:cadi_core_app/redux/models/port_event/refuel_data.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';

import 'cargo_data/container_data.dart';
import 'image/image_create.dart';

List<AccountPortDepot> converterAccountPortDepots(List<dynamic> parsedJson) {
  List<AccountPortDepot> data;
  data = parsedJson
      .map((i) => AccountPortDepot.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<AccountPort> converterAccountPorts(List<dynamic> parsedJson) {
  List<AccountPort> data;
  data = parsedJson
      .map((i) => AccountPort.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<AccountContainerType> converterAccountContainerTypes(
    List<dynamic> parsedJson) {
  List<AccountContainerType> data;
  data = parsedJson
      .map((i) => AccountContainerType.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<AccountDocumentType> converterAccountDocumentTypes(
    List<dynamic> parsedJson) {
  List<AccountDocumentType> data;
  data = parsedJson
      .map((i) => AccountDocumentType.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<AccountVehicle> converterAccountVehicles(List<dynamic> parsedJson) {
  List<AccountVehicle> data;
  data = parsedJson
      .map((i) => AccountVehicle.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<AccountVehicleDriver> converterAccountVehicleDrivers(
    List<dynamic> parsedJson) {
  List<AccountVehicleDriver> data;
  data = parsedJson
      .map((i) => AccountVehicleDriver.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<AccountCarrier> converterAccountCarriers(List<dynamic> parsedJson) {
  List<AccountCarrier> data;
  data = parsedJson
      .map((i) => AccountCarrier.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<AccountVessel> converterAccountVessels(List<dynamic> parsedJson) {
  List<AccountVessel> data;
  data = parsedJson
      .map((i) => AccountVessel.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<PortReportLine> converterPortReportLines(List<dynamic> parsedJson) {
  List<PortReportLine> data;
  data = parsedJson
      .map((i) => PortReportLine.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<PortMovementReportLine> converterPortMovementReportLines(
    List<dynamic> parsedJson) {
  List<PortMovementReportLine> data;
  data = parsedJson
      .map((i) => PortMovementReportLine.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<ContainerData> converterContainerData(List<dynamic> parsedJson) {
  List<ContainerData> data;
  data = parsedJson
      .map((i) => ContainerData.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<RefuelData> converterRefuelData(List<dynamic> parsedJson) {
  List<RefuelData> data;
  data = parsedJson
      .map((i) => RefuelData.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<ImageCreate> converterImageCreate(List<dynamic> parsedJson) {
  List<ImageCreate> data;
  data = parsedJson
      .map((i) => ImageCreate.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<AccountBerth> converterAccountBerths(List<dynamic> parsedJson) {
  List<AccountBerth> data;
  data = parsedJson
      .map((i) => AccountBerth.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<AccountCrane> converterAccountCranes(List<dynamic> parsedJson) {
  List<AccountCrane> data;
  data = parsedJson
      .map((i) => AccountCrane.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<CadiEnum> converterCadiEnum(List<dynamic> parsedJson) {
  List<CadiEnum> data;
  data = parsedJson
      .map((i) => CadiEnum.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<CadiGeneric> converterCadiGeneric(List<dynamic> parsedJson) {
  List<CadiGeneric> data;
  data = parsedJson
      .map((i) => CadiGeneric.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<FuelType> converterFuelType(List<dynamic> parsedJson) {
  List<FuelType> data;
  data = parsedJson
      .map((i) => FuelType.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<SailingSchedule> converterSailingSchedule(List<dynamic> parsedJson) {
  List<SailingSchedule> data;
  data = parsedJson
      .map((i) => SailingSchedule.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<VesselJourneyCreateQueue> converterVesselJourneyCreateQueue(
    List<dynamic> parsedJson) {
  List<VesselJourneyCreateQueue> data;
  data = parsedJson
      .map((i) => VesselJourneyCreateQueue.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<TruckJourneyCreateQueue> converterTruckJourneyCreateQueue(
    List<dynamic> parsedJson) {
  List<TruckJourneyCreateQueue> data;
  data = parsedJson
      .map((i) => TruckJourneyCreateQueue.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<VesselJourneyUpdateQueue> converterVesselJourneyUpdateQueue(
    List<dynamic> parsedJson) {
  List<VesselJourneyUpdateQueue> data;
  data = parsedJson
      .map((i) => VesselJourneyUpdateQueue.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<TruckJourneyUpdateQueue> converterTruckJourneyUpdateQueue(
    List<dynamic> parsedJson) {
  List<TruckJourneyUpdateQueue> data;
  data = parsedJson
      .map((i) => TruckJourneyUpdateQueue.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<VesselJourney> converterVesselUpdateJourneys(List<dynamic> parsedJson) {
  List<VesselJourney> data;
  data = parsedJson
      .map((i) => VesselJourney.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<TruckJourney> converterTruckUpdateJourneys(List<dynamic> parsedJson) {
  List<TruckJourney> data;
  data = parsedJson
      .map((i) => TruckJourney.fromJson(i as Map<String, dynamic>))
      .toList();
  return data;
}

List<CargoCreateQueue> converterCargoCreateQueue(List<dynamic> parsedJson) {
  List<CargoCreateQueue> cargoQueue;
  cargoQueue = parsedJson
      .map((i) => CargoCreateQueue.fromJson(i as Map<String, dynamic>))
      .toList();
  return cargoQueue;
}

List<PortCargoUpdateQueue> converterPortCargoUpdateQueue(
    List<dynamic> parsedJson) {
  List<PortCargoUpdateQueue> portCargoUpdateQueue;
  portCargoUpdateQueue = parsedJson
      .map((i) => PortCargoUpdateQueue.fromJson(i as Map<String, dynamic>))
      .toList();
  return portCargoUpdateQueue;
}

List<RoadMovementCreateQueue> converterRoadMovementCreateQueue(
    List<dynamic> parsedJson) {
  List<RoadMovementCreateQueue> queue;
  queue = parsedJson
      .map((i) => RoadMovementCreateQueue.fromJson(i as Map<String, dynamic>))
      .toList();
  return queue;
}

List<PortEventCreateQueue> converterPortEventCreateQueue(
    List<dynamic> parsedJson) {
  List<PortEventCreateQueue> queue;
  queue = parsedJson
      .map((i) => PortEventCreateQueue.fromJson(i as Map<String, dynamic>))
      .toList();
  return queue;
}


List<FileStore> converterFileStoreList(List<dynamic> parsedJson) {
  List<FileStore> cargoQueue;
  cargoQueue = parsedJson
      .map((i) => FileStore.fromJson(i as Map<String, dynamic>))
      .toList();
  return cargoQueue;
}

List<String> convertedActiveServices(List<dynamic> data) {
  return data.map(
    (item) {
      return item as String;
    },
  ).toList();
}
