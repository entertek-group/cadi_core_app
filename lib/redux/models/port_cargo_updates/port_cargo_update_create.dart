import 'package:cadi_core_app/redux/models/cargo_data/container_data.dart';
import 'package:cadi_core_app/redux/models/cargo_data/pack_data.dart';

class PortCargoUpdateCreate {
  final int accountId;
  final int applicationUserId;
  final int accountPortLocationId;
  final int carrierId;
  final int? vesselVoyageId;
  final int? sailingJobId;
  final int? sailingJobContainerId;
  final int? sailingJobPackId;
  final String? comment;
  final String vesselName;
  final String voyageNumber;
  final String role;
  final ContainerData containerData;
  final PackData packData;
  final String? billOfLading;
  final String updateAt;
  final String shipment; //not consumed but used for UI capture

  PortCargoUpdateCreate({
    required this.accountId,
    required this.applicationUserId,
    required this.accountPortLocationId,
    required this.carrierId,
    this.vesselVoyageId,
    this.sailingJobId,
    this.sailingJobContainerId,
    this.sailingJobPackId,
    this.comment,
    required this.vesselName,
    required this.voyageNumber,
    required this.role,
    required this.containerData,
    required this.packData,
    this.billOfLading,
    required this.updateAt,
    required this.shipment,
  });

  factory PortCargoUpdateCreate.initial() {
    return PortCargoUpdateCreate(
      accountId: 0,
      applicationUserId: 0,
      accountPortLocationId: 0,
      carrierId: 0,
      vesselVoyageId: null,
      sailingJobId: null,
      sailingJobContainerId: null,
      sailingJobPackId: null,
      comment: null,
      vesselName: "",
      voyageNumber: "",
      role: "",
      containerData: ContainerData.initial(),
      packData: PackData.initial(),
      billOfLading: null,
      updateAt: "",
      shipment: "",
    );
  }

  PortCargoUpdateCreate copyWith({
    int? accountId,
    int? applicationUserId,
    int? accountPortLocationId,
    int? carrierId,
    int? vesselVoyageId,
    int? sailingJobId,
    int? sailingJobContainerId,
    int? sailingJobPackId,
    String? comment,
    String? vesselName,
    String? voyageNumber,
    String? role,
    ContainerData? containerData,
    PackData? packData,
    String? billOfLading,
    String? updateAt,
    String? shipment,
  }) {
    return PortCargoUpdateCreate(
      accountId: accountId ?? this.accountId,
      applicationUserId: applicationUserId ?? this.applicationUserId,
      accountPortLocationId:
          accountPortLocationId ?? this.accountPortLocationId,
      carrierId: carrierId ?? this.carrierId,
      vesselVoyageId: vesselVoyageId ?? this.vesselVoyageId,
      sailingJobId: sailingJobId ?? this.sailingJobId,
      sailingJobContainerId:
          sailingJobContainerId ?? this.sailingJobContainerId,
      sailingJobPackId: sailingJobPackId ?? this.sailingJobPackId,
      comment: comment ?? this.comment,
      vesselName: vesselName ?? this.vesselName,
      voyageNumber: voyageNumber ?? this.voyageNumber,
      role: role ?? this.role,
      containerData: containerData ?? this.containerData,
      packData: packData ?? this.packData,
      billOfLading: billOfLading ?? this.billOfLading,
      updateAt: updateAt ?? this.updateAt,
      shipment: shipment ?? this.shipment,
    );
  }

  PortCargoUpdateCreate.fromJson(Map<String, dynamic> json)
      : accountId = json['accountId'] as int,
        applicationUserId = json['applicationUserId'] as int,
        accountPortLocationId = json['accountPortLocationId'] as int,
        carrierId = json['carrierId'] as int,
        vesselVoyageId = json['vesselVoyageId'] as int?,
        sailingJobId = json['sailingJobId'] as int?,
        sailingJobContainerId = json['sailingJobContainerId'] as int?,
        sailingJobPackId = json['sailingJobPackId'] as int?,
        comment = json['comment'] as String?,
        vesselName = json['vesselName'] as String,
        voyageNumber = json['voyageNumber'] as String,
        role = json['role'] as String,
        containerData = ContainerData.fromJson(json['containerData']),
        packData = PackData.fromJson(json['packData']),
        billOfLading = json['billOfLading'] as String?,
        updateAt = json['updateAt'] as String,
        shipment = json['shipment'] as String;

  Map<String, dynamic> toJson() => {
        'accountId': accountId,
        'applicationUserId': applicationUserId,
        'accountPortLocationId': accountPortLocationId,
        'carrierId': carrierId,
        'vesselVoyageId': vesselVoyageId,
        'sailingJobId': sailingJobId,
        'sailingJobContainerId': sailingJobContainerId,
        'sailingJobPackId': sailingJobPackId,
        'comment': comment,
        'vesselName': vesselName,
        'voyageNumber': voyageNumber,
        'role': role,
        'containerData': containerData.toJson(),
        'packData': packData.toJson(),
        'billOfLading': billOfLading,
        'updateAt': updateAt,
        'shipment': shipment,
      };
}
