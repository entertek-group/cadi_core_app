import 'port_cargo_update_create.dart';

class PortCargoUpdateQueue {
  final String uniqueId;
  final PortCargoUpdateCreate portCargoUpdateCreate;
  final bool complete;
  final String createdAt;
  final String error;

  PortCargoUpdateQueue(
      {required this.uniqueId,
      required this.portCargoUpdateCreate,
      required this.complete,
      required this.createdAt,
      required this.error});

  factory PortCargoUpdateQueue.initial() {
    return PortCargoUpdateQueue(
        uniqueId: "",
        portCargoUpdateCreate: PortCargoUpdateCreate.initial(),
        complete: true,
        createdAt: DateTime.now().toString(),
        error: "");
  }

  PortCargoUpdateQueue copyWith(
      {String? uniqueId,
      PortCargoUpdateCreate? portCargoUpdateCreate,
      bool? complete,
      String? createdAt,
      String? error}) {
    return PortCargoUpdateQueue(
        uniqueId: uniqueId ?? this.uniqueId,
        portCargoUpdateCreate: portCargoUpdateCreate ?? this.portCargoUpdateCreate,
        complete: complete ?? this.complete,
        createdAt: createdAt ?? this.createdAt,
        error: error ?? this.error);
  }

  PortCargoUpdateQueue.fromJson(Map json)
      : uniqueId = json['unique_id'],
        portCargoUpdateCreate = json['port_cargo_update_create'] != null
            ? PortCargoUpdateCreate.fromJson(json['port_cargo_update_create'])
            : PortCargoUpdateCreate.initial(),
        complete = json['complete'] as bool,
        createdAt = json['createdAt'],
        error = json['error'];

  Map toJson() => {
        'unique_id': uniqueId,
        'port_cargo_update_create': portCargoUpdateCreate,
        'complete': complete,
        'createdAt': createdAt,
        'error': error
      };
}
