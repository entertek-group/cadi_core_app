import 'package:cadi_core_app/redux/models/image/image_create.dart';
import 'package:cadi_core_app/redux/models/list_converters.dart';
import 'package:cadi_core_app/redux/models/port_event/refuel_data.dart';

class PortEventCreate {
  final int accountId;
  final int applicationUserId;
  final int accountPortLocationId;
  final int carrierId;
  int? accountPortBerthId;
  int? vesselVoyageId;
  int? fuelTypeId;
  final String role;
  final String voyageNumber;
  final String vesselName;
  final String comment;
  final String eventAt;
  String? eventCompleteAt;
  final RefuelData? refuelData;
  final List<ImageCreate>? images;

  PortEventCreate({
    required this.accountId,
    required this.applicationUserId,
    required this.accountPortLocationId,
    required this.carrierId,
    required this.accountPortBerthId,
    required this.vesselVoyageId,
    required this.fuelTypeId,
    required this.role,
    required this.voyageNumber,
    required this.vesselName,
    required this.comment,
    required this.eventAt,
    required this.eventCompleteAt,
    required this.refuelData,
    required this.images
  });

  factory PortEventCreate.initial() {
    return PortEventCreate(
        accountId: 0,
        applicationUserId: 0,
        accountPortLocationId: 0,
        carrierId: 0,
        accountPortBerthId: null,
        vesselVoyageId: null,
        fuelTypeId: null,
        role: "",
        voyageNumber: "",
        vesselName: "",
        comment: "",
        eventAt: "",
        eventCompleteAt: null,
        refuelData: null,
        images: null
    );
  }

  PortEventCreate copyWith({
    int? accountId,
    int? applicationUserId,
    int? accountPortLocationId,
    int? carrierId,
    int? accountPortBerthId,
    int? vesselVoyageId,
    int? fuelTypeId,
    String? role,
    String? voyageNumber,
    String? vesselName,
    String? comment,
    String? eventAt,
    String? eventCompleteAt,
    RefuelData? refuelData,
    List<ImageCreate>? images
  }) {
    return PortEventCreate(
        accountId: accountId ?? this.accountId,
        applicationUserId: applicationUserId ?? this.applicationUserId,
        accountPortLocationId: accountPortLocationId ?? this.accountPortLocationId,
        carrierId: carrierId ?? this.carrierId,
        accountPortBerthId: accountPortBerthId ?? this.accountPortBerthId,
        vesselVoyageId: vesselVoyageId ?? this.vesselVoyageId,
        fuelTypeId: fuelTypeId ?? this.fuelTypeId,
        role: role ?? this.role,
        voyageNumber: voyageNumber ?? this.voyageNumber,
        vesselName: vesselName ?? this.vesselName,
        comment: comment ?? this.comment,
        eventAt: eventAt ?? this.eventAt,
        eventCompleteAt: eventCompleteAt ?? this.eventCompleteAt,
        refuelData: refuelData ?? this.refuelData,
        images: images ?? this.images
    );
  }

  PortEventCreate.fromJson(Map json)
      : accountId = json['accountId'] as int,
        applicationUserId = json['applicationUserId'] as int,
        accountPortLocationId = json['accountPortLocationId'] as int,
        carrierId = json['carrierId'] as int,
        accountPortBerthId = json['accountPortBerthId'] as dynamic,
        vesselVoyageId = json['vesselVoyageId'] as dynamic,
        fuelTypeId = json['fuelTypeId'] as dynamic,
        role = json['role'],
        voyageNumber = json['voyageNumber'] ?? "",
        vesselName = json['vesselName'],
        comment = json['comment'],
        eventAt = json['eventAt'],
        eventCompleteAt = json['eventCompleteAt'] as dynamic,
        refuelData =  json['refuelData'] != null ?
            RefuelData.fromJson(json['refuelData'])
              : null,
        images = json['images'] != null ?
          converterImageCreate(json['images']) :
          null;

  Map toJson() => {
    'accountId': accountId,
    'applicationUserId': applicationUserId,
    'accountPortLocationId': accountPortLocationId,
    'carrierId': carrierId,
    'accountPortBerthId': accountPortBerthId,
    'vesselVoyageId': vesselVoyageId,
    'fuelTypeId': fuelTypeId,
    'role': role,
    'voyageNumber': voyageNumber,
    'vesselName': vesselName,
    'comment': comment,
    'eventAt': eventAt,
    'eventCompleteAt': eventCompleteAt,
    'refuelData': refuelData,
    'images': images,
  };
}
