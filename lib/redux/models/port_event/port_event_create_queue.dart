import 'package:cadi_core_app/redux/models/port_event/port_event_create.dart';

class PortEventCreateQueue {
  final String uniqueId;
  final PortEventCreate portEventCreate;
  final bool complete;
  final String createdAt;
  final String error;

  PortEventCreateQueue(
      {required this.uniqueId,
      required this.portEventCreate,
      required this.complete,
      required this.createdAt,
      required this.error});

  factory PortEventCreateQueue.initial() {
    return PortEventCreateQueue(
        uniqueId: "",
        portEventCreate: PortEventCreate.initial(),
        complete: true,
        createdAt: DateTime.now().toString(),
        error: "");
  }

  PortEventCreateQueue copyWith(
      {String? uniqueId,
      PortEventCreate? portEventCreate,
      bool? complete,
      String? createdAt,
      String? error}) {
    return PortEventCreateQueue(
        uniqueId: uniqueId ?? this.uniqueId,
        portEventCreate: portEventCreate ?? this.portEventCreate,
        complete: complete ?? this.complete,
        createdAt: createdAt ?? this.createdAt,
        error: error ?? this.error);
  }

  PortEventCreateQueue.fromJson(Map json)
      : uniqueId = json['unique_id'],
        portEventCreate = json['port_event'] != null
            ? PortEventCreate.fromJson(json['port_event'])
            : PortEventCreate.initial(),
        complete = json['complete'] as bool,
        createdAt = json['createdAt'],
        error = json['error'];

  Map toJson() => {
        'unique_id': uniqueId,
        'vessel_journey': portEventCreate,
        'complete': complete,
        'createdAt': createdAt,
        'error': error
      };
}
