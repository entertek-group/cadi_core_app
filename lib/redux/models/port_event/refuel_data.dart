
class RefuelData {
  final num volume;
  final String volumeUnit;
  final num price;
  final String priceCurrency;
  final num distanceTravelled;
  final String distanceTravelledUnit;

  RefuelData({
        required this.volume,
        required this.volumeUnit,
        required this.price,
        required this.priceCurrency,
        required this.distanceTravelled,
        required this.distanceTravelledUnit
      });

  factory RefuelData.initial() {
    return RefuelData(
      volume: 0.0,
      volumeUnit: "kl",
      price: 0.00,
      priceCurrency: "usd",
      distanceTravelled: 0,
      distanceTravelledUnit: "hr"
    );
  }

  RefuelData copyWith({
    num? volume,
    String? volumeUnit,
    num? price,
    String? priceCurrency,
    num? distanceTravelled,
    String? distanceTravelledUnit,
  }) {
    return RefuelData(
      volume: volume ?? this.volume,
      volumeUnit: volumeUnit ?? this.volumeUnit,
      price: price ?? this.price,
      priceCurrency: priceCurrency ?? this.priceCurrency,
      distanceTravelled: distanceTravelled ?? this.distanceTravelled,
      distanceTravelledUnit: distanceTravelledUnit ?? this.distanceTravelledUnit
    );
  }

  RefuelData.fromJson(Map json)
      : volume = json['volume'] as num,
        volumeUnit = json['volumeUnit'] ?? "",
        price = json['price'] ?? "",
        priceCurrency = json['priceCurrency'] ?? "",
        distanceTravelled = json['distanceTravelled'] as num,
        distanceTravelledUnit = json['distanceTravelledUnit'] ?? ""
  ;

  Map toJson() => {
    'volume': volume,
    'volumeUnit': volumeUnit,
    'price': price,
    'priceCurrency': priceCurrency,
    'distanceTravelled': distanceTravelled,
    'distanceTravelledUnit': distanceTravelledUnit,
  };
}
