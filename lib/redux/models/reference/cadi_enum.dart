class CadiEnum {
  final String id;
  final String description;

  CadiEnum({required this.id, required this.description});

  factory CadiEnum.initial() {
    return CadiEnum(id: "", description: "");
  }

  CadiEnum copyWith({String? id, String? description}) {
    return CadiEnum(
        id: id ?? this.id, description: description ?? this.description);
  }

  CadiEnum.fromJson(Map json)
      : id = json['id'] ?? "",
        description = json['description'] ?? "";

  Map toJson() => {'id': id, 'description': description};
}
