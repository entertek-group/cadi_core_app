class CadiGeneric {

  final String code;
  final String description;

  CadiGeneric({required this.code, required this.description});

  factory CadiGeneric.initial() {
    return CadiGeneric(code: "", description: "");
  }

  CadiGeneric copyWith({String? code, String? description}) {
    return CadiGeneric(
        code: code ?? this.code,
        description: description ?? this.description);
  }

  CadiGeneric.fromJson(Map json)
      : code = json['code'] ?? "",
        description = json['description'] ?? "";

  Map toJson() => {'code': code, 'description': description};
}
