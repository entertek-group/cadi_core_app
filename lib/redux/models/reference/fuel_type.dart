class FuelType {
  final int id;
  final String transportMode;
  final String code;
  final String description;

  FuelType({
    required this.id,
    required this.transportMode,
    required this.code,
    required this.description
  });

  factory FuelType.initial() {
    return FuelType(
        id: 0,
        transportMode: "Sea",
        code: "",
        description: ""
    );
  }

  FuelType copyWith({
    int? id,
    String? transportMode,
    String? code,
    String? description}) {
    return FuelType(
        id: id ?? this.id,
        transportMode: transportMode ?? this.transportMode,
        code: code ?? this.code,
        description: description ?? this.description);
  }

  FuelType.fromJson(Map json)
      :
        id = json['id'] as int,
        transportMode = json['transportMode'] ?? "",
        code = json['code'] ?? "",
        description = json['description'] ?? "";

  Map toJson() => {
    'id': id,
    'transportMode': transportMode,
    'code': code,
    'description': description
  };
}
