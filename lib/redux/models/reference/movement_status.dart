class MovementStatus {
  final int id;
  final String code;
  final String status;
  final bool active;
  final bool commentRequired;

  MovementStatus(
      {required this.id,
      required this.code,
      required this.status,
      required this.active,
      required this.commentRequired});

  factory MovementStatus.initial() {
    return MovementStatus(
        id: 0, code: "", status: "", active: false, commentRequired: false);
  }

  MovementStatus copyWith(
      {int? id,
      String? code,
      String? status,
      bool? active,
      bool? commentRequired}) {
    return MovementStatus(
        id: id ?? this.id,
        code: code ?? this.code,
        status: status ?? this.status,
        active: active ?? this.active,
        commentRequired: commentRequired ?? this.commentRequired);
  }

  MovementStatus.fromJson(Map json)
      : id = json['id'] as int,
        code = json['code'],
        status = json['status'],
        active = json['active'] as bool,
        commentRequired = json['comment_required'];

  Map toJson() => {
        'id': id,
        'code': code,
        'status': status,
        'active': active,
        'comment_required': commentRequired
      };

}
