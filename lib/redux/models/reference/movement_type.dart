class MovementType {
  final int id;
  final String code;
  final String event;
  final String description;

  MovementType(
      {required this.id,
      required this.code,
      required this.event,
      required this.description});

  factory MovementType.initial() {
    return MovementType(id: 0, code: "", event: "", description: "");
  }

  MovementType copyWith(
      {int? id, String? code, String? event, String? description}) {
    return MovementType(
        id: id ?? this.id,
        code: code ?? this.code,
        event: event ?? this.event,
        description: description ?? this.description);
  }

  MovementType.fromJson(Map json)
      : id = json['id'] as int,
        code = json['code'],
        event = json['event'],
        description = json['description'];

  Map toJson() => {
        'id': id,
        'code': code,
        'event': event,
        'description': description,
      };
}
