import 'package:cadi_core_app/redux/models/list_converters.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/report/port_movement_report_line.dart';

class PortMovementReportSummary {
  final String vesselName;
  final String voyageNumber;
  final num totalCount;
  final num remainingCount;
  final num completedCount;
  final num completedPercentage;
  final num damagedCount;
  final String unLoCode;
  final List<PortMovementReportLine> reportLines;

  PortMovementReportSummary(
      {
        required this.vesselName,
        required this.voyageNumber,
        required this.totalCount,
        required this.remainingCount,
        required this.completedCount,
        required this.completedPercentage,
        required this.damagedCount,
        required this.unLoCode,
        required this.reportLines
      });

  factory PortMovementReportSummary.initial() {
    return PortMovementReportSummary(
        vesselName: "",
        voyageNumber: "",
        totalCount: 0,
        remainingCount: 0,
        completedCount: 0,
        completedPercentage: 0,
        damagedCount: 0,
        unLoCode: "",
        reportLines: <PortMovementReportLine> [PortMovementReportLine.initial()]
    );
  }

  PortMovementReportSummary copyWith(
      {
        String? vesselName,
        String? voyageNumber,
        num? totalCount,
        num? remainingCount,
        num? completedCount,
        num? completedPercentage,
        num? damagedCount,
        String? unLoCode,
        List<PortMovementReportLine>? reportLines
      }) {
    return PortMovementReportSummary(
        vesselName: vesselName ?? this.vesselName,
        voyageNumber: voyageNumber ?? this.voyageNumber,
        totalCount: totalCount ?? this.totalCount,
        remainingCount: remainingCount ?? this.remainingCount,
        completedCount: completedCount ?? this.completedCount,
        completedPercentage: completedPercentage ?? this.completedPercentage,
        damagedCount: damagedCount ?? this.damagedCount,
        unLoCode: unLoCode ?? this.unLoCode,
        reportLines: reportLines ?? this.reportLines
    );
  }

  PortMovementReportSummary.fromJson(Map json)
      : vesselName = json['vesselName'],
        voyageNumber = json['voyageNumber'],
        totalCount = json['totalCount'] as num,
        remainingCount = json['remainingCount'] as num,
        completedCount = json['completedCount'] as num,
        completedPercentage = json['completedPercentage'] as num,
        damagedCount = json['damagedCount'] as num,
        unLoCode = json['unLoCode'],
        reportLines = json['reportStateLines'] != null
            ? converterPortMovementReportLines(json['reportStateLines'])
            : <PortMovementReportLine>[PortMovementReportLine.initial()];

  Map toJson() => {
    'vesselName': vesselName,
    'voyageNumber': voyageNumber,
    'totalCount': totalCount,
    'remainingCount': remainingCount,
    'completedCount': completedCount,
    'completedPercentage': completedPercentage,
    'damagedCount': damagedCount,
    'unLoCode': unLoCode,
    'reportStateLines': reportLines
  };
}
