class PortMovementReportLine {
  final String role;
  final int order;
  final num goalCount;
  final num pendingCount;
  final num completedCount;
  final num completedPercentage;

  PortMovementReportLine(
      {
        required this.role,
        required this.order,
        required this.goalCount,
        required this.pendingCount,
        required this.completedCount,
        required this.completedPercentage
      });

  factory PortMovementReportLine.initial() {
    return PortMovementReportLine(
        role: "",
        order: 0,
        goalCount: 0,
        pendingCount: 0,
        completedCount:0,
        completedPercentage: 0
    );
  }

  PortMovementReportLine copyWith(
      {
        String? role,
        int? order,
        num? goalCount,
        num? pendingCount,
        num? completedCount,
        num? completedPercentage
      }){
    return PortMovementReportLine(
        role: role ?? this.role,
        order: order ?? this.order,
        goalCount: goalCount ?? this.goalCount,
        pendingCount: pendingCount ?? this.pendingCount,
        completedCount: completedCount ?? this.completedCount,
        completedPercentage: completedPercentage ?? this.completedPercentage
    );
  }

  PortMovementReportLine.fromJson(Map json)
      :
        role = json['role'],
        order = json['order'] as int,
        goalCount = json['goalCount'] as num,
        pendingCount = json ['pendingCount'] as num,
        completedCount = json['completedCount'] as num,
        completedPercentage = json['completedPercentage'] as num;

  Map toJson() =>
      {
        'role': role,
        'order': order,
        'goalCount': goalCount,
        'pendingCount': pendingCount,
        'completedCount': completedCount,
        'completedPercentage': completedPercentage
      };
}
