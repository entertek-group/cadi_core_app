class PortReportLine {
  final String identifier;
  final String role;
  final String type;
  final String identifierType;
  String? movementAt;
  final String jobNumber;
  final String location;

  PortReportLine(
      {
        required this.identifier,
        required this.role,
        required this.type,
        required this.identifierType,
        required this.movementAt,
        required this.jobNumber,
        required this.location
      });

  factory PortReportLine.initial() {
    return PortReportLine(
        identifier: "",
        role: "",
        type: "",
        identifierType: "",
        movementAt: "",
        jobNumber: "",
        location: ""
    );
  }

  PortReportLine copyWith(
      {
        String? identifier,
        String? role,
        String? type,
        String? identifierType,
        String? movementAt,
        String? jobNumber,
        String? location
      }){
    return PortReportLine(
        identifier: identifier ?? this.identifier,
        role: role ?? this.role,
        type: type ?? this.type,
        identifierType: identifierType ?? this.identifierType,
        movementAt: movementAt ?? this.movementAt,
        jobNumber: jobNumber ?? this.jobNumber,
        location: location ?? this.location
    );
  }

  PortReportLine.fromJson(Map json)
      :
        identifier = json['identifier'],
        role = json['role'],
        type = json['lineType'],
        identifierType = json['identifierType'],
        movementAt = json ['movementAt'] as dynamic,
        jobNumber = json['jobNumber'],
        location = json['location'];

  Map toJson() =>
      {
        'identifier': identifier,
        'role': role,
        'lineType': type,
        'identifierType': identifierType,
        'movementAt': movementAt,
        'jobNumber': jobNumber,
        'location': location
      };
}
