import 'package:cadi_core_app/redux/models/list_converters.dart';
import 'package:cadi_core_app/redux/models/models.dart';

class PortReportSummary {
  final String vesselName;
  final String voyageNumber;
  final int totalCount;
  final int remainingCount;
  final int completedCount;
  final String unLoCode;
  final String reportType;
  final List<PortReportLine> reportLines;

  PortReportSummary(
      {
        required this.vesselName,
        required this.voyageNumber,
        required this.totalCount,
        required this.remainingCount,
        required this.completedCount,
        required this.unLoCode,
        required this.reportType,
        required this.reportLines
      });

  factory PortReportSummary.initial() {
    return PortReportSummary(
        vesselName: "",
        voyageNumber: "",
        totalCount: 0,
        remainingCount: 0,
        completedCount: 0,
        unLoCode: "",
        reportType: "",
        reportLines: <PortReportLine> [PortReportLine.initial()]
    );
  }

  PortReportSummary copyWith(
      {
        String? vesselName,
        String? voyageNumber,
        int? totalCount,
        int? remainingCount,
        int? completedCount,
        String? unLoCode,
        String? reportType,
        List<PortReportLine>? reportLines
      }) {
    return PortReportSummary(
        vesselName: vesselName ?? this.vesselName,
        voyageNumber: voyageNumber ?? this.voyageNumber,
        totalCount: totalCount ?? this.totalCount,
        remainingCount: remainingCount ?? this.remainingCount,
        completedCount: completedCount ?? this.completedCount,
        unLoCode: unLoCode ?? this.unLoCode,
        reportType: reportType ?? this.reportType,
        reportLines: reportLines ?? this.reportLines
    );
  }

  PortReportSummary.fromJson(Map json)
      : vesselName = json['vesselName'],
        voyageNumber = json['voyageNumber'],
        totalCount = json['totalCount'] as int,
        remainingCount = json['remainingCount'] as int,
        completedCount = json['completedCount'] as int,
        unLoCode = json['unLoCode'],
        reportType = json['reportType'],
        reportLines = json['reportLines'] != null
            ? converterPortReportLines(json['reportLines'])
            : <PortReportLine>[PortReportLine.initial()];

  Map toJson() => {
        'vesselName': vesselName,
        'voyageNumber': voyageNumber,
        'totalCount': totalCount,
        'remainingCount': remainingCount,
        'completedCount': completedCount,
        'unLoCode': unLoCode,
        'reportType': reportType,
        'reportLines': reportLines
      };
}
