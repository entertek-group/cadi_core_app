class VesselSummary {
  final String vesselName;
  final int cargoCount;
  final String unLocode;

  VesselSummary(
      {required this.vesselName,
      required this.cargoCount,
      required this.unLocode});

  factory VesselSummary.initial() {
    return VesselSummary(vesselName: "", cargoCount: 0, unLocode: "");
  }

  VesselSummary copyWith(
      {String? vesselName, int? cargoCount, String? unLocode}) {
    return VesselSummary(
      vesselName: vesselName ?? this.vesselName,
      cargoCount: cargoCount ?? this.cargoCount,
      unLocode: unLocode ?? this.unLocode,
    );
  }

  VesselSummary.fromJson(Map json)
      : vesselName = json['vessel_name'],
        cargoCount = json['cargo_count'] as int,
        unLocode = json['un_locode'];

  Map toJson() => {
        'vessel_name': vesselName,
        'cargo_count': cargoCount,
        'un_locode': unLocode
      };
}
