import 'package:cadi_core_app/helpers/constants.dart';

class ContainerData {
  
  final String sealNumber1;
  final String sealNumber2;
  final String sealNumber3;
  final String containerCode;
  final String containerNumber;
  final String containerMode;
  final String transportMode;
  final num weight;
  final String weightUnit;
  final num volume;
  final String volumeUnit;
  final String commodityCode;
  final String packingMode;
  
  //Image

  ContainerData(
      {
        required this.sealNumber1,
        required this.sealNumber2,
        required this.sealNumber3,
        required this.containerCode,
        required this.containerNumber,
        required this.containerMode,
        required this.transportMode,
        required this.weight,
        required this.weightUnit,
        required this.volume,
        required this.volumeUnit,
        required this.commodityCode,
        required this.packingMode
      });

  factory ContainerData.initial() {
    return ContainerData(
      sealNumber1: "",
      sealNumber2: "",
      sealNumber3: "",
      containerCode: "",
      containerNumber: "",
      containerMode: "",
      transportMode: "Sea",
      weight: 0.0,
      weightUnit: WeightUnit.tonne,
      volume: 0.0,
      volumeUnit: "M3",
      commodityCode: "GEN",
      packingMode: "FCL", // Empty list of strings
    );
  }

  ContainerData copyWith({
    String? sealNumber1,
    String? sealNumber2,
    String? sealNumber3,
    String? containerCode,
    String? containerNumber,
    String? containerMode,
    String? transportMode,
    num? weight,
    String? weightUnit,
    num? volume,
    String? volumeUnit,
    String? commodityCode,
    String? packingMode
  }) {
    return ContainerData(
      sealNumber1: sealNumber1 ?? this.sealNumber1,
      sealNumber2: sealNumber2 ?? this.sealNumber2,
      sealNumber3: sealNumber3 ?? this.sealNumber3,
      containerCode: containerCode ?? this.containerCode,
      containerNumber: containerNumber ?? this.containerNumber,
      containerMode: containerMode ?? this.containerMode,
      transportMode: transportMode ?? this.transportMode,
      weight: weight ?? this.weight,
      weightUnit: weightUnit ?? this.weightUnit,
      volume: volume ?? this.volume,
      volumeUnit: volumeUnit ?? this.volumeUnit,
      commodityCode: commodityCode ?? this.commodityCode,
      packingMode: packingMode ?? this.packingMode
    );
  }

  ContainerData.fromJson(Map json)
      : sealNumber1 = json['sealNumber1'] ?? "",
        sealNumber2 = json['sealNumber2'] ?? "",
        sealNumber3 = json['sealNumber3'] ?? "",
        containerCode = json['containerCode'] ?? "",
        containerNumber = json['containerNumber'] ?? "",
        containerMode = json['containerMode'] ?? "",
        transportMode = json['transportMode'] ?? "",
        weight = json['weight'] as num,
        weightUnit = json['weightUnit'] ?? "",
        volume = json['volume'] as num,
        volumeUnit = json['volumeUnit'] ?? "",
        commodityCode = json['commodityCode'] ?? "",
        packingMode = json['packingMode'] ?? ""
  ;

  Map toJson() => {
    'sealNumber1': sealNumber1,
    'sealNumber2': sealNumber2,
    'sealNumber3': sealNumber3,
    'containerCode': containerCode,
    'containerNumber': containerNumber,
    'containerMode': containerMode,
    'transportMode': transportMode,
    'weight': weight,
    'weightUnit': weightUnit,
    'volume': volume,
    'volumeUnit': volumeUnit,
    'container_mode': containerMode,
    'commodityCode': commodityCode,
    'packingMode': packingMode
  };
}
