import 'package:cadi_core_app/redux/models/cargo_data/container_data.dart';
import 'package:cadi_core_app/redux/models/image/image_create.dart';
import '../list_converters.dart';

class RoadMovementCreate {
  final int accountId;
  final int applicationUserId;
  final int accountPortLocationId;
  final int? accountVehicleId;
  final int? accountVehicleDriverId;
  final int? accountVehicleOperatorId;
  final int? vesselVoyageId;
  final String comment;
  final String waybillNumber;
  final String legNumber;
  final String? expectedArrivalAt;
  final String role;
  final String state;
  final String? signatoryFullName;
  final List<ContainerData>? containerData;
  final List<ImageCreate>? documents;
  final List<ImageCreate>? images;
  final ImageCreate? signature;
  final String movementAt;
  final bool isBreakBulk;

  RoadMovementCreate(
      {required this.accountId,
      required this.applicationUserId,
      required this.accountPortLocationId,
      required this.accountVehicleId,
      required this.accountVehicleDriverId,
      required this.accountVehicleOperatorId,
      required this.vesselVoyageId,
      required this.comment,
      required this.waybillNumber,
      required this.legNumber,
      required this.expectedArrivalAt,
      required this.role,
      required this.state,
      required this.signatoryFullName,
      required this.containerData,
      required this.documents,
      required this.images,
      required this.signature,
      required this.movementAt,
      required this.isBreakBulk});

  factory RoadMovementCreate.initial() {
    return RoadMovementCreate(
        accountId: 0,
        applicationUserId: 0,
        accountPortLocationId: 0,
        accountVehicleId: null,
        accountVehicleDriverId: null,
        accountVehicleOperatorId: null,
        vesselVoyageId: null,
        comment: "",
        waybillNumber: "",
        legNumber: "",
        expectedArrivalAt: null,
        role: "",
        state: "",
        containerData: null,
        documents: null,
        images: null,
        signatoryFullName: null,
        signature: null,
        movementAt: "",
        isBreakBulk: false // Empty list of strings
        );
  }

  RoadMovementCreate copyWith(
      {int? accountId,
      int? applicationUserId,
      int? accountPortLocationId,
      int? accountVehicleId,
      int? accountVehicleDriverId,
      int? accountVehicleOperatorId,
      int? vesselVoyageId,
      String? comment,
      String? waybillNumber,
      String? legNumber,
      String? expectedArrivalAt,
      String? role,
      String? state,
      String? signatoryFullName,
      List<ContainerData>? containerData,
      List<ImageCreate>? documents,
      List<ImageCreate>? images,
      ImageCreate? signature,
      String? movementAt,
      bool? isBreakBulk}) {
    return RoadMovementCreate(
        accountId: accountId ?? this.accountId,
        applicationUserId: applicationUserId ?? this.applicationUserId,
        accountPortLocationId:
            accountPortLocationId ?? this.accountPortLocationId,
        accountVehicleId: accountVehicleId ?? this.accountVehicleId,
        accountVehicleDriverId:
            accountVehicleDriverId ?? this.accountVehicleDriverId,
        accountVehicleOperatorId:
            accountVehicleOperatorId ?? this.accountVehicleOperatorId,
        vesselVoyageId: vesselVoyageId ?? this.vesselVoyageId,
        comment: comment ?? this.comment,
        waybillNumber: waybillNumber ?? this.waybillNumber,
        legNumber: legNumber ?? this.legNumber,
        expectedArrivalAt: expectedArrivalAt ?? this.expectedArrivalAt,
        role: role ?? this.role,
        state: state ?? this.state,
        signatoryFullName: signatoryFullName ?? this.signatoryFullName,
        containerData: containerData ?? this.containerData,
        documents: documents ?? this.documents,
        images: images ?? this.images,
        signature: signature ?? this.signature,
        movementAt: movementAt ?? this.movementAt,
        isBreakBulk: isBreakBulk ?? this.isBreakBulk);
  }

  RoadMovementCreate.fromJson(Map json)
      : accountId = json['accountId'] as int,
        applicationUserId = json['applicationUserId'] as int,
        accountPortLocationId = json['accountPortLocationId'] as int,
        accountVehicleId = json['accountVehicleId'] as dynamic,
        accountVehicleDriverId = json['accountVehicleDriverId'] as dynamic,
        accountVehicleOperatorId = json['accountVehicleOperatorId'] as dynamic,
        vesselVoyageId = json['vesselVoyageId'] as dynamic,
        comment = json['comment'] ?? "",
        waybillNumber = json['waybillNumber'] ?? "",
        legNumber = json['legNumber'] ?? "",
        expectedArrivalAt = json['expectedArrivalAt'] as dynamic,
        role = json['role'] ?? "",
        state = json['state'] ?? "",
        signatoryFullName = json['signatoryFullName'] as dynamic,
        containerData = json['containerData'] != null
            ? converterContainerData(json['containersData'])
            : <ContainerData>[ContainerData.initial()],
        documents = json['documents'] != null
            ? converterImageCreate(json['documents'])
            : <ImageCreate>[ImageCreate.initial()],
        images = json['images'] != null
            ? converterImageCreate(json['images'])
            : <ImageCreate>[ImageCreate.initial()],
        signature = json['signature'] != null
            ? ImageCreate.fromJson(json['signature'])
            : ImageCreate.initial(),
        movementAt = json['movementAt'] ?? "",
        isBreakBulk = json['isBreakBulk'] as bool;

  Map toJson() => {
        'accountId': accountId,
        'applicationUserId': applicationUserId,
        'accountPortLocationId': accountPortLocationId,
        'accountVehicleId': accountVehicleId,
        'accountVehicleDriverId': accountVehicleDriverId,
        'accountVehicleOperatorId': accountVehicleOperatorId,
        'vesselVoyageId': vesselVoyageId,
        'comment': comment,
        'waybillNumber': waybillNumber,
        'legNumber': legNumber,
        'expectedArrivalAt': expectedArrivalAt,
        'role': role,
        'state': state,
        'signatoryFullName': signatoryFullName,
        'containersData': containerData,
        'documents': documents,
        'images': images,
        'signature': signature,
        'movementAt': movementAt,
        'isBreakBulk': isBreakBulk
      };
}
