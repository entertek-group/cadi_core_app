import 'road_movement_create.dart';

class RoadMovementCreateQueue {
  final String uniqueId;
  final RoadMovementCreate roadMovement;
  final bool complete;
  final String createdAt;
  final String error;

  RoadMovementCreateQueue({
    required this.uniqueId,
    required this.roadMovement,
    required this.complete,
    required this.createdAt,
    required this.error
  });

  factory RoadMovementCreateQueue.initial()
  {
    return RoadMovementCreateQueue(
        uniqueId: "",
        roadMovement: RoadMovementCreate.initial(),
        complete: true,
        createdAt: DateTime.now().toString(),
        error: "");
  }

  RoadMovementCreateQueue copyWith(
      {
        String? uniqueId,
        RoadMovementCreate? roadMovement,
        bool? complete,
        String? createdAt,
        String? error})
  {
    return RoadMovementCreateQueue(
        uniqueId: uniqueId ?? this.uniqueId,
        roadMovement: roadMovement ?? this.roadMovement,
        complete: complete ?? this.complete,
        createdAt: createdAt ?? this.createdAt,
        error: error ?? this.error);
  }

  RoadMovementCreateQueue.fromJson(Map json)
      : uniqueId = json['unique_id'],
        roadMovement = json['road_movement_create'] != null
            ? RoadMovementCreate.fromJson(json['road_movement_create'])
            : RoadMovementCreate.initial(),
        complete = json['complete'] as bool,
        createdAt = json['createdAt'],
        error = json['error'];

  Map toJson() => {
        'unique_id': uniqueId,
        'road_movement_create': roadMovement,
        'complete': complete,
        'createdAt': createdAt,
        'error': error
  };
}
