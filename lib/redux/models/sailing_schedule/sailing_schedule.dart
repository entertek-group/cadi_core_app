import 'package:cadi_core_app/redux/models/vessel/vessel.dart';

class SailingSchedule {
  int? id;
  final int accountId;
  final int userId;
  final String voyageNumber;
  final int vesselId;
  String? estimatedDeparture;
  String? estimatedArrival;
  num? capacityUsed;
  String? capacityUnit;
  num? distance;
  String? distanceUnit;
  num? avgSpeed;
  String? avgSpeedUnit;
  final String createdAt;
  final String updatedAt;
  final String portOfDischarge;
  final String portOfLoad;
  final Vessel vessel;

  SailingSchedule(
      {required this.id,
      required this.accountId,
      required this.userId,
      required this.voyageNumber,
      required this.vesselId,
      required this.estimatedDeparture,
      required this.estimatedArrival,
      required this.capacityUsed,
      required this.capacityUnit,
      required this.distance,
      required this.distanceUnit,
      required this.avgSpeed,
      required this.avgSpeedUnit,
      required this.createdAt,
      required this.updatedAt,
      required this.portOfDischarge,
      required this.portOfLoad,
      required this.vessel});

  factory SailingSchedule.initial() {
    return SailingSchedule(
        id: null,
        accountId: 0,
        userId: 0,
        voyageNumber: "",
        vesselId: 0,
        estimatedDeparture: null,
        estimatedArrival: null,
        capacityUsed: 0,
        capacityUnit: "m3",
        distance: 0,
        distanceUnit: "km",
        avgSpeed: 0,
        avgSpeedUnit: "kn",
        createdAt: "",
        updatedAt: "",
        portOfDischarge: "",
        portOfLoad: "",
        vessel: Vessel.initial());
  }

  SailingSchedule copyWith(
      {int? id,
      int? accountId,
      int? userId,
      String? voyageNumber,
      int? vesselId,
      String? estimatedDeparture,
      String? estimatedArrival,
      num? capacityUsed,
      String? capacityUnit,
      num? distance,
      String? distanceUnit,
      num? avgSpeed,
      String? avgSpeedUnit,
      String? createdAt,
      String? updatedAt,
      String? portOfDischarge,
      String? portOfLoad,
      Vessel? vessel}) {
    return SailingSchedule(
        id: id ?? this.id,
        accountId: accountId ?? this.accountId,
        userId: userId ?? this.userId,
        voyageNumber: voyageNumber ?? this.voyageNumber,
        vesselId: vesselId ?? this.vesselId,
        estimatedDeparture: estimatedDeparture ?? this.estimatedDeparture,
        estimatedArrival: estimatedArrival ?? this.estimatedArrival,
        capacityUsed: capacityUsed ?? this.capacityUsed,
        capacityUnit: capacityUnit ?? this.capacityUnit,
        distance: distance ?? this.distance,
        distanceUnit: distanceUnit ?? this.distanceUnit,
        avgSpeed: avgSpeed ?? this.avgSpeed,
        avgSpeedUnit: avgSpeedUnit ?? this.avgSpeedUnit,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        portOfDischarge: portOfDischarge ?? this.portOfDischarge,
        portOfLoad: portOfLoad ?? this.portOfLoad,
        vessel: vessel ?? this.vessel);
  }

  SailingSchedule.fromJson(Map json)
      : id = json['id'] as dynamic,
        accountId = json['account_id'] as int,
        userId = json['user_id'] as int,
        voyageNumber = json['voyage_number'] ?? "",
        vesselId = json['vessel_id'] as int,
        estimatedDeparture = json['estimated_departure'] as dynamic,
        estimatedArrival = json['estimated_arrival'] as dynamic,
        capacityUsed = json['capacity_used'] as dynamic,
        capacityUnit = json['capacity_unit'] as dynamic,
        distance = json['distance'] as dynamic,
        distanceUnit = json['distance_unit'] as dynamic,
        avgSpeed = json['avg_speed'] as dynamic,
        avgSpeedUnit = json['avg_speed_unit'] as dynamic,
        createdAt = json['created_at'] ?? "",
        updatedAt = json['updated_at'] ?? "",
        portOfDischarge = json['port_of_discharge'] ?? "",
        portOfLoad = json['port_of_load'] ?? "",
        vessel = json['vessel'] != null
            ? Vessel.fromJson(json['vessel'])
            : Vessel.initial();

  Map toJson() => {
        'id': id,
        'account_id': accountId,
        'user_id': userId,
        'voyage_number': voyageNumber,
        'vessel_id': vesselId,
        'estimated_departure': estimatedDeparture,
        'estimated_arrival': estimatedArrival,
        'capacity_used': capacityUsed,
        'capacity_unit': capacityUnit,
        'distance': distance,
        'distance_unit': distanceUnit,
        'avg_speed': avgSpeed,
        'avg_speed_unit': avgSpeedUnit,
        'created_at': createdAt,
        'updated_at': updatedAt,
        'port_of_discharge': portOfDischarge,
        'port_of_load': portOfLoad,
        'vessel': vessel
      };
}
