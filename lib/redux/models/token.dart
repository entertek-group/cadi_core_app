class Token {
  final String token;
  final String expiration;

  Token({
    required this.token,
    required this.expiration
  });

  factory Token.initial() {
    return Token(
        token: "",
        expiration: ""
    );
  }

  Token copyWith({String? token, String? expiration}) {
    return Token(
        token: token ?? this.token,
        expiration: expiration ?? this.expiration
    );
  }

  Token.fromJson(Map json) :
        token = json['token'],
        expiration = json['expiration']
  ;

  Map toJson() => {
    'token': token,
    'expiration': expiration
  };
}
