import 'package:cadi_core_app/redux/models/truck_journey/truck_journey_models.dart';

class TruckJourneyCreateQueue {
  final String uniqueId;
  final TruckJourneyCreate truckJourneyCreate;
  final bool complete;
  final String createdAt;
  final String error;

  TruckJourneyCreateQueue(
      {required this.uniqueId,
      required this.truckJourneyCreate,
      required this.complete,
      required this.createdAt,
      required this.error});

  factory TruckJourneyCreateQueue.initial() {
    return TruckJourneyCreateQueue(
        uniqueId: "",
        truckJourneyCreate: TruckJourneyCreate.initial(),
        complete: true,
        createdAt: DateTime.now().toString(),
        error: "");
  }

  TruckJourneyCreateQueue copyWith(
      {String? uniqueId,
      TruckJourneyCreate? truckJourneyCreate,
      bool? complete,
      String? createdAt,
      String? error}) {
    return TruckJourneyCreateQueue(
        uniqueId: uniqueId ?? this.uniqueId,
        truckJourneyCreate: truckJourneyCreate ?? this.truckJourneyCreate,
        complete: complete ?? this.complete,
        createdAt: createdAt ?? this.createdAt,
        error: error ?? this.error);
  }

  TruckJourneyCreateQueue.fromJson(Map json)
      : uniqueId = json['unique_id'],
        truckJourneyCreate = json['truck_journey'] != null
            ? TruckJourneyCreate.fromJson(json['truck_journey'])
            : TruckJourneyCreate.initial(),
        complete = json['complete'] as bool,
        createdAt = json['createdAt'],
        error = json['error'];

  Map toJson() => {
        'unique_id': uniqueId,
        'truck_journey': truckJourneyCreate,
        'complete': complete,
        'createdAt': createdAt,
        'error': error
      };
}
