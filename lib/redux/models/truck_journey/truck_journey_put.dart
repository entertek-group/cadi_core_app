// import 'package:cadi_core_app/redux/models/models.dart';
import 'truck_journey.dart';

class TruckJourneyPut {
  TruckJourney truckJourney;

  TruckJourneyPut({required this.truckJourney});

  factory TruckJourneyPut.initial() {
    return TruckJourneyPut(truckJourney: TruckJourney.initial());
  }

  TruckJourneyPut copyWith({TruckJourney? truckJourney
      //BOLContainerList? containers
      }) {
    return TruckJourneyPut(
      truckJourney: truckJourney ?? this.truckJourney,
    );
  }

  TruckJourneyPut.fromJson(Map json)
      : truckJourney = TruckJourney.fromJson(json['truck_journey']);

  Map toJson() => {'truck_journey': truckJourney};
}
