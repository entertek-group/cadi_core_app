import 'package:cadi_core_app/redux/models/truck_journey/truck_journey_models.dart';

class TruckJourneyUpdateQueue {
  final String uniqueId;
  final TruckJourney truckJourney;
  final bool complete;
  final String createdAt;
  final String error;

  TruckJourneyUpdateQueue(
      {required this.uniqueId,
      required this.truckJourney,
      required this.complete,
      required this.createdAt,
      required this.error});

  factory TruckJourneyUpdateQueue.initial() {
    return TruckJourneyUpdateQueue(
        uniqueId: "",
        truckJourney: TruckJourney.initial(),
        complete: true,
        createdAt: DateTime.now().toString(),
        error: "");
  }

  TruckJourneyUpdateQueue copyWith(
      {String? uniqueId,
      TruckJourney? truckJourney,
      bool? complete,
      String? createdAt,
      String? error}) {
    return TruckJourneyUpdateQueue(
        uniqueId: uniqueId ?? this.uniqueId,
        truckJourney: truckJourney ?? this.truckJourney,
        complete: complete ?? this.complete,
        createdAt: createdAt ?? this.createdAt,
        error: error ?? this.error);
  }

  TruckJourneyUpdateQueue.fromJson(Map json)
      : uniqueId = json['unique_id'],
        truckJourney = json['truck_journey'] != null
            ? TruckJourney.fromJson(json['truck_journey'])
            : TruckJourney.initial(),
        complete = json['complete'] as bool,
        createdAt = json['createdAt'],
        error = json['error'];

  Map toJson() => {
        'unique_id': uniqueId,
        'truck_journey': truckJourney,
        'complete': complete,
        'createdAt': createdAt,
        'error': error
      };
}
