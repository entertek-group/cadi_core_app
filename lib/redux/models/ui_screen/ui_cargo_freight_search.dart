class UiCargoFreightSearch {
  final String vesselName;
  final String voyageNumber;
  final int selectedCraneIndex;

  UiCargoFreightSearch({required this.vesselName, required this.voyageNumber, required this.selectedCraneIndex});

  factory UiCargoFreightSearch.initial() {
    return UiCargoFreightSearch(vesselName: "", voyageNumber: "", selectedCraneIndex: 0);
  }

  UiCargoFreightSearch copyWith(
      {
        String? vesselName,
        String? voyageNumber,
        int? selectedCraneIndex
      }) {
    return UiCargoFreightSearch(
        vesselName: vesselName ?? this.vesselName,
        voyageNumber: voyageNumber ?? this.voyageNumber,
        selectedCraneIndex: selectedCraneIndex ?? this.selectedCraneIndex,
    );
  }

  UiCargoFreightSearch.fromJson(Map json)
      : vesselName = json['vesselName'],
        voyageNumber = json['voyageNumber'],
        selectedCraneIndex = json['selectedCraneIndex'] as int
  ;

  Map toJson() => {'vesselName': vesselName, 'voyageNumber': voyageNumber, 'selectedCraneIndex': selectedCraneIndex};
}
