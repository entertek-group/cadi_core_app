import 'package:cadi_core_app/helpers/constants.dart';

class UiCargoReportSearch {
  final String reportType;
  final int selectedPortIndex;
  final int selectedLocationIndex;
  final String vesselName;
  final String voyageNumber;

  UiCargoReportSearch(
      {required this.reportType,
      required this.selectedPortIndex,
      required this.selectedLocationIndex,
      required this.vesselName,
      required this.voyageNumber});

  factory UiCargoReportSearch.initial() {
    return UiCargoReportSearch(
        reportType: CadiCargoMovementShipment.container,
        selectedPortIndex: 0,
        selectedLocationIndex: 0,
        vesselName: "",
        voyageNumber: "");
  }

  UiCargoReportSearch copyWith(
      {String? reportType,
      int? selectedPortIndex,
      int? selectedLocationIndex,
      String? vesselName,
      String? voyageNumber}) {
    return UiCargoReportSearch(
        reportType: reportType ?? this.reportType,
        selectedPortIndex: selectedPortIndex ?? this.selectedPortIndex,
        selectedLocationIndex:
            selectedLocationIndex ?? this.selectedLocationIndex,
        vesselName: vesselName ?? this.vesselName,
        voyageNumber: voyageNumber ?? this.voyageNumber);
  }

  UiCargoReportSearch.fromJson(Map json)
      : reportType = json['reportType'],
        selectedPortIndex = json['selectedPortIndex'] as int,
        selectedLocationIndex = json['selectedLocationIndex'] as int,
        vesselName = json['vesselName'],
        voyageNumber = json['voyageNumber'];

  Map toJson() => {
        'reportType': reportType,
        'selectedPortIndex': selectedPortIndex,
        'selectedLocationIndex': selectedLocationIndex,
        'vesselName': vesselName,
        'voyageNumber': voyageNumber
      };
}
