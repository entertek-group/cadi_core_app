class UiCargoTruckDetails {
  final String truckRegistration;
  final String truckDriverId;
  final String truckCompany;

  UiCargoTruckDetails(
      {required this.truckRegistration, required this.truckDriverId, required this.truckCompany});

  factory UiCargoTruckDetails.initial() {
    return UiCargoTruckDetails(truckRegistration: "", truckDriverId: "", truckCompany: "");
  }

  UiCargoTruckDetails copyWith(
      {String? truckRegistration, String? truckDriverId}) {
    return UiCargoTruckDetails(
        truckRegistration: truckRegistration ?? this.truckRegistration,
        truckDriverId: truckDriverId ?? this.truckDriverId,
        truckCompany: truckCompany);
  }

  UiCargoTruckDetails.fromJson(Map json)
      : truckRegistration = json['truckRegistration'],
        truckDriverId = json['truckDriverId'],
        truckCompany = json['truckCompany'];

  Map toJson() =>
      {'truckRegistration': truckRegistration, 'truckDriverId': truckDriverId, 'truckCompany': truckCompany};
}
