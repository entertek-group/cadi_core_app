import 'package:cadi_core_app/helpers/constants.dart';

class UiCargoUpdateSearch {
  final String searchType;
  final String containerNumber;
  final String vesselName;
  final String voyageNumber;
  final String jobNumber;

  UiCargoUpdateSearch(
      {required this.searchType,
      required this.containerNumber,
      required this.vesselName,
      required this.voyageNumber,
      required this.jobNumber});

  factory UiCargoUpdateSearch.initial() {
    return UiCargoUpdateSearch(
        searchType: CadiCargoMovementShipment.container,
        containerNumber: "",
        vesselName: "",
        voyageNumber: "",
        jobNumber: "");
  }

  UiCargoUpdateSearch copyWith(
      {String? searchType,
      String? containerNumber,
      String? vesselName,
      String? voyageNumber,
      String? jobNumber}) {
    return UiCargoUpdateSearch(
        searchType: searchType ?? this.searchType,
        containerNumber: containerNumber ?? this.containerNumber,
        vesselName: vesselName ?? this.vesselName,
        voyageNumber: voyageNumber ?? this.voyageNumber,
        jobNumber: jobNumber ?? this.jobNumber);
  }

  UiCargoUpdateSearch.fromJson(Map json)
      : searchType = json['searchType'],
        containerNumber = json['containerNumber'],
        vesselName = json['vesselName'],
        voyageNumber = json['voyageNumber'],
        jobNumber = json['jobNumber'];

  Map toJson() => {
        'searchType': searchType,
        'containerNumber': containerNumber,
        'vesselName': vesselName,
        'voyageNumber': voyageNumber,
        'jobNumber': jobNumber
      };
}
