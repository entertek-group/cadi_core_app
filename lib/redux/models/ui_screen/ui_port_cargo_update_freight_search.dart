class UiPortCargoUpdateFreightSearch {
  final String vesselName;
  final String voyageNumber;

  UiPortCargoUpdateFreightSearch(
      {required this.vesselName,
      required this.voyageNumber});

  factory UiPortCargoUpdateFreightSearch.initial() {
    return UiPortCargoUpdateFreightSearch(
        vesselName: "", voyageNumber: "");
  }

  UiPortCargoUpdateFreightSearch copyWith(
      {String? vesselName, String? voyageNumber, int? selectedCraneIndex}) {
    return UiPortCargoUpdateFreightSearch(
      vesselName: vesselName ?? this.vesselName,
      voyageNumber: voyageNumber ?? this.voyageNumber,
    );
  }

  UiPortCargoUpdateFreightSearch.fromJson(Map json)
      : vesselName = json['vesselName'],
        voyageNumber = json['voyageNumber'];

  Map toJson() => {
        'vesselName': vesselName,
        'voyageNumber': voyageNumber
      };
}
