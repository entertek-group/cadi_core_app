class UiPortEventCapture {
  final String vesselName;
  final String voyageNumber;
  final int berthIndex;

  UiPortEventCapture(
      {
        required this.vesselName, 
        required this.voyageNumber,
        required this.berthIndex
      });

  factory UiPortEventCapture.initial() {
    return UiPortEventCapture(
        vesselName: "", voyageNumber: "", berthIndex: 0);
  }

  UiPortEventCapture copyWith(
      {String? vesselName, String? voyageNumber, int? berthIndex}) {
    return UiPortEventCapture(
        vesselName: vesselName ?? this.vesselName,
        voyageNumber: voyageNumber ?? this.voyageNumber,
        berthIndex: berthIndex ?? this.berthIndex);
  }

  UiPortEventCapture.fromJson(Map json)
      : vesselName = json['vesselName'],
        voyageNumber = json['voyageNumber'],
        berthIndex = json['berthIndex'];

  Map toJson() => {
        'vesselName': vesselName,
        'voyageNumber': voyageNumber,
        'berthIndex': berthIndex
      };
}
