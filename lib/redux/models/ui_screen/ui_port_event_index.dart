class UiPortEventIndex {
  final int selectedPortIndex;
  final int selectedLocationIndex;
  final int selectedCarrierIndex;
  final int selectedRoleIndex;

  UiPortEventIndex(
      {required this.selectedPortIndex,
      required this.selectedLocationIndex,
      required this.selectedCarrierIndex,
      required this.selectedRoleIndex});

  factory UiPortEventIndex.initial() {
    return UiPortEventIndex(
        selectedPortIndex: 0,
        selectedLocationIndex: 0,
        selectedCarrierIndex: 0,
        selectedRoleIndex: 0);
  }

  UiPortEventIndex copyWith(
      {int? selectedPortIndex,
      int? selectedLocationIndex,
      int? selectedCarrierIndex,
      int? selectedRoleIndex}) {
    return UiPortEventIndex(
        selectedPortIndex: selectedPortIndex ?? this.selectedPortIndex,
        selectedLocationIndex:
            selectedLocationIndex ?? this.selectedLocationIndex,
        selectedCarrierIndex: selectedCarrierIndex ?? this.selectedCarrierIndex,
        selectedRoleIndex: selectedRoleIndex ?? this.selectedRoleIndex);
  }

  UiPortEventIndex.fromJson(Map json)
      : selectedPortIndex = json['selectedPortIndex'] as int,
        selectedLocationIndex = json['selectedLocationIndex'] as int,
        selectedCarrierIndex = json['selectedCarrierIndex'] as int,
        selectedRoleIndex = json['selectedRoleIndex'] as int;

  Map toJson() => {
        'selectedPortIndex': selectedPortIndex,
        'selectedLocationIndex': selectedLocationIndex,
        'selectedCarrierIndex': selectedCarrierIndex,
        'selectedRoleIndex': selectedRoleIndex
      };
}
