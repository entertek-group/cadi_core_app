class UiRoadMovementHome {
  final int selectedPortIndex;
  final int selectedLocationIndex;
  final int selectedVehicleIndex;
  final int selectedVehicleDriverIndex;
  final int selectedVehicleOperatorIndex;
  final int selectedRoleIndex;

  UiRoadMovementHome(
      {
        required this.selectedPortIndex,
        required this.selectedLocationIndex,
        required this.selectedVehicleIndex,
        required this.selectedVehicleDriverIndex,
        required this.selectedVehicleOperatorIndex,
        required this.selectedRoleIndex
      });

  factory UiRoadMovementHome.initial() {
    return UiRoadMovementHome(
        selectedPortIndex: 0,
        selectedLocationIndex: 0,
        selectedVehicleIndex: 0,
        selectedVehicleDriverIndex: 0,
        selectedVehicleOperatorIndex: 0,
        selectedRoleIndex: 0);
  }

  UiRoadMovementHome copyWith(
      {
        int? selectedPortIndex,
        int? selectedLocationIndex,
        int? selectedVehicleIndex,
        int? selectedVehicleDriverIndex,
        int? selectedVehicleOperatorIndex,
        int? selectedRoleIndex
      }) {
    return UiRoadMovementHome(
      selectedPortIndex: selectedPortIndex ?? this.selectedPortIndex,
      selectedLocationIndex:
          selectedLocationIndex ?? this.selectedLocationIndex,
      selectedVehicleIndex: selectedVehicleIndex ?? this.selectedVehicleIndex,
      selectedVehicleDriverIndex: selectedVehicleIndex ?? this.selectedVehicleDriverIndex,
      selectedVehicleOperatorIndex: selectedVehicleOperatorIndex ?? this.selectedVehicleOperatorIndex,
      selectedRoleIndex: selectedRoleIndex ?? this.selectedRoleIndex,
    );
  }

  UiRoadMovementHome.fromJson(Map json)
      : selectedPortIndex = json['selectedPortIndex'] as int,
        selectedLocationIndex = json['selectedLocationIndex'] as int,
        selectedVehicleIndex = json['selectedVehicleIndex'] as int,
        selectedVehicleDriverIndex = json['selectedVehicleDriverIndex'] as int,
        selectedVehicleOperatorIndex = json['selectedVehicleOperatorIndex'] as int,
        selectedRoleIndex = json['selectedRoleIndex'] as int;

  Map toJson() => {
        'selectedPortIndex': selectedPortIndex,
        'selectedLocationIndex': selectedLocationIndex,
        'selectedVehicleIndex': selectedVehicleIndex,
        'selectedVehicleDriverIndex': selectedVehicleDriverIndex,
        'selectedVehicleOperatorIndex': selectedVehicleOperatorIndex,
        'selectedRoleIndex': selectedRoleIndex
      };
}
