class UiRoadMovementSearch {
  final String waybill;
  final String legNumber;
  final String? eta;

  UiRoadMovementSearch({
    required this.waybill,
    required this.legNumber,
    required this.eta
  });

  factory UiRoadMovementSearch.initial() {
    return UiRoadMovementSearch(
      waybill: "",
      legNumber: "",
      eta: null
    );
  }

  UiRoadMovementSearch copyWith({
    String? waybill,
    String? legNumber,
    String? eta
  }) {
    return UiRoadMovementSearch(
        waybill: waybill ?? this.waybill,
        legNumber: legNumber ?? this.legNumber,
        eta: eta ?? this.eta
    );
  }

  UiRoadMovementSearch.fromJson(Map json)
      :
        waybill = json['waybill'] ?? '',
        legNumber = json['legNumber'] ?? '',
        eta = json['eta'] as dynamic
  ;

  Map toJson() => {
    'waybill': waybill,
    'legNumber': legNumber,
    'eta': eta
  };
}
