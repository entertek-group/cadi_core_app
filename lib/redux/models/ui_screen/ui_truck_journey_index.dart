class UiTruckJourneyIndex {
  final int selectedPortIndex;
  final int selectedLocationIndex;
  final int selectedCarrierIndex;
  final int selectedRoleIndex;

  UiTruckJourneyIndex(
      {required this.selectedPortIndex,
      required this.selectedLocationIndex,
      required this.selectedCarrierIndex,
      required this.selectedRoleIndex});

  factory UiTruckJourneyIndex.initial() {
    return UiTruckJourneyIndex(
        selectedPortIndex: 0,
        selectedLocationIndex: 0,
        selectedCarrierIndex: 0,
        selectedRoleIndex: 0);
  }

  UiTruckJourneyIndex copyWith(
      {int? selectedPortIndex,
      int? selectedLocationIndex,
      int? selectedCarrierIndex,
      int? selectedRoleIndex}) {
    return UiTruckJourneyIndex(
        selectedPortIndex: selectedPortIndex ?? this.selectedPortIndex,
        selectedLocationIndex:
            selectedLocationIndex ?? this.selectedLocationIndex,
        selectedCarrierIndex: selectedCarrierIndex ?? this.selectedCarrierIndex,
        selectedRoleIndex: selectedRoleIndex ?? this.selectedRoleIndex);
  }

  UiTruckJourneyIndex.fromJson(Map json)
      : selectedPortIndex = json['selectedPortIndex'] as int,
        selectedLocationIndex = json['selectedLocationIndex'] as int,
        selectedCarrierIndex = json['selectedCarrierIndex'] as int,
        selectedRoleIndex = json['selectedRoleIndex'] as int;

  Map toJson() => {
        'selectedPortIndex': selectedPortIndex,
        'selectedLocationIndex': selectedLocationIndex,
        'selectedCarrierIndex': selectedCarrierIndex,
        'selectedRoleIndex': selectedRoleIndex
      };
}
