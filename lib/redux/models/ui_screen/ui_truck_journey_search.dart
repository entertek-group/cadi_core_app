class UiTruckJourneySearch {
  final String vesselName;
  final String voyageNumber;
  final String voyageDate;

  UiTruckJourneySearch(
      {required this.vesselName,
      required this.voyageNumber,
      required this.voyageDate});

  factory UiTruckJourneySearch.initial() {
    return UiTruckJourneySearch(
        vesselName: "", voyageNumber: "", voyageDate: "");
  }

  UiTruckJourneySearch copyWith(
      {String? vesselName, String? voyageNumber, String? voyageDate}) {
    return UiTruckJourneySearch(
        vesselName: vesselName ?? this.vesselName,
        voyageNumber: voyageNumber ?? this.voyageNumber,
        voyageDate: voyageDate ?? this.voyageDate);
  }

  UiTruckJourneySearch.fromJson(Map json)
      : vesselName = json['vesselName'],
        voyageNumber = json['voyageNumber'],
        voyageDate = json['voyageDate'];

  Map toJson() => {
        'vesselName': vesselName,
        'voyageNumber': voyageNumber,
        'voyageDate': voyageDate
      };
}
