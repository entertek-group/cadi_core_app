class UiTruckJourneyVessel {
  final bool isNew;

  UiTruckJourneyVessel({required this.isNew});

  factory UiTruckJourneyVessel.initial() {
    return UiTruckJourneyVessel(isNew: false);
  }

  UiTruckJourneyVessel copyWith({bool? isNew}) {
    return UiTruckJourneyVessel(isNew: isNew ?? this.isNew);
  }

  UiTruckJourneyVessel.fromJson(Map json) : isNew = json['isNew'] as bool;

  Map toJson() => {'isNew': isNew};
}
