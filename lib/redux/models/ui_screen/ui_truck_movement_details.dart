class UiTruckMovementDetails {
  final String truckRegistration;
  final String truckDriverId;

  UiTruckMovementDetails(
      {required this.truckRegistration, required this.truckDriverId});

  factory UiTruckMovementDetails.initial() {
    return UiTruckMovementDetails(truckRegistration: "", truckDriverId: "");
  }

  UiTruckMovementDetails copyWith(
      {String? truckRegistration, String? truckDriverId}) {
    return UiTruckMovementDetails(
        truckRegistration: truckRegistration ?? this.truckRegistration,
        truckDriverId: truckDriverId ?? this.truckDriverId);
  }

  UiTruckMovementDetails.fromJson(Map json)
      : truckRegistration = json['truckRegistration'],
        truckDriverId = json['truckDriverId'];

  Map toJson() =>
      {'truckRegistration': truckRegistration, 'truckDriverId': truckDriverId};
}
