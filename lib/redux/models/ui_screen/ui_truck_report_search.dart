import 'package:cadi_core_app/helpers/constants.dart';

class UiTruckReportSearch {
  final String reportType;
  final int selectedPortIndex;
  final int selectedLocationIndex;
  final String vesselName;
  final String voyageNumber;

  UiTruckReportSearch(
      {required this.reportType,
      required this.selectedPortIndex,
      required this.selectedLocationIndex,
      required this.vesselName,
      required this.voyageNumber});

  factory UiTruckReportSearch.initial() {
    return UiTruckReportSearch(
        reportType: CadiCargoMovementShipment.container,
        selectedPortIndex: 0,
        selectedLocationIndex: 0,
        vesselName: "",
        voyageNumber: "");
  }

  UiTruckReportSearch copyWith(
      {String? reportType,
      int? selectedPortIndex,
      int? selectedLocationIndex,
      String? vesselName,
      String? voyageNumber}) {
    return UiTruckReportSearch(
        reportType: reportType ?? this.reportType,
        selectedPortIndex: selectedPortIndex ?? this.selectedPortIndex,
        selectedLocationIndex:
            selectedLocationIndex ?? this.selectedLocationIndex,
        vesselName: vesselName ?? this.vesselName,
        voyageNumber: voyageNumber ?? this.voyageNumber);
  }

  UiTruckReportSearch.fromJson(Map json)
      : reportType = json['reportType'],
        selectedPortIndex = json['selectedPortIndex'] as int,
        selectedLocationIndex = json['selectedLocationIndex'] as int,
        vesselName = json['vesselName'],
        voyageNumber = json['voyageNumber'];

  Map toJson() => {
        'reportType': reportType,
        'selectedPortIndex': selectedPortIndex,
        'selectedLocationIndex': selectedLocationIndex,
        'vesselName': vesselName,
        'voyageNumber': voyageNumber
      };
}
