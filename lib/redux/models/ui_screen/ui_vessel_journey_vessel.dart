class UiVesselJourneyVessel {
  final bool isNew;

  UiVesselJourneyVessel({required this.isNew});

  factory UiVesselJourneyVessel.initial() {
    return UiVesselJourneyVessel(isNew: false);
  }

  UiVesselJourneyVessel copyWith({bool? isNew}) {
    return UiVesselJourneyVessel(isNew: isNew ?? this.isNew);
  }

  UiVesselJourneyVessel.fromJson(Map json) : isNew = json['isNew'] as bool;

  Map toJson() => {'isNew': isNew};
}
