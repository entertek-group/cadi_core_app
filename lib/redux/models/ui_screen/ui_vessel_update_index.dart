class UiVesselUpdateIndex {
  final String vesselName;
  final String voyageNumber;
  final String voyageDate;
  final int selectedPortIndex;
  final int selectedLocationIndex;

  UiVesselUpdateIndex({
    required this.vesselName,
    required this.voyageNumber,
    required this.voyageDate,
    required this.selectedPortIndex,
    required this.selectedLocationIndex,
  });

  factory UiVesselUpdateIndex.initial() {
    return UiVesselUpdateIndex(
      vesselName: "",
      voyageNumber: "",
      voyageDate: "",
      selectedPortIndex: 0,
      selectedLocationIndex: 0,
    );
  }

  UiVesselUpdateIndex copyWith(
      {int? selectedPortIndex,
      int? selectedLocationIndex,
      String? vesselName,
      String? voyageNumber,
      String? voyageDate}) {
    return UiVesselUpdateIndex(
        selectedPortIndex: selectedPortIndex ?? this.selectedPortIndex,
        selectedLocationIndex:
            selectedLocationIndex ?? this.selectedLocationIndex,
        vesselName: vesselName ?? this.vesselName,
        voyageNumber: voyageNumber ?? this.voyageNumber,
        voyageDate: voyageDate ?? this.voyageDate);
  }

  UiVesselUpdateIndex.fromJson(Map json)
      : selectedPortIndex = json['selectedPortIndex'] as int,
        selectedLocationIndex = json['selectedLocationIndex'] as int,
        vesselName = json['vesselName'],
        voyageNumber = json['voyageNumber'],
        voyageDate = json['voyageDate'];

  Map toJson() => {
        'selectedPortIndex': selectedPortIndex,
        'selectedLocationIndex': selectedLocationIndex,
        'vesselName': vesselName,
        'voyageNumber': voyageNumber,
        'voyageDate': voyageDate
      };
}
