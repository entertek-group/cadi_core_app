class User {
  final int id;
  final String email;
  final String name;
  final String timezone;
  final String avatar;
  final String mobileNumber;
  final String firstName;
  final String lastName;
  final String username;

  //final UserData userData;

  User({
    required this.id,
    required this.email,
    required this.name,
    required this.timezone,
    required this.avatar,
    required this.mobileNumber,
    required this.firstName,
    required this.lastName,
    required this.username
    //required this.userData
  });

  factory User.initial() {
    return User(
        id: 0,
        email: "",
        name: "",
        timezone: "",
        avatar: "",
        mobileNumber: "",
        firstName: "",
        lastName: "",
        username: ""
    );
  }

  User copyWith(
      {int? id,
      String? email,
      String? name,
      String? timezone,
      String? avatar,
      String? mobileNumber,
      String? firstName,
      String? lastName,
      String? username
      }) {
    return User(
        id: id ?? 0,
        email: email ?? "",
        name: name ?? "",
        timezone: timezone ?? "",
        avatar: avatar ?? "",
        mobileNumber: mobileNumber ?? "",
        firstName: firstName ?? "",
        lastName: lastName ?? "",
        username: username ?? ""
        //userData: userData ?? this.userData
        );
  }

  User.fromJson(Map json)
      : id = json['id'] as int,
        email = json['email'] ?? "",
        name = json['fullName'] ?? "",
        timezone = json['timezone'] ?? "",
        avatar = json['avatar'] ?? "",
        mobileNumber = json['mobileNumber'] ?? "",
        firstName = json['firstName'] ?? "",
        lastName = json['lastName'] ?? "",
        username = json['username'] ?? ""
  ;

  Map toJson() => {
        'id': id,
        'email': email,
        'fullName': name,
        'timezone': timezone,
        'avatar': avatar,
        'mobileNumber': mobileNumber,
        'firstName': firstName,
        'lastName': lastName,
        'username': username
      };
}
