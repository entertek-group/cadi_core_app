class Vessel {
  final int id;
  final String vesselName;
  String? vesselClass;
  String? vesselType;
  String? fuelType;

  Vessel(
      {required this.id,
      required this.vesselName,
      required this.vesselClass,
      required this.vesselType,
      required this.fuelType});

  factory Vessel.initial() {
    return Vessel(
        id: 0, vesselName: "", vesselClass: "", vesselType: "", fuelType: "");
  }

  Vessel copyWith(
      {int? id,
      String? vesselName,
      String? vesselClass,
      String? vesselType,
      String? fuelType}) {
    return Vessel(
        id: id ?? this.id,
        vesselName: vesselName ?? this.vesselName,
        vesselClass: vesselClass ?? this.vesselClass,
        vesselType: vesselType ?? this.vesselType,
        fuelType: fuelType ?? this.fuelType);
  }

  Vessel.fromJson(Map json)
      : id = json['id'] as int,
        vesselName = json['name'] ?? "",
        vesselClass = json['vessel_class'] as dynamic,
        vesselType = json['vessel_type'] as dynamic,
        fuelType = json['fuel_type'] as dynamic;

  Map toJson() => {
        'id': id,
        'name': vesselName,
        'vessel_class': vesselClass,
        'vessel_type': vesselType,
        'fuel_type': fuelType
      };
}
