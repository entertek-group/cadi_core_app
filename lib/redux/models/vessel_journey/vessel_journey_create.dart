class VesselJourneyCreate {
  final int userId;
  final int accountId;
  final int accountPortDepotId;
  final int carrierId;
  int? sailingScheduleId;
  final String role;
  final String voyageNumber;
  final String vesselName;
  final String vesselClass;
  final String fuelType;
  final String refuelDate;
  final num refuelVolume;
  final String refuelVolumeUnit;
  final num refuelPrice;
  final String refuelPriceCurrency;
  final num refuelVesselHours;
  final String refuelComment;
  final String comment;
  final bool matched;
  final String journeyAt;
  //Image
  final String vesselImage;
  final String vesselImageName;

  VesselJourneyCreate({
    required this.userId,
    required this.accountId,
    required this.accountPortDepotId,
    required this.carrierId,
    required this.sailingScheduleId,
    required this.role,
    required this.voyageNumber,
    required this.vesselName,
    required this.vesselClass,
    required this.fuelType,
    required this.refuelDate,
    required this.refuelVolume,
    required this.refuelVolumeUnit,
    required this.refuelPrice,
    required this.refuelPriceCurrency,
    required this.refuelVesselHours,
    required this.refuelComment,
    required this.comment,
    required this.matched,
    required this.journeyAt,
    required this.vesselImage,
    required this.vesselImageName
  });

  factory VesselJourneyCreate.initial() {
    return VesselJourneyCreate(
        userId: 0,
        accountId: 0,
        accountPortDepotId: 0,
        carrierId: 0,
        sailingScheduleId: null,
        role: "",
        voyageNumber: "",
        vesselName: "",
        vesselClass: "",
        fuelType: "",
        refuelDate: "",
        refuelVolume: 0.00,
        refuelVolumeUnit: "l",
        refuelPrice: 0.00,
        refuelPriceCurrency: "USD",
        refuelVesselHours: 0.00,
        refuelComment: "",
        comment: "",
        matched: false,
        journeyAt: "",
        vesselImage: "",
        vesselImageName: ""
    );
  }

  VesselJourneyCreate copyWith({
    int? userId,
    int? accountId,
    int? accountPortDepotId,
    int? carrierId,
    int? sailingScheduleId,
    String? role,
    String? voyageNumber,
    String? vesselName,
    String? vesselClass,
    String? fuelType,
    String? refuelDate,
    num? refuelVolume,
    String? refuelVolumeUnit,
    num? refuelPrice,
    String? refuelPriceCurrency,
    num? refuelVesselHours,
    String? refuelComment,
    String? comment,
    bool? matched,
    String? journeyAt,
    String? vesselImage,
    String? vesselImageName,
  }) {
    return VesselJourneyCreate(
      userId: userId ?? this.userId,
      accountId: accountId ?? this.accountId,
      accountPortDepotId: accountPortDepotId ?? this.accountPortDepotId,
      carrierId: carrierId ?? this.carrierId,
      sailingScheduleId: sailingScheduleId ?? this.sailingScheduleId,
      role: role ?? this.role,
      voyageNumber: voyageNumber ?? this.voyageNumber,
      vesselName: vesselName ?? this.vesselName,
      vesselClass: vesselClass ?? this.vesselClass,
      fuelType: fuelType ?? this.fuelType,
      refuelDate: refuelDate ?? this.refuelDate,
      refuelVolume: refuelVolume ?? this.refuelVolume,
      refuelVolumeUnit: refuelVolumeUnit ?? this.refuelVolumeUnit,
      refuelPrice: refuelPrice ?? this.refuelPrice,
      refuelPriceCurrency: refuelPriceCurrency ?? this.refuelPriceCurrency,
      refuelVesselHours: refuelVesselHours ?? this.refuelVesselHours,
      refuelComment: refuelComment ?? this.refuelComment,
      comment: comment ?? this.comment,
      matched: matched ?? this.matched,
      journeyAt: journeyAt ?? this.journeyAt,
      vesselImage: vesselImage ?? this.vesselImage,
      vesselImageName: vesselImageName ?? this.vesselImageName
    );
  }

  VesselJourneyCreate.fromJson(Map json)
      :
        userId = json['user_id'] as int,
        accountId = json['account_id'] as int,
        accountPortDepotId = json['account_port_depot_id'] as int,
        carrierId = json['carrier_id'] as int,
        sailingScheduleId = json['sailing_schedule_id'] as dynamic,
        role = json['role'],
        voyageNumber = json['voyage_number'] ?? "",
        vesselName = json['vessel_name'],
        vesselClass = json['vessel_class'],
        fuelType = json['fuel_type'],
        refuelDate = json['refuel_date'],
        refuelVolume = json['refuel_volume'] as num,
        refuelVolumeUnit = json['refuel_volume_unit'],
        refuelPrice = json['refuel_price'] as num,
        refuelPriceCurrency = json['refuel_price_currency'],
        refuelVesselHours = json['refuel_vessel_hours'] as num,
        refuelComment = json['refuel_comment'],
        comment = json['comment'],
        matched = json['matched'] as bool,
        journeyAt = json['journey_at'],
        vesselImage = json['vessel_image'] ?? "",
        vesselImageName = json['vessel_image_name'] ?? ""
  ;

  Map toJson() => {
    'user_id': userId,
    'account_id': accountId,
    'account_port_depot_id': accountPortDepotId,
    'carrier_id': carrierId,
    'sailing_schedule_id': sailingScheduleId,
    'role': role,
    'voyage_number': voyageNumber,
    'vessel_name': vesselName,
    'vessel_class': vesselClass,
    'fuel_type': fuelType,
    'refuel_date': refuelDate,
    'refuel_volume': refuelVolume,
    'refuel_volume_unit': refuelVolumeUnit,
    'refuel_price': refuelPrice,
    'refuel_price_currency': refuelPriceCurrency,
    'refuel_vessel_hours': refuelVesselHours,
    'refuel_comment': refuelComment,
    'comment': comment,
    'matched': matched,
    'journey_at': journeyAt,
    'vessel_image': vesselImage,
    'vessel_image_name': vesselImageName
  };
}
