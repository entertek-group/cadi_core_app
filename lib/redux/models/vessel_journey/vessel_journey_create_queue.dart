import 'package:cadi_core_app/redux/models/vessel_journey/vessel_journey_models.dart';

class VesselJourneyCreateQueue {
  final String uniqueId;
  final VesselJourneyCreate vesselJourneyCreate;
  final bool complete;
  final String createdAt;
  final String error;

  VesselJourneyCreateQueue(
      {required this.uniqueId,
      required this.vesselJourneyCreate,
      required this.complete,
      required this.createdAt,
      required this.error});

  factory VesselJourneyCreateQueue.initial() {
    return VesselJourneyCreateQueue(
        uniqueId: "",
        vesselJourneyCreate: VesselJourneyCreate.initial(),
        complete: true,
        createdAt: DateTime.now().toString(),
        error: "");
  }

  VesselJourneyCreateQueue copyWith(
      {String? uniqueId,
      VesselJourneyCreate? vesselJourneyCreate,
      bool? complete,
      String? createdAt,
      String? error}) {
    return VesselJourneyCreateQueue(
        uniqueId: uniqueId ?? this.uniqueId,
        vesselJourneyCreate: vesselJourneyCreate ?? this.vesselJourneyCreate,
        complete: complete ?? this.complete,
        createdAt: createdAt ?? this.createdAt,
        error: error ?? this.error);
  }

  VesselJourneyCreateQueue.fromJson(Map json)
      : uniqueId = json['unique_id'],
        vesselJourneyCreate = json['vessel_journey'] != null
            ? VesselJourneyCreate.fromJson(json['vessel_journey'])
            : VesselJourneyCreate.initial(),
        complete = json['complete'] as bool,
        createdAt = json['createdAt'],
        error = json['error'];

  Map toJson() => {
        'unique_id': uniqueId,
        'vessel_journey': vesselJourneyCreate,
        'complete': complete,
        'createdAt': createdAt,
        'error': error
      };
}
