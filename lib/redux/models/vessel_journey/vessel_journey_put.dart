// import 'package:cadi_core_app/redux/models/models.dart';
import 'vessel_journey.dart';

class VesselJourneyPut {
  VesselJourney vesselJourney;

  VesselJourneyPut({required this.vesselJourney});

  factory VesselJourneyPut.initial() {
    return VesselJourneyPut(vesselJourney: VesselJourney.initial());
  }

  VesselJourneyPut copyWith({VesselJourney? vesselJourney
      //BOLContainerList? containers
      }) {
    return VesselJourneyPut(
      vesselJourney: vesselJourney ?? this.vesselJourney,
    );
  }

  VesselJourneyPut.fromJson(Map json)
      : vesselJourney = VesselJourney.fromJson(json['vessel_journey']);

  Map toJson() => {'vessel_journey': vesselJourney};
}
