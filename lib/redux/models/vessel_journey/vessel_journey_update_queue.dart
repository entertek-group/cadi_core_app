import 'package:cadi_core_app/redux/models/vessel_journey/vessel_journey_models.dart';

class VesselJourneyUpdateQueue {
  final String uniqueId;
  final VesselJourney vesselJourney;
  final bool complete;
  final String createdAt;
  final String error;

  VesselJourneyUpdateQueue(
      {required this.uniqueId,
      required this.vesselJourney,
      required this.complete,
      required this.createdAt,
      required this.error});

  factory VesselJourneyUpdateQueue.initial() {
    return VesselJourneyUpdateQueue(
        uniqueId: "",
        vesselJourney: VesselJourney.initial(),
        complete: true,
        createdAt: DateTime.now().toString(),
        error: "");
  }

  VesselJourneyUpdateQueue copyWith(
      {String? uniqueId,
      VesselJourney? vesselJourney,
      bool? complete,
      String? createdAt,
      String? error}) {
    return VesselJourneyUpdateQueue(
        uniqueId: uniqueId ?? this.uniqueId,
        vesselJourney: vesselJourney ?? this.vesselJourney,
        complete: complete ?? this.complete,
        createdAt: createdAt ?? this.createdAt,
        error: error ?? this.error);
  }

  VesselJourneyUpdateQueue.fromJson(Map json)
      : uniqueId = json['unique_id'],
        vesselJourney = json['vessel_journey'] != null
            ? VesselJourney.fromJson(json['vessel_journey'])
            : VesselJourney.initial(),
        complete = json['complete'] as bool,
        createdAt = json['createdAt'],
        error = json['error'];

  Map toJson() => {
        'unique_id': uniqueId,
        'vessel_journey': vesselJourney,
        'complete': complete,
        'createdAt': createdAt,
        'error': error
      };
}
