import 'package:cadi_core_app/redux/actions/account_actions.dart';
import 'package:cadi_core_app/redux/state/account_state.dart';
import 'package:redux/redux.dart';

final accountReducer = combineReducers<AccountState>([
  TypedReducer<AccountState, GetAccountsSuccess>(_getAccountsSuccess).call,
  TypedReducer<AccountState, GetAccountsError>(_getAccountsError).call,
  TypedReducer<AccountState, FilterByAccountSelection>(
      _filterByAccountSelection).call,
  TypedReducer<AccountState, GetAccountPortDepotsSuccess>(
      _getAccountPortDepotsSuccess).call,
  TypedReducer<AccountState, GetAccountPortDepotsError>(
      _getAccountPortDepotsError).call,
  TypedReducer<AccountState, SetActiveService>(_setActiveService).call,
  TypedReducer<AccountState, GetAccountVehiclesSuccess>(
      _getAccountVehiclesSuccess).call,
  TypedReducer<AccountState, GetAccountVehiclesError>(
      _getAccountVehiclesError).call,
  TypedReducer<AccountState, GetAccountVehicleDriversSuccess>(
      _getAccountVehicleDriversSuccess).call,
  TypedReducer<AccountState, GetAccountVehicleDriversError>(
      _getAccountVehicleDriversError).call,
  TypedReducer<AccountState, GetAccountVehicleOperatorsSuccess>(
      _getAccountVehicleOperatorsSuccess).call,
  TypedReducer<AccountState, GetAccountVehicleOperatorsError>(
      _getAccountVehicleOperatorsError).call,
  TypedReducer<AccountState, GetAccountDocumentTypesSuccess>(
      _getAccountDocumentTypesSuccess).call,
  TypedReducer<AccountState, GetAccountDocumentTypesError>(
      _getAccountDocumentTypesError).call,
  TypedReducer<AccountState, GetAccountContainerTypesSuccess>(
      _getAccountContainerTypesSuccess).call,
  TypedReducer<AccountState, GetAccountContainerTypesError>(
      _getAccountContainerTypesError).call,
  TypedReducer<AccountState, GetAccountCarriersSuccess>(
      _getAccountCarriersSuccess).call,
  TypedReducer<AccountState, GetAccountCarriersError>(_getAccountCarriersError).call,
  TypedReducer<AccountState, GetAccountVesselsSuccess>(
      _getAccountVesselsSuccess).call,
  TypedReducer<AccountState, GetAccountVesselsError>(_getAccountVesselsError).call,
  TypedReducer<AccountState, GetAccountCranesSuccess>(
      _getAccountCranesSuccess).call,
  TypedReducer<AccountState, GetAccountCranesError>(_getAccountCranesError).call,
  TypedReducer<AccountState, GetAccountBerthsSuccess>(
      _getAccountBerthsSuccess).call,
  TypedReducer<AccountState, GetAccountBerthsError>(_getAccountBerthsError).call,
]);

AccountState _getAccountsSuccess(
    AccountState state, GetAccountsSuccess action) {
  return state.copyWith(
      accountList: action.accountList, account: action.account, error: "");
}

AccountState _getAccountsError(AccountState state, GetAccountsError action) {
  return state.copyWith(error: action.error);
}

AccountState _filterByAccountSelection(
    AccountState state, FilterByAccountSelection action) {
  return state.copyWith(
      account: action.account,
      filteredAccountPortDepots: action.filteredAccountPortDepots,
      filteredAccountPorts: action.filteredAccountPorts,
      filteredAccountContainerTypes: action.filteredAccountContainerTypes,
      filteredAccountVehicleDrivers: action.filteredAccountVehicleDrivers,
      filteredAccountVehicleOperators: action.filteredAccountVehicleOperators,
      filteredAccountDocumentTypes: action.filteredAccountDocumentTypes,
      filteredAccountCarriers: action.filteredAccountCarriers,
      filteredAccountVessels: action.filteredAccountVessels,
      filteredAccountCranes: action.filteredAccountCranes,
      filteredAccountVehicles: action.filteredAccountVehicles,
      filteredAccountBerths: action.filteredAccountBerths
  );
}

AccountState _getAccountPortDepotsSuccess(
    AccountState state, GetAccountPortDepotsSuccess action) {
  return state.copyWith(
      accountPortDepots: action.accountPortDepots,
      accountPorts: action.accountPorts,
      error: "");
}

AccountState _getAccountPortDepotsError(
    AccountState state, GetAccountPortDepotsError action) {
  return state.copyWith(error: action.error);
}

AccountState _setActiveService(AccountState state, SetActiveService action) {
  return state.copyWith(activeService: action.activeService);
}

AccountState _getAccountVehiclesSuccess(
    AccountState state, GetAccountVehiclesSuccess action) {
  return state.copyWith(
      accountVehicles: action.accountVehicles, error: "");
}

AccountState _getAccountVehiclesError(
    AccountState state, GetAccountVehiclesError action) {
  return state.copyWith(error: action.error);
}

AccountState _getAccountVehicleDriversSuccess(
    AccountState state, GetAccountVehicleDriversSuccess action) {
  return state.copyWith(
      accountVehicleDrivers: action.accountVehicleDrivers, error: "");
}

AccountState _getAccountVehicleDriversError(
    AccountState state, GetAccountVehicleDriversError action) {
  return state.copyWith(error: action.error);
}

AccountState _getAccountVehicleOperatorsSuccess(
    AccountState state, GetAccountVehicleOperatorsSuccess action) {
  return state.copyWith(
      accountVehicleOperators: action.accountVehicleOperators, error: "");
}

AccountState _getAccountVehicleOperatorsError(
    AccountState state, GetAccountVehicleOperatorsError action) {
  return state.copyWith(error: action.error);
}

AccountState _getAccountDocumentTypesSuccess(
    AccountState state, GetAccountDocumentTypesSuccess action) {
  return state.copyWith(
      accountDocumentTypes: action.accountDocumentTypes, error: "");
}

AccountState _getAccountDocumentTypesError(
    AccountState state, GetAccountDocumentTypesError action) {
  return state.copyWith(error: action.error);
}

AccountState _getAccountContainerTypesSuccess(
    AccountState state, GetAccountContainerTypesSuccess action) {
  return state.copyWith(
      accountContainerTypes: action.accountContainerTypes, error: "");
}

AccountState _getAccountContainerTypesError(
    AccountState state, GetAccountContainerTypesError action) {
  return state.copyWith(error: action.error);
}

AccountState _getAccountCarriersSuccess(
    AccountState state, GetAccountCarriersSuccess action) {
  return state.copyWith(accountCarriers: action.accountCarriers, error: "");
}

AccountState _getAccountCarriersError(
    AccountState state, GetAccountCarriersError action) {
  return state.copyWith(error: action.error);
}

AccountState _getAccountVesselsSuccess(
    AccountState state, GetAccountVesselsSuccess action) {
  return state.copyWith(accountVessels: action.accountVessels, error: "");
}

AccountState _getAccountVesselsError(
    AccountState state, GetAccountVesselsError action) {
  return state.copyWith(error: action.error);
}

AccountState _getAccountCranesSuccess(
    AccountState state, GetAccountCranesSuccess action) {
  return state.copyWith(accountCranes: action.accountCranes, error: "");
}

AccountState _getAccountCranesError(
    AccountState state, GetAccountCranesError action) {
  return state.copyWith(error: action.error);
}

AccountState _getAccountBerthsSuccess(
    AccountState state, GetAccountBerthsSuccess action) {
  return state.copyWith(accountBerths: action.accountBerths, error: "");
}

AccountState _getAccountBerthsError(
    AccountState state, GetAccountBerthsError action) {
  return state.copyWith(error: action.error);
}
