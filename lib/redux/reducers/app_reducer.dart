import 'package:cadi_core_app/redux/reducers/reducers.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:cadi_core_app/redux/actions/app_actions.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'reference_reducer.dart';
import 'package:redux/redux.dart';
import 'storage_reducer.dart';
import 'report_reducer.dart';

AppState appReducer(AppState state, action) {
  if (action is CompleteSignOut) {
    return AppState.initialWithoutWelcome();
  }

  return AppState(
    isLoading: loadingReducer(state.isLoading, action),
    isDownloading: downloadingReducer(state.isDownloading, action),
    isSubmitting: submittingReducer(state.isSubmitting, action),
    welcomeShown: welcomeReducer(state.welcomeShown, action),
    homeIndex: indexReducer(state.homeIndex, action),
    authState: authReducer(state.authState, action),
    cargoMovementState: cargoMovementReducer(state.cargoMovementState, action),
    portCargoUpdateState:
        portCargoUpdateReducer(state.portCargoUpdateState, action),
    accountState: accountReducer(state.accountState, action),
    connectivityState: connectivityReducer(state.connectivityState, action),
    referenceState: referenceReducer(state.referenceState, action),
    uiState: uiReducer(state.uiState, action),
    storageState: storageReducer(state.storageState, action),
    reportState: reportReducer(state.reportState, action),
    portEventState: portEventReducer(state.portEventState, action),
    roadMovementState: roadMovementReducer(state.roadMovementState, action),
    processingIndex: processingIndexReducer(state.processingIndex, action),
    processingTotal: processingTotalReducer(state.processingTotal, action),
  );
}

final loadingReducer = combineReducers<bool>([
  TypedReducer<bool, StartLoading>(_setLoading).call,
  TypedReducer<bool, StopLoading>(_setLoaded).call,
]);

bool _setLoading(bool state, StartLoading action) {
  return true;
}

bool _setLoaded(bool state, StopLoading action) {
  return false;
}

final indexReducer = combineReducers<int>([
  TypedReducer<int, SetHomeIndex>(_setHomeIndex).call,
]);

int _setHomeIndex(int state, SetHomeIndex action) {
  return action.index;
}

final downloadingReducer = combineReducers<bool>([
  TypedReducer<bool, StartDownloading>(_setDownloading).call,
  TypedReducer<bool, StopDownloading>(_setDownloaded).call,
]);

bool _setDownloading(bool state, StartDownloading action) {
  return true;
}

bool _setDownloaded(bool state, StopDownloading action) {
  return false;
}

final processingIndexReducer = combineReducers<int>([
  TypedReducer<int, SetProcessingIndex>(_setProcessingIndex).call,
]);

int _setProcessingIndex(int state, SetProcessingIndex action) {
  return action.processingIndex;
}

final processingTotalReducer = combineReducers<int>([
  TypedReducer<int, SetProcessingTotal>(_setProcessingTotal).call,
]);

int _setProcessingTotal(int state, SetProcessingTotal action) {
  return action.processingTotal;
}

final submittingReducer = combineReducers<bool>([
  TypedReducer<bool, StartSubmitting>(_setSubmitting).call,
  TypedReducer<bool, StopSubmitting>(_setSubmitted).call,
]);

bool _setSubmitting(bool state, StartSubmitting action) {
  return true;
}

bool _setSubmitted(bool state, StopSubmitting action) {
  return false;
}

final welcomeReducer = combineReducers<bool>([
  TypedReducer<bool, StopShowingWelcome>(_stopShowingWelcome).call,
]);

bool _stopShowingWelcome(bool state, StopShowingWelcome action) {
  return true;
}
