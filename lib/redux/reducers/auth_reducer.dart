import 'package:cadi_core_app/redux/actions/auth_actions.dart';
import 'package:cadi_core_app/redux/state/auth_state.dart';
import 'package:redux/redux.dart';

final authReducer = combineReducers<AuthState>([
  TypedReducer<AuthState, AuthenticateSuccess>(_authenticateSuccess).call,
  TypedReducer<AuthState, AuthenticateError>(_authenticateError).call,
  TypedReducer<AuthState, GetMyUserSuccess>(_getMyUserSuccess).call,
  TypedReducer<AuthState, GetMyUserError>(_getMyUserError).call,
  TypedReducer<AuthState, SignOutSuccess>(_signOutSuccess).call,
  TypedReducer<AuthState, UpdateUserSuccess>(_updateUserSuccess).call,
  TypedReducer<AuthState, UpdateUserError>(_updateUserError).call,
  TypedReducer<AuthState, UpdateUserPasswordSuccess>(
      _updateUserPasswordSuccess).call,
  TypedReducer<AuthState, UpdateUserPasswordError>(_updateUserPasswordError).call,
  TypedReducer<AuthState, UpdateUserAvatarSuccess>(_updateUserAvatarSuccess).call,
  TypedReducer<AuthState, UpdateUserAvatarError>(_updateUserAvatarError).call,
  TypedReducer<AuthState, ForgetPassword>(_forgetPassword).call,
  TypedReducer<AuthState, ForgetPasswordSuccess>(_forgetPasswordSuccess).call,
  TypedReducer<AuthState, ForgetPasswordError>(_forgetPasswordError).call,
]);

AuthState _authenticateSuccess(AuthState state, AuthenticateSuccess action) {
  return state.copyWith(
      token: action.token,
      isAuthenticated: true,
      error: "",
      isInitialized: false,
      password: "",
      email: action.email);
}

AuthState _authenticateError(AuthState state, AuthenticateError action) {
  return state.copyWith(error: action.error);
}

AuthState _getMyUserSuccess(AuthState state, GetMyUserSuccess action) {
  return state.copyWith(user: action.user, error: "");
}

AuthState _updateUserSuccess(AuthState state, UpdateUserSuccess action) {
  return state.copyWith(user: action.user, error: "");
}

AuthState _updateUserPasswordSuccess(
    AuthState state, UpdateUserPasswordSuccess action) {
  return state.copyWith(user: action.user, error: "");
}

AuthState _getMyUserError(AuthState state, GetMyUserError action) {
  return state.copyWith(error: action.error);
}

AuthState _updateUserError(AuthState state, UpdateUserError action) {
  return state.copyWith(error: action.error);
}

AuthState _updateUserPasswordError(
    AuthState state, UpdateUserPasswordError action) {
  return state.copyWith(error: action.error);
}

AuthState _signOutSuccess(AuthState state, SignOutSuccess action) {
  return state.copyWith(
      isAuthenticated: false,
      token: action.token,
      user: action.user,
      password: "",
      email: "",
      error: "");
}

AuthState _updateUserAvatarSuccess(
    AuthState state, UpdateUserAvatarSuccess action) {
  return state.copyWith(user: action.user, error: "");
}

AuthState _updateUserAvatarError(
    AuthState state, UpdateUserAvatarError action) {
  return state.copyWith(error: action.error);
}

AuthState _forgetPassword(AuthState state, ForgetPassword action) {
  return state.copyWith(error: "");
}

AuthState _forgetPasswordSuccess(
    AuthState state, ForgetPasswordSuccess action) {
  return state.copyWith(error: "");
}

AuthState _forgetPasswordError(AuthState state, ForgetPasswordError action) {
  return state.copyWith(error: action.error);
}
