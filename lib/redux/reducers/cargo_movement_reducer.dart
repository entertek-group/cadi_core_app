import 'package:cadi_core_app/redux/actions/cargo_movement_actions.dart';
import 'package:cadi_core_app/redux/state/cargo_movement_state.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:redux/redux.dart';

final cargoMovementReducer = combineReducers<CargoMovementState>([
  TypedReducer<CargoMovementState, UpdateQueueCargoMovement>(
      _updateQueueCargoMovement).call,
  TypedReducer<CargoMovementState, UpdateCargoMovementCreate>(
      _updateCargoMovementCreate).call,
  TypedReducer<CargoMovementState, ResetCargoMovementCreate>(
      _resetCargoMovementCreate).call,
  TypedReducer<CargoMovementState, FlagQueued>(_flagQueued).call,
  TypedReducer<CargoMovementState, UnFlagQueued>(_unFlagQueued).call,
  TypedReducer<CargoMovementState, StartProcessingQueue>(_startProcessingQueue).call,
  TypedReducer<CargoMovementState, StopProcessingQueue>(_stopProcessingQueue).call,
]);

CargoMovementState _updateQueueCargoMovement(
    CargoMovementState state, UpdateQueueCargoMovement action) {
  return state.copyWith(cargoCreateQueue: action.cargoCreateQueue, error: "");
}

CargoMovementState _updateCargoMovementCreate(
    CargoMovementState state, UpdateCargoMovementCreate action) {
  return state.copyWith(cargoMovementCreate: action.cargoMovementCreate);
}

CargoMovementState _resetCargoMovementCreate(
    CargoMovementState state, ResetCargoMovementCreate action) {
  return state.copyWith(cargoMovementCreate: CargoMovementCreate.initial());
}

CargoMovementState _flagQueued(CargoMovementState state, FlagQueued action) {
  return state.copyWith(queued: true);
}

CargoMovementState _unFlagQueued(
    CargoMovementState state, UnFlagQueued action) {
  return state.copyWith(queued: false);
}

CargoMovementState _startProcessingQueue(
    CargoMovementState state, StartProcessingQueue action) {
  return state.copyWith(processingQueue: true);
}

CargoMovementState _stopProcessingQueue(
    CargoMovementState state, StopProcessingQueue action) {
  return state.copyWith(processingQueue: false);
}
