import 'package:cadi_core_app/redux/actions/connectivity_actions.dart';
import 'package:cadi_core_app/redux/state/connectivity_state.dart';
import 'package:redux/redux.dart';

final connectivityReducer = combineReducers<ConnectivityState>([
  TypedReducer<ConnectivityState, UpdateConnectivity>(_updateConnectivity).call,
  TypedReducer<ConnectivityState, EnableDataSync>(_enableDataSync).call,
  TypedReducer<ConnectivityState, DisableDataSync>(_disableDataSync).call
]);

ConnectivityState _updateConnectivity(
    ConnectivityState state, UpdateConnectivity action) {
  return state.copyWith(
      connectionStatus: action.connectionStatus,
      statusAt: action.statusAt,
      error: "");
}

ConnectivityState _enableDataSync(
    ConnectivityState state, EnableDataSync action) {
  return state.copyWith(dataSync: true);
}

ConnectivityState _disableDataSync(
    ConnectivityState state, DisableDataSync action) {
  return state.copyWith(dataSync: false);
}
