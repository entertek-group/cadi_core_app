import 'package:cadi_core_app/redux/actions/port_cargo_update_actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/state/port_cargo_update_state.dart';
import 'package:redux/redux.dart';

final portCargoUpdateReducer = combineReducers<PortCargoUpdateState>([
  TypedReducer<PortCargoUpdateState, UpdateQueuePortCargoUpdate>(
          _updateQueuePortCargoUpdate)
      .call,
  TypedReducer<PortCargoUpdateState, UpdatePortCargoUpdateCreate>(
          _updatePortCargoUpdateCreate)
      .call,
  TypedReducer<PortCargoUpdateState, ResetPortCargoUpdateCreate>(
          _resetPortCargoUpdateCreate)
      .call,
  TypedReducer<PortCargoUpdateState, FlagPortCargoUpdateQueued>(_flagPortCargoUpdateQueued)
      .call,
  TypedReducer<PortCargoUpdateState, UnFlagPortCargoUpdateQueued>(_unFlagPortCargoUpdateQueued)
      .call,
  TypedReducer<PortCargoUpdateState, StartPortCargoUpdateProcessingQueue>(
          _startPortCargoUpdateProcessingQueue)
      .call,
  TypedReducer<PortCargoUpdateState, StopPortCargoUpdateProcessingQueue>(
          _stopPortCargoUpdateProcessingQueue)
      .call,
]);

PortCargoUpdateState _updateQueuePortCargoUpdate(
    PortCargoUpdateState state, UpdateQueuePortCargoUpdate action) {
  return state.copyWith(
      portCargoUpdateQueue: action.portCargoUpdateQueue, error: "");
}

PortCargoUpdateState _updatePortCargoUpdateCreate(
    PortCargoUpdateState state, UpdatePortCargoUpdateCreate action) {
  return state.copyWith(portCargoUpdateCreate: action.portCargoUpdateCreate);
}

PortCargoUpdateState _resetPortCargoUpdateCreate(
    PortCargoUpdateState state, ResetPortCargoUpdateCreate action) {
  return state.copyWith(portCargoUpdateCreate: PortCargoUpdateCreate.initial());
}

PortCargoUpdateState _flagPortCargoUpdateQueued(
    PortCargoUpdateState state, FlagPortCargoUpdateQueued action) {
  return state.copyWith(queued: true);
}

PortCargoUpdateState _unFlagPortCargoUpdateQueued(
    PortCargoUpdateState state, UnFlagPortCargoUpdateQueued action) {
  return state.copyWith(queued: false);
}

PortCargoUpdateState _startPortCargoUpdateProcessingQueue(
    PortCargoUpdateState state, StartPortCargoUpdateProcessingQueue action) {
  return state.copyWith(processingQueue: true);
}

PortCargoUpdateState _stopPortCargoUpdateProcessingQueue(
    PortCargoUpdateState state, StopPortCargoUpdateProcessingQueue action) {
  return state.copyWith(processingQueue: false);
}
