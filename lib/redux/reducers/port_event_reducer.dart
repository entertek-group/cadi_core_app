import 'package:cadi_core_app/redux/actions/port_event_actions.dart';
import 'package:cadi_core_app/redux/state/port_event_state.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:redux/redux.dart';

final portEventReducer = combineReducers<PortEventState>([
  TypedReducer<PortEventState, UpdatePortEventCreate>(
      _updatePortEventCreate).call,
  TypedReducer<PortEventState, UpdateQueuePortEvent>(
      _updateQueuePortEvent).call,
  TypedReducer<PortEventState, FlagPortEventQueued>(
      _flagPortEventQueued).call,
  TypedReducer<PortEventState, UnFlagPortEventQueued>(
      _unFlagPortEventQueued).call,
  TypedReducer<PortEventState, StartPortEventProcessingQueue>(
      _startPortEventProcessingQueue).call,
  TypedReducer<PortEventState, StopPortEventProcessingQueue>(
      _stopPortEventProcessingQueue).call,
]);

PortEventState _updatePortEventCreate (
    PortEventState state, UpdatePortEventCreate action) {
  return state.copyWith(portEventCreate: action.portEventCreate);
}

PortEventState _updateQueuePortEvent (
    PortEventState state, UpdateQueuePortEvent action) {
  return state.copyWith(portEventCreateQueue: action.portEventCreateQueue);
}

PortEventState _flagPortEventQueued(
    PortEventState state, FlagPortEventQueued action) {
  return state.copyWith(queued: true);
}

PortEventState _unFlagPortEventQueued(
    PortEventState state, UnFlagPortEventQueued action) {
  return state.copyWith(queued: false);
}

PortEventState _startPortEventProcessingQueue(
    PortEventState state, StartPortEventProcessingQueue action) {
  return state.copyWith(processingQueue: true);
}

PortEventState _stopPortEventProcessingQueue(
    PortEventState state, StopPortEventProcessingQueue action) {
  return state.copyWith(processingQueue: false);
}
