import 'package:cadi_core_app/redux/actions/reference_actions.dart';
import 'package:cadi_core_app/redux/state/reference_state.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:redux/redux.dart';

final referenceReducer = combineReducers<ReferenceState>([
  TypedReducer<ReferenceState, GetVesselClassesSuccess>(
      _getVesselClassesSuccess).call,
  TypedReducer<ReferenceState, GetVesselClassesError>(_getVesselClassesError).call,
  TypedReducer<ReferenceState, GetFuelTypesSuccess>(_getFuelTypesSuccess).call,
  TypedReducer<ReferenceState, GetFuelTypesError>(_getFuelTypesError).call,
  TypedReducer<ReferenceState, GetCargoTypesSuccess>(_getCargoTypesSuccess).call,
  TypedReducer<ReferenceState, GetCargoTypesError>(_getCargoTypesError).call,
  TypedReducer<ReferenceState, GetJobPackTypesSuccess>(_getJobPackTypesSuccess).call,
  TypedReducer<ReferenceState, GetJobPackTypesError>(_getJobPackTypesError).call,
]);

ReferenceState _getVesselClassesSuccess(
    ReferenceState state, GetVesselClassesSuccess action) {
  return state.copyWith(vesselClasses: action.vesselClasses, error: "");
}

ReferenceState _getVesselClassesError(
    ReferenceState state, GetVesselClassesError action) {
  return state.copyWith(error: action.error);
}

ReferenceState _getFuelTypesSuccess(
    ReferenceState state, GetFuelTypesSuccess action) {
  return state.copyWith(fuelTypes: action.fuelTypes, error: "");
}

ReferenceState _getFuelTypesError(
    ReferenceState state, GetFuelTypesError action) {
  return state.copyWith(error: action.error);
}

ReferenceState _getCargoTypesSuccess(
    ReferenceState state, GetCargoTypesSuccess action) {
  return state.copyWith(cargoTypes: action.cargoTypes, error: "");
}

ReferenceState _getCargoTypesError(
    ReferenceState state, GetCargoTypesError action) {
  return state.copyWith(error: action.error);
}

ReferenceState _getJobPackTypesSuccess(
    ReferenceState state, GetJobPackTypesSuccess action) {
  return state.copyWith(jobPackTypes: action.jobPackTypes, error: "");
}

ReferenceState _getJobPackTypesError(
    ReferenceState state, GetJobPackTypesError action) {
  return state.copyWith(error: action.error);
}
