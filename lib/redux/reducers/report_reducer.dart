import 'package:cadi_core_app/redux/actions/report_actions.dart';
import 'package:cadi_core_app/redux/state/report_state.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:redux/redux.dart';

final reportReducer = combineReducers<ReportState>([
  TypedReducer<ReportState, GetPortReportSuccess>(_getPortReportSuccess).call,
  TypedReducer<ReportState, GetPortReportError>(_getPortReportError).call,
  TypedReducer<ReportState, SetReportType>(_setReportType).call,
  TypedReducer<ReportState, GetPortMovementReportSuccess>(_getPortMovementReportSuccess).call,

]);

ReportState _getPortReportSuccess(
    ReportState state, GetPortReportSuccess action) {
  return state.copyWith(portReportSummary: action.portReportSummary, error: "");
}

ReportState _getPortReportError(
    ReportState state, GetPortReportError action) {
  return state.copyWith(error: action.error);
}

ReportState _setReportType(ReportState state, SetReportType action) {
  return state.copyWith(reportType: action.reportType);
}

ReportState _getPortMovementReportSuccess(
    ReportState state, GetPortMovementReportSuccess action) {
  return state.copyWith(portMovementReportSummary: action.portMovementReportSummary, error: "");
}

