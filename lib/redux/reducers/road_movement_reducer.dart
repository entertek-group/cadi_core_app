import 'package:cadi_core_app/redux/actions/road_movement_actions.dart';
import 'package:cadi_core_app/redux/models/road_movement/road_movement_create.dart';
import 'package:cadi_core_app/redux/state/road_movement_state.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:redux/redux.dart';

final roadMovementReducer = combineReducers<RoadMovementState>([
  TypedReducer<RoadMovementState, UpdateQueueRoadMovement>(
      _updateQueueTruckMovement).call,
  TypedReducer<RoadMovementState, UpdateRoadMovementCreate>(
      _updateRoadMovementCreate).call,
  TypedReducer<RoadMovementState, ResetRoadMovementCreate>(
      _resetRoadMovementCreate).call,
  TypedReducer<RoadMovementState, FlagRoadMovementQueued>(
      _flagRoadMovementQueued).call,
  TypedReducer<RoadMovementState, UnFlagRoadMovementQueued>(
      _unFlagRoadMovementQueued).call,
  TypedReducer<RoadMovementState, StartRoadMovementProcessingQueue>(
      _startRoadMovementProcessingQueue).call,
  TypedReducer<RoadMovementState, StopRoadMovementProcessingQueue>(
      _stopRoadMovementProcessingQueue).call,
  // TypedReducer<RoadMovementState, ClearRoadMovementImages>(
  //     _clearRoadMovementImages),
]);

RoadMovementState _updateQueueTruckMovement(
    RoadMovementState state, UpdateQueueRoadMovement action) {
  return state.copyWith(
      roadMovementCreateQueue: action.roadMovementCreateQueue, error: "");
}

RoadMovementState _updateRoadMovementCreate(
    RoadMovementState state, UpdateRoadMovementCreate action) {
  return state.copyWith(roadMovementCreate: action.roadMovementCreate);
}

RoadMovementState _resetRoadMovementCreate(
    RoadMovementState state, ResetRoadMovementCreate action) {
  return state.copyWith(roadMovementCreate: RoadMovementCreate.initial());
}

RoadMovementState _flagRoadMovementQueued(
    RoadMovementState state, FlagRoadMovementQueued action) {
  return state.copyWith(queued: true);
}

RoadMovementState _unFlagRoadMovementQueued(
    RoadMovementState state, UnFlagRoadMovementQueued action) {
  return state.copyWith(queued: false);
}

RoadMovementState _startRoadMovementProcessingQueue(
    RoadMovementState state, StartRoadMovementProcessingQueue action) {
  return state.copyWith(processingQueue: true);
}

RoadMovementState _stopRoadMovementProcessingQueue(
    RoadMovementState state, StopRoadMovementProcessingQueue action) {
  return state.copyWith(processingQueue: false);
}

// RoadMovementState _clearRoadMovementImages(
//     RoadMovementState state, ClearRoadMovementImages action) {
//   RoadMovementCreate updatedRoadMovementCreate =
//       state.roadMovementCreate.copyWith(
//     roadImages: [],
//     roadImageNames: [],
//   );
//   return state.copyWith(roadMovementCreate: updatedRoadMovementCreate);
// }
