import 'package:cadi_core_app/redux/actions/storage_actions.dart';
import 'package:cadi_core_app/redux/models/file_store.dart';
import 'package:cadi_core_app/redux/state/storage_state.dart';
import 'package:redux/redux.dart';

final storageReducer = combineReducers<StorageState>([
  TypedReducer<StorageState, UpdateFileStores>(_updateFileStores).call,
  TypedReducer<StorageState, UpdateTempStorage>(_updateTempStorage).call,
  TypedReducer<StorageState, CompleteTempStorageReset>(
      _completeTempStorageReset).call,
]);

StorageState _updateFileStores(StorageState state, UpdateFileStores action) {
  return state.copyWith(fileStores: action.fileStores);
}

StorageState _updateTempStorage(StorageState state, UpdateTempStorage action) {
  return state.copyWith(tempStorage: action.fileStore);
}

StorageState _completeTempStorageReset(
    StorageState state, CompleteTempStorageReset action) {
  return state.copyWith(
      tempStorage: FileStore(
          fileName: "",
          filePath: "",
          fileType: "",
          apiId: "",
          uniqueId: "",
          storageType: ""));
}
