import 'package:cadi_core_app/redux/actions/ui_actions.dart';
import 'package:cadi_core_app/redux/models/ui_screen/ui_screen_models.dart';
import 'package:cadi_core_app/redux/state/ui_state.dart';
import 'package:redux/redux.dart';

final uiReducer = combineReducers<UiState>([
  TypedReducer<UiState, UiDropDownAutoContinueOn>(_uiDropDownAutoContinueOn).call,
  TypedReducer<UiState, UiDropDownAutoContinueOff>(_uiDropDownAutoContinueOff).call,
  TypedReducer<UiState, UiAutoSubmitPageOn>(_uiAutoSubmitPageOn).call,
  TypedReducer<UiState, UiAutoSubmitPageOff>(_uiAutoSubmitPageOff).call,
  TypedReducer<UiState, UiCargoHomeUpdate>(_uiCargoHomeUpdate).call,
  TypedReducer<UiState, UiCargoHomeReset>(_uiCargoHomeReset).call,
  TypedReducer<UiState, UiCargoSetRole>(_uiCargoSetRole).call,
  TypedReducer<UiState, UiPortCargoUpdateHomeUpdate>(_uiPortCargoUpdateHomeUpdate).call,
  TypedReducer<UiState, UiPortCargoUpdateHomeReset>(_uiPortCargoUpdateHomeReset).call,
  TypedReducer<UiState, UiPortCargoUpdateSetRole>(_uiPortCargoUpdateSetRole).call,
  TypedReducer<UiState, UiPortCargoUpdateFreightSearchUpdate>(
      _uiPortCargoUpdateFreightSearchUpdate).call,
  TypedReducer<UiState, UiPortCargoUpdateFreightSearchReset>(
      _uiPortCargoUpdateFreightSearchReset).call,
  TypedReducer<UiState, UiCargoFreightSearchUpdate>(
      _uiCargoFreightSearchUpdate).call,
  TypedReducer<UiState, UiCargoFreightSearchReset>(_uiCargoFreightSearchReset).call,
  TypedReducer<UiState, UiCargoTruckDetailsUpdate>(_uiCargoTruckDetailsUpdate).call,
  TypedReducer<UiState, UiCargoTruckDetailsReset>(_uiCargoTruckDetailsReset).call,
  TypedReducer<UiState, UiCargoUpdateSearchUpdate>(_uiCargoUpdateSearchUpdate).call,
  TypedReducer<UiState, UiCargoUpdateSearchReset>(_uiCargoUpdateSearchReset).call,
  TypedReducer<UiState, UiCargoReportSearchUpdate>(_uiCargoReportSearchUpdate).call,
  TypedReducer<UiState, UiPortEventIndexUpdate>(
      _uiPortEventIndexUpdate).call,
  TypedReducer<UiState, UiPortEventSetRole>(_uiVesselJourneySetRole).call,
  TypedReducer<UiState, UiPortEventCaptureUpdate>(
      _uiPortEventCaptureUpdate).call,
  TypedReducer<UiState, UiVesselUpdateIndexUpdate>(_uiVesselUpdateIndexUpdate).call,
  TypedReducer<UiState, UiRoadMovementSetRole>(_uiRoadMovementSetRole).call,
  TypedReducer<UiState, SetUiRoadMovementHome>(_setUiRoadMovementHome).call,
  TypedReducer<UiState, SetUiRoadMovementSearch>(_setUiRoadMovementSearch).call,


]);

UiState _uiDropDownAutoContinueOn(
    UiState state, UiDropDownAutoContinueOn action) {
  return state.copyWith(uiDropDownOrInputAutoContinue: true);
}

UiState _uiDropDownAutoContinueOff(
    UiState state, UiDropDownAutoContinueOff action) {
  return state.copyWith(uiDropDownOrInputAutoContinue: false);
}

UiState _uiAutoSubmitPageOn(UiState state, UiAutoSubmitPageOn action) {
  return state.copyWith(uiAutoSubmitPage: true);
}

UiState _uiAutoSubmitPageOff(UiState state, UiAutoSubmitPageOff action) {
  return state.copyWith(uiAutoSubmitPage: false);
}

UiState _uiCargoHomeUpdate(UiState state, UiCargoHomeUpdate action) {
  return state.copyWith(uiCargoHome: action.uiCargoHome);
}

UiState _uiCargoHomeReset(UiState state, UiCargoHomeReset action) {
  return state.copyWith(uiCargoHome: UiCargoHome.initial());
}

UiState _uiCargoSetRole(UiState state, UiCargoSetRole action) {
  return state.copyWith(cargoRole: action.cargoRole);
}

UiState _uiPortCargoUpdateHomeUpdate(UiState state, UiPortCargoUpdateHomeUpdate action) {
  return state.copyWith(uiPortCargoUpdateHome: action.uiPortCargoUpdateHome);
}

UiState _uiPortCargoUpdateHomeReset(UiState state, UiPortCargoUpdateHomeReset action) {
  return state.copyWith(uiPortCargoUpdateHome: UiPortCargoUpdateHome.initial());
}

UiState _uiPortCargoUpdateSetRole(UiState state, UiPortCargoUpdateSetRole action) {
  return state.copyWith(portCargoUpdateRole: action.portCargoUpdateRole);
}

UiState _uiPortCargoUpdateFreightSearchUpdate(UiState state, UiPortCargoUpdateFreightSearchUpdate action) {
  return state.copyWith(uiPortCargoUpdateFreightSearch: action.uiPortCargoUpdateFreightSearch);
}

UiState _uiPortCargoUpdateFreightSearchReset(UiState state, UiPortCargoUpdateFreightSearchReset action) {
  return state.copyWith(uiPortCargoUpdateFreightSearch: UiPortCargoUpdateFreightSearch.initial());
}

UiState _uiCargoFreightSearchUpdate(
    UiState state, UiCargoFreightSearchUpdate action) {
  return state.copyWith(uiCargoFreightSearch: action.uiCargoFreightSearch);
}

UiState _uiCargoFreightSearchReset(
    UiState state, UiCargoFreightSearchReset action) {
  return state.copyWith(uiCargoFreightSearch: UiCargoFreightSearch.initial());
}

UiState _uiCargoTruckDetailsUpdate(
    UiState state, UiCargoTruckDetailsUpdate action) {
  return state.copyWith(uiCargoTruckDetails: action.uiCargoTruckDetails);
}

UiState _uiCargoTruckDetailsReset(
    UiState state, UiCargoTruckDetailsReset action) {
  return state.copyWith(uiCargoTruckDetails: UiCargoTruckDetails.initial());
}

UiState _uiCargoUpdateSearchUpdate(
    UiState state, UiCargoUpdateSearchUpdate action) {
  return state.copyWith(uiCargoUpdateSearch: action.uiCargoUpdateSearch);
}

UiState _uiCargoUpdateSearchReset(
    UiState state, UiCargoUpdateSearchReset action) {
  return state.copyWith(uiCargoUpdateSearch: UiCargoUpdateSearch.initial());
}

UiState _uiCargoReportSearchUpdate(
    UiState state, UiCargoReportSearchUpdate action) {
  return state.copyWith(uiCargoReportSearch: action.uiCargoReportSearch);
}

UiState _uiPortEventIndexUpdate(
    UiState state, UiPortEventIndexUpdate action) {
  return state.copyWith(uiPortEventIndex: action.uiPortEventIndex);
}

UiState _uiVesselJourneySetRole(UiState state, UiPortEventSetRole action) {
  return state.copyWith(portEventRole: action.portEventRole);
}

UiState _uiPortEventCaptureUpdate(
    UiState state, UiPortEventCaptureUpdate action) {
  return state.copyWith(uiPortEventCapture: action.uiPortEventCapture);
}

UiState _uiVesselUpdateIndexUpdate(
    UiState state, UiVesselUpdateIndexUpdate action) {
  return state.copyWith(uiVesselUpdateIndex: action.uiVesselUpdateIndex);
}

UiState _uiRoadMovementSetRole(UiState state, UiRoadMovementSetRole action) {
  return state.copyWith(roadMovementRole: action.roadMovementRole);
}

UiState _setUiRoadMovementHome(UiState state, SetUiRoadMovementHome action) {
  return state.copyWith(uiRoadMovementHome: action.uiRoadMovementHome);
}

UiState _setUiRoadMovementSearch(UiState state, SetUiRoadMovementSearch action) {
  return state.copyWith(uiRoadMovementSearch : action.uiRoadMovementSearch );
}


