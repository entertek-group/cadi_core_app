import 'package:cadi_core_app/redux/models/list_converters.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:meta/meta.dart';

/*
 The account state contains many instances of duplicate data storage items, i.e. model and filtered counterpart.
 The model stores data for all accounts, wheres filtered is set to the front-end selected account only.
 */

@immutable
class AccountState {
  final Account account;
  final String activeService;
  final AccountList? accountList;
  final List<AccountPortDepot> accountPortDepots;
  final List<AccountPortDepot> filteredAccountPortDepots;
  final List<AccountPort> accountPorts;
  final List<AccountPort> filteredAccountPorts;
  final List<AccountContainerType> accountContainerTypes;
  final List<AccountContainerType> filteredAccountContainerTypes;
  final List<AccountDocumentType> accountDocumentTypes;
  final List<AccountDocumentType> filteredAccountDocumentTypes;
  final List<AccountVehicle> accountVehicles;
  final List<AccountVehicle> filteredAccountVehicles;
  final List<AccountVehicleDriver> accountVehicleDrivers;
  final List<AccountVehicleDriver> filteredAccountVehicleDrivers;
  final List<AccountVehicleDriver> accountVehicleOperators;
  final List<AccountVehicleDriver> filteredAccountVehicleOperators;
  final List<AccountCarrier> accountCarriers;
  final List<AccountCarrier> filteredAccountCarriers;
  final List<AccountVessel> accountVessels;
  final List<AccountVessel> filteredAccountVessels;
  final List<AccountCrane> accountCranes;
  final List<AccountCrane> filteredAccountCranes;
  final List<AccountBerth> accountBerths;
  final List<AccountBerth> filteredAccountBerths;
  final String error;

  const AccountState(
      {
        required this.account,
        required this.activeService,
        required this.accountList,
        required this.accountPortDepots,
        required this.filteredAccountPortDepots,
        required this.accountPorts,
        required this.filteredAccountPorts,
        required this.accountContainerTypes,
        required this.filteredAccountContainerTypes,
        required this.accountVehicles,
        required this.filteredAccountVehicles,
        required this.accountDocumentTypes,
        required this.filteredAccountDocumentTypes,
        required this.accountVehicleDrivers,
        required this.filteredAccountVehicleDrivers,
        required this.accountVehicleOperators,
        required this.filteredAccountVehicleOperators,
        required this.accountVessels,
        required this.filteredAccountVessels,
        required this.accountCarriers,
        required this.filteredAccountCarriers,
        required this.accountCranes,
        required this.filteredAccountCranes,
        required this.accountBerths,
        required this.filteredAccountBerths,
        required this.error
      });

  factory AccountState.initial() {
    return AccountState(
        account: Account.initial(),
        accountList: AccountList.initial(),
        activeService: "",
        accountPortDepots: <AccountPortDepot>[AccountPortDepot.initial()],
        filteredAccountPortDepots: <AccountPortDepot>[
          AccountPortDepot.initial()
        ],
        accountPorts: <AccountPort>[AccountPort.initial()],
        filteredAccountPorts: <AccountPort>[AccountPort.initial()],
        accountVehicles: <AccountVehicle>[
          AccountVehicle.initial()
        ],
        filteredAccountVehicles: <AccountVehicle>[
          AccountVehicle.initial()
        ],
        accountDocumentTypes: <AccountDocumentType>[
          AccountDocumentType.initial()
        ],
        accountVehicleDrivers: <AccountVehicleDriver>[
          AccountVehicleDriver.initial()
        ],
        filteredAccountVehicleDrivers: <AccountVehicleDriver>[
          AccountVehicleDriver.initial()
        ],
        accountVehicleOperators: <AccountVehicleDriver>[
          AccountVehicleDriver.initial()
        ],
        filteredAccountVehicleOperators: <AccountVehicleDriver>[
          AccountVehicleDriver.initial()
        ],
        filteredAccountDocumentTypes: <AccountDocumentType>[
          AccountDocumentType.initial()
        ],
        accountContainerTypes: <AccountContainerType>[
          AccountContainerType.initial()
        ],
        filteredAccountContainerTypes: <AccountContainerType>[
          AccountContainerType.initial()
        ],
        accountVessels: <AccountVessel>[AccountVessel.initial()],
        filteredAccountVessels: <AccountVessel>[AccountVessel.initial()],
        accountCarriers: <AccountCarrier>[AccountCarrier.initial()],
        filteredAccountCarriers: <AccountCarrier>[AccountCarrier.initial()],
        accountCranes: <AccountCrane>[AccountCrane.initial()],
        filteredAccountCranes:  <AccountCrane>[AccountCrane.initial()],
        accountBerths: <AccountBerth>[AccountBerth.initial()],
        filteredAccountBerths:  <AccountBerth>[AccountBerth.initial()],
        error: "");
  }

  AccountState copyWith(
      {
        Account? account,
        String? activeService,
        AccountList? accountList,
        List<AccountPortDepot>? accountPortDepots,
        List<AccountPortDepot>? filteredAccountPortDepots,
        List<AccountPort>? accountPorts,
        List<AccountPort>? filteredAccountPorts,
        List<AccountContainerType>? accountContainerTypes,
        List<AccountContainerType>? filteredAccountContainerTypes,
        List<AccountDocumentType>? accountDocumentTypes,
        List<AccountDocumentType>? filteredAccountDocumentTypes,
        List<AccountVehicle>? accountVehicles,
        List<AccountVehicle>? filteredAccountVehicles,
        List<AccountVehicleDriver>? accountVehicleDrivers,
        List<AccountVehicleDriver>? filteredAccountVehicleDrivers,
        List<AccountVehicleDriver>? accountVehicleOperators,
        List<AccountVehicleDriver>? filteredAccountVehicleOperators,
        List<AccountCarrier>? accountCarriers,
        List<AccountCarrier>? filteredAccountCarriers,
        List<AccountVessel>? accountVessels,
        List<AccountVessel>? filteredAccountVessels,
        List<AccountCrane>? accountCranes,
        List<AccountCrane>? filteredAccountCranes,
        List<AccountBerth>? accountBerths,
        List<AccountBerth>? filteredAccountBerths,
        String? error
      }) {
    return AccountState(
        account: account ?? this.account,
        activeService: activeService ?? this.activeService,
        accountList: accountList ?? this.accountList,
        accountPortDepots: accountPortDepots ?? this.accountPortDepots,
        filteredAccountPortDepots:
            filteredAccountPortDepots ?? this.filteredAccountPortDepots,
        accountPorts: accountPorts ?? this.accountPorts,
        filteredAccountPorts: filteredAccountPorts ?? this.filteredAccountPorts,
        accountContainerTypes:
            accountContainerTypes ?? this.accountContainerTypes,
        filteredAccountContainerTypes:
            filteredAccountContainerTypes ?? this.filteredAccountContainerTypes,
        accountDocumentTypes:
            accountDocumentTypes ?? this.accountDocumentTypes,
        filteredAccountDocumentTypes: filteredAccountDocumentTypes ??
            this.filteredAccountDocumentTypes,
        accountVehicles: accountVehicles ?? this.accountVehicles,
        filteredAccountVehicles: filteredAccountVehicles ?? this.filteredAccountVehicles,
        accountVehicleDrivers:
            accountVehicleDrivers ?? this.accountVehicleDrivers,
        filteredAccountVehicleDrivers: filteredAccountVehicleDrivers ??
            this.filteredAccountVehicleDrivers,
        accountVehicleOperators:
        accountVehicleOperators ?? this.accountVehicleOperators,
        filteredAccountVehicleOperators: filteredAccountVehicleOperators ??
            this.filteredAccountVehicleOperators,
        accountCarriers: accountCarriers ?? this.accountCarriers,
        filteredAccountCarriers:
            filteredAccountCarriers ?? this.filteredAccountCarriers,
        accountVessels: accountVessels ?? this.accountVessels,
        filteredAccountVessels:
        filteredAccountVessels ?? this.filteredAccountVessels,
        accountCranes: accountCranes ?? this.accountCranes,
        filteredAccountCranes:
        filteredAccountCranes ?? this.filteredAccountCranes,
        accountBerths: accountBerths ?? this.accountBerths,
        filteredAccountBerths:
        filteredAccountBerths ?? this.filteredAccountBerths,
        error: error ?? this.error);
  }

  factory AccountState.fromJson(dynamic parsedJson) {
    final accountList = parsedJson['accounts'] ?? AccountList.initial();
    return AccountState(
        accountList: AccountList.fromJson(accountList['accounts']),
        account: parsedJson['account'] != null
            ? Account.fromJson(parsedJson['account'])
            : Account.initial(),
        activeService: parsedJson['activeService'],
        accountPortDepots: parsedJson['accountPortDepots'] != null
            ? converterAccountPortDepots(parsedJson['accountPortDepots'])
            : <AccountPortDepot>[AccountPortDepot.initial()],
        filteredAccountPortDepots:
            parsedJson['filteredAccountPortDepots'] != null
                ? converterAccountPortDepots(
                    parsedJson['filteredAccountPortDepots'])
                : <AccountPortDepot>[AccountPortDepot.initial()],
        accountPorts: parsedJson['accountPorts'] != null
            ? converterAccountPorts(parsedJson['accountPorts'])
            : <AccountPort>[AccountPort.initial()],
        filteredAccountPorts: parsedJson['filteredAccountPorts'] != null
            ? converterAccountPorts(parsedJson['filteredAccountPorts'])
            : <AccountPort>[AccountPort.initial()],
        accountContainerTypes: parsedJson['accountContainerTypes'] != null
            ? converterAccountContainerTypes(
                parsedJson['accountContainerTypes'])
            : <AccountContainerType>[AccountContainerType.initial()],
        filteredAccountContainerTypes:
            parsedJson['filteredAccountContainerTypes'] != null
                ? converterAccountContainerTypes(
                    parsedJson['filteredAccountContainerTypes'])
                : <AccountContainerType>[AccountContainerType.initial()],
        accountDocumentTypes: parsedJson['accountDocumentTypes'] != null
            ? converterAccountDocumentTypes(
                parsedJson['accountDocumentTypes'])
            : <AccountDocumentType>[AccountDocumentType.initial()],
        filteredAccountDocumentTypes:
            parsedJson['filteredAccountDocumentTypes'] != null
                ? converterAccountDocumentTypes(
                    parsedJson['filteredAccountDocumentTypes'])
                : <AccountDocumentType>[AccountDocumentType.initial()],
        accountVehicles: parsedJson['accountVehicles'] != null
            ? converterAccountVehicles(
            parsedJson['accountVehicles'])
            : <AccountVehicle>[AccountVehicle.initial()],
        filteredAccountVehicles:
        parsedJson['filteredAccountVehicles'] != null
            ? converterAccountVehicles(
            parsedJson['filteredAccountVehicles'])
            : <AccountVehicle>[AccountVehicle.initial()],
        accountVehicleDrivers: parsedJson['accountVehicleDrivers'] != null
            ? converterAccountVehicleDrivers(
                parsedJson['accountVehicleDrivers'])
            : <AccountVehicleDriver>[AccountVehicleDriver.initial()],
        filteredAccountVehicleDrivers:
            parsedJson['filteredAccountVehicleDrivers'] != null
                ? converterAccountVehicleDrivers(
                    parsedJson['filteredAccountVehicleDrivers'])
                : <AccountVehicleDriver>[AccountVehicleDriver.initial()],
        accountVehicleOperators: parsedJson['accountVehicleOperators'] != null
            ? converterAccountVehicleDrivers(
            parsedJson['accountVehicleOperators'])
            : <AccountVehicleDriver>[AccountVehicleDriver.initial()],
        filteredAccountVehicleOperators:
        parsedJson['filteredAccountVehicleOperators'] != null
            ? converterAccountVehicleDrivers(
            parsedJson['filteredAccountVehicleOperators'])
            : <AccountVehicleDriver>[AccountVehicleDriver.initial()],
        accountCarriers: parsedJson['accountCarriers'] != null
            ? converterAccountCarriers(parsedJson['accountCarriers'])
            : <AccountCarrier>[AccountCarrier.initial()],
        filteredAccountCarriers: parsedJson['filteredAccountCarriers'] != null
            ? converterAccountCarriers(parsedJson['filteredAccountCarriers'])
            : <AccountCarrier>[AccountCarrier.initial()],
        accountVessels: parsedJson['accountVessels'] != null
            ? converterAccountVessels(parsedJson['accountVessels'])
            : <AccountVessel>[AccountVessel.initial()],
        filteredAccountVessels: parsedJson['filteredAccountVessels'] != null
            ? converterAccountVessels(parsedJson['filteredAccountVessels'])
            : <AccountVessel>[AccountVessel.initial()],
        accountCranes: parsedJson['accountCranes'] != null
            ? converterAccountCranes(parsedJson['accountCranes'])
            : <AccountCrane>[AccountCrane.initial()],
        filteredAccountCranes: parsedJson['filteredAccountCranes'] != null
            ? converterAccountCranes(parsedJson['filteredAccountCranes'])
            : <AccountCrane>[AccountCrane.initial()],
        accountBerths: parsedJson['accountBerths'] != null
            ? converterAccountBerths(parsedJson['accountBerths'])
            : <AccountBerth>[AccountBerth.initial()],
        filteredAccountBerths: parsedJson['filteredAccountBerths'] != null
            ? converterAccountBerths(parsedJson['filteredAccountBerths'])
            : <AccountBerth>[AccountBerth.initial()],
        error: parsedJson['error']);
  }

  dynamic toJson() => {
        'accounts': accountList,
        'account': account,
        'activeService': activeService,
        'accountPortDepots': accountPortDepots,
        'filteredAccountPortDepots': filteredAccountPortDepots,
        'accountPorts': accountPorts,
        'filteredAccountPorts': filteredAccountPorts,
        'accountContainerTypes': accountContainerTypes,
        'filteredAccountContainerTypes': filteredAccountContainerTypes,
        'accountDocumentTypes': accountDocumentTypes,
        'filteredAccountDocumentTypes': filteredAccountDocumentTypes,
        'accountVehicles': accountVehicles,
        'filteredAccountVehicles': filteredAccountVehicles,
        'accountVehicleDrivers': accountVehicleDrivers,
        'filteredAccountVehicleDrivers': filteredAccountVehicleDrivers,
        'accountVehicleOperators': accountVehicleOperators,
        'filteredAccountVehicleOperators': filteredAccountVehicleOperators,
        'accountCarriers': accountCarriers,
        'filteredAccountCarriers': filteredAccountCarriers,
        'accountVessels': accountVessels,
        'filteredAccountVessels': filteredAccountVessels,
        'accountCranes': accountCranes,
        'filteredAccountCranes': filteredAccountCranes,
        'accountBerths': accountBerths,
        'filteredAccountBerths': filteredAccountBerths,
        'error': error
      };

  @override
  toString() {
    return 'AccountState{account: $account, '
        'activeService: $activeService,'
        'accountList: $accountList,'
        'accountPortDepots: $accountPortDepots,'
        'filteredAccountPortDepots: $filteredAccountPortDepots,'
        'accountPorts: $accountPorts,'
        'filteredAccountPorts: $filteredAccountPorts,'
        'accountContainerTypes: $accountContainerTypes,'
        'filteredAccountContainerTypes: $filteredAccountContainerTypes,'
        'accountDocumentTypes: $accountDocumentTypes,'
        'filteredAccountDocumentTypes: $filteredAccountDocumentTypes,'
        'accountVehicles: $accountVehicles,'
        'filteredAccountVehicles: $filteredAccountVehicles,'
        'accountVehicleDrivers: $accountVehicleDrivers,'
        'filteredAccountVehicleDrivers: $filteredAccountVehicleDrivers,'
        'accountVehicleOperators: $accountVehicleOperators,'
        'filteredAccountVehicleOperators: $filteredAccountVehicleOperators,'
        'accountCarriers: $accountCarriers,'
        'filteredAccountCarriers: $filteredAccountCarriers,'
        'accountVessels: $accountVessels,'
        'filteredAccountVessels: $filteredAccountVessels,'
        'accountCranes: $accountCranes,'
        'filteredAccountCranes: $filteredAccountCranes,'
        'accountBerths: $accountBerths,'
        'filteredAccountBerths: $filteredAccountBerths,'
        'error: $error }';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AccountState &&
          runtimeType == other.runtimeType &&
          accountList == other.accountList &&
          account == other.account &&
          activeService == other.activeService &&
          accountPortDepots == other.accountPortDepots &&
          filteredAccountPortDepots == other.filteredAccountPortDepots &&
          accountPorts == other.accountPorts &&
          filteredAccountPorts == other.filteredAccountPorts &&
          accountDocumentTypes == other.accountDocumentTypes &&
          filteredAccountDocumentTypes ==
              other.filteredAccountDocumentTypes &&
          accountVehicles == other.accountVehicles &&
          filteredAccountVehicles ==
              other.filteredAccountVehicles &&
          accountVehicleDrivers == other.accountVehicleDrivers &&
          filteredAccountVehicleDrivers ==
              other.filteredAccountVehicleDrivers &&
          accountVehicleOperators == other.accountVehicleOperators &&
          filteredAccountVehicleOperators ==
              other.filteredAccountVehicleOperators &&
          accountContainerTypes == other.accountContainerTypes &&
          filteredAccountContainerTypes ==
              other.filteredAccountContainerTypes &&
          accountCarriers == other.accountCarriers &&
          filteredAccountCarriers == other.filteredAccountCarriers &&
          accountVessels == other.accountVessels &&
          filteredAccountVessels == other.filteredAccountVessels &&
          accountCranes == other.accountCranes &&
          filteredAccountCranes == other.filteredAccountCranes &&
          accountBerths == other.accountBerths &&
          filteredAccountBerths == other.filteredAccountBerths &&
          error == other.error;

  @override
  int get hashCode =>
      accountList.hashCode ^
      account.hashCode ^
      activeService.hashCode ^
      accountPortDepots.hashCode ^
      filteredAccountPortDepots.hashCode ^
      accountPorts.hashCode ^
      filteredAccountPorts.hashCode ^
      accountDocumentTypes.hashCode ^
      filteredAccountDocumentTypes.hashCode ^
      accountVehicles.hashCode ^
      filteredAccountVehicles.hashCode ^
      accountVehicleDrivers.hashCode ^
      filteredAccountVehicleDrivers.hashCode ^
      accountVehicleOperators.hashCode ^
      filteredAccountVehicleOperators.hashCode ^
      accountContainerTypes.hashCode ^
      filteredAccountContainerTypes.hashCode ^
      accountCarriers.hashCode ^
      filteredAccountCarriers.hashCode ^
      accountVessels.hashCode ^
      filteredAccountVessels.hashCode ^
      accountCranes.hashCode ^
      filteredAccountCranes.hashCode ^
      accountBerths.hashCode ^
      filteredAccountBerths.hashCode ^
      error.hashCode;
}
