import 'package:cadi_core_app/redux/state/states.dart';
import 'package:meta/meta.dart';

@immutable
class AppState {
  //Constructor
  //use curlies to denote named parameters
  const AppState({
    required this.isLoading,
    required this.isDownloading,
    required this.isSubmitting,
    required this.welcomeShown,
    required this.homeIndex,
    required this.authState,
    required this.cargoMovementState,
    required this.portCargoUpdateState,
    required this.accountState,
    required this.connectivityState,
    required this.referenceState,
    required this.uiState,
    required this.storageState,
    required this.reportState,
    required this.portEventState,
    required this.roadMovementState,
    required this.processingIndex,
    required this.processingTotal
  });

  final bool isLoading;
  final bool isDownloading;
  final bool isSubmitting;
  final bool welcomeShown;
  final int homeIndex;
  final AuthState authState;
  final CargoMovementState cargoMovementState;
  final PortCargoUpdateState portCargoUpdateState;
  final AccountState accountState;
  final ConnectivityState connectivityState;
  final ReferenceState referenceState;
  final UiState uiState;
  final StorageState storageState;
  final ReportState reportState;
  final PortEventState portEventState;
  final RoadMovementState roadMovementState;
  final int processingIndex;
  final int processingTotal;

  factory AppState.initial() {
    return AppState(
      isLoading: false,
      isDownloading: false,
      isSubmitting: false,
      welcomeShown: false,
      homeIndex: 0,
      authState: AuthState.initial(),
      cargoMovementState: CargoMovementState.initial(),
      portCargoUpdateState: PortCargoUpdateState.initial(),
      accountState: AccountState.initial(),
      connectivityState: ConnectivityState.initial(),
      referenceState: ReferenceState.initial(),
      uiState: UiState.initial(),
      storageState: StorageState.initial(),
      reportState: ReportState.initial(),
      portEventState: PortEventState.initial(),
      roadMovementState: RoadMovementState.initial(),
      processingIndex: 0,
      processingTotal: 0
    );
  }

  factory AppState.initialWithoutWelcome() {
    return AppState(
        isLoading: false,
        isDownloading: false,
        isSubmitting: false,
        welcomeShown: true,
        homeIndex: 0,
        authState: AuthState.initial(),
        cargoMovementState: CargoMovementState.initial(),
        portCargoUpdateState: PortCargoUpdateState.initial(),
        accountState: AccountState.initial(),
        connectivityState: ConnectivityState.initial(),
        referenceState: ReferenceState.initial(),
        uiState: UiState.initial(),
        storageState: StorageState.initial(),
        reportState: ReportState.initial(),
        portEventState: PortEventState.initial(),
        roadMovementState: RoadMovementState.initial(),
        processingIndex: 0,
        processingTotal: 0
    );
  }

  AppState copyWith({
    int? count,
    required bool isLoading,
    required bool isDownloading,
    required bool isSubmitting,
    required bool welcomeShown,
    required int homeIndex,
    AuthState? authState,
    CargoMovementState? cargoMovementState,
    PortCargoUpdateState? portCargoUpdateState,
    AccountState? accountState,
    ConnectivityState? connectivityState,
    ReferenceState? referenceState,
    UiState? uiState,
    StorageState? storageState,
    ReportState? reportState,
    PortEventState? portEventState,
    RoadMovementState? roadMovementState,
    int? processingIndex,
    int? processingTotal
  }) {
    return AppState(
      isLoading: isLoading,
      isDownloading: isDownloading,
      isSubmitting: isSubmitting,
      welcomeShown: welcomeShown,
      homeIndex: homeIndex,
      authState: authState ?? this.authState,
      cargoMovementState: cargoMovementState ?? this.cargoMovementState,
      portCargoUpdateState: portCargoUpdateState ?? this.portCargoUpdateState,
      accountState: accountState ?? this.accountState,
      connectivityState: connectivityState ?? this.connectivityState,
      referenceState: referenceState ?? this.referenceState,
      uiState: uiState ?? this.uiState,
      storageState: storageState ?? this.storageState,
      reportState: reportState ?? this.reportState,
      portEventState: portEventState ?? this.portEventState,
      roadMovementState: roadMovementState ?? this.roadMovementState,
      processingIndex: processingIndex ?? this.processingIndex,
      processingTotal: processingTotal ?? this.processingTotal
    );
  }

  static AppState fromJson(dynamic json) {
    if (json != null) {
      print(json['reportState']);
      return AppState(
        isLoading: json["isLoading"] as bool,
        isDownloading: json['isDownloading'] as bool,
        isSubmitting: json['isSubmitting'] as bool,
        welcomeShown: json['welcomeShown'] as bool,
        homeIndex: json['homeIndex'] as int,
        authState: AuthState.fromJson(json["authState"]),
        cargoMovementState: json["cargoMovementState"] != null
            ? CargoMovementState.fromJson(json["cargoMovementState"])
            : CargoMovementState.initial(),
        portCargoUpdateState: json["portCargoUpdateState"] != null
            ? PortCargoUpdateState.fromJson(json["portCargoUpdateState"])
            : PortCargoUpdateState.initial(),
        accountState: json['accountState'] != null
            ? AccountState.fromJson(json['accountState'])
            : AccountState.initial(),
        connectivityState: json['connectivityState'] != null //statusAt
            ? ConnectivityState.fromJson(json['connectivityState'])
            : ConnectivityState.initial(),
        referenceState: json['referenceState'] != null
            ? ReferenceState.fromJson(json['referenceState'])
            : ReferenceState.initial(),
        uiState: json['uiState'] != null
            ? UiState.fromJson(json['uiState'])
            : UiState.initial(),
        storageState: json['storageState'] != null
            ? StorageState.fromJson(json['storageState'])
            : StorageState.initial(),
        reportState: json['reportState'] != null
            ? ReportState.fromJson(json['reportState'])
            : ReportState.initial(),
        portEventState: json['portEventState'] != null
            ? PortEventState.fromJson(json['portEventState'])
            : PortEventState.initial(),
        roadMovementState: json["roadMovementState"] != null
            ? RoadMovementState.fromJson(json["roadMovementState"])
            : RoadMovementState.initial(),
        processingIndex: json['processingIndex'] as int,
        processingTotal: json['processingTotal'] as int
      );
    } else {
      return AppState.initial();
    }
  }

  dynamic toJson() => {
        'isLoading': isLoading,
        'isDownloading': isDownloading,
        'isSubmitting': isSubmitting,
        'welcomeShown': welcomeShown,
        'homeIndex': homeIndex,
        'authState': authState,
        'cargoMovementState': cargoMovementState,
        'portCargoUpdateState': portCargoUpdateState,
        'accountState': accountState,
        'connectivityState': connectivityState,
        'referenceState': referenceState,
        'uiState': uiState,
        'storageState': storageState,
        'reportState': reportState,
        'portEventState': portEventState,
        'roadMovementState': roadMovementState,
        'processingIndex': processingIndex,
        'processingTotal': processingTotal
      };

  @override
  toString() {
    return 'AppState{isLoading: $isLoading,'
        'isDownloading: $isDownloading,'
        'isSubmitting: $isSubmitting,'
        'welcomeShown: $welcomeShown,'
        'homeIndex: $homeIndex,'
        'authState: $authState,'
        'cargoMovementState: $cargoMovementState,'
        'portCargoUpdateState: $portCargoUpdateState,'
        'accountState: $accountState,'
        'connectivityState: $connectivityState,'
        'referenceState: $referenceState,'
        'uiState: $uiState,'
        'storageState: $storageState,'
        'reportState: $reportState,'
        'portEventState: $portEventState,'
        'roadMovementState: $roadMovementState,'
        'processingIndex: $processingIndex,'
        'processingTotal: $processingTotal,'
        '}';
  }
}
