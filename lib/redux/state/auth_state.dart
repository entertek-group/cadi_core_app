import 'package:meta/meta.dart';
import 'package:cadi_core_app/redux/models/models.dart';

@immutable
class AuthState {
  final String? email;
  final String? password;
  final bool isInitialized;
  final bool isAuthenticated;
  final Token? token;
  final User? user;
  final String error;
  final String? name;
  final String? passwordConfirmation;
  final String? surname;

  // ignore: prefer_const_constructors_in_immutables
  AuthState({
    required this.email,
    required this.password,
    required this.isInitialized,
    required this.isAuthenticated,
    required this.token,
    required this.user,
    required this.error,
    required this.passwordConfirmation,
    required this.name,
    required this.surname,
  });

  factory AuthState.initial() {
    return AuthState(
        email: "",
        password: "",
        isInitialized: true,
        isAuthenticated: false,
        token: Token.initial(),
        user: User.initial(),
        error: "",
        passwordConfirmation: "",
        name: "",
        surname: "");
  }

  AuthState copyWith(
      {String? email,
      String? password,
      bool? isInitialized,
      bool? isAuthenticated,
      Token? token,
      User? user,
      String? error,
      String? passwordConfirmation,
      String? name,
      String? surname}) {
    return AuthState(
      email: email ?? this.email,
      password: password ?? this.password,
      isAuthenticated: isAuthenticated ?? this.isAuthenticated,
      isInitialized: isInitialized ?? this.isInitialized,
      token: token ?? this.token,
      user: user ?? this.user,
      error: error ?? this.error,
      passwordConfirmation: passwordConfirmation ?? this.passwordConfirmation,
      name: name ?? this.name,
      surname: surname ?? this.surname,
    );
  }

  AuthState.fromJson(json)
      : email = json['email'] ?? "",
        password = json['password'] ?? "",
        isAuthenticated = json['isAuthenticated'] ?? "",
        isInitialized = json['isInitialized'] ?? "",
        token = json['token'] != null
            ? Token.fromJson(json['token'])
            : Token.initial(),
        user =
            json['user'] != null ? User.fromJson(json['user']) : User.initial(),
        error = json['error'] ?? "",
        passwordConfirmation = json['passwordConfirmation'] ?? "",
        name = json['name'] ?? "",
        surname = json['surname'] ?? "";

  Map toJson() => {
        //<String, dynamic>
        'email': email,
        'password': password,
        'isAuthenticated': isAuthenticated,
        'isInitialized': isInitialized,
        'token': token,
        'user': user,
        'error': error,
        'passwordConfirmation': passwordConfirmation,
        'name': name,
        'surname': surname,
      };

  @override
  toString() {
    return 'AuthState{email: $email, password: $password,'
        ' isAuthenticated: $isAuthenticated, isInitialized: $isInitialized,'
        ' token: $token, user: $user, error: $error, name: $name, surname: $surname,'
        ' passwordConfirmation: $passwordConfirmation}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AuthState &&
          runtimeType == other.runtimeType &&
          password == other.password &&
          email == other.email &&
          isInitialized == other.isInitialized &&
          isAuthenticated == other.isAuthenticated &&
          token == other.token &&
          user == other.user &&
          error == other.error &&
          passwordConfirmation == other.passwordConfirmation &&
          name == other.name &&
          surname == other.surname;

  @override
  int get hashCode =>
      password.hashCode ^
      email.hashCode ^
      isInitialized.hashCode ^
      isAuthenticated.hashCode ^
      token.hashCode ^
      user.hashCode ^
      error.hashCode ^
      passwordConfirmation.hashCode ^
      name.hashCode ^
      surname.hashCode;
}
