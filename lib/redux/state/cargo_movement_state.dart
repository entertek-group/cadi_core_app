import 'package:cadi_core_app/redux/models/list_converters.dart';
import 'package:meta/meta.dart';
import 'package:cadi_core_app/redux/models/models.dart';

@immutable
class CargoMovementState {
  final CargoMovementCreate cargoMovementCreate;
  final List<CargoCreateQueue>? cargoCreateQueue;
  final String error;
  final bool queued;
  final bool processingQueue;

  const CargoMovementState(
      {required this.cargoMovementCreate,
      required this.cargoCreateQueue,
      required this.error,
      required this.queued,
      required this.processingQueue});

  factory CargoMovementState.initial() {
    return CargoMovementState(
        cargoMovementCreate: CargoMovementCreate.initial(),
        cargoCreateQueue: null,
        error: "",
        queued: false,
        processingQueue: false);
  }

  CargoMovementState copyWith(
      {CargoMovementCreate? cargoMovementCreate,
      List<CargoCreateQueue>? cargoCreateQueue,
      String? error,
      bool? queued,
      bool? processingQueue}) {
    return CargoMovementState(
        cargoMovementCreate: cargoMovementCreate ?? this.cargoMovementCreate,
        cargoCreateQueue: cargoCreateQueue ?? this.cargoCreateQueue,
        error: error ?? this.error,
        queued: queued ?? this.queued,
        processingQueue: processingQueue ?? this.processingQueue);
  }

  CargoMovementState.fromJson(json)
      : cargoMovementCreate = json['cargoMovementCreate'] != null
            ? CargoMovementCreate.fromJson(json['cargoMovementCreate'])
            : CargoMovementCreate.initial(),
        cargoCreateQueue = json['cargoCreateQueue'] != null
            ? converterCargoCreateQueue(json['cargoCreateQueue'])
            : null,
        error = json['error'] ?? "",
        queued = json['queued'] != null ? json['queued'] as bool : false,
        processingQueue = json['processingQueue'] != null
            ? json['processingQueue'] as bool
            : false;

  Map toJson() => {
        //<String, dynamic>
        'cargoMovementCreate': cargoMovementCreate,
        'cargoCreateQueue': cargoCreateQueue,
        'error': error,
        'queued': queued,
        'processingQueue': processingQueue
      };

  @override
  toString() {
    return 'CargoMovementState{'
        'cargoMovementCreate: $cargoMovementCreate,'
        'cargoCreateQueue: $cargoCreateQueue,'
        'error: $error,'
        'queued: $queued,'
        'processingQueue: $processingQueue}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CargoMovementState &&
          runtimeType == other.runtimeType &&
          cargoMovementCreate == other.cargoMovementCreate &&
          cargoCreateQueue == other.cargoCreateQueue &&
          error == other.error &&
          queued == other.queued &&
          processingQueue == other.processingQueue;

  @override
  int get hashCode =>
      cargoMovementCreate.hashCode ^
      cargoCreateQueue.hashCode ^
      error.hashCode ^
      queued.hashCode ^
      processingQueue.hashCode;
}
