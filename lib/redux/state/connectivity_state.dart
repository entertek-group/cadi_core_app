import 'package:meta/meta.dart';

@immutable
class ConnectivityState {
  final String connectionStatus;
  final bool dataSync;
  final String error;
  final String statusAt;

  // ignore: prefer_const_constructors_in_immutables
  ConnectivityState(
      {required this.connectionStatus,
      required this.dataSync,
      required this.error,
      required this.statusAt});

  factory ConnectivityState.initial() {
    return ConnectivityState(
        connectionStatus: "none",
        dataSync: false,
        error: "",
        statusAt: DateTime.now().toString());
  }

  ConnectivityState copyWith(
      {String? connectionStatus,
      bool? dataSync,
      String? error,
      String? statusAt}) {
    return ConnectivityState(
        connectionStatus: connectionStatus ?? this.connectionStatus,
        dataSync: dataSync ?? this.dataSync,
        error: error ?? this.error,
        statusAt: statusAt ?? this.statusAt);
  }

  factory ConnectivityState.fromJson(dynamic parsedJson) {
    return ConnectivityState(
        connectionStatus: parsedJson['connectionStatus'],
        dataSync: parsedJson['dataSync'],
        error: parsedJson['error'],
        statusAt: parsedJson['statusAt']);
  }

  dynamic toJson() => {
        'connectionStatus': connectionStatus,
        'dataSync': dataSync,
        'error': error,
        'statusAt': statusAt
      };

  @override
  toString() {
    return 'ConnectivityState{connectionStatus: $connectionStatus, '
        'dataSync: $dataSync,'
        'error: $error ,'
        'statusAt: $statusAt}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ConnectivityState &&
          runtimeType == other.runtimeType &&
          connectionStatus == other.connectionStatus &&
          dataSync == other.dataSync &&
          statusAt == other.statusAt &&
          error == other.error;

  @override
  int get hashCode =>
      connectionStatus.hashCode ^
      dataSync.hashCode ^
      error.hashCode ^
      statusAt.hashCode;
}
