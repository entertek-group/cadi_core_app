import 'package:cadi_core_app/redux/models/list_converters.dart';
import 'package:meta/meta.dart';
import 'package:cadi_core_app/redux/models/models.dart';

@immutable
class PortCargoUpdateState {
  final PortCargoUpdateCreate portCargoUpdateCreate;
  final List<PortCargoUpdateQueue>? portCargoUpdateQueue;
  final String error;
  final bool queued;
  final bool processingQueue;

  const PortCargoUpdateState(
      {required this.portCargoUpdateCreate,
      required this.portCargoUpdateQueue,
      required this.error,
      required this.queued,
      required this.processingQueue});

  factory PortCargoUpdateState.initial() {
    return PortCargoUpdateState(
        portCargoUpdateCreate: PortCargoUpdateCreate.initial(),
        portCargoUpdateQueue: null,
        error: "",
        queued: false,
        processingQueue: false);
  }

  PortCargoUpdateState copyWith(
      {PortCargoUpdateCreate? portCargoUpdateCreate,
      List<PortCargoUpdateQueue>? portCargoUpdateQueue,
      String? error,
      bool? queued,
      bool? processingQueue}) {
    return PortCargoUpdateState(
        portCargoUpdateCreate: portCargoUpdateCreate ?? this.portCargoUpdateCreate,
        portCargoUpdateQueue: portCargoUpdateQueue ?? this.portCargoUpdateQueue,
        error: error ?? this.error,
        queued: queued ?? this.queued,
        processingQueue: processingQueue ?? this.processingQueue);
  }

  PortCargoUpdateState.fromJson(json)
      : portCargoUpdateCreate = json['portCargoUpdateCreate'] != null
            ? PortCargoUpdateCreate.fromJson(json['portCargoUpdateCreate'])
            : PortCargoUpdateCreate.initial(),
        portCargoUpdateQueue = json['portCargoUpdateQueue'] != null
            ? converterPortCargoUpdateQueue(json['portCargoUpdateQueue'])
            : null,
        error = json['error'] ?? "",
        queued = json['queued'] != null ? json['queued'] as bool : false,
        processingQueue = json['processingQueue'] != null
            ? json['processingQueue'] as bool
            : false;

  Map toJson() => {
        //<String, dynamic>
        'portCargoUpdateCreate': portCargoUpdateCreate,
        'portCargoUpdateQueue': portCargoUpdateQueue,
        'error': error,
        'queued': queued,
        'processingQueue': processingQueue
      };

  @override
  toString() {
    return 'PortCargoUpdateState{'
        'portCargoUpdateCreate: $portCargoUpdateCreate,'
        'portCargoUpdateQueue: $portCargoUpdateQueue,'
        'error: $error,'
        'queued: $queued,'
        'processingQueue: $processingQueue}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PortCargoUpdateState &&
          runtimeType == other.runtimeType &&
          portCargoUpdateCreate == other.portCargoUpdateCreate &&
          portCargoUpdateQueue == other.portCargoUpdateQueue &&
          error == other.error &&
          queued == other.queued &&
          processingQueue == other.processingQueue;

  @override
  int get hashCode =>
      portCargoUpdateCreate.hashCode ^
      portCargoUpdateQueue.hashCode ^
      error.hashCode ^
      queued.hashCode ^
      processingQueue.hashCode;
}
