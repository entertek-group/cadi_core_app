import 'package:cadi_core_app/redux/models/list_converters.dart';
import 'package:cadi_core_app/redux/models/port_event/port_event_models.dart';
import 'package:meta/meta.dart';

@immutable
class PortEventState {
  final PortEventCreate portEventCreate;
  final List<PortEventCreateQueue>? portEventCreateQueue;
  final String error;
  final bool queued;
  final bool processingQueue;

  const PortEventState({
      required this.portEventCreate,
      required this.portEventCreateQueue,
      required this.error,
      required this.queued,
      required this.processingQueue
  });

  factory PortEventState.initial() {
    return PortEventState(
      portEventCreate: PortEventCreate.initial(),
      portEventCreateQueue: null,
      error: "",
      queued: false,
      processingQueue: false
    );
  }

  PortEventState copyWith ({
    PortEventCreate? portEventCreate,
    List<PortEventCreateQueue>? portEventCreateQueue,
    String? error,
    bool? queued,
    bool? processingQueue
  }) {
    return PortEventState (
        portEventCreateQueue:
            portEventCreateQueue ?? this.portEventCreateQueue,
        portEventCreate: portEventCreate ?? this.portEventCreate,
        error: error ?? this.error,
        queued: queued ?? this.queued,
        processingQueue: processingQueue ?? this.processingQueue);
  }

  factory PortEventState.fromJson(dynamic parsedJson) {
    return PortEventState(
        portEventCreate: parsedJson['portEventCreate'] != null
            ? PortEventCreate.fromJson(parsedJson['portEventCreate'])
            : PortEventCreate.initial(),
        portEventCreateQueue: parsedJson['portEventCreateQueue'] != null
            ? converterPortEventCreateQueue(
                parsedJson['portEventCreateQueue'])
            : null,
        error: parsedJson['error'] ?? "",
        queued: parsedJson['queued'] != null ? parsedJson['queued'] as bool : false,
        processingQueue: parsedJson['processingQueue'] != null
            ? parsedJson['processingQueue'] as bool
            : false
    );
  }

  dynamic toJson() => {
        'portEventCreate': portEventCreate,
        'portEventCreateQueue': portEventCreateQueue,
        'error': error,
        'queued': queued,
        'processingQueue': processingQueue
      };

  @override
  toString() {
    return 'PortEventState{'
        'portEventCreate: $portEventCreate,'
        'portEventCreateQueue: $portEventCreateQueue,'
        'error: $error,'
        'queued: $queued,'
        'processingQueue: $processingQueue}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PortEventState &&
          runtimeType == other.runtimeType &&
          portEventCreate == other.portEventCreate &&
          portEventCreateQueue == other.portEventCreateQueue &&
          error == other.error &&
          queued == other.queued &&
          processingQueue == other.processingQueue;

  @override
  int get hashCode =>
      portEventCreate.hashCode ^
      portEventCreateQueue.hashCode ^
      error.hashCode ^
      queued.hashCode ^
      processingQueue.hashCode;
}
