import 'package:cadi_core_app/factory/cadi_enum_factory.dart';
import 'package:cadi_core_app/redux/models/list_converters.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:meta/meta.dart';
import 'package:cadi_core_app/redux/models/models.dart';

@immutable
class ReferenceState {
  final List<CadiEnum> cargoMovementStates;
  final List<CadiEnum> roadCargoMovementStates;
  final List<CadiEnum> cargoMovementRoles;
  final List<CadiEnum> portEventRoles;
  final List<CadiEnum> cargoMovementShipments;
  final List<CadiEnum> roadCargoMovementRoles;
  final List<CadiEnum> roadMovementCargoTypes;
  final List<CadiEnum> portCargoUpdateTypes;
  final List<CadiEnum> portCargoUpdateShipments;
  final List<CadiEnum> restowStates;
  final List<CadiEnum> currencies;
  List<CadiEnum> volumeUnits;
  List<CadiEnum> distanceTravelledUnits;
  final List<CadiGeneric> vesselClasses;
  final List<FuelType> fuelTypes;
  final List<CadiGeneric> cargoTypes;
  final List<CadiGeneric> containerTypes;
  final List<CadiGeneric> jobPackTypes;
  final String error;

  // ignore: prefer_const_constructors_in_immutables
  ReferenceState(
      {required this.cargoMovementStates,
      required this.roadCargoMovementStates,
      required this.cargoMovementRoles,
      required this.cargoMovementShipments,
      required this.roadCargoMovementRoles,
      required this.roadMovementCargoTypes,
      required this.portCargoUpdateTypes,
      required this.portCargoUpdateShipments,
      required this.portEventRoles,
      required this.restowStates,
      required this.currencies,
      required this.volumeUnits,
      required this.distanceTravelledUnits,
      required this.vesselClasses,
      required this.fuelTypes,
      required this.cargoTypes,
      required this.containerTypes,
      required this.jobPackTypes,
      required this.error});

  factory ReferenceState.initial() {
    return ReferenceState(
      cargoMovementStates: CadiEnumFactory().createCargoMovementStates(),
      roadCargoMovementStates: CadiEnumFactory().createCargoMovementStates(),
      cargoMovementRoles: CadiEnumFactory().createCargoMovementRoles(),
      cargoMovementShipments: CadiEnumFactory().createCargoMovementShipments(),
      roadCargoMovementRoles: CadiEnumFactory().createRoadMovementRoles(),
      roadMovementCargoTypes: CadiEnumFactory().createRoadMovementCargoTypes(),
      portCargoUpdateTypes: CadiEnumFactory().createPortCargoUpdateTypes(),
      portCargoUpdateShipments:
          CadiEnumFactory().createPortCargoUpdateShipments(),
      portEventRoles: CadiEnumFactory().createVesselJourneyRoles(),
      restowStates: CadiEnumFactory().createRestowStates(),
      currencies: CadiEnumFactory().createCurrencies(),
      volumeUnits: CadiEnumFactory().createVolumeUnits(),
      distanceTravelledUnits: CadiEnumFactory().createDistanceTravelledUnits(),
      vesselClasses: <CadiGeneric>[CadiGeneric.initial()],
      fuelTypes: <FuelType>[FuelType.initial()],
      cargoTypes: <CadiGeneric>[CadiGeneric.initial()],
      containerTypes: <CadiGeneric>[CadiGeneric.initial()],
      jobPackTypes: <CadiGeneric>[CadiGeneric.initial()],
      error: "",
    );
  }

  ReferenceState copyWith(
      {List<CadiEnum>? cargoMovementStates,
      List<CadiEnum>? cargoMovementRoles,
      List<CadiEnum>? cargoMovementShipments,
      List<CadiEnum>? roadCargoMovementRoles,
      List<CadiEnum>? roadMovementCargoTypes,
      List<CadiEnum>? roadCargoMovementStates,
      List<CadiEnum>? portEventRoles,
      List<CadiEnum>? portCargoUpdateTypes,
      List<CadiEnum>? portCargoUpdateShipments,
      List<CadiEnum>? restowStates,
      List<CadiEnum>? currencies,
      List<CadiEnum>? volumeUnits,
      List<CadiEnum>? distanceTravelledUnits,
      List<CadiGeneric>? vesselClasses,
      List<FuelType>? fuelTypes,
      List<CadiGeneric>? cargoTypes,
      List<CadiGeneric>? containerTypes,
      List<CadiGeneric>? jobPackTypes,
      String? error}) {
    return ReferenceState(
        cargoMovementStates: cargoMovementStates ?? this.cargoMovementStates,
        cargoMovementRoles: cargoMovementRoles ?? this.cargoMovementRoles,
        cargoMovementShipments:
            cargoMovementShipments ?? this.cargoMovementShipments,
        roadCargoMovementRoles:
            roadCargoMovementRoles ?? this.roadCargoMovementRoles,
        roadMovementCargoTypes:
            roadMovementCargoTypes ?? this.roadMovementCargoTypes,
        roadCargoMovementStates:
            roadCargoMovementStates ?? this.roadCargoMovementStates,
        portEventRoles: portEventRoles ?? this.portEventRoles,
        portCargoUpdateTypes: portCargoUpdateTypes ?? this.portCargoUpdateTypes,
        portCargoUpdateShipments:
            portCargoUpdateShipments ?? this.portCargoUpdateShipments,
        restowStates: restowStates ?? this.restowStates,
        currencies: currencies ?? this.currencies,
        volumeUnits: volumeUnits ?? this.volumeUnits,
        distanceTravelledUnits:
            distanceTravelledUnits ?? this.distanceTravelledUnits,
        vesselClasses: vesselClasses ?? this.vesselClasses,
        fuelTypes: fuelTypes ?? this.fuelTypes,
        cargoTypes: cargoTypes ?? this.cargoTypes,
        containerTypes: containerTypes ?? this.containerTypes,
        jobPackTypes: jobPackTypes ?? this.jobPackTypes,
        error: error ?? this.error);
  }

  ReferenceState.fromJson(json)
      : cargoMovementStates = json['cargoMovementStates'] != null
            ? converterCadiEnum(json['cargoMovementStates'])
            : CadiEnumFactory().createCargoMovementStates(),
        cargoMovementRoles = json['cargoMovementRoles'] != null
            ? converterCadiEnum(json['cargoMovementRoles'])
            : CadiEnumFactory().createCargoMovementRoles(),
        cargoMovementShipments = json['cargoMovementShipments'] != null
            ? converterCadiEnum(json['cargoMovementShipments'])
            : CadiEnumFactory().createCargoMovementShipments(),
        roadCargoMovementRoles = json['roadCargoMovementRoles'] != null
            ? converterCadiEnum(json['roadCargoMovementRoles'])
            : CadiEnumFactory().createRoadMovementRoles(),
        roadMovementCargoTypes = json['roadMovementCargoTypes'] != null
            ? converterCadiEnum(json['roadMovementCargoTypes'])
            : CadiEnumFactory().createRoadMovementCargoTypes(),
        portCargoUpdateTypes = json['portCargoUpdateTypes'] != null
            ? converterCadiEnum(json['portCargoUpdateTypes'])
            : CadiEnumFactory().createPortCargoUpdateTypes(),
        portCargoUpdateShipments = json['portCargoUpdateShipments'] != null
            ? converterCadiEnum(json['portCargoUpdateShipments'])
            : CadiEnumFactory().createPortCargoUpdateShipments(),
        roadCargoMovementStates = json['roadCargoMovementStates'] != null
            ? converterCadiEnum(json['roadCargoMovementStates'])
            : CadiEnumFactory().createRoadCargoMovementStates(),
        portEventRoles = json['portEventRoles'] != null
            ? converterCadiEnum(json['portEventRoles'])
            : CadiEnumFactory().createVesselJourneyRoles(),
        restowStates = json['restowStates'] != null
            ? converterCadiEnum(json['restowStates'])
            : CadiEnumFactory().createRestowStates(),
        currencies = json['currencies'] != null
            ? converterCadiEnum(json['currencies'])
            : CadiEnumFactory().createCurrencies(),
        volumeUnits = json['volumeUnits'] != null
            ? converterCadiEnum(json['volumeUnits'])
            : CadiEnumFactory().createVolumeUnits(),
        distanceTravelledUnits = json['distanceTravelledUnits'] != null
            ? converterCadiEnum(json['distanceTravelledUnits'])
            : CadiEnumFactory().createDistanceTravelledUnits(),
        vesselClasses = json['vesselClasses'] != null
            ? converterCadiGeneric(json['vesselClasses'])
            : <CadiGeneric>[CadiGeneric.initial()],
        fuelTypes = json['fuelTypes'] != null
            ? converterFuelType(json['fuelTypes'])
            : <FuelType>[FuelType.initial()],
        cargoTypes = json['cargoTypes'] != null
            ? converterCadiGeneric(json['cargoTypes'])
            : <CadiGeneric>[CadiGeneric.initial()],
        containerTypes = json['containerTypes'] != null
            ? converterCadiGeneric(json['containerTypes'])
            : <CadiGeneric>[CadiGeneric.initial()],
        jobPackTypes = json['jobPackTypes'] != null
            ? converterCadiGeneric(json['jobPackTypes'])
            : <CadiGeneric>[CadiGeneric.initial()],
        error = json['error'] ?? "";

  Map toJson() => {
        //<String, dynamic>
        'cargoMovementStates': cargoMovementStates,
        'cargoMovementRoles': cargoMovementRoles,
        'cargoMovementShipments': cargoMovementShipments,
        'roadCargoMovementRoles': roadCargoMovementRoles,
        'roadMovementCargoTypes': roadMovementCargoTypes,
        'roadCargoMovementStates': roadCargoMovementStates,
        'portEventRoles': portEventRoles,
        'portCargoUpdateTypes': portCargoUpdateTypes,
        'portCargoUpdateShipments': portCargoUpdateShipments,
        'restowStates': restowStates,
        'currencies': currencies,
        'volumeUnits': volumeUnits,
        'distanceTravelledUnits': distanceTravelledUnits,
        'vesselClasses': vesselClasses,
        'fuelTypes': fuelTypes,
        'cargoTypes': cargoTypes,
        'containerTypes': containerTypes,
        'jobPackTypes': jobPackTypes,
        'error': error,
      };

  @override
  toString() {
    return 'ReferenceState{'
        'cargoMovementStates: $cargoMovementStates,'
        'cargoMovementRoles: $cargoMovementRoles,'
        'cargoMovementShipments: $cargoMovementShipments,'
        'roadCargoMovementRoles: $roadCargoMovementRoles,'
        'roadMovementCargoTypes: $roadMovementCargoTypes,'
        'roadCargoMovementStates: $roadCargoMovementStates,'
        'portEventRoles: $portEventRoles,'
        'portCargoUpdateTypes: $portCargoUpdateTypes,'
        'portCargoUpdateShipments: $portCargoUpdateShipments,'
        'restowStates: $restowStates,'
        'currencies: $currencies,'
        'volumeUnits: $volumeUnits,'
        'distanceTravelledUnits: $distanceTravelledUnits,'
        'vesselClasses: $vesselClasses,'
        'fuelTypes: $fuelTypes,'
        'cargoTypes: $cargoTypes,'
        'containerTypes: $containerTypes,'
        'jobPackTypes $jobPackTypes,'
        ' error: $error}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ReferenceState &&
          runtimeType == other.runtimeType &&
          cargoMovementStates == other.cargoMovementStates &&
          cargoMovementRoles == other.cargoMovementRoles &&
          cargoMovementShipments == other.cargoMovementShipments &&
          roadCargoMovementRoles == other.roadCargoMovementRoles &&
          roadMovementCargoTypes == other.roadMovementCargoTypes &&
          roadCargoMovementStates == other.roadCargoMovementStates &&
          portEventRoles == other.portEventRoles &&
          portCargoUpdateTypes == other.portCargoUpdateTypes &&
          portCargoUpdateShipments == other.portCargoUpdateShipments &&
          restowStates == other.restowStates &&
          currencies == other.currencies &&
          volumeUnits == other.volumeUnits &&
          distanceTravelledUnits == other.distanceTravelledUnits &&
          vesselClasses == other.vesselClasses &&
          fuelTypes == other.fuelTypes &&
          cargoTypes == other.cargoTypes &&
          containerTypes == other.containerTypes &&
          jobPackTypes == other.jobPackTypes &&
          error == other.error;

  @override
  int get hashCode =>
      cargoMovementStates.hashCode ^
      cargoMovementRoles.hashCode ^
      cargoMovementShipments.hashCode ^
      roadCargoMovementRoles.hashCode ^
      roadMovementCargoTypes.hashCode ^
      roadCargoMovementStates.hashCode ^
      portEventRoles.hashCode ^
      portCargoUpdateTypes.hashCode ^
      portCargoUpdateShipments.hashCode ^
      restowStates.hashCode ^
      currencies.hashCode ^
      volumeUnits.hashCode ^
      distanceTravelledUnits.hashCode ^
      vesselClasses.hashCode ^
      fuelTypes.hashCode ^
      cargoTypes.hashCode ^
      containerTypes.hashCode ^
      jobPackTypes.hashCode ^
      error.hashCode;
}
