import 'package:meta/meta.dart';
import 'package:cadi_core_app/redux/models/models.dart';

@immutable
class ReportState {
  final PortReportSummary portReportSummary;
  final PortMovementReportSummary portMovementReportSummary;
  final String filterVesselName;
  final String reportType;
  final String error;

  // ignore: prefer_const_constructors_in_immutables
  ReportState(
      {
        required this.portReportSummary,
        required this.portMovementReportSummary,
        required this.filterVesselName,
        required this.reportType,
        required this.error
      });

  factory ReportState.initial() {
    return ReportState(
      portReportSummary: PortReportSummary.initial(),
      portMovementReportSummary: PortMovementReportSummary.initial(),
      reportType: "",
      filterVesselName: "",
      error: "",
    );
  }

  ReportState copyWith(
      {
        PortReportSummary? portReportSummary,
        PortMovementReportSummary? portMovementReportSummary,
        String? filterVesselName,
        String? reportType,
        String? error})
  {
    return ReportState(
        portReportSummary: portReportSummary ?? this.portReportSummary,
        portMovementReportSummary: portMovementReportSummary ?? this.portMovementReportSummary,
        filterVesselName: filterVesselName ?? this.filterVesselName,
        reportType: reportType ?? this.reportType,
        error: error ?? this.error
    );
  }

  ReportState.fromJson(json)
      :
        portReportSummary = json['portReportSummary'] =
            PortReportSummary.fromJson(json['portReportSummary']),
        portMovementReportSummary = json['portMovementReportSummary'] =
            PortMovementReportSummary.fromJson(json['portMovementReportSummary']),
        filterVesselName = json['filterVesselName'] ?? "",
        reportType = json['reportType'],
        error = json['error'] ?? "";
  Map toJson() =>
      {
        //<String, dynamic>
        'portReportSummary': portReportSummary,
        'portMovementReportSummary': portMovementReportSummary,
        'filterVesselName': filterVesselName,
        'reportType': reportType,
        'error': error,
      };

  @override
  toString() {
    return 'ReportState{'
        'portReportSummary: $portReportSummary,'
        'portMovementReportSummary: $portMovementReportSummary,'
        'filterVesselName: $filterVesselName,'
        'reportType: $reportType,'
        ' error: $error}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ReportState &&
          runtimeType == other.runtimeType &&
          portReportSummary == other.portReportSummary &&
          portMovementReportSummary == other.portMovementReportSummary &&
          filterVesselName == other.filterVesselName &&
          reportType == other.reportType &&
          error == other.error;

  @override
  int get hashCode =>
      portReportSummary.hashCode ^
      portMovementReportSummary.hashCode ^
      filterVesselName.hashCode ^
      reportType.hashCode ^
      error.hashCode;
}
