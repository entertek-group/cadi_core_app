import 'package:cadi_core_app/redux/models/list_converters.dart';
import 'package:cadi_core_app/redux/models/road_movement/road_movement_models.dart';
import 'package:meta/meta.dart';

@immutable
class RoadMovementState {
  final RoadMovementCreate roadMovementCreate;
  final List<RoadMovementCreateQueue>? roadMovementCreateQueue;
  final String error;
  final bool queued;
  final bool processingQueue;

  const RoadMovementState(
      {required this.roadMovementCreate,
      required this.roadMovementCreateQueue,
      required this.error,
      required this.queued,
      required this.processingQueue});

  factory RoadMovementState.initial() {
    return RoadMovementState(
        roadMovementCreate: RoadMovementCreate.initial(),
        roadMovementCreateQueue: null,
        error: "",
        queued: false,
        processingQueue: false);
  }

  RoadMovementState copyWith(
      {RoadMovementCreate? roadMovementCreate,
      List<RoadMovementCreateQueue>? roadMovementCreateQueue,
      String? error,
      bool? queued,
      bool? processingQueue}) {
    return RoadMovementState(
        roadMovementCreate: roadMovementCreate ?? this.roadMovementCreate,
        roadMovementCreateQueue:
            roadMovementCreateQueue ?? this.roadMovementCreateQueue,
        error: error ?? this.error,
        queued: queued ?? this.queued,
        processingQueue: processingQueue ?? this.processingQueue);
  }

  RoadMovementState.fromJson(json)
      : roadMovementCreate = json['roadMovementCreate'] != null
            ? RoadMovementCreate.fromJson(json['roadMovementCreate'])
            : RoadMovementCreate.initial(),
        roadMovementCreateQueue = json['roadMovementCreateQueue'] != null
            ? converterRoadMovementCreateQueue(json['roadMovementCreateQueue'])
            : null,
        error = json['error'] ?? "",
        queued = json['queued'] != null ? json['queued'] as bool : false,
        processingQueue = json['processingQueue'] != null
            ? json['processingQueue'] as bool
            : false;

  Map toJson() => {
        'roadMovementCreate': roadMovementCreate,
        'roadMovementCreateQueue': roadMovementCreateQueue,
        'error': error,
        'queued': queued,
        'processingQueue': processingQueue
      };

  @override
  toString() {
    return 'RoadMovementState{'
        'roadMovementCreate: $roadMovementCreate,'
        'roadMovementCreateQueue: $roadMovementCreateQueue,'
        'error: $error,'
        'queued: $queued,'
        'processingQueue: $processingQueue}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RoadMovementState &&
          runtimeType == other.runtimeType &&
          roadMovementCreate == other.roadMovementCreate &&
          roadMovementCreateQueue == other.roadMovementCreateQueue &&
          error == other.error &&
          queued == other.queued &&
          processingQueue == other.processingQueue;

  @override
  int get hashCode =>
      roadMovementCreate.hashCode ^
      roadMovementCreateQueue.hashCode ^
      error.hashCode ^
      queued.hashCode ^
      processingQueue.hashCode;
}
