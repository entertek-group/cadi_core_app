import 'package:cadi_core_app/redux/models/list_converters.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:meta/meta.dart';

@immutable
class StorageState {
  final List<FileStore> fileStores;
  final FileStore tempStorage;

  const StorageState({required this.fileStores, required this.tempStorage});

  factory StorageState.initial() {
    return StorageState(
      fileStores: [
        FileStore(
            fileName: "",
            filePath: "",
            fileType: "",
            storageType: "",
            uniqueId: "",
            apiId: null)
      ],
      tempStorage: FileStore(
          fileName: "",
          filePath: "",
          fileType: "",
          storageType: "",
          uniqueId: "",
          apiId: null),
    );
  }

  StorageState copyWith({List<FileStore>? fileStores, FileStore? tempStorage}) {
    return StorageState(
        fileStores: fileStores ?? this.fileStores,
        tempStorage: tempStorage ?? this.tempStorage);
  }

  factory StorageState.fromJson(dynamic parsedJson) {
    return StorageState(
      fileStores: parsedJson['fileStores'] != null
          ? converterFileStoreList(parsedJson['fileStores'])
          : <FileStore>[
              FileStore(
                  fileName: "",
                  filePath: "",
                  fileType: "",
                  storageType: "",
                  uniqueId: "",
                  apiId: null)
            ],
      tempStorage: parsedJson['tempStorage'] != null
          ? FileStore.fromJson(parsedJson['tempStorage'])
          : FileStore(
              fileName: "",
              filePath: "",
              fileType: "",
              storageType: "",
              uniqueId: "",
              apiId: null),
    );
  }

  dynamic toJson() => {'fileStores': fileStores, 'tempStorage': tempStorage};

  @override
  toString() {
    return 'StorageState{fileStores: $fileStores,'
        'tempStorage: $tempStorage}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is StorageState &&
          runtimeType == other.runtimeType &&
          fileStores == other.fileStores &&
          tempStorage == other.tempStorage;

  @override
  int get hashCode => fileStores.hashCode ^ tempStorage.hashCode;
}
