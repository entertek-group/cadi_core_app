import 'package:cadi_core_app/redux/models/list_converters.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:meta/meta.dart';

@immutable
class TruckJourneyState {
  final List<SailingSchedule> sailingSchedules;
  final List<SailingSchedule> filteredSailingSchedules;
  final SailingSchedule selectedSailingSchedule;
  //#truck journey create
  final TruckJourneyCreate truckJourneyCreate;
  final List<TruckJourneyCreateQueue> truckJourneyCreateQueue;
  //#truck journey update
  final List<TruckJourneyUpdateQueue> truckJourneyUpdateQueue;
  final List<TruckJourney> truckUpdateJourneys;
  final List<TruckJourney> filteredTruckUpdateJourneys;
  final TruckJourney selectedTruckUpdateJourney;

  final String error;

  const TruckJourneyState(
      {required this.sailingSchedules,
      required this.filteredSailingSchedules,
      required this.selectedSailingSchedule,
      required this.truckUpdateJourneys,
      required this.filteredTruckUpdateJourneys,
      required this.truckJourneyUpdateQueue,
      required this.selectedTruckUpdateJourney,
      required this.truckJourneyCreate,
      required this.truckJourneyCreateQueue,
      required this.error});

  factory TruckJourneyState.initial() {
    return TruckJourneyState(
      sailingSchedules: <SailingSchedule>[SailingSchedule.initial()],
      filteredSailingSchedules: <SailingSchedule>[SailingSchedule.initial()],
      selectedSailingSchedule: SailingSchedule.initial(),
      truckUpdateJourneys: <TruckJourney>[TruckJourney.initial()],
      filteredTruckUpdateJourneys: <TruckJourney>[TruckJourney.initial()],
      selectedTruckUpdateJourney: TruckJourney.initial(),
      truckJourneyCreate: TruckJourneyCreate.initial(),
      truckJourneyCreateQueue: <TruckJourneyCreateQueue>[
        TruckJourneyCreateQueue.initial()
      ],
      truckJourneyUpdateQueue: <TruckJourneyUpdateQueue>[
        TruckJourneyUpdateQueue.initial()
      ],
      error: "",
    );
  }

  TruckJourneyState copyWith({
    List<SailingSchedule>? sailingSchedules,
    List<SailingSchedule>? filteredSailingSchedules,
    SailingSchedule? selectedSailingSchedule,
    List<TruckJourney>? truckUpdateJourneys,
    List<TruckJourney>? filteredTruckUpdateJourneys,
    TruckJourney? selectedTruckUpdateJourney,
    TruckJourneyCreate? truckJourneyCreate,
    List<TruckJourneyCreateQueue>? truckJourneyCreateQueue,
    List<TruckJourneyUpdateQueue>? truckJourneyUpdateQueue,
    String? error,
  }) {
    return TruckJourneyState(
        sailingSchedules: sailingSchedules ?? this.sailingSchedules,
        filteredSailingSchedules:
            filteredSailingSchedules ?? this.filteredSailingSchedules,
        selectedSailingSchedule:
            selectedSailingSchedule ?? this.selectedSailingSchedule,
        filteredTruckUpdateJourneys:
            filteredTruckUpdateJourneys ?? this.filteredTruckUpdateJourneys,
        truckUpdateJourneys: truckUpdateJourneys ?? this.truckUpdateJourneys,
        truckJourneyCreateQueue:
            truckJourneyCreateQueue ?? this.truckJourneyCreateQueue,
        truckJourneyCreate: truckJourneyCreate ?? this.truckJourneyCreate,
        truckJourneyUpdateQueue:
            truckJourneyUpdateQueue ?? this.truckJourneyUpdateQueue,
        selectedTruckUpdateJourney:
            selectedTruckUpdateJourney ?? this.selectedTruckUpdateJourney,
        error: error ?? this.error);
  }

  factory TruckJourneyState.fromJson(dynamic parsedJson) {
    return TruckJourneyState(
        truckJourneyCreate: parsedJson['truckJourneyCreate'] != null
            ? TruckJourneyCreate.fromJson(parsedJson['truckJourneyCreate'])
            : TruckJourneyCreate.initial(),
        truckJourneyCreateQueue: parsedJson['truckJourneyCreateQueue'] != null
            ? converterTruckJourneyCreateQueue(
                parsedJson['truckJourneyCreateQueue'])
            : <TruckJourneyCreateQueue>[TruckJourneyCreateQueue.initial()],
        sailingSchedules: parsedJson['sailingSchedules'] != null
            ? converterSailingSchedule(parsedJson['sailingSchedules'])
            : <SailingSchedule>[SailingSchedule.initial()],
        filteredSailingSchedules: parsedJson['filteredSailingSchedules'] != null
            ? converterSailingSchedule(parsedJson['filteredSailingSchedules'])
            : <SailingSchedule>[SailingSchedule.initial()],
        selectedSailingSchedule: parsedJson['selectedSailingSchedule'] != null
            ? SailingSchedule.fromJson(parsedJson['selectedSailingSchedule'])
            : SailingSchedule.initial(),
        truckJourneyUpdateQueue: parsedJson['truckJourneyUpdateQueue'] != null
            ? converterTruckJourneyUpdateQueue(
                parsedJson['truckJourneyUpdateQueue'])
            : <TruckJourneyUpdateQueue>[TruckJourneyUpdateQueue.initial()],
        filteredTruckUpdateJourneys:
            parsedJson['filteredTruckUpdateJourneys'] != null
                ? converterTruckUpdateJourneys(
                    parsedJson['filteredTruckUpdateJourneys'])
                : <TruckJourney>[TruckJourney.initial()],
        truckUpdateJourneys: parsedJson['truckUpdateJourneys'] != null
            ? converterTruckUpdateJourneys(parsedJson['truckUpdateJourneys'])
            : <TruckJourney>[TruckJourney.initial()],
        error: parsedJson['error'] ?? "",
        selectedTruckUpdateJourney: parsedJson['selectedTruckUpdateJourney'] !=
                null
            ? TruckJourney.fromJson(parsedJson['selectedTruckUpdateJourney'])
            : TruckJourney.initial());
  }

  dynamic toJson() => {
        'sailingSchedules': sailingSchedules,
        'filteredSailingSchedules': filteredSailingSchedules,
        'selectedSailingSchedule': selectedSailingSchedule,
        'truckJourneyCreate': truckJourneyCreate,
        'truckJourneyCreateQueue': truckJourneyCreateQueue,
        'truckJourneyUpdateQueue': truckJourneyUpdateQueue,
        'truckUpdateJourney': truckUpdateJourneys,
        'filteredTruckUpdateJourney': filteredTruckUpdateJourneys,
        'selectedTruckUpdateJourney': selectedTruckUpdateJourney,
        'error': error
      };

  @override
  toString() {
    return 'TruckJourneyState{sailingSchedules: $sailingSchedules,'
        'filteredSailingSchedules: $filteredSailingSchedules,'
        'selectedSailingSchedule: $selectedSailingSchedule,'
        'truckJourneyUpdateQueue: $truckJourneyUpdateQueue,'
        'filteredTruckUpdateJourney: $filteredTruckUpdateJourneys'
        'truckUpdateJourney: $truckUpdateJourneys'
        'truckJourneyCreate: $truckJourneyCreate,'
        'truckJourneyCreateQueue: $truckJourneyCreateQueue,'
        'selectedTruckUpdateJourney: $selectedTruckUpdateJourney'
        'error: $error}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TruckJourneyState &&
          runtimeType == other.runtimeType &&
          sailingSchedules == other.sailingSchedules &&
          filteredSailingSchedules == other.filteredSailingSchedules &&
          selectedSailingSchedule == other.selectedSailingSchedule &&
          truckJourneyCreate == other.truckJourneyCreate &&
          truckJourneyCreateQueue == other.truckJourneyCreateQueue &&
          truckJourneyUpdateQueue == other.truckJourneyUpdateQueue &&
          truckUpdateJourneys == other.truckUpdateJourneys &&
          filteredTruckUpdateJourneys == other.filteredTruckUpdateJourneys &&
          selectedTruckUpdateJourney == other.selectedTruckUpdateJourney &&
          error == other.error;

  @override
  int get hashCode =>
      sailingSchedules.hashCode ^
      filteredSailingSchedules.hashCode ^
      selectedSailingSchedule.hashCode ^
      truckJourneyCreate.hashCode ^
      truckJourneyCreateQueue.hashCode ^
      truckJourneyUpdateQueue.hashCode ^
      truckUpdateJourneys.hashCode ^
      filteredTruckUpdateJourneys.hashCode ^
      selectedTruckUpdateJourney.hashCode ^
      error.hashCode;
}
