import 'package:cadi_core_app/redux/models/models.dart';
import 'package:meta/meta.dart';
import '../models/ui_screen/ui_screen_models.dart';

@immutable
class UiState {
  final bool uiDropDownOrInputAutoContinue;
  final bool uiAutoSubmitPage;
  final UiCargoHome uiCargoHome;
  final UiPortCargoUpdateHome uiPortCargoUpdateHome;
  final String portCargoUpdateRole;
  final String cargoRole;
  final String portEventRole;
  final String truckJourneyRole;
  final UiPortCargoUpdateFreightSearch uiPortCargoUpdateFreightSearch;
  final UiCargoFreightSearch uiCargoFreightSearch;
  final UiCargoTruckDetails uiCargoTruckDetails;
  final UiCargoUpdateSearch uiCargoUpdateSearch;
  final UiCargoReportSearch uiCargoReportSearch;
  final UiPortEventIndex uiPortEventIndex;
  final UiPortEventCapture uiPortEventCapture;
  final UiVesselJourneyVessel uiVesselJourneyVessel;
  final UiVesselUpdateIndex uiVesselUpdateIndex;
  final UiRoadMovementSearch uiRoadMovementSearch;
  final UiTruckJourneyIndex uiTruckJourneyIndex;
  final UiTruckJourneySearch uiTruckJourneySearch;
  final UiTruckJourneyVessel uiTruckJourneyVessel;
  final UiTruckUpdateIndex uiTruckUpdateIndex;
  final UiRoadMovementHome uiRoadMovementHome;
  final String roadMovementRole;

  const UiState({
    required this.uiDropDownOrInputAutoContinue,
    required this.uiAutoSubmitPage,
    required this.uiCargoHome,
    required this.uiPortCargoUpdateHome,
    required this.cargoRole,
    required this.portCargoUpdateRole,
    required this.portEventRole,
    required this.truckJourneyRole,
    required this.uiPortCargoUpdateFreightSearch,
    required this.uiCargoFreightSearch,
    required this.uiCargoTruckDetails,
    required this.uiCargoUpdateSearch,
    required this.uiCargoReportSearch,
    required this.uiPortEventIndex,
    required this.uiPortEventCapture,
    required this.uiTruckJourneyIndex,
    required this.uiTruckJourneySearch,
    required this.uiTruckJourneyVessel,
    required this.uiTruckUpdateIndex,
    required this.uiVesselJourneyVessel,
    required this.uiVesselUpdateIndex,
    required this.uiRoadMovementSearch,
    required this.uiRoadMovementHome,
    required this.roadMovementRole,
  });

  factory UiState.initial() {
    return UiState(
      uiDropDownOrInputAutoContinue: false,
      uiAutoSubmitPage: false,
      uiCargoHome: UiCargoHome.initial(),
      uiPortCargoUpdateHome: UiPortCargoUpdateHome.initial(),
      cargoRole: "",
      portCargoUpdateRole: "",
      portEventRole: "",
      truckJourneyRole: "",
      uiPortCargoUpdateFreightSearch: UiPortCargoUpdateFreightSearch.initial(),
      uiCargoFreightSearch: UiCargoFreightSearch.initial(),
      uiCargoTruckDetails: UiCargoTruckDetails.initial(),
      uiCargoUpdateSearch: UiCargoUpdateSearch.initial(),
      uiCargoReportSearch: UiCargoReportSearch.initial(),
      uiPortEventIndex: UiPortEventIndex.initial(),
      uiPortEventCapture: UiPortEventCapture.initial(),
      uiVesselJourneyVessel: UiVesselJourneyVessel.initial(),
      uiVesselUpdateIndex: UiVesselUpdateIndex.initial(),
      uiTruckJourneyIndex: UiTruckJourneyIndex.initial(),
      uiTruckJourneySearch: UiTruckJourneySearch.initial(),
      uiTruckJourneyVessel: UiTruckJourneyVessel.initial(),
      uiTruckUpdateIndex: UiTruckUpdateIndex.initial(),
      uiRoadMovementSearch: UiRoadMovementSearch.initial(),
      uiRoadMovementHome: UiRoadMovementHome.initial(),
      roadMovementRole: "",
    );
  }

  UiState copyWith({
    bool? uiDropDownOrInputAutoContinue,
    bool? uiAutoSubmitPage,
    UiCargoHome? uiCargoHome,
    UiPortCargoUpdateHome? uiPortCargoUpdateHome,
    String? cargoRole,
    String? portCargoUpdateRole,
    String? portEventRole,
    String? truckJourneyRole,
    UiPortCargoUpdateFreightSearch? uiPortCargoUpdateFreightSearch,
    UiCargoFreightSearch? uiCargoFreightSearch,
    UiCargoTruckDetails? uiCargoTruckDetails,
    UiCargoUpdateSearch? uiCargoUpdateSearch,
    UiCargoReportSearch? uiCargoReportSearch,
    UiPortEventIndex? uiPortEventIndex,
    UiPortEventCapture? uiPortEventCapture,
    UiVesselJourneyVessel? uiVesselJourneyVessel,
    UiVesselUpdateIndex? uiVesselUpdateIndex,
    UiTruckJourneyIndex? uiTruckJourneyIndex,
    UiTruckJourneySearch? uiTruckJourneySearch,
    UiTruckJourneyVessel? uiTruckJourneyVessel,
    UiTruckUpdateIndex? uiTruckUpdateIndex,
    UiRoadMovementSearch? uiRoadMovementSearch,
    UiRoadMovementHome? uiRoadMovementHome,
    String? roadMovementRole,
  }) {
    return UiState(
      uiDropDownOrInputAutoContinue:
          uiDropDownOrInputAutoContinue ?? this.uiDropDownOrInputAutoContinue,
      uiAutoSubmitPage: uiAutoSubmitPage ?? this.uiAutoSubmitPage,
      uiCargoHome: uiCargoHome ?? this.uiCargoHome,
      cargoRole: cargoRole ?? this.cargoRole,
      portCargoUpdateRole: portCargoUpdateRole ?? this.portCargoUpdateRole,
      uiPortCargoUpdateHome:
          uiPortCargoUpdateHome ?? this.uiPortCargoUpdateHome,
      portEventRole: portEventRole ?? this.portEventRole,
      truckJourneyRole: truckJourneyRole ?? this.truckJourneyRole,
      uiPortCargoUpdateFreightSearch:
          uiPortCargoUpdateFreightSearch ?? this.uiPortCargoUpdateFreightSearch,
      uiCargoFreightSearch: uiCargoFreightSearch ?? this.uiCargoFreightSearch,
      uiCargoTruckDetails: uiCargoTruckDetails ?? this.uiCargoTruckDetails,
      uiCargoUpdateSearch: uiCargoUpdateSearch ?? this.uiCargoUpdateSearch,
      uiCargoReportSearch: uiCargoReportSearch ?? this.uiCargoReportSearch,
      uiPortEventIndex: uiPortEventIndex ?? this.uiPortEventIndex,
      uiPortEventCapture: uiPortEventCapture ?? this.uiPortEventCapture,
      uiVesselJourneyVessel:
          uiVesselJourneyVessel ?? this.uiVesselJourneyVessel,
      uiVesselUpdateIndex: uiVesselUpdateIndex ?? this.uiVesselUpdateIndex,
      uiTruckJourneyIndex: uiTruckJourneyIndex ?? this.uiTruckJourneyIndex,
      uiTruckJourneySearch: uiTruckJourneySearch ?? this.uiTruckJourneySearch,
      uiTruckJourneyVessel: uiTruckJourneyVessel ?? this.uiTruckJourneyVessel,
      uiTruckUpdateIndex: uiTruckUpdateIndex ?? this.uiTruckUpdateIndex,
      uiRoadMovementSearch: uiRoadMovementSearch ?? this.uiRoadMovementSearch,
      uiRoadMovementHome: uiRoadMovementHome ?? this.uiRoadMovementHome,
      roadMovementRole: roadMovementRole ?? this.roadMovementRole,
    );
  }

  factory UiState.fromJson(dynamic parsedJson) {
    return UiState(
      uiDropDownOrInputAutoContinue:
          parsedJson['uiDropDownOrInputAutoContinue'] ?? false,
      uiAutoSubmitPage: parsedJson['uiAutoSubmitPage'] ?? false,
      uiCargoHome: parsedJson['uiCargoHome'] != null
          ? UiCargoHome.fromJson(parsedJson['uiCargoHome'])
          : UiCargoHome.initial(),
      uiPortCargoUpdateHome: parsedJson['uiPortCargoUpdateHome'] != null
          ? UiPortCargoUpdateHome.fromJson(parsedJson['uiPortCargoUpdateHome'])
          : UiPortCargoUpdateHome.initial(),
      cargoRole: parsedJson['cargoRole'] ?? '',
      portCargoUpdateRole: parsedJson['portCargoUpdateRole'] ?? '',
      portEventRole: parsedJson['portEventRole'] ?? '',
      truckJourneyRole: parsedJson['truckJourneyRole'] ?? '',
      roadMovementRole: parsedJson['roadMovementRole'] ?? '',
      uiPortCargoUpdateFreightSearch:
          parsedJson['uiPortCargoUpdateFreightSearch'] != null
              ? UiPortCargoUpdateFreightSearch.fromJson(
                  parsedJson['uiPortCargoUpdateFreightSearch'])
              : UiPortCargoUpdateFreightSearch.initial(),
      uiCargoFreightSearch: parsedJson['uiCargoFreightSearch'] != null
          ? UiCargoFreightSearch.fromJson(parsedJson['uiCargoFreightSearch'])
          : UiCargoFreightSearch.initial(),
      uiCargoTruckDetails: parsedJson['uiCargoTruckDetails'] != null
          ? UiCargoTruckDetails.fromJson(parsedJson['uiCargoTruckDetails'])
          : UiCargoTruckDetails.initial(),
      uiCargoUpdateSearch: parsedJson['uiCargoUpdateSearch'] != null
          ? UiCargoUpdateSearch.fromJson(parsedJson['uiCargoUpdateSearch'])
          : UiCargoUpdateSearch.initial(),
      uiCargoReportSearch: parsedJson['uiCargoReportSearch'] != null
          ? UiCargoReportSearch.fromJson(parsedJson['uiCargoReportSearch'])
          : UiCargoReportSearch.initial(),
      uiPortEventIndex: parsedJson['uiPortEventIndex'] != null
          ? UiPortEventIndex.fromJson(parsedJson['uiPortEventIndex'])
          : UiPortEventIndex.initial(),
      uiPortEventCapture: parsedJson['uiPortEventCapture'] != null
          ? UiPortEventCapture.fromJson(parsedJson['uiPortEventCapture'])
          : UiPortEventCapture.initial(),
      uiVesselJourneyVessel: parsedJson['uiVesselJourneyVessel'] != null
          ? UiVesselJourneyVessel.fromJson(parsedJson['uiVesselJourneyVessel'])
          : UiVesselJourneyVessel.initial(),
      uiVesselUpdateIndex: parsedJson['uiVesselUpdateIndex'] != null
          ? UiVesselUpdateIndex.fromJson(parsedJson['uiVesselUpdateIndex'])
          : UiVesselUpdateIndex.initial(),
      uiTruckJourneyIndex: parsedJson['uiTruckJourneyIndex'] != null
          ? UiTruckJourneyIndex.fromJson(parsedJson['uiTruckJourneyIndex'])
          : UiTruckJourneyIndex.initial(),
      uiTruckJourneySearch: parsedJson['uiTruckJourneySearch'] != null
          ? UiTruckJourneySearch.fromJson(parsedJson['uiTruckJourneySearch'])
          : UiTruckJourneySearch.initial(),
      uiTruckJourneyVessel: parsedJson['uiTruckJourneyVessel'] != null
          ? UiTruckJourneyVessel.fromJson(parsedJson['uiTruckJourneyVessel'])
          : UiTruckJourneyVessel.initial(),
      uiTruckUpdateIndex: parsedJson['uiTruckUpdateIndex'] != null
          ? UiTruckUpdateIndex.fromJson(parsedJson['uiTruckUpdateIndex'])
          : UiTruckUpdateIndex.initial(),
      uiRoadMovementSearch: parsedJson['uiRoadMovementSearch'] != null
          ? UiRoadMovementSearch.fromJson(parsedJson['uiRoadMovementSearch'])
          : UiRoadMovementSearch.initial(),
      uiRoadMovementHome: parsedJson['uiRoadMovementHome'] != null
          ? UiRoadMovementHome.fromJson(parsedJson['uiRoadMovementHome'])
          : UiRoadMovementHome.initial(),
    );
  }

  dynamic toJson() => {
        'uiDropDownOrInputAutoContinue': uiDropDownOrInputAutoContinue,
        'uiAutoSubmitPage': uiAutoSubmitPage,
        'uiCargoHome': uiCargoHome,
        'uiPortCargoUpdateHome': uiPortCargoUpdateHome,
        'cargoRole': cargoRole,
        'portCargoUpdateRole': portCargoUpdateRole,
        'portEventRole': portEventRole,
        'truckJourneyRole': truckJourneyRole,
        'uiPortCargoUpdateFreightSearch': uiPortCargoUpdateFreightSearch,
        'uiCargoFreightSearch': uiCargoFreightSearch,
        'uiCargoTruckDetails': uiCargoTruckDetails,
        'uiCargoUpdateSearch': uiCargoUpdateSearch,
        'uiCargoReportSearch': uiCargoReportSearch,
        'uiPortEventIndex': uiPortEventIndex,
        'uiPortEventCapture': uiPortEventCapture,
        'uiVesselJourneyVessel': uiVesselJourneyVessel,
        'uiVesselUpdateIndex': uiVesselUpdateIndex,
        'uiTruckJourneyIndex': uiTruckJourneyIndex,
        'uiTruckJourneySearch': uiTruckJourneySearch,
        'uiTruckJourneyVessel': uiTruckJourneyVessel,
        'uiTruckUpdateIndex': uiTruckUpdateIndex,
        'uiRoadMovementSearch': uiRoadMovementSearch,
        'uiRoadMovementHome': uiRoadMovementHome,
        'roadMovementRole': roadMovementRole,
      };

  @override
  toString() {
    return 'UiState{uiCargoHome: $uiCargoHome,'
        'uiPortCargoUpdateHome: $uiPortCargoUpdateHome,'
        'uiDropDownOrInputAutoContinue: $uiDropDownOrInputAutoContinue,'
        'uiAutoSubmitPage: $uiAutoSubmitPage,'
        'cargoRole: $cargoRole,'
        'portCargoUpdateRole: $portCargoUpdateRole,'
        'portEventRole: $portEventRole,'
        'truckJourneyRole: $truckJourneyRole,'
        'uiPortCargoUpdateFreightSearch: $uiPortCargoUpdateFreightSearch,'
        'uiCargoFreightSearch: $uiCargoFreightSearch,'
        'uiCargoTruckDetails: $uiCargoTruckDetails,'
        'uiCargoReportSearch: $uiCargoReportSearch,'
        'uiCargoUpdateSearch: $uiCargoUpdateSearch'
        'uiPortEventIndex: $uiPortEventIndex,'
        'uiPortEventCapture: $uiPortEventCapture,'
        'uiVesselJourneyVessel: $uiVesselJourneyVessel,'
        'uiVesselUpdateIndex: $uiVesselUpdateIndex'
        'uiTruckJourneyIndex: $uiTruckJourneyIndex,'
        'uiTruckJourneySearch: $uiTruckJourneySearch,'
        'uiTruckJourneyVessel: $uiTruckJourneyVessel,'
        'uiTruckUpdateIndex: $uiTruckUpdateIndex,'
        'uiRoadMovementSearch: $uiRoadMovementSearch,'
        'uiRoadMovementHome: $uiRoadMovementHome,'
        'roadMovementRole: $roadMovementRole,'
        '}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UiState &&
          runtimeType == other.runtimeType &&
          uiDropDownOrInputAutoContinue ==
              other.uiDropDownOrInputAutoContinue &&
          uiAutoSubmitPage == other.uiAutoSubmitPage &&
          uiCargoHome == other.uiCargoHome &&
          uiPortCargoUpdateHome == other.uiPortCargoUpdateHome &&
          cargoRole == other.cargoRole &&
          portCargoUpdateRole == other.portCargoUpdateRole &&
          portEventRole == other.portEventRole &&
          truckJourneyRole == other.truckJourneyRole &&
          uiPortCargoUpdateFreightSearch ==
              other.uiPortCargoUpdateFreightSearch &&
          uiCargoFreightSearch == other.uiCargoFreightSearch &&
          uiCargoTruckDetails == other.uiCargoTruckDetails &&
          uiCargoUpdateSearch == other.uiCargoUpdateSearch &&
          uiCargoReportSearch == other.uiCargoReportSearch &&
          uiPortEventIndex == other.uiPortEventIndex &&
          uiPortEventCapture == other.uiPortEventCapture &&
          uiVesselJourneyVessel == other.uiVesselJourneyVessel &&
          uiVesselUpdateIndex == other.uiVesselUpdateIndex &&
          uiTruckJourneyIndex == other.uiTruckJourneyIndex &&
          uiTruckJourneySearch == other.uiTruckJourneySearch &&
          uiTruckJourneyVessel == other.uiTruckJourneyVessel &&
          uiTruckUpdateIndex == other.uiTruckUpdateIndex &&
          uiRoadMovementSearch == other.uiRoadMovementSearch &&
          uiRoadMovementHome == other.uiRoadMovementHome &&
          roadMovementRole == other.roadMovementRole;

  @override
  int get hashCode =>
      uiDropDownOrInputAutoContinue.hashCode ^
      uiAutoSubmitPage.hashCode ^
      uiCargoHome.hashCode ^
      uiPortCargoUpdateHome.hashCode ^
      cargoRole.hashCode ^
      portCargoUpdateRole.hashCode ^
      portEventRole.hashCode ^
      truckJourneyRole.hashCode ^
      uiPortCargoUpdateFreightSearch.hashCode ^
      uiCargoFreightSearch.hashCode ^
      uiCargoTruckDetails.hashCode ^
      uiCargoUpdateSearch.hashCode ^
      uiCargoReportSearch.hashCode ^
      uiPortEventIndex.hashCode ^
      uiPortEventCapture.hashCode ^
      uiVesselJourneyVessel.hashCode ^
      uiVesselUpdateIndex.hashCode ^
      uiTruckJourneyIndex.hashCode ^
      uiTruckJourneySearch.hashCode ^
      uiTruckJourneyVessel.hashCode ^
      uiTruckUpdateIndex.hashCode ^
      uiRoadMovementSearch.hashCode ^
      uiRoadMovementHome.hashCode ^
      roadMovementRole.hashCode;
}
