import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/services.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/helpers/constants.dart';

void initConnectivity(Store store) async {
  final Connectivity connectivity = Connectivity();
  late List<ConnectivityResult> result;
  // Platform messages may fail, so we use a try/catch PlatformException.
  try {
    result = await connectivity.checkConnectivity();
  } on PlatformException catch (e) {
    print('Couldn\'t check connectivity status; Error: $e');
    return;
  }
  // If the widget was removed from the tree while the asynchronous platform
  // message was in flight, we want to discard the reply rather than calling
  // setState to update our non-existent appearance.
  var connectionStatus = getConnectivityStatus(result);
  store.dispatch(SetConnectivity(connectionStatus, DateTime.now().toString()));
}

String getConnectivityStatus(List<ConnectivityResult> connectivityResult) {
  String connectionStatus = "";

  if (connectivityResult.contains(ConnectivityResult.mobile)) {
    connectionStatus = ConnectivityConstants.mobile;
    // Mobile network available.
  } else if (connectivityResult.contains(ConnectivityResult.wifi)) {
    connectionStatus = ConnectivityConstants.wifi;
    // Wi-fi is available.
    // Note for Android:
    // When both mobile and Wi-Fi are turned on system will return Wi-Fi only as active network type
  } else if (connectivityResult.contains(ConnectivityResult.ethernet)) {
    connectionStatus = ConnectivityConstants.ethernet;
    // Ethernet connection available.
  } else if (connectivityResult.contains(ConnectivityResult.vpn)) {
    // Vpn connection active.
    // Note for iOS and macOS:
    // There is no separate network interface type for [vpn].
    // It returns [other] on any device (also simulator)
  } else if (connectivityResult.contains(ConnectivityResult.bluetooth)) {
    // Bluetooth connection available.
  } else if (connectivityResult.contains(ConnectivityResult.other)) {
    // Connected to a network which is not in the above mentioned networks.
  } else if (connectivityResult.contains(ConnectivityResult.none)) {
    connectionStatus = ConnectivityConstants.none;
    // No available network types
  } else {
    connectionStatus = ConnectivityConstants.none;
  }

  return connectionStatus;
}
