
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/port_event/port_event_models.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'dart:async';

void processAllQueues(Store<AppState> store) async {
  processCargoMovementQueue(store);
  processPortCargoUpdateQueue(store);
  processRoadMovementQueue(store);
  processPortEventQueue(store);
}

void processCargoMovementQueue(Store store) async {
  List<CargoCreateQueue>? selectedCargoCreateQueue =
      store.state.cargoMovementState.cargoCreateQueue;
  bool isProcessingQueue = store.state.cargoMovementState.processingQueue;

  //reset this!
  if (!isProcessingQueue && selectedCargoCreateQueue != null) {
    /*
     We need to clone the list, otherwise the linked state list
     will keep decreasing while the index increases. This eventually
     results in a index out of range at the halfway mark.
    */
    var processingList = List.from(selectedCargoCreateQueue);
    var processingTotal = processingList.length;

    store.dispatch(SetProcessingTotal(processingTotal));
    for (var index = 0; index < processingTotal; index++) {
      store.dispatch(SetProcessingIndex(index));
      CargoCreateQueue cargoCreateQueue = processingList[index];
      print("length of queue is${processingList.length}");
      store.dispatch(StartProcessingQueue());
      await Future.delayed(const Duration(milliseconds: 5));
        final Completer<ApiResponse> cargoItemCompleter =
            Completer<ApiResponse>();
        store.dispatch(WriteCargoMovement(cargoItemCompleter,
            cargoCreateQueue.uniqueId, cargoCreateQueue.cargoMovement));
        await cargoItemCompleter.future.then((ApiResponse apiResponse) {
          if (store.state.cargoMovementState.cargoCreateQueue.length < 1) {
            store.dispatch(UnFlagQueued());
            resetProcessingIndex(store);
            store.dispatch(StopProcessingQueue());
          }

          if (index == selectedCargoCreateQueue.length - 1) {
            resetProcessingIndex(store);
            store.dispatch(StopProcessingQueue());
          }
        });
    }
  }
}

void resetProcessingIndex(Store store)
{
  store.dispatch(SetProcessingTotal(0));
  store.dispatch(SetProcessingIndex(0));
}


void processRoadMovementQueue(Store store) async {
  List<RoadMovementCreateQueue>? selectedRoadCreateQueue =
      store.state.roadMovementState.roadMovementCreateQueue;

  bool isProcessingQueue = store.state.roadMovementState.processingQueue;

  if (!isProcessingQueue && selectedRoadCreateQueue != null) {
    var processingList = List.from(selectedRoadCreateQueue);
    var processingTotal = processingList.length;

    store.dispatch(SetProcessingTotal(processingTotal));

    for (var index = 0; index < processingTotal; index++) {
      store.dispatch(SetProcessingIndex(index));
      RoadMovementCreateQueue roadCreateQueue = processingList[index];
      print("length of queue is${processingList.length}");
      store.dispatch(StartRoadMovementProcessingQueue());
      await Future.delayed(const Duration(milliseconds: 5));
      final Completer<ApiResponse> roadItemCompleter =
      Completer<ApiResponse>();
      store.dispatch(WriteRoadMovement(roadItemCompleter,
          roadCreateQueue.uniqueId, roadCreateQueue.roadMovement));

      await roadItemCompleter.future.then((ApiResponse apiResponse) {
        if (store.state.roadMovementState.roadMovementCreateQueue!.length < 1) {
          store.dispatch(UnFlagRoadMovementQueued());
          resetProcessingIndex(store);
          store.dispatch(StopRoadMovementProcessingQueue());
        }

        if (index == selectedRoadCreateQueue.length - 1) {
          resetProcessingIndex(store);
          store.dispatch(StopRoadMovementProcessingQueue());
        }
      });
    }
  }
}

void processPortEventQueue(Store store) async {
  List<PortEventCreateQueue>? selectedPortEventCreateQueue =
      store.state.portEventState.portEventCreateQueue;
  bool isProcessingQueue = store.state.portEventState.processingQueue;

  if (!isProcessingQueue && selectedPortEventCreateQueue != null) {
    var processingList = List.from(selectedPortEventCreateQueue);
    var processingTotal = processingList.length;

    store.dispatch(SetProcessingTotal(processingTotal));

    for (var index = 0; index < processingTotal; index++) {
      store.dispatch(SetProcessingIndex(index));
      PortEventCreateQueue portCreateQueue = processingList[index];
      store.dispatch(StartPortEventProcessingQueue());
      await Future.delayed(const Duration(milliseconds: 5));
      final Completer<ApiResponse> portItemCompleter =
      Completer<ApiResponse>();
      store.dispatch(WritePortEvent(portItemCompleter,
          portCreateQueue.uniqueId, portCreateQueue.portEventCreate));

      await portItemCompleter.future.then((ApiResponse apiResponse) {
        if (store.state.portEventState.portEventCreateQueue!.length < 1) {
          store.dispatch(UnFlagPortEventQueued());
          resetProcessingIndex(store);
          store.dispatch(StopPortEventProcessingQueue());
        }

        if (index == selectedPortEventCreateQueue.length - 1) {
          resetProcessingIndex(store);
          store.dispatch(StopPortEventProcessingQueue());
        }
      });
    }
  }
}

void processPortCargoUpdateQueue(Store store) async {
  List<PortCargoUpdateQueue>? selectedPortCargoUpdateQueue =
      store.state.portCargoUpdateState.portCargoUpdateQueue;
  bool isProcessingQueue = store.state.portCargoUpdateState.processingQueue;

  if (!isProcessingQueue &&
      selectedPortCargoUpdateQueue != null &&
      selectedPortCargoUpdateQueue.isNotEmpty) {
    var processingList = List.from(selectedPortCargoUpdateQueue);
    var processingTotal = processingList.length;

    store.dispatch(SetProcessingTotal(processingTotal));

    for (var index = 0; index < processingTotal; index++) {
      store.dispatch(SetProcessingIndex(index));
      PortCargoUpdateQueue portCargoUpdateQueue = processingList[index];
      store.dispatch(StartPortCargoUpdateProcessingQueue());

      await Future.delayed(const Duration(milliseconds: 5));

      final Completer<ApiResponse> portItemCompleter = Completer<ApiResponse>();
      store.dispatch(WritePortCargoUpdate(portItemCompleter, portCargoUpdateQueue.uniqueId,
          portCargoUpdateQueue.portCargoUpdateCreate));

      await portItemCompleter.future.then((ApiResponse apiResponse) {
        if (store.state.portCargoUpdateState.portCargoUpdateQueue!.isEmpty) {
          store.dispatch(UnFlagPortCargoUpdateQueued());
          resetProcessingIndex(store);
          store.dispatch(StopPortCargoUpdateProcessingQueue());
        }

        if (index == selectedPortCargoUpdateQueue.length - 1) {
          resetProcessingIndex(store);
          store.dispatch(StopPortCargoUpdateProcessingQueue());
        }
      });
    }
  }
}


