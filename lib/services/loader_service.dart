//import 'package:cadi_core_app/redux/actions/account_actions.dart';
// import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
//import 'package:cadi_core_app/redux/models/models.dart';
import 'dart:async';

void loadLogin(Store store, Completer primaryCompleter) async {
  store.dispatch(StartLoading());
  final Completer<Null> userCompleter = Completer<Null>();
  final Completer<Null> accountCompleter = Completer<Null>();
  final Completer<Null> accountPortDepotsCompleter = Completer<Null>();
  final Completer<ApiResponse> accountCarriersCompleter =
      Completer<ApiResponse>();
  final Completer<ApiResponse> accountVesselsCompleter = Completer<ApiResponse>();
  final Completer<ApiResponse> accountCranesCompleter = Completer<ApiResponse>();
  final Completer<ApiResponse> accountBerthsCompleter = Completer<ApiResponse>();
  final Completer<ApiResponse> accountContainerTypesCompleter =
      Completer<ApiResponse>();
  final Completer<ApiResponse> accountVehiclesCompleter =
      Completer<ApiResponse>();
  final Completer<ApiResponse> accountVehicleDriversCompleter =
  Completer<ApiResponse>();
  final Completer<ApiResponse> accountVehicleOperatorsCompleter =
  Completer<ApiResponse>();
  final Completer<ApiResponse> accountDocumentTypesCompleter =
      Completer<ApiResponse>();
  // final Completer<ApiResponse> vesselClassesCompleter =
  //     Completer<ApiResponse>();
  final Completer<ApiResponse> fuelTypesCompleter = Completer<ApiResponse>();
  final Completer<ApiResponse> cargoTypesCompleter = Completer<ApiResponse>();
  final Completer<ApiResponse> jobPackTypesCompleter = Completer<ApiResponse>();

  store.dispatch(GetMyUser(userCompleter));//Done
  store.dispatch(GetAccounts(accountCompleter));//Done
  store.dispatch(GetAccountPortDepots(accountPortDepotsCompleter)); //Dome
  store.dispatch(GetAccountCarriers(accountCarriersCompleter));//Done
  store.dispatch(GetAccountVessels(accountVesselsCompleter));//Done
  store.dispatch(GetAccountCranes(accountCranesCompleter));//Done
  store.dispatch(GetAccountBerths(accountBerthsCompleter));//Done
  store.dispatch(GetAccountContainerTypes(accountContainerTypesCompleter));//done

  //store.dispatch(GetVesselClasses(vesselClassesCompleter));
  store.dispatch(GetFuelTypes(fuelTypesCompleter));
  store.dispatch(GetCargoTypes(cargoTypesCompleter));//done
  store.dispatch(GetJobPackTypes(jobPackTypesCompleter)); //done
  store.dispatch(GetAccountVehicles(accountVehiclesCompleter));
  store.dispatch(GetAccountVehicleDrivers(accountVehicleDriversCompleter));
  store.dispatch(GetAccountVehicleOperators(accountVehicleOperatorsCompleter));
  store.dispatch(GetAccountDocumentTypes(accountDocumentTypesCompleter));

  Future.wait([
    userCompleter,
    accountCompleter,
    accountPortDepotsCompleter,
    accountCarriersCompleter,
    accountCranesCompleter,
    accountBerthsCompleter,
    accountContainerTypesCompleter,
    // vesselClassesCompleter,
    fuelTypesCompleter,
    cargoTypesCompleter,
    accountVehiclesCompleter,
    accountVehicleDriversCompleter,
    accountVehicleOperatorsCompleter,
    accountDocumentTypesCompleter,
    jobPackTypesCompleter
  ].map((e) => e.future)).then((_) {
    List<Account> accounts = store.state.accountState.accountList.accounts;

    Account defaultAccount = accounts.first;
    store.dispatch(SetSelectedAccount(defaultAccount));

    store.dispatch(StopLoading());
    primaryCompleter.complete(ApiResponse(success: true, result: ""));
  }).catchError((e) {
    store.dispatch(StopLoading());
    primaryCompleter
        .complete(ApiResponse(success: false, result: e.toString()));
  });
}
