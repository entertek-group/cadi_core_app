//Todo Convert methods to https://pub.dev/packages/http/example

import 'dart:async';
import 'dart:convert';
import 'package:async/async.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'package:path/path.dart';

class RestService {
  static final RestService _instance = RestService.internal();
  RestService.internal();
  factory RestService() => _instance;
  final JsonDecoder _decoder = const JsonDecoder();


  Future<dynamic> get(String route, {required Map<String, String> headers}) {
    var url = Uri.parse(route);
    return http.get(url, headers: headers).then((http.Response response) {
      final res = response;
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 403) {
        throw Exception("Error received status $statusCode");
      }
      return res; //_decoder.convert(res);
    });
  }

  Future<dynamic> getWithContent(String route,
      {required Map<String, String> content}) {
    var newRoute = route;
    content.forEach((k, v) => newRoute = '$newRoute$k=$v');
    var url = Uri.parse(newRoute);
    return http.get(url).then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 403) {
        throw Exception("Error received status $statusCode");
      }
      return _decoder.convert(res);
    });
  }

  Future<dynamic> post(String route,
      {required Map<String, String> headers, body, encoding}) {
    var url = Uri.parse(route);
    return http
        .post(url, body: body, headers: headers, encoding: encoding)
        .then((http.Response response) {
      final res = response;
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 422) {
        throw Exception("Error received status $statusCode");
      }
      return res;
    });
  }

  Future<dynamic> put(String route,
      {required Map<String, String> headers, body, encoding}) {
    var url = Uri.parse(route);
    return http
        .put(url, body: body, headers: headers, encoding: encoding)
        .then((http.Response response) {
      final res = response;
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 422) {
        throw Exception("Error received status $statusCode");
      }
      return res;
    });
  }

  Future<dynamic> upload(String route, File file,String fileField,
      {required Map<String, String> headers}) async {
    var url = Uri.parse(route);
    var request = http.MultipartRequest("PUT", url);
    final length = await file.length();
    var stream = http.ByteStream(DelegatingStream.typed(file.openRead()));
    var multipartFile = http.MultipartFile(fileField, stream, length,
        filename: basename(file.path));
    request.files.add(multipartFile);
    request.headers.addAll(headers);
    var response = await request.send();
    return http.Response.fromStream(response);
  }

  Future<dynamic> uploadBody(String route, File file, String fileField,
      {required Map<String, String> headers}) async 
      {
    var url = Uri.parse(route);
    var request = http.MultipartRequest("PUT", url);
    final length = await file.length();
    var stream = http.ByteStream(DelegatingStream.typed(file.openRead()));
    var multipartFile = http.MultipartFile(fileField, stream, length,
        filename: basename(file.path));
    request.files.add(multipartFile);
    request.headers.addAll(headers);
    var response = await request.send();
    return http.Response.fromStream(response);
  }

}
