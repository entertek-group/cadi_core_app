import 'package:cadi_core_app/helpers/constants.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class BrandingColors {
  static const Color red = Color.fromRGBO(237, 29, 46, 1);
  static const Color blue = Color.fromRGBO(19, 72, 130, 1);
  static const Color lightBlue = Color.fromRGBO(66, 200, 245, 1);
  static const Color grey = Color.fromRGBO(167, 169, 172, 1);
  static const Color black = Colors.black;
  static const Color white = Colors.white;
  // static const Color green = Color.fromRGBO(107, 194, 54, 1);
  static const Color background = cadiWhite;
  static const Color cadiGreen = Color.fromRGBO(119, 221, 119, 1);
  static const Color cadiLightGreen = Color.fromRGBO(145, 209, 144, 1);
  static const Color cadiBlue = Color.fromRGBO(84, 194, 204, 1);
  static const Color cadiLightBlue = Color.fromRGBO(133, 195, 203, 1);
  static const Color cadiGrey = Color.fromRGBO(199, 207, 202, 1);
  static const Color cadiBlack = Color.fromRGBO(59, 59, 59, 1);
  static const Color cadiWhite = Color.fromRGBO(246, 246, 246, 1);
  static const Color cadiWhiteOpacity = Color.fromRGBO(246, 246, 246, 0.8);
  static const Color cadiBlackOpacity = Color.fromRGBO(59, 59, 59, 0.8);
}

class UniversalBranding {
  final String activeService;

  UniversalBranding(this.activeService);

  Color getPrimaryColor() {
    switch (activeService) {
      case CadiAppServices.portMovements:
        return BrandingColors.cadiGreen;
      case CadiAppServices.portEvents:
        return BrandingColors.cadiBlue;
      case CadiAppServices.roadMovements:
        return BrandingColors.cadiLightBlue;
      case CadiAppServices.roadEvents:
        return BrandingColors.cadiLightGreen;
      default:
        return BrandingColors.cadiGreen;
    }
  }

  Color getButtonPrimaryColor() {
    switch (activeService) {
      case CadiAppServices.portMovements:
        return BrandingColors.cadiGreen;
      case CadiAppServices.portEvents:
        return BrandingColors.cadiBlue;

      case CadiAppServices.roadMovements:
        return BrandingColors.cadiLightBlue;
      case CadiAppServices.roadEvents:
        return BrandingColors.cadiLightGreen;
      default:
        return BrandingColors.cadiGreen;
    }
  }

  Color getHoverColor() {
    switch (activeService) {
      case CadiAppServices.portMovements:
        return BrandingColors.cadiLightGreen;
      case CadiAppServices.portEvents:
        return BrandingColors.cadiLightBlue;

      case CadiAppServices.roadMovements:
        return BrandingColors.cadiBlue;
      case CadiAppServices.roadEvents:
        return BrandingColors.cadiGreen;
      default:
        return BrandingColors.cadiGreen;
    }
  }
}

class BrandingImages {
  static const logoWhite = "images/cadi_logo_white.png";
  static const logoBlack = "images/cadi_logo_black.png";
  //#Backgroundr
  static const backgroundLogin = "images/login_background.png";
  //#Cargo
  static const cargoCheck = "images/green_check_circle.png";
  //#Placeholder
  static const placeholderProfile = "images/user_profile.png";
  //Custom Icons
  static const shortShip = "images/shortship.png";
  static const shortShipDark = "images/shortship_dark.png";
  static const shortLand = "images/shortland.png";
  static const shortLandDark = "images/shortland_dark.png";
}

class PlatformSizing {
  double inputTextFieldSize(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    return screenHeight * 0.0225;
  }

  double headingTextSize(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    return screenHeight * 0.06;
  }

  double largeTextSize(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    return screenHeight * 0.03;
  }

  double mediumTextSize(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    return screenHeight * 0.022;
  }

  double largerTextSize(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    return screenHeight * 0.035;
  }

  double normalTextSize(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    return screenHeight * 0.026;
  }

  double smallTextSize(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    return screenHeight * 0.019;
  }

  double superSmallTextSize(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    return screenHeight * 0.018;
  }
}

class BrandIcons {
  static const cargoMovement = FontAwesomeIcons.cartFlatbed;
  static const cargoUpdate = FontAwesomeIcons.filePen;
  static const reports = FontAwesomeIcons.fileCircleExclamation;
  static const home = FontAwesomeIcons.house;
  static const settings = FontAwesomeIcons.gear;
  static const user = FontAwesomeIcons.solidUser;
  static const help = FontAwesomeIcons.solidCircleQuestion;
  static const logout = FontAwesomeIcons.rightFromBracket;
  static const userCircle = FontAwesomeIcons.circleCheck;
  static const listClipboard = FontAwesomeIcons.listCheck;
  static const listExclamation = FontAwesomeIcons.circleExclamation;
  static const listItem = FontAwesomeIcons.listSquares;
  static const listShield = FontAwesomeIcons.shield;
  static const listLock = FontAwesomeIcons.lock;
  static const listUsers = FontAwesomeIcons.users;
  static const email = FontAwesomeIcons.envelope;
  static const mobile = FontAwesomeIcons.squarePhone;
  static const search = FontAwesomeIcons.search;
  static const edit = FontAwesomeIcons.solidPenToSquare;
  static const restow = FontAwesomeIcons.arrowsRotate;
  static const shortShip = FontAwesomeIcons.rightToBracket;
  static const shortLand = FontAwesomeIcons.rightFromBracket;
  static const calculator = FontAwesomeIcons.calculator;
  static const vessel = FontAwesomeIcons.ship;
  static const truck = FontAwesomeIcons.truck;
  static const truckFront = FontAwesomeIcons.truckFront;
  static const inTransit = FontAwesomeIcons.truckMoving;
  static const readyCollection = FontAwesomeIcons.boxesPacking;
  static const syncData = FontAwesomeIcons.rotate;
  static const status = FontAwesomeIcons.circleInfo;
}
