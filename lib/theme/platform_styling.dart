
import 'package:cadi_core_app/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

final materialThemeData = ThemeData(
    primarySwatch: Colors.green,
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: Colors.green,
          selectionHandleColor: Colors.green, 
          selectionColor: Colors.green.withOpacity(0.5),
        ),
    scaffoldBackgroundColor: Colors.white,
    // accentColor: Colors.blue,
    appBarTheme: const AppBarTheme(color: Colors.white),
    primaryColor: Colors.white,
    secondaryHeaderColor: Colors.white,
    canvasColor: Colors.white,
    fontFamily: 'Roboto',
    textTheme: const TextTheme()
        .copyWith(headlineLarge: const TextStyle(color: BrandingColors.cadiBlack)));

const cupertinoTheme = CupertinoThemeData(
    primaryColor: BrandingColors.cadiBlack,
    barBackgroundColor: Colors.white,
    scaffoldBackgroundColor: Colors.white,
    textTheme: CupertinoTextThemeData(
        textStyle:
            TextStyle(fontFamily: 'Roboto', color: BrandingColors.cadiBlack),
        primaryColor: BrandingColors.cadiBlack));
