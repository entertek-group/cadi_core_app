import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/helpers/version.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/popup_menu.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'account_settings_index_vm.dart';
import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:flutter_redux/flutter_redux.dart';

final Uri _emailLaunchUri = Uri(
    scheme: 'mailto',
    path: 'info@cadisystems.com',
    queryParameters: {'subject': 'AppHelp'});

class AccountSettingsIndexPage extends StatefulWidget {
  final AccountSettingsIndexVM viewModel;
  const AccountSettingsIndexPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<AccountSettingsIndexPage> createState() {
    return AccountSettingsIndexPageState(viewModel: viewModel);
  }
}

class AccountSettingsIndexPageState extends State<AccountSettingsIndexPage> {
  final AccountSettingsIndexVM viewModel;
  var accountSettingList = <PopMenuItem>[];
  //final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  AccountSettingsIndexPageState({
    Key? key,
    required this.viewModel,
  });

  @override
  initState() {
    super.initState();
    if (widget.viewModel.accounts != null) {
      for (final account in widget.viewModel.accounts!.accounts) {
        var a = PopMenuItem(
            account.name, () => widget.viewModel.setAccount(account));
        accountSettingList.add(a);
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var accountSettingList = <PopMenuItem>[];
    var serviceSettingList = <PopMenuItem>[];
    StoreProvider.of<AppState>(context);
    if (widget.viewModel.accounts != null) {
      for (final account in widget.viewModel.accounts!.accounts) {
        var a = PopMenuItem(account.name, () => viewModel.setAccount(account));
        accountSettingList.add(a);
      }
    }

    for (final service in widget.viewModel.account.activeServices) {
      if (getAppServiceName(service) != CadiAppServices.unavailable) {
        var a = PopMenuItem(
          getAppServiceName(service),
          () => viewModel.setService(service),
        );
        serviceSettingList.add(a);
      }
    }

    List<Widget> settingItems = <Widget>[];

    settingItems.add(Material(
        elevation: 2,
        borderRadius: const BorderRadius.only(
            topRight: Radius.circular(10), topLeft: Radius.circular(10)),
        //data-policy
        color: BrandingColors.white,
        child: ListTile(
          shape: const RoundedRectangleBorder(
              side: BorderSide(width: 0, color: BrandingColors.white),
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10), topLeft: Radius.circular(10))),
          leading: const FaIcon(
            BrandIcons.user,
            color: BrandingColors.cadiBlackOpacity,
          ),
          title: const Text('Update my Details'),
          onTap: () {
            widget.viewModel.onUpdateMyDetailsPressed(context);
          },
        )));

    settingItems.add(Material(
        elevation: 2,
        color: BrandingColors.white,
        child: ListTile(
          //faqs
          hoverColor: BrandingColors.cadiGrey,
          leading: const FaIcon(
            BrandIcons.listLock,
            color: BrandingColors.cadiBlackOpacity,
          ),
          title: const Text('Change Password'),
          onTap: () {
            widget.viewModel.onChangePasswordPressed(context);
          },
        )));

    settingItems.add(Material(
        elevation: 2,
        color: BrandingColors.white,
        child: ListTile(
          hoverColor: BrandingColors.cadiGrey,
          leading: const FaIcon(
            BrandIcons.listUsers,
            color: BrandingColors.cadiBlackOpacity,
          ),
          title: const Text('Change Account'),
          onTap: () {
            showPopupMenu(
                context, "Account", "Select an Account", accountSettingList);
          },
        )));
    settingItems.add(Material(
        elevation: 2,
        borderRadius: const BorderRadius.only(
            bottomRight: Radius.circular(10), bottomLeft: Radius.circular(10)),
        color: BrandingColors.white,
        child: ListTile(
          shape: const RoundedRectangleBorder(
              side: BorderSide(width: 0, color: BrandingColors.white),
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(10),
                  bottomLeft: Radius.circular(10))),
          hoverColor: BrandingColors.cadiGrey,
          leading: const FaIcon(
            FontAwesomeIcons.arrowRightArrowLeft,
            color: BrandingColors.cadiBlackOpacity,
          ),
          title: const Text('Change Service'),
          onTap: () {
            showPopupMenu(
                context, "Service", "Select an Service", serviceSettingList);
          },
        )));

    List<Widget> applicationManagementItems = <Widget>[];
    applicationManagementItems.add(Material(
        elevation: 2,
        borderRadius: const BorderRadius.only(
            topRight: Radius.circular(10), topLeft: Radius.circular(10)),
        color: BrandingColors.white,
        child: ListTile(
          shape: const RoundedRectangleBorder(
              side: BorderSide(width: 0, color: BrandingColors.white),
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10), topLeft: Radius.circular(10))),
          hoverColor: BrandingColors.cadiGrey,
          leading: const FaIcon(
            BrandIcons.syncData,
            color: BrandingColors.cadiBlackOpacity,
          ),
          title: const Text('Sync Data'),
          onTap: () {
            widget.viewModel.syncData(context);
          },
        )));
    applicationManagementItems.add(Material(
        elevation: 2,
        borderRadius: const BorderRadius.only(
            bottomRight: Radius.circular(10), bottomLeft: Radius.circular(10)),
        color: BrandingColors.white,
        child: ListTile(
          shape: const RoundedRectangleBorder(
              side: BorderSide(width: 0, color: BrandingColors.white),
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10), topLeft: Radius.circular(10))),
          hoverColor: BrandingColors.cadiGrey,
          leading: Switch(
            // Switch widget here
            value: widget.viewModel.settingsAutoDisplayNextDropdownOrInput,
            activeColor: BrandingColors.cadiGreen,
            onChanged: (value) {
              widget.viewModel.toggleSettingsAutoDisplayNextDropdown(value);
            },
          ),
          onTap: () {
            if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
              setState(() {
                widget.viewModel.toggleSettingsAutoDisplayNextDropdown(false);
              });
            } else {
              setState(() {
                widget.viewModel.toggleSettingsAutoDisplayNextDropdown(true);
              });
            }
          },
          title: const Text('Auto-display Next Dropdown'),
        )));

    applicationManagementItems.add(Material(
        elevation: 2,
        borderRadius: const BorderRadius.only(
            bottomRight: Radius.circular(10), bottomLeft: Radius.circular(10)),
        color: BrandingColors.white,
        child: ListTile(
          shape: const RoundedRectangleBorder(
              side: BorderSide(width: 0, color: BrandingColors.white),
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10), topLeft: Radius.circular(10))),
          hoverColor: BrandingColors.cadiGrey,
          leading: Switch(
            // Switch widget here
            value: widget.viewModel.settingsAutoSubmitPage,
            activeColor: BrandingColors.cadiGreen,
            onChanged: (value) {
              widget.viewModel.toggleSettingsAutoSubmitPage(value);
            },
          ),
          onTap: () {
            if (widget.viewModel.settingsAutoSubmitPage) {
              setState(() {
                widget.viewModel.toggleSettingsAutoSubmitPage(false);
              });
            } else {
              setState(() {
                widget.viewModel.toggleSettingsAutoSubmitPage(true);
              });
            }
          },
          title: const Text('Auto-Submit Page'),
        )));

    List<Widget> helpItems = <Widget>[];

    helpItems.add(Material(
        elevation: 2,
        borderRadius: const BorderRadius.only(
            topRight: Radius.circular(10), topLeft: Radius.circular(10)),
        color: BrandingColors.white,
        child: ListTile(
          shape: const RoundedRectangleBorder(
              side: BorderSide(width: 0, color: BrandingColors.white),
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10), topLeft: Radius.circular(10))),
          hoverColor: BrandingColors.cadiGrey,
          leading: const FaIcon(
            BrandIcons.listExclamation,
            color: BrandingColors.cadiBlackOpacity,
          ),
          title: const Text('Report a Problem'),
          onTap: () {
            _launchURL(_emailLaunchUri.toString());
            // @garrett link to email pop instead
          },
        )));

    helpItems.add(Material(
        elevation: 2,
        borderRadius: const BorderRadius.only(
            bottomRight: Radius.circular(10), bottomLeft: Radius.circular(10)),
        color: BrandingColors.white,
        child: ListTile(
          shape: const RoundedRectangleBorder(
              side: BorderSide(width: 0, color: BrandingColors.white),
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(10),
                  bottomLeft: Radius.circular(10))),
          hoverColor: BrandingColors.cadiGrey,
          leading: const FaIcon(
            BrandIcons.help,
            color: BrandingColors.cadiBlackOpacity,
          ),
          title: const Text('Help Centre'),
          onTap: () {
            widget.viewModel.onHelpCentrePressed(context);
          },
        )));

    //contact-us

    final List<SettingTile> allSettings = <SettingTile>[];
    allSettings.add(SettingTile(
        0,
        'help_support',
        ListTile(
            title: Text("Account & Settings",
                style: TextStyle(
                    fontSize: PlatformSizing().normalTextSize(context),
                    color: BrandingColors.cadiBlack)))));
    allSettings.add(SettingTile(
        1,
        '',
        Padding(
            padding: EdgeInsets.only(
                top: screenHeight * 0.02, bottom: screenHeight * 0.00),
            child: buildImage(context))));
    allSettings.add(SettingTile(
        2,
        'account_settings',
        ListTile(
            title: Text("Account Settings",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    color: BrandingColors.cadiBlack)))));

    allSettings
        .add(SettingTile(3, '', buildSettingItems(context, settingItems)));

    allSettings.add(SettingTile(
        4,
        'application_management',
        ListTile(
            title: Text("Application Management",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    color: BrandingColors.cadiBlack)))));

    allSettings.add(SettingTile(
        5, '', buildSettingItems(context, applicationManagementItems)));

    allSettings.add(SettingTile(
        6,
        'help_support',
        ListTile(
            title: Text("Help & Support",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    color: BrandingColors.cadiBlack)))));

    allSettings.add(SettingTile(7, '', buildSettingItems(context, helpItems)));

    allSettings.add(SettingTile(
      8,
      '',
      Padding(
        padding: EdgeInsets.only(top: screenHeight * 0.02, left: 40, right: 40),
        child: BaseGhostButton(
          onPressed: () {
            widget.viewModel.logOut(context);
          },
          textColor: BrandingColors.cadiBlack,
          child: Text(
            'Logout',
            style: TextStyle(
              fontSize: PlatformSizing().normalTextSize(context),
              color:
                  BrandingColors.cadiBlack, // Set your desired text color here
            ),
          ), // Pass the text color here
        ),
      ),
    ));

    allSettings.add(SettingTile(
        9,
        '',
        ListTile(
            title: Text("Version: $versionNumber",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    color: BrandingColors.cadiBlack)))));

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.only(top: 0, left: 40, right: 40),
      child: ListView.builder(
          itemCount: allSettings.length,
          padding: const EdgeInsets.only(
            left: 10.0,
            right: 10,
          ),
          // separatorBuilder: (BuildContext context, int index) =>
          //     Divider(height: 1),
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
  }
}

Widget buildCompleteImage(BuildContext context) {
  var maxHeight = MediaQuery.of(context).size.height * 0.1;

  return Container(
      alignment: Alignment.center,
      child: FaIcon(
        BrandIcons.help,
        size: maxHeight,
        color: BrandingColors.cadiGrey,
      ));
}

Widget buildSettingItems(BuildContext context, List<Widget> items) {
  return ListView.separated(
      shrinkWrap: true,
      padding: const EdgeInsets.only(top: 0, bottom: 20, left: 10, right: 10),
      //.separated(
      itemCount: items.length,
      separatorBuilder: (BuildContext context, int index) =>
          const Divider(height: 0.3),
      itemBuilder: (BuildContext context, int index) {
        return items[index];
      });
}

Widget buildImage(BuildContext context) {
  var maxHeight = MediaQuery.of(context).size.height * 0.1;

  return Container(
      padding: const EdgeInsets.only(bottom: 25),
      alignment: Alignment.center,
      child: FaIcon(
        BrandIcons.settings,
        size: maxHeight,
        color: BrandingColors.cadiGrey,
      ));
}

void _launchURL(String url) async {
  if (!await launch(url)) throw 'Could not launch $url';
}
