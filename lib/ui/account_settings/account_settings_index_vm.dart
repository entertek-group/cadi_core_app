import 'dart:async';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/services/data_processing_service.dart';
import 'package:cadi_core_app/services/loader_service.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'account_settings_index_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class AccountSettingsIndexScreenBuilder extends StatelessWidget {
  const AccountSettingsIndexScreenBuilder({super.key});

  static const String route = route_helper.accountSettingsIndexScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: BrandingColors.cadiWhite,
      body: StoreConnector<AppState, AccountSettingsIndexVM>(
        converter: AccountSettingsIndexVM.fromStore,
        builder: (context, vm) {
          return AccountSettingsIndexPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class AccountSettingsIndexVM {
  bool isLoading;
  bool settingsAutoDisplayNextDropdownOrInput;
  bool settingsAutoSubmitPage;
  AuthState authState;
  Account account;
  AccountList? accounts;
  final Function(BuildContext, String) onNavigatePressed;
  final Function(BuildContext) onChangePasswordPressed;
  final Function(BuildContext) onUpdateMyDetailsPressed;
  final Function(BuildContext) onHelpCentrePressed;
  final Function(String token) setToken;
  final Function(bool toggleValue) toggleSettingsAutoDisplayNextDropdown;
  final Function(bool toggleValue) toggleSettingsAutoSubmitPage;
  final Function(Account account) setAccount;
  final Function(String activeService) setService;
  final Function(BuildContext context) logOut;
  final Function(BuildContext context) syncData;

  AccountSettingsIndexVM(
      {required this.isLoading,
      required this.authState,
      required this.account,
      required this.settingsAutoDisplayNextDropdownOrInput,
      required this.settingsAutoSubmitPage,
      required this.onNavigatePressed,
      required this.setToken,
      required this.toggleSettingsAutoDisplayNextDropdown,
      required this.toggleSettingsAutoSubmitPage,
      required this.accounts,
      required this.setAccount,
      required this.logOut,
      required this.setService,
      required this.onChangePasswordPressed,
      required this.onUpdateMyDetailsPressed,
      required this.syncData,
      required this.onHelpCentrePressed});

  static AccountSettingsIndexVM fromStore(Store<AppState> store) {
    return AccountSettingsIndexVM(
      isLoading: store.state.isLoading,
      settingsAutoDisplayNextDropdownOrInput:
          store.state.uiState.uiDropDownOrInputAutoContinue,
      settingsAutoSubmitPage: store.state.uiState.uiAutoSubmitPage,
      authState: store.state.authState,
      account: store.state.accountState.account,
      accounts: store.state.accountState.accountList,
      setAccount: (Account account) {
        store.dispatch(SetHomeIndex(0));
        store.dispatch(SetSelectedAccount(account));
      },
      setService: (String activeService) {
        store.dispatch(SetHomeIndex(0));
        store.dispatch(SetActiveService(activeService));
      },
      onNavigatePressed: (BuildContext context, String route) {
        if (store.state.isLoading) {
          return;
        }
        navigation_helper.navigateRoute(route, store, null);
      },
      setToken: (String token) {},
      toggleSettingsAutoDisplayNextDropdown: (bool toggleValue) {
        if (toggleValue) {
          store.dispatch(UiDropDownAutoContinueOn());
        } else {
          store.dispatch(UiDropDownAutoContinueOff());
        }
      },
      toggleSettingsAutoSubmitPage: (bool toggleValue) {
        if (toggleValue) {
          store.dispatch(UiAutoSubmitPageOn());
        } else {
          store.dispatch(UiAutoSubmitPageOff());
        }
      },
      logOut: (context) {
        if (store.state.cargoMovementState.queued) {
          showOkMessage(context, "WARNING!", "You have pending Port Movements. Please select 'Sync Now' on the home page before logging off..");
          processCargoMovementQueue(store);
        } else if (store.state.roadMovementState.queued) {
          processRoadMovementQueue(store);
          showOkMessage(context, "WARNING!", "You have pending Road Movements. Please select 'Sync Now' on the home page before logging off..");
        } else if (store.state.portEventState.queued) {
          processPortEventQueue(store);
          showOkMessage(context, "WARNING!", "You have pending Port Events. Please select 'Sync Now' on the home page before logging off..");
        } else if (store.state.portCargoUpdateState.queued) {
          processPortCargoUpdateQueue(store);
          showOkMessage(context, "WARNING!", "You have pending Cargo Updates. Please select 'Sync Now' on the home page before logging off..");
        } else {
          showMessageWithActions(
              context,
              "Logout Confirmation",
              "Are you sure you would like to logout?",
                  () {
                Navigator.pop(context);
                navigation_helper.navigateRoute(
                    route_helper.logoutScreen, store, null);
              },
              "Yes",
                  () {
                Navigator.pop(context);
              },
              "No");
        }
      },
      onChangePasswordPressed: (BuildContext context) {
        store.dispatch(
            NavigateToAction.push(route_helper.changePasswordScreenBuilder));
      },
      onUpdateMyDetailsPressed: (BuildContext context) {
        store.dispatch(
            NavigateToAction.push(route_helper.userProfileIndexScreenBuilder));
      },
      onHelpCentrePressed: (BuildContext context) {
        store.dispatch(
            NavigateToAction.push(route_helper.helpSupportIndexScreenBuilder));
      },
      syncData: (BuildContext context) {
        final Completer<ApiResponse> completer = Completer<ApiResponse>();
        loadLogin(store, completer);

        completer.future.then((ApiResponse apiResponse) {
          if (apiResponse.success) {
            store.dispatch(SetHomeIndex(0));
          } else {
            showMessageWithActions(context, "An error occurred",
                "Error: ${apiResponse.result}. Please login again.", () {
              Navigator.pop(context);
              navigation_helper.navigateRoute(
                  route_helper.logoutScreen, store, null);
            }, "Ok", null, "");
          }
        });
      },
    );
  }
}
