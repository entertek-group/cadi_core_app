import 'package:cadi_core_app/helpers/helpers.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/ui/shared/components/base/base_button.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/popup_menu.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'change_password_vm.dart';
import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

class ChangePasswordPage extends StatefulWidget {
  final ChangePasswordVM viewModel;
  const ChangePasswordPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<ChangePasswordPage> createState() {
    return ChangePasswordPageState(viewModel: viewModel);
  }
}

class ChangePasswordPageState extends State<ChangePasswordPage> {
  final ChangePasswordVM viewModel;
  final _oldPasswordController = TextEditingController();
  final _newPasswordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();

  static const Key _oldPasswordKey = Key(ChangePasswordKeys.oldPasswordKey);
  static const Key _newPasswordKey = Key(ChangePasswordKeys.newPasswordKey);
  static const Key _confirmPasswordKey =
      Key(ChangePasswordKeys.confirmPasswordKey);
  //final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  ChangePasswordPageState({
    Key? key,
    required this.viewModel,
  });

  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _oldPasswordController.dispose();
    _newPasswordController.dispose();
    _confirmPasswordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var accountSettingList = <PopMenuItem>[];
    if (widget.viewModel.accounts != null) {
      for (final account in widget.viewModel.accounts!.accounts) {
        var a = PopMenuItem(account.name, () => viewModel.setAccount(account));
        accountSettingList.add(a);
      }
    }

    List<Widget> settingItems = <Widget>[];

    settingItems.add(Padding(
        //data-policy
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: PlatformTextField(
          // #todo onfocus change border to CadiBlack
          cursorColor: BrandingColors.cadiGrey,
          controller: _oldPasswordController,
          key: _oldPasswordKey,
          autocorrect: false,
          material: (_, __) => MaterialAssetsDark()
              .textFieldData('Current Password', FontAwesomeIcons.lock),
          cupertino: (_, __) => CupertinoAssets()
              .textFieldData('Current Password', FontAwesomeIcons.lock),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
          obscureText: true,
        )));
    settingItems.add(Padding(
        //data-policy
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: PlatformTextField(
          // #todo onfocus change border to CadiBlack
          cursorColor: BrandingColors.cadiGrey,
          controller: _newPasswordController,
          key: _newPasswordKey,
          autocorrect: false,
          material: (_, __) => MaterialAssetsDark()
              .textFieldData('New Password', FontAwesomeIcons.lock),
          cupertino: (_, __) => CupertinoAssets()
              .textFieldData('New Password', FontAwesomeIcons.lock),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
          obscureText: true,
        )));

    settingItems.add(Padding(
        //data-policy
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: PlatformTextField(
          // #todo onfocus change border to CadiBlack
          cursorColor: BrandingColors.cadiGrey,
          controller: _confirmPasswordController,
          key: _confirmPasswordKey,
          autocorrect: false,
          material: (_, __) => MaterialAssetsDark()
              .textFieldData('Confirm Password', FontAwesomeIcons.lock),
          cupertino: (_, __) => CupertinoAssets()
              .textFieldData('Confirm Password', FontAwesomeIcons.lock),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
          obscureText: true,
        )));

    //contact-us

    final List<SettingTile> allSettings = <SettingTile>[];
    allSettings.add(SettingTile(
        0,
        'help_support',
        Material(
            color: BrandingColors.cadiWhite,
            child: ListTile(
                title: Text("Account & Settings",
                    style: TextStyle(
                        fontSize: PlatformSizing().normalTextSize(context),
                        color: BrandingColors.cadiBlack))))

        // ListTile(
        //   title: Text('Account: ' + widget.viewModel.account.name),
        // )

        ));
    allSettings.add(SettingTile(
        1,
        'help_centre',
        Material(
            color: BrandingColors.cadiWhite,
            child: ListTile(
                title: Text("Change Password",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontSize: PlatformSizing().smallTextSize(context),
                        color: BrandingColors.cadiBlack)))

            // ListTile(
            //   title: Text('Account: ' + widget.viewModel.account.name),
            // )

            )));
    allSettings.add(SettingTile(
        2,
        '',
        Padding(
            padding: EdgeInsets.only(
                top: screenHeight * 0.02, bottom: screenHeight * 0.01),
            child: buildCompleteImage(context))));

    allSettings.add(SettingTile(
        5,
        '',
        Column(
          children: [
            Text("Reset your Password",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: PlatformSizing().normalTextSize(context),
                    color: BrandingColors.cadiBlack)),
            GestureDetector(
                onTap: () {
                  _launchURL(urlContactUs);
                },
                child: Text("Enter a new password",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack)))
          ],
        )));

    allSettings
        .add(SettingTile(4, '', buildSettingItems(context, settingItems)));

    allSettings.add(SettingTile(
        5,
        '',
        Padding(
            padding: EdgeInsets.only(top: screenHeight * 0.02),
            child: Column(
              children: [
                Text("Need help?",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: PlatformSizing().smallTextSize(context),
                        fontStyle: FontStyle.italic,
                        color: BrandingColors.cadiBlack)),
                GestureDetector(
                    onTap: () {
                      _launchURL(urlContactUs);
                    },
                    child: Text("Contact Us",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            decoration: TextDecoration.underline,
                            fontSize: PlatformSizing().smallTextSize(context),
                            color: BrandingColors.cadiBlack)))
              ],
            ))));

    allSettings.add(SettingTile(
        5,
        '',
        Padding(
            padding: EdgeInsets.only(top: screenHeight * 0.02),
            child: BaseButton(
                color:
                    widget.viewModel.universalBranding.getButtonPrimaryColor(),
                child: Text(
                  'Save Changes',
                  style: TextStyle(
                      fontSize: PlatformSizing().normalTextSize(context)),
                ),
                onPressed: () {
                  if (widget.viewModel.isSubmitting) {
                    showOkMessage(
                        // FontAwesomeIcons.warning,
                        context,
                        "Update Profile Error",
                        "Request is already in progress");
                  } else if (_oldPasswordController.text.isEmpty) {
                    showOkMessage(
                        // FontAwesomeIcons.warning,
                        context,
                        "Update Password Error",
                        "Please enter your current password.");
                  } else if (_newPasswordController.text.isEmpty) {
                    showOkMessage(
                        // FontAwesomeIcons.warning,
                        context,
                        "Update Password Error",
                        "Please enter your new password.");
                  } else if (_confirmPasswordController.text.isEmpty) {
                    showOkMessage(
                        // FontAwesomeIcons.warning,
                        context,
                        "Update Password Error",
                        "Please confirm your new password");
                  }
                  // else if (_newPasswordController.text.length > 5) {
                  //   showOkMessage(
                  //       // FontAwesomeIcons.server,
                  //       context,
                  //       "Update Password Error",
                  //       "Your new password needs to be at least 6 characters.");
                  // }
                  else if (_confirmPasswordController.text !=
                      _newPasswordController.text) {
                    showOkMessage(
                        // FontAwesomeIcons.warning,
                        context,
                        "Update Password Error",
                        "New Password and Password Confirmation do not match.");
                  } else {
                    viewModel.onUpdateUserPasswordPressed(
                        context,
                        _oldPasswordController.text,
                        _newPasswordController.text,
                        _confirmPasswordController.text);
                  }
                }))));

    if (widget.viewModel.isLoading) {
      return const Center(
        child: Text("Loading..."),
      );
    }

    return Padding(
      padding: const EdgeInsets.only(top: 0, left: 40, right: 40),
      child: ListView.builder(
          itemCount: allSettings.length,
          padding: const EdgeInsets.only(
            left: 10.0,
            right: 10,
          ),
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
  }
}

Widget buildCompleteImage(BuildContext context) {
  var maxHeight = MediaQuery.of(context).size.height * 0.1;

  return Container(
      alignment: Alignment.center,
      child: FaIcon(
        BrandIcons.listLock,
        size: maxHeight,
        color: BrandingColors.cadiGrey,
      ));
}

Widget buildSettingItems(BuildContext context, List<Widget> items) {
  return ListView.separated(
      shrinkWrap: true,
      padding: const EdgeInsets.only(top: 30, bottom: 20),
      //.separated(
      itemCount: items.length,
      separatorBuilder: (BuildContext context, int index) => const Divider(
            height: 10,
            color: BrandingColors.background,
          ),
      itemBuilder: (BuildContext context, int index) {
        return items[index];
      });
}

void _launchURL(String url) async {
  if (!await launch(url)) throw 'Could not launch $url';
}
