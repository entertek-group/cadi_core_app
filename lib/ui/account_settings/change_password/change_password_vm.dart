import 'dart:async';

import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:redux/redux.dart';
import 'change_password_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class ChangePasswordScreenBuilder extends StatelessWidget {
  const ChangePasswordScreenBuilder({super.key});

  static const String route = route_helper.changePasswordScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(context, "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, ChangePasswordVM>(
        converter: ChangePasswordVM.fromStore,
        builder: (context, vm) {
          return ChangePasswordPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class ChangePasswordVM {
  bool isLoading;
  bool isSubmitting;
  AuthState authState;
  Account account;
  AccountList? accounts;
  UniversalBranding universalBranding;
  final Function(BuildContext context, String currentPassword, String password,
      String confirmPassword) onUpdateUserPasswordPressed;
  final Function(BuildContext, String) onNavigatePressed;
  final Function(String token) setToken;
  final Function(Account account) setAccount;
  final Function(BuildContext context) logOut;
  ChangePasswordVM(
      {required this.isLoading,
      required this.isSubmitting,
      required this.authState,
      required this.account,
      required this.universalBranding,
      required this.onUpdateUserPasswordPressed,
      required this.onNavigatePressed,
      required this.setToken,
      required this.accounts,
      required this.setAccount,
      required this.logOut});

  static ChangePasswordVM fromStore(Store<AppState> store) {
    return ChangePasswordVM(
      isLoading: store.state.isLoading,
      isSubmitting: store.state.isSubmitting,
      authState: store.state.authState,
      account: store.state.accountState.account,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      accounts: store.state.accountState.accountList,
      setAccount: (Account account) {
        store.dispatch(SetSelectedAccount(account));
      },
      onUpdateUserPasswordPressed: (BuildContext context,
          String currentPassword, String password, String confirmPassword) {
        final Completer<Null> completer = Completer<Null>();

        store.dispatch(UpdateUserPassword(
            completer, currentPassword, password, confirmPassword));
        completer.future.then((_) {
          var error = store.state.authState.error;
          if (error.length > 1) {
            showOkMessage(
                // FontAwesomeIcons.warning,
                context,
                "Update password error",
                error);
          } else {
            showOkMessage(
                // FontAwesomeIcons.warning,
                context,
                "Update password success",
                "User password has been updated.");
          }
        });
      },
      onNavigatePressed: (BuildContext context, String route) {
        if (store.state.isLoading) {
          return;
        }
        navigation_helper.navigateRoute(route, store, null);
      },
      setToken: (String token) {},
      logOut: (context) {
        navigation_helper.navigateRoute(
            route_helper.logoutScreen, store, null);
      },
    );
  }
}
