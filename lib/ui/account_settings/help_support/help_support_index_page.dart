import 'package:cadi_core_app/helpers/helpers.dart';
import 'package:cadi_core_app/ui/shared/components/popup_menu.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'help_support_index_vm.dart';
import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:url_launcher/url_launcher.dart';

final Uri _emailLaunchUri = Uri(
    scheme: 'mailto',
    path: 'info@cadisystems.com',
    queryParameters: {'subject': "AppHelp"});

class HelpSupportIndexPage extends StatefulWidget {
  final HelpSupportIndexVM viewModel;
  const HelpSupportIndexPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<HelpSupportIndexPage> createState() {
    return HelpSupportIndexPageState(viewModel: viewModel);
  }
}

class HelpSupportIndexPageState extends State<HelpSupportIndexPage> {
  final HelpSupportIndexVM viewModel;
  //final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  HelpSupportIndexPageState({
    Key? key,
    required this.viewModel,
  });

  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var accountSettingList = <PopMenuItem>[];
    if (widget.viewModel.accounts != null) {
      for (final account in widget.viewModel.accounts!.accounts) {
        var a = PopMenuItem(account.name, () => viewModel.setAccount(account));
        accountSettingList.add(a);
      }
    }

    List<Widget> settingItems = <Widget>[];

    settingItems.add(Material(
        elevation: 2,
        borderRadius: const BorderRadius.only(
            topRight: Radius.circular(10), topLeft: Radius.circular(10)),
        //data-policy
        color: BrandingColors.white,
        child: ListTile(
          shape: const RoundedRectangleBorder(
              side: BorderSide(width: 0, color: BrandingColors.white),
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10), topLeft: Radius.circular(10))),
          hoverColor: BrandingColors.cadiGrey,
          leading: const FaIcon(
            FontAwesomeIcons.circleInfo,
            color: BrandingColors.cadiBlackOpacity,
          ),
          title: const Text('Data Policy'),
          onTap: () {
            _launchURL(urlDataPolicy);
          },
        )));

    // settingItems.add(Material(
    //     elevation: 2,
    //     color: BrandingColors.white,
    //     child: ListTile(
    //       //faqs
    //       hoverColor: BrandingColors.cadiGrey,
    //       leading: const FaIcon(
    //         FontAwesomeIcons.solidCircleQuestion,
    //         color: BrandingColors.cadiBlackOpacity,
    //       ),
    //       title: const Text('FAQs'),
    //       onTap: () {
    //         _launchURL(urlFaqs);
    //       },
    //     )));

    settingItems.add(Material(
        elevation: 2,
        color: BrandingColors.white,
        child: ListTile(
          hoverColor: BrandingColors.cadiGrey,
          leading: const FaIcon(
            FontAwesomeIcons.fileCircleCheck,
            color: BrandingColors.cadiBlackOpacity,
          ),
          title: const Text('Terms of Use'),
          onTap: () {
            _launchURL(urlTermsOfUse);
          },
        )));

    settingItems.add(Material(
        elevation: 2,
        borderRadius: const BorderRadius.only(
            bottomRight: Radius.circular(10), bottomLeft: Radius.circular(10)),
        color: BrandingColors.white,
        child: ListTile(
          shape: const RoundedRectangleBorder(
              side: BorderSide(width: 0, color: BrandingColors.white),
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(10),
                  bottomLeft: Radius.circular(10))),
          hoverColor: BrandingColors.cadiGrey,
          leading: const FaIcon(
            FontAwesomeIcons.fileCircleExclamation,
            color: BrandingColors.cadiBlackOpacity,
          ),
          title: const Text('Privacy Statement'),
          onTap: () {
            _launchURL(urlPrivacyStatement);
          },
        )));

    final List<SettingTile> allSettings = <SettingTile>[];
    allSettings.add(SettingTile(
        0,
        'help_support',
        Material(
            color: BrandingColors.cadiWhite,
            child: ListTile(
                title: Text("Help & Support",
                    style: TextStyle(
                        fontSize: PlatformSizing().normalTextSize(context),
                        color: BrandingColors.cadiBlack))))));
    allSettings.add(SettingTile(
        1,
        'help_centre',
        Material(
            color: BrandingColors.cadiWhite,
            child: ListTile(
                title: Text("Help Centre",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontSize: PlatformSizing().smallTextSize(context),
                        color: BrandingColors.cadiBlack))))));
    allSettings.add(SettingTile(
        2,
        '',
        Padding(
            padding: EdgeInsets.only(
                top: screenHeight * 0.02, bottom: screenHeight * 0.01),
            child: buildCompleteImage(context))));

    allSettings
        .add(SettingTile(4, '', buildSettingItems(context, settingItems)));

    allSettings.add(SettingTile(
        5,
        '',
        Padding(
            padding: EdgeInsets.only(top: screenHeight * 0.02),
            child: Column(
              children: [
                Text("Need further help?",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: PlatformSizing().smallTextSize(context),
                        fontStyle: FontStyle.italic,
                        color: BrandingColors.cadiBlack)),
                GestureDetector(
                    onTap: () {
                      _launchURL(urlContactUs);
                    },
                    child: Text("Contact Us",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            decoration: TextDecoration.underline,
                            fontSize: PlatformSizing().smallTextSize(context),
                            color: BrandingColors.cadiBlack)))
              ],
            ))));
    allSettings.add(SettingTile(
        6,
        '',
        Padding(
            padding: EdgeInsets.only(top: screenHeight * 0.03),
            child: Column(
              children: [
                Text("Or",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: PlatformSizing().smallTextSize(context),
                        fontStyle: FontStyle.italic,
                        color: BrandingColors.cadiBlack)),
                GestureDetector(
                    onTap: () {
                      _launchURL(_emailLaunchUri.toString());
                    },
                    child: Text("Report Problem",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            decoration: TextDecoration.underline,
                            fontSize: PlatformSizing().smallTextSize(context),
                            color: BrandingColors.cadiBlack)))
              ],
            ))));
    if (widget.viewModel.isLoading) {
      return const Center(
        child: Text("Loading..."),
      );
    }

    return Padding(
      padding: const EdgeInsets.only(top: 0, left: 40, right: 40),
      child: ListView.builder(
          itemCount: allSettings.length,
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
  }
}

Widget buildCompleteImage(BuildContext context) {
  var maxHeight = MediaQuery.of(context).size.height * 0.1;

  return Container(
      padding: const EdgeInsets.only(bottom: 25),
      alignment: Alignment.center,
      child: FaIcon(
        BrandIcons.help,
        size: maxHeight,
        color: BrandingColors.cadiGrey,
      ));
}

Widget buildSettingItems(BuildContext context, List<Widget> items) {
  return ListView.separated(
      shrinkWrap: true,
      padding: const EdgeInsets.all(10),
      itemCount: items.length,
      separatorBuilder: (BuildContext context, int index) =>
          const Divider(height: 0.3),
      itemBuilder: (BuildContext context, int index) {
        return items[index];
      });
}

void _launchURL(String url) async {
  if (!await launch(url)) throw 'Could not launch $url';
}
