import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_home_button.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_user_account.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:redux/redux.dart';
import 'help_support_index_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class HelpSupportIndexScreenBuilder extends StatelessWidget {
  const HelpSupportIndexScreenBuilder({super.key});

  static const String route = route_helper.helpSupportIndexScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(context, "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, HelpSupportIndexVM>(
        converter: HelpSupportIndexVM.fromStore,
        builder: (context, vm) {
          return HelpSupportIndexPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class HelpSupportIndexVM {
  bool isLoading;
  AuthState authState;
  Account account;
  AccountList? accounts;
  final Function(BuildContext, String) onNavigatePressed;
  final Function(String token) setToken;
  final Function(Account account) setAccount;
  final Function(BuildContext context) logOut;
  HelpSupportIndexVM(
      {required this.isLoading,
      required this.authState,
      required this.account,
      required this.onNavigatePressed,
      required this.setToken,
      required this.accounts,
      required this.setAccount,
      required this.logOut});

  static HelpSupportIndexVM fromStore(Store<AppState> store) {
    return HelpSupportIndexVM(
      isLoading: store.state.isLoading,
      authState: store.state.authState,
      account: store.state.accountState.account,
      accounts: store.state.accountState.accountList,
      setAccount: (Account account) {
        store.dispatch(SetSelectedAccount(account));
      },
      onNavigatePressed: (BuildContext context, String route) {
        if (store.state.isLoading) {
          return;
        }
        navigation_helper.navigateRoute(route, store, null);
      },
      setToken: (String token) {},
      logOut: (context) {
        navigation_helper.navigateRoute(
            route_helper.logoutScreen, store, null);
      },
    );
  }
}
