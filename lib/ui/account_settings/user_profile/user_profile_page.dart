import 'dart:convert';

import 'package:cadi_core_app/helpers/helpers.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/ui/shared/components/base/base_button.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/popup_menu.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'user_profile_vm.dart';
import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';

class UserProfilePage extends StatefulWidget {
  final UserProfileVM viewModel;
  const UserProfilePage({
    super.key,
    required this.viewModel,
  });

  @override
  State<UserProfilePage> createState() {
    return UserProfilePageState(viewModel: viewModel);
  }
}

class UserProfilePageState extends State<UserProfilePage> {
  final UserProfileVM viewModel;
  final _nameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _emailController = TextEditingController();
  final _mobileController = TextEditingController();

  static const Key _nameKey = Key(UserProfileKeys.nameKey);
  static const Key _lastNameKey = Key("${UserProfileKeys.nameKey}last");
  static const Key _emailKey = Key(UserProfileKeys.emailKey);
  static const Key _mobileKey = Key(UserProfileKeys.mobileKey);

  UserProfilePageState({
    Key? key,
    required this.viewModel,
  });

  @override
  initState() {
    super.initState();
    _nameController.text = widget.viewModel.authState.user!.firstName;
    _lastNameController.text = widget.viewModel.authState.user!.lastName;
    _emailController.text = widget.viewModel.authState.user!.email;
    _mobileController.text = widget.viewModel.authState.user!.mobileNumber;
  }

  @override
  void dispose() {
    super.dispose();
    _nameController.dispose();
    _lastNameController.dispose();
    _emailController.dispose();
    _mobileController.dispose();
  }

  void setImage(XFile? image) {
    if (image != null) {
      final bytes = File(image.path).readAsBytesSync();
      String imageName = image.name;
      String img64 = base64Encode(bytes);
      widget.viewModel.updateAvatar(context, img64, imageName);
    }
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var accountSettingList = <PopMenuItem>[];
    if (widget.viewModel.accounts != null) {
      for (final account in widget.viewModel.accounts!.accounts) {
        var a = PopMenuItem(account.name, () => viewModel.setAccount(account));
        accountSettingList.add(a);
      }
    }

    List<Widget> settingItems = <Widget>[];

    settingItems.add(Padding(
        //Text("Full Name"),
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: PlatformTextField(
          // textCapitalization: TextCapitalization.characters,
          cursorColor: BrandingColors.cadiGrey,
          controller: _nameController,
          key: _nameKey,
          autocorrect: false,
          material: (_, __) => MaterialAssetsDark()
              .textFieldData('First Name', FontAwesomeIcons.solidUser),
          cupertino: (_, __) => CupertinoAssets()
              .textFieldData('First Name', FontAwesomeIcons.solidUser),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
        )));

    settingItems.add(Padding(
        //Text("Full Name"),
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: PlatformTextField(
          // textCapitalization: TextCapitalization.characters,
          cursorColor: BrandingColors.cadiGrey,
          controller: _lastNameController,
          key: _lastNameKey,
          autocorrect: false,
          material: (_, __) => MaterialAssetsDark()
              .textFieldData('Last Name', FontAwesomeIcons.solidUser),
          cupertino: (_, __) => CupertinoAssets()
              .textFieldData('Last Name', FontAwesomeIcons.solidUser),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
        )));

    settingItems.add(Padding(
        //Text("Email Address"),
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: PlatformTextField(
          cursorColor: BrandingColors.cadiGrey,
          textCapitalization: TextCapitalization.characters,
          controller: _emailController,
          key: _emailKey,
          autocorrect: false,
          material: (_, __) => MaterialAssetsDark().textFieldData(
            'Email',
            BrandIcons.email,
          ),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData('Email', BrandIcons.email),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.emailAddress,
        )));

    settingItems.add(Padding(
        //Text("Mobile"),
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: PlatformTextField(
          cursorColor: BrandingColors.cadiGrey,
          textCapitalization: TextCapitalization.characters,
          controller: _mobileController,
          key: _mobileKey,
          autocorrect: false,
          material: (_, __) => MaterialAssetsDark()
              .textFieldData('Mobile', FontAwesomeIcons.phone),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData('Mobile', FontAwesomeIcons.phone),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.phone,
        )));

    //contact-us

    final List<SettingTile> allSettings = <SettingTile>[];
    allSettings.add(SettingTile(
        0,
        'help_support',
        Material(
            color: BrandingColors.cadiWhite,
            child: ListTile(
                title: Text("Account & Settings",
                    style: TextStyle(
                        fontSize: PlatformSizing().normalTextSize(context),
                        color: BrandingColors.cadiBlack))))

        // ListTile(
        //   title: Text('Account: ' + widget.viewModel.account.name),
        // )

        ));
    allSettings.add(SettingTile(
        1,
        'help_centre',
        Material(
            color: BrandingColors.cadiWhite,
            child: ListTile(
                title: Text("Update my Details",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontSize: PlatformSizing().smallTextSize(context),
                        color: BrandingColors.cadiBlack))))

        // ListTile(
        //   title: Text('Account: ' + widget.viewModel.account.name),
        // )

        ));

    allSettings.add(SettingTile(
        2,
        '',
        Padding(
            padding: EdgeInsets.only(
                top: screenHeight * 0.01, bottom: screenHeight * 0.02),
            child: Material(
                color: BrandingColors.cadiWhite,
                child: InkWell(
                    onTap: () async {
                      XFile? image;
                      var imageSelectList = <PopMenuItem>[];
                      final ImagePicker picker = ImagePicker();
                      var galleryOption = PopMenuItem("Gallery", () async {
                        image =
                            await picker.pickImage(source: ImageSource.gallery);
                        setImage(image);
                      });
                      imageSelectList.add(galleryOption);
                      var cameraOption = PopMenuItem("Camera", () async {
                        image =
                            await picker.pickImage(source: ImageSource.camera);
                        setImage(image);
                      });
                      imageSelectList.add(cameraOption);

                      showPopupMenu(
                          context,
                          "Image",
                          "Where would you like to retrieve the image from?",
                          imageSelectList);
                    },
                    child: buildCompleteImage(context,
                        getImageUrl(widget.viewModel.user!.avatar)))))));

    allSettings.add(SettingTile(
        5,
        '',
        Column(
          children: [
            Text(widget.viewModel.authState.user!.name,
                textAlign: TextAlign.center,
                style: TextStyle(
                    // fontSize: PlatformSizing().smallTextSize(context),
                    fontWeight: FontWeight.bold,
                    fontSize: PlatformSizing().normalTextSize(context),
                    color: BrandingColors.cadiBlack)),
            GestureDetector(
                onTap: () {
                  _launchURL(urlContactUs);
                },
                child: Text(widget.viewModel.account.name,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        // decoration: TextDecoration.underline,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack)))
          ],
        )));

    allSettings
        .add(SettingTile(4, '', buildSettingItems(context, settingItems)));

    allSettings.add(SettingTile(
        5,
        '',
        Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Column(
              children: [
                Text("Need help?",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: PlatformSizing().smallTextSize(context),
                        fontStyle: FontStyle.italic,
                        color: BrandingColors.cadiBlack)),
                GestureDetector(
                    onTap: () {
                      _launchURL(urlContactUs);
                    },
                    child: Text("Contact Us",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            decoration: TextDecoration.underline,
                            fontSize: PlatformSizing().smallTextSize(context),
                            color: BrandingColors.cadiBlack)))
              ],
            ))));

    allSettings.add(SettingTile(
        5,
        '',
        Padding(
            padding: const EdgeInsets.only(top: 30),
            child: BaseButton(
                color:
                    widget.viewModel.universalBranding.getButtonPrimaryColor(),
                child: Text(
                  'Save Changes',
                  style: TextStyle(
                      fontSize: PlatformSizing().normalTextSize(context)),
                ),
                onPressed: () {
                  if (widget.viewModel.isSubmitting) {
                    showOkMessage(
                        // FontAwesomeIcons.warning,
                        context,
                        "Update Profile Error",
                        "Request is already in progress");
                  } else if (_emailController.text.isEmpty ||
                          _emailController.text.trim().isEmpty

                      // ||
                      // !(RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                      //     .hasMatch(_emailController.text)))
                      ) {
                    showOkMessage(context, "Update Profile Error",
                        "Please enter username");
                  } else if (_nameController.text.isEmpty) {
                    showOkMessage(context, "Update Profile Error",
                        "Please enter your first name");
                  } else if (_lastNameController.text.isEmpty) {
                    showOkMessage(context, "Update Profile Error",
                        "Please enter your last name");
                  } else {
                    viewModel.onUpdateUserPressed(
                        context,
                        _nameController.text,
                        _lastNameController.text,
                        _emailController.text,
                        _mobileController.text);
                  }
                  // widget.viewModel.onUpdateUserPressed()
                }))));

    if (widget.viewModel.isLoading) {
      return const Center(
        child: Text("Loading..."),
      );
    }

    return Padding(
      padding: const EdgeInsets.only(top: 0, left: 40, right: 40),
      child: ListView.builder(
          itemCount: allSettings.length,
          padding: const EdgeInsets.only(
            left: 10.0,
            right: 10,
          ),
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
  }
}

Widget buildCompleteImage(BuildContext context, String avatar) {
  return Container(
      color: BrandingColors.cadiWhite,
      alignment: Alignment.center,
      child: CachedNetworkImage(
          height: 120,
          width: 120,
          imageUrl: avatar,
          imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  border: Border.all(color: BrandingColors.cadiGreen, width: 2),
                  borderRadius: const BorderRadius.all(Radius.circular(60)),
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
          fit: BoxFit.fitHeight,
          progressIndicatorBuilder: (context, url, downloadProgress) =>
              CircularProgressIndicator(
                  color: BrandingColors.cadiGreen,
                  value: downloadProgress.progress),
          errorWidget: (context, url, error) => Container(
                height: 120,
                width: 120,
                decoration: BoxDecoration(
                  color: BrandingColors.cadiBlackOpacity,
                  border: Border.all(color: BrandingColors.cadiGreen, width: 2),
                  borderRadius: const BorderRadius.all(Radius.circular(60)),
                ),
                child: Container(
                  alignment: Alignment.center,
                  child: const FaIcon(
                    FontAwesomeIcons.cloudArrowUp,
                    size: 40,
                    color: BrandingColors.cadiWhite,
                  ),
                ),
              )));
}

Widget buildSettingItems(BuildContext context, List<Widget> items) {
  return ListView.separated(
      shrinkWrap: true,
      padding: const EdgeInsets.only(top: 30, bottom: 20),
      //.separated(
      itemCount: items.length,
      separatorBuilder: (BuildContext context, int index) => const Divider(
            height: 10,
            thickness: 0,
            color: BrandingColors.background,
          ),
      itemBuilder: (BuildContext context, int index) {
        return items[index];
      });
}

void _launchURL(String url) async {
  if (!await launch(url)) throw 'Could not launch $url';
}

/**
 * 
//  *  //   ExpansionTile(
//                 title: Text(
//                   'Sub title',
//                 ),
//                 children: <Widget>[
//                   ListTile(
//                     title: Text('data'),
//                   )
//                 ],
//               ),
 */

