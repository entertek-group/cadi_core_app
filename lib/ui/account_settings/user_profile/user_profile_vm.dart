import 'dart:async';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_home_button.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_user_account.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:redux/redux.dart';
import 'user_profile_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class UserProfileScreenBuilder extends StatelessWidget {
  const UserProfileScreenBuilder({super.key});

  static const String route = route_helper.userProfileIndexScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(context, "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, UserProfileVM>(
        converter: UserProfileVM.fromStore,
        builder: (context, vm) {
          return UserProfilePage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class UserProfileVM {
  bool isLoading;
  bool isSubmitting;
  AuthState authState;
  Account account;
  AccountList? accounts;
  User? user;
  UniversalBranding universalBranding;

  final Function(BuildContext, String) onNavigatePressed;
  final Function(String token) setToken;
  final Function(Account account) setAccount;
  final Function(BuildContext context) logOut;
  final Function(
          BuildContext context, String firstName, String lastName, String email, String mobileNumber)
      onUpdateUserPressed;
  final Function(BuildContext context, String avatar, String avatarName) updateAvatar;
  UserProfileVM(
      {required this.isLoading,
      required this.isSubmitting,
      required this.authState,
      required this.user,
      required this.universalBranding,
      required this.account,
      required this.onNavigatePressed,
      required this.setToken,
      required this.accounts,
      required this.setAccount,
      required this.logOut,
      required this.onUpdateUserPressed,
      required this.updateAvatar});

  static UserProfileVM fromStore(Store<AppState> store) {
    return UserProfileVM(
      isLoading: store.state.isLoading,
      isSubmitting: store.state.isSubmitting,
      authState: store.state.authState,
      user: store.state.authState.user,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      account: store.state.accountState.account,
      accounts: store.state.accountState.accountList,
      setAccount: (Account account) {
        store.dispatch(SetSelectedAccount(account));
      },
      onUpdateUserPressed: (BuildContext context, String firstName,  String lastName,  String email,
          String mobileNumber) {
        final Completer<Null> completer = Completer<Null>();

        store.dispatch(UpdateUser(
            completer, firstName.trim(), lastName.trim(), email.trim(), mobileNumber.trim()));
        completer.future.then((_) {
          var error = store.state.authState.error;
          if (error.length > 1) {
            showOkMessage(context, "Update user error", error);
          } else {
            showOkMessage(
                context, "Update user sucess", "User has been updated.");
          }
        });
      },
      onNavigatePressed: (BuildContext context, String route) {
        if (store.state.isLoading) {
          return;
        }
        navigation_helper.navigateRoute(route, store, null);
      },
      setToken: (String token) {},
      updateAvatar: (BuildContext context, String avatar, String avatarName) {
        final Completer<ApiResponse> completer = Completer<ApiResponse>();
        store.dispatch(UpdateUserAvatar(completer, avatar, avatarName));

        completer.future.then((ApiResponse apiResponse) {
          if (apiResponse.success) {
            // store.dispatch(NavigateToAction.push(
            //     _route_helper.cargoUpdateSummaryScreenBuilder));
          } else {
            //show errpr
            showOkMessage(
                context, "Update avatar", "Could not update avatar image.");
          }
        });
      },
      logOut: (context) {
        navigation_helper.navigateRoute(
            route_helper.logoutScreen, store, null);
      },
    );
  }
}
