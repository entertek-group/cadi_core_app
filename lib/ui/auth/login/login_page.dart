import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'login_vm.dart';
import 'package:cadi_core_app/theme/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_logo.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';

class LoginView extends StatefulWidget {
  final LoginVM viewModel;

  const LoginView({
    super.key,
    required this.viewModel,
  });

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginView> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  static const Key _emailKey = Key(LoginKeys.emailKey);
  static const Key _passwordKey = Key(LoginKeys.passwordKey);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    widget.viewModel.clearError();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var viewModel = widget.viewModel;
    var topHeight = MediaQuery.of(context).size.height * 0.05;
    var topWidth = MediaQuery.of(context).size.width;
    return Container(
      // color: Colors.white,
      decoration: const BoxDecoration(
          image: DecorationImage(
        image: AssetImage(BrandingImages.backgroundLogin),
        fit: BoxFit.cover,
        colorFilter: ColorFilter.mode(
          BrandingColors.cadiBlackOpacity,
          BlendMode.srcOver,
        ),
      )),
      child: Form(
          key: _formKey,
          // autovalidateMode: AutovalidateMode.always,
          child: ListView(
            padding: EdgeInsets.only(left: 30, right: 20, top: topHeight),
            children: <Widget>[
              const Text(""),
              const Align(
                  alignment: Alignment.centerLeft,
                  child: AppLogo(
                    maxFontSize: 40,
                  )),
              const Text(""),

              Padding(
                padding: EdgeInsets.only(top: topHeight * 2.5),
                child: Center(
                  child: Text("Welcome",
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: PlatformSizing().headingTextSize(context),
                          color: BrandingColors.cadiWhite)),
                ),
              ),

              Padding(
                padding: EdgeInsets.only(top: topHeight / 2),
                child: Center(
                  child: Text("Login to continue",
                      style: TextStyle(
                          fontSize: PlatformSizing().normalTextSize(context),
                          fontWeight: FontWeight.w200,
                          color: BrandingColors.cadiGrey)),
                ),
              ),

              const Text(""),
//                      Text("Email Address"),
              Padding(
                padding: const EdgeInsets.only(
                  top: 10,
                  bottom: 0,
                ),
                child: PlatformTextField(
                  cursorColor: BrandingColors.cadiWhiteOpacity,
                  controller: _emailController,
                  key: _emailKey,
                  autocorrect: false,
                  material: (_, __) => MaterialAssets()
                      .textFieldData('Username', FontAwesomeIcons.userLarge),
                  cupertino: (_, __) => CupertinoAssets()
                      .textFieldData('Username', FontAwesomeIcons.userLarge),
                  style: TextStyle(
                      color: BrandingColors.cadiBlack,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  keyboardType: TextInputType.emailAddress,
                ),
              ),
              //Text("Password"),
              Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 10),
                child: PlatformTextField(
                  cursorColor: BrandingColors.cadiWhiteOpacity,
                  controller: _passwordController,
                  key: _passwordKey,
                  autocorrect: false,
                  material: (_, __) => MaterialAssets()
                      .textFieldData('Password', FontAwesomeIcons.lock),
                  cupertino: (_, __) => CupertinoAssets()
                      .textFieldData('Password', FontAwesomeIcons.lock),
                  style: TextStyle(
                      color: BrandingColors.cadiBlack,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  obscureText: true,
                ),
              ),
              viewModel.authState.error.isEmpty
                  ? Container()
                  : Container(
                      padding: const EdgeInsets.only(top: 10.0, bottom: 5.0),
                      child: Center(
                        child: Text(
                          viewModel.authState.error,
                          style: TextStyle(
                              color: BrandingColors.red,
                              fontWeight: FontWeight.w400,
                              fontSize:
                                  PlatformSizing().smallTextSize(context)),
                        ),
                      ),
                    ),
              const Text(""),
              const Text(""),
              Padding(
                padding: EdgeInsets.only(top: topHeight, left: 40, right: 40),
                child: BaseButton(
                    color: widget.viewModel.universalBranding
                        .getButtonPrimaryColor(),
                    child: Text(
                      'Login',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: PlatformSizing().normalTextSize(context)),
                    ),
                    onPressed: () {
                      if (_emailController.text.isEmpty ||
                              _emailController.text.trim().isEmpty
                          // ||
                          // !(RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                          //     .hasMatch(_emailController.text))
                          ) {
                        showOkMessage(
                            context, "Sign in error", "Not a valid username");
                      }

                      if (_passwordController.text.isEmpty) {
                        showOkMessage(context, "Sign in error",
                            "Please enter your password");
                      }

                      if (_passwordController.text.length < 6) {
                        showOkMessage(
                            // FontAwesomeIcons.warning,
                            context,
                            "Sign in error",
                            "Password length requires 6 or more characters.");
                      }

                      viewModel.onLoginPressed(context, _emailController.text,
                          _passwordController.text);
                    }),
              ),

              const Text(""),
              const Text(""),
              Container(
                width: topWidth,
                alignment: FractionalOffset.center,
                child: GestureDetector(
                  child: Text(
                    "Forgot Password?",
                    style: TextStyle(
                        fontStyle: FontStyle.italic,
                        color: BrandingColors.cadiGrey,
                        fontSize: PlatformSizing().smallTextSize(context),
                        fontWeight: FontWeight.w400),
                  ),
                  onTap: () {
                    viewModel.onPasswordResetPressed(context);
                  },
                ),
              ),
            ],
          )),
    );
  }
}
