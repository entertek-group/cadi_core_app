import 'dart:async';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'login_page.dart';
import 'package:redux/redux.dart';
//import 'package:repdynamics/ui/consultation_page.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
//import 'package:cadi_core_app/helpers/navigators.dart' as _navigator;

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  static const String route = route_helper.loginScreen;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, LoginVM>(
        converter: LoginVM.fromStore,
        builder: (context, vm) {
          return LoginView(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class LoginVM {
  bool isLoading;
  bool isSubmitting;
  AuthState authState;
  UniversalBranding universalBranding;
  final Function(BuildContext, String, String) onLoginPressed;
  final Function(BuildContext) onPasswordResetPressed;
  final Function clearError;

  LoginVM(
      {required this.isLoading,
      required this.isSubmitting,
      required this.authState,
      required this.universalBranding,
      required this.onLoginPressed,
      required this.onPasswordResetPressed,
      required this.clearError});

  static LoginVM fromStore(Store<AppState> store) {
    return LoginVM(
        isLoading: store.state.isLoading,
        isSubmitting: store.state.isSubmitting,
        authState: store.state.authState,
        universalBranding:
            UniversalBranding(store.state.accountState.activeService),
        onLoginPressed: (BuildContext context, String email, String password) {
          // if (store.state.isSubmitting) {
          //   return;
          // }
          print("here we are");
          final Completer<Null> completer = Completer<Null>();
          store
              .dispatch(Authenticate(completer, email.trim(), password.trim()));
          completer.future.then((_) {
            var error = store.state.authState.error;
            if (error.length > 1) {
              showOkMessage(
                  // FontAwesomeIcons.warning,
                  context,
                  "Sign in error",
                  error);
            } else {
              store.dispatch(
                  NavigateToAction.replace(route_helper.loaderLogin));
            }
          });
        },
        onPasswordResetPressed: (BuildContext context) {
          store.dispatch(
              NavigateToAction.push(route_helper.passwordResetScreen));
//          store.dispatch(NavigateToAction.replace(_route_helper.password_reset_screen));
        },
        clearError: () {
          store.dispatch(ClearAuthError());
        });
  }
}
