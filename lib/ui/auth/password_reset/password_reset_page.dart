import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/keys/keys.dart';
import '../../shared/components/base/base_ghost_button_dark.dart';
import 'password_reset_vm.dart';
import 'package:cadi_core_app/theme/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_logo.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

String formatData(BuildContext context, String data) {
  if (isMaterial(context)) {
    return data.toString();
  }
  return data;
}

class PasswordResetView extends StatefulWidget {
  final PasswordResetVM viewModel;

  const PasswordResetView({
    super.key,
    required this.viewModel,
  });

  @override
  _PasswordResetState createState() => _PasswordResetState();
}

class _PasswordResetState extends State<PasswordResetView> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final _emailController = TextEditingController();

  static const Key _emailKey = Key(PasswordResetKeys.emailKey);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _emailController.dispose();
    widget.viewModel.clearError();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var viewModel = widget.viewModel;
    return widget.viewModel.isLoading
        ? const AppLoading(message: "Loading...")
        : Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
              image: AssetImage(BrandingImages.backgroundLogin),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(
                BrandingColors.cadiBlackOpacity,
                BlendMode.srcOver,
              ),
            )),
            child: Form(
                key: _formKey,
                child: ListView(
                  padding: const EdgeInsets.only(
                    left: 30,
                    right: 20,
                    top: 10,
                  ),
                  children: <Widget>[
                    const Text(""),
                    const Align(
                        alignment: Alignment.centerLeft,
                        child: AppLogo(
                          maxFontSize: 40,
                        )),
                    const Text(""),
                    Padding(
                      padding: const EdgeInsets.only(top: 150, bottom: 20),
                      child: Text("Forgot your password?",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize:
                                  PlatformSizing().largerTextSize(context),
                              fontWeight: FontWeight.w500,
                              color: BrandingColors.cadiWhite)),
                    ),
                    const Text(""),
                    Center(
                      child: Text(
                          "No worries, we will send instructions via email explaining how to reset your password.",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize:
                                  PlatformSizing().normalTextSize(context),
                              color: BrandingColors.cadiGrey)),
                    ),
                    const Text(""),

//                      Text("Email Address"),
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 20),
                      child: PlatformTextField(
                        cursorColor: BrandingColors.cadiWhiteOpacity,
                        controller: _emailController,
                        key: _emailKey,
                        autocorrect: false,
                        material: (_, __) => MaterialAssets().textFieldData(
                            'Username/Email', FontAwesomeIcons.userLarge),
                        cupertino: (_, __) => CupertinoAssets().textFieldData(
                            'Username/Email', FontAwesomeIcons.userLarge),
                        style: TextStyle(
                            color: BrandingColors.cadiBlack,
                            fontSize:
                                PlatformSizing().inputTextFieldSize(context)),
                        keyboardType: TextInputType.emailAddress,
                      ),
                    ),
                    Center(
                      child: Text("Please enter your username or email address",
                          style: TextStyle(
                              fontStyle: FontStyle.italic,
                              fontSize: PlatformSizing().smallTextSize(context),
                              fontWeight: FontWeight.w100,
                              color: BrandingColors.cadiWhite)),
                    ),
                    const Text(""),
                    const Text(""),
                    const Text(""),
                    const Text(""),
                    const Text(""),
                    Padding(
                      padding: const EdgeInsets.only(left: 40, right: 40),
                      child: BaseButton(
                          color: widget.viewModel.universalBranding
                              .getButtonPrimaryColor(),
                          child: const Text('Reset Password',
                              style: TextStyle(
                                  fontSize: 20,
                                  color: BrandingColors.cadiWhite)),
                          onPressed: () {
                            if (_emailController.text.isEmpty ||
                                    _emailController.text.trim().isEmpty
                                // ||
                                // !(RegExp(
                                //         r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                //     .hasMatch(_emailController.text))
                                ) {
                              showOkMessage(context, "Incorrect Email",
                                  "Please validate your username format");
                            } else {
                              viewModel.onPasswordResetPressed(
                                  context, _emailController.text);
                            }
                          }),
                    ),
                    const Text(""),

                    Padding(
                      padding: const EdgeInsets.only(left: 40, right: 40),
                      child: BaseGhostButtonDark(
                        child: const Text(
                          "Back to login",
                          style: TextStyle(
                            fontSize: 18,
                            color: BrandingColors.cadiWhite,
                          ),
                        ),
                        onPressed: () => Navigator.of(context).pop(),
                      ),
                    ),
                  ],
                )),
          );
  }
}
