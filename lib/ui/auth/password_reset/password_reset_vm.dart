import 'dart:async';
import 'package:cadi_core_app/redux/models/api_response.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'password_reset_page.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/components/alerts.dart';

class PasswordResetScreen extends StatelessWidget {
  const PasswordResetScreen({super.key});

  static const String route = route_helper.passwordResetScreen;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      body: StoreConnector<AppState, PasswordResetVM>(
        converter: PasswordResetVM.fromStore,
        builder: (context, vm) {
          return PasswordResetView(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class PasswordResetVM {
  bool isLoading;
  bool isSubmitting;
  AuthState authState;
  final Function(BuildContext, String email) onPasswordResetPressed;
  final Function clearError;
  UniversalBranding universalBranding;
  PasswordResetVM(
      {required this.isLoading,
      required this.isSubmitting,
      required this.authState,
      required this.universalBranding,
      required this.onPasswordResetPressed,
      required this.clearError});

  static PasswordResetVM fromStore(Store<AppState> store) {
    return PasswordResetVM(
        isLoading: store.state.isLoading,
        isSubmitting: store.state.isSubmitting,
        authState: store.state.authState,
        universalBranding:
            UniversalBranding(store.state.accountState.activeService),
        onPasswordResetPressed: (BuildContext context, String email) {
          final Completer<ApiResponse> completer = Completer<ApiResponse>();
          store.dispatch(ForgetPassword(completer, email));

          completer.future.then((ApiResponse apiResponse) {
            if (apiResponse.success) {
              showOkMessage(context, "Password Reset email sent",
                  "Please check your email.");
            } else {
              showOkMessage(
                  context, "Something went wrong", apiResponse.result);
            }
          });
        },
        clearError: () {
          store.dispatch(ClearAuthError());
        });
  }
}
