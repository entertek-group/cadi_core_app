import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:flutter/material.dart';
import 'welcome_vm.dart';
import 'package:cadi_core_app/theme/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_logo.dart';

class WelcomeView extends StatefulWidget {
  final WelcomeVM viewModel;

  const WelcomeView({
    super.key,
    required this.viewModel,
  });

  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<WelcomeView> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var viewModel = widget.viewModel;
    var topHeight = MediaQuery.of(context).size.height * 0.05;
    return Container(
      // color: Colors.white,
      decoration: const BoxDecoration(
          image: DecorationImage(
        image: AssetImage(BrandingImages.backgroundLogin),
        fit: BoxFit.cover,
        colorFilter: ColorFilter.mode(
          BrandingColors.cadiBlackOpacity,
          BlendMode.srcOver,
        ),
      )),
      child: Form(
          key: _formKey,
          // autovalidateMode: AutovalidateMode.always,
          child: ListView(
            padding: EdgeInsets.only(left: 30, right: 40, top: topHeight),
            children: <Widget>[
              const Text(""),
              const Align(
                  alignment: Alignment.centerLeft,
                  child: AppLogo(
                    maxFontSize: 40,
                  )),
              Padding(
                padding: EdgeInsets.only(top: topHeight * 2.5, right: 20),
                child: Center(
                  child: Text("Enabling carbon neutral cargo movements.",
                      style: TextStyle(
                          fontWeight: FontWeight.w100,
                          fontSize: PlatformSizing().largerTextSize(context),
                          color: BrandingColors.cadiGrey)),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: topHeight * 1, right: 50),
                child: Center(
                  child: Text("Creating positive change.",
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontWeight: FontWeight.w100,
                          fontSize: PlatformSizing().largerTextSize(context),
                          color: BrandingColors.cadiGrey)),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: topHeight * 1, right: 45),
                child: Center(
                  child: Text("One cargo movement at a time.",
                      style: TextStyle(
                          fontWeight: FontWeight.w100,
                          fontSize: PlatformSizing().largerTextSize(context),
                          color: BrandingColors.cadiGrey)),
                ),
              ),
              const Text(""),
              Padding(
                padding: const EdgeInsets.only(top: 80, left: 40, right: 40),
                child: BaseButton(
                  color: widget.viewModel.universalBranding
                      .getButtonPrimaryColor(),
                  child: Text(
                    "Get Started",
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: PlatformSizing().normalTextSize(context)),
                  ),
                  onPressed: () {
                    viewModel.onGetStartedPressed();
                  },
                ),
              ),
            ],
          )),
    );
  }
}
