import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'welcome_page.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({super.key});

  static const String route = route_helper.welcomeScreen;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, WelcomeVM>(
        converter: WelcomeVM.fromStore,
        builder: (context, vm) {
          return WelcomeView(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class WelcomeVM {
  bool isLoading;
  bool isSubmitting;
  AuthState authState;
  UniversalBranding universalBranding;
  final Function(BuildContext, String) onButtonPressed;
  final Function() onGetStartedPressed;

  WelcomeVM({
    required this.isLoading,
    required this.isSubmitting,
    required this.authState,
    required this.universalBranding,
    required this.onButtonPressed,
    required this.onGetStartedPressed,
  });

  static WelcomeVM fromStore(Store<AppState> store) {
    return WelcomeVM(
      isLoading: store.state.isLoading,
      isSubmitting: store.state.isSubmitting,
      authState: store.state.authState,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      onButtonPressed: (BuildContext context, String route) {
        if (store.state.isLoading) {
          return;
        }
        navigation_helper.navigateRoute(route, store, null);
      },
      onGetStartedPressed: () {
        store.dispatch(NavigateToAction.replace(route_helper.loginScreen));
        store.dispatch(StopShowingWelcome());
      },
    );
  }
}
