import 'package:cadi_core_app/factory/cadi_enum_factory.dart';
import 'package:cadi_core_app/factory/ui_factory.dart';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/helpers/helpers.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_generic.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/base/base_move_event_select.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'cargo_movement_container_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/setting_tile.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

class CargoMovementContainerPage extends StatefulWidget {
  final CargoMovementContainerVM viewModel;
  const CargoMovementContainerPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<CargoMovementContainerPage> createState() {
    return CargoMovementContainerPageState(viewModel: viewModel);
  }
}

class CargoMovementContainerPageState
    extends State<CargoMovementContainerPage> {
  final CargoMovementContainerVM viewModel;
  //final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  String _selectedShipmentStatus = "";

  final _inputContainerOneController = TextEditingController();
  final _inputContainerTwoController = TextEditingController();
  final _inputContainerThreeController = TextEditingController();
  final _inputContainerStowagePositionController = TextEditingController();
  final _selectCargoTypeController = TextEditingController();
  final _selectJobPackTypeController = TextEditingController();

  String _inputContainerOneTitle = "Container Number";
  String _inputContainerTwoTitle = "Seal Number";
  String _inputContainerThreeTitle = "No Seal Number?";

  bool _inputOneVisible = true;
  bool _inputTwoVisible = true;
  bool _inputThreeVisible = true;
  bool _selectCargoTypeVisible = true;
  bool _selectJobPackTypeVisible = false;

  int _selectedNoSealIndex = 0;

  final FocusNode inputContainerOneNode = FocusNode();
  final FocusNode inputContainerTwoNode = FocusNode();
  final FocusNode stowagePositionNode = FocusNode();
  final FocusNode inputThreeNode = FocusNode();

  static const Key _inputContainerOneKey = Key(CargoKeys.inputContainerOneKey);
  static const Key _inputContainerTwoKey = Key(CargoKeys.inputContainerTwoKey);
  static const Key _inputContainerThreeKey =
      Key(CargoKeys.inputContainerThreeKey);
  static const Key _selectCargoTypeKey = Key(CargoKeys.selectCargoTypeKey);
  static const Key _selectJobPackTypeKey = Key(CargoKeys.selectJobPackTypeKey);
  static const Key _selectStowagePositionKey =
      Key(CargoKeys.inputContainerStowagePositionKey);

  List<DropdownItem> _noSealItems = [];
  List<DropdownItem> _cargoTypeItems = [];
  final List<DropdownItem> _containerTypeItems = [];
  List<DropdownItem> _jobPackTypeItems = [];

  int _selectedCargoTypeIndex = 0;
  int _selectedJobPackTypeIndex = 0;

  bool _overrideContainerFormat = false;

  CargoMovementContainerPageState({
    Key? key,
    required this.viewModel,
  });

  void _setSealSelect(int index, [bool initiatePicker = true]) {
    setState(() {
      _inputContainerThreeController.text = _noSealItems[index].text;
      _selectedNoSealIndex = index;
    });

    if (index != 0 &&
        initiatePicker &&
        widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
      Future.delayed(
          const Duration(milliseconds: 300), _showCargoTypeItemsBottomPicker);
    } else if (initiatePicker) {
      FocusScope.of(context).requestFocus(inputContainerTwoNode);
    }
  }

  void _setCargoType(int index, [bool initiatePicker = true]) {
    setState(() {
      _selectCargoTypeController.text = _cargoTypeItems[index].text;
      _selectedCargoTypeIndex = index;
    });
    // if (initiatePicker &&
    //     widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
    //   Future.delayed(const Duration(milliseconds: 300),
    //       _showContainerTypeItemsBottomPicker);
    // }
  }

  void _setJobPackType(int index, [bool initiatePicker = true]) {
    setState(() {
      _selectJobPackTypeController.text = _jobPackTypeItems[index].text;
      _selectedJobPackTypeIndex = index;
    });

    if (initiatePicker &&
        widget.viewModel.settingsAutoDisplayNextDropdownOrInput &&
        widget.viewModel.settingsAutoSubmitPage) {
      Future.delayed(const Duration(milliseconds: 300), goNext);
    }
  }

  @override
  initState() {
    super.initState();

    _noSealItems = UiFactory().convertToDropdown<CadiEnum, String>(
        CadiEnumFactory().createNoSealOptions(),
        (item) => item.description,
        (item) => item.id);

    _cargoTypeItems = UiFactory().convertToDropdown<CadiGeneric, String>(
        widget.viewModel.cargoTypes,
        (item) => item.description,
        (item) => item.code);

    // _containerTypeItems = UiFactory()
    //     .convertToDropdown<AccountContainerType, String>(
    //         widget.viewModel.accountContainerTypes,
    //         (item) => item.code,
    //         (item) => item.code);

    _jobPackTypeItems = UiFactory().convertToDropdown<CadiGeneric, String>(
        widget.viewModel.jobPackTypes,
        (item) => item.description,
        (item) => item.code);
    _setSealSelect(0, false);

    int cargoTypeIndex = 0;

    if (widget.viewModel.cargoTypes.isNotEmpty) {
      var i = widget.viewModel.cargoTypes
          .indexWhere((element) => element.code == "FCL");
      cargoTypeIndex = i < 0 ? 0 : i;
      _setCargoType(cargoTypeIndex, false);
    }

    int packTypeIndex = 0;

    if (widget.viewModel.jobPackTypes.isNotEmpty) {
      var i = widget.viewModel.jobPackTypes
          .indexWhere((element) => element.code == "PLT");
      packTypeIndex = i < 0 ? 0 : i;
      _setJobPackType(packTypeIndex, false);
    }

    if (widget.viewModel.cargoMovementCreate.shipment != "") {
      setState(() {
        _selectedShipmentStatus = widget.viewModel.cargoMovementCreate.shipment;
      });
      _setShipment(false);
      if (_selectedShipmentStatus == CadiCargoMovementShipment.container ||
          _selectedShipmentStatus == CadiCargoMovementShipment.restow) {
        _inputContainerOneController.text =
            widget.viewModel.cargoMovementCreate.containerData.containerNumber;
        _inputContainerTwoController.text =
            widget.viewModel.cargoMovementCreate.containerData.sealNumber1;
        _inputContainerThreeController.text =
            widget.viewModel.cargoMovementCreate.containerData.sealNumber2;
      } else {
        _inputContainerOneController.text =
            widget.viewModel.cargoMovementCreate.packData.packSequenceNumber;
        _inputContainerTwoController.text = "";
        _inputContainerThreeController.text = "";
      }
    } else {
      _selectedShipmentStatus = CadiCargoMovementShipment.container;
    }
  }

  @override
  void dispose() {
    _inputContainerOneController.dispose();
    _inputContainerTwoController.dispose();
    _inputContainerThreeController.dispose();
    _selectCargoTypeController.dispose();
    _selectJobPackTypeController.dispose();
    super.dispose();
  }

  _setShipment([bool initiateFocus = true]) {
    switch (_selectedShipmentStatus) {
      case CadiCargoMovementShipment.container:
        setState(() {
          _inputContainerOneTitle = "Container Number";
          _inputOneVisible = true;
          _inputContainerTwoTitle = "Seal Number";
          _inputTwoVisible = true;
          _inputContainerThreeTitle = "No Seal Number?";
          _inputThreeVisible = true;
          _selectCargoTypeVisible = true;
          _selectJobPackTypeVisible = false;
        });
        break;
      case CadiCargoMovementShipment.breakBulk:
        setState(() {
          _inputContainerOneTitle = "Bill Of Lading";
          _inputOneVisible = true;
          _inputContainerTwoTitle = "Pack Line Number";
          _inputTwoVisible = true;
          _inputContainerThreeTitle = "";
          _inputThreeVisible = false;
          _selectCargoTypeVisible = false;
          _selectJobPackTypeVisible = true;
        });
        break;
      case CadiCargoMovementShipment.restow:
        setState(() {
          _inputContainerOneTitle = "Container Number";
          _inputOneVisible = true;
          _inputContainerTwoTitle = "Seal Number";
          _inputTwoVisible = true;
          _inputContainerThreeTitle = "No Seal Number?";
          _inputThreeVisible = true;
          _selectCargoTypeVisible = true;
          _selectJobPackTypeVisible = false;
        });
        break;
      default:
        setState(() {
          _inputContainerOneTitle = "Container Number";
          _inputOneVisible = true;
          _inputContainerTwoTitle = "Seal Number";
          _inputTwoVisible = true;
          _inputContainerThreeTitle = "No Seal Number?";
          _inputThreeVisible = true;
          _selectCargoTypeVisible = true;
          _selectJobPackTypeVisible = false;
        });
        break;
    }
    if (initiateFocus) {
      FocusScope.of(context).requestFocus(inputContainerOneNode);
    }
  }

  void _showCargoTypeItemsBottomPicker() {
    if (widget.viewModel.cargoTypes.isEmpty) {
      showOkMessage(context, "Cannot select Cargo Type",
          "Account setup is incomplete. Please contact support. ");
    } else {
      showBottomDropdown(context, 'Cargo Type', 'Please select a Cargo Type',
          _cargoTypeItems, _selectedCargoTypeIndex, _setCargoType, true);
    }
  }

  void _showJobPackTypeBottomPicker() {
    if (widget.viewModel.jobPackTypes.isEmpty) {
      showOkMessage(
          // FontAwesomeIcons.server,
          context,
          "Cannot select Pack Type",
          "Account setup is incomplete. Please contact support. ");
    } else {
      showBottomDropdown(context, 'Pack Type', 'Please select a Pack Type',
          _jobPackTypeItems, _selectedJobPackTypeIndex, _setJobPackType, true);
    }
  }

  void goNext() {
    if ((_selectedShipmentStatus == CadiCargoMovementShipment.container ||
        _selectedShipmentStatus == CadiCargoMovementShipment.restow)) {
      if (_inputContainerOneController.text.trim() == "") {
        viewModel.showErrorAlert(context, "Please input a container number");
        return;
      } else if ((widget.viewModel.cargoRole ==
                  CadiCargoMovementRole.freightLoad ||
              widget.viewModel.cargoRole ==
                  CadiCargoMovementRole.freightUnload) &&
          _inputContainerStowagePositionController.text.isEmpty) {
        viewModel.showErrorAlert(context, "Please input a stowage position");
        return;
      } else if (_selectedNoSealIndex == 0 &&
          _inputContainerTwoController.text.trim() == "") {
        viewModel.showErrorAlert(context, "Please input a Serial number");
        return;
      } else if (!_overrideContainerFormat &&
          !isContainerInputValid(
              context, _inputContainerOneController.text.trim())) {
        showMessageWithActions(
            context,
            "Container Number Format",
            WarningMessages.containerOverrideMessage,
            () {
              setState(() {
                _overrideContainerFormat = true;
              });
              Navigator.pop(context);
              goNextViewModel();
            },
            "Accept",
            () {
              Navigator.pop(context);
            },
            "Cancel");
        return;
      }
    } else if (_selectedShipmentStatus == CadiCargoMovementShipment.breakBulk &&
        _inputContainerTwoController.text.trim() == "") {
      viewModel.showErrorAlert(context, "Please input a serial number.");
      return;
    } else if (_selectedShipmentStatus == CadiCargoMovementShipment.breakBulk &&
        _inputContainerTwoController.text.trim() == "") {
      viewModel.showErrorAlert(context, "Please input a pack line number.");
      return;
    } else if (_selectedShipmentStatus != CadiCargoMovementShipment.breakBulk &&
        _containerTypeItems.isEmpty) {
      viewModel.showErrorAlert(context, "Container Type Cannot be empty.");
      return;
    }
    goNextViewModel();
  }

  void goNextViewModel() {
    viewModel.setCargoContainerVariables(
        context,
        _selectedShipmentStatus,
        _inputContainerOneController.text,
        _inputContainerTwoController.text,
        _inputContainerThreeController.text,
        _inputContainerStowagePositionController.text,
        _cargoTypeItems[_selectedCargoTypeIndex].value,
        _jobPackTypeItems[_selectedJobPackTypeIndex].value);
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> moveEventWidgets = <Widget>[];
    for (var cadiEnum in widget.viewModel.cargoMovementShipments) {
      var color = _selectedShipmentStatus != cadiEnum.id
          ? BrandingColors.white
          : BrandingColors.cadiGrey;

      moveEventWidgets.add(buildContainerCard(
        context,
        "",
        () => {
          setState(() {
            _selectedShipmentStatus = cadiEnum.id;
          }),
          _setShipment(),
        },
        color,
        cadiEnum.description,
      ));
      // index += 1;
    }

    List<SettingTile> allOptions = <SettingTile>[];

    allOptions.addAll([
      SettingTile(
        0,
        'Active Account',
        Column(
          children: <Widget>[
            Text(
                "Cargo Movement: ${getCargoMovementRoleDescription(widget.viewModel.cargoRole)}",
                style: TextStyle(
        
                    // fontWeight: FontWeight.w500,
                    fontSize: PlatformSizing().normalTextSize(context),
                    color: BrandingColors.cadiBlack)),
            // Any additional widget/codes
          ],
        ),
      ),
      SettingTile(
        1,
        'Account Operations',
        Container(
          margin: const EdgeInsets.only(
              top: 10.0, bottom: 15), // Add your own value here
          child: Column(
            children: <Widget>[
              Text("Enter Shipment details",
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),

      SettingTile(
          2,
          "Container List",
          Material(
              color: BrandingColors.cadiWhite,
              child: buildMoveEventList(context, moveEventWidgets))),
      SettingTile(
          3,
          'INPUT ONE',
          Material(
            color: BrandingColors.cadiWhite,
            child: ListTile(
                title: Text(_inputContainerOneTitle,
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack))),
          )),
      SettingTile(
          4,
          '',
          _inputOneVisible
              ? PlatformTextField(
                  cursorColor: BrandingColors.cadiGrey,
                  focusNode: inputContainerOneNode,
                  textCapitalization: TextCapitalization.characters,
                  controller: _inputContainerOneController,
                  key: _inputContainerOneKey,
                  autocorrect: false,
                  material: (_, __) => MaterialAssetsDark()
                      .textFieldData("", FontAwesomeIcons.magnifyingGlass),
                  cupertino: (_, __) => CupertinoAssets()
                      .textFieldData(_inputContainerOneTitle, null),
                  style: TextStyle(
                      color: BrandingColors.cadiBlack,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  keyboardType: TextInputType.text,
                  //  maxLength: 11,
                  onEditingComplete: () {
                    if (widget
                        .viewModel.settingsAutoDisplayNextDropdownOrInput) {
                      if (widget.viewModel.cargoRole ==
                              CadiCargoMovementRole.freightLoad ||
                          widget.viewModel.cargoRole ==
                              CadiCargoMovementRole.freightUnload) {
                        FocusScope.of(context)
                            .requestFocus(stowagePositionNode);
                      } else if (_selectedShipmentStatus ==
                              CadiCargoMovementShipment.container ||
                          _selectedShipmentStatus ==
                              CadiCargoMovementShipment.restow) {
                        showBottomDropdown(
                            context,
                            'Seal Item',
                            'Please make a selection',
                            _noSealItems,
                            _selectedNoSealIndex,
                            _setSealSelect,
                            true);
                      } else if (_selectedShipmentStatus ==
                          CadiCargoMovementShipment.breakBulk) {
                        FocusScope.of(context)
                            .requestFocus(inputContainerTwoNode);
                      }
                    }
                  },
                )
              : Container()),

      SettingTile(
          5,
          'STOWAGE POSITION',
          widget.viewModel.cargoRole == CadiCargoMovementRole.freightLoad ||
                  widget.viewModel.cargoRole ==
                      CadiCargoMovementRole.freightUnload
              ? Material(
                  color: BrandingColors.cadiWhite,
                  child: ListTile(
                      title: Text('Stowage Position',
                          style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize:
                                  PlatformSizing().mediumTextSize(context),
                              color: BrandingColors.cadiBlack))),
                )
              : Container()),
      SettingTile(
          6,
          'Stowage Position',
          widget.viewModel.cargoRole == CadiCargoMovementRole.freightLoad ||
                  widget.viewModel.cargoRole ==
                      CadiCargoMovementRole.freightUnload
              ? PlatformTextField(
                  cursorColor: BrandingColors.cadiGrey,
                  inputFormatters: [
                    StowagePositionFormatter(sample: 'xx xx xx', separator: ' ')
                  ],
                  focusNode: stowagePositionNode,
                  textCapitalization: TextCapitalization.characters,
                  controller: _inputContainerStowagePositionController,
                  key: _selectStowagePositionKey,
                  autocorrect: false,
                  material: (_, __) => MaterialAssetsDark()
                      .textFieldData("", FontAwesomeIcons.magnifyingGlass),
                  cupertino: (_, __) =>
                      CupertinoAssets().textFieldData('XX XX XX', null),
                  style: TextStyle(
                      color: BrandingColors.cadiBlack,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  keyboardType: TextInputType.number,
                  maxLength: 8,
                  onEditingComplete: () {
                    if (_inputThreeVisible &&
                        widget
                            .viewModel.settingsAutoDisplayNextDropdownOrInput) {
                      showBottomDropdown(
                          context,
                          'Seal Item',
                          'Please make a selection',
                          _noSealItems,
                          _selectedNoSealIndex,
                          _setSealSelect,
                          true);
                    }
                  },
                )
              : Container()),
      SettingTile(
          7,
          'INPUT THREE',
          _inputThreeVisible
              ? Material(
                  color: BrandingColors.cadiWhite,
                  child: ListTile(
                      title: Text(_inputContainerThreeTitle,
                          style: TextStyle(
                              fontStyle: FontStyle.italic,
                              fontSize:
                                  PlatformSizing().mediumTextSize(context),
                              color: BrandingColors.cadiBlack))),
                )
              : Container()),

      // SEAL NUMBER
      SettingTile(
          8,
          '',
          _inputThreeVisible
              ? Material(
                  color: BrandingColors.cadiWhite,
                  child: InkWell(
                      onTap: () {
                        showBottomDropdown(
                            context,
                            'Seal Item',
                            'Please make a selection',
                            _noSealItems,
                            _selectedNoSealIndex,
                            _setSealSelect,
                            true);
                      },
                      child: PlatformTextField(
                        cursorColor: BrandingColors.cadiGrey,
                        textCapitalization: TextCapitalization.characters,
                        controller: _inputContainerThreeController,
                        key: _inputContainerThreeKey,
                        autocorrect: false,
                        enabled: false,
                        material: (_, __) => MaterialAssetsDark()
                            .textFieldData("", FontAwesomeIcons.chevronDown),
                        cupertino: (_, __) => CupertinoAssets().textFieldData(
                            _inputContainerThreeTitle,
                            FontAwesomeIcons.chevronDown),
                        style: TextStyle(
                            color: BrandingColors.cadiBlack,
                            fontSize:
                                PlatformSizing().inputTextFieldSize(context)),
                        keyboardType: TextInputType.text,
                        onEditingComplete: () {
                          if (_inputTwoVisible &&
                              _selectedNoSealIndex == 0 &&
                              widget.viewModel
                                  .settingsAutoDisplayNextDropdownOrInput) {
                            FocusScope.of(context)
                                .requestFocus(inputContainerTwoNode);
                          } else {
                            // sdf
                          }
                        },
                      )))
              : Container()),
      SettingTile(
          9,
          'INPUT TWO',
          _selectedNoSealIndex == 0
              ? Material(
                  color: BrandingColors.cadiWhite,
                  child: ListTile(
                      title: Text(_inputContainerTwoTitle,
                          style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize:
                                  PlatformSizing().mediumTextSize(context),
                              color: BrandingColors.cadiBlack))),
                )
              : Container()),
      SettingTile(
          10,
          '',
          _inputTwoVisible && _selectedNoSealIndex == 0
              ? PlatformTextField(
                  cursorColor: BrandingColors.cadiGrey,
                  focusNode: inputContainerTwoNode,
                  textCapitalization: TextCapitalization.characters,
                  controller: _inputContainerTwoController,
                  key: _inputContainerTwoKey,
                  autocorrect: false,
                  material: (_, __) => MaterialAssetsDark()
                      .textFieldData("", FontAwesomeIcons.magnifyingGlass),
                  cupertino: (_, __) => CupertinoAssets()
                      .textFieldData(_inputContainerTwoTitle, null),
                  style: TextStyle(
                      color: BrandingColors.cadiBlack,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  keyboardType: TextInputType.text,
                  onEditingComplete: () {
                    FocusScope.of(context).nextFocus();

                    if (_selectedShipmentStatus ==
                        CadiCargoMovementShipment.breakBulk) {
                      Future.delayed(const Duration(milliseconds: 300),
                          _showJobPackTypeBottomPicker);
                    } else if (_selectedShipmentStatus ==
                            CadiCargoMovementShipment.container ||
                        _selectedShipmentStatus ==
                            CadiCargoMovementShipment.restow) {
                      Future.delayed(const Duration(milliseconds: 300),
                          _showCargoTypeItemsBottomPicker);
                    }
                  })
              : Container()),
    ]);

    if (_selectCargoTypeVisible == true) {
      allOptions.addAll([
        SettingTile(
            11,
            'CARGO TYPE',
            Material(
              color: BrandingColors.cadiWhite,
              child: ListTile(
                  title: Text("Cargo Type",
                      style: TextStyle(
                          fontStyle: FontStyle.normal,
                          fontSize: PlatformSizing().mediumTextSize(context),
                          color: BrandingColors.cadiBlack))),
            )),
        SettingTile(
            12,
            "",
            Material(
                color: BrandingColors.cadiWhite,
                child: InkWell(
                  onTap: () {
                    _showCargoTypeItemsBottomPicker();
                  },
                  child: PlatformTextField(
                    cursorColor: BrandingColors.cadiGrey,
                    textCapitalization: TextCapitalization.characters,
                    controller: _selectCargoTypeController,
                    key: _selectCargoTypeKey,
                    autocorrect: false,
                    enabled: false,
                    material: (_, __) => MaterialAssetsDark()
                        .textFieldData("", FontAwesomeIcons.chevronDown),
                    cupertino: (_, __) => CupertinoAssets().textFieldData(
                        'Cargo Type', FontAwesomeIcons.chevronDown),
                    style: TextStyle(
                        color: BrandingColors.cadiBlack,
                        fontWeight: FontWeight.w400,
                        fontSize: PlatformSizing().inputTextFieldSize(context)),
                    keyboardType: TextInputType.text,
                  ),
                ))),
      ]);
    }

    if (_selectJobPackTypeVisible == true) {
      allOptions.addAll([
        SettingTile(
            15,
            'JOB PACK TYPE',
            Material(
              color: BrandingColors.cadiWhite,
              child: ListTile(
                  title: Text("Pack Type",
                      style: TextStyle(
                          fontStyle: FontStyle.normal,
                          fontSize: PlatformSizing().mediumTextSize(context),
                          color: BrandingColors.cadiBlack))),
            )),
        SettingTile(
            16,
            "",
            Material(
                color: BrandingColors.cadiWhite,
                child: InkWell(
                  onTap: () {
                    _showJobPackTypeBottomPicker();
                  },
                  child: PlatformTextField(
                    cursorColor: BrandingColors.cadiGrey,
                    textCapitalization: TextCapitalization.characters,
                    controller: _selectJobPackTypeController,
                    key: _selectJobPackTypeKey,
                    autocorrect: false,
                    enabled: false,
                    material: (_, __) => MaterialAssetsDark()
                        .textFieldData("", FontAwesomeIcons.chevronDown),
                    cupertino: (_, __) => CupertinoAssets().textFieldData(
                        'Pack Type', FontAwesomeIcons.chevronDown),
                    style: TextStyle(
                        color: BrandingColors.cadiBlack,
                        fontWeight: FontWeight.w400,
                        fontSize: PlatformSizing().inputTextFieldSize(context)),
                    keyboardType: TextInputType.text,
                  ),
                ))),
      ]);
    }

    allOptions.addAll([
      const SettingTile(
          17,
          '',
          Material(
              color: BrandingColors.cadiWhite,
              child: ListTile(
                title: Text(''),
              ))),
      SettingTile(
          18,
          '',
          Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 50.0, vertical: 0.0),
              child: BaseButton(
                  color: widget.viewModel.universalBranding
                      .getButtonPrimaryColor(),
                  child: Text(
                    'Next',
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: PlatformSizing().normalTextSize(context)),
                  ),
                  onPressed: () {
                    goNext();
                  })))
    ]);

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          //.separated(
          itemCount: allOptions.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          // separatorBuilder: (BuildContext context, int index) =>
          //     const Divider(height: 1),
          itemBuilder: (BuildContext context, int index) {
            return allOptions[index].body;
          }),
    );
    // );
    //return new SafeArea(child: Text("Setting Index placeholder"));
  }

  Widget buildMoveEventList(BuildContext context, List<Widget> moveEvents) {
  return LayoutBuilder(
    builder: (context, constraints) {
      double mainAxisSpacing = constraints.maxWidth > 600 ? 20 : 10; // Adjust spacing based on screen width
      double crossAxisSpacing = constraints.maxWidth > 600 ? 20 : 10; // Adjust spacing based on screen width
      double childAspectRatio = constraints.maxWidth > 600 ? 6 : 2.8; // Adjust aspect ratio based on screen width

      return GridView(
        padding: const EdgeInsets.only(top: 10, bottom: 20, left: 5, right: 5),
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: mainAxisSpacing,
          crossAxisSpacing: crossAxisSpacing,
          childAspectRatio: childAspectRatio,
        ),
        children: moveEvents,
      );
    },
  );
}

  Widget buildContainerCard(
    BuildContext context,
    String menuName,
    Function() navFunction,
    Color color,
    String eventStatus,
  ) {
    return BaseMoveEventSelect(
      color: color,
      onPressed: navFunction,
      child: Center(
        child: Text(
          eventStatus,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: PlatformSizing().superSmallTextSize(context),
            color: BrandingColors.cadiBlack,
          ),
        ),
      ),
    );
  }
}
