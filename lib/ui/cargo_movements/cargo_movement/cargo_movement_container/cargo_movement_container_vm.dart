import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/theme/theme.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';

import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:redux/redux.dart';
import 'cargo_movement_container_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class CargoMovementContainerScreenBuilder extends StatelessWidget {
  const CargoMovementContainerScreenBuilder({super.key});

  static const String route = route_helper.cargoMovementContainerScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(
                  // FontAwesomeIcons.warning,
                  context,
                  "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, CargoMovementContainerVM>(
        converter: CargoMovementContainerVM.fromStore,
        builder: (context, vm) {
          return CargoMovementContainerPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class CargoMovementContainerVM {
  bool isLoading;
  bool settingsAutoDisplayNextDropdownOrInput;
  bool settingsAutoSubmitPage;
  AuthState authState;
  Account account;
  CargoMovementCreate cargoMovementCreate;
  String cargoRole;
  UniversalBranding universalBranding;
  List<CadiEnum> cargoMovementShipments;
  List<CadiGeneric> cargoTypes;
  // List<AccountContainerType> accountContainerTypes;
  List<CadiGeneric> jobPackTypes;
  final Function(BuildContext, String) onNavigatePressed;
  final Function(String token) setToken;
  final Function(
      BuildContext context,
      String shipment,
      String inputOne,
      String inputTwo,
      String inputThree,
      String inputStowagePosition,
      String selectedCargoType,
      String selectedJobPackType) setCargoContainerVariables;
  final Function(BuildContext context, String message) showErrorAlert;
  CargoMovementContainerVM({
    required this.isLoading,
    required this.settingsAutoDisplayNextDropdownOrInput,
    required this.settingsAutoSubmitPage,
    required this.authState,
    required this.account,
    required this.cargoRole,
    required this.universalBranding,
    required this.cargoTypes,
    // required this.accountContainerTypes,
    required this.jobPackTypes,
    required this.cargoMovementCreate,
    required this.cargoMovementShipments,
    required this.onNavigatePressed,
    required this.setToken,
    required this.setCargoContainerVariables,
    required this.showErrorAlert,
  });

  static CargoMovementContainerVM fromStore(Store<AppState> store) {
    return CargoMovementContainerVM(
        settingsAutoDisplayNextDropdownOrInput:
            store.state.uiState.uiDropDownOrInputAutoContinue,
        settingsAutoSubmitPage: store.state.uiState.uiAutoSubmitPage,
        isLoading: store.state.isLoading,
        authState: store.state.authState,
        account: store.state.accountState.account,
        cargoRole: store.state.uiState.cargoRole,
        universalBranding:
            UniversalBranding(store.state.accountState.activeService),
        cargoTypes: store.state.referenceState.cargoTypes,
        // accountContainerTypes:
        //     store.state.accountState.filteredAccountContainerTypes,
        jobPackTypes: store.state.referenceState.jobPackTypes,
        cargoMovementCreate: store.state.cargoMovementState.cargoMovementCreate,
        showErrorAlert: (BuildContext context, String message) {
          showOkMessage(
              // FontAwesomeIcons.warning,
              context,
              "Submit Error",
              message);
        },
        cargoMovementShipments: store.state.uiState.cargoRole ==
                    CadiCargoMovementRole.gateIn ||
                store.state.uiState.cargoRole == CadiCargoMovementRole.gateOut
            ? store.state.referenceState.cargoMovementShipments
                .where(
                    (element) => element.id != CadiCargoMovementShipment.restow)
                .toList()
            : store.state.referenceState.cargoMovementShipments,
        onNavigatePressed: (BuildContext context, String route) {
          if (store.state.isLoading) {
            return;
          }
          navigation_helper.navigateRoute(route, store, null);
        },
        setToken: (String token) {},
        setCargoContainerVariables: (BuildContext context,
            String shipment,
            String inputOne,
            String inputTwo,
            String inputThree,
            String inputStowagePosition,
            String selectedCargoType,
            String selectedJobPackType) {
          store.dispatch(SetCargoContainerVariables(
              shipment,
              inputOne.replaceAll(' ', ''),
              inputTwo.replaceAll(' ', ''),
              inputThree.replaceAll(' ', ''),
              inputStowagePosition.replaceAll(' ', ''),
              selectedCargoType,
              "",
              selectedJobPackType));
          navigation_helper.navigateRoute(
              route_helper.cargoMovementStatusScreenBuilder, store, null);
        });
  }
}
