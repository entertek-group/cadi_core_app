import 'package:cadi_core_app/factory/ui_factory.dart';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';

import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'cargo_movement_freight_search_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

class CargoMovementFreightSearchPage extends StatefulWidget {
  final CargoMovementFreightSearchVM viewModel;
  const CargoMovementFreightSearchPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<CargoMovementFreightSearchPage> createState() {
    return CargoMovementFreightSearchPageState(viewModel: viewModel);
  }
}

class CargoMovementFreightSearchPageState
    extends State<CargoMovementFreightSearchPage> {
  final CargoMovementFreightSearchVM viewModel;
  final _searchVesselController = TextEditingController();
  final _searchVoyageController = TextEditingController();
  final _selectCraneController = TextEditingController();

  final FocusNode vesselNode = FocusNode();
  final FocusNode voyageNode = FocusNode();

  static const Key _searchVesselKey = Key(CargoKeys.searchVesselKey);
  static const Key _searchVoyageKey = Key(CargoKeys.searchVoyageKey);
  static const Key _selectCraneKey = Key(CargoKeys.selectCraneKey);

  int _selectedCraneIndex = 0;
  int _selectedVesselIndex = 0;
  int _vesselNameLength = 0;

  bool _showCrane = true;

  List<DropdownItem> _craneItems = [];
  List<DropdownItem> _vesselItems = [];

  CargoMovementFreightSearchPageState({
    Key? key,
    required this.viewModel,
  });

  @override
  void initState() {
    super.initState();
    final uiFactory = UiFactory();

    _searchVesselController.text =
        widget.viewModel.uiCargoFreightSearch.vesselName;

    setState(() {
      _vesselNameLength =
          widget.viewModel.uiCargoFreightSearch.vesselName.length;
    });

    if (widget.viewModel.cargoRole == CadiCargoMovementRole.gateIn ||
        widget.viewModel.cargoRole == CadiCargoMovementRole.gateOut) {
      setState(() {
        _showCrane = false;
      });
    }

    _searchVoyageController.text =
        widget.viewModel.uiCargoFreightSearch.voyageNumber;

    var accountPortCranes = widget.viewModel.accountCranes
        .where((element) =>
            element.portId == widget.viewModel.cargoMovementCreate.portId)
        .toList();

    if (accountPortCranes.isNotEmpty) { 
      _craneItems = uiFactory.convertToDropdown<AccountCrane, int>(
          accountPortCranes,
          (carrier) => carrier.name,
          (carrier) => carrier.id);

      _selectedCraneIndex =
          widget.viewModel.uiCargoFreightSearch.selectedCraneIndex;

      _setCrane(_selectedCraneIndex);
    }

    if (widget.viewModel.accountVessels.isNotEmpty) {
      _vesselItems = uiFactory.convertToDropdown<AccountVessel, int>(
          widget.viewModel.accountVessels,
          (vessel) => vessel.vesselName,
          (vessel) => vessel.vesselId);
    }
  }

  @override
  void dispose() {
    _searchVesselController.dispose();
    _searchVoyageController.dispose();
    _selectCraneController.dispose();
    super.dispose();
  }

  void goNext(BuildContext context) {
    var craneId =
        _craneItems.isEmpty ? null : _craneItems[_selectedCraneIndex].value;
    if (_searchVesselController.text.trim() == "") {
      viewModel.showErrorAlert(context, "Please input a vessel name.");
    } else if (_searchVoyageController.text.trim() == "") {
      viewModel.showErrorAlert(context, "Please input a voyage number.");
    } else {
      viewModel.findCargoMovement(context, _searchVesselController.text.trim(),
          _searchVoyageController.text.trim(), craneId);
    }
  }

  void _setCrane(int index) {
    if (index < 0 || index >= _craneItems.length) {
      index = 0; // Reset index if out of range
    }

    setState(() {
      _selectCraneController.text = _craneItems[index].text;
      _selectedCraneIndex = index;
      widget.viewModel.setSelectedCraneIndex(index);
    });
  }

  void _setVessel(int index) {
    setState(() {
      _searchVesselController.text = _vesselItems[index].text.toUpperCase();
      _selectedVesselIndex = index;
      _vesselNameLength = _vesselItems[index].text.toUpperCase().length;
    });
    FocusScope.of(context).requestFocus(voyageNode);
  }

  void _showCraneBottomPicker() {
    if (_craneItems.isEmpty) {
      showOkMessage(context, "Cannot select Crane",
          "Account setup is incomplete. Please contact support. ");
    } else {
      showBottomDropdown(context, 'Crane', 'Select a Crane', _craneItems,
          _selectedCraneIndex, _setCrane, true);
    }
  }

  void _showVesselBottomPicker(String searchParams) {
    if (widget.viewModel.accountVessels.isNotEmpty) {
      var searchList = _vesselItems
          .where((element) =>
              element.text.toUpperCase().startsWith(searchParams.toUpperCase()))
          .toList();
      if (searchList.isNotEmpty) {
        showBottomDropdown(context, 'Vessel', 'Select a Vessel', searchList,
            _selectedVesselIndex, _setVessel, true);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final List<SettingTile> allSettings = <SettingTile>[
      SettingTile(
        0,
        'Active Account',
        Container(
          child: Column(
            children: <Widget>[
              Text(
                  "Cargo Movement: ${getCargoMovementRoleDescription(widget.viewModel.cargoRole)}",
                  style: TextStyle(
                      fontSize: PlatformSizing().normalTextSize(context),
                      color: BrandingColors.cadiBlack))
              // Any additional widget/codes
            ],
          ),
        ),
      ),
      SettingTile(
        1,
        'Account Operations',
        Container(
          margin: const EdgeInsets.only(top: 10.0), // Add your own value here
          child: Column(
            children: <Widget>[
              Text("Search for Vessel",
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      fontWeight: FontWeight.w300,
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),
      SettingTile(
          2,
          'VESSEL Name',
          Material(
            color: BrandingColors.background,
            child: ListTile(
                title: Text('Vessel Name',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack))),
          )),
      SettingTile(
        3,
        "VESSEL Name INPUT",
        PlatformTextField(
          onChanged: (dataString) {
            if (dataString.isNotEmpty &&
                dataString.length > _vesselNameLength) {
              _showVesselBottomPicker(dataString);
            }
            setState(() {
              _vesselNameLength = dataString.length;
            });
          },
          focusNode: vesselNode,
          textInputAction: TextInputAction.next,
          textCapitalization: TextCapitalization.characters,
          controller: _searchVesselController,
          key: _searchVesselKey,
          autocorrect: false,
          material: (_, __) =>
              MaterialAssetsDark().textFieldData("", FontAwesomeIcons.magnifyingGlass),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData('Vessel Name', null),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontWeight: FontWeight.w400,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
          onEditingComplete: () {
            if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
              FocusScope.of(context).requestFocus(voyageNode);
            }
          },
        ),
      ),
      SettingTile(
          4,
          'Voyage Number',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('Voyage Number',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(
        5,
        "Voyage Number",
        PlatformTextField(
          focusNode: voyageNode,
          textCapitalization: TextCapitalization.characters,
          controller: _searchVoyageController,
          key: _searchVoyageKey,
          autocorrect: false,
          material: (_, __) =>
              MaterialAssetsDark().textFieldData("", FontAwesomeIcons.magnifyingGlass),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData('Voyage Number', null),
          style: TextStyle(
              fontWeight: FontWeight.w400,
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
          onEditingComplete: () {
            // if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput &&
            //     widget.viewModel.settingsAutoSubmitPage) {
            //   goNext(context);
            // }
          },
        ),
      ),
      SettingTile(
          6,
          'Crane',
          _showCrane
              ? Material(
                  color: BrandingColors.background,
                  child: ListTile(
                    title: Text('Crane',
                        style: TextStyle(
                            fontWeight: FontWeight.w300,
                            fontSize: PlatformSizing().mediumTextSize(context),
                            color: BrandingColors.cadiBlack)),
                  ))
              : Container()),
      SettingTile(
          7,
          "",
          _showCrane
              ? Material(
                  color: BrandingColors.cadiWhite,
                  child: InkWell(
                    onTap: () {
                      _showCraneBottomPicker();
                    },
                    child: PlatformTextField(
                      textCapitalization: TextCapitalization.characters,
                      controller: _selectCraneController,
                      key: _selectCraneKey,
                      autocorrect: false,
                      enabled: false,
                      material: (_, __) => MaterialAssetsDark()
                          .textFieldData("", FontAwesomeIcons.chevronDown),
                      cupertino: (_, __) => CupertinoAssets()
                          .textFieldData('Crane', FontAwesomeIcons.chevronDown),
                      style: TextStyle(
                          color: BrandingColors.cadiBlack,
                          fontWeight: FontWeight.w400,
                          fontSize:
                              PlatformSizing().inputTextFieldSize(context)),
                      keyboardType: TextInputType.text,
                    ),
                  ))
              : Container()),
      const SettingTile(
          8,
          '',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text(''),
              ))),
      SettingTile(
          9,
          '',
          Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 50.0, vertical: 0.0),
              child: BaseButton(
                  color: widget.viewModel.universalBranding
                      .getButtonPrimaryColor(),
                  child: Text('Next',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: PlatformSizing().normalTextSize(context))),
                  onPressed: () {
                    goNext(context);
                  }))),
    ];

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          itemCount: allSettings.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
  }
}
