import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'cargo_movement_freight_search_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class CargoMovementFreightSearchScreenBuilder extends StatelessWidget {
  const CargoMovementFreightSearchScreenBuilder({super.key});

  static const String route =
      route_helper.cargoMovementFreightSearchScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(
                  // FontAwesomeIcons.warning,
                  context,
                  "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, CargoMovementFreightSearchVM>(
        converter: CargoMovementFreightSearchVM.fromStore,
        builder: (context, vm) {
          return CargoMovementFreightSearchPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class CargoMovementFreightSearchVM {
  bool settingsAutoDisplayNextDropdownOrInput;
  bool settingsAutoSubmitPage;
  bool isLoading;
  AuthState authState;
  Account account;
  String cargoRole;
  UniversalBranding universalBranding;
  List<AccountCrane> accountCranes;
  List<AccountVessel> accountVessels;
  CargoMovementCreate cargoMovementCreate;

  UiCargoFreightSearch uiCargoFreightSearch;
  final Function(BuildContext context, String message) showErrorAlert;
  final Function(BuildContext, String) onNavigatePressed;
  final Function(String token) setToken;
  final Function(int selectedCraneIndex) setSelectedCraneIndex;
  final Function(BuildContext context, String vesselName, String voyageNumber,
      int? craneId) findCargoMovement;
  CargoMovementFreightSearchVM(
      {required this.isLoading,
      required this.settingsAutoDisplayNextDropdownOrInput,
      required this.settingsAutoSubmitPage,
      required this.authState,
      required this.account,
      required this.cargoRole,
      required this.universalBranding,
      required this.accountCranes,
      required this.accountVessels,
      required this.cargoMovementCreate,
      required this.uiCargoFreightSearch,
      required this.onNavigatePressed,
      required this.showErrorAlert,
      required this.setToken,
      required this.setSelectedCraneIndex,
      required this.findCargoMovement});

  static CargoMovementFreightSearchVM fromStore(Store<AppState> store) {
    return CargoMovementFreightSearchVM(
        isLoading: store.state.isLoading,
        settingsAutoDisplayNextDropdownOrInput:
            store.state.uiState.uiDropDownOrInputAutoContinue,
        settingsAutoSubmitPage: store.state.uiState.uiAutoSubmitPage,
        authState: store.state.authState,
        account: store.state.accountState.account,
        cargoRole: store.state.uiState.cargoRole,
        universalBranding:
            UniversalBranding(store.state.accountState.activeService),
        accountCranes: store.state.accountState.filteredAccountCranes,
        accountVessels: store.state.accountState.filteredAccountVessels,
        cargoMovementCreate: store.state.cargoMovementState.cargoMovementCreate,
        uiCargoFreightSearch: store.state.uiState.uiCargoFreightSearch,
        showErrorAlert: (BuildContext context, String message) {
          showOkMessage(
              // FontAwesomeIcons.warning,
              context,
              "Submit Error",
              message);
        },
        onNavigatePressed: (BuildContext context, String route) {
          if (store.state.isLoading) {
            return;
          }
          navigation_helper.navigateRoute(route, store, null);
        },
        setToken: (String token) {},
        setSelectedCraneIndex: (int selectedCraneIndex) {
          store.dispatch(UiCargoHomeFreightSelectCrane(selectedCraneIndex));
        },
        findCargoMovement: (BuildContext context, String vesselName,
            String voyageNumber, int? craneId) {
          store.dispatch(UiCargoHomeFreightSearchSetVesselName(vesselName));
          store.dispatch(UiCargoHomeFreightSearchSetVoyageNumber(
              voyageNumber.replaceAll(' ', '')));
          store.dispatch(SetCargoFreightVariables(
              vesselName, voyageNumber.replaceAll(' ', ''), craneId));
          store.dispatch(NavigateToAction.push(
              route_helper.cargoMovementContainerScreenBuilder));
        });
  }
}
