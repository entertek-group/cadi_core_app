import 'package:cadi_core_app/factory/ui_factory.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/redux/models/account/account_carrier.dart';
import 'package:cadi_core_app/redux/models/account/account_port.dart';
import 'package:cadi_core_app/redux/models/account/account_port_depot.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';
import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'cargo_movement_home_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

class CargoMovementHomePage extends StatefulWidget {
  final CargoMovementHomeVM viewModel;
  const CargoMovementHomePage({
    super.key,
    required this.viewModel,
  });

  @override
  State<CargoMovementHomePage> createState() {
    return CargoMovementHomePageState(viewModel: viewModel);
  }
}

class CargoMovementHomePageState extends State<CargoMovementHomePage> {
  final CargoMovementHomeVM viewModel;
  //final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final _selectAccountPortDepotController = TextEditingController();
  final _selectPortController = TextEditingController();
  final _selectCarrierController = TextEditingController();
  final _selectRoleController = TextEditingController();

  static const Key _selectAccountPortDepotKey =
      Key(CargoKeys.selectAccountPortDepotKey);
  static const Key _selectPortKey = Key(CargoKeys.selectPortKey);
  static const Key _selectCarrierKey = Key(CargoKeys.selectCarrierKey);
  static const Key _selectRoleKey = Key(CargoKeys.selectRoleKey);

  int _selectedLocationIndex = 0;
  int _selectedPortIndex = 0;
  int _selectedCarrierIndex = 0;
  int _selectedRoleIndex = 0;

  List<DropdownItem> _accountLocationItems = [];
  List<DropdownItem> _portItems = [];
  List<DropdownItem> _carrierItems = [];
  List<DropdownItem> _roleItems = [];

  List<AccountPortDepot> _accountPortDepots = [];

  CargoMovementHomePageState({
    Key? key,
    required this.viewModel,
  });

  @override
  initState() {
    super.initState();
    final uiFactory = UiFactory();

    _accountPortDepots = widget.viewModel.accountPortDepots;
    _portItems = uiFactory.convertToDropdown<AccountPort, int>(
        widget.viewModel.accountPorts,
        (port) => port.unLocode,
        (port) => port.portId);

    _carrierItems = uiFactory.convertToDropdown<AccountCarrier, int>(
        widget.viewModel.accountCarriers,
        (carrier) => carrier.carrierName,
        (carrier) => carrier.carrierId);

    _roleItems = uiFactory.convertToDropdown<CadiEnum, String>(
        widget.viewModel.cargoMovementRoles,
        (role) => role.description,
        (role) => role.id);

    widget.viewModel.initScreenData(context);

    _selectedPortIndex = widget.viewModel.uiCargoHome.selectedPortIndex;
    _selectedLocationIndex = widget.viewModel.uiCargoHome.selectedLocationIndex;
    _selectedCarrierIndex = widget.viewModel.uiCargoHome.selectedCarrierIndex;
    _selectedRoleIndex = widget.viewModel.uiCargoHome.selectedRoleIndex;

    if (widget.viewModel.accountPorts.isNotEmpty &&
        _selectedPortIndex < widget.viewModel.accountPorts.length) {
      _selectPortController.text = _portItems[_selectedPortIndex].text;
      _accountPortDepots = widget.viewModel.accountPortDepots
          .where((element) =>
              element.portId == _portItems[_selectedPortIndex].value)
          .toList();

      _accountLocationItems =
          uiFactory.convertToDropdown<AccountPortDepot, int>(
              _accountPortDepots, (item) => item.depotName, (item) => item.id);

      if (_accountLocationItems.isNotEmpty &&
          _selectedLocationIndex < _accountLocationItems.length) {
        _selectAccountPortDepotController.text =
            _accountLocationItems[_selectedLocationIndex].text;
      }
    }

    if (widget.viewModel.accountCarriers.isNotEmpty &&
        _selectedCarrierIndex < widget.viewModel.accountCarriers.length) {
      _selectCarrierController.text = _carrierItems[_selectedCarrierIndex].text;
    }

    if (widget.viewModel.cargoMovementRoles.isNotEmpty &&
        _selectedRoleIndex < widget.viewModel.cargoMovementRoles.length) {
      _selectRoleController.text = _roleItems[_selectedRoleIndex].text;
    }
  }

  void _setPort(int index) {
    setState(() {
      _selectPortController.text = _portItems[index].text;
      _selectedPortIndex = index;
      widget.viewModel.setSelectedPortIndex(index);
      widget.viewModel.setSelectedCraneIndex(0); // Reset crane index
      _accountPortDepots = widget.viewModel.accountPortDepots
          .where((element) =>
              element.portId == _portItems[_selectedPortIndex].value)
          .toList();
      _accountLocationItems = UiFactory()
          .convertToDropdown<AccountPortDepot, int>(
              _accountPortDepots, (item) => item.depotName, (item) => item.id);

      _selectedLocationIndex = 0;
    });
    if (_accountLocationItems.isNotEmpty &&
        _selectedLocationIndex < _accountLocationItems.length) {
      _selectAccountPortDepotController.text =
          _accountLocationItems[_selectedLocationIndex].text;
    }

    if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
      Future.delayed(
          const Duration(milliseconds: 300), _showLocationBottomPicker);
    }
  }

  void _setLocation(int index) {
    setState(() {
      _selectAccountPortDepotController.text =
          _accountLocationItems[index].text;
      _selectedLocationIndex = index;
      widget.viewModel.setSelectedLocationIndex(index);
    });

    if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
      Future.delayed(
          const Duration(milliseconds: 300), _showCarrierBottomPicker);
    }
  }

  void _setCarrier(int index) {
    setState(() {
      _selectCarrierController.text = _carrierItems[index].text;
      _selectedCarrierIndex = index;
      widget.viewModel.setSelectedCarrierIndex(index);
    });

    if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
      Future.delayed(const Duration(milliseconds: 300), _showRoleBottomPicker);
    }
  }

  void _setRole(int index) {
    setState(() {
      _selectRoleController.text = _roleItems[index].text;
      _selectedRoleIndex = index;
      widget.viewModel.setSelectedRoleIndex(index);
    });

    if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput &&
        widget.viewModel.settingsAutoSubmitPage) {
      Future.delayed(const Duration(milliseconds: 300), () => goNext(context));
    }
  }

  void _showPortBottomPicker() {
    if (widget.viewModel.accountPorts.isEmpty) {
      showOkMessage(context, "Cannot select Port",
          "Account setup is incomplete. Please contact support. ");
    } else {
      showBottomDropdown(context, 'Port', 'Please select a port', _portItems,
          _selectedPortIndex, _setPort, true);
    }
  }

  void _showLocationBottomPicker() {
    if (widget.viewModel.accountPortDepots.isEmpty) {
      showOkMessage(context, "Cannot select Location",
          "Account setup is incomplete. Please contact support. ");
    } else {
      showBottomDropdown(context, 'Location', 'Select a location',
          _accountLocationItems, _selectedLocationIndex, _setLocation, true);
    }
  }

  void _showCarrierBottomPicker() {
    if (widget.viewModel.accountCarriers.isEmpty) {
      showOkMessage(context, "Cannot select Carrier",
          "Account setup is incomplete. Please contact support. ");
    } else {
      showBottomDropdown(context, 'Carrier', 'Select a Carrier', _carrierItems,
          _selectedCarrierIndex, _setCarrier, true);
    }
  }

  void _showRoleBottomPicker() {
    if (widget.viewModel.cargoMovementRoles.isEmpty) {
      showOkMessage(
          // FontAwesomeIcons.warning,
          context,
          "Cannot select Role",
          "Account setup is incomplete. Please contact support. ");
    } else {
      showBottomDropdown(context, 'Role', 'Select a Role', _roleItems,
          _selectedRoleIndex, _setRole, true);
    }
  }

  @override
  void dispose() {
    _selectAccountPortDepotController.dispose();
    _selectPortController.dispose();
    _selectCarrierController.dispose();
    _selectRoleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final List<SettingTile> allSettings = <SettingTile>[
      SettingTile(
        0,
        'Active Account',
        Container(
          child: Column(
            children: <Widget>[
              Text("Cargo Movement",
                  style: TextStyle(
                      fontSize: PlatformSizing().normalTextSize(context),
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),
      SettingTile(
        1,
        'Account Operations',
        Container(
          margin: const EdgeInsets.only(top: 10.0), // Add your own value here
          child: Column(
            children: <Widget>[
              Text("Select Location, Carrier and Roles",
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      fontWeight: FontWeight.w300,
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),
      SettingTile(
          2,
          'PORT',
          ListTile(
            title: Text("Port",
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          )),
      SettingTile(
          3,
          "",
          InkWell(
            onTap: () {
              _showPortBottomPicker();
            },
            child: PlatformTextField(
              textCapitalization: TextCapitalization.characters,
              controller: _selectPortController,
              key: _selectPortKey,
              autocorrect: false,
              enabled: false,
              material: (_, __) => MaterialAssetsDark()
                  .textFieldData("", FontAwesomeIcons.chevronDown),
              cupertino: (_, __) => CupertinoAssets()
                  .textFieldData('Port', FontAwesomeIcons.chevronDown),
              style: TextStyle(
                  color: BrandingColors.cadiBlack,
                  fontWeight: FontWeight.w400,
                  fontSize: PlatformSizing().inputTextFieldSize(context)),
              // keyboardType: TextInputType.text,
            ),
          )),
      SettingTile(
          4,
          'LOCATION',
          ListTile(
            title: Text('Location',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          )),
      SettingTile(
          5,
          "",
          InkWell(
            onTap: () {
              _showLocationBottomPicker();
            },
            child: PlatformTextField(
              textCapitalization: TextCapitalization.characters,
              controller: _selectAccountPortDepotController,
              key: _selectAccountPortDepotKey,
              autocorrect: false,
              enabled: false,
              material: (_, __) => MaterialAssetsDark()
                  .textFieldData("", FontAwesomeIcons.chevronDown),
              cupertino: (_, __) => CupertinoAssets()
                  .textFieldData('Location', FontAwesomeIcons.chevronDown),
              style: TextStyle(
                  color: BrandingColors.cadiBlack,
                  fontWeight: FontWeight.normal,
                  fontSize: PlatformSizing().inputTextFieldSize(context)),
              keyboardType: TextInputType.text,
            ),
          )),
      SettingTile(
          6,
          'CARRIER',
          ListTile(
            title: Text('Carrier',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          )),
      SettingTile(
          7,
          "",
          InkWell(
            onTap: () {
              _showCarrierBottomPicker();
            },
            child: PlatformTextField(
              textCapitalization: TextCapitalization.characters,
              controller: _selectCarrierController,
              key: _selectCarrierKey,
              autocorrect: false,
              enabled: false,
              material: (_, __) => MaterialAssetsDark()
                  .textFieldData("", FontAwesomeIcons.chevronDown),
              cupertino: (_, __) => CupertinoAssets()
                  .textFieldData('Carrier', FontAwesomeIcons.chevronDown),
              style: TextStyle(
                  color: BrandingColors.cadiBlack,
                  fontWeight: FontWeight.w400,
                  fontSize: PlatformSizing().inputTextFieldSize(context)),
              keyboardType: TextInputType.text,
            ),
          )),
      SettingTile(
          8,
          'ROLE',
          ListTile(
            title: Text('Role',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          )),
      SettingTile(
          9,
          "",
          InkWell(
            onTap: () {
              _showRoleBottomPicker();
            },
            child: PlatformTextField(
              textCapitalization: TextCapitalization.characters,
              controller: _selectRoleController,
              key: _selectRoleKey,
              autocorrect: false,
              enabled: false,
              readOnly: true,
              material: (_, __) => MaterialAssetsDark()
                  .textFieldData("", FontAwesomeIcons.chevronDown),
              cupertino: (_, __) => CupertinoAssets()
                  .textFieldData('Role', FontAwesomeIcons.chevronDown),
              style: TextStyle(
                  color: BrandingColors.cadiBlack,
                  fontWeight: FontWeight.w400,
                  fontSize: PlatformSizing().inputTextFieldSize(context)),
              keyboardType: TextInputType.text,
            ),
          )),
      const SettingTile(
          10,
          '',
          ListTile(
            title: Text(
              '',
            ),
          )),
      SettingTile(
          11,
          '',
          Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 50.0, vertical: 0.0),
              child: BaseButton(
                  color: widget.viewModel.universalBranding
                      .getButtonPrimaryColor(),
                  child: Text('Go',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: PlatformSizing().normalTextSize(context))),
                  onPressed: () {
                    goNext(context);
                  }))),
    ];

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.only(top: 0.0, left: 40, right: 40),
      child: ListView.builder(
          shrinkWrap: true,
          //itemBuilder: itemBuilder) .separated(
          itemCount: allSettings.length,
          padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 0),
          // separatorBuilder: (BuildContext context, int index) =>
          //     Divider(height: 1),
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
  }

  void goNext(BuildContext context) {
    viewModel.onNavigatePressed(
        context,
        _roleItems[_selectedRoleIndex].value,
        _accountLocationItems[_selectedLocationIndex].value,
        _carrierItems[_selectedCarrierIndex].value,
        _portItems[_selectedPortIndex].value);
  }
}
