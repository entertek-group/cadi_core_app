import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'cargo_movement_home_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

class CargoMovementHomeScreenBuilder extends StatelessWidget {
  const CargoMovementHomeScreenBuilder({super.key});

  static const String route = route_helper.cargoMovementHomeScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: BrandingColors.cadiWhite,
      body: StoreConnector<AppState, CargoMovementHomeVM>(
        converter: CargoMovementHomeVM.fromStore,
        builder: (context, vm) {
          return CargoMovementHomePage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class CargoMovementHomeVM {
  bool isLoading;
  bool settingsAutoDisplayNextDropdownOrInput;
  bool settingsAutoSubmitPage;
  AuthState authState;
  Account account;
  UniversalBranding universalBranding;
  List<AccountCarrier> accountCarriers;
  List<CadiEnum> cargoMovementRoles;
  List<AccountPortDepot> accountPortDepots;
  List<AccountPort> accountPorts;
  UiCargoHome uiCargoHome;

  final Function(BuildContext, String, int, int, int) onNavigatePressed;
  final Function(int selectedPortIndex) setSelectedPortIndex;
  final Function(int selectedLocationIndex) setSelectedLocationIndex;
  final Function(int selectedCarrierIndex) setSelectedCarrierIndex;
  final Function(int selectedRoleIndex) setSelectedRoleIndex;
  final Function(BuildContext context) initScreenData;
  final Function(int selectedCraneIndex) setSelectedCraneIndex;

  CargoMovementHomeVM(
      {required this.isLoading,
      required this.settingsAutoDisplayNextDropdownOrInput,
      required this.settingsAutoSubmitPage,
      required this.authState,
      required this.account,
      required this.universalBranding,
      required this.accountCarriers,
      required this.cargoMovementRoles,
      required this.accountPortDepots,
      required this.setSelectedCraneIndex,
      required this.accountPorts,
      required this.uiCargoHome,
      required this.onNavigatePressed,
      required this.setSelectedPortIndex,
      required this.setSelectedLocationIndex,
      required this.setSelectedCarrierIndex,
      required this.setSelectedRoleIndex,
      required this.initScreenData});

  static CargoMovementHomeVM fromStore(Store<AppState> store) {
    return CargoMovementHomeVM(
      isLoading: store.state.isLoading,
      settingsAutoDisplayNextDropdownOrInput:
          store.state.uiState.uiDropDownOrInputAutoContinue,
      settingsAutoSubmitPage: store.state.uiState.uiAutoSubmitPage,
      authState: store.state.authState,
      account: store.state.accountState.account,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      accountCarriers: store.state.accountState.filteredAccountCarriers,
      cargoMovementRoles: store.state.referenceState.cargoMovementRoles,
      accountPortDepots: store.state.accountState.filteredAccountPortDepots,
      accountPorts: store.state.accountState.filteredAccountPorts,
      uiCargoHome: store.state.uiState.uiCargoHome,
      initScreenData: (context) {
        store.dispatch(ResetCargoMovementCreate());
      },
      onNavigatePressed: (BuildContext context, String role,
          int accountPortDepotId, int carrierId, int portId) {
        if (store.state.isLoading) {
          return;
        }

        store.dispatch(UiCargoSetRole(role));
        store.dispatch(SetAccountPortDepot(accountPortDepotId));
        store.dispatch(
            SetCargoHomeVariables(accountPortDepotId, carrierId, role, portId));

        store.dispatch(NavigateToAction.push(
            route_helper.cargoMovementFreightSearchScreenBuilder));
      },
      setSelectedPortIndex: (int selectedPortIndex) {
        store.dispatch(UiCargoHomeSetPortIndex(selectedPortIndex));
      },
      setSelectedLocationIndex: (int selectedLocationIndex) {
        store.dispatch(UiCargoHomeSetLocationIndex(selectedLocationIndex));
      },
      setSelectedCraneIndex: (int selectedCraneIndex) {
        store.dispatch(UiCargoHomeFreightSelectCrane(selectedCraneIndex));
      },
      setSelectedCarrierIndex: (int selectedCarrierIndex) {
        store.dispatch(UiCargoHomeSetCarrierIndex(selectedCarrierIndex));
      },
      setSelectedRoleIndex: (int selectedRoleIndex) {
        store.dispatch(UiCargoHomeSetRoleIndex(selectedRoleIndex));
      },
    );
  }
}
