import 'dart:io';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/image/image_create.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'cargo_movement_status_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class CargoMovementStatusScreenBuilder extends StatelessWidget {
  const CargoMovementStatusScreenBuilder({super.key});

  static const String route = route_helper.cargoMovementStatusScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(
                  // FontAwesomeIcons.warning,
                  context,
                  "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, CargoMovementStatusVM>(
        converter: CargoMovementStatusVM.fromStore,
        builder: (context, vm) {
          return CargoMovementStatusPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class CargoMovementStatusVM {
  bool isLoading;
  AuthState authState;
  Account account;
  String cargoRole;
  CargoMovementCreate cargoMovementCreate;
  UniversalBranding universalBranding;

  FileStore tempStorage;
  List<CadiEnum> cargoMovementStates;
  final Function(BuildContext, String) onNavigatePressed;
  final Function(String token) setToken;
  final Function(BuildContext context, String message) showErrorAlert;
  final Function(BuildContext context, String status, String inputComment,
      String inputRestow, List<ImageCreate>? attachedImages, String? eir) selectStatus;
  final Function(File file) saveTemporaryImage;
  CargoMovementStatusVM(
      {required this.isLoading,
      required this.authState,
      required this.account,
      required this.cargoRole,
      required this.cargoMovementCreate,
      required this.universalBranding,
      required this.cargoMovementStates,
      required this.onNavigatePressed,
      required this.setToken,
      required this.showErrorAlert,
      required this.selectStatus,
      required this.saveTemporaryImage,
      required this.tempStorage,
      });

  static CargoMovementStatusVM fromStore(Store<AppState> store) {
    return CargoMovementStatusVM(
        isLoading: store.state.isLoading,
        authState: store.state.authState,
        account: store.state.accountState.account,
        cargoRole: store.state.uiState.cargoRole,
        cargoMovementCreate: store.state.cargoMovementState.cargoMovementCreate,
        universalBranding:
            UniversalBranding(store.state.accountState.activeService),
        cargoMovementStates: store.state.referenceState.cargoMovementStates,
        tempStorage: store.state.storageState.tempStorage,
        saveTemporaryImage: (File file) {
          store.dispatch(StoreTemporaryFile(file, StorageType.cargoMovement));
        },
        onNavigatePressed: (BuildContext context, String route) {
          if (store.state.isLoading) {
            return;
          }
          navigation_helper.navigateRoute(route, store, null);
        },
        setToken: (String token) {},
        showErrorAlert: (BuildContext context, String message) {
          showOkMessage(
              // FontAwesomeIcons.warning,
              context,
              "Submit Error",
              message);
        },
        selectStatus: (BuildContext context, String status, String inputComment,
            String inputRestow, List<ImageCreate>? attachedImages, String? eir) {
          store.dispatch(SetCargoStatusVariables(
              status, inputComment, inputRestow, attachedImages, eir));
          var role = store.state.uiState.cargoRole;
          if (role == CadiCargoMovementRole.freightLoad ||
              role == CadiCargoMovementRole.freightUnload) {
            store.dispatch(NavigateToAction.push(
                route_helper.cargoMovementSummaryScreenBuilder));
          } else {
            store.dispatch(UiVehicleDriverSetIndex(-1));
            store.dispatch(NavigateToAction.push(
                //Replace truck screen
                route_helper.cargoMovementEirGenerationScreenBuilder));
          }
        });
  }
}