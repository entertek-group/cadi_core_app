import 'dart:core';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/app/error_snack.dart';
import 'package:cadi_core_app/ui/shared/components/app/setting_tile.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/date_picker.dart';
import 'package:cadi_core_app/ui/shared/components/input.dart';
import 'package:cadi_core_app/ui/shared/components/popup_menu.dart';
import 'package:cadi_core_app/ui/shared/components/time_picker.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'cargo_movement_summary_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/helpers/helpers.dart';
import 'package:intl/intl.dart';

class CargoMovementSummaryPage extends StatefulWidget {
  final CargoMovementSummaryVM viewModel;
  const CargoMovementSummaryPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<CargoMovementSummaryPage> createState() {
    return CargoMovementSummaryPageState(viewModel: viewModel);
  }
}

class CargoMovementSummaryPageState extends State<CargoMovementSummaryPage> {
  final CargoMovementSummaryVM viewModel;
  String movementStatus = "";
  String movementType = "";
  var containerWeightUnitList = <PopMenuItem>[];
  var packWeightUnitList = <PopMenuItem>[];
  final TextEditingController _textEditingController = TextEditingController();

  DateTime _selectedDateTime = DateTime.now();
  late String dateTime;
  late String time;

  CargoMovementSummaryPageState({
    Key? key,
    required this.viewModel,
  });

  @override
  initState() {
    super.initState();
    for (final weightUnit in widget.viewModel.weightUnits) {
      var containerItem = PopMenuItem(weightUnit.description,
          () => viewModel.updateContainerWeightUnit(weightUnit.id));
      containerWeightUnitList.add(containerItem);
      var packItem = PopMenuItem(weightUnit.description,
          () => viewModel.updatePackWeightUnit(weightUnit.id));
      packWeightUnitList.add(packItem);
    }

    if (widget.viewModel.cargoMovementCreate.movementAt != "") {
      _selectedDateTime =
          DateTime.parse(widget.viewModel.cargoMovementCreate.movementAt);
    } else {
      _selectedDateTime = DateTime.now();
    }

    dateTime = DateFormat.yMMMEd().format(_selectedDateTime).toString();
    time = DateFormat('HH:mm:ss').format(_selectedDateTime);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> cargoSummary = <Widget>[];
    if (widget.viewModel.cargoMovementCreate.shipment ==
        CadiCargoMovementShipment.breakBulk) {
      cargoSummary.add(buildSummaryRow(context, "Vessel",
          widget.viewModel.cargoMovementCreate.vesselName, false, null));
      cargoSummary.add(buildSummaryRow(context, "Voyage",
          widget.viewModel.cargoMovementCreate.voyageNumber, false, null));
      cargoSummary.add(buildSummaryRow(
          context,
          "Type",
          getShipmentTypeDisplayFormat(
              widget.viewModel.cargoMovementCreate.shipment),
          false,
          null));
      cargoSummary.add(buildSummaryRow(
          context,
          "Weight",
          widget.viewModel.cargoMovementCreate.packData.packWeight.toString(),
          true, () {
        _textEditingController.text =
            widget.viewModel.cargoMovementCreate.packData.packWeight.toString();
        showMessageWithInputActions(
            context,
            "Amend Container",
            "Enter new container weight",
            () {
              widget.viewModel.updatePackWeight(_textEditingController.text);
              Navigator.pop(context);
            },
            "Save",
            () {
              Navigator.pop(context);
            },
            "Cancel",
            inputWidget(context, _textEditingController, "", true));
      }));
      cargoSummary.add(buildSummaryRow(
          context,
          "Unit",
          widget.viewModel.cargoMovementCreate.packData.packWeightUnit,
          true, () {
        showPopupMenu(
            context,
            "Weight Unit: ${widget.viewModel.cargoMovementCreate.packData.packWeightUnit}",
            "Select a Unit",
            packWeightUnitList);
      }));
      cargoSummary.add(buildSummaryRow(
          context,
          "Pack Sequence #",
          widget.viewModel.cargoMovementCreate.packData.packSequenceNumber,
          false,
          null));
      cargoSummary.add(buildSummaryRow(context, "", "", false, null));
      cargoSummary.add(buildSummaryRow(context, "Status",
          widget.viewModel.cargoMovementCreate.state, false, () => {}));
      cargoSummary.add(buildSummaryRow(context, "Comments",
          widget.viewModel.cargoMovementCreate.comment ?? "", false, null));

      if (widget.viewModel.cargoRole == CadiCargoMovementRole.gateIn ||
          widget.viewModel.cargoRole == CadiCargoMovementRole.gateOut) {
        cargoSummary.add(buildSummaryRow(context, "", "", false, null));
        cargoSummary.add(buildSummaryRow(
            context,
            "Truck Reg",
            widget.viewModel.cargoMovementCreate.truckRegistration ?? "",
            false,
            null));
        cargoSummary.add(buildSummaryRow(
            context,
            "Drivers ID",
            widget.viewModel.cargoMovementCreate.truckDriverId ?? "",
            false,
            null));
        cargoSummary.add(buildSummaryRow(
            context,
            "Truck Company",
            widget.viewModel.cargoMovementCreate.truckCompany ?? "",
            false,
            null));
        cargoSummary.add(buildSummaryRow(context, "", "", false, null));
        cargoSummary.add(buildSummaryRow(
            context,
            "EIR #",
            widget.viewModel.cargoMovementCreate.eir ?? "EIR # Generated",
            false,
            null));
        cargoSummary.add(buildSummaryRow(
            context,
            "Signatory",
            widget.viewModel.cargoMovementCreate.signatoryFullName ?? "",
            false,
            null));
        cargoSummary.add(buildSummaryRow(
            context,
            "Docket #",
            widget.viewModel.cargoMovementCreate.deliveryDocketNumber ?? "",
            false,
            null));
        if (widget.viewModel.cargoRole == CadiCargoMovementRole.gateIn) {
          cargoSummary.add(buildSummaryRow(
              context,
              "POL",
              widget.viewModel.cargoMovementCreate.portOfLoadOrDischarge ?? "",
              false,
              null));
        } else {
          cargoSummary.add(buildSummaryRow(
              context,
              "POD",
              widget.viewModel.cargoMovementCreate.portOfLoadOrDischarge ?? "",
              false,
              null));
        }
      }

      cargoSummary.add(buildSummaryRow(context, "", "", false, null));
      cargoSummary.add(buildSummaryRow(context, "Port",
          widget.viewModel.accountPortDepot.unLocode, false, null));
      cargoSummary.add(buildSummaryRow(context, "Location",
          widget.viewModel.accountPortDepot.depotName, false, null));
      cargoSummary.add(buildSummaryRow(context, "Carrier",
          widget.viewModel.accountCarrier.carrierName, false, null));

      if (widget.viewModel.cargoRole != CadiCargoMovementRole.gateIn &&
          widget.viewModel.cargoRole != CadiCargoMovementRole.gateOut) {
        cargoSummary.add(buildSummaryRow(context, "Crane",
            widget.viewModel.accountCrane?.name ?? "", false, null));
      }

      cargoSummary.add(
          buildSummaryRow(context, "Date", dateTime, true, _showDatePicker));
      cargoSummary.add(
          buildSummaryRow(context, "Time", time, true, _showDateTimePicker));
      cargoSummary.add(buildSummaryRow(
          context, "Profile", widget.viewModel.account.name, false, null));
    } else {
      cargoSummary.add(buildSummaryRow(context, "Vessel",
          widget.viewModel.cargoMovementCreate.vesselName, false, null));
      cargoSummary.add(buildSummaryRow(context, "Voyage",
          widget.viewModel.cargoMovementCreate.voyageNumber, false, null));
      cargoSummary.add(buildSummaryRow(
          context,
          "Container #",
          widget.viewModel.cargoMovementCreate.containerData.containerNumber,
          true, () {
        _textEditingController.text =
            widget.viewModel.cargoMovementCreate.containerData.containerNumber;
        showMessageWithInputActions(
            context,
            "Amend Container",
            "Enter new container number",
            () {
              if (isContainerInputValid(
                  context, _textEditingController.text, true)) {
                widget.viewModel
                    .updateContainerNumber(_textEditingController.text);
              }
              Navigator.pop(context);
            },
            "Save",
            () {
              Navigator.pop(context);
            },
            "Cancel",
            inputWidget(context, _textEditingController, "", false));
      }));

      if (widget.viewModel.cargoRole == CadiCargoMovementRole.freightUnload ||
          widget.viewModel.cargoRole == CadiCargoMovementRole.freightLoad) {
        cargoSummary.add(buildSummaryRow(
            context,
            "Stowage #",
            widget.viewModel.cargoMovementCreate.stowagePosition ?? "",
            true, () {
          _textEditingController.text =
              widget.viewModel.cargoMovementCreate.stowagePosition ?? "";
          showMessageWithInputActions(
              context,
              "Amend Stowage Position",
              "Enter new Stowage Position",
              () {
                if (_textEditingController.text.trim() == "") {
                  errorSnack(context, 'Please input a Stowage Position number');
                } else if (_textEditingController.text.length > 6) {
                  errorSnack(context, 'Max of 6 characters allowed');
                } else {
                  widget.viewModel
                      .updateStowagePosition(_textEditingController.text);
                }
                Navigator.pop(context);
              },
              "Save",
              () {
                Navigator.pop(context);
              },
              "Cancel",
              inputWidget(context, _textEditingController, "", true));
        }));
      }

      cargoSummary.add(buildSummaryRow(
          context,
          "Type",
          getShipmentTypeDisplayFormat(
              widget.viewModel.cargoMovementCreate.shipment),
          false,
          null));
      cargoSummary.add(buildSummaryRow(
          context,
          "Weight",
          widget.viewModel.cargoMovementCreate.containerData.containerWeight
              .toString(),
          true, () {
        _textEditingController.text = widget
            .viewModel.cargoMovementCreate.containerData.containerWeight
            .toString();
        showMessageWithInputActions(
            context,
            "Amend Container",
            "Enter new container weight",
            () {
              widget.viewModel
                  .updateContainerWeight(_textEditingController.text);
              Navigator.pop(context);
            },
            "Save",
            () {
              Navigator.pop(context);
            },
            "Cancel",
            inputWidget(context, _textEditingController, "", true));
      }));

      cargoSummary.add(buildSummaryRow(
          context,
          "Unit",
          widget
              .viewModel.cargoMovementCreate.containerData.containerWeightUnit,
          true, () {
        showPopupMenu(
            context,
            "Weight Unit: ${widget.viewModel.cargoMovementCreate.containerData.containerWeightUnit}",
            "Select a Unit",
            containerWeightUnitList);
      }));
      cargoSummary.add(buildSummaryRow(
          context,
          "Seal #",
          widget.viewModel.cargoMovementCreate.containerData.sealNumber1,
          true, () {
        _textEditingController.text =
            widget.viewModel.cargoMovementCreate.containerData.sealNumber1;
        showMessageWithInputActions(
            context,
            "Amend Seal",
            "Enter correct Seal Number",
            () {
              widget.viewModel.updateSealNumber1(_textEditingController.text);
              Navigator.pop(context);
            },
            "Update",
            () {
              Navigator.pop(context);
            },
            "Cancel",
            inputWidget(context, _textEditingController, "", false));
      }));

      cargoSummary.add(buildSummaryRow(context, "", "", false, null));
      cargoSummary.add(buildSummaryRow(context, "Status",
          widget.viewModel.cargoMovementCreate.state, false, null));
      cargoSummary.add(buildSummaryRow(context, "Comments",
          widget.viewModel.cargoMovementCreate.comment ?? "", false, null));
      if (widget.viewModel.cargoRole == CadiCargoMovementRole.gateIn ||
          widget.viewModel.cargoRole == CadiCargoMovementRole.gateOut) {
        cargoSummary.add(buildSummaryRow(context, "", "", false, null));
        cargoSummary.add(buildSummaryRow(
            context,
            "Truck Reg",
            widget.viewModel.cargoMovementCreate.truckRegistration ?? "",
            false,
            null));
        cargoSummary.add(buildSummaryRow(
            context,
            "Drivers ID",
            widget.viewModel.cargoMovementCreate.truckDriverId ?? "",
            false,
            null));
        cargoSummary.add(buildSummaryRow(
            context,
            "Truck Company",
            widget.viewModel.cargoMovementCreate.truckCompany ?? "",
            false,
            null));
        cargoSummary.add(buildSummaryRow(context, "", "", false, null));
        cargoSummary.add(buildSummaryRow(
            context,
            "EIR #",
            widget.viewModel.cargoMovementCreate.eir ?? "EIR # Generated",
            false,
            null));
        cargoSummary.add(buildSummaryRow(
            context,
            "Signatory",
            widget.viewModel.cargoMovementCreate.signatoryFullName ?? "",
            false,
            null));
        cargoSummary.add(buildSummaryRow(
            context,
            "Docket #",
            widget.viewModel.cargoMovementCreate.deliveryDocketNumber ?? "",
            false,
            null));
        if (widget.viewModel.cargoRole == CadiCargoMovementRole.gateIn) {
          cargoSummary.add(buildSummaryRow(
              context,
              "POL",
              widget.viewModel.cargoMovementCreate.portOfLoadOrDischarge ?? "",
              false,
              null));
        } else {
          cargoSummary.add(buildSummaryRow(
              context,
              "POD",
              widget.viewModel.cargoMovementCreate.portOfLoadOrDischarge ?? "",
              false,
              null));
        }
        cargoSummary.add(buildSummaryRow(context, "", "", false, null));
      }
      cargoSummary.add(buildSummaryRow(context, "Port",
          widget.viewModel.accountPortDepot.unLocode, false, null));
      cargoSummary.add(buildSummaryRow(context, "Location",
          widget.viewModel.accountPortDepot.depotName, false, null));
      cargoSummary.add(buildSummaryRow(context, "Carrier",
          widget.viewModel.accountCarrier.carrierName, false, null));

      if (widget.viewModel.cargoRole != CadiCargoMovementRole.gateIn &&
          widget.viewModel.cargoRole != CadiCargoMovementRole.gateOut) {
        cargoSummary.add(buildSummaryRow(context, "Crane",
            widget.viewModel.accountCrane?.name ?? "", false, null));
      }

      cargoSummary.add(
          buildSummaryRow(context, "Date", dateTime, true, _showDatePicker));
      cargoSummary.add(
          buildSummaryRow(context, "Time", time, true, _showDateTimePicker));
      cargoSummary.add(buildSummaryRow(
          context, "Profile", widget.viewModel.account.name, false, null));
    }

    List<SettingTile> allOptions = <SettingTile>[
      SettingTile(
        0,
        'Port Movement',
        Column(
          children: <Widget>[
            Text(
                "Cargo Movement: ${getCargoMovementRoleDescription(widget.viewModel.cargoRole)}",
                style: TextStyle(
                    fontSize: PlatformSizing().normalTextSize(context),
                    color: BrandingColors.cadiBlack)),
          ],
        ),
      ),
      SettingTile(
        1,
        'Account Operations',
        Container(
          margin: const EdgeInsets.only(top: 10.0, bottom: 15),
          child: Column(
            children: <Widget>[
              Text("Summary: Check, Update and Submit",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      color: BrandingColors.cadiBlack)),
            ],
          ),
        ),
      ),
      SettingTile(2, 'Summary', buildSummaryCard(context, cargoSummary)),
    ];

    allOptions.add(SettingTile(
        7,
        '',
        Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 50.0, vertical: 10.0),
            child: BaseButton(
                color:
                    widget.viewModel.universalBranding.getButtonPrimaryColor(),
                child: Text('Submit',
                    style: TextStyle(
                        fontSize: PlatformSizing().normalTextSize(context))),
                onPressed: () {
                  viewModel.confirmCargoMovement(context);
                }))));

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          physics: const ClampingScrollPhysics(),
          itemCount: allOptions.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          itemBuilder: (BuildContext context, int index) {
            return allOptions[index].body;
          }),
    );
  }

  Widget buildSummaryCard(BuildContext context, List<Widget> cargoSummary) {
    return Card(
        elevation: 2.0,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          side: const BorderSide(color: BrandingColors.cadiGrey, width: 0.5),
          borderRadius: BorderRadius.circular(5),
        ),
        margin: const EdgeInsets.only(left: 5, right: 5, bottom: 20),
        child: ListView.builder(
            shrinkWrap: true,
            physics: const ClampingScrollPhysics(),
            padding: const EdgeInsets.all(15),
            itemCount: cargoSummary.length,
            itemBuilder: (BuildContext context, int index) {
              return cargoSummary[index];
            }));
  }

  Widget buildSummaryRow(BuildContext context, String textDisplay,
      String textValue, bool hasAction, Function? buttonAction) {
    return ListTile(
        dense: true,
        visualDensity: const VisualDensity(horizontal: 0, vertical: -2),
        minVerticalPadding: 4,
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 0.0, vertical: 2),
        tileColor: Colors.transparent,
        leading: Text(
          textDisplay,
          style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: PlatformSizing().smallTextSize(context),
              color: BrandingColors.cadiBlack),
        ),
        title: Text(
          textValue,
          style: TextStyle(
              fontSize: PlatformSizing().smallTextSize(context),
              color: BrandingColors.cadiBlack),
          textAlign: TextAlign.end,
        ),
        trailing: hasAction
            ? IconButton(
                padding: const EdgeInsets.only(bottom: 10, left: 25),
                icon: const Icon(FontAwesomeIcons.penToSquare,
                    color: BrandingColors.cadiBlack),
                onPressed: buttonAction != null ? () => buttonAction() : () {})
            : const IconButton(
                icon: Icon(Icons.edit_outlined, color: Colors.transparent),
                onPressed: null));
  }

  void _showDateTimePicker() {
    final today = DateTime.now();
    final sevenDaysAgo = today.subtract(const Duration(days: 7));

    showPlatformTimePicker(
      context: context,
      initialTime: TimeOfDay.fromDateTime(_selectedDateTime),
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: const ColorScheme.light(
              primary: Color(0xFF43A047),
              onPrimary: Colors.white,
              surface: Color(0xFFFFFFFF),
              onSurface: Color(0xFF000000),
            ),
            dialogBackgroundColor: Colors.white,
          ),
          child: child!,
        );
      },
    ).then((time) {
      if (time != null) {
        var selectedDateTime = DateTime(
          _selectedDateTime.year,
          _selectedDateTime.month,
          _selectedDateTime.day,
          time.hour,
          time.minute,
          _selectedDateTime.second,
          _selectedDateTime.millisecond,
        );

        if (selectedDateTime.isBefore(sevenDaysAgo)) {
          selectedDateTime = sevenDaysAgo;
        }

        if (selectedDateTime.isAfter(today)) {
          selectedDateTime = today;
        }

        _selectedDateTime = selectedDateTime;

        final offset = _selectedDateTime.timeZoneOffset;
        final offsetSign = offset.isNegative ? '-' : '+';
        final offsetHours = offset.inHours.abs().toString().padLeft(2, '0');
        final offsetMinutes =
            (offset.inMinutes % 60).abs().toString().padLeft(2, '0');

        final formattedDateTime =
            "${_selectedDateTime.toIso8601String().split('Z')[0]}$offsetSign$offsetHours:$offsetMinutes";

        widget.viewModel.updateMovementAt(formattedDateTime);
        setState(() {
          dateTime = DateFormat.yMMMEd().format(_selectedDateTime).toString();
          this.time = DateFormat('HH:mm:ss').format(_selectedDateTime);
        });
      }
    });
  }

  void _showDatePicker() {
    final today = DateTime.now();
    final sevenDaysAgo = today.subtract(const Duration(days: 7));

    showPlatformDatePicker(
      context: context,
      initialDate: _selectedDateTime.isBefore(sevenDaysAgo)
          ? sevenDaysAgo
          : (_selectedDateTime.isAfter(today) ? today : _selectedDateTime),
      firstDate: sevenDaysAgo,
      lastDate: today,
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: const ColorScheme.light(
              primary: Color(0xFF43A047),
              onPrimary: Colors.white,
              surface: Color(0xFFFFFFFF),
              onSurface: Color(0xFF000000),
            ),
            dialogBackgroundColor: Colors.white,
          ),
          child: child!,
        );
      },
    ).then((selectedDate) {
      if (selectedDate != null) {
        var selectedDateTime = DateTime(
          selectedDate.year,
          selectedDate.month,
          selectedDate.day,
          _selectedDateTime.hour,
          _selectedDateTime.minute,
          _selectedDateTime.second,
          _selectedDateTime.millisecond,
        );

        if (selectedDateTime.isBefore(sevenDaysAgo)) {
          selectedDateTime = sevenDaysAgo;
        }

        if (selectedDateTime.isAfter(today)) {
          selectedDateTime = today;
        }

        _selectedDateTime = selectedDateTime;

        final offset = _selectedDateTime.timeZoneOffset;
        final offsetSign = offset.isNegative ? '-' : '+';
        final offsetHours = offset.inHours.abs().toString().padLeft(2, '0');
        final offsetMinutes =
            (offset.inMinutes % 60).abs().toString().padLeft(2, '0');

        final formattedDateTime =
            "${_selectedDateTime.toIso8601String().split('Z')[0]}$offsetSign$offsetHours:$offsetMinutes";

        widget.viewModel.updateMovementAt(formattedDateTime);
        setState(() {
          dateTime = DateFormat.yMMMEd().format(_selectedDateTime).toString();
          time = DateFormat('HH:mm:ss').format(_selectedDateTime);
        });
      }
    });
  }
}
