import 'package:cadi_core_app/factory/cadi_enum_factory.dart';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/services/data_processing_service.dart';
import 'package:cadi_core_app/theme/theme.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'cargo_movement_summary_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class CargoMovementSummaryScreenBuilder extends StatelessWidget {
  const CargoMovementSummaryScreenBuilder({super.key});

  static const String route = route_helper.cargoMovementSummaryScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(context, "Action Blocked",
                  "You cannot change account while performing this action on summary screen.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, CargoMovementSummaryVM>(
        converter: CargoMovementSummaryVM.fromStore,
        builder: (context, vm) {
          return CargoMovementSummaryPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class CargoMovementSummaryVM {
  bool isLoading;
  AuthState authState;
  Account account;
  AccountPortDepot accountPortDepot;
  AccountCarrier accountCarrier;
  AccountCrane? accountCrane;
  String cargoRole;
  CargoMovementCreate cargoMovementCreate;
  UniversalBranding universalBranding;
  List<CadiEnum> weightUnits;

  // FileStore tempStorage;
  final Function(BuildContext, String) onNavigatePressed;
  final Function(String containerNumber) updateContainerNumber;
  final Function(String stowagePosition) updateStowagePosition;
  final Function(String containerWeight) updateContainerWeight;
  final Function(String packWeight) updatePackWeight;
  final Function(String sealNumber1) updateSealNumber1;
  final Function(String weightUnit) updateContainerWeightUnit;
  final Function(String weightUnit) updatePackWeightUnit;
  final Function(BuildContext context) confirmCargoMovement;
  final Function(String movementAt) updateMovementAt;

  var dateTime = DateFormat.yMMMEd().format(DateTime.now()).toString();
  var time = DateTime.now().toString().substring(10, 20);

  CargoMovementSummaryVM({
    required this.isLoading,
    required this.authState,
    required this.account,
    required this.accountPortDepot,
    required this.accountCarrier,
    required this.accountCrane,
    required this.cargoRole,
    required this.cargoMovementCreate,
    required this.universalBranding,
    required this.weightUnits,
    required this.onNavigatePressed,
    required this.updateContainerNumber,
    required this.updateStowagePosition,
    required this.updateContainerWeight,
    required this.updatePackWeight,
    required this.updateSealNumber1,
    required this.updateContainerWeightUnit,
    required this.updatePackWeightUnit,
    required this.confirmCargoMovement,
    required this.updateMovementAt,
  });

  static CargoMovementSummaryVM fromStore(Store<AppState> store) {
    return CargoMovementSummaryVM(
      isLoading: store.state.isLoading,
      authState: store.state.authState,
      account: store.state.accountState.account,
      accountPortDepot: store.state.accountState.accountPortDepots
          .where((element) =>
              element.id ==
              store.state.cargoMovementState.cargoMovementCreate
                  .accountPortLocationId)
          .first,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      weightUnits: CadiEnumFactory().createWeightMeasurements(),
      accountCarrier: store.state.accountState.accountCarriers.firstWhere(
          (element) =>
              element.carrierId ==
              store.state.cargoMovementState.cargoMovementCreate.carrierId,
          orElse: () => AccountCarrier.initial()),
      accountCrane: store.state.accountState.accountCranes.firstWhere(
              (element) =>
          element.id ==
              store.state.cargoMovementState.cargoMovementCreate.accountPortCraneId,
          orElse: () => AccountCrane.initial()),

      cargoRole: store.state.uiState.cargoRole,
      cargoMovementCreate: store.state.cargoMovementState.cargoMovementCreate,
      // tempStorage: store.state.storageState.tempStorage,
      onNavigatePressed: (BuildContext context, String route) {
        if (store.state.isLoading) {
          return;
        }
        navigation_helper.navigateRoute(route, store, null);
      },
      updateContainerNumber: (String containerNumber) {
        store.dispatch(
            UpdateCargoContainerNumber(containerNumber.replaceAll(' ', '')));
      },
      updateStowagePosition: (String stowagePosition) {
        store.dispatch(
            UpdateCargoStowagePosition(stowagePosition.replaceAll(' ', '')));
      },
      updateContainerWeight: (String containerWeight) {
        num numContainerWeight = num.tryParse(containerWeight) ?? 0.00;
        store.dispatch(UpdateCargoContainerWeight(numContainerWeight));
      },
      updatePackWeight: (String packWeight) {
        num numWeight = num.tryParse(packWeight) ?? 0.00;
        store.dispatch(UpdateCargoPackWeight(numWeight));
      },
      updateSealNumber1: (String sealNumber1) {
        store.dispatch(UpdateCargoSealNumber1(sealNumber1.replaceAll(' ', '')));
      },
      updateContainerWeightUnit: (String weightUnit) {
        store.dispatch(UpdateCargoContainerWeightUnit(weightUnit));
      },
      updatePackWeightUnit: (String weightUnit) {
        store.dispatch(UpdateCargoPackWeightUnit(weightUnit));
      },
      updateMovementAt: (String movementAt) {
        store.dispatch(UpdateCargoMovementAt(movementAt));
      },
      confirmCargoMovement: (BuildContext context) {
        store.dispatch(QueueCargoMovement(
            store.state.cargoMovementState.cargoMovementCreate));
        if (store.state.connectivityState.connectionStatus ==
                ConnectivityConstants.none ||
            store.state.connectivityState.connectionStatus ==
                ConnectivityConstants.ethernet) {
          showMessageWithActions(context, 'Queued',
              "Port movement has scheduled for submission when device is online.",
              () {
            Navigator.pop(context);
            store.dispatch(NavigateToAction.pushNamedAndRemoveUntil(
                route_helper.cargoMovementCompletionScreenBuilder,
                (route) => false));
          }, "Ok", null, "");
        } else {
          store.dispatch(NavigateToAction.pushNamedAndRemoveUntil(
              route_helper.cargoMovementCompletionScreenBuilder,
              (route) => false));

          processCargoMovementQueue(store);
        }
      },
    );
  }
}
