import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'cargo_movement_vehicle_details_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

class CargoMovementVehicleDetailsPage extends StatefulWidget {
  final CargoMovementVehicleDetailsVM viewModel;
  const CargoMovementVehicleDetailsPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<CargoMovementVehicleDetailsPage> createState() {
    return CargoMovementVehicleDetailsPageState(viewModel: viewModel);
  }
}

class CargoMovementVehicleDetailsPageState
    extends State<CargoMovementVehicleDetailsPage> {
  final CargoMovementVehicleDetailsVM viewModel;
  //final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final _truckRegistrationController = TextEditingController();
  final _truckDriverIdController = TextEditingController();
  final _truckCompanyController = TextEditingController();

  static const Key _truckRegistrationKey = Key(CargoKeys.truckRegistrationKey);
  static const Key _truckDriverIdKey = Key(CargoKeys.truckDriverIdKey);

  CargoMovementVehicleDetailsPageState({
    Key? key,
    required this.viewModel,
  });

  @override
  initState() {
    super.initState();

    _truckRegistrationController.text =
        widget.viewModel.uiCargoTruckDetails.truckRegistration;
    _truckDriverIdController.text =
        widget.viewModel.uiCargoTruckDetails.truckDriverId;
    _truckCompanyController.text =
        widget.viewModel.uiCargoTruckDetails.truckCompany;
  }

  @override
  void dispose() {
    super.dispose();
    _truckRegistrationController.dispose();
    _truckDriverIdController.dispose();
    _truckCompanyController.dispose();
  }

  void goNext(BuildContext context) {
    if (_truckRegistrationController.text.trim().isEmpty) {
      viewModel.showErrorAlert(
          context, "Please provide a truck registration number");
      return;
    } else if (_truckDriverIdController.text.trim().isEmpty) {
      viewModel.showErrorAlert(
          context, "Please provide a truck driver id");
      return;
    }
    viewModel.findCargoMovement(
        context,
        _truckRegistrationController.text.trim(),
        _truckDriverIdController.text.trim(),
        _truckCompanyController.text.trim());
  }

  @override
  Widget build(BuildContext context) {
    final List<SettingTile> allSettings = <SettingTile>[
      SettingTile(
        0,
        'Port Movement',
        Column(
          children: <Widget>[
            Text(
                "Cargo Movement: ${getCargoMovementRoleDescription(widget.viewModel.cargoRole)}",
                style: TextStyle(
                    fontSize: PlatformSizing().normalTextSize(context),
                    color: BrandingColors.cadiBlack)),
            // Any additional widget/codes
          ],
        ),
      ),
      SettingTile(
        1,
        'Truck Details',
        Container(
          margin: const EdgeInsets.only(top: 10.0), // Add your own value here
          child: Column(
            children: <Widget>[
              Text("Enter Truck Details",
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      fontWeight: FontWeight.w300,
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),
      SettingTile(
          2,
          'Truck Registration',
          Material(
            color: BrandingColors.background,
            child: ListTile(
                title: Text('Truck Registration',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack))),
          )),
      SettingTile(
        3,
        "",
        PlatformTextField(
            cursorColor: BrandingColors.cadiGrey,
            textCapitalization: TextCapitalization.characters,
            controller: _truckRegistrationController,
            key: _truckRegistrationKey,
            autocorrect: false,
            material: (_, __) => MaterialAssetsDark()
                .textFieldData("", FontAwesomeIcons.magnifyingGlass),
            cupertino: (_, __) =>
                CupertinoAssets().textFieldData('Truck Registration', null),
            style: TextStyle(
                color: BrandingColors.cadiBlack,
                fontWeight: FontWeight.w400,
                fontSize: PlatformSizing().inputTextFieldSize(context)),
            keyboardType: TextInputType.text,
            onEditingComplete: () {}),
      ),
      SettingTile(
          4,
          "Truck Driver ID",
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('Truck Driver ID',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(
        5,
        "Truck Driver Id Input",
        PlatformTextField(
            cursorColor: BrandingColors.cadiGrey,
            textCapitalization: TextCapitalization.characters,
            controller: _truckDriverIdController,
            key: _truckDriverIdKey,
            autocorrect: false,
            material: (_, __) => MaterialAssetsDark()
                .textFieldData("", FontAwesomeIcons.magnifyingGlass),
            cupertino: (_, __) =>
                CupertinoAssets().textFieldData('Truck Driver Id', null),
            style: TextStyle(
                fontWeight: FontWeight.w400,
                color: BrandingColors.cadiBlack,
                fontSize: PlatformSizing().inputTextFieldSize(context)),
            keyboardType: TextInputType.text,
            onEditingComplete: () {
              if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput &&
                  widget.viewModel.settingsAutoSubmitPage) {
                Future.delayed(
                    const Duration(milliseconds: 300), () => goNext(context));
              }
            }),
      ),
      SettingTile(
          4,
          "Truck Company",
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('Truck Company',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(
        5,
        "Truck Company Input",
        PlatformTextField(
            cursorColor: BrandingColors.cadiGrey,
            textCapitalization: TextCapitalization.characters,
            controller: _truckCompanyController,
            key: _truckDriverIdKey,
            autocorrect: false,
            material: (_, __) => MaterialAssetsDark()
                .textFieldData("", FontAwesomeIcons.magnifyingGlass),
            cupertino: (_, __) =>
                CupertinoAssets().textFieldData('Truck Company', null),
            style: TextStyle(
                fontWeight: FontWeight.w400,
                color: BrandingColors.cadiBlack,
                fontSize: PlatformSizing().inputTextFieldSize(context)),
            keyboardType: TextInputType.text,
            onEditingComplete: () {
              if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput &&
                  widget.viewModel.settingsAutoSubmitPage) {
                Future.delayed(
                    const Duration(milliseconds: 300), () => goNext(context));
              }
            }),
      ),
      const SettingTile(
          6,
          '',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text(''),
              ))),
      SettingTile(
          7,
          '',
          Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 50.0, vertical: 0.0),
              child: BaseButton(
                  color: widget.viewModel.universalBranding
                      .getButtonPrimaryColor(),
                  child: Text('Next',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: PlatformSizing().normalTextSize(context))),
                  onPressed: () {
                    goNext(context);
                  }))),
    ];

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          itemCount: allSettings.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20),
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
    // return Scaffold(
    //   // Optionally add an AppBar, Drawer, FloatingActionButton, etc. here
    //   body: Padding(
    //     padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
    //     child: Material(
    //       // Wrap the ListView with Material
    //       child: ListView.builder(
    //           itemCount: allSettings.length,
    //           padding: const EdgeInsets.only(left: 20.0, right:r 20),
    //           itemBuilder: (BuildContext context, int index) {
    //             return allSettings[index].body;
    //           }),
    //     ),
    //   ),
    // );
  }
}
