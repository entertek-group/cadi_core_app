import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'cargo_movement_vehicle_details_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

class CargoMovementVehicleDetailsScreenBuilder extends StatelessWidget {
  const CargoMovementVehicleDetailsScreenBuilder({super.key});

  static const String route = route_helper.cargoMovementVehicleDetailsBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(
                  // FontAwesomeIcons.warning,
                  context,
                  "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, CargoMovementVehicleDetailsVM>(
        converter: CargoMovementVehicleDetailsVM.fromStore,
        builder: (context, vm) {
          return CargoMovementVehicleDetailsPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class CargoMovementVehicleDetailsVM {
  bool settingsAutoDisplayNextDropdownOrInput;
  bool settingsAutoSubmitPage;
  bool isLoading;
  AuthState authState;
  Account account;
  String cargoRole;
  UiCargoTruckDetails uiCargoTruckDetails;
  UniversalBranding universalBranding;

  final Function(BuildContext context, String truckRegistration,
      String truckDriverId, String truckCompany) findCargoMovement;
  final Function(BuildContext context, String message) showErrorAlert;
  final Function(BuildContext, int) onNavigatePressed;
  final Function(String token) setToken;
  final Function(int selectedVehicleDriverIndex) setSelectedVehicleDriverIndex;

  CargoMovementVehicleDetailsVM(
      {required this.isLoading,
      required this.settingsAutoDisplayNextDropdownOrInput,
      required this.settingsAutoSubmitPage,
      required this.authState,
      required this.account,
      required this.cargoRole,
      required this.uiCargoTruckDetails,
      required this.showErrorAlert,
      required this.universalBranding,
      required this.onNavigatePressed,
      required this.setToken,
      required this.setSelectedVehicleDriverIndex,
      required this.findCargoMovement});

  static CargoMovementVehicleDetailsVM fromStore(Store<AppState> store) {
    return CargoMovementVehicleDetailsVM(
        settingsAutoDisplayNextDropdownOrInput:
            store.state.uiState.uiDropDownOrInputAutoContinue,
        settingsAutoSubmitPage: store.state.uiState.uiAutoSubmitPage,
        isLoading: store.state.isLoading,
        authState: store.state.authState,
        account: store.state.accountState.account,
        cargoRole: store.state.uiState.cargoRole,
        uiCargoTruckDetails: store.state.uiState.uiCargoTruckDetails,
        showErrorAlert: (BuildContext context, String message) {
          showOkMessage(context, "Submit Error", message);
        },
        universalBranding:
            UniversalBranding(store.state.accountState.activeService),
        findCargoMovement: (BuildContext context, String truckRegistration,
            String truckDriverId, String truckCompany) {
          // store.dispatch(
          //     UiCargoHomeTruckDetailsSetTruckRegistration(truckRegistration));
          // store
          //     .dispatch(UiCargoHomeTruckDetailsSetTruckDriverId(truckDriverId));
          store.dispatch(SetCargoTruckDriverVariables(
              truckRegistration.replaceAll(' ', ''),
              truckDriverId,
              truckCompany));
          store.dispatch(NavigateToAction.push(
              route_helper.cargoMovementSummaryScreenBuilder));
          //only capture for now
        },
        onNavigatePressed: (BuildContext context, int vehicleDriverId) {
          if (store.state.isLoading) {
            return;
          }
          if (vehicleDriverId > 0) {
            store.dispatch(SetAccountVehicleDriverId(vehicleDriverId));
          }
          store.dispatch(NavigateToAction.push(
              route_helper.cargoMovementSummaryScreenBuilder));
        },
        setToken: (String token) {},
        setSelectedVehicleDriverIndex: (int selectedVehicleDriverIndex) {
          store.dispatch(UiVehicleDriverSetIndex(selectedVehicleDriverIndex));
        });
  }
}
