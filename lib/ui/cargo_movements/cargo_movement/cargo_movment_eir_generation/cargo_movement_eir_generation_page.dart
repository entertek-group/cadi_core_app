import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/redux/models/image/image_create.dart';
import 'package:cadi_core_app/ui/cargo_movements/cargo_movement/cargo_movment_eir_generation/cargo_movement_eir_generation_vm.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';
// IMPORT PACKAGE
import 'package:signature/signature.dart';
import 'dart:convert';

class CargoMovementEirGenerationPage extends StatefulWidget {
  final CargoMovementEirGenerationVM viewModel;
  const CargoMovementEirGenerationPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<CargoMovementEirGenerationPage> createState() {
    return CargoMovementEirGenerationPageState(viewModel: viewModel);
  }
}

class CargoMovementEirGenerationPageState
    extends State<CargoMovementEirGenerationPage> {
  final CargoMovementEirGenerationVM viewModel;
  final SignatureController _controller = SignatureController(
    penStrokeWidth: 5,
    penColor: BrandingColors.cadiBlack,
    exportBackgroundColor: BrandingColors.cadiWhite,
  );

  final _inputSignatoryFullNameController = TextEditingController();
  final _inputConsigneeOrConsignorNameController = TextEditingController();
  final _inputDeliveryDocketNumberController = TextEditingController();
  final _inputPortOfLoadOrDischargeController = TextEditingController();

  static const Key _inputSignatoryFullNameKey =
      Key(RoadMovementKeys.inputSignatoryFullNameKey);

  CargoMovementEirGenerationPageState({
    Key? key,
    required this.viewModel,
  });

  @override
  initState() {
    super.initState();
    // Initialize text fields if values exist
    if (widget.viewModel.cargoMovementCreate.consigneeOrConsignorName != null) {
      _inputConsigneeOrConsignorNameController.text =
          widget.viewModel.cargoMovementCreate.consigneeOrConsignorName!;
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    _inputSignatoryFullNameController.dispose();
    _inputConsigneeOrConsignorNameController.dispose();
    _inputDeliveryDocketNumberController.dispose();
    _inputPortOfLoadOrDischargeController.dispose();
    super.dispose();
  }

  void goNext(BuildContext context) async {
    if (_inputSignatoryFullNameController.text.trim().isEmpty) {
      viewModel.showErrorAlert(context, "Please add signatory full name");
      return;
    } else if (_inputConsigneeOrConsignorNameController.text.trim().isEmpty) {
      viewModel.showErrorAlert(
          context, "Please provide a consignee/consignor name");
      return;
    } else if (_inputDeliveryDocketNumberController.text.trim().isEmpty) {
      viewModel.showErrorAlert(
          context, "Please provide a delivery docket number");
      return;
    } else if (_inputPortOfLoadOrDischargeController.text.trim().isEmpty) {
      viewModel.showErrorAlert(
          context, "Please provide a port of load/discharge");
      return;
    } else if (_controller.isEmpty &&
        widget.viewModel.cargoMovementCreate.signature == null) {
      viewModel.showErrorAlert(context, "Please provide signature");
      return;
    }

    var bytes = await _controller.toPngBytes();
    if (bytes != null) {
      String base64Content = base64Encode(bytes.toList());
      String name =
          '${_inputSignatoryFullNameController.text.replaceAll(" ", "").trim()}_SIGNATURE.png';
      ImageCreate imageCreate = ImageCreate(
          name: name, base64Content: base64Content, kind: "SIGNATURE");
      if (context.mounted) {
        viewModel.onNavigatePressed(
            context,
            _inputSignatoryFullNameController.text.trim(),
            imageCreate,
            _inputConsigneeOrConsignorNameController.text.trim(),
            _inputDeliveryDocketNumberController.text.trim(),
            _inputPortOfLoadOrDischargeController.text.trim());
      }
    } else {
      if (context.mounted) {
        viewModel.showErrorAlert(
            context, "Please try again. Signature not captured");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;

    var signatureCanvas = Container(
      decoration: BoxDecoration(
        border: Border.all(color: BrandingColors.cadiBlack.withOpacity(0.2)),
        color: Colors.white,
      ),
      child: ClipRect(
        child: Signature(
          controller: _controller,
          width: screenWidth,
          height: screenHeight * 0.35,
          backgroundColor: Colors.white,
        ),
      ),
    );

    List<SettingTile> allOptions = <SettingTile>[
      SettingTile(
        0,
        'Cargo Movement',
        Column(
          children: <Widget>[
            Text(
                "Cargo Movement: ${getCargoMovementRoleDescription(widget.viewModel.cargoRole)}",
                style: TextStyle(
                    fontSize: PlatformSizing().normalTextSize(context),
                    color: BrandingColors.cadiBlack)),
            // Any additional widget/codes
          ],
        ),
      ),
      SettingTile(
        1,
        'Status Information',
        Container(
          margin: const EdgeInsets.only(
              top: 10.0, bottom: 15), // Add your own value here
          child: Column(
            children: <Widget>[
              Text("Complete EIR Document Information",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),
    
      // Signatory Name Field with Label
      SettingTile(
        2,
        'Signatory',
        Material(
          color: BrandingColors.cadiWhite,
          child: ListTile(
            title: Text('Signatory Full Name',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          ),
        ),
      ),
      SettingTile(
        3,
        '',
        PlatformTextField(
          controller: _inputSignatoryFullNameController,
          key: _inputSignatoryFullNameKey,
          textCapitalization: TextCapitalization.characters,
          material: (_, __) =>
              MaterialAssetsDark().textFieldData("", FontAwesomeIcons.user),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData("Full Name", null),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
        ),
      ),

      // Consignee/Consignor Field with Label
      SettingTile(
        4,
        'Consignee/Consignor',
        Material(
          color: BrandingColors.cadiWhite,
          child: ListTile(
            title: Text('Consignee/Consignor Name',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          ),
        ),
      ),
      SettingTile(
        5,
        '',
        PlatformTextField(
          controller: _inputConsigneeOrConsignorNameController,
          textCapitalization: TextCapitalization.characters,
          material: (_, __) =>
              MaterialAssetsDark().textFieldData("", FontAwesomeIcons.user),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData("Consignee/Consignor Name", null),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
        ),
      ),

      // Delivery Docket Field with Label
      SettingTile(
        6,
        'Delivery Docket',
        Material(
          color: BrandingColors.cadiWhite,
          child: ListTile(
            title: Text('Delivery Docket Number',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          ),
        ),
      ),
      SettingTile(
        7,
        '',
        PlatformTextField(
          controller: _inputDeliveryDocketNumberController,
          textCapitalization: TextCapitalization.characters,
          material: (_, __) =>
              MaterialAssetsDark().textFieldData("", FontAwesomeIcons.receipt),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData("Delivery Docket Number", null),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
        ),
      ),

      // Port Field with Label
      SettingTile(
        8,
        'Port',
        Material(
          color: BrandingColors.cadiWhite,
          child: ListTile(
            title: Text(
                widget.viewModel.cargoRole == CadiCargoMovementRole.gateIn
                    ? 'Port of Load'
                    : 'Port of Discharge',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          ),
        ),
      ),
      SettingTile(
        9,
        '',
        PlatformTextField(
          controller: _inputPortOfLoadOrDischargeController,
          textCapitalization: TextCapitalization.characters,
          material: (_, __) =>
              MaterialAssetsDark().textFieldData("", FontAwesomeIcons.anchor),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData("Port of Load/Discharge", null),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
        ),
      ),

      // Signature Section with Label
      SettingTile(
        10,
        'Signature',
        Material(
          color: BrandingColors.cadiWhite,
          child: ListTile(
            title: Text('Signature',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          ),
        ),
      ),
      SettingTile(10, '', signatureCanvas),

      // Updated Signature Clear Button
      SettingTile(
        11,
        '',
        Material(
          color: BrandingColors.background,
          child: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                BaseGhostButton(
                  onPressed: () => _controller.clear(),
                  textColor: BrandingColors.cadiBlack,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Icon(
                        FontAwesomeIcons.trash,
                        size: 20,
                        color: BrandingColors.cadiBlack,
                      ),
                      const SizedBox(width: 8),
                      Text(
                        'Clear',
                        style: TextStyle(
                          color: BrandingColors.cadiBlack,
                          fontSize: PlatformSizing().mediumTextSize(context),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),

      // Next Button
      SettingTile(
        12,
        '',
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50.0, vertical: 20.0),
          child: BaseButton(
            color: widget.viewModel.universalBranding.getButtonPrimaryColor(),
            child: Text('Next',
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: PlatformSizing().normalTextSize(context))),
            onPressed: () => goNext(context),
          ),
        ),
      ),
    ];

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          itemCount: allOptions.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          itemBuilder: (BuildContext context, int index) {
            return allOptions[index].body;
          }),
    );
  }
}
