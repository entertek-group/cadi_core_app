import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/image/image_create.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/cargo_movements/cargo_movement/cargo_movment_eir_generation/cargo_movement_eir_generation_page.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

class CargoMovementEirGenerationScreenBuilder extends StatelessWidget {
  const CargoMovementEirGenerationScreenBuilder({super.key});

  static const String route =
      route_helper.cargoMovementEirGenerationScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(
                  // FontAwesomeIcons.warning,
                  context,
                  "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, CargoMovementEirGenerationVM>(
        converter: CargoMovementEirGenerationVM.fromStore,
        builder: (context, vm) {
          return CargoMovementEirGenerationPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class CargoMovementEirGenerationVM {
  bool settingsAutoDisplayNextDropdownOrInput;
  bool settingsAutoSubmitPage;
  bool isLoading;
  AuthState authState;
  Account account;
  String cargoRole;
  CargoMovementCreate cargoMovementCreate;
  UniversalBranding universalBranding;

  final Function(BuildContext context, String message) showErrorAlert;
  final Function(
      BuildContext context,
      String signatoryFullName,
      ImageCreate imageCreate,
      String consigneeOrConsignorName,
      String deliveryDocketNumber,
      String portOfLoadOrDischarge) onNavigatePressed;

  CargoMovementEirGenerationVM({
    required this.isLoading,
    required this.settingsAutoDisplayNextDropdownOrInput,
    required this.settingsAutoSubmitPage,
    required this.authState,
    required this.account,
    required this.cargoRole,
    required this.cargoMovementCreate,
    required this.universalBranding,
    required this.showErrorAlert,
    required this.onNavigatePressed,
  });

  static CargoMovementEirGenerationVM fromStore(Store<AppState> store) {
    return CargoMovementEirGenerationVM(
      settingsAutoDisplayNextDropdownOrInput:
          store.state.uiState.uiDropDownOrInputAutoContinue,
      settingsAutoSubmitPage: store.state.uiState.uiAutoSubmitPage,
      isLoading: store.state.isLoading,
      authState: store.state.authState,
      account: store.state.accountState.account,
      cargoRole: store.state.uiState.cargoRole,
      cargoMovementCreate: store.state.cargoMovementState.cargoMovementCreate,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      onNavigatePressed: (BuildContext context,
          String signatoryFullName,
          ImageCreate imageCreate,
          String consigneeOrConsignorName,
          String deliveryDocketNumber,
          String portOfLoadOrDischarge) {
        if (store.state.isLoading) {
          return;
        }

        store.dispatch(SetCargoMovementEirVariables(
            signatoryFullName,
            imageCreate,
            consigneeOrConsignorName,
            deliveryDocketNumber,
            portOfLoadOrDischarge));
        store.dispatch(NavigateToAction.push(
            route_helper.cargoMovementVehicleDetailsBuilder));
      },
      showErrorAlert: (BuildContext context, String message) {
        showOkMessage(context, "Submit Error", message);
      },
    );
  }
}
