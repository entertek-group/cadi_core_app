import 'package:cadi_core_app/redux/models/report/report_models.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:cadi_core_app/ui/shared/components/app/setting_tile.dart';
import 'package:intl/intl.dart';
import 'cargo_movement_report_vm.dart';
import 'package:fl_chart/fl_chart.dart';

class CargoMovementReportPage extends StatefulWidget {
  final CargoMovementReportVM viewModel;
  const CargoMovementReportPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<CargoMovementReportPage> createState() {
    return CargoMovementReportPageState(viewModel: viewModel);
  }
}

class CargoMovementReportPageState extends State<CargoMovementReportPage> {
  final CargoMovementReportVM viewModel;

  CargoMovementReportPageState({
    Key? key,
    required this.viewModel,
  });

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    final List<SettingTile> allSettings = <SettingTile>[
      SettingTile(
          2,
          '',
          Padding(
              padding: EdgeInsets.only(
                  top: screenHeight * 0.01, bottom: screenHeight * 0.01),
              child: buildCompleteTopInfoContainer(
                  context, viewModel.portMovementReportSummary))),
      SettingTile(
          2,
          '',
          Padding(
              padding: EdgeInsets.only(
                  top: screenHeight * 0.01, bottom: screenHeight * 0.01),
              child: buildCompleteBottomInfoContainer(
                  context, viewModel.portMovementReportSummary.reportLines))),
    ];

    return Padding(
      padding: const EdgeInsets.only(top: 0, left: 5, right: 5),
      child: ListView.builder(
          shrinkWrap: false,
          itemCount: allSettings.length,
          padding: const EdgeInsets.only(
            left: 10.0,
            right: 10.0,
          ),
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
  }
}

Widget buildCompleteImage(BuildContext context) {
  return Container(
      padding: const EdgeInsets.only(bottom: 25),
      alignment: Alignment.center,
      child: const FaIcon(
        BrandIcons.reports,
        size: 74,
        color: BrandingColors.cadiGrey,
      ));
}

// Top container widget
Widget buildCompleteTopInfoContainer(
    BuildContext context, PortMovementReportSummary mainMovementProgress) {
  return Card(
    color: Colors.white,
    margin: const EdgeInsets.all(0),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10.0),
    ),
    child: Padding(
      padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 15.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '${mainMovementProgress.voyageNumber}-${mainMovementProgress.vesselName}-${mainMovementProgress.unLoCode}',
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: BrandingColors.cadiBlack,
            ),
          ),
          const SizedBox(height: 14),
          Row(
            children: [
              Expanded(
                child: Text(
                  '${mainMovementProgress.completedPercentage.toStringAsFixed(2)}% Completed',
                  style: const TextStyle(
                    fontSize: 27,
                    fontWeight: FontWeight.bold,
                    color: BrandingColors.cadiBlack,
                  ),
                ),
              ),
              Container(
                width: 70,
                height: 70,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: BrandingColors.cadiBlack, // Black border around the pie chart
                    width: 6, // Border thickness
                  ),
                ),
                child: PieChart(
                  PieChartData(
                    borderData: FlBorderData(
                      show: false,
                    ),
                    sectionsSpace: 0,
                    centerSpaceRadius: 0,
                    startDegreeOffset: 0, // Start at 0 degrees
                    sections: showingSections(mainMovementProgress.completedPercentage.toDouble()),
                    pieTouchData: PieTouchData(enabled: false), // Disable touch interactions
                  ),
                ),
              ),
              const SizedBox(width: 30),
            ],
          ),
          Text(
            '+${mainMovementProgress.damagedCount} Damaged Containers',
            style: const TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
              color: BrandingColors.cadiGrey,
            ),
          ),
        ],
      ),
    ),
  );
}

List<PieChartSectionData> showingSections(double completionPercentage) {
  return [
    PieChartSectionData(
      color: BrandingColors.cadiBlack,
      value: 100 - completionPercentage,
      title: '',
      radius: 30,
    ),
    PieChartSectionData(
      color: BrandingColors.white,
      value: completionPercentage,
      title: '',
      radius: 30,
    ),
  ];
}


// Bottom container widget
Widget buildCompleteBottomInfoContainer(
    BuildContext context, List<PortMovementReportLine> data) {
  // Get the current local date
  DateTime now = DateTime.now();
  String formattedDate = DateFormat('MM/dd/yyyy').format(now);

  return Card(
    color: Colors.white,
    margin: const EdgeInsets.all(0),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10.0),
    ),
    child: Padding(
      padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 15.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Date $formattedDate",
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 10),
          LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minWidth: constraints.maxWidth,
                  ),
                  child: DataTable(
                    columnSpacing: 12.0,
                    columns: [
                      DataColumn(
                        label: SizedBox(
                          width: constraints.maxWidth * 0.23,
                          child: const Text(
                            'Movement',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: BrandingColors.cadiBlackOpacity,
                            ),
                          ),
                        ),
                      ),
                      DataColumn(
                        label: SizedBox(
                          width: constraints.maxWidth * 0.14,
                          child: const Center(
                            child: Text(
                              '',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: BrandingColors.cadiBlackOpacity,
                              ),
                            ),
                          ),
                        ),
                      ),
                      DataColumn(
                        label: SizedBox(
                          width: constraints.maxWidth * 0.21,
                          child: const Center(
                            child: Text(
                              'Goal',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: BrandingColors.cadiBlackOpacity,
                              ),
                            ),
                          ),
                        ),
                      ),
                      DataColumn(
                        label: SizedBox(
                          width: constraints.maxWidth * 0.21,
                          child: const Center(
                            child: Text(
                              'Completed',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: BrandingColors.cadiBlackOpacity,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                    rows: data.map((movement) {
                      if (movement.role == "Damaged") {
                        return DataRow(
                          cells: [
                            DataCell(
                              SizedBox(
                                width: constraints.maxWidth * 0.23,
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    movement.role,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.w200),
                                  ),
                                ),
                              ),
                            ),
                            DataCell(
                              SizedBox(
                                width: constraints.maxWidth * 0.14,
                                child: Center(
                                  child: Text(
                                    movement.goalCount.toString(),
                                    style: const TextStyle(
                                        fontWeight: FontWeight.w200),
                                  ),
                                ),
                              ),
                            ),
                            DataCell(
                              SizedBox(
                                width: constraints.maxWidth * 0.21,
                              ), // Empty cell
                            ),
                            DataCell(
                              SizedBox(
                                width: constraints.maxWidth * 0.21,
                              ), // Empty cell
                            ),
                          ],
                        );
                      } else {
                        return DataRow(
                          cells: [
                            DataCell(
                              SizedBox(
                                width: constraints.maxWidth * 0.23,
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(movement.role),
                                ),
                              ),
                            ),
                            DataCell(
                              Center(
                                child: SizedBox(
                                  width: constraints.maxWidth * 0.14,
                                  height: 10,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(4),
                                    child: LinearProgressIndicator(
                                      value: movement.goalCount > 0
                                          ? movement.completedPercentage
                                              .toDouble()/100.0
                                          : 0.0,
                                      backgroundColor: BrandingColors.cadiGrey,
                                      valueColor:
                                          const AlwaysStoppedAnimation<Color>(
                                        BrandingColors.cadiBlack,
                                      ),
                                      minHeight: 16,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            DataCell(
                              SizedBox(
                                width: constraints.maxWidth * 0.21,
                                child: Center(
                                  child: Text(
                                    movement.goalCount.toString(),
                                  ),
                                ),
                              ),
                            ),
                            DataCell(
                              SizedBox(
                                width: constraints.maxWidth * 0.21,
                                child: Center(
                                  child: Text(
                                    movement.completedCount.toString(),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        );
                      }
                    }).toList(),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    ),
  );
}
