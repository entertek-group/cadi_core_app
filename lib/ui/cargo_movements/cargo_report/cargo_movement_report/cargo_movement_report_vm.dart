import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_home_button.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_user_account.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:redux/redux.dart';
import 'cargo_movement_report_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

class CargoMovementReportBuilder extends StatelessWidget {
  const CargoMovementReportBuilder({super.key});

  static const String route = route_helper.cargoMovementReportsBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(
                  // FontAwesomeIcons.warning,
                  context,
                  "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, CargoMovementReportVM>(
        converter: CargoMovementReportVM.fromStore,
        builder: (context, vm) {
          return CargoMovementReportPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class CargoMovementReportVM {
  bool isLoading;
  AuthState authState;
  Account account;
  PortMovementReportSummary portMovementReportSummary;

  CargoMovementReportVM({
    required this.isLoading,
    required this.authState,
    required this.account,
    required this.portMovementReportSummary
  });

  static CargoMovementReportVM fromStore(Store<AppState> store) {
    return CargoMovementReportVM(
      isLoading: store.state.isLoading,
      authState: store.state.authState,
      account: store.state.accountState.account,
      portMovementReportSummary: store.state.reportState.portMovementReportSummary,
    );
  }
}
