import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:cadi_core_app/ui/shared/components/app/setting_tile.dart';
import 'cargo_report_home_vm.dart';

class CargoReportHomePage extends StatefulWidget {
  final CargoReportHomeVM viewModel;
  const CargoReportHomePage({
    super.key,
    required this.viewModel,
  });

  @override
  State<CargoReportHomePage> createState() {
    return CargoReportHomePageState(viewModel: viewModel);
  }
}

class CargoReportHomePageState extends State<CargoReportHomePage> {
  final CargoReportHomeVM viewModel;

  CargoReportHomePageState({
    Key? key,
    required this.viewModel,
  });

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    final List<SettingTile> allSettings = <SettingTile>[
      SettingTile(
          0,
          'Menu Title',
          ListTile(
              title: Text("Report",
                  style: TextStyle(

                      // fontWeight: FontWeight.w500,
                      fontSize: PlatformSizing().normalTextSize(context),
                      color: BrandingColors.cadiBlack)))),
      SettingTile(
          2,
          '',
          Padding(
              padding: EdgeInsets.only(
                  top: screenHeight * 0.01, bottom: screenHeight * 0.01),
              child: buildCompleteImage(context))),
      SettingTile(
        2,
        'List view',
        ListView(shrinkWrap: true,
            // gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            //     crossAxisCount: 1,
            //     mainAxisSpacing: 5,
            //     crossAxisSpacing: 5),s
            children: [
              buildReportMenuItem(
                context,
                "Restow",
                () => widget.viewModel.onRestowSelected(context),
                BrandIcons.restow,
                false,
                "",
              ),
              buildReportMenuItem(
                context,
                "Short Ship",
                () => widget.viewModel.onShortShipSelected(context),
                BrandIcons.shortShip,
                true,
                BrandingImages.shortShip,
              ),
              buildReportMenuItem(
                context,
                "Short Land",
                () => widget.viewModel.onShortLandSelected(context),
                BrandIcons.shortLand,
                true,
                BrandingImages.shortLand,
              ),
              buildReportMenuItem(
                context,
                "Movement Status",
                () => widget.viewModel.onMovementStatusSelected(context),
                BrandIcons.status,
                true,
                BrandingImages.shortLand, // TODO: Add branding image for movement status?
              ),
            ]),
      ),
    ];

    return Padding(
      padding: const EdgeInsets.only(top: 0, left: 40, right: 40),
      child: ListView.builder(
          shrinkWrap: false,
          itemCount: allSettings.length,
          padding: const EdgeInsets.only(
            left: 10.0,
            right: 10.0,
          ),
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
  }
}

Widget buildReportMenuItem(
  BuildContext context,
  menuName,
  Function() navFunction,
  IconData icon,
  bool customIcon,
  String customIconPath,
) {
  var topHeight = MediaQuery.of(context).size.height;
  return Card(
    elevation: 3,
    shadowColor: BrandingColors.cadiGrey,
    color: BrandingColors.background,
    shape: RoundedRectangleBorder(
      side: const BorderSide(
        color: BrandingColors.cadiBlack,
        width: 0.9,
      ),
      borderRadius: BorderRadius.circular(25),
    ),
    child: InkWell(
        highlightColor: BrandingColors.cadiGreen,
        borderRadius: const BorderRadius.all(Radius.circular(25)),
        onTap: navFunction,
        child: Container(
            padding: const EdgeInsets.only(top: 25, bottom: 25, left: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                FaIcon(icon,
                    color: BrandingColors.cadiBlackOpacity,
                    size: topHeight * 0.04),
                // Text(" "),
                Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Text(menuName,
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            fontWeight: FontWeight.w300,
                            fontSize: PlatformSizing().normalTextSize(context),
                            color: BrandingColors.cadiBlack))),
              ],
            ))),
  );
}

Widget buildCompleteImage(BuildContext context) {

  return Container(
      padding: const EdgeInsets.only(bottom: 25),
      alignment: Alignment.center,
      child: const FaIcon(
        BrandIcons.reports,
        size: 74,
        color: BrandingColors.cadiGrey,
      ));
}
