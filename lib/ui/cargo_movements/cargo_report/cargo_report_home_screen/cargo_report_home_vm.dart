import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'cargo_report_home_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

class CargoReportHomeBuilder extends StatelessWidget {
  const CargoReportHomeBuilder({super.key});

  static const String route = route_helper.cargoReportHomeBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      body: StoreConnector<AppState, CargoReportHomeVM>(
        converter: CargoReportHomeVM.fromStore,
        builder: (context, vm) {
          return CargoReportHomePage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class CargoReportHomeVM {
  bool isLoading;
  AuthState authState;
  Account account;

  final Function(BuildContext context) onRestowSelected;
  final Function(BuildContext context) onShortShipSelected;
  final Function(BuildContext context) onShortLandSelected;
  final Function(BuildContext context) onMovementStatusSelected;

  CargoReportHomeVM({
    required this.isLoading,
    required this.authState,
    required this.account,
    required this.onRestowSelected,
    required this.onShortShipSelected,
    required this.onShortLandSelected,
    required this.onMovementStatusSelected,
  });

  static CargoReportHomeVM fromStore(Store<AppState> store) {
    return CargoReportHomeVM(
      isLoading: store.state.isLoading,
      authState: store.state.authState,
      account: store.state.accountState.account,
      onShortShipSelected: (BuildContext context) {
        store.dispatch(UiCargoReportSetReportType(PortReportType.shortShip));
        store.dispatch(SetReportType(PortReportType.shortShip));
        store.dispatch(
            NavigateToAction.push(route_helper.cargoReportSearchBuilder));
      },
      onRestowSelected: (BuildContext context) {
        store.dispatch(UiCargoReportSetReportType(PortReportType.restow));
        store.dispatch(SetReportType(PortReportType.restow));
        store.dispatch(
            NavigateToAction.push(route_helper.cargoReportSearchBuilder));
      },
      onShortLandSelected: (BuildContext context) {
        store.dispatch(UiCargoReportSetReportType(PortReportType.shortLand));
        store.dispatch(SetReportType(PortReportType.shortLand));
        store.dispatch(
            NavigateToAction.push(route_helper.cargoReportSearchBuilder));
      },
      onMovementStatusSelected: (BuildContext context){
        store.dispatch(UiCargoReportSetReportType(PortReportType.movementStatus));
        store.dispatch(SetReportType(PortReportType.movementStatus));
        store.dispatch(
            NavigateToAction.push(route_helper.cargoReportSearchBuilder));
      }
    );
  }
}
