import 'dart:core';
import 'package:cadi_core_app/factory/cadi_enum_factory.dart';
import 'package:cadi_core_app/factory/ui_factory.dart';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/app/setting_tile.dart';
import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'cargo_report_results_vm.dart';

class CargoReportResultsPage extends StatefulWidget {
  final CargoReportResultsVM viewModel;
  const CargoReportResultsPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<CargoReportResultsPage> createState() {
    return CargoReportResultsState(viewModel: viewModel);
  }
}

class CargoReportResultsState extends State<CargoReportResultsPage> {
  final CargoReportResultsVM viewModel;
  final _selectedFilterController = TextEditingController();


  List<DropdownItem> _filterItems = [];

  CargoReportResultsState({
    Key? key,
    required this.viewModel,
  });

  @override
  initState() {
    super.initState();

    _filterItems = UiFactory().convertToDropdown<CadiEnum, String>(
        CadiEnumFactory().createReportFilterOptions(),
        (item) => item.description,
        (item) => item.id);

    _setFilterSelect(0);
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _setFilterSelect(int index) {
    setState(() {
      _selectedFilterController.text = _filterItems[index].text;
      widget.viewModel
          .filterCargoMovementsBySelection(_filterItems[index].value);
    });
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    List<Widget> cargoSummary = <Widget>[];
    {
      cargoSummary.add(buildSummaryRow(context, "Vessel #",
          widget.viewModel.reportSummary.vesselName, false, null));

      cargoSummary.add(buildSummaryRow(context, "Voyage #",
          widget.viewModel.reportSummary.voyageNumber, false, null));
      cargoSummary.add(buildSummaryRow(context, "Port",
          widget.viewModel.reportSummary.unLoCode, false, null));

      cargoSummary.add(buildSummaryJobs(
          context,
          "Completed",
          "${widget.viewModel.reportSummary.completedCount}  of  ${widget.viewModel.reportSummary.remainingCount}",
          true,
          null));
    }

    List<SettingTile> allOptions = <SettingTile>[
      SettingTile(
          0,
          'DISPLAY TYPE',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                  title: Text("Reports: ${getReportTypeDescription(widget.viewModel.reportType)}",
                      // +
                      //     getReportsRoleDescroption(
                      //         widget.viewModel.cargoRole),
                      style: TextStyle(
                          fontSize: PlatformSizing().normalTextSize(context),
                          color: BrandingColors.cadiBlack))))),
      SettingTile(
          1,
          'DISPLAY STATUS',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                  title: Text("Search Results",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: PlatformSizing().smallTextSize(context),
                          color: BrandingColors.cadiBlack))))),
      SettingTile(2, 'Summary', buildSummaryCard(context, cargoSummary)),

      // SettingTile(
      //     7,
      //     "",
      //     Material(
      //         color: BrandingColors.background,
      //         child: InkWell(
      //           onTap: () {
      //             showBottomDropdown(
      //                 context,
      //                 'Filter Option',
      //                 'Please select a filter option',
      //                 _filterItems,
      //                 _selectedFilterIndex,
      //                 _setFilterSelect,
      //                 true);
      //           },
      //           child: PlatformTextField(
      //             textCapitalization: TextCapitalization.characters,
      //             controller: _selectedFilterController,
      //             key: _selectFilterKey,
      //             autocorrect: false,
      //             enabled: false,
      //             material: (_, __) => MaterialAssetsDark().textFieldData(
      //                 "All / Loaded / Unloaded", FontAwesomeIcons.chevronDown),
      //             cupertino: (_, __) => CupertinoAssets().textFieldData(
      //                 'All / Loaded / Unloaded', FontAwesomeIcons.chevronDown),
      //             style: TextStyle(
      //                 color: BrandingColors.black,
      //                 fontWeight: FontWeight.w400,
      //                 fontSize: PlatformSizing().inputTextFieldSize(context)),
      //             keyboardType: TextInputType.text,
      //           ),
      //         ))),
    ];

    List<TableRow> summaryTableRows = [
      buildTableRow(context, buildTableTitle("NUMBER #"),
          buildTableTitle("TYPE"), buildTableTitle("STATUS")),
    ];

    for (var portReportLine in widget.viewModel.reportSummary.reportLines) {

      summaryTableRows.add(buildTableRow(
          context,
          buildTableText(portReportLine.identifier),
          buildTableText(portReportLine.type),
          buildStatusText(portReportLine.role)));
    }

    allOptions.add(SettingTile(
        20,
        'Summary',
        Material(
            color: BrandingColors.background,
            child: Padding(
              padding: const EdgeInsets.only(top: 20, left: 10),
              child: buildScrollableTable(
                  buildSummaryTable(context, summaryTableRows),
                  screenHeight * 0.35),
            ))));

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      child: ListView.builder(
          physics: const ClampingScrollPhysics(),
          itemCount: allOptions.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          itemBuilder: (BuildContext context, int index) {
            return allOptions[index].body;
          }),
    );
  }
}

Widget buildSummaryCard(BuildContext context, List<Widget> cargoSummary) {
  return Card(
      elevation: 0,
      color: BrandingColors.background,
      margin: const EdgeInsets.only(left: 5, right: 5, bottom: 0),
      child: ListView.builder(
          physics: const ClampingScrollPhysics(),
          shrinkWrap: true,
          padding: const EdgeInsets.all(15),
          itemCount: cargoSummary.length,
          itemBuilder: (BuildContext context, int index) {
            return cargoSummary[index];
          }));
}

Widget buildSummaryCardThree(BuildContext context, Widget reportSummary) {
  return SingleChildScrollView(child: reportSummary
      // shrinkWrap: true,
      // children: [Card(elevation: 1, child: reportSummary)],
      );
}

Widget buildSummaryRow(BuildContext context, String textDisplay,
    String textValue, bool hasAction, Function? buttonAction) {
  return ListTile(
      dense: true,
      visualDensity: const VisualDensity(horizontal: 0, vertical: -2),
      minVerticalPadding: 4,
      contentPadding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 2),
      tileColor: Colors.transparent,
      leading: Text(
        textDisplay,
        style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: PlatformSizing().smallTextSize(context),
            color: BrandingColors.cadiBlack),
      ),
      title: Text(
        textValue,
        style: TextStyle(
            fontSize: PlatformSizing().smallTextSize(context),
            color: BrandingColors.cadiBlack),
        textAlign: TextAlign.end,
      ),
      trailing: hasAction
          ? IconButton(
              padding: const EdgeInsets.only(bottom: 0, left: 25),
              icon: const Icon(FontAwesomeIcons.penToSquare,
                  color: BrandingColors.cadiBlack),
              onPressed: buttonAction != null ? () => buttonAction() : () {})
          : const IconButton(
              icon: Icon(Icons.edit_outlined, color: Colors.transparent),
              onPressed: null));
}

Widget buildSummaryJobs(BuildContext context, String textDisplay,
    String textValue, bool hasAction, Function? buttonAction) {
  return ListTile(
      dense: true,
      contentPadding: const EdgeInsets.only(top: 1, bottom: 0),
      tileColor: Colors.transparent,
      leading: Text(
        textDisplay,
        style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: PlatformSizing().mediumTextSize(context),
            color: BrandingColors.cadiBlack),
      ),
      title: Text(
        textValue,
        style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: PlatformSizing().mediumTextSize(context),
            color: BrandingColors.cadiBlack),
        textAlign: TextAlign.end,
      ),
      trailing: hasAction
          ? IconButton(
              padding: const EdgeInsets.only(bottom: 0, left: 0),
              icon: const Icon(FontAwesomeIcons.circleExclamation,
                  color: BrandingColors.cadiBlack),
              onPressed: buttonAction != null ? () => buttonAction() : () {})
          : const IconButton(
              icon: Icon(Icons.edit_outlined, color: Colors.transparent),
              onPressed: null));
}

Container buildTableText(String text) {
  return Container(
    child: Padding(
      padding: const EdgeInsets.only(bottom: 4, left: 5),
      child: Text(text,
          style: const TextStyle(
              color: BrandingColors.cadiBlack,
              fontWeight: FontWeight.w300,
              fontSize: 15)),
    ),
  );
}

// #TODO if statement for status
Container buildStatusText(String text) {
  return Container(
    child: Padding(
      padding: const EdgeInsets.only(bottom: 4, left: 5),
      child: Text(text,
          style: const TextStyle(
              color: BrandingColors.cadiGreen,
              fontWeight: FontWeight.w800,
              fontSize: 15)),
    ),
  );
}

Container buildTableTitle(String text) {
  return Container(
    child: Padding(
      padding: const EdgeInsets.only(bottom: 8, top: 10, left: 5),
      child: Text(text,
          style: const TextStyle(
              color: BrandingColors.cadiBlack,
              fontWeight: FontWeight.w800,
              fontSize: 15)),
    ),
  );
}

TableRow buildTableRow(BuildContext context, Widget rowItemOne,
    Widget rowItemTwo, Widget rowItemThree) {
  return TableRow(children: [rowItemOne, rowItemTwo, rowItemThree]);
}

Table buildSummaryTable(BuildContext context, List<TableRow> tableRows) {
  return Table(
    children: tableRows,
  );
}

SizedBox buildScrollableTable(Table table, double screenHeight) {
  return SizedBox(
      height: screenHeight,
      child: Scrollbar(
          child: SingleChildScrollView(
              scrollDirection: Axis.vertical, child: table)));
}
