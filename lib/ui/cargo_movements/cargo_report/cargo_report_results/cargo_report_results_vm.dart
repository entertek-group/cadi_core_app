import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/theme.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/cargo_movements/cargo_report/cargo_report_results/cargo_report_results_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

class CargoReportResultsBuilder extends StatelessWidget {
  const CargoReportResultsBuilder({super.key});

  static const String route = route_helper.cargoReportResultsBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(context, "Action Blocked",
                  "You cannot change account while performing this action on summary screen.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, CargoReportResultsVM>(
        converter: CargoReportResultsVM.fromStore,
        builder: (context, vm) {
          return CargoReportResultsPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class CargoReportResultsVM {
  bool isLoading;
  AuthState authState;
  Account account;
  UniversalBranding universalBranding;

  final Function(String role) filterCargoMovementsBySelection;
  final PortReportSummary reportSummary;
  final String reportType;
  // FileStore tempStorage;
  final Function(BuildContext context) confirmCargoMovement;
  final Function(int selectedStatusIndex) setSelectedStatusIndex;
  var dateTime = DateFormat.yMMMEd().format(DateTime.now()).toString();
  var time = DateTime.now().toString().substring(10, 20);

  CargoReportResultsVM({
    required this.isLoading,
    required this.authState,
    required this.account,
    required this.universalBranding,
    required this.filterCargoMovementsBySelection,
    required this.reportSummary,
    required this.reportType,

    // required this.accountPortDepot,
    required this.setSelectedStatusIndex,
    required this.confirmCargoMovement,
    // required this.tempStorage,
  });

  static CargoReportResultsVM fromStore(Store<AppState> store) {
    return CargoReportResultsVM(
      isLoading: store.state.isLoading,
      authState: store.state.authState,
      account: store.state.accountState.account,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      reportType: store.state.reportState.reportType,
      reportSummary: store.state.reportState.portReportSummary,
      setSelectedStatusIndex: (int selectedCarrierIndex) {
        store.dispatch(UiCargoHomeSetCarrierIndex(selectedCarrierIndex));
      },
      filterCargoMovementsBySelection: (String role) {
        // if(store.state.reportState.reportType == CadiReportTypes.restow) {
        //   store.dispatch(FilterReportCargoMovementsByRole(role));
        // }else{
        //   store.dispatch(FilterReportShortDataByRole(role));
        // }
      },
      confirmCargoMovement: (BuildContext context) {
        store.dispatch(NavigateToAction.push(
            //#Enhancement - Set Cargo movement Parameters Here!
            route_helper.cargoMovementContainerScreenBuilder));
      },
    );
  }
}
