import 'package:cadi_core_app/factory/ui_factory.dart';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/redux/models/account/account_port.dart';
import 'package:cadi_core_app/redux/models/account/account_port_depot.dart';
import 'package:cadi_core_app/redux/models/account/account_vessel.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';
import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'cargo_report_search_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

class CargoReportSearch extends StatefulWidget {
  final CargoReportSearchVM viewModel;
  const CargoReportSearch({
    super.key,
    required this.viewModel,
  });

  @override
  State<CargoReportSearch> createState() {
    return CargoReportSearchState(viewModel: viewModel);
  }
}

class CargoReportSearchState extends State<CargoReportSearch> {
  final CargoReportSearchVM viewModel;
  final _searchVesselController = TextEditingController();
  final _searchVoyageController = TextEditingController();
  final _selectAccountPortDepotController = TextEditingController();
  final _selectPortController = TextEditingController();
  final _selectCarrierController = TextEditingController();
  final _selectRoleController = TextEditingController();

  static const Key _selectAccountPortDepotKey =
      Key(CargoKeys.selectAccountPortDepotKey);
  static const Key _selectPortKey = Key(CargoKeys.selectPortKey);
  static const Key _searchVesselKey = Key(CargoKeys.searchVesselKey);
  static const Key _searchVoyageKey = Key(CargoKeys.searchVoyageKey);

  int _selectedLocationIndex = 0;
  int _selectedPortIndex = 0;

  int _selectedVesselIndex = 0;
  int _vesselNameLength = 0;

  List<DropdownItem> _accountLocationItems = [];
  List<DropdownItem> _portItems = [];
  List<DropdownItem> _vesselItems = [];

  List<AccountPortDepot> _accountPortDepots = [];

  CargoReportSearchState({
    Key? key,
    required this.viewModel,
  });

  void _setLocation(int index) {
    setState(() {
      _selectAccountPortDepotController.text =
          _accountLocationItems[index].text;
      _selectedLocationIndex = index;
      widget.viewModel.setSelectedLocationIndex(index);
    });
  }

  void _setPort(int index) {
    setState(() {
      _selectPortController.text = _portItems[index].text;
      _selectedPortIndex = index;
      widget.viewModel.setSelectedPortIndex(index);
      _accountPortDepots = widget.viewModel.accountPortDepots
          .where((element) =>
              element.portId == _portItems[_selectedPortIndex].value)
          .toList();

      _accountLocationItems = UiFactory()
          .convertToDropdown<AccountPortDepot, int>(
              _accountPortDepots, (item) => item.depotName, (item) => item.id);

      _selectedLocationIndex = 0;
    });
    if (_accountLocationItems.isNotEmpty &&
        _selectedLocationIndex < _accountLocationItems.length) {
      _selectAccountPortDepotController.text =
          _accountLocationItems[_selectedLocationIndex].text;
    }
  }

  void _setVessel(int index) {
    setState(() {
      _searchVesselController.text = _vesselItems[index].text.toUpperCase();
      _selectedVesselIndex = index;
      _vesselNameLength = _vesselItems[index].text.toUpperCase().length;
    });
    // FocusScope.of(context).requestFocus(voyageNode);
  }

  void _showVesselBottomPicker(String searchParams) {
    if (widget.viewModel.accountVessels.isNotEmpty) {
      var searchList = _vesselItems
          .where((element) =>
              element.text.toUpperCase().startsWith(searchParams.toUpperCase()))
          .toList();
      if (searchList.isNotEmpty) {
        showBottomDropdown(context, 'Vessel', 'Select a Vessel', searchList,
            _selectedVesselIndex, _setVessel, true);
      }
    }
  }

  @override
  initState() {
    super.initState();
    final uiFactory = UiFactory();
    _portItems = uiFactory.convertToDropdown<AccountPort, int>(
        widget.viewModel.accountPorts,
        (port) => port.unLocode,
        (port) => port.portId);

    setState(() {
      _vesselNameLength =
          widget.viewModel.uiCargoReportSearch.vesselName.length;
    });

    if (widget.viewModel.accountVessels.isNotEmpty) {
      _vesselItems = uiFactory.convertToDropdown<AccountVessel, int>(
          widget.viewModel.accountVessels,
          (vessel) => vessel.vesselName,
          (vessel) => vessel.vesselId);
    }

    widget.viewModel.initScreenData(context);

    _accountPortDepots = widget.viewModel.accountPortDepots;

    _selectedPortIndex = widget.viewModel.uiCargoReportSearch.selectedPortIndex;
    _selectedLocationIndex =
        widget.viewModel.uiCargoReportSearch.selectedLocationIndex;

    if (widget.viewModel.accountPorts.isNotEmpty &&
        _selectedPortIndex < widget.viewModel.accountPorts.length) {
      _selectPortController.text = _portItems[_selectedPortIndex].text;
      _accountPortDepots = widget.viewModel.accountPortDepots
          .where((element) =>
              element.portId == _portItems[_selectedPortIndex].value)
          .toList();

      _accountLocationItems = UiFactory()
          .convertToDropdown<AccountPortDepot, int>(
              _accountPortDepots, (item) => item.depotName, (item) => item.id);

      if (_accountLocationItems.isNotEmpty &&
          _selectedLocationIndex < _accountLocationItems.length) {
        _selectAccountPortDepotController.text =
            _accountLocationItems[_selectedLocationIndex].text;
      }
    }
  }

  @override
  void dispose() {
    _selectAccountPortDepotController.dispose();
    _selectPortController.dispose();
    _selectCarrierController.dispose();
    _selectRoleController.dispose();
    widget.viewModel.stopLoading();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final List<SettingTile> allSettings = <SettingTile>[
      SettingTile(
        0,
        'Active Account',
        Container(
          child: Column(
            children: <Widget>[
              Text(
                  "Reports: ${getReportTypeDescription(widget.viewModel.reportType)}",
                  style: TextStyle(
                      fontSize: PlatformSizing().normalTextSize(context),
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),
      SettingTile(
        1,
        'Account Operations',
        Container(
          margin: const EdgeInsets.only(top: 10.0), // Add your own value here
          child: Column(
            children: <Widget>[
              Text("Search by Location, Vessel Name and Voyage Number",
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      fontWeight: FontWeight.w300,
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),
      SettingTile(
          2,
          'PORT',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text("Port",
                    style: TextStyle(
                        fontSize: PlatformSizing().mediumTextSize(context),
                        fontWeight: FontWeight.w300,
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(
          3,
          "",
          Material(
              color: BrandingColors.background,
              child: InkWell(
                onTap: () {
                  if (widget.viewModel.accountPortDepots.isEmpty) {
                    showOkMessage(context, "Cannot select Port",
                        "Account setup is incomplete. Please contact support. ");
                  } else {
                    showBottomDropdown(context, 'Port', 'Please select a port',
                        _portItems, _selectedPortIndex, _setPort, true);
                  }
                },
                child: PlatformTextField(
                  cursorColor: BrandingColors.cadiGrey,
                  textCapitalization: TextCapitalization.characters,
                  controller: _selectPortController,
                  key: _selectPortKey,
                  autocorrect: false,
                  enabled: false,

                  material: (_, __) => MaterialAssetsDark()
                      .textFieldData("", FontAwesomeIcons.chevronDown),
                  cupertino: (_, __) => CupertinoAssets()
                      .textFieldData('Port', FontAwesomeIcons.chevronDown),
                  style: TextStyle(
                      color: BrandingColors.cadiBlack,
                      fontWeight: FontWeight.w400,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  // keyboardType: TextInputType.text,
                ),
              ))),
      SettingTile(
          4,
          'LOCATION',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('Location',
                    style: TextStyle(
                        fontSize: PlatformSizing().mediumTextSize(context),
                        fontWeight: FontWeight.w300,
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(
          5,
          "",
          Material(
              color: BrandingColors.background,
              child: InkWell(
                onTap: () {
                  if (widget.viewModel.accountPortDepots.isEmpty) {
                    showOkMessage(context, "Cannot select Location",
                        "Account setup is incomplete. Please contact support. ");
                  } else {
                    showBottomDropdown(
                        context,
                        'Location',
                        'Please select location',
                        _accountLocationItems,
                        _selectedLocationIndex,
                        _setLocation,
                        true);
                  }
                },
                child: PlatformTextField(
                  cursorColor: BrandingColors.cadiGrey,
                  textCapitalization: TextCapitalization.characters,
                  controller: _selectAccountPortDepotController,
                  key: _selectAccountPortDepotKey,
                  autocorrect: false,
                  enabled: false,
                  material: (_, __) => MaterialAssetsDark()
                      .textFieldData("", FontAwesomeIcons.chevronDown),
                  cupertino: (_, __) => CupertinoAssets()
                      .textFieldData('Location', FontAwesomeIcons.chevronDown),
                  style: TextStyle(
                      color: BrandingColors.cadiBlack,
                      fontWeight: FontWeight.w400,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  keyboardType: TextInputType.text,
                ),
              ))),
      SettingTile(
          6,
          'VESSEL Name',
          Material(
            color: BrandingColors.background,
            child: ListTile(
                title: Text('Vessel Name',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack))),
          )),
      SettingTile(
        7,
        "VESSEL Name INPUT",
        PlatformTextField(
          cursorColor: BrandingColors.cadiGrey,
          onChanged: (dataString) {
            if (dataString.isNotEmpty &&
                dataString.length > _vesselNameLength) {
              _showVesselBottomPicker(dataString);
            }
            setState(() {
              _vesselNameLength = dataString.length;
            });
          },
          textCapitalization: TextCapitalization.characters,
          controller: _searchVesselController,
          key: _searchVesselKey,
          autocorrect: false,
          material: (_, __) =>
              MaterialAssetsDark().textFieldData("", FontAwesomeIcons.search),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData('Vessel Name', null),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontWeight: FontWeight.w400,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
        ),
      ),
      SettingTile(
          8,
          'VOYAGE Number',
          Material(
            color: BrandingColors.background,
            child: ListTile(
                title: Text('Voyage Number',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack))),
          )),
      SettingTile(
        9,
        "VOYAGE NUMBER INPUT",
        PlatformTextField(
          cursorColor: BrandingColors.cadiGrey,
          textCapitalization: TextCapitalization.characters,
          controller: _searchVoyageController,
          key: _searchVoyageKey,
          autocorrect: false,
          material: (_, __) =>
              MaterialAssetsDark().textFieldData("", FontAwesomeIcons.search),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData('Voyage Number', null),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontWeight: FontWeight.w400,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
        ),
      ),
      SettingTile(
          10,
          '',
          Padding(
              padding: const EdgeInsets.only(
                  left: 50.0, top: 30.0, right: 50, bottom: 10),
              child: BaseButton(
                  color: widget.viewModel.universalBranding
                      .getButtonPrimaryColor(),
                  child: Text('Next',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: PlatformSizing().normalTextSize(context))),
                  onPressed: () {
                    if (_searchVesselController.text.trim() == "") {
                      viewModel.showErrorAlert(
                          context, "Please input a vessel name.");
                    } else if (_searchVoyageController.text.trim() == "") {
                      viewModel.showErrorAlert(
                          context, "Please input a voyage number.");
                    } else {
                      viewModel.findCargoMovement(
                          context,
                          widget.viewModel.uiCargoReportSearch.reportType,
                          _portItems[_selectedPortIndex].text,
                          _accountLocationItems[_selectedLocationIndex].value,
                          _searchVesselController.text.trim(),
                          _searchVoyageController.text.trim());
                    }
                  }))),
    ];

    // if (widget.viewModel.reportType == CadiReportTypes.restow) {
    //   allSettings.add(SettingTile(
    //       11,
    //       '',
    //       Material(
    //           color: BrandingColors.background,
    //           child: ListTile(
    //               title: Text("OR ",
    //                   textAlign: TextAlign.center,
    //                   style: TextStyle(
    //                       fontWeight: FontWeight.w600,
    //                       fontSize: PlatformSizing().normalTextSize(context),
    //                       color: BrandingColors.cadiBlack))))));
    //   allSettings.add(SettingTile(
    //       12,
    //       '',
    //       Padding(
    //           padding: const EdgeInsets.only(left: 50.0, top: 10.0, right: 50),
    //           child: BaseGhostButton(
    //               child: Text('Show All',
    //                   style: TextStyle(
    //                       fontWeight: FontWeight.w500,
    //                       fontSize: PlatformSizing().normalTextSize(context))),
    //               onPressed: () {
    //                 viewModel.findCargoMovement(
    //                     context,
    //                     widget.viewModel.uiCargoReportSearch.reportType,
    //                     _portItems[_selectedPortIndex].text,
    //                     _accountLocationItems[_selectedLocationIndex].value,
    //                     "",
    //                     "");
    //               }))));
    // }

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Searching...");
    }

    return Padding(
      padding: const EdgeInsets.only(top: 0, left: 40, right: 40),
      child: ListView.builder(
          shrinkWrap: true,
          //itemBuilder: itemBuilder) .separated(
          itemCount: allSettings.length,
          padding: const EdgeInsets.only(
            left: 10.0,
            right: 10,
          ),
          // separatorBuilder: (BuildContext context, int index) =>
          //     Divider(height: 1),
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
  }
}
