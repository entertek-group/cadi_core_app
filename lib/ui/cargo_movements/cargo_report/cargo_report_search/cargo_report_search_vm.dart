import 'dart:async';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_home_button.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_user_account.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'cargo_report_search_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

class CargoReportSearchBuilder extends StatelessWidget {
  const CargoReportSearchBuilder({super.key});

  static const String route = route_helper.cargoReportSearchBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(
                  // FontAwesomeIcons.warning,
                  context,
                  "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, CargoReportSearchVM>(
        converter: CargoReportSearchVM.fromStore,
        builder: (context, vm) {
          return CargoReportSearch(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class CargoReportSearchVM {
  bool isLoading;
  AuthState authState;
  Account account;
  List<AccountPortDepot> accountPortDepots;
  List<AccountPort> accountPorts;
  List<AccountVessel> accountVessels;
  UiCargoReportSearch uiCargoReportSearch;
  UniversalBranding universalBranding;

  final String reportType;
  final Function(BuildContext context, String message) showErrorAlert;
  final Function(BuildContext, String, int, int) onNavigatePressed;
  final Function() stopLoading;
  final Function(int selectedPortIndex) setSelectedPortIndex;
  final Function(int selectedLocationIndex) setSelectedLocationIndex;
  final Function(BuildContext context) initScreenData;
  final Function(BuildContext context) showAllResults;
  final Function(
      BuildContext context,
      String reportType,
      String unLocode,
      int accountPortDepotId,
      String vesselName,
      String voyageNumber) findCargoMovement;
  CargoReportSearchVM(
      {
        required this.isLoading,
        required this.stopLoading,
        required this.authState,
        required this.account,
        required this.accountPortDepots,
        required this.accountPorts,
        required this.accountVessels,
        required this.reportType,
        required this.uiCargoReportSearch,
        required this.universalBranding,
        required this.onNavigatePressed,
        required this.setSelectedPortIndex,
        required this.setSelectedLocationIndex,
        required this.showErrorAlert,
        required this.findCargoMovement,
        required this.showAllResults,
        required this.initScreenData
      });

  static CargoReportSearchVM fromStore(Store<AppState> store) {
    return CargoReportSearchVM(
        isLoading: store.state.isLoading,
        authState: store.state.authState,
        account: store.state.accountState.account,
        accountPortDepots: store.state.accountState.filteredAccountPortDepots,
        accountPorts: store.state.accountState.filteredAccountPorts,
        accountVessels: store.state.accountState.filteredAccountVessels,
        uiCargoReportSearch: store.state.uiState.uiCargoReportSearch,
        universalBranding:
            UniversalBranding(store.state.accountState.activeService),
        reportType:
           store.state.reportState.reportType,
        stopLoading: () {
          store.dispatch(StopLoading());
        },
        initScreenData: (context) {
          store.dispatch(ResetCargoMovementCreate());
        },
        showErrorAlert: (BuildContext context, String message) {
          showOkMessage(
              // FontAwesomeIcons.warning,
              context,
              "Submit Error",
              message);
        },
        onNavigatePressed: (BuildContext context, String role,
            int accountPortDepotId, int carrierId) {
          if (store.state.isLoading) {
            return;
          }
          store.dispatch(
              NavigateToAction.push(route_helper.cargoReportResultsBuilder));
        },
        showAllResults: (BuildContext context) {
          store.dispatch(
              NavigateToAction.push(route_helper.cargoReportShowAllBuilder));
        },
        setSelectedPortIndex: (int selectedPortIndex) {
          store.dispatch(UiCargoReportSetPortIndex(selectedPortIndex));
        },
        setSelectedLocationIndex: (int selectedLocationIndex) {
          store.dispatch(UiCargoReportSetLocationIndex(selectedLocationIndex));
        },
        findCargoMovement: (BuildContext context,
            String reportType,
            String unLocode,
            int accountPortDepotId,
            String vesselName,
            String voyageNumber) {

          store.dispatch(
              UiCargoReportSetSailingSchedule(vesselName, voyageNumber));

          final Completer<ApiResponse> completer = Completer<ApiResponse>();

          store.dispatch(GetPortReport(completer, unLocode, vesselName, voyageNumber, reportType));


          completer.future.then((ApiResponse apiResponse) {             
            if (apiResponse.success) {
              if(store.state.reportState.reportType == PortReportType.movementStatus){
                store.dispatch(
                    NavigateToAction.push(route_helper.cargoMovementReportsBuilder));
              } else {
                store.dispatch(
                    NavigateToAction.push(route_helper.cargoReportResultsBuilder));
              }
            } else {
              //show error
              showOkMessage(context, "Reports",
                  "No reports found. Please change search criteria.");
            } 
          });
        });
  }
}
