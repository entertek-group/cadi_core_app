// import 'dart:core';
// import 'package:cadi_core_app/helpers/constants.dart';
// import 'package:cadi_core_app/keys/keys.dart';
// import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
// import 'package:cadi_core_app/ui/shared/components/app/setting_tile.dart';
// import 'package:cadi_core_app/ui/shared/components/alerts.dart';
// import 'package:cadi_core_app/ui/shared/components/picker.dart';
// import 'package:flutter/material.dart';
// import 'package:cadi_core_app/theme/branding.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'cargo_report_show_all_vm.dart';
// import 'package:cadi_core_app/ui/shared/components/base/base.dart';
//
// class CargoReportShowAllPage extends StatefulWidget {
//   final CargoReportShowAllVM viewModel;
//   const CargoReportShowAllPage({
//     Key? key,
//     required this.viewModel,
//   }) : super(key: key);
//
//   @override
//   State<CargoReportShowAllPage> createState() {
//     return CargoReportShowAllState(viewModel: this.viewModel);
//   }
// }
//
// class CargoReportShowAllState extends State<CargoReportShowAllPage> {
//   final CargoReportShowAllVM viewModel;
//   final _selectStatusController = TextEditingController();
//
//   String movementStatus = "";
//   String movementType = "";
//   static const Key _selectStatusKey = Key(CargoKeys.selectStatusKey);
//
//   int _selectedStatusIndex = 0;
//   List<PickerItem> _statusItems = [];
//   String _selectedVesselName = "";
//
//   CargoReportShowAllState({
//     Key? key,
//     required this.viewModel,
//   });
//
//   void _refreshStatus(int index) {
//     setState(() {
//       _selectStatusController.text = _statusItems[index].text;
//       _selectedStatusIndex = index;
//       //widget.viewModel.setSelectedStatusIndex(index);
//     });
//   }
//
//   void _setSelectedVessel(String vesselName) {
//     setState(() {
//       _selectedVesselName = _selectedVesselName == vesselName ? "" : vesselName;
//     });
//   }
//
//   final TextEditingController _textEditingController = TextEditingController();
//
//   @override
//   initState() {
//     _setSelectedVessel(widget.viewModel.filterVesselName);
//     super.initState();
//   }
//
//   @override
//   void dispose() {
//     super.dispose();
//   }
//   // ignore: non_constant_identifier_names
//
//   @override
//   Widget build(BuildContext context) {
//     var screenHeight = MediaQuery.of(context).size.height;
//
//     List<SettingTile> allOptions = <SettingTile>[
//       SettingTile(
//           0,
//           'DISPLAY TYPE',
//           Material(
//               color: BrandingColors.background,
//               child: ListTile(
//                   title: Text("Reports: " + getReportTypeDescription(widget.viewModel.reportType),
//                       // getCargoMovementRoleDescription(
//                       //     widget.viewModel.cargoRole),
//                       style: TextStyle(
//                           fontSize: PlatformSizing().normalTextSize(context),
//                           color: BrandingColors.cadiBlack))))),
//       SettingTile(
//           1,
//           'DISPLAY STATUS',
//           Material(
//               color: BrandingColors.background,
//               child: ListTile(
//                   title: Text("All Results",
//                       textAlign: TextAlign.left,
//                       style: TextStyle(
//                           fontSize: PlatformSizing().smallTextSize(context),
//                           color: BrandingColors.cadiBlack))))),
//     ];
//
//     List<TableRow> summaryTableRows = [];
//
//     summaryTableRows.add(buildTableRow(
//         context,
//         () => {},
//         buildTableTitle(" "),
//         buildTableTitle("VESSEL"),
//         buildTableCountTitle("COUNT"),
//         buildStatusTitle("PORT")));
//
//     for (var vessel in widget.viewModel.vesselSummary) {
//       summaryTableRows.add(buildTableRow(
//           context,
//           () => {_setSelectedVessel(vessel.vesselName)},
//           buildTableIcon(
//               vessel.vesselName == _selectedVesselName ? true : false),
//           buildTableText(vessel.vesselName),
//           buildTableCount(vessel.cargoCount.toString()),
//           buildStatusText(vessel.unLocode)));
//     }
//
//     allOptions.add(SettingTile(
//         20,
//         'Summary',
//         Material(
//             color: BrandingColors.background,
//             child: Padding(
//               padding: const EdgeInsets.only(
//                 top: 20,
//               ),
//               child: buildScrollableTable(
//                   buildSummaryTable(context, summaryTableRows),
//                   screenHeight * 0.5),
//             ))));
//     allOptions.add(SettingTile(
//         7,
//         '',
//         Padding(
//             padding:
//                 const EdgeInsets.symmetric(horizontal: 50.0, vertical: 30.0),
//             child: BaseButton(
//                 color:
//                     widget.viewModel.universalBranding.getButtonPrimaryColor(),
//                 child: Text('Next',
//                     style: TextStyle(
//                         fontSize: PlatformSizing().normalTextSize(context))),
//                 onPressed: () {
//                   if (_selectedVesselName == "") {
//                     showOkMessage(
//                         context, "Make selection", "Please select a vessel");
//                   } else {
//                     widget.viewModel.selectReport(context, _selectedVesselName);
//                   }
//                   ;
//                 }))));
//
//     if (widget.viewModel.isLoading) {
//       return const AppLoading(message: "Loading...");
//     }
//
//     return Padding(
//       padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
//       child: ListView.builder(
//           itemCount: allOptions.length,
//           padding: const EdgeInsets.only(left: 20.0, right: 20.0),
//           // separatorBuilder: (BuildContext context, int index) =>
//           //     const Divider(height: 1),
//           itemBuilder: (BuildContext context, int index) {
//             return allOptions[index].body;
//           }),
//     );
//     // );
//     //return new SafeArea(child: Text("Setting Index placeholder"));
//   }
// }
//
// Container buildTableText(String text) {
//   return Container(
//     height: 50,
//     alignment: Alignment.centerLeft,
//     child: Text(text,
//         style: const TextStyle(
//             color: BrandingColors.cadiBlack,
//             fontWeight: FontWeight.w300,
//             fontSize: 15)),
//   );
// }
//
// // COUNT
// Container buildTableCount(String text) {
//   return Container(
//     alignment: Alignment.center,
//     height: 50,
//     child: Padding(
//       padding: const EdgeInsets.only(left: 20),
//       child: Text(text,
//           style: const TextStyle(
//               color: BrandingColors.cadiBlack,
//               fontWeight: FontWeight.w300,
//               fontSize: 15)),
//     ),
//   );
// }
//
// Container buildTableCountTitle(String text) {
//   return Container(
//     alignment: Alignment.centerRight,
//     child: Padding(
//       padding: const EdgeInsets.only(bottom: 12, top: 10),
//       child: Text(text,
//           style: const TextStyle(
//               color: BrandingColors.cadiBlack,
//               fontWeight: FontWeight.w800,
//               fontSize: 15)),
//     ),
//   );
// }
//
// // ICON
// Container buildTableIcon(bool selected) {
//   return Container(
//     alignment: Alignment.center,
//     height: 50,
//     child: selected
//         ? const Icon(FontAwesomeIcons.solidSquareCheck,
//             color: BrandingColors.cadiGreen)
//         : const Icon(
//             FontAwesomeIcons.solidSquare,
//             color: BrandingColors.cadiGrey,
//           ),
//   );
// }
//
// // STATUS
// // #TODO if statement for status
// Container buildStatusText(String text) {
//   return Container(
//     alignment: Alignment.centerRight,
//     height: 40,
//     child: Text(text,
//         style: const TextStyle(
//             color: BrandingColors.cadiGreen,
//             fontWeight: FontWeight.w800,
//             fontSize: 15)),
//   );
// }
//
// Container buildStatusTitle(String text) {
//   return Container(
//     alignment: Alignment.centerRight,
//     child: Padding(
//       padding: const EdgeInsets.only(
//         bottom: 12,
//         top: 10,
//       ),
//       child: Text(text,
//           style: const TextStyle(
//               color: BrandingColors.cadiBlack,
//               fontWeight: FontWeight.w800,
//               fontSize: 15)),
//     ),
//   );
// }
//
// // TITLE
// Container buildTableTitle(String text) {
//   return Container(
//     alignment: Alignment.centerLeft,
//     child: Padding(
//       padding: const EdgeInsets.only(
//         bottom: 12,
//         top: 10,
//       ),
//       child: Text(text,
//           style: const TextStyle(
//               color: BrandingColors.cadiBlack,
//               fontWeight: FontWeight.w800,
//               fontSize: 15)),
//     ),
//   );
// }
//
// TableRow buildTableRow(BuildContext context, Function() onTap, Widget rowIcon,
//     Widget rowItemOne, Widget rowItemTwo, Widget rowItemThree) {
//   return TableRow(children: [
//     Material(
//         color: BrandingColors.background,
//         child: TableRowInkWell(
//           onTap: onTap,
//           child: rowIcon,
//         )),
//     Material(
//         color: BrandingColors.background,
//         child: TableRowInkWell(onTap: onTap, child: rowItemOne)),
//     Material(
//         color: BrandingColors.background,
//         child: TableRowInkWell(onTap: onTap, child: rowItemTwo)),
//     Material(
//         color: BrandingColors.background,
//         child: TableRowInkWell(onTap: onTap, child: rowItemThree)),
//   ]);
// }
//
// Table buildSummaryTable(BuildContext context, List<TableRow> tableRows) {
//   return Table(
//     children: tableRows,
//   );
// }
//
// Container buildScrollableTable(Table table, double screenHeight) {
//   return Container(
//       color: BrandingColors.background,
//       height: screenHeight,
//       child: Scrollbar(
//           child: SingleChildScrollView(
//               scrollDirection: Axis.vertical, child: table)));
// }
