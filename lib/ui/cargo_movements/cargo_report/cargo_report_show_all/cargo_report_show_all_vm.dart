// import 'package:cadi_core_app/factory/report_factory.dart';
// import 'package:cadi_core_app/helpers/constants.dart';
// import 'package:cadi_core_app/redux/actions/actions.dart';
// import 'package:cadi_core_app/redux/actions/report_actions.dart';
// import 'package:cadi_core_app/redux/models/models.dart';
// import 'package:cadi_core_app/theme/theme.dart';
// import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
// import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
// import 'package:cadi_core_app/ui/shared/components/alerts.dart';
// import 'package:cadi_core_app/ui/cargo_movements/cargo_report/cargo_report_show_all/cargo_report_show_all_page.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/widgets.dart';
// import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
// import 'package:flutter_redux/flutter_redux.dart';
// import 'package:cadi_core_app/redux/state/states.dart';
// import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
// import 'package:intl/intl.dart';
// import 'package:redux/redux.dart';
// import 'package:cadi_core_app/helpers/routes.dart' as _route_helper;
//
// class CargoReportShowAllBuilder extends StatelessWidget {
//   const CargoReportShowAllBuilder({Key? key}) : super(key: key);
//
//   static const String route = _route_helper.cargoReportShowAllBuilder;
//
//   @override
//   Widget build(BuildContext context) {
//     return PlatformScaffold(
//       backgroundColor: BrandingColors.cadiWhite,
//       appBar: appBar(
//           context,
//           appBarUserAccount(
//               context,
//               () => showOkMessage(context, "Action Blocked",
//                   "You cannot change account while performing this action on summary screen.")),
//           null,
//           <Widget>[
//             appBarHomeButton(context, true),
//           ]),
//       body: StoreConnector<AppState, CargoReportShowAllVM>(
//         converter: CargoReportShowAllVM.fromStore,
//         builder: (context, vm) {
//           return CargoReportShowAllPage(
//             viewModel: vm,
//           );
//         },
//       ),
//     );
//   }
// }
//
// class CargoReportShowAllVM {
//   bool isLoading;
//   AuthState authState;
//   Account account;
//   UiCargoReportSearch uiCargoReportSearch;
//   UniversalBranding universalBranding;
//
//   List<CargoMovementReport> cargoMovements;
//   List<ShortReport> shortReportData;
//   List<VesselSummary> vesselSummary;
//   String filterVesselName;
//   final String reportType;
//   // FileStore tempStorage;
//
//   final Function(BuildContext context, String vesselName) selectReport;
//   var dateTime = DateFormat.yMMMEd().format(DateTime.now()).toString();
//   var time = DateTime.now().toString().substring(10, 20);
//
//   CargoReportShowAllVM(
//       {
//         required this.isLoading,
//         required this.authState,
//         required this.account,
//         required this.uiCargoReportSearch,
//         required this.universalBranding,
//         required this.cargoMovements,
//         required this.shortReportData,
//         required this.vesselSummary,
//         required this.filterVesselName,
//         required this.selectReport,
//         required this.reportType
//       });
//
//   static CargoReportShowAllVM fromStore(Store<AppState> store) {
//     return CargoReportShowAllVM(
//         isLoading: store.state.isLoading,
//         authState: store.state.authState,
//         account: store.state.accountState.account,
//         uiCargoReportSearch: store.state.uiState.uiCargoReportSearch,
//         universalBranding:
//             UniversalBranding(store.state.accountState.activeService),
//         cargoMovements: store.state.reportState.cargoMovements,
//         shortReportData: store.state.reportState.shortReportData,
//         filterVesselName: store.state.reportState.filterVesselName,
//         reportType:
//             store.state.reportState.reportType,
//         vesselSummary: <VesselSummary>[VesselSummary.initial()],
//         selectReport: (BuildContext context, String vesselName) {
//           // store.dispatch(SetFilterVesselName(vesselName));
//           // store
//           //     .dispatch(FilterReportCargoMovementsByRole(ReportFilterRole.all));
//           // store.dispatch(BuildReportSummary(vesselName));
//           // store.dispatch(
//           //     NavigateToAction.push(_route_helper.cargoReportResultsBuilder));
//           // //Navigate
//         });
//   }
// }
