import 'package:cadi_core_app/ui/cargo_movements/port_cargo_updates/port_cargo_update_completion/port_cargo_update_completion_vm.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/setting_tile.dart';
import 'package:intl/intl.dart';

class PortCargoUpdateCompletionPage extends StatefulWidget {
  final PortCargoUpdateCompletionVM viewModel;
  const PortCargoUpdateCompletionPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<PortCargoUpdateCompletionPage> createState() {
    return PortCargoUpdateCompletionPageState(viewModel: viewModel);
  }
}

class PortCargoUpdateCompletionPageState
    extends State<PortCargoUpdateCompletionPage> {
  final PortCargoUpdateCompletionVM viewModel;
  String movementStatus = "";
  String movementType = "";

  //final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  PortCargoUpdateCompletionPageState({
    Key? key,
    required this.viewModel,
  });

  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    List<SettingTile> allOptions = <SettingTile>[];

    DateTime now = DateTime.now();
    String formattedDate = DateFormat('dd-MM-yyyy kk:mm:ss').format(now);

    allOptions.add(SettingTile(
      0,
      'Active Account',
      Column(
        children: <Widget>[
          Text("Cargo Update Complete",
              style: TextStyle(
                  fontSize: PlatformSizing().normalTextSize(context),
                  color: BrandingColors.cadiBlack)),
          // Any additional widget/codes
        ],
      ),
    ));

    allOptions.add(SettingTile(
      0,
      'Active Account',
      Column(
        children: <Widget>[
          Text("Confirmation",
              style: TextStyle(
                  fontSize: PlatformSizing().smallTextSize(context),
                  color: BrandingColors.cadiBlack)),
          // Any additional widget/codes
        ],
      ),
    ));

    allOptions.add(SettingTile(
        2,
        '',
        Container(
            padding: EdgeInsets.only(
                top: screenHeight * 0.08, bottom: screenHeight * 0.03),
            child: buildCompleteImage(context))));

    allOptions.add(SettingTile(
        3,
        '',
        Column(
          children: [
            Text("Cargo Update Confirmed",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: PlatformSizing().mediumTextSize(context),
                    color: BrandingColors.cadiBlack)),
            Text(
                viewModel.portCargoUpdateCreate.updateAt == ""
                    ? formattedDate
                    : DateFormat('dd-MM-yyyy kk:mm:ss').format(DateTime.parse(
                        viewModel.portCargoUpdateCreate.updateAt).toLocal()),
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    color: BrandingColors.cadiBlack))
          ],
        )));

    allOptions.add(SettingTile(
        7,
        '',
        Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 40.0, vertical: 0.0),
            child: Padding(
                padding: EdgeInsets.only(top: screenHeight * 0.13),
                child: BaseButton(
                    color: widget.viewModel.universalBranding
                        .getButtonPrimaryColor(),
                    child: Text('New Update',
                        style: TextStyle(
                            fontSize:
                                PlatformSizing().normalTextSize(context))),
                    onPressed: () {
                      viewModel.newPortCargoUpdate(context);
                    })))));

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          itemCount: allOptions.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20),
          // separatorBuilder: (BuildContext context, int index) =>
          //     const Divider(height: 1),
          itemBuilder: (BuildContext context, int index) {
            return allOptions[index].body;
          }),
    );
  }
}

Widget buildCompleteImage(BuildContext context) {
  var maxWidth = MediaQuery.of(context).size.width * 0.3;
  var maxHeight = MediaQuery.of(context).size.height * 0.25;
  return SizedBox(
      height: maxHeight,
      width: maxWidth,
      child: Container(
          alignment: Alignment.center,
          decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: BrandingColors.white,
              image: DecorationImage(
                image: AssetImage(BrandingImages.cargoCheck),
                fit: BoxFit.scaleDown,
              ))));
}
