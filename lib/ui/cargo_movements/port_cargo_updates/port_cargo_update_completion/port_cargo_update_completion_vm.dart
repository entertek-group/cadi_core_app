// ignore_for_file: prefer_function_declarations_over_variables
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/theme.dart';
import 'package:cadi_core_app/ui/cargo_movements/port_cargo_updates/port_cargo_update_completion/port_cargo_update_completion_page.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class PortCargoUpdateCompletionScreenBuilder extends StatelessWidget {
  const PortCargoUpdateCompletionScreenBuilder({super.key});

  static const String route =
      route_helper.portCargoUpdateCompletionScreenBuilder;
  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(
                  // FontAwesomeIcons.warning,
                  context,
                  "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, PortCargoUpdateCompletionVM>(
        converter: PortCargoUpdateCompletionVM.fromStore,
        builder: (context, vm) {
          return PortCargoUpdateCompletionPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class PortCargoUpdateCompletionVM {
  bool isLoading;
  AuthState authState;
  Account account;
  UniversalBranding universalBranding;
  final Function(BuildContext, String) onNavigatePressed;
  final Function(String token) setToken;
  final Function(BuildContext context) newPortCargoUpdate;
  PortCargoUpdateCreate portCargoUpdateCreate;

  PortCargoUpdateCompletionVM({
    required this.isLoading,
    required this.authState,
    required this.account,
    required this.universalBranding,
    required this.onNavigatePressed,
    required this.setToken,
    required this.newPortCargoUpdate,
    required this.portCargoUpdateCreate,
  });

  static PortCargoUpdateCompletionVM fromStore(Store<AppState> store) {
    return PortCargoUpdateCompletionVM(
      isLoading: store.state.isLoading,
      authState: store.state.authState,
      account: store.state.accountState.account,
      portCargoUpdateCreate:
          store.state.portCargoUpdateState.portCargoUpdateCreate,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      onNavigatePressed: (BuildContext context, String route) {
        if (store.state.isLoading) {
          return;
        }
        navigation_helper.navigateRoute(route, store, null);
      },
      setToken: (String token) {},
      newPortCargoUpdate: (BuildContext context) {
        store
            .dispatch(SetHomeIndex(HomeNavigationConstants.portCargoUpdateIndex));
        store.dispatch(NavigateToAction.replace(route_helper.homeScreen));
      },
    );
  }
}
