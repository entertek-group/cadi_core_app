import 'package:cadi_core_app/factory/ui_factory.dart';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/helpers/helpers.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_generic.dart';
import 'package:cadi_core_app/ui/cargo_movements/port_cargo_updates/port_cargo_update_container/port_cargo_update_container_vm.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/base/base_move_event_select.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/setting_tile.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

class PortCargoUpdateContainerPage extends StatefulWidget {
  final PortCargoUpdateContainerVM viewModel;
  const PortCargoUpdateContainerPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<PortCargoUpdateContainerPage> createState() {
    return PortCargoUpdateContainerPageState(viewModel: viewModel);
  }
}

class PortCargoUpdateContainerPageState
    extends State<PortCargoUpdateContainerPage> {
  final PortCargoUpdateContainerVM viewModel;
  String _selectedShipmentStatus = "";

  final _inputContainerOneController = TextEditingController();
  final _inputContainerTwoController = TextEditingController();
  final _selectCargoTypeController = TextEditingController();
  final _selectJobPackTypeController = TextEditingController();

  String _inputContainerOneTitle = "Bill of Lading";
  String _inputContainerTwoTitle = "Container Number";

  bool _inputOneVisible = true;
  bool _inputTwoVisible = true;
  bool _selectJobPackTypeVisible = false;

  final FocusNode inputContainerOneNode = FocusNode();
  final FocusNode inputContainerTwoNode = FocusNode();

  static const Key _inputContainerOneKey = Key(CargoKeys.inputContainerOneKey);
  static const Key _inputContainerTwoKey = Key(CargoKeys.inputContainerTwoKey);
  static const Key _selectJobPackTypeKey = Key(CargoKeys.selectJobPackTypeKey);

  List<DropdownItem> _jobPackTypeItems = [];

  int _selectedJobPackTypeIndex = 0;

  bool _overrideContainerFormat = false;

  final _weightController = TextEditingController();
  String _selectedWeightUnit = "";
  int _selectedWeightUnitIndex = 0;
  List<DropdownItem> _weightUnitItems = [];

  PortCargoUpdateContainerPageState({
    required this.viewModel,
  });


  void _setJobPackType(int index, [bool initiatePicker = true]) {
    setState(() {
      _selectJobPackTypeController.text = _jobPackTypeItems[index].text;
      _selectedJobPackTypeIndex = index;
    });

    if (initiatePicker &&
        widget.viewModel.settingsAutoDisplayNextDropdownOrInput &&
        widget.viewModel.settingsAutoSubmitPage) {
      Future.delayed(const Duration(milliseconds: 300), goNext);
    }
  }

  void _setWeightUnit(int index, [bool initiatePicker = true]) {
    setState(() {
      _selectedWeightUnitIndex = index;
      _selectedWeightUnit = _weightUnitItems[index].value;
    });
  }

  void _showWeightUnitBottomPicker() {
    if (_weightUnitItems.isEmpty) {
      showOkMessage(context, "Cannot select Weight Unit",
          "Account setup is incomplete. Please contact support.");
    } else {
      showBottomDropdown(context, 'Weight Unit', 'Please select a Weight Unit',
          _weightUnitItems, _selectedWeightUnitIndex, _setWeightUnit, true);
    }
  }

  @override
  initState() {
    super.initState();



    _jobPackTypeItems = UiFactory().convertToDropdown<CadiGeneric, String>(
        widget.viewModel.jobPackTypes,
        (item) => item.description,
        (item) => item.code);

    if (widget.viewModel.jobPackTypes.isNotEmpty) {
      var i = widget.viewModel.jobPackTypes
          .indexWhere((element) => element.code == "PLT");
      int packTypeIndex = i < 0 ? 0 : i;
      _setJobPackType(packTypeIndex, false);
    }

    _weightUnitItems = UiFactory().convertToDropdown<CadiEnum, String>(
        widget.viewModel.weightUnits,
        (item) => item.description,
        (item) => item.id);

    if (_weightUnitItems.isNotEmpty) {
      _setWeightUnit(0, false);
    }

    if (widget.viewModel.portCargoUpdateCreate.shipment != "") {
      setState(() {
        _selectedShipmentStatus =
            widget.viewModel.portCargoUpdateCreate.shipment;
      });
      _setShipment(false);
      if (_selectedShipmentStatus == CadiPortCargoUpdateShipment.container) {
        _inputContainerOneController.text = widget
            .viewModel.portCargoUpdateCreate.containerData.containerNumber;
      } else {
        _inputContainerOneController.text =
            widget.viewModel.portCargoUpdateCreate.packData.packSequenceNumber;
        _inputContainerTwoController.text = "";
      }
    } else {
      _selectedShipmentStatus = CadiPortCargoUpdateShipment.container;
    }
  }

  @override
  void dispose() {
    _inputContainerOneController.dispose();
    _inputContainerTwoController.dispose();
    _selectCargoTypeController.dispose();
    _selectJobPackTypeController.dispose();
    _weightController.dispose();
    super.dispose();
  }

  _setShipment([bool initiateFocus = true]) {
    switch (_selectedShipmentStatus) {
      case CadiPortCargoUpdateShipment.container:
        setState(() {
          _inputContainerOneTitle = "Bill of Lading";
          _inputOneVisible = true;
          _inputContainerTwoTitle = "Container Number";
          _inputTwoVisible = true;
          _selectJobPackTypeVisible = false;
        });
        break;
      case CadiPortCargoUpdateShipment.breakBulk:
        setState(() {
          _inputContainerOneTitle = "Bill of Lading";
          _inputOneVisible = true;
          _inputContainerTwoTitle = "Pack Line Number";
          _inputTwoVisible = true;
          _selectJobPackTypeVisible = true;
        });
        break;
      default:
        setState(() {
          _inputContainerOneTitle = "Bill of Lading";
          _inputOneVisible = true;
          _inputContainerTwoTitle = "Container Number";
          _inputTwoVisible = true;
          _selectJobPackTypeVisible = false;
        });
        break;
    }
    if (initiateFocus) {
      FocusScope.of(context).requestFocus(inputContainerOneNode);
    }
  }


  void _showJobPackTypeBottomPicker() {
    if (widget.viewModel.jobPackTypes.isEmpty) {
      showOkMessage(context, "Cannot select Pack Type",
          "Account setup is incomplete. Please contact support. ");
    } else {
      showBottomDropdown(context, 'Pack Type', 'Please select a Pack Type',
          _jobPackTypeItems, _selectedJobPackTypeIndex, _setJobPackType, true);
    }
  }

  bool _validateWeight() {
    if (_weightController.text.trim().isEmpty) {
      viewModel.showErrorAlert(context, "Please enter a weight value");
      return false;
    }

    try {
      num.parse(_weightController.text.trim());
      return true;
    } catch (e) {
      viewModel.showErrorAlert(context, "Please enter a valid weight value");
      return false;
    }
  }

  void goNext() {
    if ((_selectedShipmentStatus == CadiPortCargoUpdateShipment.container)) {
      if (_inputContainerOneController.text.trim() == "") {
        viewModel.showErrorAlert(context, "Please input a Bill of Lading");
        return;
      } else if (_inputContainerTwoController.text.trim() == "") {
        viewModel.showErrorAlert(context, "Please input a container number");
        return;
      } else if (!_overrideContainerFormat &&
          !isContainerInputValid(
              context, _inputContainerTwoController.text.trim())) {
        showMessageWithActions(
            context,
            "Container Number Format",
            WarningMessages.containerOverrideMessage,
            () {
              setState(() {
                _overrideContainerFormat = true;
              });
              Navigator.pop(context);
              if (_validateWeight()) {
                goNextViewModel();
              }
            },
            "Accept",
            () {
              Navigator.pop(context);
            },
            "Cancel");
        return;
      }
    } else if (_selectedShipmentStatus == CadiPortCargoUpdateShipment.breakBulk) {
      if (_inputContainerOneController.text.trim() == "") {
        viewModel.showErrorAlert(context, "Please input a Bill of Lading");
        return;
      } else if (_inputContainerTwoController.text.trim() == "") {
        viewModel.showErrorAlert(context, "Please input a pack line number.");
        return;
      }
    }

    if (_validateWeight()) {
      goNextViewModel();
    }
  }

  void goNextViewModel() {
    viewModel.setPortCargoUpdateContainerVariables(
        context,
        _selectedShipmentStatus,
        _inputContainerOneController.text,
        _inputContainerTwoController.text,
        num.parse(_weightController.text.trim()),
        _selectedWeightUnit,
        _jobPackTypeItems.isNotEmpty
            ? _jobPackTypeItems[_selectedJobPackTypeIndex].value
            : "",
        true);
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> moveEventWidgets = <Widget>[];
    for (var cadiEnum in widget.viewModel.portCargoUpdateShipments) {
      var color = _selectedShipmentStatus != cadiEnum.id
          ? BrandingColors.white
          : BrandingColors.cadiGrey;

      moveEventWidgets.add(buildContainerCard(
        context,
        "",
        () => {
          setState(() {
            _selectedShipmentStatus = cadiEnum.id;
          }),
          _setShipment(),
        },
        color,
        cadiEnum.description,
      ));
    }

    List<SettingTile> allOptions = <SettingTile>[];

    allOptions.addAll([
      SettingTile(
        0,
        'Active Account',
        Column(
          children: <Widget>[
            Center(
              child: Text(
                "Cargo Update:",
                style: TextStyle(
                  fontSize: PlatformSizing().normalTextSize(context),
                  color: BrandingColors.cadiBlack,
                ),
              ),
            ),
            Center(
              child: Text(
                getPortCargoUpdateRoleDescription(
                    widget.viewModel.portCargoUpdateRole),
                style: TextStyle(
                  fontSize: PlatformSizing().normalTextSize(context),
                  color: BrandingColors.cadiBlack,
                ),
              ),
            ),
          ],
        ),
      ),
      SettingTile(
        1,
        'Account Operations',
        Container(
          margin: const EdgeInsets.only(
              top: 10.0, bottom: 15), // Add your own value here
          child: Column(
            children: <Widget>[
              Text("Enter Shipment details",
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),
      SettingTile(
          2,
          "Container List",
          Material(
              color: BrandingColors.cadiWhite,
              child: buildMoveEventList(context, moveEventWidgets))),
      SettingTile(
          3,
          'INPUT ONE',
          Material(
            color: BrandingColors.cadiWhite,
            child: ListTile(
                title: Text(_inputContainerOneTitle,
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack))),
          )),
      SettingTile(
          4,
          '',
          _inputOneVisible
              ? PlatformTextField(
                  cursorColor: BrandingColors.cadiGrey,
                  focusNode: inputContainerOneNode,
                  textCapitalization: TextCapitalization.characters,
                  controller: _inputContainerOneController,
                  key: _inputContainerOneKey,
                  autocorrect: false,
                  material: (_, __) => MaterialAssetsDark()
                      .textFieldData("", FontAwesomeIcons.magnifyingGlass),
                  cupertino: (_, __) => CupertinoAssets()
                      .textFieldData(_inputContainerOneTitle, null),
                  style: TextStyle(
                      color: BrandingColors.cadiBlack,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  keyboardType: TextInputType.text,
                  onEditingComplete: () {
                    if (widget
                        .viewModel.settingsAutoDisplayNextDropdownOrInput) {
                      if (_selectedShipmentStatus ==
                          CadiPortCargoUpdateShipment.container) {
                        FocusScope.of(context)
                            .requestFocus(inputContainerTwoNode);
                      } else if (_selectedShipmentStatus ==
                          CadiPortCargoUpdateShipment.breakBulk) {
                        FocusScope.of(context)
                            .requestFocus(inputContainerTwoNode);
                      }
                    }
                  },
                )
              : Container()),
      SettingTile(
          5,
          'INPUT TWO',
          Material(
            color: BrandingColors.cadiWhite,
            child: ListTile(
                title: Text(_inputContainerTwoTitle,
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack))),
          )),
      SettingTile(
          6,
          '',
          _inputTwoVisible
              ? PlatformTextField(
                  cursorColor: BrandingColors.cadiGrey,
                  focusNode: inputContainerTwoNode,
                  textCapitalization: TextCapitalization.characters,
                  controller: _inputContainerTwoController,
                  key: _inputContainerTwoKey,
                  autocorrect: false,
                  material: (_, __) => MaterialAssetsDark()
                      .textFieldData("", FontAwesomeIcons.magnifyingGlass),
                  cupertino: (_, __) => CupertinoAssets()
                      .textFieldData(_inputContainerTwoTitle, null),
                  style: TextStyle(
                      color: BrandingColors.cadiBlack,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  keyboardType: TextInputType.text,
                  onEditingComplete: () {
                    FocusScope.of(context).nextFocus();

                    if (_selectedShipmentStatus ==
                        CadiPortCargoUpdateShipment.breakBulk) {
                      Future.delayed(const Duration(milliseconds: 300),
                          _showJobPackTypeBottomPicker);
                    }
                  })
              : Container()),
    ]);

    if (_selectJobPackTypeVisible == true) {
      allOptions.addAll([
        SettingTile(
            9,
            'JOB PACK TYPE',
            Material(
              color: BrandingColors.cadiWhite,
              child: ListTile(
                  title: Text("Pack Type",
                      style: TextStyle(
                          fontStyle: FontStyle.normal,
                          fontSize: PlatformSizing().mediumTextSize(context),
                          color: BrandingColors.cadiBlack))),
            )),
        SettingTile(
            10,
            "",
            Material(
                color: BrandingColors.cadiWhite,
                child: InkWell(
                  onTap: () {
                    _showJobPackTypeBottomPicker();
                  },
                  child: PlatformTextField(
                    cursorColor: BrandingColors.cadiGrey,
                    textCapitalization: TextCapitalization.characters,
                    controller: _selectJobPackTypeController,
                    key: _selectJobPackTypeKey,
                    autocorrect: false,
                    enabled: false,
                    material: (_, __) => MaterialAssetsDark()
                        .textFieldData("", FontAwesomeIcons.chevronDown),
                    cupertino: (_, __) => CupertinoAssets().textFieldData(
                        'Pack Type', FontAwesomeIcons.chevronDown),
                    style: TextStyle(
                        color: BrandingColors.cadiBlack,
                        fontWeight: FontWeight.w400,
                        fontSize: PlatformSizing().inputTextFieldSize(context)),
                    keyboardType: TextInputType.text,
                  ),
                ))),
      ]);
    }

    allOptions.addAll([
      SettingTile(
        11,
        'WEIGHT',
        Material(
          color: BrandingColors.cadiWhite,
          child: ListTile(
            title: Text(
              "Gross Weight",
              style: TextStyle(
                fontStyle: FontStyle.normal,
                fontSize: PlatformSizing().mediumTextSize(context),
                color: BrandingColors.cadiBlack,
              ),
            ),
          ),
        ),
      ),
      SettingTile(
        12,
        '',
        PlatformTextField(
          cursorColor: BrandingColors.cadiGrey,
          controller: _weightController,
          autocorrect: false,
          material: (_, __) => MaterialAssetsDark()
              .textFieldData("", FontAwesomeIcons.weightScale),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData('Enter Weight', null),
          style: TextStyle(
            color: BrandingColors.cadiBlack,
            fontSize: PlatformSizing().inputTextFieldSize(context),
          ),
          keyboardType: TextInputType.number,
          onEditingComplete: () {
            if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
              _showWeightUnitBottomPicker();
            }
          },
        ),
      ),
      SettingTile(
        13,
        'WEIGHT UNIT',
        Material(
          color: BrandingColors.cadiWhite,
          child: ListTile(
            title: Text(
              "Weight Unit",
              style: TextStyle(
                fontStyle: FontStyle.normal,
                fontSize: PlatformSizing().mediumTextSize(context),
                color: BrandingColors.cadiBlack,
              ),
            ),
          ),
        ),
      ),
      SettingTile(
        14,
        '',
        Material(
          color: BrandingColors.cadiWhite,
          child: InkWell(
            onTap: _showWeightUnitBottomPicker,
            child: PlatformTextField(
              cursorColor: BrandingColors.cadiGrey,
              controller: TextEditingController(text: _selectedWeightUnit),
              enabled: false,
              material: (_, __) => MaterialAssetsDark()
                  .textFieldData("", FontAwesomeIcons.chevronDown),
              cupertino: (_, __) => CupertinoAssets()
                  .textFieldData('Weight Unit', FontAwesomeIcons.chevronDown),
              style: TextStyle(
                color: BrandingColors.cadiBlack,
                fontSize: PlatformSizing().inputTextFieldSize(context),
              ),
            ),
          ),
        ),
      ),
    ]);

    allOptions.addAll([
      const SettingTile(
          15,
          '',
          Material(
              color: BrandingColors.cadiWhite,
              child: ListTile(
                title: Text(''),
              ))),
      SettingTile(
          16,
          '',
          Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 50.0, vertical: 0.0),
              child: BaseButton(
                  color: widget.viewModel.universalBranding
                      .getButtonPrimaryColor(),
                  child: Text(
                    'Next',
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: PlatformSizing().normalTextSize(context)),
                  ),
                  onPressed: () {
                    goNext();
                  })))
    ]);

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          itemCount: allOptions.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          itemBuilder: (BuildContext context, int index) {
            return allOptions[index].body;
          }),
    );
  }

  Widget buildMoveEventList(BuildContext context, List<Widget> moveEvents) {
    return LayoutBuilder(
      builder: (context, constraints) {
        double mainAxisSpacing = constraints.maxWidth > 600
            ? 20
            : 10; // Adjust spacing based on screen width
        double crossAxisSpacing = constraints.maxWidth > 600
            ? 20
            : 10; // Adjust spacing based on screen width
        double childAspectRatio = constraints.maxWidth > 600
            ? 6
            : 2.8; // Adjust aspect ratio based on screen width

        return GridView(
          padding:
              const EdgeInsets.only(top: 10, bottom: 20, left: 5, right: 5),
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: mainAxisSpacing,
            crossAxisSpacing: crossAxisSpacing,
            childAspectRatio: childAspectRatio,
          ),
          children: moveEvents,
        );
      },
    );
  }

  Widget buildContainerCard(
    BuildContext context,
    String menuName,
    Function() navFunction,
    Color color,
    String eventStatus,
  ) {
    return BaseMoveEventSelect(
      color: color,
      onPressed: navFunction,
      child: Center(
        child: Text(
          eventStatus,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: PlatformSizing().superSmallTextSize(context),
            color: BrandingColors.cadiBlack,
          ),
        ),
      ),
    );
  }
}
