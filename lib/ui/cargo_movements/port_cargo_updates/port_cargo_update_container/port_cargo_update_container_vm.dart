import 'package:cadi_core_app/factory/cadi_enum_factory.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/theme/theme.dart';
import 'package:cadi_core_app/ui/cargo_movements/port_cargo_updates/port_cargo_update_container/port_cargo_update_container_page.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';

import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class PortCargoUpdateContainerScreenBuilder extends StatelessWidget {
  const PortCargoUpdateContainerScreenBuilder({super.key});

  static const String route =
      route_helper.portCargoUpdateContainerScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(context, "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, PortCargoUpdateContainerVM>(
        converter: PortCargoUpdateContainerVM.fromStore,
        builder: (context, vm) {
          return PortCargoUpdateContainerPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class PortCargoUpdateContainerVM {
  bool isLoading;
  bool settingsAutoDisplayNextDropdownOrInput;
  bool settingsAutoSubmitPage;
  AuthState authState;
  Account account;
  PortCargoUpdateCreate portCargoUpdateCreate;
  String portCargoUpdateRole;
  UniversalBranding universalBranding;
  List<CadiEnum> portCargoUpdateShipments;
  List<CadiGeneric> cargoTypes;
  List<CadiGeneric> jobPackTypes;
  List<CadiEnum> weightUnits;
  final Function(BuildContext, String) onNavigatePressed;
  final Function(String token) setToken;
  final Function(
      BuildContext context,
      String shipment,
      String inputOne,
      String inputTwo,
      num grossWeight,
      String weightUnit,
      String selectedJobPackType,
      bool shouldNavigate) setPortCargoUpdateContainerVariables;
  final Function(BuildContext context, String message) showErrorAlert;
  PortCargoUpdateContainerVM({
    required this.isLoading,
    required this.settingsAutoDisplayNextDropdownOrInput,
    required this.settingsAutoSubmitPage,
    required this.authState,
    required this.account,
    required this.portCargoUpdateRole,
    required this.universalBranding,
    required this.cargoTypes,
    required this.jobPackTypes,
    required this.portCargoUpdateCreate,
    required this.portCargoUpdateShipments,
    required this.onNavigatePressed,
    required this.setToken,
    required this.setPortCargoUpdateContainerVariables,
    required this.showErrorAlert,
    required this.weightUnits,
  });

  static PortCargoUpdateContainerVM fromStore(Store<AppState> store) {
    return PortCargoUpdateContainerVM(
      settingsAutoDisplayNextDropdownOrInput:
          store.state.uiState.uiDropDownOrInputAutoContinue,
      settingsAutoSubmitPage: store.state.uiState.uiAutoSubmitPage,
      isLoading: store.state.isLoading,
      authState: store.state.authState,
      account: store.state.accountState.account,
      portCargoUpdateRole: store.state.uiState.portCargoUpdateRole,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      cargoTypes: store.state.referenceState.cargoTypes,
      jobPackTypes: store.state.referenceState.jobPackTypes,
      portCargoUpdateCreate:
          store.state.portCargoUpdateState.portCargoUpdateCreate,
      showErrorAlert: (BuildContext context, String message) {
        showOkMessage(context, "Submit Error", message);
      },
      portCargoUpdateShipments:
          store.state.referenceState.portCargoUpdateShipments,
      onNavigatePressed: (BuildContext context, String route) {
        if (store.state.isLoading) {
          return;
        }
        navigation_helper.navigateRoute(route, store, null);
      },
      setToken: (String token) {},
      setPortCargoUpdateContainerVariables: (BuildContext context,
          String shipment,
          String inputOne,
          String inputTwo,
          num grossWeight,
          String weightUnit,
          String selectedJobPackType,
          bool shouldNavigate) {
        store.dispatch(SetPortCargoUpdateContainerVariables(
            shipment,
            inputOne.replaceAll(' ', ''),
            inputTwo.replaceAll(' ', ''),
            grossWeight,
            weightUnit,
            selectedJobPackType));
        if (shouldNavigate) {
          navigation_helper.navigateRoute(
              route_helper.portCargoUpdateStatusScreenBuilder, store, null);
        }
      },
      weightUnits: CadiEnumFactory().createWeightMeasurements(),
    );
  }
}
