import 'package:cadi_core_app/factory/ui_factory.dart';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';

import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'port_cargo_update_freight_search_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

class PortCargoUpdateFreightSearchPage extends StatefulWidget {
  final PortCargoUpdateFreightSearchVM viewModel;
  const PortCargoUpdateFreightSearchPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<PortCargoUpdateFreightSearchPage> createState() {
    return PortCargoUpdateFreightSearchPageState(viewModel: viewModel);
  }
}

class PortCargoUpdateFreightSearchPageState
    extends State<PortCargoUpdateFreightSearchPage> {
  final PortCargoUpdateFreightSearchVM viewModel;
  final _searchVesselController = TextEditingController();
  final _searchVoyageController = TextEditingController();
  final _selectCraneController = TextEditingController();

  final FocusNode vesselNode = FocusNode();
  final FocusNode voyageNode = FocusNode();

  static const Key _searchVesselKey = Key(CargoKeys.searchVesselKey);
  static const Key _searchVoyageKey = Key(CargoKeys.searchVoyageKey);

  int _selectedVesselIndex = 0;
  int _vesselNameLength = 0;
  List<DropdownItem> _vesselItems = [];

  PortCargoUpdateFreightSearchPageState({
    Key? key,
    required this.viewModel,
  });

  @override
  void initState() {
    super.initState();
    final uiFactory = UiFactory();

    _searchVesselController.text =
        widget.viewModel.uiPortCargoUpdateFreightSearch.vesselName;

    setState(() {
      _vesselNameLength =
          widget.viewModel.uiPortCargoUpdateFreightSearch.vesselName.length;
    });

    _searchVoyageController.text =
        widget.viewModel.uiPortCargoUpdateFreightSearch.voyageNumber;

    if (widget.viewModel.accountVessels.isNotEmpty) {
      _vesselItems = uiFactory.convertToDropdown<AccountVessel, int>(
          widget.viewModel.accountVessels,
          (vessel) => vessel.vesselName,
          (vessel) => vessel.vesselId);
    }
  }

  @override
  void dispose() {
    _searchVesselController.dispose();
    _searchVoyageController.dispose();
    _selectCraneController.dispose();
    super.dispose();
  }

  void goNext(BuildContext context) {
    if (_searchVesselController.text.trim() == "") {
      viewModel.showErrorAlert(context, "Please input a vessel name.");
    } else if (_searchVoyageController.text.trim() == "") {
      viewModel.showErrorAlert(context, "Please input a voyage number.");
    } else {
      viewModel.findCargoMovement(context, _searchVesselController.text.trim(),
          _searchVoyageController.text.trim());
    }
  }

  void _setVessel(int index) {
    setState(() {
      _searchVesselController.text = _vesselItems[index].text.toUpperCase();
      _selectedVesselIndex = index;
      _vesselNameLength = _vesselItems[index].text.toUpperCase().length;
    });
    FocusScope.of(context).requestFocus(voyageNode);
  }

  void _showVesselBottomPicker(String searchParams) {
    if (widget.viewModel.accountVessels.isNotEmpty) {
      var searchList = _vesselItems
          .where((element) =>
              element.text.toUpperCase().startsWith(searchParams.toUpperCase()))
          .toList();
      if (searchList.isNotEmpty) {
        showBottomDropdown(context, 'Vessel', 'Select a Vessel', searchList,
            _selectedVesselIndex, _setVessel, true);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final List<SettingTile> allSettings = <SettingTile>[
      SettingTile(
        0,
        'Active Account',
        Column(
          children: <Widget>[
            Center(
              child: Text(
                "Cargo Update:",
                style: TextStyle(
                  fontSize: PlatformSizing().normalTextSize(context),
                  color: BrandingColors.cadiBlack,
                ),
              ),
            ),
            Center(
              child: Text(
                getPortCargoUpdateRoleDescription(widget.viewModel.portCargoUpdateRole),
                style: TextStyle(
                  fontSize: PlatformSizing().normalTextSize(context),
                  color: BrandingColors.cadiBlack,
                ),
              ),
            ),
          ],
        ),
      ),
      SettingTile(
        1,
        'Account Operations',
        Container(
          margin: const EdgeInsets.only(top: 10.0), // Add your own value here
          child: Column(
            children: <Widget>[
              Text("Search for Vessel",
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      fontWeight: FontWeight.w300,
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),
      SettingTile(
          2,
          'VESSEL Name',
          Material(
            color: BrandingColors.background,
            child: ListTile(
                title: Text('Vessel Name',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack))),
          )),
      SettingTile(
        3,
        "VESSEL Name INPUT",
        PlatformTextField(
          onChanged: (dataString) {
            if (dataString.isNotEmpty &&
                dataString.length > _vesselNameLength) {
              _showVesselBottomPicker(dataString);
            }
            setState(() {
              _vesselNameLength = dataString.length;
            });
          },
          focusNode: vesselNode,
          textInputAction: TextInputAction.next,
          textCapitalization: TextCapitalization.characters,
          controller: _searchVesselController,
          key: _searchVesselKey,
          autocorrect: false,
          material: (_, __) => MaterialAssetsDark()
              .textFieldData("", FontAwesomeIcons.magnifyingGlass),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData('Vessel Name', null),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontWeight: FontWeight.w400,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
          onEditingComplete: () {
            if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
              FocusScope.of(context).requestFocus(voyageNode);
            }
          },
        ),
      ),
      SettingTile(
          4,
          'Voyage Number',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('Voyage Number',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(
        5,
        "Voyage Number",
        PlatformTextField(
          focusNode: voyageNode,
          textCapitalization: TextCapitalization.characters,
          controller: _searchVoyageController,
          key: _searchVoyageKey,
          autocorrect: false,
          material: (_, __) => MaterialAssetsDark()
              .textFieldData("", FontAwesomeIcons.magnifyingGlass),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData('Voyage Number', null),
          style: TextStyle(
              fontWeight: FontWeight.w400,
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
          onEditingComplete: () {},
        ),
      ),
      const SettingTile(
          6,
          '',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text(''),
              ))),
      SettingTile(
          7,
          '',
          Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 50.0, vertical: 0.0),
              child: BaseButton(
                  color: widget.viewModel.universalBranding
                      .getButtonPrimaryColor(),
                  child: Text('Next',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: PlatformSizing().normalTextSize(context))),
                  onPressed: () {
                    goNext(context);
                  }))),
    ];

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          itemCount: allSettings.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
  }
}
