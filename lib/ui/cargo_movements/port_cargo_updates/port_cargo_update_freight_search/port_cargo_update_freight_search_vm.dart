import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

import 'port_cargo_update_freight_search_page.dart';

class PortCargoUpdateFreightSearchScreenBuilder extends StatelessWidget {
  const PortCargoUpdateFreightSearchScreenBuilder({super.key});

  static const String route =
      route_helper.portCargoUpdateFreightSearchScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(
                  // FontAwesomeIcons.warning,
                  context,
                  "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, PortCargoUpdateFreightSearchVM>(
        converter: PortCargoUpdateFreightSearchVM.fromStore,
        builder: (context, vm) {
          return PortCargoUpdateFreightSearchPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class PortCargoUpdateFreightSearchVM {
  bool settingsAutoDisplayNextDropdownOrInput;
  bool settingsAutoSubmitPage;
  bool isLoading;
  AuthState authState;
  Account account;
  String portCargoUpdateRole;
  UniversalBranding universalBranding;
  List<AccountVessel> accountVessels;
  PortCargoUpdateCreate portCargoUpdateCreate;

  UiPortCargoUpdateFreightSearch uiPortCargoUpdateFreightSearch;
  final Function(BuildContext context, String message) showErrorAlert;
  final Function(BuildContext, String) onNavigatePressed;
  final Function(String token) setToken;
  final Function(BuildContext context, String vesselName, String voyageNumber)
      findCargoMovement;
  PortCargoUpdateFreightSearchVM(
      {required this.isLoading,
      required this.settingsAutoDisplayNextDropdownOrInput,
      required this.settingsAutoSubmitPage,
      required this.authState,
      required this.account,
      required this.portCargoUpdateRole,
      required this.universalBranding,
      required this.accountVessels,
      required this.portCargoUpdateCreate,
      required this.uiPortCargoUpdateFreightSearch,
      required this.onNavigatePressed,
      required this.showErrorAlert,
      required this.setToken,
      required this.findCargoMovement});

  static PortCargoUpdateFreightSearchVM fromStore(Store<AppState> store) {
    return PortCargoUpdateFreightSearchVM(
        isLoading: store.state.isLoading,
        settingsAutoDisplayNextDropdownOrInput:
            store.state.uiState.uiDropDownOrInputAutoContinue,
        settingsAutoSubmitPage: store.state.uiState.uiAutoSubmitPage,
        authState: store.state.authState,
        account: store.state.accountState.account,
        portCargoUpdateRole: store.state.uiState.portCargoUpdateRole,
        universalBranding:
            UniversalBranding(store.state.accountState.activeService),
        accountVessels: store.state.accountState.filteredAccountVessels,
        portCargoUpdateCreate:
            store.state.portCargoUpdateState.portCargoUpdateCreate,
        uiPortCargoUpdateFreightSearch:
            store.state.uiState.uiPortCargoUpdateFreightSearch,
        showErrorAlert: (BuildContext context, String message) {
          showOkMessage(
              // FontAwesomeIcons.warning,
              context,
              "Submit Error",
              message);
        },
        onNavigatePressed: (BuildContext context, String route) {
          if (store.state.isLoading) {
            return;
          }
          navigation_helper.navigateRoute(route, store, null);
        },
        setToken: (String token) {},
        findCargoMovement:
            (BuildContext context, String vesselName, String voyageNumber) {
          store.dispatch(
              UiPortCargoUpdateFreightSearchSetVesselName(vesselName));
          store.dispatch(UiPortCargoUpdateFreightSearchSetVoyageNumber(
              voyageNumber.replaceAll(' ', '')));
          store.dispatch(SetPortCargoUpdateFreightVariables(
              vesselName, voyageNumber.replaceAll(' ', '')));
          store.dispatch(NavigateToAction.push(
              route_helper.portCargoUpdateContainerScreenBuilder));
        });
  }
}
