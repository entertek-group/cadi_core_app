import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/cargo_movements/port_cargo_updates/port_cargo_update_home/port_cargo_update_home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

class PortCargoUpdateHomeScreenBuilder extends StatelessWidget {
  const PortCargoUpdateHomeScreenBuilder({super.key});

  static const String route = route_helper.portCargoUpdateHomeScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: BrandingColors.cadiWhite,
      body: StoreConnector<AppState, PortCargoUpdateHomeVM>(
        converter: PortCargoUpdateHomeVM.fromStore,
        builder: (context, vm) {
          return PortCargoUpdateHomePage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class PortCargoUpdateHomeVM {
  bool isLoading;
  bool settingsAutoDisplayNextDropdownOrInput;
  bool settingsAutoSubmitPage;
  AuthState authState;
  Account account;
  UniversalBranding universalBranding;
  List<AccountCarrier> accountCarriers;
  List<CadiEnum> portCargoUpdateTypes;
  List<AccountPortDepot> accountPortDepots;
  List<AccountPort> accountPorts;
  UiPortCargoUpdateHome uiPortCargoUpdateHome;

  final Function(BuildContext, String, int, int, int) onNavigatePressed;
  final Function(int selectedPortIndex) setSelectedPortIndex;
  final Function(int selectedLocationIndex) setSelectedLocationIndex;
  final Function(int selectedCarrierIndex) setSelectedCarrierIndex;
  final Function(int selectedRoleIndex) setSelectedRoleIndex;
  final Function(BuildContext context) initScreenData;

  PortCargoUpdateHomeVM(
      {required this.isLoading,
      required this.settingsAutoDisplayNextDropdownOrInput,
      required this.settingsAutoSubmitPage,
      required this.authState,
      required this.account,
      required this.universalBranding,
      required this.accountCarriers,
      required this.portCargoUpdateTypes,
      required this.accountPortDepots,
      required this.accountPorts,
      required this.uiPortCargoUpdateHome,
      required this.onNavigatePressed,
      required this.setSelectedPortIndex,
      required this.setSelectedLocationIndex,
      required this.setSelectedCarrierIndex,
      required this.setSelectedRoleIndex,
      required this.initScreenData});

  static PortCargoUpdateHomeVM fromStore(Store<AppState> store) {
    return PortCargoUpdateHomeVM(
      isLoading: store.state.isLoading,
      settingsAutoDisplayNextDropdownOrInput:
          store.state.uiState.uiDropDownOrInputAutoContinue,
      settingsAutoSubmitPage: store.state.uiState.uiAutoSubmitPage,
      authState: store.state.authState,
      account: store.state.accountState.account,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      accountCarriers: store.state.accountState.filteredAccountCarriers,
      portCargoUpdateTypes: store.state.referenceState.portCargoUpdateTypes,
      accountPortDepots: store.state.accountState.filteredAccountPortDepots,
      accountPorts: store.state.accountState.filteredAccountPorts,
      uiPortCargoUpdateHome: store.state.uiState.uiPortCargoUpdateHome,
      initScreenData: (context) {
        store.dispatch(ResetPortCargoUpdateCreate());
      },
      onNavigatePressed: (BuildContext context, String role,
          int accountPortDepotId, int carrierId, int portId) {
        if (store.state.isLoading) {
          return;
        }

        store.dispatch(UiPortCargoUpdateSetRole(role));
        store.dispatch(SetPortCargoUpdateAccountPortDepot(accountPortDepotId));
        store.dispatch(
            SetPortCargoUpdateHomeVariables(accountPortDepotId, carrierId, role));

        store.dispatch(NavigateToAction.push(
            route_helper.portCargoUpdateFreightSearchScreenBuilder));
      },
      setSelectedPortIndex: (int selectedPortIndex) {
        store.dispatch(UiPortCargoUpdateSetPortIndex(selectedPortIndex));
      },
      setSelectedLocationIndex: (int selectedLocationIndex) {
        store
            .dispatch(UiPortCargoUpdateSetLocationIndex(selectedLocationIndex));
      },
      setSelectedCarrierIndex: (int selectedCarrierIndex) {
        store.dispatch(UiPortCargoUpdateSetCarrierIndex(selectedCarrierIndex));
      },
      setSelectedRoleIndex: (int selectedRoleIndex) {
        store.dispatch(UiPortCargoUpdateSetRoleIndex(selectedRoleIndex));
      },
    );
  }
}
