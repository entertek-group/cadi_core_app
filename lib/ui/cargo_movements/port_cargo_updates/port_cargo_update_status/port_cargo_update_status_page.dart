import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/ui/cargo_movements/port_cargo_updates/port_cargo_update_status/port_cargo_update_status_vm.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/setting_tile.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

class PortCargoUpdateStatusPage extends StatefulWidget {
  final PortCargoUpdateStatusVM viewModel;
  const PortCargoUpdateStatusPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<PortCargoUpdateStatusPage> createState() {
    return PortCargoUpdateStatusPageState(viewModel: viewModel);
  }
}

class PortCargoUpdateStatusPageState extends State<PortCargoUpdateStatusPage> {
  final PortCargoUpdateStatusVM viewModel;
  final _commentController = TextEditingController();

  static const Key _commentControllerKey = Key(CargoKeys.commentKey);

  PortCargoUpdateStatusPageState({
    required this.viewModel,
  });

  @override
  void dispose() {
    _commentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<SettingTile> allOptions = <SettingTile>[
      SettingTile(
        0,
        'Cargo Update',
        Column(
          children: <Widget>[
            Center(
              child: Text(
                "Cargo Update:",
                style: TextStyle(
                  fontSize: PlatformSizing().normalTextSize(context),
                  color: BrandingColors.cadiBlack,
                ),
              ),
            ),
            Center(
              child: Text(
                getPortCargoUpdateRoleDescription(
                    widget.viewModel.portCargoUpdateRole),
                style: TextStyle(
                  fontSize: PlatformSizing().normalTextSize(context),
                  color: BrandingColors.cadiBlack,
                ),
              ),
            ),
          ],
        ),
      ),
      SettingTile(
        1,
        'Status Information',
        Container(
          margin: const EdgeInsets.only(top: 10.0, bottom: 15),
          child: Column(
            children: <Widget>[
              Text("Add Comment",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      color: BrandingColors.cadiBlack)),
            ],
          ),
        ),
      ),
      SettingTile(
          2,
          'comment',
          Padding(
              padding:
                  const EdgeInsets.only(top: 0, bottom: 20, left: 5, right: 5),
              child: PlatformTextField(
                cursorColor: BrandingColors.cadiGrey,
                controller: _commentController,
                key: _commentControllerKey,
                maxLines: 5,
                hintText: "Enter optional comments",
                material: (_, __) => CommentBox().textFieldData(''),
                cupertino: (_, __) =>
                    CupertinoAssets().textFieldData('Add Comments', null),
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    color: BrandingColors.cadiBlack,
                    fontSize: PlatformSizing().inputTextFieldSize(context)),
                keyboardType: TextInputType.text,
              ))),
      SettingTile(
          3,
          '',
          Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 50.0, vertical: 0.0),
              child: BaseButton(
                  color: widget.viewModel.universalBranding
                      .getButtonPrimaryColor(),
                  child: Text(
                    'Next',
                    style: TextStyle(
                        fontSize: PlatformSizing().normalTextSize(context)),
                  ),
                  onPressed: () {
                    viewModel.selectStatus(
                      context,
                      _commentController.text,
                    );
                  }))),
    ];

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          itemCount: allOptions.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          itemBuilder: (BuildContext context, int index) {
            return allOptions[index].body;
          }),
    );
  }
}
