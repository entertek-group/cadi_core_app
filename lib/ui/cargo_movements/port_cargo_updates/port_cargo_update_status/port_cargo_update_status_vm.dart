import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/cargo_movements/port_cargo_updates/port_cargo_update_status/port_cargo_update_status_page.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class PortCargoUpdateStatusScreenBuilder extends StatelessWidget {
  const PortCargoUpdateStatusScreenBuilder({super.key});

  static const String route = route_helper.portCargoUpdateStatusScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(
                  // FontAwesomeIcons.warning,
                  context,
                  "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, PortCargoUpdateStatusVM>(
        converter: PortCargoUpdateStatusVM.fromStore,
        builder: (context, vm) {
          return PortCargoUpdateStatusPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class PortCargoUpdateStatusVM {
  bool isLoading;
  AuthState authState;
  Account account;
  String portCargoUpdateRole;
  PortCargoUpdateCreate portCargoUpdateCreate;
  UniversalBranding universalBranding;

  final Function(BuildContext, String) onNavigatePressed;
  final Function(String token) setToken;
  final Function(BuildContext context, String message) showErrorAlert;
  final Function(
    BuildContext context,
    String inputComment,
  ) selectStatus;
  PortCargoUpdateStatusVM({
    required this.isLoading,
    required this.authState,
    required this.account,
    required this.portCargoUpdateRole,
    required this.portCargoUpdateCreate,
    required this.universalBranding,
    required this.onNavigatePressed,
    required this.setToken,
    required this.showErrorAlert,
    required this.selectStatus,
  });

  static PortCargoUpdateStatusVM fromStore(Store<AppState> store) {
    return PortCargoUpdateStatusVM(
        isLoading: store.state.isLoading,
        authState: store.state.authState,
        account: store.state.accountState.account,
        portCargoUpdateRole: store.state.uiState.portCargoUpdateRole,
        portCargoUpdateCreate:
            store.state.portCargoUpdateState.portCargoUpdateCreate,
        universalBranding:
            UniversalBranding(store.state.accountState.activeService),
        onNavigatePressed: (BuildContext context, String route) {
          if (store.state.isLoading) {
            return;
          }
          navigation_helper.navigateRoute(route, store, null);
        },
        setToken: (String token) {},
        showErrorAlert: (BuildContext context, String message) {
          showOkMessage(
              // FontAwesomeIcons.warning,
              context,
              "Submit Error",
              message);
        },
        selectStatus: (
          BuildContext context,
          String inputComment,
        ) {
          store.dispatch(SetPortCargoUpdateStatusVariables(inputComment));
          store.dispatch(NavigateToAction.push(
              route_helper.portCargoUpdateSummaryScreenBuilder));
        });
  }
}
