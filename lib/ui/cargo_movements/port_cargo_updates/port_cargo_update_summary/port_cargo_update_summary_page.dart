import 'dart:core';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/ui/cargo_movements/port_cargo_updates/port_cargo_update_summary/port_cargo_update_summary_vm.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/app/setting_tile.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/input.dart';
import 'package:cadi_core_app/ui/shared/components/popup_menu.dart';
import 'package:cadi_core_app/ui/shared/components/time_picker.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
//helpers
import 'package:cadi_core_app/helpers/helpers.dart';
import 'package:intl/intl.dart';

class PortCargoUpdateSummaryPage extends StatefulWidget {
  final PortCargoUpdateSummaryVM viewModel;
  const PortCargoUpdateSummaryPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<PortCargoUpdateSummaryPage> createState() {
    return PortCargoUpdateSummaryPageState(viewModel: viewModel);
  }
}

class PortCargoUpdateSummaryPageState
    extends State<PortCargoUpdateSummaryPage> {
  final PortCargoUpdateSummaryVM viewModel;
  String movementStatus = "";
  String movementType = "";
  var containerWeightUnitList = <PopMenuItem>[];
  var packWeightUnitList = <PopMenuItem>[];

  PortCargoUpdateSummaryPageState({
    Key? key,
    required this.viewModel,
  });

  final TextEditingController _textEditingController = TextEditingController();

  var dateTime = DateFormat.yMMMEd().format(DateTime.now()).toString();
  var time = DateTime.now().toString().substring(10, 20);

  @override
  initState() {
    super.initState();
    for (final weightUnit in widget.viewModel.weightUnits) {
      var containerItem = PopMenuItem(weightUnit.description,
          () => viewModel.updateContainerWeightUnit(weightUnit.id));
      containerWeightUnitList.add(containerItem);
      var packItem = PopMenuItem(weightUnit.description,
          () => viewModel.updatePackWeightUnit(weightUnit.id));
      packWeightUnitList.add(packItem);
    }

    // Initialize dateTime and time based on cargoMovementCreate.movementAt
    if (widget.viewModel.portCargoUpdateCreate.updateAt != "") {
      final updateAt =
          DateTime.parse(widget.viewModel.portCargoUpdateCreate.updateAt);
      dateTime = DateFormat.yMMMEd().format(updateAt).toString();
      time = updateAt.toString().substring(11, 16);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> cargoSummary = <Widget>[];
    if (widget.viewModel.portCargoUpdateCreate.shipment ==
        CadiPortCargoUpdateShipment.breakBulk) {
      cargoSummary.add(buildSummaryRow(context, "Vessel",
          widget.viewModel.portCargoUpdateCreate.vesselName, false, null));
      cargoSummary.add(buildSummaryRow(context, "Voyage",
          widget.viewModel.portCargoUpdateCreate.voyageNumber, false, null));
      cargoSummary.add(buildSummaryRow(
          context,
          "Type",
          getShipmentTypeDisplayFormat(
              widget.viewModel.portCargoUpdateCreate.shipment),
          false,
          null));
      cargoSummary.add(buildSummaryRow(
          context,
          "Weight",
          widget.viewModel.portCargoUpdateCreate.packData.packWeight.toString(),
          true, () {
        _textEditingController.text = widget
            .viewModel.portCargoUpdateCreate.packData.packWeight
            .toString();
        showMessageWithInputActions(
            context,
            "Amend Container",
            "Enter new container weight",
            () {
              widget.viewModel.updatePackWeight(_textEditingController.text);
              Navigator.pop(context);
            },
            "Save",
            () {
              Navigator.pop(context);
            },
            "Cancel",
            inputWidget(context, _textEditingController, "", true));
      }));
      cargoSummary.add(buildSummaryRow(
          context,
          "Unit",
          widget.viewModel.portCargoUpdateCreate.packData.packWeightUnit,
          true, () {
        showPopupMenu(
            context,
            "Weight Unit: ${widget.viewModel.portCargoUpdateCreate.packData.packWeightUnit}",
            "Select a Unit",
            packWeightUnitList);
      }));
      cargoSummary.add(buildSummaryRow(
          context,
          "Pack Sequence #",
          widget.viewModel.portCargoUpdateCreate.packData.packSequenceNumber,
          false,
          null));
      cargoSummary.add(buildSummaryRow(context, "", "", false, null));
      cargoSummary.add(buildSummaryRow(context, "Comments",
          widget.viewModel.portCargoUpdateCreate.comment ?? "", false, null));

      cargoSummary.add(buildSummaryRow(context, "", "", false, null));
      cargoSummary.add(buildSummaryRow(context, "Port",
          widget.viewModel.accountPortDepot.unLocode, false, null));
      cargoSummary.add(buildSummaryRow(context, "Location",
          widget.viewModel.accountPortDepot.depotName, false, null));
      cargoSummary.add(buildSummaryRow(context, "Carrier",
          widget.viewModel.accountCarrier.carrierName, false, null));

      cargoSummary.add(buildSummaryRow(context, "Date", dateTime, false, null));
      cargoSummary.add(
          buildSummaryRow(context, "Time", time, true, _showDateTimePicker));
      cargoSummary.add(buildSummaryRow(
          context, "Profile", widget.viewModel.account.name, false, null));
    } else {
      cargoSummary.add(buildSummaryRow(context, "Vessel",
          widget.viewModel.portCargoUpdateCreate.vesselName, false, null));
      cargoSummary.add(buildSummaryRow(context, "Voyage",
          widget.viewModel.portCargoUpdateCreate.voyageNumber, false, null));
      cargoSummary.add(buildSummaryRow(
          context,
          "Container #",
          widget.viewModel.portCargoUpdateCreate.containerData.containerNumber,
          true, () {
        _textEditingController.text = widget
            .viewModel.portCargoUpdateCreate.containerData.containerNumber;
        showMessageWithInputActions(
            context,
            "Amend Container",
            "Enter new container number",
            () {
              if (isContainerInputValid(
                  context, _textEditingController.text, true)) {
                widget.viewModel
                    .updateContainerNumber(_textEditingController.text);
              }
              Navigator.pop(context);
            },
            "Save",
            () {
              Navigator.pop(context);
            },
            "Cancel",
            inputWidget(context, _textEditingController, "", false));
      }));

      cargoSummary.add(buildSummaryRow(
          context,
          "Type",
          getShipmentTypeDisplayFormat(
              widget.viewModel.portCargoUpdateCreate.shipment),
          false,
          null));
      cargoSummary.add(buildSummaryRow(
          context,
          "Weight",
          widget.viewModel.portCargoUpdateCreate.containerData.containerWeight
              .toString(),
          true, () {
        _textEditingController.text = widget
            .viewModel.portCargoUpdateCreate.containerData.containerWeight
            .toString();
        showMessageWithInputActions(
            context,
            "Amend Container",
            "Enter new container weight",
            () {
              widget.viewModel
                  .updateContainerWeight(_textEditingController.text);
              Navigator.pop(context);
            },
            "Save",
            () {
              Navigator.pop(context);
            },
            "Cancel",
            inputWidget(context, _textEditingController, "", true));
      }));

      cargoSummary.add(buildSummaryRow(
          context,
          "Unit",
          widget.viewModel.portCargoUpdateCreate.containerData
              .containerWeightUnit,
          true, () {
        showPopupMenu(
            context,
            "Weight Unit: ${widget.viewModel.portCargoUpdateCreate.containerData.containerWeightUnit}",
            "Select a Unit",
            containerWeightUnitList);
      }));

      cargoSummary.add(buildSummaryRow(context, "", "", false, null));
      cargoSummary.add(buildSummaryRow(context, "Comments",
          widget.viewModel.portCargoUpdateCreate.comment ?? "", false, null));
      cargoSummary.add(buildSummaryRow(context, "Port",
          widget.viewModel.accountPortDepot.unLocode, false, null));
      cargoSummary.add(buildSummaryRow(context, "Location",
          widget.viewModel.accountPortDepot.depotName, false, null));
      cargoSummary.add(buildSummaryRow(context, "Carrier",
          widget.viewModel.accountCarrier.carrierName, false, null));

      cargoSummary.add(buildSummaryRow(context, "Date", dateTime, false, null));
      cargoSummary.add(
          buildSummaryRow(context, "Time", time, true, _showDateTimePicker));
      cargoSummary.add(buildSummaryRow(
          context, "Profile", widget.viewModel.account.name, false, null));
    }

    List<SettingTile> allOptions = <SettingTile>[
      SettingTile(
        0,
        'Cargo Update',
        Column(
          children: <Widget>[
            Center(
              child: Text(
                "Cargo Update:",
                style: TextStyle(
                  fontSize: PlatformSizing().normalTextSize(context),
                  color: BrandingColors.cadiBlack,
                ),
              ),
            ),
            Center(
              child: Text(
                getPortCargoUpdateRoleDescription(
                    widget.viewModel.portCargoUpdateRole),
                style: TextStyle(
                  fontSize: PlatformSizing().normalTextSize(context),
                  color: BrandingColors.cadiBlack,
                ),
              ),
            ),
          ],
        ),
      ),
      SettingTile(
        1,
        'Account Operations',
        Container(
          margin: const EdgeInsets.only(top: 10.0, bottom: 15),
          child: Column(
            children: <Widget>[
              Text("Summary: Check, Update and Submit",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      color: BrandingColors.cadiBlack)),
            ],
          ),
        ),
      ),
      SettingTile(2, 'Summary', buildSummaryCard(context, cargoSummary)),
    ];

    allOptions.add(SettingTile(
        7,
        '',
        Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 50.0, vertical: 10.0),
            child: BaseButton(
                color:
                    widget.viewModel.universalBranding.getButtonPrimaryColor(),
                child: Text('Submit',
                    style: TextStyle(
                        fontSize: PlatformSizing().normalTextSize(context))),
                onPressed: () {
                  viewModel.confirmPortCargoUpdate(context);
                }))));

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          physics: const ClampingScrollPhysics(),
          itemCount: allOptions.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          itemBuilder: (BuildContext context, int index) {
            return allOptions[index].body;
          }),
    );
  }

  Widget buildSummaryCard(BuildContext context, List<Widget> cargoSummary) {
    return Card(
        elevation: 2.0,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          side: const BorderSide(color: BrandingColors.cadiGrey, width: 0.5),
          borderRadius: BorderRadius.circular(5),
        ),
        margin: const EdgeInsets.only(left: 5, right: 5, bottom: 20),
        child: ListView.builder(
            shrinkWrap: true,
            physics: const ClampingScrollPhysics(),
            padding: const EdgeInsets.all(15),
            itemCount: cargoSummary.length,
            itemBuilder: (BuildContext context, int index) {
              return cargoSummary[index];
            }));
  }

  Widget buildSummaryRow(BuildContext context, String textDisplay,
      String textValue, bool hasAction, Function? buttonAction) {
    return ListTile(
        dense: true,
        visualDensity: const VisualDensity(horizontal: 0, vertical: -2),
        minVerticalPadding: 4,
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 0.0, vertical: 2),
        tileColor: Colors.transparent,
        leading: Text(
          textDisplay,
          style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: PlatformSizing().smallTextSize(context),
              color: BrandingColors.cadiBlack),
        ),
        title: Text(
          textValue,
          style: TextStyle(
              fontSize: PlatformSizing().smallTextSize(context),
              color: BrandingColors.cadiBlack),
          textAlign: TextAlign.end,
        ),
        trailing: hasAction
            ? IconButton(
                padding: const EdgeInsets.only(bottom: 10, left: 25),
                icon: const Icon(FontAwesomeIcons.penToSquare,
                    color: BrandingColors.cadiBlack),
                onPressed: buttonAction != null ? () => buttonAction() : () {})
            : const IconButton(
                icon: Icon(Icons.edit_outlined, color: Colors.transparent),
                onPressed: null));
  }

  void _showDateTimePicker() {
    final today = DateTime.now();
    showPlatformTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: const ColorScheme.light(
              primary: BrandingColors.cadiGreen,
              onPrimary: Colors.white,
              surface: BrandingColors.cadiWhite,
              onSurface: BrandingColors.cadiBlack,
            ),
            dialogBackgroundColor: Colors.white,
          ),
          child: child!,
        );
      },
    ).then((time) {
      if (time != null) {
        var selectedDateTime = DateTime(
          today.year,
          today.month,
          today.day,
          time.hour,
          time.minute,
          today.second,
          today.millisecond,
        );

        final sevenDaysAgo = today.subtract(const Duration(days: 7));

        // Prevent going more than 7 days back
        if (selectedDateTime.isBefore(sevenDaysAgo)) {
          selectedDateTime = sevenDaysAgo;
        }

        // Prevent setting date in the future
        if (selectedDateTime.isAfter(today)) {
          selectedDateTime = today;
        }

        final offset = selectedDateTime.timeZoneOffset;
        final offsetSign = offset.isNegative ? '-' : '+';
        final offsetHours = offset.inHours.abs().toString().padLeft(2, '0');
        final offsetMinutes =
            (offset.inMinutes % 60).abs().toString().padLeft(2, '0');

        final formattedDateTime =
            "${selectedDateTime.toIso8601String().split('Z')[0]}$offsetSign$offsetHours:$offsetMinutes";

        widget.viewModel.updateUpdateAt(formattedDateTime);
        setState(() {
          dateTime = DateFormat.yMMMEd().format(selectedDateTime).toString();
          this.time = DateFormat('HH:mm:ss').format(selectedDateTime);
        });
      }
    });
  }

}
