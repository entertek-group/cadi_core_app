import 'package:cadi_core_app/factory/cadi_enum_factory.dart';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/services/data_processing_service.dart';
import 'package:cadi_core_app/theme/theme.dart';
import 'package:cadi_core_app/ui/cargo_movements/port_cargo_updates/port_cargo_update_summary/port_cargo_update_summary_page.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class PortCargoUpdateSummaryScreenBuilder extends StatelessWidget {
  const PortCargoUpdateSummaryScreenBuilder({super.key});

  static const String route = route_helper.portCargoUpdateSummaryScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(context, "Action Blocked",
                  "You cannot change account while performing this action on summary screen.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, PortCargoUpdateSummaryVM>(
        converter: PortCargoUpdateSummaryVM.fromStore,
        builder: (context, vm) {
          return PortCargoUpdateSummaryPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class PortCargoUpdateSummaryVM {
  bool isLoading;
  AuthState authState;
  Account account;
  AccountPortDepot accountPortDepot;
  AccountCarrier accountCarrier;
  String portCargoUpdateRole;
  PortCargoUpdateCreate portCargoUpdateCreate;
  UniversalBranding universalBranding;
  List<CadiEnum> weightUnits;

  // FileStore tempStorage;
  final Function(BuildContext, String) onNavigatePressed;
  final Function(String containerNumber) updateContainerNumber;
  final Function(String containerWeight) updateContainerWeight;
  final Function(String packWeight) updatePackWeight;
  final Function(String weightUnit) updateContainerWeightUnit;
  final Function(String weightUnit) updatePackWeightUnit;
  final Function(BuildContext context) confirmPortCargoUpdate;
  final Function(String movementAt) updateUpdateAt;

  var dateTime = DateFormat.yMMMEd().format(DateTime.now()).toString();
  var time = DateTime.now().toString().substring(10, 20);

  PortCargoUpdateSummaryVM({
    required this.isLoading,
    required this.authState,
    required this.account,
    required this.accountPortDepot,
    required this.accountCarrier,
    required this.portCargoUpdateRole,
    required this.portCargoUpdateCreate,
    required this.universalBranding,
    required this.weightUnits,
    required this.onNavigatePressed,
    required this.updateContainerNumber,
    required this.updateContainerWeight,
    required this.updatePackWeight,
    required this.updateContainerWeightUnit,
    required this.updatePackWeightUnit,
    required this.confirmPortCargoUpdate,
    required this.updateUpdateAt,
  });

  static PortCargoUpdateSummaryVM fromStore(Store<AppState> store) {
    return PortCargoUpdateSummaryVM(
      isLoading: store.state.isLoading,
      authState: store.state.authState,
      account: store.state.accountState.account,
      accountPortDepot: store.state.accountState.accountPortDepots
          .where((element) =>
              element.id ==
              store.state.portCargoUpdateState.portCargoUpdateCreate
                  .accountPortLocationId)
          .first,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      weightUnits: CadiEnumFactory().createWeightMeasurements(),
      accountCarrier: store.state.accountState.accountCarriers.firstWhere(
          (element) =>
              element.carrierId ==
              store.state.portCargoUpdateState.portCargoUpdateCreate.carrierId,
          orElse: () => AccountCarrier.initial()),

      portCargoUpdateRole: store.state.uiState.portCargoUpdateRole,
      portCargoUpdateCreate: store.state.portCargoUpdateState.portCargoUpdateCreate,

      onNavigatePressed: (BuildContext context, String route) {
        if (store.state.isLoading) {
          return;
        }
        navigation_helper.navigateRoute(route, store, null);
      },
      updateContainerNumber: (String containerNumber) {
        store.dispatch(
            UpdatePortCargoUpdateContainerNumber(containerNumber.replaceAll(' ', '')));
      },
      updateContainerWeight: (String containerWeight) {
        num numContainerWeight = num.tryParse(containerWeight) ?? 0.00;
        store.dispatch(UpdatePortCargoUpdateContainerWeight(numContainerWeight));
      },
      updatePackWeight: (String packWeight) {
        num numWeight = num.tryParse(packWeight) ?? 0.00;
        store.dispatch(UpdatePortCargoUpdatePackWeight(numWeight));
      },
      updateContainerWeightUnit: (String weightUnit) {
        store.dispatch(UpdatePortCargoUpdateContainerWeightUnit(weightUnit));
      },
      updatePackWeightUnit: (String weightUnit) {
        store.dispatch(UpdatePortCargoUpdatePackWeightUnit(weightUnit));
      },
      updateUpdateAt: (String updateAt) {
        store.dispatch(UpdatePortCargoUpdateAt(updateAt));
      },
      confirmPortCargoUpdate: (BuildContext context) {
        store.dispatch(QueuePortCargoUpdate(
            store.state.portCargoUpdateState.portCargoUpdateCreate));
        if (store.state.connectivityState.connectionStatus ==
                ConnectivityConstants.none ||
            store.state.connectivityState.connectionStatus ==
                ConnectivityConstants.ethernet) {
          showMessageWithActions(context, 'Queued',
              "Cargo update has scheduled for submission when device is online.",
              () {
            Navigator.pop(context);
            store.dispatch(NavigateToAction.pushNamedAndRemoveUntil(
                route_helper.portCargoUpdateCompletionScreenBuilder,
                (route) => false));
          }, "Ok", null, "");
        } else {
          store.dispatch(NavigateToAction.pushNamedAndRemoveUntil(
              route_helper.portCargoUpdateCompletionScreenBuilder,
              (route) => false));

          processPortCargoUpdateQueue(store);
        }
      },
    );
  }
}
