import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/base/base_button.dart';
import 'package:cadi_core_app/ui/shared/components/destination.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../ui.dart';

List<Destination> buildHomeScreenDestinations(String activeService,
    Widget userWidget, Widget appLogo, List<Widget> appHomeWidgets) {
  final List<Destination> homeDestinations = <Destination>[];

  List<_DestinationMenuItem> destinationList = [];
  var index = 0;
  // activeService = CadiAppServices.truckMovements;
  //TODO DELETE
  switch (activeService) {
    case CadiAppServices.portMovements:
      destinationList = [
        _DestinationMenuItem(
            'cargo_movements', const CargoMovementHomeScreenBuilder()),
        // _DestinationMenuItem('cargo_updates', const CargoUpdateSearchScreenBuilder()),
        _DestinationMenuItem(
            'port_cargo_updates', const PortCargoUpdateHomeScreenBuilder()),
        _DestinationMenuItem('reports', const CargoReportHomeBuilder())
      ];

      break;
    case CadiAppServices.portEvents:
      destinationList = [
        _DestinationMenuItem(
            'port_events', const PortEventIndexScreenBuilder()),
      ];
      break;
    case CadiAppServices.roadMovements:
      destinationList = [
        _DestinationMenuItem(
            'road_movements', const RoadMovementHomeScreenBuilder()),
        _DestinationMenuItem('reports', const RoadMovementHomeScreenBuilder()),
      ];
      break;
    case CadiAppServices.roadEvents:
      destinationList = [];
      break;
  }

  //This will always be added.
  destinationList.add(_DestinationMenuItem(
      'settings', const AccountSettingsIndexScreenBuilder()));

//Make sure there is ALWAYS at least one value in destination list.
  for (final _destination in destinationList) {
    index = index + 1;
    homeDestinations.add(Destination(
        index,
        _destination.title,
        userWidget,
        // appHeaderText("", 30, BrandingColors.black),
        appLogo,
        appHomeWidgets,
        _destination.menuWidget));
  }

  return homeDestinations;
}

Widget buildHomeScreen(
    BuildContext context,
    final void Function(int) callback,
    String activeService,
    bool queued,
    bool processingQueue,
    int processingIndex,
    int processingTotal,
    final void Function(BuildContext) syncCallback) {
  var topHeight = MediaQuery.of(context).size.height * 0.06;
  List<Widget> homeMenuItems = [];
  switch (activeService) {
    case CadiAppServices.portMovements:
      homeMenuItems = [
        buildHomeMenuItem(
            context,
            "Cargo Movements",
            () => callback(HomeNavigationConstants.cargoMovementIndex),
            BrandIcons.cargoMovement,
            activeService),
        buildHomeMenuItem(
            context,
            "Cargo Updates",
            () => callback(HomeNavigationConstants.portCargoUpdateIndex),
            BrandIcons.cargoUpdate,
            activeService),
        buildHomeMenuItem(
            context,
            "Reports",
            () => callback(HomeNavigationConstants.reportsIndex),
            BrandIcons.reports,
            activeService),
        buildHomeMenuItem(
            context,
            "Account & Settings",
            () => callback(HomeNavigationConstants.settingsScreenIndex),
            BrandIcons.settings,
            activeService)
      ];
      break;
    case CadiAppServices.portEvents:
      homeMenuItems = [
        buildHomeMenuItem(context, "Port Events", () => callback(1),
            BrandIcons.vessel, activeService),
        buildHomeMenuItem(context, "Account & Settings", () => callback(2),
            BrandIcons.settings, activeService)
      ];
      break;
    case CadiAppServices.roadMovements:
      homeMenuItems = [
        buildHomeMenuItem(
            context,
            "Road Movements",
            () => callback(HomeNavigationConstants.roadMovementIndex),
            BrandIcons.truck,
            activeService),
        buildHomeMenuItem(context, "Reports", () => callback(2),
            BrandIcons.reports, activeService),
        buildHomeMenuItem(context, "Account & Settings", () => callback(3),
            BrandIcons.settings, activeService)
      ];
      break;
    case CadiAppServices.roadEvents:
      homeMenuItems = [
        buildHomeMenuItem(context, "Account & Settings", () => callback(1),
            BrandIcons.settings, activeService)
      ];
      break;
    default:
      homeMenuItems = [
        buildHomeMenuItem(context, "Account & Settings", () => callback(1),
            BrandIcons.settings, activeService)
      ];
  }
  // homeMenuItems.add(buildHomeMenuItem(
  //     context, "Account & Settings", () => callback(4), BrandIcons.settings));

  List<Widget> displayWidgets = <Widget>[];

  if (processingQueue) {
    displayWidgets.add(Padding(
      padding: EdgeInsets.only(top: topHeight / 8),
      child: Align(
        child: Text("Transactions Syncing $processingIndex of $processingTotal",
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: PlatformSizing().inputTextFieldSize(context),
                color: BrandingColors.red)),
      ),
    ));

    displayWidgets.add(Padding(
      padding: EdgeInsets.only(top: topHeight / 8),
      child: const CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(BrandingColors.red),
        strokeWidth: 6.0,
      ),
    ));
  } else if (queued) {
    displayWidgets.add(Padding(
      padding: EdgeInsets.only(top: topHeight / 8),
      child: Align(
        child: Text("Transactions Pending!",
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: PlatformSizing().inputTextFieldSize(context),
                color: BrandingColors.red)),
      ),
    ));

    displayWidgets.add(Padding(
      padding: EdgeInsets.only(top: topHeight / 8),
      child: BaseButton(
          color: BrandingColors.red,
          child: Text('Sync Now',
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: PlatformSizing().normalTextSize(context))),
          onPressed: () => syncCallback(context)),
    ));
  }

  displayWidgets.add(
    Padding(
      padding: EdgeInsets.only(top: topHeight / 8),
      child: Align(
        child: Text("Welcome",
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: PlatformSizing().headingTextSize(context),
                color: BrandingColors.cadiBlack)),
      ),
    ),
  );

  displayWidgets.add(
    Padding(
      padding: EdgeInsets.only(top: topHeight / 38, bottom: 30),
      child: Center(
        child: Text(getAppServiceName(activeService),
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: PlatformSizing().inputTextFieldSize(context),
                color: UniversalBranding(activeService).getPrimaryColor())),
      ),
    ),
  );

  displayWidgets.add(Padding(
    padding: const EdgeInsets.all(10),
    child: ListView(shrinkWrap: true, children: homeMenuItems),
  ));

  return Container(
      color: BrandingColors.cadiWhite,
      child: ListView(
          padding: EdgeInsets.only(left: 20, right: 20, top: topHeight),
          children: displayWidgets));
}

@override
Widget buildHomeMenuItem(BuildContext context, menuName, Function() navFunction,
    IconData icon, String activeService) {
  var topHeight = MediaQuery.of(context).size.height;
  return Card(
    elevation: 3,
    color: BrandingColors.background,
    shadowColor: BrandingColors.cadiGrey,
    shape: RoundedRectangleBorder(
      side: const BorderSide(
        color: BrandingColors.cadiBlack,
        width: 0.9,
      ),
      borderRadius: BorderRadius.circular(25),
    ),
    child: InkWell(
        highlightColor: UniversalBranding(activeService).getPrimaryColor(),
        borderRadius: const BorderRadius.all(Radius.circular(25)),
        onTap: navFunction,
        child: Container(
            padding: const EdgeInsets.only(
              top: 30,
              bottom: 30,
              left: 40,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                FaIcon(icon,
                    color: BrandingColors.cadiBlack, size: topHeight * 0.04),
                // Text(" "),
                Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Text(menuName,
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            fontWeight: FontWeight.w300,
                            fontSize: PlatformSizing().normalTextSize(context),
                            color: BrandingColors.cadiBlack))),
              ],
            ))),
  );
}

//#private
class _DestinationMenuItem {
  final String title;
  final Widget menuWidget;

  _DestinationMenuItem(this.title, this.menuWidget);
}
