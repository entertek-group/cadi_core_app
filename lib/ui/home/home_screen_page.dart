import 'dart:async';

import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bottom_nav_bar.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/popup_menu.dart';
import 'package:cadi_core_app/ui/ui.dart';
import 'package:flutter/material.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cadi_core_app/ui/shared/components/destination.dart';
//#helpers
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:cadi_core_app/theme/theme.dart';
// import 'package:cadi_core_app/ui/app/app_logo.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'destination_builder/destination_builder.dart';
import 'home_screen_vm.dart';
import 'dart:io' show Platform;

class HomeView extends StatefulWidget {
  final HomeVM viewModel;

  const HomeView({
    super.key,
    required this.viewModel,
  });

  @override
  State<HomeView> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<HomeView> {
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<List<ConnectivityResult>> _connectivitySubscription;
  var accountSettingList = <PopMenuItem>[];

  @override
  initState() {
    super.initState();
    widget.viewModel.resetHomeIndex(context);
    widget.viewModel.activeService;
    widget.viewModel.getConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    if (widget.viewModel.accounts != null) {
      for (final account in widget.viewModel.accounts!.accounts) {
        var a = PopMenuItem(
            account.name, () => widget.viewModel.setAccount(account));
        accountSettingList.add(a);
      }
    }
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  Future<void> _updateConnectionStatus(List<ConnectivityResult> result) async {
    await widget.viewModel.updateConnectivity(result);
  }

  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      border: Border.all(
        width: 1, //                   <--- border width here
      ),
    );
  }

  Widget _userWidget() {
    return appBarUserAccount(context, () {
      if (widget.viewModel.homeIndex == 0) {
        showPopupMenu(
            context, "Account", "Select an Account", accountSettingList);
      } else {
        showOkMessage(
            // FontAwesomeIcons.warning,
            context,
            "Action Blocked",
            "You cannot change account while performing this action.");
      }
    });
  }

  navigateToIndex(int index) {
    widget.viewModel.setHomeIndex(index);
  }

  syncNow(BuildContext context) {
    widget.viewModel.syncNow(context);
  }

  @override
  Widget build(BuildContext context) {
    homeButtonAction() => setState(() {
          widget.viewModel
              .setHomeIndex(HomeNavigationConstants.homeScreenIndex);
          widget.viewModel.resetHomeIndex;
        });

    final List<Destination> allDestinations = <Destination>[];

    var movementQueued = false;
    var processingQueue = false;

    switch (widget.viewModel.activeService) {
      case CadiAppServices.portMovements:
        movementQueued = widget.viewModel.cargoMovementQueued || widget.viewModel.portCargoUpdateQueued;
        processingQueue = widget.viewModel.cargoMovementProcessingQueue || widget.viewModel.portCargoUpdateProcessingQueue;
        break;
      case CadiAppServices.roadMovements:
        movementQueued = widget.viewModel.roadMovementQueued;
        processingQueue = widget.viewModel.roadMovementProcessingQueue;
        break;
      case CadiAppServices.portEvents:
        movementQueued = widget.viewModel.portEventQueued;
        processingQueue = widget.viewModel.portEventProcessingQueue;
        break;
      default:
        movementQueued = false;
        processingQueue = false;
        break;
    }

    allDestinations.add(Destination(
        0,
        'Home',
        _userWidget(),
        appBarLogo(context),
        <Widget>[
          appBarLogoutButton(() => widget.viewModel.logOut(context), context)
        ],
        buildHomeScreen(
            context,
            navigateToIndex,
            widget.viewModel.activeService,
            movementQueued,
            processingQueue,
            widget.viewModel.processingIndex,
            widget.viewModel.processingTotal,
            syncNow)));

    allDestinations.addAll(buildHomeScreenDestinations(
        widget.viewModel.activeService,
        _userWidget(),
        appBarLogo(context), <Widget>[
      appBarHomeButton(context, false),
    ]));

    return PlatformScaffold(
      appBar: appBar(
          context,
          allDestinations[widget.viewModel.homeIndex].titleWidget,
          allDestinations[widget.viewModel.homeIndex].leading,
          allDestinations[widget.viewModel.homeIndex].trailing),
      body: allDestinations[widget.viewModel.homeIndex].body,
      bottomNavBar: allDestinations.length > 2
          ? appBottomNavBar(context, false, BottomNavigationBarType.fixed,
              widget.viewModel.activeService)
          : null,
    );
  }
}

Widget homeIconButton(Function() function) {
  return PlatformIconButton(
    onPressed: function,
    materialIcon:
        const FaIcon(FontAwesomeIcons.house, color: BrandingColors.cadiBlack),
    cupertinoIcon:
        const FaIcon(FontAwesomeIcons.house, color: BrandingColors.cadiBlack),
    material: (_, __) => MaterialIconButtonData(),
    cupertino: (_, __) => CupertinoIconButtonData(),
  );
}

Widget appBarLogo(BuildContext context) {
  var dynamicSize = MediaQuery.of(context).size.width * 0.0035;
  var edgeInsets = EdgeInsets.all(dynamicSize);
  var height = MediaQuery.of(context).size.height * 0.04;

  if (Platform.isIOS) {
    dynamicSize = MediaQuery.of(context).size.width * 0.047;
    edgeInsets = EdgeInsets.only(
      left: dynamicSize,
      right: dynamicSize * 0.1,
      bottom: dynamicSize,
    );
  }

  // Adding extra padding to the top and left
  var extraPadding = EdgeInsets.only(
    top: MediaQuery.of(context).size.height * 0.02,
    left: MediaQuery.of(context).size.width * 0.04,
  );

  return Padding(
    padding: extraPadding,
    child: RichText(
      text: TextSpan(
        children: [
          WidgetSpan(
            child: Padding(
              padding: edgeInsets,
              child: Image.asset(
                BrandingImages.logoBlack,
                height: height,
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
