import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:cadi_core_app/services/data_processing_service.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
//#helpers
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/services/connectivity_service.dart';
import 'package:redux/redux.dart';
import 'home_screen_page.dart';
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class HomeScreen extends StatelessWidget {
  static const String route = route_helper.homeScreen;

  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, HomeVM>(
        converter: HomeVM.fromStore,
        builder: (context, vm) {
          return HomeView(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class HomeVM {
  ConnectivityState connectivityState;
  UniversalBranding universalBranding;
  final Function getConnectivity;
  final Function(List<ConnectivityResult> result) updateConnectivity;
  final Function(BuildContext context) logOut;
  final AuthState authState;
  final Account account;
  final AccountList? accounts;
  final Function(Account account) setAccount;
  final int homeIndex;
  final Function(BuildContext context) resetHomeIndex;
  final Function(int index) setHomeIndex;
  final Function(BuildContext context) syncNow;
  final String activeService;
  final bool cargoMovementQueued;
  final bool roadMovementQueued;
  final bool portEventQueued;
  final bool portCargoUpdateQueued;
  final int processingIndex;
  final int processingTotal;
  final bool cargoMovementProcessingQueue;
  final bool roadMovementProcessingQueue;
  final bool portEventProcessingQueue;
  final bool portCargoUpdateProcessingQueue;

  HomeVM(
      {required this.connectivityState,
      required this.universalBranding,
      required this.getConnectivity,
      required this.updateConnectivity,
      required this.logOut,
      required this.authState,
      required this.account,
      required this.accounts,
      required this.setAccount,
      required this.homeIndex,
      required this.resetHomeIndex,
      required this.setHomeIndex,
      required this.syncNow,
      required this.activeService,
      required this.cargoMovementQueued,
      required this.roadMovementQueued,
      required this.portEventQueued,
      required this.portCargoUpdateQueued,
      required this.processingIndex,
      required this.processingTotal,
      required this.cargoMovementProcessingQueue,
      required this.roadMovementProcessingQueue,
      required this.portEventProcessingQueue,
      required this.portCargoUpdateProcessingQueue});

  static HomeVM fromStore(Store<AppState> store) {
    return HomeVM(
        connectivityState: store.state.connectivityState,
        universalBranding:
            UniversalBranding(store.state.accountState.activeService),
        getConnectivity: () {
          initConnectivity(store);
        },
        homeIndex: store.state.homeIndex,
        authState: store.state.authState,
        account: store.state.accountState.account,
        accounts: store.state.accountState.accountList,
        activeService: store.state.accountState.activeService,
        cargoMovementQueued: store.state.cargoMovementState.queued,
        roadMovementQueued: store.state.roadMovementState.queued,
        portEventQueued: store.state.portEventState.queued,
        portCargoUpdateQueued: store.state.portCargoUpdateState.queued,
        setHomeIndex: (int index) {
          store.dispatch(SetHomeIndex(index));
        },
        processingIndex: store.state.processingIndex,
        processingTotal: store.state.processingTotal,
        cargoMovementProcessingQueue:
            store.state.cargoMovementState.processingQueue,
        roadMovementProcessingQueue:
            store.state.roadMovementState.processingQueue,
        portEventProcessingQueue: store.state.portEventState.processingQueue,
        portCargoUpdateProcessingQueue:
            store.state.portCargoUpdateState.processingQueue,
        syncNow: (BuildContext context) {
          if (store.state.connectivityState.connectionStatus ==
                  ConnectivityConstants.none ||
              store.state.connectivityState.connectionStatus ==
                  ConnectivityConstants.ethernet) {
            showMessageWithActions(context, 'Offline',
                "Your device is presently offline. Please connect to the internet and try again.",
                () {
              Navigator.pop(context);
            }, "Ok", null, "");
          } else {
            processAllQueues(store);
          }
        },
        resetHomeIndex: (BuildContext context) {
          store.dispatch(SetHomeIndex(HomeNavigationConstants.homeScreenIndex));
        },
        setAccount: (Account account) {
          store.dispatch(SetSelectedAccount(account));
        },
        logOut: (context) {
          if (store.state.cargoMovementState.queued) {
            showOkMessage(context, "WARNING!",
                "You have pending Port Movements. Please select 'Sync Now' on the home page before logging off..");
            processCargoMovementQueue(store);
          } else if (store.state.roadMovementState.queued) {
            processRoadMovementQueue(store);
            showOkMessage(context, "WARNING!",
                "You have pending Road Movements. Please select 'Sync Now' on the home page before logging off..");
          } else if (store.state.portEventState.queued) {
            processPortEventQueue(store);
            showOkMessage(context, "WARNING!",
                "You have pending Port Events. Please select 'Sync Now' on the home page before logging off..");
          } else if (store.state.portCargoUpdateState.queued) {
            processPortCargoUpdateQueue(store);
            showOkMessage(context, "WARNING!",
                "You have pending Cargo Updates. Please select 'Sync Now' on the home page before logging off..");
          } else {
            showMessageWithActions(
                context,
                "Logout Confirmation",
                "Are you sure you would like to logout?",
                () {
                  Navigator.pop(context);
                  navigation_helper.navigateRoute(
                      route_helper.logoutScreen, store, null);
                },
                "Yes",
                () {
                  Navigator.pop(context);
                },
                "No");
          }
        },
        updateConnectivity: (List<ConnectivityResult> result) {
          String connectionStatus = getConnectivityStatus(result);
          store.dispatch(
              SetConnectivity(connectionStatus, DateTime.now().toString()));
          //#Test this
        });
  }
}
