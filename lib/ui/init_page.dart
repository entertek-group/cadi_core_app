import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:cadi_core_app/redux/actions/auth_actions.dart';
import 'package:after_layout/after_layout.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';

class InitScreen extends StatefulWidget {
  static const String route = '/';

  const InitScreen({super.key});

  @override
  InitScreenState createState() => InitScreenState();
}

class InitScreenState extends State<InitScreen>
    with AfterLayoutMixin<InitScreen> {
  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
        body: Container(
      color: Colors.white,
      child: Stack(
        children: <Widget>[
          Container(),
          const AppLoading(message: "Loading...")
        ],
      ),
    ));
  }

  @override
  void afterFirstLayout(BuildContext context) {
    showInit();
  }

  void showInit() {
    StoreProvider.of<AppState>(context).dispatch(LoadStateRequest(context));
  }
}
