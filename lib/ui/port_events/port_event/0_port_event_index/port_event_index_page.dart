import 'package:cadi_core_app/factory/ui_factory.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/redux/models/account/account_carrier.dart';
import 'package:cadi_core_app/redux/models/account/account_port.dart';
import 'package:cadi_core_app/redux/models/account/account_port_depot.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';
import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'port_event_index_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

class PortEventPage extends StatefulWidget {
  final PortEventVM viewModel;
  const PortEventPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<PortEventPage> createState() {
    return PortEventPageState(viewModel: viewModel);
  }
}

class PortEventPageState extends State<PortEventPage> {
  final PortEventVM viewModel;
  //final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final _selectAccountPortDepotController = TextEditingController();
  final _selectPortController = TextEditingController();
  final _selectCarrierController = TextEditingController();
  final _selectRoleController = TextEditingController();

  static const Key _selectAccountPortDepotKey =
      Key(CargoEventKeys.selectAccountPortDepotKey);
  static const Key _selectPortKey = Key(CargoEventKeys.selectPortKey);
  static const Key _selectCarrierKey = Key(CargoEventKeys.selectCarrierKey);
  static const Key _selectRoleKey = Key(CargoEventKeys.selectRoleKey);

  int _selectedLocationIndex = 0;
  int _selectedPortIndex = 0;
  int _selectedCarrierIndex = 0;
  int _selectedRoleIndex = 0;

  List<DropdownItem> _accountLocationItems = [];
  List<DropdownItem> _portItems = [];
  List<DropdownItem> _carrierItems = [];
  List<DropdownItem> _roleItems = [];

  List<AccountPortDepot> _accountPortDepots = [];

  PortEventPageState({
    Key? key,
    required this.viewModel,
  });

  void _setLocation(int index) {
    setState(() {
      _selectAccountPortDepotController.text =
          _accountLocationItems[index].text;
      _selectedLocationIndex = index;
      widget.viewModel.setSelectedLocationIndex(index);
    });
  }

  void _setPort(int index) {
    setState(() {
      _selectPortController.text = _portItems[index].text;
      _selectedPortIndex = index;
      widget.viewModel.setSelectedPortIndex(index);
      _accountPortDepots = widget.viewModel.accountPortDepots
          .where((element) =>
              element.portId == _portItems[_selectedPortIndex].value)
          .toList();
      _accountLocationItems = UiFactory()
          .convertToDropdown<AccountPortDepot, int>(
              _accountPortDepots, (item) => item.depotName, (item) => item.id);
      _selectedLocationIndex = 0;
    });
    if (_accountLocationItems.isNotEmpty &&
        _selectedLocationIndex < _accountLocationItems.length) {
      _selectAccountPortDepotController.text =
          _accountLocationItems[_selectedLocationIndex].text;
    }
  }

  void _setCarrier(int index) {
    setState(() {
      _selectCarrierController.text = _carrierItems[index].text;
      _selectedCarrierIndex = index;
      widget.viewModel.setSelectedCarrierIndex(index);
    });
  }

  void _setRole(int index) {
    setState(() {
      _selectRoleController.text = _roleItems[index].text;
      _selectedRoleIndex = index;
      widget.viewModel.setSelectedRoleIndex(index);
    });
  }

  @override
  initState() {
    super.initState();
    final uiFactory = UiFactory();

    _portItems = uiFactory.convertToDropdown<AccountPort, int>(
        widget.viewModel.accountPorts,
        (port) => port.unLocode,
        (port) => port.portId);

    _carrierItems = uiFactory.convertToDropdown<AccountCarrier, int>(
        widget.viewModel.accountCarriers,
        (carrier) => carrier.carrierName,
        (carrier) => carrier.id);

    _roleItems = uiFactory.convertToDropdown<CadiEnum, String>(
        widget.viewModel.portEventRoles,
        (role) => role.description,
        (role) => role.id);

    _accountPortDepots = widget.viewModel.accountPortDepots;

    _selectedPortIndex = widget.viewModel.uiPortEventIndex.selectedPortIndex;
    _selectedLocationIndex =
        widget.viewModel.uiPortEventIndex.selectedLocationIndex;
    _selectedCarrierIndex =
        widget.viewModel.uiPortEventIndex.selectedCarrierIndex;
    _selectedRoleIndex = widget.viewModel.uiPortEventIndex.selectedRoleIndex;

    if (widget.viewModel.accountPorts.isNotEmpty &&
        _selectedPortIndex < widget.viewModel.accountPorts.length) {
      _selectPortController.text = _portItems[_selectedPortIndex].text;
      _accountPortDepots = widget.viewModel.accountPortDepots
          .where((element) =>
              element.portId == _portItems[_selectedPortIndex].value)
          .toList();

      _accountLocationItems = UiFactory()
          .convertToDropdown<AccountPortDepot, int>(
              _accountPortDepots, (item) => item.depotName, (item) => item.id);

      if (_accountLocationItems.isNotEmpty &&
          _selectedLocationIndex < _accountLocationItems.length) {
        _selectAccountPortDepotController.text =
            _accountLocationItems[_selectedLocationIndex].text;
      }
    }

    if (widget.viewModel.accountCarriers.isNotEmpty &&
        _selectedCarrierIndex < widget.viewModel.accountCarriers.length) {
      _selectCarrierController.text = _carrierItems[_selectedCarrierIndex].text;
    }

    if (widget.viewModel.portEventRoles.isNotEmpty &&
        _selectedRoleIndex < widget.viewModel.portEventRoles.length) {
      _selectRoleController.text = _roleItems[_selectedRoleIndex].text;
    }
  }

  @override
  void dispose() {
    _selectAccountPortDepotController.dispose();
    _selectPortController.dispose();
    _selectCarrierController.dispose();
    _selectRoleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final List<SettingTile> allSettings = <SettingTile>[
      SettingTile(
          0,
          'Active Account',
          ListTile(
              title: Text("Port Event",
                  style: TextStyle(

                      // fontWeight: FontWeight.w500,
                      fontSize: PlatformSizing().normalTextSize(context),
                      color: widget.viewModel.universalBranding
                          .getPrimaryColor())))),
      SettingTile(
          1,
          'Account Operations',
          ListTile(
              title: Text("Select Location, Carrier and Role",
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      fontWeight: FontWeight.w300,
                      color: BrandingColors.cadiBlack)))),
      SettingTile(
          2,
          'PORT',
          ListTile(
            title: Text("Port",
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          )),
      SettingTile(
          3,
          "",
          InkWell(
            onTap: () {
              if (widget.viewModel.accountPortDepots.isEmpty) {
                showOkMessage(context, "Cannot select Port",
                    "Account setup is incomplete. Please contact support. ");
              } else {
                showBottomDropdown(context, 'Port', 'Please select a port',
                    _portItems, _selectedPortIndex, _setPort, true);
              }
            },
            child: PlatformTextField(
              cursorColor: BrandingColors.cadiGrey,
              textCapitalization: TextCapitalization.characters,
              controller: _selectPortController,
              key: _selectPortKey,
              autocorrect: false,
              enabled: false,
              material: (_, __) => MaterialAssetsDark()
                  .textFieldData("", FontAwesomeIcons.chevronDown),
              cupertino: (_, __) => CupertinoAssets()
                  .textFieldData('Port', FontAwesomeIcons.chevronDown),
              style: TextStyle(
                  color: BrandingColors.cadiBlack,
                  fontWeight: FontWeight.w400,
                  fontSize: PlatformSizing().inputTextFieldSize(context)),
            ),
          )),
      SettingTile(
          4,
          'LOCATION',
          ListTile(
            title: Text('Location',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          )),
      SettingTile(
          5,
          "",
          InkWell(
            onTap: () {
              if (widget.viewModel.accountPortDepots.isEmpty) {
                showOkMessage(context, "Cannot select Location",
                    "Account setup is incomplete. Please contact support. ");
              } else {
                showBottomDropdown(
                    context,
                    'Location',
                    'Please select a Location',
                    _accountLocationItems,
                    _selectedLocationIndex,
                    _setLocation,
                    true);
              }
            },
            child: PlatformTextField(
              cursorColor: BrandingColors.cadiGrey,
              textCapitalization: TextCapitalization.characters,
              controller: _selectAccountPortDepotController,
              key: _selectAccountPortDepotKey,
              autocorrect: false,
              enabled: false,
              material: (_, __) => MaterialAssetsDark()
                  .textFieldData("", FontAwesomeIcons.chevronDown),
              cupertino: (_, __) => CupertinoAssets()
                  .textFieldData('Location', FontAwesomeIcons.chevronDown),
              style: TextStyle(
                  color: BrandingColors.cadiBlack,
                  fontWeight: FontWeight.w400,
                  fontSize: PlatformSizing().inputTextFieldSize(context)),
              keyboardType: TextInputType.text,
            ),
          )),
      SettingTile(
          6,
          'CARRIER',
          ListTile(
            title: Text('Carrier',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          )),
      SettingTile(
          7,
          "",
          InkWell(
            onTap: () {
              if (widget.viewModel.accountCarriers.isEmpty) {
                showOkMessage(context, "Cannot select Carrier",
                    "Account setup is incomplete. Please contact support. ");
              } else {
                showBottomDropdown(
                    context,
                    'Carrier',
                    'Please select a Carrier',
                    _carrierItems,
                    _selectedCarrierIndex,
                    _setCarrier,
                    true);
              }
            },
            child: PlatformTextField(
              cursorColor: BrandingColors.cadiGrey,
              textCapitalization: TextCapitalization.characters,
              controller: _selectCarrierController,
              key: _selectCarrierKey,
              autocorrect: false,
              enabled: false,
              material: (_, __) => MaterialAssetsDark()
                  .textFieldData("", FontAwesomeIcons.chevronDown),
              cupertino: (_, __) => CupertinoAssets()
                  .textFieldData('Carrier', FontAwesomeIcons.chevronDown),
              style: TextStyle(
                  color: BrandingColors.cadiBlack,
                  fontWeight: FontWeight.w400,
                  fontSize: PlatformSizing().inputTextFieldSize(context)),
              keyboardType: TextInputType.text,
            ),
          )),
      SettingTile(
          8,
          'ROLE',
          ListTile(
            title: Text('Role',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          )),
      SettingTile(
          9,
          "",
          InkWell(
            onTap: () {
              if (widget.viewModel.portEventRoles.isEmpty) {
                showOkMessage(context, "Cannot select Role",
                    "Account setup is incomplete. Please contact support. ");
              } else {
                showBottomDropdown(context, 'Role', 'Please select a Role',
                    _roleItems, _selectedRoleIndex, _setRole, true);
              }
            },
            child: PlatformTextField(
              cursorColor: BrandingColors.cadiGrey,
              textCapitalization: TextCapitalization.characters,
              controller: _selectRoleController,
              key: _selectRoleKey,
              autocorrect: false,
              enabled: false,
              readOnly: true,
              material: (_, __) => MaterialAssetsDark()
                  .textFieldData("", FontAwesomeIcons.chevronDown),
              cupertino: (_, __) => CupertinoAssets()
                  .textFieldData('Role', FontAwesomeIcons.chevronDown),
              style: TextStyle(
                  color: BrandingColors.cadiBlack,
                  fontWeight: FontWeight.w400,
                  fontSize: PlatformSizing().inputTextFieldSize(context)),
              keyboardType: TextInputType.text,
            ),
          )),
      const SettingTile(
          10,
          '',
          ListTile(
            title: Text(
              '',
            ),
          )),
      SettingTile(
          11,
          '',
          Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 50.0, vertical: 0.0),
              child: BaseButton(
                  color: widget.viewModel.universalBranding
                      .getButtonPrimaryColor(),
                  child: Text('Go',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: PlatformSizing().normalTextSize(context))),
                  onPressed: () {
                    viewModel.onNavigatePressed(
                        context,
                        _roleItems[_selectedRoleIndex].value,
                        _accountLocationItems[_selectedLocationIndex].value,
                        _carrierItems[_selectedCarrierIndex].value);
                  }))),
    ];

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.only(top: 30.0, left: 40, right: 40),
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: allSettings.length,
          padding: const EdgeInsets.only(
            left: 10.0,
            right: 10,
          ),
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
  }
}
