import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'port_event_index_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
class PortEventIndexScreenBuilder extends StatelessWidget {
  const PortEventIndexScreenBuilder({super.key});

  static const String route = route_helper.portEventIndexScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: BrandingColors.cadiWhite,
      body: StoreConnector<AppState, PortEventVM>(
        converter: PortEventVM.fromStore,
        builder: (context, vm) {
          return PortEventPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class PortEventVM {
  bool isLoading;
  AuthState authState;
  Account account;
  List<AccountCarrier> accountCarriers;
  List<CadiEnum> portEventRoles;
  List<AccountPortDepot> accountPortDepots;
  List<AccountPort> accountPorts;
  UniversalBranding universalBranding;

  UiPortEventIndex uiPortEventIndex;

  final Function(BuildContext, String, int, int) onNavigatePressed;
  final Function(int selectedPortIndex) setSelectedPortIndex;
  final Function(int selectedLocationIndex) setSelectedLocationIndex;
  final Function(int selectedCarrierIndex) setSelectedCarrierIndex;
  final Function(int selectedRoleIndex) setSelectedRoleIndex;
  final Function(String token) setToken;
  final Function(BuildContext context) initScreenData;
  PortEventVM(
      {required this.isLoading,
      required this.authState,
      required this.account,
      required this.accountCarriers,
      required this.portEventRoles,
      required this.accountPortDepots,
      required this.accountPorts,
      required this.universalBranding,
      required this.uiPortEventIndex,
      required this.onNavigatePressed,
      required this.setSelectedPortIndex,
      required this.setSelectedLocationIndex,
      required this.setSelectedCarrierIndex,
      required this.setSelectedRoleIndex,
      required this.setToken,
      required this.initScreenData});

  static PortEventVM fromStore(Store<AppState> store) {
    return PortEventVM(
      isLoading: store.state.isLoading,
      authState: store.state.authState,
      account: store.state.accountState.account,
      accountCarriers: store.state.accountState.filteredAccountCarriers,
      portEventRoles: store.state.referenceState.portEventRoles,
      accountPortDepots: store.state.accountState.filteredAccountPortDepots,
      accountPorts: store.state.accountState.filteredAccountPorts,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      uiPortEventIndex: store.state.uiState.uiPortEventIndex,
      initScreenData: (context) {
        store.dispatch(ResetCargoMovementCreate());
      },
      setToken: (String token) {},
      onNavigatePressed: (BuildContext context, String role,
          int accountPortDepotId, int carrierId) {
        if (store.state.isLoading) {
          return;
        }
        store.dispatch(SetPortEventIndexVariables(
            accountPortDepotId, carrierId, role));

        store.dispatch(UiPortEventSetRole(role));

        store.dispatch(
            NavigateToAction.push(route_helper.portEventCaptureBuilder));
      },
      setSelectedPortIndex: (int selectedPortIndex) {
        store.dispatch(UiPortEventIndexSetPortIndex(selectedPortIndex));
      },
      setSelectedLocationIndex: (int selectedLocationIndex) {
        store.dispatch(
            UiPortEventIndexSetLocationIndex(selectedLocationIndex));
      },
      setSelectedCarrierIndex: (int selectedCarrierIndex) {
        store.dispatch(
            UiPortEventIndexSetCarrierIndex(selectedCarrierIndex));
      },
      setSelectedRoleIndex: (int selectedRoleIndex) {
        store.dispatch(UiPortEventIndexSetRoleIndex(selectedRoleIndex));
      },
    );
  }
}
