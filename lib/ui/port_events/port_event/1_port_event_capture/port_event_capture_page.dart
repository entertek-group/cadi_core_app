import 'dart:convert';
import 'dart:io';
import 'package:cadi_core_app/factory/ui_factory.dart';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/helpers/datetime_helper.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/redux/models/image/image_create.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/base/base_table.dart';
import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';
import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:cadi_core_app/ui/shared/components/time_picker.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'port_event_capture_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

class PortEventCapturePage extends StatefulWidget {
  final PortEventCaptureVM viewModel;
  const PortEventCapturePage({
    super.key,
    required this.viewModel,
  });

  @override
  State<PortEventCapturePage> createState() {
    return PortEventCaptureState(viewModel: viewModel);
  }
}

class PortEventCaptureState extends State<PortEventCapturePage> {
  final PortEventCaptureVM viewModel;
  final _vesselNameController = TextEditingController();
  final _voyageNumberController = TextEditingController();
  final _startDateController = TextEditingController();
  final _endDateController = TextEditingController();
  final _commentController = TextEditingController();
  final _selectBerthController = TextEditingController();

  static const Key _vesselNameKey = Key(CargoEventKeys.vesselNameKey);
  static const Key _voyageNumberKey = Key(CargoEventKeys.voyageNumberKey);
  static const Key _endDateKey = Key(CargoEventKeys.endDateKey);
  static const Key _startDateKey = Key(CargoEventKeys.startDateKey);
  static const Key _searchCommentKey = Key(CargoEventKeys.searchCommentKey);
  static const Key _selectBerthKey = Key(CargoEventKeys.selectBerthKey);

  int _selectedVesselIndex = 0;
  int _selectedBerthIndex = 0;
  int _vesselNameLength = 0;
  int? _berthId;
  bool _commentRequired = false;

  bool _showBerth = false;
  bool _showEventCompleteAt = false;

  List<TableRow> imageTableRows = <TableRow>[];
  List<ImageCreate> attachedImages = <ImageCreate>[];

  List<DropdownItem> _vesselItems = [];
  List<DropdownItem> _berthItems = [];

  String startDate = "";
  String endDate = "";

  DateTime dateTime = DateTime.now();

  final FocusNode voyageInputNode = FocusNode();

  PortEventCaptureState({
    Key? key,
    required this.viewModel,
  });

  @override
  initState() {
    super.initState();
    final uiFactory = UiFactory();

    _vesselNameController.text = widget.viewModel.uiPortEventCapture.vesselName;
    _voyageNumberController.text =
        widget.viewModel.uiPortEventCapture.voyageNumber;

    setState(() {
      _vesselNameLength = widget.viewModel.uiPortEventCapture.vesselName.length;
      dateTime = DateTime.now();
      startDate = getDateTimeWithOffset(dateTime);
      endDate = getDateTimeWithOffset(dateTime);
    });

    var inputFormat = DateFormat('yyyy-MM-dd HH:mm');
    _startDateController.text = inputFormat.format(dateTime);
    _endDateController.text = inputFormat.format(dateTime);

    if (widget.viewModel.accountVessels.isNotEmpty) {
      _vesselItems = uiFactory.convertToDropdown<AccountVessel, int>(
          widget.viewModel.accountVessels,
          (vessel) => vessel.vesselName,
          (vessel) => vessel.vesselId);
    }

    var accountPortBerths = widget.viewModel.accountBerths
        .where((element) =>
            element.portId == widget.viewModel.accountPortDepot.portId)
        .toList();

    if (accountPortBerths.isNotEmpty) {
      _berthItems = uiFactory.convertToDropdown<AccountBerth, int>(
          accountPortBerths, (berth) => berth.name, (berth) => berth.id);
      //Make provision for out of bound index error.
      if (widget.viewModel.uiPortEventCapture.berthIndex > _berthItems.length) {
        setState(() {
          _selectedBerthIndex = 0;
        });
      } else {
        setState(() {
          _selectedBerthIndex = widget.viewModel.uiPortEventCapture.berthIndex;
        });
      }

      _setBerth(_selectedBerthIndex);
    }

    if (widget.viewModel.portEventRole == CadiPortEventRole.delay ||
        widget.viewModel.portEventRole == CadiPortEventRole.refuel) {
      setState(() {
        _showBerth = false;
      });
    } else {
      setState(() {
        _showBerth = true;
      });
    }

    setState(() {
      _commentRequired =
          widget.viewModel.portEventRole == CadiPortEventRole.delay;
      _showEventCompleteAt =
          widget.viewModel.portEventRole == CadiPortEventRole.delay ||
              widget.viewModel.portEventRole == CadiPortEventRole.unLashing ||
              widget.viewModel.portEventRole == CadiPortEventRole.lashing ||
              widget.viewModel.portEventRole == CadiPortEventRole.hatchLids;
    });
// Uncomment and refactor this block if needed in the future
// if (widget.viewModel.attachedImages != null && widget.viewModel.attachedImages!.isNotEmpty) {
//   attachedImages = widget.viewModel.attachedImages!;
//   rebuildTableRows();
// }
  }

  @override
  void dispose() {
    _vesselNameController.dispose();
    _voyageNumberController.dispose();
    _startDateController.dispose();
    _endDateController.dispose();
    _commentController.dispose();
    _selectBerthController.dispose();
    widget.viewModel.stopLoading();
    super.dispose();
  }

  void _setVessel(int index) {
    setState(() {
      _vesselNameController.text = _vesselItems[index].text.toUpperCase();
      _selectedVesselIndex = index;
      _vesselNameLength = _vesselItems[index].text.toUpperCase().length;
    });
    FocusScope.of(context).requestFocus(voyageInputNode);
  }

  void _showVesselBottomPicker(String searchParams) {
    if (widget.viewModel.accountVessels.isNotEmpty) {
      var searchList = _vesselItems
          .where((element) =>
              element.text.toUpperCase().startsWith(searchParams.toUpperCase()))
          .toList();
      if (searchList.isNotEmpty) {
        showBottomDropdown(context, 'Vessel', 'Select a Vessel', searchList,
            _selectedVesselIndex, _setVessel, true);
      }
    }
  }

  void _setBerth(int index) {
    setState(() {
      _selectBerthController.text = _berthItems[index].text;
      _berthId = _berthItems[index].value;
      _selectedBerthIndex = index;
      //widget.viewModel.setSelectedBerthIndex(index); #TODO
    });
  }

  void _showBerthBottomPicker() {
    if (_berthItems.isNotEmpty) {
      showBottomDropdown(context, 'Berth', 'Select a Berth', _berthItems,
          _selectedBerthIndex, _setBerth, true);
    } else {
      showOkMessage(context, "Cannot select Berth",
          "Account setup is incomplete. Please contact support. ");
    }
  }

  Future<void> _showDateTimePicker() async {
    var inputFormat = DateFormat('yyyy-MM-dd HH:mm');
    TimeOfDay? timeOfDay = await showPlatformTimePicker(
      context: context,
      initialTime: TimeOfDay.fromDateTime(dateTime),
    );

    if (timeOfDay == null) {
      _startDateController.text = inputFormat.format(dateTime);
      setState(() {
        startDate = getDateTimeWithOffset(dateTime);
      });
    } else {
      final converted = dateTime.applied(timeOfDay);
      setState(() {
        dateTime = converted;
        startDate = getDateTimeWithOffset(converted);
      });
      _startDateController.text = inputFormat.format(dateTime);
    }
  }

  Future<void> _showDateTimeCompletePicker() async {
    var inputFormat = DateFormat('yyyy-MM-dd HH:mm');
    TimeOfDay? timeOfDay = await showPlatformTimePicker(
      context: context,
      initialTime: TimeOfDay.fromDateTime(dateTime),
    );

    if (timeOfDay == null) {
      setState(() {
        endDate = getDateTimeWithOffset(dateTime);
      });
      _endDateController.text = inputFormat.format(dateTime);
    } else {
      final converted = dateTime.applied(timeOfDay);
      setState(() {
        dateTime = converted;
        endDate = getDateTimeWithOffset(converted);
      });
      _endDateController.text = inputFormat.format(dateTime);
    }
  }

  void rebuildTableRows() {
    setState(() {
      imageTableRows = <TableRow>[];
    });
    for (var attachedImage in attachedImages) {
      addImageRow(attachedImage);
    }
  }

  void _imagePicker() {
    var dropDownItems = <DropdownItem>[];
    dropDownItems.add(const DropdownItem("Camera", 0, ImageSource.camera));
    dropDownItems.add(const DropdownItem("Gallery", 0, ImageSource.gallery));

    showBottomDropdown(
        context,
        'Image',
        'Where would you like to retrieve the images from?',
        dropDownItems,
        0,
        _processImage,
        false);
  }

  Future<void> _processImage(ImageSource imageSource) async {
    final ImagePicker picker = ImagePicker();
    XFile? image = await picker.pickImage(source: imageSource);
    if (image != null) {
      setImage(image);
    }
  }

  void setImage(XFile? image) {
    if (image != null) {
      final bytes = File(image.path).readAsBytesSync();
      String name = image.name;
      String base64Content = base64Encode(bytes);
      ImageCreate imageCreate = ImageCreate(
        name: name,
        base64Content: base64Content,
        kind: null,
      );

      setState(() {
        attachedImages.add(imageCreate);
        addImageRow(imageCreate);
      });
    }
  }

  void addImageRow(ImageCreate attachedImage) {
    var displayName = imageDisplayName(attachedImage.name);
    setState(() {
      imageTableRows.add(buildTableRow(context, buildTableTitle(displayName),
          buildTableTitle(""), delete(attachedImage)));
    });
  }

  String imageDisplayName(String imageName) {
    return imageName.length > 6
        ? imageName.substring(imageName.length - 5)
        : imageName;
  }

  TextButton delete(ImageCreate imageCreate) {
    return TextButton.icon(
        onPressed: () {
          var displayName = imageDisplayName(imageCreate.name);
          showMessageWithActions(
              context,
              "Remove image #$displayName",
              "Are you sure you want to remove this image?",
              () {
                Navigator.pop(context);
                setState(() {
                  attachedImages.remove(imageCreate);
                });
                rebuildTableRows();
              },
              "Yes",
              () {
                Navigator.pop(context);
              },
              "No");
        },
        icon: const Icon(
          FontAwesomeIcons.remove,
          size: 20,
          color: BrandingColors.cadiGreen,
        ),
        label: const Text(""));
  }

  @override
  Widget build(BuildContext context) {
    final List<SettingTile> allSettings = <SettingTile>[
      SettingTile(
          0,
          'Active Account',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                  title: Text(
                      "Port Event: ${getPortEventRoleDescription(widget.viewModel.portEventRole)}",
                      style: TextStyle(
                          fontSize: PlatformSizing().normalTextSize(context),
                          color: widget.viewModel.universalBranding
                              .getPrimaryColor()))))),
      SettingTile(
          1,
          'Active Account',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                  title: Text("Capture event details below",
                      style: TextStyle(
                          fontSize: PlatformSizing().smallTextSize(context),
                          fontWeight: FontWeight.w300,
                          color: BrandingColors.cadiBlack))))),
      SettingTile(
          2,
          'VESSEL Name',
          Material(
            color: BrandingColors.background,
            child: ListTile(
                title: Text('Vessel Name',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack))),
          )),
      SettingTile(
        3,
        "VESSEL  INPUT",
        PlatformTextField(
          cursorColor: BrandingColors.cadiGrey,
          onChanged: (dataString) {
            if (dataString.isNotEmpty &&
                dataString.length > _vesselNameLength) {
              _showVesselBottomPicker(dataString);
            }
            setState(() {
              _vesselNameLength = dataString.length;
            });
          },
          textCapitalization: TextCapitalization.characters,
          controller: _vesselNameController,
          key: _vesselNameKey,
          autocorrect: false,
          material: (_, __) =>
              MaterialAssetsDark().textFieldData("", FontAwesomeIcons.search),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData('Vessel Name', null),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontWeight: FontWeight.w400,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
          onEditingComplete: () {
            FocusScope.of(context).requestFocus(voyageInputNode);
          },
        ),
      ),
      SettingTile(
          4,
          'VOYAGE',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('Voyage Number',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(
        5,
        "VOYAGE INPUT",
        PlatformTextField(
          cursorColor: BrandingColors.cadiGrey,
          focusNode: voyageInputNode,
          textCapitalization: TextCapitalization.characters,
          controller: _voyageNumberController,
          key: _voyageNumberKey,
          autocorrect: false,
          material: (_, __) =>
              MaterialAssetsDark().textFieldData("", FontAwesomeIcons.search),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData('Voyage Number', null),
          style: TextStyle(
              fontWeight: FontWeight.w400,
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
          // onEditingComplete: () {
          //
          // },
        ),
      ),
    ];

    if (_showBerth) {
      allSettings.add(
        SettingTile(
            6,
            'Berth',
            Material(
                color: BrandingColors.background,
                child: ListTile(
                  title: Text('Berth #',
                      style: TextStyle(
                          fontWeight: FontWeight.w300,
                          fontSize: PlatformSizing().mediumTextSize(context),
                          color: BrandingColors.cadiBlack)),
                ))),
      );

      allSettings.add(SettingTile(
          7,
          "",
          Material(
              color: BrandingColors.cadiWhite,
              child: InkWell(
                onTap: () {
                  _showBerthBottomPicker();
                },
                child: PlatformTextField(
                  cursorColor: BrandingColors.cadiGrey,
                  textCapitalization: TextCapitalization.characters,
                  controller: _selectBerthController,
                  key: _selectBerthKey,
                  autocorrect: false,
                  enabled: false,
                  material: (_, __) => MaterialAssetsDark()
                      .textFieldData("Berth", FontAwesomeIcons.chevronDown),
                  cupertino: (_, __) => CupertinoAssets()
                      .textFieldData('Berth', FontAwesomeIcons.chevronDown),
                  style: TextStyle(
                      color: BrandingColors.cadiBlack,
                      fontWeight: FontWeight.w400,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  keyboardType: TextInputType.text,
                ),
              ))));
    }

    allSettings.add(SettingTile(
        8,
        'DATE',
        Material(
            color: BrandingColors.background,
            child: ListTile(
              title: Text(_showEventCompleteAt ? 'Start Time' : 'Date',
                  style: TextStyle(
                      fontSize: PlatformSizing().mediumTextSize(context),
                      fontWeight: FontWeight.w300,
                      color: BrandingColors.cadiBlack)),
            ))));

    allSettings.add(SettingTile(
        9,
        "",
        Material(
            color: BrandingColors.background,
            child: InkWell(
              onTap: () async {
                _showDateTimePicker();
              },
              child: PlatformTextField(
                cursorColor: BrandingColors.cadiGrey,
                textCapitalization: TextCapitalization.characters,
                controller: _startDateController,
                key: _endDateKey,
                autocorrect: false,
                enabled: false,
                material: (_, __) => MaterialAssetsDark()
                    .textFieldData("", FontAwesomeIcons.calendar),
                cupertino: (_, __) => CupertinoAssets()
                    .textFieldData('Date', FontAwesomeIcons.calendar),
                style: TextStyle(
                    color: BrandingColors.cadiBlack,
                    fontWeight: FontWeight.w400,
                    fontSize: PlatformSizing().inputTextFieldSize(context)),
                keyboardType: TextInputType.text,
              ),
            ))));

    if (_showEventCompleteAt) {
      allSettings.add(SettingTile(
          10,
          'COMPLETE',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('End Time (Estimate)',
                    style: TextStyle(
                        fontSize: PlatformSizing().mediumTextSize(context),
                        fontWeight: FontWeight.w300,
                        color: BrandingColors.cadiBlack)),
              ))));

      allSettings.add(SettingTile(
          11,
          "",
          Material(
              color: BrandingColors.background,
              child: InkWell(
                onTap: () async {
                  _showDateTimeCompletePicker();
                },
                child: PlatformTextField(
                  cursorColor: BrandingColors.cadiGrey,
                  textCapitalization: TextCapitalization.characters,
                  controller: _endDateController,
                  key: _startDateKey,
                  autocorrect: false,
                  enabled: false,
                  material: (_, __) => MaterialAssetsDark()
                      .textFieldData("Complete", FontAwesomeIcons.calendar),
                  cupertino: (_, __) => CupertinoAssets()
                      .textFieldData('Complete', FontAwesomeIcons.calendar),
                  style: TextStyle(
                      color: BrandingColors.cadiBlack,
                      fontWeight: FontWeight.w400,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  keyboardType: TextInputType.text,
                ),
              ))));
    }

    if (_commentRequired) {
      allSettings.add(SettingTile(
          12,
          'COMMENTS',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('Enter additional comments:',
                    style: TextStyle(
                        fontSize: PlatformSizing().smallTextSize(context),
                        fontWeight: FontWeight.w300,
                        color: BrandingColors.cadiBlack)),
              ))));

      allSettings.add(SettingTile(
          13,
          'comment',
          Padding(
              padding:
                  const EdgeInsets.only(top: 0, bottom: 20, left: 5, right: 5),
              child: PlatformTextField(
                cursorColor: BrandingColors.cadiGrey,
                controller: _commentController,
                key: _searchCommentKey,
                maxLines: 5,
                hintText: "* Enter additional comments",
                material: (_, __) => CommentBox().textFieldData(''),
                cupertino: (_, __) =>
                    CupertinoAssets().textFieldData('Add Comments', null),
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    color: BrandingColors.cadiBlack,
                    fontSize: PlatformSizing().inputTextFieldSize(context)),
                keyboardType: TextInputType.text,
              ))));
    }

    allSettings.add(SettingTile(
      14,
      '',
      Padding(
          padding: const EdgeInsets.only(top: 10),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                BaseGhostButton(
                  onPressed: () {
                    _imagePicker();
                  },
                  textColor: BrandingColors.cadiBlack,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Icon(
                        FontAwesomeIcons.paperclip,
                        size: 20,
                        color: BrandingColors.cadiBlack,
                      ),
                      const SizedBox(width: 8),
                      Text(
                        'Add Photos',
                        style: TextStyle(
                          color: BrandingColors
                              .cadiBlack, // Set your desired text color here
                          fontSize: PlatformSizing().mediumTextSize(context),
                        ),
                      ),
                    ],
                  ), // Pass the text color here
                ),
                if (attachedImages.isNotEmpty)
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      '${attachedImages.length} Image(s) Attached',
                      style: TextStyle(
                        fontSize: PlatformSizing().smallTextSize(context),
                        color: BrandingColors.cadiBlackOpacity,
                        fontStyle: FontStyle.italic,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                if (attachedImages.isNotEmpty)
                  Material(
                      color: BrandingColors.background,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20, left: 10),
                        child: buildScrollableDynamicTable(
                            buildSummaryTable(context, imageTableRows)),
                      ))
              ],
            ),
          )),
    ));

    allSettings.add(SettingTile(
        15,
        '',
        Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 50.0, vertical: 60.0),
            child: BaseButton(
                color:
                    widget.viewModel.universalBranding.getButtonPrimaryColor(),
                child: Text('Next',
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: PlatformSizing().normalTextSize(context))),
                onPressed: () => {
                      if (_vesselNameController.text.trim().isEmpty)
                        {
                          viewModel.showErrorAlert(
                              context, "Please input a vessel name.")
                        }
                      else if (_voyageNumberController.text.trim().isEmpty)
                        {
                          viewModel.showErrorAlert(
                              context, "Please input a voyage number.")
                        }
                      else if (_commentRequired &&
                          _commentController.text.trim() == "")
                        {
                          viewModel.showErrorAlert(context,
                              "Comment is required. Please tell us about the event.")
                        }
                      else
                        {
                          widget.viewModel.onNextSelected(
                              context,
                              _vesselNameController.text,
                              _voyageNumberController.text,
                              _selectedBerthIndex,
                              _berthId,
                              startDate,
                              endDate,
                              _commentController.text,
                              attachedImages)
                        }
                    }))));

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          //itemBuilder: itemBuilder) .separated(
          itemCount: allSettings.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          // separatorBuilder: (BuildContext context, int index) =>
          //     Divider(height: 1),
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
  }
}
