import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/image/image_create.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'port_event_capture_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

class PortEventCaptureBuilder extends StatelessWidget {
  const PortEventCaptureBuilder({super.key});

  static const String route = route_helper.portEventCaptureBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(
                  // FontAwesomeIcons.warning,
                  context,
                  "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, PortEventCaptureVM>(
        converter: PortEventCaptureVM.fromStore,
        builder: (context, vm) {
          return PortEventCapturePage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class PortEventCaptureVM {
  bool isLoading;
  AuthState authState;
  Account account;
  String portEventRole;
  UiPortEventCapture uiPortEventCapture;
  UniversalBranding universalBranding;
  List<AccountVessel> accountVessels;
  List<AccountBerth> accountBerths;
  AccountPortDepot accountPortDepot;

  final Function() stopLoading;
  final Function(BuildContext context, String message) showErrorAlert;
  final Function(BuildContext context, String vesselName, String voyageNumber,
      int berthIndex,int? berthId, String startDate, String endDate, String comment, List<ImageCreate>? attachedImages) onNextSelected;
  PortEventCaptureVM({
    required this.isLoading,
    required this.authState,
    required this.account,
    required this.portEventRole,
    required this.uiPortEventCapture,
    required this.universalBranding,
    required this.accountVessels,
    required this.accountBerths,
    required this.accountPortDepot,
    required this.stopLoading,
    required this.showErrorAlert,
    required this.onNextSelected,
  });

  static PortEventCaptureVM fromStore(Store<AppState> store) {
    return PortEventCaptureVM(
      isLoading: store.state.isLoading,
      authState: store.state.authState,
      account: store.state.accountState.account,
      portEventRole: store.state.uiState.portEventRole,
      uiPortEventCapture: store.state.uiState.uiPortEventCapture,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      accountVessels: store.state.accountState.filteredAccountVessels,
      accountBerths: store.state.accountState.filteredAccountBerths,
      accountPortDepot: store.state.accountState.accountPortDepots
          .firstWhere(
              (element) =>
          element.id == store.state.portEventState.portEventCreate.accountPortLocationId,
          orElse: () => AccountPortDepot.initial()),
      stopLoading: () {
        store.dispatch(StopLoading());
      },
      showErrorAlert: (BuildContext context, String message) {
        showOkMessage(
            // FontAwesomeIcons.warning,
            context,
            "Submit Error",
            message);
      },
      onNextSelected: (BuildContext context, String vesselName, String voyageNumber,
          int berthIndex, int? berthId, String startDate, String endDate, String comment, List<ImageCreate>? attachedImages) {
        store.dispatch(UiPortEventCaptureSetVariables(vesselName, voyageNumber.replaceAll(' ', ''), berthIndex));
        store.dispatch(SetPortEventCaptureVariables(vesselName, voyageNumber, berthId, comment, startDate, endDate, attachedImages));
        
        if (store.state.uiState.portEventRole ==  CadiPortEventRole.refuel) {
          store.dispatch(NavigateToAction.push(
              route_helper.portEventRefuelBuilder));
        } else {
          store.dispatch(NavigateToAction.push(
              route_helper.portEventSummaryScreenBuilder));
        }

      },
    );
  }
}
