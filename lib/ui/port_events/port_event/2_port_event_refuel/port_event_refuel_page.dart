import 'package:cadi_core_app/factory/ui_factory.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';
import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'port_event_refuel_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

class PortEventRefuelPage extends StatefulWidget {
  final PortEventRefuelVM viewModel;
  const PortEventRefuelPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<PortEventRefuelPage> createState() {
    return PortEventRefuelState(viewModel: viewModel);
  }
}

class PortEventRefuelState extends State<PortEventRefuelPage> {
  final PortEventRefuelVM viewModel;

  final _selectFuelTypeController = TextEditingController();
  final _inputVolumeController = TextEditingController();
  final _selectVolumeUnitController = TextEditingController();
  final _selectCurrencyController = TextEditingController();
  final _inputPriceController = TextEditingController();
  final _inputDistanceTravelledController = TextEditingController();
  final _selectDistanceTravelledUnitController = TextEditingController();

  static const Key _selectFuelTypeKey = Key(CargoEventKeys.selectFuelTypeKey);
  static const Key _inputVolumeKey = Key(CargoEventKeys.inputVolumeKey);
  static const Key _selectVolumeUnitKey =
      Key(CargoEventKeys.selectVolumeUnitKey);
  static const Key _selectCurrencyKey = Key(CargoEventKeys.selectCurrencyKey);
  static const Key _inputPriceKey = Key(CargoEventKeys.inputPriceKey);
  static const Key _inputDistanceTravelledKey =
      Key(CargoEventKeys.inputDistanceTravelledKey);
  static const Key _selectDistanceTravelledKey =
      Key(CargoEventKeys.selectDistanceTravelledUnityKey);

  int _selectedFuelTypeIndex = 0;
  int _selectedVolumeUnitIndex = 0;
  int _selectedCurrencyIndex = 0;
  int _selectedDistanceTravelledUnitIndex = 0;

  List<DropdownItem> _fuelTypeItems = [];
  List<DropdownItem> _volumeUnitItems = [];
  List<DropdownItem> _currencyItems = [];
  List<DropdownItem> _distanceTravelledUnitItems = [];

  void _setFuelType(int index) {
    setState(() {
      _selectFuelTypeController.text = _fuelTypeItems[index].text;
      _selectedFuelTypeIndex = index;
    });
  }

  void _setVolumeUnit(int index) {
    setState(() {
      _selectVolumeUnitController.text = _volumeUnitItems[index].text;
      _selectedVolumeUnitIndex = index;
    });
  }

  void _setCurrency(int index) {
    setState(() {
      _selectCurrencyController.text = _currencyItems[index].text;
      _selectedCurrencyIndex = index;
    });
  }

  void _setDistanceTravelledUnit(int index) {
    setState(() {
      _selectDistanceTravelledUnitController.text =
          _distanceTravelledUnitItems[index].text;
      _selectedDistanceTravelledUnitIndex = index;
    });
  }

  PortEventRefuelState({
    Key? key,
    required this.viewModel,
  });

  @override
  initState() {
    super.initState();

    var uiFactory = UiFactory();

    _currencyItems = uiFactory.convertToDropdown<CadiEnum, String>(
        widget.viewModel.currencies,
        (item) => item.description,
        (item) => item.id);

    if (_currencyItems.length > 5) {
      _selectedCurrencyIndex = 4;
      _setCurrency(_selectedCurrencyIndex);
    }

    _volumeUnitItems = uiFactory.convertToDropdown<CadiEnum, String>(
        widget.viewModel.volumeUnits,
        (item) => item.description,
        (item) => item.id);

    _setVolumeUnit(_selectedVolumeUnitIndex);

    _distanceTravelledUnitItems = uiFactory.convertToDropdown<CadiEnum, String>(
        widget.viewModel.distanceTravelledUnits,
        (item) => item.description,
        (item) => item.id);

    _setDistanceTravelledUnit(_selectedDistanceTravelledUnitIndex);

    _fuelTypeItems = uiFactory.convertToDropdown<FuelType, int>(
        widget.viewModel.fuelTypes,
        (item) => item.description,
        (item) => item.id);

    _setFuelType(_selectedFuelTypeIndex);
  }

  @override
  void dispose() {
    _selectFuelTypeController.dispose();
    _inputVolumeController.dispose();
    _selectVolumeUnitController.dispose();
    _selectCurrencyController.dispose();
    _inputPriceController.dispose();
    _inputDistanceTravelledController.dispose();
    _selectDistanceTravelledUnitController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final List<SettingTile> allSettings = <SettingTile>[
      SettingTile(
          0,
          'Active Account',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                  title: Text("Port Event: ${widget.viewModel.portEventRole}",
                      style: TextStyle(
                          fontSize: PlatformSizing().normalTextSize(context),
                          color: BrandingColors.cadiBlack))))),
      SettingTile(
          1,
          'Active Account',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                  title: Text("Please fill the refuel event details below.",
                      style: TextStyle(
                          fontSize: PlatformSizing().smallTextSize(context),
                          fontWeight: FontWeight.w300,
                          color: BrandingColors.cadiBlack))))),
      SettingTile(
          2,
          'Fuel Type',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('Fuel Type',
                    style: TextStyle(
                        fontSize: PlatformSizing().mediumTextSize(context),
                        fontWeight: FontWeight.w300,
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(
        3,
        "SELECT FUEL TYPE",
        Material(
            color: BrandingColors.background,
            child: InkWell(
                onTap: () {
                  showBottomDropdown(
                      context,
                      'Fuel Type',
                      'Please select a Fuel Type',
                      _fuelTypeItems,
                      _selectedFuelTypeIndex,
                      _setFuelType,
                      true);
                },
                child: PlatformTextField(
                  cursorColor: BrandingColors.cadiGrey,
                  textCapitalization: TextCapitalization.characters,
                  controller: _selectFuelTypeController,
                  key: _selectFuelTypeKey,
                  enabled: false,
                  autocorrect: false,
                  material: (_, __) => MaterialAssetsDark()
                      .textFieldData("Fuel Type", FontAwesomeIcons.chevronDown),
                  cupertino: (_, __) => CupertinoAssets()
                      .textFieldData('Fuel Type', FontAwesomeIcons.chevronDown),
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: BrandingColors.cadiBlack,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  keyboardType: TextInputType.text,
                ))),
      ),
      SettingTile(
          4,
          'REFUEL VOLUME',
          Material(
            color: BrandingColors.background,
            child: ListTile(
                title: Text('Fuel Volume',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack))),
          )),
      SettingTile(
        5,
        "FUEL VOLUME INPUT",
        PlatformTextField(
            cursorColor: BrandingColors.cadiGrey,
            textCapitalization: TextCapitalization.characters,
            controller: _inputVolumeController,
            key: _inputVolumeKey,
            autocorrect: false,
            material: (_, __) => MaterialAssetsDark()
                .textFieldData("Refuel Volume", FontAwesomeIcons.weightScale),
            cupertino: (_, __) =>
                CupertinoAssets().textFieldData('Refuel Volume', null),
            style: TextStyle(
                color: BrandingColors.cadiBlack,
                fontWeight: FontWeight.w400,
                fontSize: PlatformSizing().inputTextFieldSize(context)),
            inputFormatters: [
              FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}'))
            ],
            keyboardType: const TextInputType.numberWithOptions(decimal: true)),
      ),
      SettingTile(
          6,
          'VOLUME UNIT',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('Volume Unit',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(
        7,
        "VOLUME UNIT SELECT",
        Material(
            color: BrandingColors.background,
            child: InkWell(
                onTap: () {
                  showBottomDropdown(
                      context,
                      'Volume Unit',
                      'Please select a Unit',
                      _volumeUnitItems,
                      _selectedVolumeUnitIndex,
                      _setVolumeUnit,
                      true);
                },
                child: PlatformTextField(
                  cursorColor: BrandingColors.cadiGrey,
                  textCapitalization: TextCapitalization.characters,
                  controller: _selectVolumeUnitController,
                  key: _selectVolumeUnitKey,
                  enabled: false,
                  autocorrect: false,
                  material: (_, __) => MaterialAssetsDark().textFieldData(
                      "Volume Unit", FontAwesomeIcons.chevronDown),
                  cupertino: (_, __) => CupertinoAssets().textFieldData(
                      'Volume Unit', FontAwesomeIcons.chevronDown),
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: BrandingColors.cadiBlack,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  keyboardType: TextInputType.text,
                ))),
      ),
      SettingTile(
          8,
          'PRICE',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('Price',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(
        9,
        "PRICE INPUT",
        PlatformTextField(
          cursorColor: BrandingColors.cadiGrey,
          textCapitalization: TextCapitalization.characters,
          controller: _inputPriceController,
          key: _inputPriceKey,
          autocorrect: false,
          material: (_, __) => MaterialAssetsDark()
              .textFieldData("", FontAwesomeIcons.dollarSign),
          cupertino: (_, __) => CupertinoAssets().textFieldData('Price', null),
          style: TextStyle(
              fontWeight: FontWeight.w400,
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          inputFormatters: [
            FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}'))
          ],
          keyboardType: const TextInputType.numberWithOptions(decimal: true),
        ),
      ),
      SettingTile(
          10,
          'CURRENCY',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('Currency',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(
        11,
        "CURRENCY SELECT",
        Material(
            color: BrandingColors.background,
            child: InkWell(
                onTap: () {
                  showBottomDropdown(
                      context,
                      'Currency',
                      'Please select a Currency',
                      _currencyItems,
                      _selectedCurrencyIndex,
                      _setCurrency,
                      true);
                },
                child: PlatformTextField(
                  cursorColor: BrandingColors.cadiGrey,
                  textCapitalization: TextCapitalization.characters,
                  controller: _selectCurrencyController,
                  key: _selectCurrencyKey,
                  enabled: false,
                  autocorrect: false,
                  material: (_, __) => MaterialAssetsDark()
                      .textFieldData("", FontAwesomeIcons.chevronDown),
                  cupertino: (_, __) => CupertinoAssets()
                      .textFieldData('Currency', FontAwesomeIcons.chevronDown),
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: BrandingColors.cadiBlack,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  keyboardType: TextInputType.text,
                ))),
      ),
      SettingTile(
          12,
          'Distance Travelled',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('Distance Travelled',
                    style: TextStyle(
                        fontSize: PlatformSizing().mediumTextSize(context),
                        fontWeight: FontWeight.w300,
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(
        13,
        "Distance Travelled Input",
        PlatformTextField(
          cursorColor: BrandingColors.cadiGrey,
          textCapitalization: TextCapitalization.characters,
          controller: _inputDistanceTravelledController,
          key: _inputDistanceTravelledKey,
          autocorrect: false,
          material: (_, __) => MaterialAssetsDark().textFieldData(
              "Distance Travelled", FontAwesomeIcons.gaugeSimple),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData('Distance Travelled', null),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontWeight: FontWeight.w400,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          inputFormatters: [
            FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}'))
          ],
          keyboardType: const TextInputType.numberWithOptions(decimal: true),
        ),
      ),
      SettingTile(
          14,
          'DISTANCE TRAVELLED UNIT',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('Distance Travelled Unit',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(
        15,
        "DISTANCE TRAVELLED SELECT",
        Material(
            color: BrandingColors.background,
            child: InkWell(
                onTap: () {
                  showBottomDropdown(
                      context,
                      'Distance Travelled Unit',
                      'Please select a Unit',
                      _distanceTravelledUnitItems,
                      _selectedDistanceTravelledUnitIndex,
                      _setDistanceTravelledUnit,
                      true);
                },
                child: PlatformTextField(
                  cursorColor: BrandingColors.cadiGrey,
                  textCapitalization: TextCapitalization.characters,
                  controller: _selectDistanceTravelledUnitController,
                  key: _selectDistanceTravelledKey,
                  enabled: false,
                  autocorrect: false,
                  material: (_, __) => MaterialAssetsDark()
                      .textFieldData("", FontAwesomeIcons.chevronDown),
                  cupertino: (_, __) => CupertinoAssets()
                      .textFieldData('Currency', FontAwesomeIcons.chevronDown),
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: BrandingColors.cadiBlack,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  keyboardType: TextInputType.text,
                ))),
      ),
      SettingTile(
          16,
          '',
          Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 50.0, vertical: 60.0),
              child: BaseButton(
                  color: widget.viewModel.universalBranding
                      .getButtonPrimaryColor(),
                  child: Text('Next',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: PlatformSizing().normalTextSize(context))),
                  onPressed: () {
                    if (_inputPriceController.text.isEmpty) {
                      showOkMessage(
                          context, "Input error", "Please input a price");
                    } else if (_inputVolumeController.text.isEmpty) {
                      showOkMessage(context, "Input error",
                          "Please input the refuel volume");
                    } else {
                      widget.viewModel.onNextSelected(
                          context,
                          _fuelTypeItems[_selectedFuelTypeIndex].value,
                          _inputVolumeController.text,
                          _volumeUnitItems[_selectedVolumeUnitIndex].value,
                          _inputPriceController.text,
                          _currencyItems[_selectedCurrencyIndex].value,
                          _inputDistanceTravelledController.text,
                          _distanceTravelledUnitItems[
                                  _selectedDistanceTravelledUnitIndex]
                              .value);
                    }
                  }))),
    ];

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          itemCount: allSettings.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
  }
}
