import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'port_event_refuel_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

class PortEventRefuelBuilder extends StatelessWidget {
  const PortEventRefuelBuilder({super.key});

  static const String route = route_helper.portEventRefuelBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(
                  context,
                  "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, PortEventRefuelVM>(
        converter: PortEventRefuelVM.fromStore,
        builder: (context, vm) {
          return PortEventRefuelPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class PortEventRefuelVM {
  bool isLoading;
  AuthState authState;
  Account account;
  String portEventRole;
  List<CadiEnum> currencies;
  List<CadiEnum> volumeUnits;
  List<CadiEnum> distanceTravelledUnits;
  List<FuelType> fuelTypes;

  UniversalBranding universalBranding;

  final Function(BuildContext context, String message) showErrorAlert;
  final Function(
      BuildContext context,
      int fuelType,
      String volume,
      String volumeUnit,
      String price,
      String currency,
      String distanceTravelled,
      String distanceTravelledUnit) onNextSelected;

  PortEventRefuelVM({
    required this.isLoading,
    required this.authState,
    required this.account,
    required this.portEventRole,
    required this.currencies,
    required this.volumeUnits,
    required this.distanceTravelledUnits,
    required this.fuelTypes,
    required this.showErrorAlert,
    required this.onNextSelected,
    required this.universalBranding,
  });

  static PortEventRefuelVM fromStore(Store<AppState> store) {
    return PortEventRefuelVM(
      isLoading: store.state.isLoading,
      authState: store.state.authState,
      account: store.state.accountState.account,
      portEventRole: store.state.uiState.portEventRole,
      currencies: store.state.referenceState.currencies,
      volumeUnits: store.state.referenceState.volumeUnits,
      distanceTravelledUnits: store.state.referenceState.distanceTravelledUnits,
      fuelTypes: store.state.referenceState.fuelTypes,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      showErrorAlert: (BuildContext context, String message) {
        showOkMessage(
            // FontAwesomeIcons.warning,
            context,
            "Submit Error",
            message);
      },
      onNextSelected: (BuildContext context,
          int fuelType,
          String volume,
          String volumeUnit,
          String price,
          String currency,
          String distanceTravelled,
          String distanceTravelledUnit) {
            num numVolume = num.tryParse(volume) ?? 0.00;
            num numPrice = num.tryParse(price) ?? 0.00;
            num numDistanceTravelled = num.tryParse(distanceTravelled) ?? 0.00;

            store.dispatch(SetPortEventRefuelVariables(
                fuelType,
                numVolume,
                volumeUnit,
                numPrice,
                currency,
                numDistanceTravelled,
                distanceTravelledUnit));

            store.dispatch(NavigateToAction.push(
                route_helper.portEventSummaryScreenBuilder));
      },
    );
  }
}
