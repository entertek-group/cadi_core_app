import 'dart:core';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/time_picker.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'port_event_summary_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/setting_tile.dart';

class PortEventSummaryPage extends StatefulWidget {
  final PortEventSummaryVM viewModel;
  const PortEventSummaryPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<PortEventSummaryPage> createState() {
    return PortEventSummaryPageState(viewModel: viewModel);
  }
}

class PortEventSummaryPageState extends State<PortEventSummaryPage> {
  final PortEventSummaryVM viewModel;
  String movementStatus = "";
  String movementType = "";

  PortEventSummaryPageState({
    Key? key,
    required this.viewModel,
  });

  var dateTime = DateFormat.yMMMEd().format(DateTime.now()).toString();
  var time = DateTime.now().toString().substring(10, 20);

  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }
  // ignore: non_constant_identifier_names

  @override
  Widget build(BuildContext context) {
    //Build List of Items here.
    List<Widget> cargoSummary = <Widget>[];
    var inputFormat = DateFormat('yyyy-MM-dd HH:mm');

    //Convert eventAt to local time
    DateTime eventAtUTC =
        DateTime.parse(widget.viewModel.portEventCreate.eventAt);
    DateTime localEventAt = eventAtUTC.toLocal();
    String finalEventAt = inputFormat.format(localEventAt);

    //Convert eventCompleteAt to local time
    String finalEventCompletedAt = "";
    if (widget.viewModel.portEventCreate.eventCompleteAt != null) {
      DateTime eventCompletedAtUTC =
          DateTime.parse(widget.viewModel.portEventCreate.eventCompleteAt!);
      DateTime localEventCompletedAt = eventCompletedAtUTC.toLocal();
      finalEventCompletedAt = inputFormat.format(localEventCompletedAt);
    } else {
      finalEventCompletedAt = "";
    }

    if (widget.viewModel.portEventRole == CadiPortEventRole.refuel) {
      cargoSummary.add(buildSummaryRow(context, "Vessel",
          widget.viewModel.portEventCreate.vesselName, false, null));
      cargoSummary.add(buildSummaryRow(context, "Voyage #",
          widget.viewModel.portEventCreate.voyageNumber, false, null));
      cargoSummary.add(buildSummaryRow(context, "Carrier",
          widget.viewModel.accountCarrier.carrierName, false, null));
      cargoSummary.add(buildSummaryRow(
          context, "Role", widget.viewModel.portEventCreate.role, false, null));

      cargoSummary.add(buildSummaryRow(context, "", "", false, null));
      var refuelData = widget.viewModel.portEventCreate.refuelData;
      if (refuelData != null) {
        cargoSummary.add(buildSummaryRow(context, "Fuel Type",
            widget.viewModel.fuelType.description, false, null));
        cargoSummary.add(buildSummaryRow(context, "Volume",
            "${refuelData.volume} ${refuelData.volumeUnit}", false, null));
        cargoSummary.add(buildSummaryRow(context, "Price",
            "${refuelData.price} ${refuelData.priceCurrency}", false, null));
        cargoSummary.add(buildSummaryRow(
            context,
            "Distance",
            "${refuelData.distanceTravelled} ${refuelData.distanceTravelledUnit}",
            false,
            null));
        cargoSummary.add(buildSummaryRow(context, "", "", false, null));
      }

      cargoSummary.add(buildSummaryRow(context, "Port",
          widget.viewModel.accountPortDepot.unLocode, false, null));
      cargoSummary.add(buildSummaryRow(context, "Location",
          widget.viewModel.accountPortDepot.depotName, false, null));
      cargoSummary.add(buildSummaryRow(context, "Carrier",
          widget.viewModel.accountCarrier.carrierName, false, null));

      cargoSummary
          .add(buildSummaryRow(context, "Date", finalEventAt, false, null));
      cargoSummary.add(buildSummaryRow(
          context, "Profile", widget.viewModel.account.name, false, null));
    } else {
      cargoSummary.add(buildSummaryRow(context, "Vessel",
          widget.viewModel.portEventCreate.vesselName, false, null));
      cargoSummary.add(buildSummaryRow(context, "Voyage #",
          widget.viewModel.portEventCreate.voyageNumber, false, null));
      cargoSummary.add(buildSummaryRow(context, "Carrier",
          widget.viewModel.accountCarrier.carrierName, false, null));
      cargoSummary.add(buildSummaryRow(
          context, "Role", widget.viewModel.portEventCreate.role, false, null));

      cargoSummary.add(buildSummaryRow(context, "", "", false, null));

      cargoSummary.add(buildSummaryRow(context, "Port",
          widget.viewModel.accountPortDepot.unLocode, false, null));
      cargoSummary.add(buildSummaryRow(context, "Location",
          widget.viewModel.accountPortDepot.depotName, false, null));
      cargoSummary.add(buildSummaryRow(
          context,
          (widget.viewModel.portEventRole == CadiPortEventRole.delay ||
                  widget.viewModel.portEventRole ==
                      CadiPortEventRole.hatchLids ||
                  widget.viewModel.portEventRole == CadiPortEventRole.lashing ||
                  widget.viewModel.portEventRole == CadiPortEventRole.unLashing)
              ? "Start Time"
              : "Date",
          finalEventAt,
          true,
          () => _showDateTimePicker(
              true, TimeOfDay.fromDateTime(DateTime.parse(finalEventAt)))));

      if ((widget.viewModel.portEventRole == CadiPortEventRole.delay ||
              widget.viewModel.portEventRole == CadiPortEventRole.unLashing ||
              widget.viewModel.portEventRole == CadiPortEventRole.lashing ||
              widget.viewModel.portEventRole == CadiPortEventRole.hatchLids) &&
          widget.viewModel.portEventCreate.eventCompleteAt != null) {
        cargoSummary.add(buildSummaryRow(
            context,
            "End Time",
            finalEventCompletedAt,
            true,
            () => _showDateTimePicker(
                false,
                TimeOfDay.fromDateTime(
                    DateTime.parse(finalEventCompletedAt)))));
      }

      if (widget.viewModel.portEventCreate.images != null) {
        cargoSummary.add(buildSummaryRow(
            context,
            "Images",
            '${widget.viewModel.portEventCreate.images!.length} attached',
            false,
            null));
      }

      cargoSummary.add(buildSummaryRow(context, "", "", false, null));

      cargoSummary.add(buildSummaryRow(context, "Comment",
          widget.viewModel.portEventCreate.comment, false, null));

      cargoSummary.add(buildSummaryRow(
          context, "Date", widget.viewModel.dateTime, false, null));

      cargoSummary.add(
          buildSummaryRow(context, "Time", widget.viewModel.time, false, null));
      cargoSummary.add(buildSummaryRow(
          context, "Profile", widget.viewModel.account.name, false, null));
    }

    List<SettingTile> allOptions = <SettingTile>[
      SettingTile(
          0,
          'DISPLAY TYPE',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                  title: Text(
                      "Cargo Event: ${getPortEventRoleDescription(widget.viewModel.portEventRole)}",
                      style: TextStyle(
                          fontSize: PlatformSizing().normalTextSize(context),
                          color: BrandingColors.cadiBlack))))),
      SettingTile(
          1,
          'DISPLAY STATUS',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                  title: Text("Summary: Check, Update and Submit",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: PlatformSizing().smallTextSize(context),
                          color: BrandingColors.cadiBlack))))),
      SettingTile(2, 'Summary', buildSummaryCard(context, cargoSummary)),
    ];

    allOptions.add(SettingTile(
        7,
        '',
        Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 50.0, vertical: 10.0),
            child: BaseButton(
              color: widget.viewModel.universalBranding.getButtonPrimaryColor(),
              child: Text('Submit',
                  style: TextStyle(
                      fontSize: PlatformSizing().normalTextSize(context))),
              onPressed: () => widget.viewModel.confirmPortEvent(context),
            ))));

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          physics: const ClampingScrollPhysics(),
          itemCount: allOptions.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          // separatorBuilder: (BuildContext context, int index) =>
          //     const Divider(height: 1),
          itemBuilder: (BuildContext context, int index) {
            return allOptions[index].body;
          }),
    );
    // );
    //return new SafeArea(child: Text("Setting Index placeholder"));
  }

  void _showDateTimePicker(bool isEventAt, TimeOfDay initialTime) {
    final today = DateTime.now();
    showPlatformTimePicker(
      context: context,
      initialTime: initialTime,
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: const ColorScheme.light(
              primary: BrandingColors.cadiGreen,
              onPrimary: Colors.white,
              surface: BrandingColors.cadiWhite,
              onSurface: BrandingColors.cadiBlack,
            ),
            dialogBackgroundColor: Colors.white,
          ),
          child: child!,
        );
      },
    ).then((time) {
      if (time != null) {
        var selectedDateTime = DateTime(
          today.year,
          today.month,
          today.day,
          time.hour,
          time.minute,
          today.second,
          today.millisecond,
        );

        final sevenDaysAgo = today.subtract(const Duration(days: 7));

        if (selectedDateTime.isBefore(sevenDaysAgo)) {
          selectedDateTime = sevenDaysAgo;
        }

        if (selectedDateTime.isAfter(today)) {
          selectedDateTime = today;
        }

        final offset = selectedDateTime.timeZoneOffset;
        final offsetSign = offset.isNegative ? '-' : '+';
        final offsetHours = offset.inHours.abs().toString().padLeft(2, '0');
        final offsetMinutes =
            (offset.inMinutes % 60).abs().toString().padLeft(2, '0');

        final formattedDateTime =
            "${selectedDateTime.toIso8601String().split('Z')[0]}$offsetSign$offsetHours:$offsetMinutes";

        if (isEventAt) {
          widget.viewModel.updateEventAt(formattedDateTime);
        } else {
          widget.viewModel.updateEventCompleteAt(formattedDateTime);
        }
        setState(() {
          dateTime = DateFormat.yMMMEd().format(selectedDateTime).toString();
          this.time = DateFormat('HH:mm:ss').format(selectedDateTime);
        });
      }
    });
  }
}

Widget buildSummaryCard(BuildContext context, List<Widget> cargoSummary) {
  return Card(
      elevation: 2.0,
      color: Colors.white,
      shape: RoundedRectangleBorder(
        side: const BorderSide(color: BrandingColors.cadiGrey, width: 0.5),
        borderRadius: BorderRadius.circular(5),
      ),
      margin: const EdgeInsets.only(left: 5, right: 5, bottom: 20),
      child: ListView.builder(
          physics: const ClampingScrollPhysics(),
          shrinkWrap: true,
          padding: const EdgeInsets.all(15),
          //.separated(
          itemCount: cargoSummary.length,
          // separatorBuilder: (BuildContext context, int index) =>
          //     const Divider(height: 1),
          itemBuilder: (BuildContext context, int index) {
            return cargoSummary[index];
          })

      // Column(mainAxisSize: MainAxisSize.min, children: cargoSummary)
      );
}

Widget buildSummaryRow(BuildContext context, String textDisplay,
    String textValue, bool hasAction, Function? buttonAction) {
  return ListTile(
      dense: true,
      visualDensity: const VisualDensity(horizontal: 0, vertical: -2),
      minVerticalPadding: 4,
      contentPadding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 2),
      tileColor: Colors.transparent,
      leading: Text(
        textDisplay,
        style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: PlatformSizing().smallTextSize(context),
            color: BrandingColors.cadiBlack),
      ),
      title: Text(
        textValue,
        style: TextStyle(
            fontSize: PlatformSizing().smallTextSize(context),
            color: BrandingColors.cadiBlack),
        textAlign: TextAlign.end,
      ),
      trailing: hasAction
          ? IconButton(
              padding: const EdgeInsets.only(bottom: 10, left: 25),
              icon: const Icon(FontAwesomeIcons.penToSquare,
                  color: BrandingColors.cadiBlack),
              onPressed: buttonAction != null ? () => buttonAction() : () {})
          : const IconButton(
              icon: Icon(Icons.edit_outlined, color: Colors.transparent),
              onPressed: null));
}
