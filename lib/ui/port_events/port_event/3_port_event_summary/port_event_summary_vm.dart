import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/port_event/port_event_create.dart';
import 'package:cadi_core_app/services/data_processing_service.dart';
import 'package:cadi_core_app/theme/theme.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'port_event_summary_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

class PortEventSummaryScreenBuilder extends StatelessWidget {
  const PortEventSummaryScreenBuilder({super.key});

  static const String route = route_helper.portEventSummaryScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(context, "Action Blocked",
                  "You cannot change account while performing this action on summary screen.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, PortEventSummaryVM>(
        converter: PortEventSummaryVM.fromStore,
        builder: (context, vm) {
          return PortEventSummaryPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class PortEventSummaryVM {
  bool isLoading;
  AuthState authState;
  Account account;
  AccountPortDepot accountPortDepot;
  String portEventRole;
  PortEventCreate portEventCreate;
  UniversalBranding universalBranding;
  AccountCarrier accountCarrier;
  FuelType fuelType;

  final Function(BuildContext context) confirmPortEvent;
  final Function(String eventAt) updateEventAt;
  final Function(String? eventCompleteAt) updateEventCompleteAt;
  
  var dateTime = DateFormat.yMMMEd().format(DateTime.now()).toString();
  var time = DateTime.now().toString().substring(10, 20);

  PortEventSummaryVM({
    required this.isLoading,
    required this.authState,
    required this.account,
    required this.accountPortDepot,
    required this.portEventCreate,
    required this.universalBranding,
    required this.accountCarrier,
    required this.fuelType,
    required this.portEventRole,
    required this.confirmPortEvent,
    required this.updateEventAt,
    required this.updateEventCompleteAt,
    // required this.tempStorage,
  });

  static PortEventSummaryVM fromStore(Store<AppState> store) {
    return PortEventSummaryVM(
      isLoading: store.state.isLoading,
      authState: store.state.authState,
      account: store.state.accountState.account,
      portEventRole: store.state.uiState.portEventRole,
      portEventCreate: store.state.portEventState.portEventCreate,
      updateEventAt: (String eventAt) {
        store.dispatch(UpdatePortEventEventAt(eventAt));
      },
      updateEventCompleteAt: (String? eventCompleteAt) {
        store.dispatch(UpdatePortEventEventCompleteAt(eventCompleteAt));
      },
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      accountPortDepot: store.state.accountState.accountPortDepots
          .firstWhere(
              (element) =>
          element.id == store.state.portEventState.portEventCreate.accountPortLocationId,
          orElse: () => AccountPortDepot.initial()),
      accountCarrier: store.state.accountState.accountCarriers.firstWhere(
          (element) =>
              element.carrierId ==
              store.state.portEventState.portEventCreate.carrierId,
          orElse: () => AccountCarrier.initial()),
      fuelType: store.state.referenceState.fuelTypes.firstWhere(
              (element) =>
          element.id ==
              store.state.portEventState.portEventCreate.fuelTypeId,
          orElse: () => FuelType.initial()),
      confirmPortEvent: (BuildContext context) {
        store.dispatch(QueuePortEvent(
            store.state.portEventState.portEventCreate));
        if (store.state.connectivityState.connectionStatus ==
                ConnectivityConstants.none ||
            store.state.connectivityState.connectionStatus ==
                ConnectivityConstants.ethernet) {
          showMessageWithActions(context, 'Queued',
              "Port Event has scheduled for submission when device is online.",
              () {
            Navigator.pop(context);
            store.dispatch(NavigateToAction.push(
                route_helper.portEventCompletionScreenBuilder));
          }, "Ok", null, "");
        } else {
          store.dispatch(NavigateToAction.pushNamedAndRemoveUntil(
              route_helper.portEventCompletionScreenBuilder,
              (route) => false));

          processPortEventQueue(store);
        }
      },
    );
  }
}
