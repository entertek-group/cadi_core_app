import 'package:cadi_core_app/factory/ui_factory.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/redux/models/account/account_port.dart';
import 'package:cadi_core_app/redux/models/account/account_port_depot.dart';
import 'package:cadi_core_app/redux/models/account/account_vehicle.dart';
import 'package:cadi_core_app/redux/models/account/account_vehicle_driver.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';
import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:flutter/material.dart';

import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'road_movement_home_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

class RoadMovementHomePage extends StatefulWidget {
  final RoadMovementHomeVM viewModel;
  const RoadMovementHomePage({
    super.key,
    required this.viewModel,
  });

  @override
  State<RoadMovementHomePage> createState() {
    return RoadMovementHomePageState(viewModel: viewModel);
  }
}

class RoadMovementHomePageState extends State<RoadMovementHomePage> {
  final RoadMovementHomeVM viewModel;

  final _selectAccountPortDepotController = TextEditingController();
  final _selectPortController = TextEditingController();
  final _selectVehicleController = TextEditingController();
  final _selectVehicleDriverController = TextEditingController();
  final _selectVehicleOperatorController = TextEditingController();
  final _selectRoleController = TextEditingController();

  static const Key _selectAccountPortDepotKey =
      Key(RoadMovementKeys.selectAccountPortDepotKey);
  static const Key _selectPortKey = Key(RoadMovementKeys.selectPortKey);
  static const Key _selectVehicleKey = Key(RoadMovementKeys.selectVehicleKey);
  static const Key _selectVehicleDriverKey =
      Key(RoadMovementKeys.selectVehicleDriverKey);
  static const Key _selectVehicleOperatorKey =
      Key(RoadMovementKeys.selectVehicleOperatorKey);
  static const Key _selectRoleKey = Key(RoadMovementKeys.selectRoleKey);

  int _selectedLocationIndex = 0;
  int _selectedPortIndex = 0;
  int _selectedVehicleIndex = 0;
  int _selectedVehicleDriverIndex = 0;
  int _selectedVehicleOperatorIndex = 0;
  int _selectedRoleIndex = 0;
  int _driverNameLength = 0;
  int _operatorNameLength = 0;
  int _vehicleNameLength = 0;

  List<DropdownItem> _accountLocationItems = [];
  List<DropdownItem> _portItems = [];
  List<DropdownItem> _vehicleItems = [];
  List<DropdownItem> _vehicleDriverItems = [];
  List<DropdownItem> _vehicleOperatorItems = [];
  List<DropdownItem> _roleItems = [];

  final FocusNode driverNode = FocusNode();
  final FocusNode operatorNode = FocusNode();
  final FocusNode vehicleNode = FocusNode();

  List<AccountPortDepot> _accountPortDepots = [];

  RoadMovementHomePageState({
    Key? key,
    required this.viewModel,
  });

  void _setPort(int index) {
    setState(() {
      _selectPortController.text = _portItems[index].text;
      _selectedPortIndex = index;
      widget.viewModel.setSelectedPortIndex(index);
      _accountPortDepots = widget.viewModel.accountPortDepots
          .where((element) =>
              element.portId == _portItems[_selectedPortIndex].value)
          .toList();

      _accountLocationItems = UiFactory()
          .convertToDropdown<AccountPortDepot, int>(
              _accountPortDepots, (item) => item.depotName, (item) => item.id);

      _selectedLocationIndex = 0;
    });
    if (_accountLocationItems.isNotEmpty &&
        _selectedLocationIndex < _accountLocationItems.length) {
      _selectAccountPortDepotController.text =
          _accountLocationItems[_selectedLocationIndex].text;
    }

    if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
      Future.delayed(
          const Duration(milliseconds: 300), _showLocationBottomPicker);
    }
  }

  void _setLocation(int index) {
    setState(() {
      _selectAccountPortDepotController.text =
          _accountLocationItems[index].text;
      _selectedLocationIndex = index;
      widget.viewModel.setSelectedLocationIndex(index);
    });

    FocusScope.of(context).requestFocus(vehicleNode);

    // if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
    //   Future.delayed(
    //       const Duration(milliseconds: 300), _showVehicleBottomPicker);
    // }
  }

  void _setVehicle(int index) {
    setState(() {
      _selectVehicleController.text = _vehicleItems[index].text;
      _selectedVehicleIndex = index;
      widget.viewModel.setSelectedVehicleIndex(index);
      _vehicleNameLength = _vehicleItems[index].text.length;
    });

    FocusScope.of(context).requestFocus(driverNode);
    // if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
    //   Future.delayed(const Duration(milliseconds: 300), _showVehicleDriverBottomPicker);
    // }
  }

  void _setVehicleDriver(int index) {
    setState(() {
      _selectVehicleDriverController.text = _vehicleDriverItems[index].text;
      _selectedVehicleDriverIndex = index;
      widget.viewModel.setSelectedVehicleDriverIndex(index);
      _driverNameLength = _vehicleDriverItems[index].text.length;
    });

    if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
      Future.delayed(const Duration(milliseconds: 300), _showRoleBottomPicker);
    }
  }

  void _setVehicleOperator(int index) {
    setState(() {
      _selectVehicleOperatorController.text = _vehicleOperatorItems[index].text;
      _selectedVehicleOperatorIndex = index;
      widget.viewModel.setSelectedVehicleOperatorIndex(index);
      _operatorNameLength = _vehicleOperatorItems[index].text.length;
    });

    if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
      Future.delayed(const Duration(milliseconds: 300), _showRoleBottomPicker);
    }
  }

  void _setRole(int index) {
    setState(() {
      _selectRoleController.text = _roleItems[index].text;
      _selectedRoleIndex = index;
      widget.viewModel.setSelectedRoleIndex(index);
    });

    Future.delayed(const Duration(milliseconds: 300), () => goNext(context));
  }

  void _showPortBottomPicker() {
    if (widget.viewModel.accountPortDepots.isEmpty) {
      showOkMessage(context, "Cannot select Port",
          "Account setup is incomplete. Please contact support. ");
    } else {
      showBottomDropdown(context, 'Port', 'Please select a port', _portItems,
          _selectedPortIndex, _setPort, true);
    }
  }

  void _showLocationBottomPicker() {
    if (widget.viewModel.accountPortDepots.isEmpty) {
      showOkMessage(context, "Cannot select Location",
          "Account setup is incomplete. Please contact support. ");
    } else {
      showBottomDropdown(
          context,
          'Account Location',
          'Please select account location',
          _accountLocationItems,
          _selectedLocationIndex,
          _setLocation,
          true);
    }
  }

  void _showVehicleBottomPicker(String searchParams) {
    if (widget.viewModel.accountVehicles.isEmpty) {
      showOkMessage(context, "Cannot select Vehicle",
          "Account setup is incomplete. Please contact support. ");
    } else {
      var searchList = _vehicleItems
          .where((element) =>
              element.text.toUpperCase().startsWith(searchParams.toUpperCase()))
          .toList();
      if (searchList.isNotEmpty) {
        showBottomDropdown(context, 'Vehicle', 'Please select Vehicle',
            searchList, _selectedVehicleIndex, _setVehicle, true);
      }
    }
  }

  void _showVehicleDriverBottomPicker(String searchParams) {
    if (widget.viewModel.accountVehicleDrivers.isEmpty) {
      showOkMessage(context, "Cannot select Driver",
          "Account setup is incomplete. Please contact support. ");
    } else {
      var searchList = _vehicleDriverItems
          .where((element) =>
              element.text.toUpperCase().startsWith(searchParams.toUpperCase()))
          .toList();
      if (searchList.isNotEmpty) {
        showBottomDropdown(context, 'Driver', 'Please select Driver',
            searchList, _selectedVehicleDriverIndex, _setVehicleDriver, true);
      }
    }
  }

  void _showVehicleOperatorBottomPicker(String searchParams) {
    if (widget.viewModel.accountVehicleOperators.isEmpty) {
    } else {
      var searchList = _vehicleOperatorItems
          .where((element) =>
              element.text.toUpperCase().startsWith(searchParams.toUpperCase()))
          .toList();
      if (searchList.isNotEmpty) {
        showBottomDropdown(
            context,
            'Operator',
            'Please select Operator',
            searchList,
            _selectedVehicleOperatorIndex,
            _setVehicleOperator,
            true);
      }
    }
  }

  void _showRoleBottomPicker() {
    if (widget.viewModel.roadCargoMovementRoles.isEmpty) {
      showOkMessage(context, "Cannot select Role",
          "Account setup is incomplete. Please contact support. ");
    } else {
      showBottomDropdown(context, 'Role', 'Please select Role', _roleItems,
          _selectedRoleIndex, _setRole, true);
    }
  }

  @override
  initState() {
    super.initState();
    final uiFactory = UiFactory();

    _portItems = uiFactory.convertToDropdown<AccountPort, int>(
        widget.viewModel.accountPorts,
        (port) => port.unLocode,
        (port) => port.portId);

    _vehicleItems = uiFactory.convertToDropdown<AccountVehicle, int>(
        widget.viewModel.accountVehicles,
        (vehicle) => vehicle.model,
        (vehicle) => vehicle.id);

    _vehicleDriverItems =
        uiFactory.convertToDropdown<AccountVehicleDriver, int>(
            widget.viewModel.accountVehicleDrivers,
            (driver) => driver.fullName,
            (driver) => driver.id);

    _vehicleOperatorItems =
        uiFactory.convertToDropdown<AccountVehicleDriver, int>(
            widget.viewModel.accountVehicleOperators,
            (driver) => driver.fullName,
            (driver) => driver.id);

    _roleItems = uiFactory.convertToDropdown<CadiEnum, String>(
        widget.viewModel.roadCargoMovementRoles,
        (role) => role.description,
        (role) => role.id);

    widget.viewModel.initScreenData(context);

    _accountPortDepots = widget.viewModel.accountPortDepots;

    _selectedPortIndex = widget.viewModel.uiRoadMovementHome.selectedPortIndex;
    _selectedLocationIndex =
        widget.viewModel.uiRoadMovementHome.selectedLocationIndex;
    _selectedVehicleIndex =
        widget.viewModel.uiRoadMovementHome.selectedVehicleIndex;
    _selectedVehicleDriverIndex =
        widget.viewModel.uiRoadMovementHome.selectedVehicleDriverIndex;
    _selectedVehicleOperatorIndex =
        widget.viewModel.uiRoadMovementHome.selectedVehicleOperatorIndex;
    _selectedRoleIndex = widget.viewModel.uiRoadMovementHome.selectedRoleIndex;

    if (widget.viewModel.accountPorts.isNotEmpty &&
        _selectedPortIndex < widget.viewModel.accountPorts.length) {
      _selectPortController.text = _portItems[_selectedPortIndex].text;
      _accountPortDepots = widget.viewModel.accountPortDepots
          .where((element) =>
              element.portId == _portItems[_selectedPortIndex].value)
          .toList();

      _accountLocationItems = UiFactory()
          .convertToDropdown<AccountPortDepot, int>(
              _accountPortDepots, (item) => item.depotName, (item) => item.id);

      if (_accountLocationItems.isNotEmpty &&
          _selectedLocationIndex < _accountLocationItems.length) {
        _selectAccountPortDepotController.text =
            _accountLocationItems[_selectedLocationIndex].text;
      }
    }

    if (widget.viewModel.accountVehicles.isNotEmpty &&
        _selectedVehicleIndex < widget.viewModel.accountVehicles.length) {
      _selectVehicleController.text = _vehicleItems[_selectedVehicleIndex].text;
      _vehicleNameLength = _vehicleItems[_selectedVehicleIndex].text.length;
    }

    if (widget.viewModel.accountVehicleDrivers.isNotEmpty &&
        _selectedVehicleDriverIndex <
            widget.viewModel.accountVehicleDrivers.length) {
      _selectVehicleDriverController.text =
          _vehicleDriverItems[_selectedVehicleDriverIndex].text;
      _driverNameLength =
          _vehicleDriverItems[_selectedVehicleDriverIndex].text.length;
      // _setVehicleDriver(_selectedVehicleDriverIndex);
    }

    if (widget.viewModel.accountVehicleOperators.isNotEmpty &&
        _selectedVehicleOperatorIndex <
            widget.viewModel.accountVehicleOperators.length) {
      _selectVehicleOperatorController.text =
          _vehicleOperatorItems[_selectedVehicleOperatorIndex].text;
      _operatorNameLength =
          _vehicleOperatorItems[_selectedVehicleOperatorIndex].text.length;
      // _setVehicleOperator(_selectedVehicleOperatorIndex);
    }

    if (widget.viewModel.roadCargoMovementRoles.isNotEmpty &&
        _selectedRoleIndex < widget.viewModel.roadCargoMovementRoles.length) {
      _selectRoleController.text = _roleItems[_selectedRoleIndex].text;

      setState(() {});
    }
  }

  @override
  void dispose() {
    _selectAccountPortDepotController.dispose();
    _selectPortController.dispose();
    _selectVehicleController.dispose();
    _selectVehicleDriverController.dispose();
    _selectVehicleOperatorController.dispose();
    _selectRoleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final List<SettingTile> allSettings = <SettingTile>[
      SettingTile(
        0,
        'Active Account',
        Container(
          child: Column(
            children: <Widget>[
              Text("Road Movement",
                  style: TextStyle(
                      fontSize: PlatformSizing().normalTextSize(context),
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),
      SettingTile(
        1,
        'Account Operations',
        Container(
          margin: const EdgeInsets.only(top: 10.0), // Add your own value here
          child: Column(
            children: <Widget>[
              Text("Select Location, Carrier and Roles",
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      fontWeight: FontWeight.w300,
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),
      SettingTile(
          2,
          'PORT',
          ListTile(
            title: Text("Port",
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          )),
      SettingTile(
          3,
          "",
          InkWell(
            onTap: () {
              _showPortBottomPicker();
            },
            child: PlatformTextField(
              cursorColor: BrandingColors.cadiGrey,
              textCapitalization: TextCapitalization.characters,
              controller: _selectPortController,
              key: _selectPortKey,
              autocorrect: false,
              enabled: false,
              material: (_, __) => MaterialAssetsDark()
                  .textFieldData("", FontAwesomeIcons.chevronDown),
              cupertino: (_, __) => CupertinoAssets()
                  .textFieldData('Port', FontAwesomeIcons.chevronDown),
              style: TextStyle(
                  color: BrandingColors.cadiBlack,
                  fontWeight: FontWeight.w400,
                  fontSize: PlatformSizing().inputTextFieldSize(context)),
            ),
          )),
      SettingTile(
          4,
          'LOCATION',
          ListTile(
            title: Text('Location',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          )),
      SettingTile(
          5,
          "",
          InkWell(
            onTap: () {
              _showLocationBottomPicker();
            },
            child: PlatformTextField(
              cursorColor: BrandingColors.cadiGrey,
              textCapitalization: TextCapitalization.characters,
              controller: _selectAccountPortDepotController,
              key: _selectAccountPortDepotKey,
              autocorrect: false,
              enabled: false,
              material: (_, __) => MaterialAssetsDark()
                  .textFieldData("", FontAwesomeIcons.chevronDown),
              cupertino: (_, __) => CupertinoAssets()
                  .textFieldData('Location', FontAwesomeIcons.chevronDown),
              style: TextStyle(
                  color: BrandingColors.cadiBlack,
                  fontWeight: FontWeight.w400,
                  fontSize: PlatformSizing().inputTextFieldSize(context)),
              keyboardType: TextInputType.text,
            ),
          )),
      SettingTile(
          6,
          'Vehicle',
          ListTile(
            title: Text('Vehicle',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          )),
      SettingTile(
        7,
        "",
        PlatformTextField(
          cursorColor: BrandingColors.cadiGrey,
          onChanged: (dataString) {
            if (dataString.isNotEmpty &&
                dataString.length > _vehicleNameLength &&
                dataString.length > 2) {
              _showVehicleBottomPicker(dataString);
            }
            setState(() {
              _vehicleNameLength = dataString.length;
            });
          },
          focusNode: vehicleNode,
          textCapitalization: TextCapitalization.characters,
          controller: _selectVehicleController,
          key: _selectVehicleKey,
          autocorrect: false,
          enabled: true,
          material: (_, __) =>
              MaterialAssetsDark().textFieldData("", FontAwesomeIcons.search),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData('Vehicle', null),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontWeight: FontWeight.w400,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
        ),
      ),
      SettingTile(
          8,
          'Driver',
          ListTile(
            title: Text('Driver',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          )),
      SettingTile(
        9,
        "",
        PlatformTextField(
          cursorColor: BrandingColors.cadiGrey,
          onChanged: (dataString) {
            if (dataString.isNotEmpty &&
                dataString.length > _driverNameLength &&
                dataString.length > 2) {
              _showVehicleDriverBottomPicker(dataString);
            }
            setState(() {
              _driverNameLength = dataString.length;
            });
          },
          focusNode: driverNode,
          textCapitalization: TextCapitalization.characters,
          controller: _selectVehicleDriverController,
          key: _selectVehicleDriverKey,
          autocorrect: false,
          enabled: true,
          material: (_, __) =>
              MaterialAssetsDark().textFieldData("", FontAwesomeIcons.search),
          cupertino: (_, __) => CupertinoAssets().textFieldData('Driver', null),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontWeight: FontWeight.w400,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
        ),
      ),
      SettingTile(
          10,
          'Operator',
          ListTile(
            title: Text('Operator',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          )),
      SettingTile(
        11,
        "",
        PlatformTextField(
          cursorColor: BrandingColors.cadiGrey,
          onChanged: (dataString) {
            if (dataString.isNotEmpty &&
                dataString.length > _operatorNameLength &&
                dataString.length > 2) {
              _showVehicleOperatorBottomPicker(dataString);
            }
            setState(() {
              _operatorNameLength = dataString.length;
            });
          },
          focusNode: operatorNode,
          textCapitalization: TextCapitalization.characters,
          controller: _selectVehicleOperatorController,
          key: _selectVehicleOperatorKey,
          autocorrect: false,
          enabled: true,
          material: (_, __) =>
              MaterialAssetsDark().textFieldData("", FontAwesomeIcons.search),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData('Operator', null),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontWeight: FontWeight.w400,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
        ),
      ),
      SettingTile(
          12,
          'ROLE',
          ListTile(
            title: Text('Role',
                style: TextStyle(
                    fontSize: PlatformSizing().mediumTextSize(context),
                    fontWeight: FontWeight.w300,
                    color: BrandingColors.cadiBlack)),
          )),
      SettingTile(
          13,
          "",
          InkWell(
            onTap: () {
              _showRoleBottomPicker();
            },
            child: PlatformTextField(
              cursorColor: BrandingColors.cadiGrey,
              textCapitalization: TextCapitalization.characters,
              controller: _selectRoleController,
              key: _selectRoleKey,
              autocorrect: false,
              enabled: false,
              readOnly: true,
              material: (_, __) => MaterialAssetsDark()
                  .textFieldData("", FontAwesomeIcons.chevronDown),
              cupertino: (_, __) => CupertinoAssets()
                  .textFieldData('Role', FontAwesomeIcons.chevronDown),
              style: TextStyle(
                  color: BrandingColors.cadiBlack,
                  fontWeight: FontWeight.w400,
                  fontSize: PlatformSizing().inputTextFieldSize(context)),
              keyboardType: TextInputType.text,
            ),
          )),
      const SettingTile(
          14,
          '',
          ListTile(
            title: Text(
              '',
            ),
          )),
      SettingTile(
          15,
          '',
          Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 50.0, vertical: 0.0),
              child: BaseButton(
                  color: widget.viewModel.universalBranding
                      .getButtonPrimaryColor(),
                  child: Text('Go',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: PlatformSizing().normalTextSize(context))),
                  onPressed: () {
                    goNext(context);
                  }))),
    ];

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.only(top: 0.0, left: 40, right: 40),
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: allSettings.length,
          padding: const EdgeInsets.only(
            left: 10.0,
            right: 10,
          ),
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
  }

  void goNext(BuildContext context) {
    int? operatorId;
    if (_selectVehicleOperatorController.text.trim() == "") {
      operatorId = null;
    } else {
      operatorId = _vehicleOperatorItems[_selectedVehicleOperatorIndex].value;
    }

    viewModel.onNavigatePressed(
        context,
        _roleItems[_selectedRoleIndex].value,
        _accountLocationItems[_selectedLocationIndex].value,
        _vehicleItems[_selectedVehicleIndex].value,
        _vehicleDriverItems[_selectedVehicleDriverIndex].value,
        operatorId);
  }
}
