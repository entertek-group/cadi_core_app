import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/theme/branding.dart';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'road_movement_home_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

class RoadMovementHomeScreenBuilder extends StatelessWidget {
  const RoadMovementHomeScreenBuilder({super.key});

  static const String route = route_helper.roadMovementHomeScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: BrandingColors.cadiWhite,
      body: StoreConnector<AppState, RoadMovementHomeVM>(
        converter: RoadMovementHomeVM.fromStore,
        builder: (context, vm) {
          return RoadMovementHomePage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class RoadMovementHomeVM {
  bool isLoading;
  bool settingsAutoDisplayNextDropdownOrInput;
  bool settingsAutoSubmitPage;
  AuthState authState;
  Account account;
  List<AccountPortDepot> accountPortDepots;
  List<AccountPort> accountPorts;
  List<AccountVehicle> accountVehicles;
  List<AccountVehicleDriver> accountVehicleDrivers;
  List<AccountVehicleDriver> accountVehicleOperators;
  UiRoadMovementHome uiRoadMovementHome;
  UniversalBranding universalBranding;

  final List<CadiEnum> roadCargoMovementRoles;
  final Function(BuildContext context, String role,
      int accountPortDepotId, int vehicleId, int vehicleDriver, int? vehicleOperator) onNavigatePressed;
  final Function(int selectedPortIndex) setSelectedPortIndex;
  final Function(int selectedLocationIndex) setSelectedLocationIndex;
  final Function(int selectedvehicleIndex) setSelectedVehicleIndex;
  final Function(int selectedvehicleDriverIndex) setSelectedVehicleDriverIndex;
  final Function(int selectedvehicleOperatorIndex) setSelectedVehicleOperatorIndex;
  final Function(int selectedRoleIndex) setSelectedRoleIndex;
  final Function(BuildContext context) initScreenData;

  RoadMovementHomeVM({
    required this.isLoading,
    required this.settingsAutoDisplayNextDropdownOrInput,
    required this.settingsAutoSubmitPage,
    required this.authState,
    required this.account,
    required this.accountPortDepots,
    required this.accountPorts,
    required this.accountVehicles,
    required this.accountVehicleDrivers,
    required this.accountVehicleOperators,
    required this.uiRoadMovementHome,
    required this.universalBranding,
    required this.roadCargoMovementRoles,
    required this.onNavigatePressed,
    required this.setSelectedPortIndex,
    required this.setSelectedLocationIndex,
    required this.setSelectedVehicleIndex,
    required this.setSelectedVehicleDriverIndex,
    required this.setSelectedVehicleOperatorIndex,
    required this.setSelectedRoleIndex,
    required this.initScreenData,
  });

  static RoadMovementHomeVM fromStore(Store<AppState> store) {
    return RoadMovementHomeVM(
      isLoading: store.state.isLoading,
      settingsAutoDisplayNextDropdownOrInput:
          store.state.uiState.uiDropDownOrInputAutoContinue,
      settingsAutoSubmitPage: store.state.uiState.uiAutoSubmitPage,
      authState: store.state.authState,
      account: store.state.accountState.account,
      accountVehicles: store.state.accountState.filteredAccountVehicles,
      accountVehicleDrivers: store.state.accountState.filteredAccountVehicleDrivers,
      accountVehicleOperators: store.state.accountState.filteredAccountVehicleOperators,
      accountPortDepots: store.state.accountState.filteredAccountPortDepots,
      accountPorts: store.state.accountState.filteredAccountPorts,
      roadCargoMovementRoles: store.state.referenceState.roadCargoMovementRoles,
      uiRoadMovementHome: store.state.uiState.uiRoadMovementHome,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      initScreenData: (context) {
        store.dispatch(ResetRoadMovementCreate());
      },
      onNavigatePressed: (BuildContext context, String role,
          int accountPortDepotId, int vehicleId, int vehicleDriver, int? vehicleOperator) {
        if (store.state.isLoading) {
          return;
        }

        store.dispatch(UiRoadMovementSetRole(role));
        store.dispatch(SetRoadAccountPortDepot(accountPortDepotId));
        store.dispatch(
            SetRoadHomeVariables(accountPortDepotId, vehicleId, vehicleDriver, vehicleOperator, role));
        store.dispatch(NavigateToAction.push(
            route_helper.roadMovementFreightSearchScreenBuilder));
      },
      setSelectedPortIndex: (int selectedPortIndex) {
        store.dispatch(UiRoadMovementHomeSetPortIndex(selectedPortIndex));
      },
      setSelectedLocationIndex: (int selectedLocationIndex) {
        store.dispatch(UiRoadMovementHomeSetLocationIndex(selectedLocationIndex));
      },
      setSelectedVehicleIndex: (int selectedVehicleIndex) {
        store.dispatch(UiRoadMovementHomeSetVehicleIndex(selectedVehicleIndex));
      },
      setSelectedVehicleDriverIndex: (int selectedVehicleDriverIndex) {
        store.dispatch(UiRoadMovementHomeSetVehicleDriverIndex(selectedVehicleDriverIndex));
      },
      setSelectedVehicleOperatorIndex: (int selectedVehicleOperatorIndex) {
        store.dispatch(UiRoadMovementHomeSetVehicleOperatorIndex(selectedVehicleOperatorIndex));
      },
      setSelectedRoleIndex: (int selectedRoleIndex) {
        store.dispatch(UiRoadMovementHomeSetRoleIndex(selectedRoleIndex));
      },
    );
  }
}
