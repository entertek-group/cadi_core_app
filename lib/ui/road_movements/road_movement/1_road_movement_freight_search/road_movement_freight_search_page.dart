import 'dart:convert';
import 'dart:io';

import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/helpers/datetime_helper.dart';
import 'package:cadi_core_app/helpers/messages.dart';
import 'package:cadi_core_app/helpers/validation_helper.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/redux/models/image/image_create.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/base/base_move_event_select.dart';
import 'package:cadi_core_app/ui/shared/components/base/base_table.dart';
import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';
import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'road_movement_freight_search_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

class RoadMovementFreightSearchPage extends StatefulWidget {
  final RoadMovementFreightSearchVM viewModel;
  const RoadMovementFreightSearchPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<RoadMovementFreightSearchPage> createState() {
    return RoadMovementFreightSearchPageState(viewModel: viewModel);
  }
}

class RoadMovementFreightSearchPageState
    extends State<RoadMovementFreightSearchPage> {
  final RoadMovementFreightSearchVM viewModel;
  final _searchWaybillController = TextEditingController();
  final _searchLegNumberController = TextEditingController();
  final _inputContainerNumberController = TextEditingController();
  DateTime dateTime = DateTime.now();
  List<TableRow> documentTableRows = <TableRow>[];
  List<ImageCreate> attachedDocuments = <ImageCreate>[];
  String documentTypeCode = "";
  bool _isBreakBulk = false;
  String _selectedCargoType = CadiRoadMovementCargoType.container;
  bool _overrideContainerFormat = false;

  var timeIn = "";

  static const Key _searchWaybillKey = Key(RoadMovementKeys.searchWaybillKey);
  static const Key _searchLegNumberKey =
      Key(RoadMovementKeys.searchLegNumberKey);
  static const Key _inputContainerNumberKey =
      Key(RoadMovementKeys.inputContainerNumberKey);

  final FocusNode voyageWayBillNumberInputNode = FocusNode();
  final FocusNode legNumberInputNode = FocusNode();
  final FocusNode etaInputNode = FocusNode();
  final FocusNode containerInputNode = FocusNode();

  RoadMovementFreightSearchPageState({
    Key? key,
    required this.viewModel,
  });

  @override
  initState() {
    super.initState();
    _searchWaybillController.text =
        viewModel.uiRoadMovementSearch.waybill == CadiRoadMovementCargoType.fuel
            ? ""
            : viewModel.uiRoadMovementSearch.waybill;
    _searchLegNumberController.text =
        viewModel.uiRoadMovementSearch.legNumber == "0"
            ? ""
            : viewModel.uiRoadMovementSearch.legNumber;
    timeIn = viewModel.roadMovementCreate.expectedArrivalAt ?? "";

    if (timeIn == "") {
      timeIn = getDateTimeWithOffset(dateTime);
    }

    buildTableHeader();

    if (widget.viewModel.attachedDocuments != null &&
        widget.viewModel.attachedDocuments!.isNotEmpty) {
      attachedDocuments = widget.viewModel.attachedDocuments!;
      rebuildTableRows();
    }

    if (widget.viewModel.roadMovementCreate.containerData != null) {
      var firstContainer =
          widget.viewModel.roadMovementCreate.containerData!.first;
      _inputContainerNumberController.text = firstContainer.containerNumber;
    }

    //set Selected Cargo Type (save this om screen state).
  }

  @override
  void dispose() {
    _searchWaybillController.dispose();
    _searchLegNumberController.dispose();
    _inputContainerNumberController.dispose();
    super.dispose();
  }

  void addImage() {
    var dropDownItem = <DropdownItem>[];
    for (var element in widget.viewModel.documentTypes) {
      dropDownItem.add(DropdownItem(element.description, 0, element.code));
    }

    showBottomDropdown(
        context,
        'Document Type',
        'What document type do you want to upload?',
        dropDownItem,
        0,
        _imagePicker,
        false);
  }

  void _imagePicker(String documentType) {
    setState(() {
      documentTypeCode = documentType;
    });

    var dropDownItems = <DropdownItem>[];
    dropDownItems.add(const DropdownItem("Camera", 0, ImageSource.camera));
    dropDownItems.add(const DropdownItem("Gallery", 0, ImageSource.gallery));

    if (Platform.isAndroid) {
      /*
       Showing
        */
      Navigator.pop(context, "");
      showBottomDropdown(
          context,
          'Document',
          'Where would you like to retrieve the document from?',
          dropDownItems,
          0,
          _processImage,
          false);
    }

    showBottomDropdown(
        context,
        'Document',
        'Where would you like to retrieve the document from?',
        dropDownItems,
        0,
        _processImage,
        false);
  }

  void buildTableHeader() {
    setState(() {
      documentTableRows = <TableRow>[];
      documentTableRows.add(buildTableRow(context, buildTableTitle("DOCUMENT"),
          buildTableTitle("TYPE"), buildTableTitle("ACTION")));
    });
  }

  void rebuildTableRows() {
    buildTableHeader();
    for (var attachedDocument in attachedDocuments) {
      addDocumentRow(attachedDocument);
    }
  }

  void addDocumentRow(ImageCreate attachedImage) {
    var displayName = imageDisplayName(attachedImage.name);
    setState(() {
      documentTableRows.add(buildTableRow(context, buildTableTitle(displayName),
          buildTableTitle(attachedImage.kind ?? ""), delete(attachedImage)));
    });
  }

  String imageDisplayName(String imageName) {
    return imageName.length > 6
        ? imageName.substring(imageName.length - 5)
        : imageName;
  }

  Future<void> _processImage(ImageSource imageSource) async {
    final ImagePicker picker = ImagePicker();
    XFile? image = await picker.pickImage(source: imageSource);
    if (image != null) {
      setImage(image);
    }
  }

  void setImage(XFile? image) {
    if (image != null) {
      final bytes = File(image.path).readAsBytesSync();
      String name = image.name;
      String base64Content = base64Encode(bytes);
      ImageCreate imageCreate = ImageCreate(
          name: name, base64Content: base64Content, kind: documentTypeCode);

      setState(() {
        attachedDocuments.add(imageCreate);
        addDocumentRow(imageCreate);
      });
    }
  }

  TextButton delete(ImageCreate imageCreate) {
    return TextButton.icon(
        onPressed: () {
          var displayName = imageDisplayName(imageCreate.name);
          showMessageWithActions(
              context,
              "Remove document #$displayName",
              "Are you sure you want to remove this document?",
              () {
                Navigator.pop(context);
                setState(() {
                  attachedDocuments.remove(imageCreate);
                });
                rebuildTableRows();
              },
              "Yes",
              () {
                Navigator.pop(context);
              },
              "No");
        },
        icon: const Icon(
          FontAwesomeIcons.xmark,
          size: 20,
          color: BrandingColors.cadiGreen,
        ),
        label: const Text(""));
  }

  void goNext() {
    if (_selectedCargoType != CadiRoadMovementCargoType.fuel &&
        _searchWaybillController.text.trim() == "") {
      viewModel.showErrorAlert(context, "Please input a waybill number.");
    } else if (_selectedCargoType != CadiRoadMovementCargoType.fuel &&
        _searchLegNumberController.text.trim() == "") {
      viewModel.showErrorAlert(context, "Please input a leg number.");
    } else {
      var waybillNumber = "";
      var legNumber = "";
      var containerNumber = "";

      if (_selectedCargoType == CadiRoadMovementCargoType.fuel) {
        waybillNumber = CadiRoadMovementCargoType.fuel;
        legNumber = "0";
      } else {
        waybillNumber = _searchWaybillController.text
            .toUpperCase()
            .trim()
            .replaceAll(" ", "");
        legNumber = _searchLegNumberController.text
            .toUpperCase()
            .trim()
            .replaceAll(" ", "");
      }

      if (_selectedCargoType == CadiRoadMovementCargoType.container) {
        containerNumber = _inputContainerNumberController.text
            .toUpperCase()
            .trim()
            .replaceAll(" ", "");
        if (containerNumber == "") {
          viewModel.showErrorAlert(context, "Please input a container number");
          return;
        } else if (!_overrideContainerFormat &&
            !isContainerInputValid(
                context, _inputContainerNumberController.text.trim())) {
          showMessageWithActions(
              context,
              "Container Number Format",
              WarningMessages.containerOverrideMessage,
              () {
                setState(() {
                  _overrideContainerFormat = true;
                });
                Navigator.pop(context);
                viewModel.createRoadMovement(context, waybillNumber, legNumber,
                    timeIn, containerNumber, _isBreakBulk, attachedDocuments);
              },
              //Add Container
              "Accept",
              () {
                Navigator.pop(context);
              },
              "Cancel");
          return;
        }
      }

      viewModel.createRoadMovement(context, waybillNumber, legNumber, timeIn,
          containerNumber, _isBreakBulk, attachedDocuments);
    }
  }

  // Future<void> _showDateTimePicker() async {
  //   TimeOfDay? timeOfDay = await showPlatformTimePicker(
  //     context: context,
  //     initialTime: TimeOfDay.fromDateTime(dateTime),
  //   );
  //
  //   if (timeOfDay == null) {
  //     _searchEtaController.text = getDateTimeWithOffset(dateTime);
  //   } else{
  //     final converted = dateTime.applied(timeOfDay);
  //     setState(() {
  //       dateTime = converted;
  //     });
  //     _searchEtaController.text = getDateTimeWithOffset(converted);
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    List<Widget> moveEventWidgets = <Widget>[];
    for (var cadiEnum in widget.viewModel.roadMovementCargoTypes) {
      var color = _selectedCargoType != cadiEnum.id
          ? BrandingColors.white
          : BrandingColors.cadiGrey;

      moveEventWidgets.add(buildContainerCard(
        context,
        "",
        () => {
          setState(() {
            if (cadiEnum.id != CadiRoadMovementCargoType.container) {
              _isBreakBulk = true;
            } else {
              _isBreakBulk = false;
            }
            _selectedCargoType = cadiEnum.id;
          }),
        },
        color,
        cadiEnum.description,
      ));
      // index += 1;
    }

    final List<SettingTile> allSettings = <SettingTile>[
      SettingTile(
        0,
        'Active Account',
        Column(
          children: <Widget>[
            Text(
                "Road Movement: ${getRoadMovementRoleDescription(widget.viewModel.roadMovementRole)}",
                style: TextStyle(
                    fontSize: PlatformSizing().normalTextSize(context),
                    color: BrandingColors.cadiBlack)),
            // Any additional widget/codes
          ],
        ),
      ),
      SettingTile(
        1,
        'Account Operations',
        Container(
          margin: const EdgeInsets.only(top: 10.0), // Add your own value here
          child: Column(
            children: <Widget>[
              Text("Enter Details",
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      fontWeight: FontWeight.w300,
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),
      SettingTile(
          2,
          "CARGO TYPE",
          Material(
              color: BrandingColors.cadiWhite,
              child: buildMoveEventList(context, moveEventWidgets))),
    ];

    if (_selectedCargoType != CadiRoadMovementCargoType.fuel) {
      allSettings.add(SettingTile(
          3,
          'Waybill',
          Material(
            color: BrandingColors.background,
            child: ListTile(
                title: Text('Job Number',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack))),
          )));

      allSettings.add(SettingTile(
        4,
        "Waybill Input",
        PlatformTextField(
          cursorColor: BrandingColors.cadiGrey,
          textCapitalization: TextCapitalization.characters,
          controller: _searchWaybillController,
          key: _searchWaybillKey,
          autocorrect: false,
          material: (_, __) =>
              MaterialAssetsDark().textFieldData("", FontAwesomeIcons.magnifyingGlass),
          cupertino: (_, __) => CupertinoAssets().textFieldData('Job', null),
          style: TextStyle(
              color: BrandingColors.cadiBlack,
              fontWeight: FontWeight.w400,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
          onEditingComplete: () {
            if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
              FocusScope.of(context).requestFocus(legNumberInputNode);
            }
          },
        ),
      ));

      allSettings.add(SettingTile(
          5,
          'LEG NUMBER',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('Leg',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: PlatformSizing().mediumTextSize(context),
                        color: BrandingColors.cadiBlack)),
              ))));

      allSettings.add(SettingTile(
        6,
        "Leg Number",
        PlatformTextField(
          cursorColor: BrandingColors.cadiGrey,
          focusNode: legNumberInputNode,
          textCapitalization: TextCapitalization.characters,
          controller: _searchLegNumberController,
          key: _searchLegNumberKey,
          autocorrect: false,
          hintText: "Leg",
          material: (_, __) =>
              MaterialAssetsDark().textFieldData("", FontAwesomeIcons.magnifyingGlass),
          cupertino: (_, __) =>
              CupertinoAssets().textFieldData('Leg', null),
          style: TextStyle(
              fontWeight: FontWeight.w400,
              color: BrandingColors.cadiBlack,
              fontSize: PlatformSizing().inputTextFieldSize(context)),
          keyboardType: TextInputType.text,
          onEditingComplete: () {
            if (widget.viewModel.settingsAutoDisplayNextDropdownOrInput) {
              FocusScope.of(context).requestFocus(etaInputNode);
            }
          },
        ),
      ));

      if (_selectedCargoType == CadiRoadMovementCargoType.container) {
        allSettings.add(SettingTile(
            61,
            'CONTAINER NUMBER',
            Material(
              color: BrandingColors.cadiWhite,
              child: ListTile(
                  title: Text("Container Number",
                      style: TextStyle(
                          fontWeight: FontWeight.w300,
                          fontSize: PlatformSizing().mediumTextSize(context),
                          color: BrandingColors.cadiBlack))),
            )));

        allSettings.add(SettingTile(
            3,
            '',
            PlatformTextField(
              cursorColor: BrandingColors.cadiGrey,
              focusNode: containerInputNode,
              textCapitalization: TextCapitalization.characters,
              controller: _inputContainerNumberController,
              key: _inputContainerNumberKey,
              hintText: "Container Number",
              autocorrect: false,
              material: (_, __) => MaterialAssetsDark()
                  .textFieldData("", FontAwesomeIcons.magnifyingGlass),
              cupertino: (_, __) =>
                  CupertinoAssets().textFieldData("Container Number", null),
              style: TextStyle(
                  color: BrandingColors.cadiBlack,
                  fontSize: PlatformSizing().inputTextFieldSize(context)),
              //maxLength: 11,
              keyboardType: TextInputType.text,
            )));
      }
    }

    // allSettings.add(SettingTile(
    //     7,
    //     'ETA INPUT',
    //     Material(
    //         color: BrandingColors.background,
    //         child: ListTile(
    //           title: Text('Time In $timeIn',
    //               style: TextStyle(
    //                   fontWeight: FontWeight.w300,
    //                   fontSize: PlatformSizing().mediumTextSize(context),
    //                   color: BrandingColors.cadiBlack)),
    //         ))));

    // allSettings.add(SettingTile(
    //   8,
    //   "ETA",
    //   Material(
    //     color: BrandingColors.background,
    //     child:
    //     InkWell(
    //       onTap: () {
    //         _showDateTimePicker();
    //       },
    //       child:
    //       PlatformTextField(
    //         textCapitalization: TextCapitalization.characters,
    //         controller: _searchEtaController,
    //         key: _searchEtaKey,
    //         autocorrect: false,
    //         enabled: false,
    //         material: (_, __) => MaterialAssetsDark()
    //             .textFieldData("", FontAwesomeIcons.chevronDown),
    //         cupertino: (_, __) => CupertinoAssets()
    //             .textFieldData('Port', FontAwesomeIcons.chevronDown),
    //         style: TextStyle(
    //             color: BrandingColors.cadiBlack,
    //             fontWeight: FontWeight.w400,
    //             fontSize: PlatformSizing().inputTextFieldSize(context)),
    //       ),
    //     ),
    //   ),
    // ));

    allSettings.add(SettingTile(
        8,
        '',
        Material(
          color: BrandingColors.background,
          child: Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    BaseGhostButton(
                      onPressed: () {
                        addImage();
                      },
                      textColor: BrandingColors.cadiBlack,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const Icon(
                            FontAwesomeIcons.paperclip,
                            size: 20,
                            color: BrandingColors.cadiBlack,
                          ),
                          const SizedBox(width: 8),
                          Text(
                            'Add',
                            style: TextStyle(
                              color: BrandingColors
                                  .cadiBlack, // Set your desired text color here
                              fontSize:
                                  PlatformSizing().mediumTextSize(context),
                            ),
                          ),
                        ],
                      ), // Pass the text color here
                    ),
                  ])),
        )));

    allSettings.add(SettingTile(
        20,
        'Summary',
        Material(
            color: BrandingColors.background,
            child: Padding(
              padding: const EdgeInsets.only(top: 20, left: 10),
              child: buildScrollableDynamicTable(
                  buildSummaryTable(context, documentTableRows)),
            ))));

    allSettings.add(const SettingTile(
        9,
        '',
        Material(
            color: BrandingColors.background,
            child: ListTile(
              title: Text(''),
            ))));

    allSettings.add(SettingTile(
        10,
        '',
        Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 50.0, vertical: 0.0),
            child: BaseButton(
                color:
                    widget.viewModel.universalBranding.getButtonPrimaryColor(),
                child: Text('Next',
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: PlatformSizing().normalTextSize(context))),
                onPressed: () {
                  goNext();
                }))));

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          //itemBuilder: itemBuilder) .separated(
          itemCount: allSettings.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          // separatorBuilder: (BuildContext context, int index) =>
          //     Divider(height: 1),
          itemBuilder: (BuildContext context, int index) {
            return allSettings[index].body;
          }),
    );
  }
}

Widget buildDocumentRow(BuildContext context, String textDisplay,
    String textValue, bool hasAction, Function? buttonAction) {
  return ListTile(
      dense: true,
      visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
      contentPadding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0),
      tileColor: Colors.transparent,
      leading: Text(
        textDisplay,
        style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: PlatformSizing().smallTextSize(context),
            color: BrandingColors.cadiBlack),
      ),
      title: Text(
        textValue,
        style: TextStyle(
            fontSize: PlatformSizing().smallTextSize(context),
            color: BrandingColors.cadiBlack),
        textAlign: TextAlign.end,
      ),
      trailing: hasAction
          ? IconButton(
              padding: const EdgeInsets.only(bottom: 0, left: 25),
              icon: const Icon(FontAwesomeIcons.penToSquare,
                  color: BrandingColors.cadiBlack),
              onPressed: buttonAction != null ? () => buttonAction() : () {})
          : const IconButton(
              icon: Icon(Icons.edit_outlined, color: Colors.transparent),
              onPressed: null));
}

Widget buildMoveEventList(BuildContext context, List<Widget> moveEvents) {
  return LayoutBuilder(
    builder: (context, constraints) {
      double mainAxisSpacing = constraints.maxWidth > 600 ? 20 : 10; // Adjust spacing based on screen width
      double crossAxisSpacing = constraints.maxWidth > 600 ? 20 : 10; // Adjust spacing based on screen width
      double childAspectRatio = constraints.maxWidth > 600 ? 6 : 2.8; // Adjust aspect ratio based on screen width

      return GridView(
        padding: const EdgeInsets.only(top: 10, bottom: 20, left: 5, right: 5),
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: mainAxisSpacing,
          crossAxisSpacing: crossAxisSpacing,
          childAspectRatio: childAspectRatio,
        ),
        children: moveEvents,
      );
    },
  );
}

Widget buildContainerCard(
  BuildContext context,
  String menuName,
  Function() navFunction,
  Color color,
  String eventStatus,
) {
  return BaseMoveEventSelect(
    color: color,
    onPressed: navFunction,
    child: Center(
      child: Text(
        eventStatus,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: PlatformSizing().superSmallTextSize(context),
          color: BrandingColors.cadiBlack,
        ),
      ),
    ),
  );
}
