import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/cargo_data/container_data.dart';
import 'package:cadi_core_app/redux/models/image/image_create.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'road_movement_freight_search_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

class RoadMovementFreightSearchScreenBuilder extends StatelessWidget {
  const RoadMovementFreightSearchScreenBuilder({super.key});

  static const String route =
      route_helper.roadMovementFreightSearchScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(context, "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, RoadMovementFreightSearchVM>(
        converter: RoadMovementFreightSearchVM.fromStore,
        builder: (context, vm) {
          return RoadMovementFreightSearchPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class RoadMovementFreightSearchVM {
  bool settingsAutoDisplayNextDropdownOrInput;
  bool settingsAutoSubmitPage;
  bool isLoading;
  AuthState authState;
  Account account;
  RoadMovementCreate roadMovementCreate;
  String roadMovementRole;
  UniversalBranding universalBranding;
  List<ImageCreate>? attachedDocuments;
  List<AccountDocumentType> documentTypes;
  List<CadiEnum> roadMovementCargoTypes;

  UiRoadMovementSearch uiRoadMovementSearch;
  final Function(BuildContext context, String message) showErrorAlert;
  final Function(
      BuildContext context,
      String waybill,
      String legNumber,
      String? eta,
      String containerNumber,
      bool isBreakBulk,
      List<ImageCreate>? attachedDocuments) createRoadMovement;

  RoadMovementFreightSearchVM(
      {required this.isLoading,
      required this.settingsAutoDisplayNextDropdownOrInput,
      required this.settingsAutoSubmitPage,
      required this.authState,
      required this.account,
      required this.roadMovementCreate,
      required this.roadMovementRole,
      required this.universalBranding,
      required this.attachedDocuments,
      required this.documentTypes,
      required this.roadMovementCargoTypes,
      required this.uiRoadMovementSearch,
      required this.showErrorAlert,
      required this.createRoadMovement});

  static RoadMovementFreightSearchVM fromStore(Store<AppState> store) {
    return RoadMovementFreightSearchVM(
        isLoading: store.state.isLoading,
        authState: store.state.authState,
        settingsAutoDisplayNextDropdownOrInput:
            store.state.uiState.uiDropDownOrInputAutoContinue,
        settingsAutoSubmitPage: store.state.uiState.uiAutoSubmitPage,
        account: store.state.accountState.account,
        roadMovementRole: store.state.uiState.roadMovementRole,
        universalBranding:
            UniversalBranding(store.state.accountState.activeService),
        attachedDocuments:
            store.state.roadMovementState.roadMovementCreate.documents,
        documentTypes: store.state.accountState.filteredAccountDocumentTypes,
        roadMovementCargoTypes:
            store.state.referenceState.roadMovementCargoTypes,
        roadMovementCreate: store.state.roadMovementState.roadMovementCreate,
        uiRoadMovementSearch: store.state.uiState.uiRoadMovementSearch,
        showErrorAlert: (BuildContext context, String message) {
          showOkMessage(context, "Submit Error", message);
        },
        createRoadMovement: (BuildContext context,
            String waybill,
            String legNumber,
            String? eta,
            String containerNumber,
            bool isBreakBulk,
            List<ImageCreate>? attachedDocuments) {
          var formattedWaybill = waybill.replaceAll(' ', '');
          var formattedLegNumber =
              legNumber.replaceAll(' ', '').replaceAll("/", "");
          var containerData = ContainerData.initial()
              .copyWith(containerNumber: containerNumber);
          List<ContainerData> containers = [containerData];

          //Set UI store variables
          store.dispatch(UiRoadMovementSearchSetWaybill(formattedWaybill));
          store.dispatch(UiRoadMovementSearchSetLegNumber(formattedLegNumber));
          store.dispatch(UiRoadMovementSearchSetEta(eta));

          //Set Road Movement Create variables
          store.dispatch(SetRoadFreightVariables(
              formattedWaybill,
              formattedLegNumber,
              eta,
              containers,
              isBreakBulk,
              attachedDocuments));

          store.dispatch(NavigateToAction.push(
              route_helper.roadMovementStatusScreenBuilder));
        });
  }
}
