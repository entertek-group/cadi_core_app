import 'package:cadi_core_app/factory/ui_factory.dart';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/helpers/messages.dart';
import 'package:cadi_core_app/helpers/validation_helper.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/redux/models/account/account_container_type.dart';
import 'package:cadi_core_app/redux/models/cargo_data/container_data.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/app/setting_tile.dart';
import 'package:cadi_core_app/ui/shared/components/base/base_move_event_select.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/base/base_table.dart';
import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'road_movement_container_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';

class RoadMovementContainerPage extends StatefulWidget {
  final RoadMovementContainerVM viewModel;
  const RoadMovementContainerPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<RoadMovementContainerPage> createState() {
    return RoadMovementContainerPageState(viewModel: viewModel);
  }
}

class RoadMovementContainerPageState extends State<RoadMovementContainerPage> {
  final RoadMovementContainerVM viewModel;

  List<ContainerData> containers = <ContainerData>[];
  List<TableRow> documentTableRows = <TableRow>[];

  final _inputContainerNumberController = TextEditingController();
  final _selectContainerTypeController = TextEditingController();

  final FocusNode containerNumberInputNode = FocusNode();

  static const Key _inputContainerNumberKey =
      Key(RoadMovementKeys.inputContainerNumberKey);
  static const Key _selectContainerTypeKey =
      Key(RoadMovementKeys.selectContainerTypeKey);

  List<DropdownItem> _containerTypeItems = [];

  int _selectedContainerTypeIndex = 0;
  bool _overrideContainerFormat = false;
  bool _isBreakBulk = false;
  String _selectedCargoType = CadiRoadMovementCargoType.container;

  RoadMovementContainerPageState({
    Key? key,
    required this.viewModel,
  });

  void _setContainerType(int index, [bool initiatePicker = true]) {
    setState(() {
      _selectContainerTypeController.text = _containerTypeItems[index].text;
      _selectedContainerTypeIndex = index;
    });
  }

  @override
  initState() {
    super.initState();
    _containerTypeItems = UiFactory()
        .convertToDropdown<AccountContainerType, String>(
            widget.viewModel.accountContainerTypes,
            (item) => item.code,
            (item) => item.code);

    _containerTypeItems = UiFactory()
        .convertToDropdown<AccountContainerType, String>(
            widget.viewModel.accountContainerTypes,
            (item) => item.code,
            (item) => item.code);

    int containerTypeIndex = 0;

    if (widget.viewModel.roadMovementCreate.isBreakBulk) {
      setState(() {
        _isBreakBulk = true;
        _selectedCargoType = CadiRoadMovementCargoType.breakBulk;
      });
    }

    if (widget.viewModel.accountContainerTypes.isNotEmpty) {
      var i = 0;
      containerTypeIndex = i < 0 ? 0 : i;
    }
    _setContainerType(containerTypeIndex, false);

    buildTableHeader();

    if (widget.viewModel.containers != null) {
      setState(() {
        containers = widget.viewModel.containers!;
      });
      rebuildTableRows();
    }
  }

  @override
  void dispose() {
    _inputContainerNumberController.dispose();
    _selectContainerTypeController.dispose();

    super.dispose();
  }

  void buildTableHeader() {
    setState(() {
      documentTableRows = <TableRow>[];
      documentTableRows.add(buildTableRow(context, buildTableTitle("#"),
          buildTableTitle("TYPE"), buildTableTitle("ACTION")));
    });
  }

  void showContainerTypeBottomPicker() {
    if (widget.viewModel.accountContainerTypes.isEmpty) {
      showOkMessage(context, "Cannot select Container Type",
          "Account setup is incomplete. Please contact support. ");
    } else {
      showBottomDropdown(
          context,
          'Container Type',
          'Please select Container Type',
          _containerTypeItems,
          _selectedContainerTypeIndex,
          _setContainerType,
          true);
    }
  }

  void addContainer() {
    if (_inputContainerNumberController.text.trim() == "") {
      viewModel.showErrorAlert(context, "Please input a container number");
      return;
    } else if (!_overrideContainerFormat &&
        !isContainerInputValid(
            context, _inputContainerNumberController.text.trim())) {
      showMessageWithActions(
          context,
          "Container Number Format",
          WarningMessages.containerOverrideMessage,
          () {
            setState(() {
              _overrideContainerFormat = true;
            });
            Navigator.pop(context);
            addContainerRow();
          },
          //Add Container
          "Accept",
          () {
            Navigator.pop(context);
          },
          "Cancel");
      return;
    } else {
      addContainerRow();
    }
  }

  void addContainerRow() {
    var newContainer = ContainerData.initial().copyWith(
        containerNumber: _inputContainerNumberController.text.trim(),
        containerCode: _containerTypeItems[_selectedContainerTypeIndex].value);

    setState(() {
      containers.add(newContainer);
      _selectedContainerTypeIndex = 0;
    });
    rebuildTableRows();
    _inputContainerNumberController.text = "";
    _setContainerType(_selectedContainerTypeIndex, false);
  }

  void rebuildTableRows() {
    buildTableHeader();
    for (var container in containers) {
      addDocumentRow(container);
    }
  }

  void addDocumentRow(ContainerData container) {
    setState(() {
      documentTableRows.add(buildTableRow(
          context,
          buildTableTitle(container.containerNumber),
          buildTableTitle(container.containerCode),
          delete(container)));
    });
  }

  TextButton delete(ContainerData container) {
    return TextButton.icon(
        onPressed: () {
          var displayName = container.containerNumber;
          showMessageWithActions(
              context,
              "Remove container #$displayName",
              "Are you sure you want to remove this container?",
              () {
                Navigator.pop(context);
                setState(() {
                  containers.remove(container);
                });
                rebuildTableRows();
              },
              "Yes",
              () {
                Navigator.pop(context);
              },
              "No");
        },
        icon: const Icon(
          FontAwesomeIcons.remove,
          size: 20,
          color: BrandingColors.cadiGreen,
        ),
        label: const Text(""));
  }

  void goNext() {
    if (containers.isEmpty && !_isBreakBulk) {
      viewModel.showErrorAlert(context, "Please add at least 1 container");
      return;
    } else if (_inputContainerNumberController.text.isNotEmpty &&
        !_isBreakBulk) {
      viewModel.showErrorAlert(context,
          "You appear to have input a container number but not yet added it. Please add or remove it before continuing.");
    } else {
      viewModel.setRoadCargoContainerVariables(
          context, containers, _isBreakBulk);
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> moveEventWidgets = <Widget>[];
    for (var cadiEnum in widget.viewModel.roadMovementCargoType) {
      var color = _selectedCargoType != cadiEnum.id
          ? BrandingColors.white
          : BrandingColors.cadiGrey;

      moveEventWidgets.add(buildContainerCard(
        context,
        "",
        () => {
          setState(() {
            if (cadiEnum.id == CadiRoadMovementCargoType.breakBulk) {
              _isBreakBulk = true;
            } else {
              _isBreakBulk = false;
            }
            _selectedCargoType = cadiEnum.id;
          }),
        },
        color,
        cadiEnum.description,
      ));
      // index += 1;
    }

    List<SettingTile> allOptions = <SettingTile>[];
    allOptions.addAll([
      SettingTile(
        0,
        'Active Account',
        Column(
          children: <Widget>[
            Text(
                "Road Movement: ${getRoadMovementRoleDescription(widget.viewModel.roadMovementRole)}",
                style: TextStyle(
                    fontSize: PlatformSizing().normalTextSize(context),
                    color: BrandingColors.cadiBlack)),
            // Any additional widget/codes
          ],
        ),
      ),
      SettingTile(
        1,
        'Account Operations',
        Container(
          margin: const EdgeInsets.only(
              top: 10.0, bottom: 15), // Add your own value here
          child: Column(
            children: <Widget>[
              Text("Enter Container details",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      fontWeight: FontWeight.w300,
                      color: BrandingColors.cadiBlack)),
            ],
          ),
        ),
      ),
      SettingTile(
          2,
          "CARGO TYPE",
          Material(
              color: BrandingColors.cadiWhite,
              child: buildMoveEventList(context, moveEventWidgets))),
      SettingTile(
          2,
          'CONTAINER NUMBER',
          !_isBreakBulk
              ? Material(
                  color: BrandingColors.cadiWhite,
                  child: ListTile(
                      title: Text("Container Number",
                          style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize:
                                  PlatformSizing().mediumTextSize(context),
                              color: BrandingColors.cadiBlack))),
                )
              : Container()),
      SettingTile(
          3,
          '',
          !_isBreakBulk
              ? PlatformTextField(
                  cursorColor: BrandingColors.cadiGrey,
                  focusNode: containerNumberInputNode,
                  textCapitalization: TextCapitalization.characters,
                  controller: _inputContainerNumberController,
                  key: _inputContainerNumberKey,
                  autocorrect: false,
                  material: (_, __) => MaterialAssetsDark()
                      .textFieldData("", FontAwesomeIcons.search),
                  cupertino: (_, __) =>
                      CupertinoAssets().textFieldData("Container Number", null),
                  style: TextStyle(
                      color: BrandingColors.cadiBlack,
                      fontSize: PlatformSizing().inputTextFieldSize(context)),
                  //maxLength: 11,
                  keyboardType: TextInputType.text,
                  onEditingComplete: () {
                    containerNumberInputNode.nextFocus();
                  },
                )
              : Container()),
      SettingTile(
          4,
          'CONTAINER TYPE',
          !_isBreakBulk
              ? Material(
                  color: BrandingColors.cadiWhite,
                  child: ListTile(
                      title: Text("Container Type",
                          style: TextStyle(
                              fontStyle: FontStyle.normal,
                              fontSize:
                                  PlatformSizing().mediumTextSize(context),
                              color: BrandingColors.cadiBlack))),
                )
              : Container()),
      SettingTile(
          5,
          "",
          !_isBreakBulk
              ? Material(
                  color: BrandingColors.cadiWhite,
                  child: InkWell(
                    onTap: () {
                      showContainerTypeBottomPicker();
                    },
                    child: PlatformTextField(
                      cursorColor: BrandingColors.cadiGrey,
                      textCapitalization: TextCapitalization.characters,
                      controller: _selectContainerTypeController,
                      key: _selectContainerTypeKey,
                      autocorrect: false,
                      enabled: false,
                      material: (_, __) => MaterialAssetsDark()
                          .textFieldData("", FontAwesomeIcons.chevronDown),
                      cupertino: (_, __) => CupertinoAssets().textFieldData(
                          'Container Type', FontAwesomeIcons.chevronDown),
                      style: TextStyle(
                          color: BrandingColors.cadiBlack,
                          fontWeight: FontWeight.w400,
                          fontSize:
                              PlatformSizing().inputTextFieldSize(context)),
                      keyboardType: TextInputType.text,
                    ),
                  ))
              : Container()),
      const SettingTile(
          6,
          '',
          Material(
              color: BrandingColors.cadiWhite,
              child: ListTile(
                title: Text(''),
              ))),
      SettingTile(
        7,
        '',
        !_isBreakBulk
            ? Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 50.0, vertical: 0.0),
                child: BaseGhostButton(
                  onPressed: () {
                    addContainer();
                  },
                  textColor: BrandingColors.cadiBlack,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Icon(
                        FontAwesomeIcons.plus,
                        size: 20,
                        color: BrandingColors.cadiBlack,
                      ),
                      const SizedBox(width: 8),
                      Text(
                        'Save Container',
                        style: TextStyle(
                          color: BrandingColors.cadiBlack,
                          fontSize: PlatformSizing().mediumTextSize(context),
                        ),
                      ),
                    ],
                  ), // Pass the text color here
                ),
              )
            : Container(),
      ),
      SettingTile(
          8,
          'Summary',
          !_isBreakBulk
              ? Material(
                  color: BrandingColors.background,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20, left: 0),
                    child: buildScrollableDynamicTable(
                        buildSummaryTable(context, documentTableRows)),
                  ))
              : Container()),
      SettingTile(
          15,
          '',
          !_isBreakBulk
              ? const Material(
                  color: BrandingColors.cadiWhite,
                  child: ListTile(
                    title: Text(''),
                  ))
              : Container()),
      SettingTile(
          12,
          '',
          Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 50.0, vertical: 0.0),
              child: BaseButton(
                  color: widget.viewModel.universalBranding
                      .getButtonPrimaryColor(),
                  child: Text(
                    'Next',
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: PlatformSizing().normalTextSize(context)),
                  ),
                  onPressed: () {
                    goNext();
                  })))
    ]);

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          itemCount: allOptions.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          itemBuilder: (BuildContext context, int index) {
            return allOptions[index].body;
          }),
    );
  }
}

Widget buildMoveEventList(BuildContext context, List<Widget> moveEvents) {
  return LayoutBuilder(
    builder: (context, constraints) {
      double mainAxisSpacing = constraints.maxWidth > 600
          ? 20
          : 10; // Adjust spacing based on screen width
      double crossAxisSpacing = constraints.maxWidth > 600
          ? 20
          : 10; // Adjust spacing based on screen width
      double childAspectRatio = constraints.maxWidth > 600
          ? 6
          : 2.8; // Adjust aspect ratio based on screen width

      return GridView(
        padding: const EdgeInsets.only(top: 10, bottom: 20, left: 5, right: 5),
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: mainAxisSpacing,
          crossAxisSpacing: crossAxisSpacing,
          childAspectRatio: childAspectRatio,
        ),
        children: moveEvents,
      );
    },
  );
}

Widget buildContainerCard(
  BuildContext context,
  String menuName,
  Function() navFunction,
  Color color,
  String eventStatus,
) {
  return BaseMoveEventSelect(
    color: color,
    onPressed: navFunction,
    child: Center(
      child: Text(
        eventStatus,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: PlatformSizing().superSmallTextSize(context),
          color: BrandingColors.cadiBlack,
        ),
      ),
    ),
  );
}
