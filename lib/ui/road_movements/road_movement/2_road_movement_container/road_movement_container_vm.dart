import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/cargo_data/container_data.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/theme/theme.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:redux/redux.dart';
import 'road_movement_container_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class RoadMovementContainerScreenBuilder extends StatelessWidget {
  const RoadMovementContainerScreenBuilder({super.key});

  static const String route = route_helper.roadMovementContainerScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(
                  // FontAwesomeIcons.warning,
                  context,
                  "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, RoadMovementContainerVM>(
        converter: RoadMovementContainerVM.fromStore,
        builder: (context, vm) {
          return RoadMovementContainerPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class RoadMovementContainerVM {
  bool settingsAutoDisplayNextDropdownOrInput;
  bool settingsAutoSubmitPage;
  bool isLoading;
  AuthState authState;
  Account account;
  RoadMovementCreate roadMovementCreate;
  UniversalBranding universalBranding;
  String roadMovementRole;
  List<CadiEnum> roadMovementCargoType;
  List<AccountContainerType> accountContainerTypes;
  List<CadiGeneric> cargoTypes;
  List<ContainerData>? containers;

  final Function(BuildContext, String) onNavigatePressed;
  final Function(String token) setToken;
  final Function(
      BuildContext context,
      List<ContainerData>? containers,
      bool isBreakBulk) setRoadCargoContainerVariables;
  final Function(BuildContext context, String message) showErrorAlert;
  RoadMovementContainerVM ({
    required this.settingsAutoDisplayNextDropdownOrInput,
    required this.settingsAutoSubmitPage,
    required this.isLoading,
    required this.authState,
    required this.account,
    required this.roadMovementRole,
    required this.roadMovementCargoType,
    required this.roadMovementCreate,
    required this.cargoTypes,
    required this.containers,
    required this.universalBranding,
    required this.accountContainerTypes,
    required this.onNavigatePressed,
    required this.setToken,
    required this.setRoadCargoContainerVariables,
    required this.showErrorAlert,
  });

  static RoadMovementContainerVM fromStore(Store<AppState> store) {
    return RoadMovementContainerVM(
        isLoading: store.state.isLoading,
        authState: store.state.authState,
        settingsAutoDisplayNextDropdownOrInput:
            store.state.uiState.uiDropDownOrInputAutoContinue,
        settingsAutoSubmitPage: store.state.uiState.uiAutoSubmitPage,
        account: store.state.accountState.account,
        cargoTypes: store.state.referenceState.cargoTypes,
        containers: store.state.roadMovementState.roadMovementCreate.containerData,
        roadMovementRole: store.state.uiState.roadMovementRole,
        roadMovementCargoType: store.state.referenceState.roadMovementCargoTypes,
        roadMovementCreate: store.state.roadMovementState.roadMovementCreate,
        universalBranding:
            UniversalBranding(store.state.accountState.activeService),
        showErrorAlert: (BuildContext context, String message) {
          showOkMessage(
              context,
              "Submit Error",
              message);
        },
        accountContainerTypes:
            store.state.accountState.filteredAccountContainerTypes,
        onNavigatePressed: (BuildContext context, String route) {
          if (store.state.isLoading) {
            return;
          }
          navigation_helper.navigateRoute(route, store, null);
        },
        setToken: (String token) {},
        setRoadCargoContainerVariables: (
          BuildContext context,
            List<ContainerData>? containers,
            bool isBreakBulk
        ) {
          store.dispatch(SetRoadContainerVariables(containers, isBreakBulk));
          navigation_helper.navigateRoute(
              route_helper.roadMovementStatusScreenBuilder, store, null);
        });
  }
}
