import 'dart:convert';

import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/redux/models/image/image_create.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/app/setting_tile.dart';
import 'package:cadi_core_app/ui/shared/components/base/base_move_event_select.dart';
import 'package:cadi_core_app/ui/shared/components/base/base_table.dart';
import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'road_movement_status_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class RoadMovementStatusPage extends StatefulWidget {
  final RoadMovementStatusVM viewModel;
  const RoadMovementStatusPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<RoadMovementStatusPage> createState() {
    return RoadMovementStatusPageState(viewModel: viewModel);
  }
}

class RoadMovementStatusPageState extends State<RoadMovementStatusPage> {
  final RoadMovementStatusVM viewModel;

  final _commentController = TextEditingController();

  String _selectedStatus = "";
  bool _commentRequired = false;

  List<TableRow> imageTableRows = <TableRow>[];
  List<ImageCreate> attachedImages = <ImageCreate>[];

  static const Key _commentControllerKey = Key(RoadMovementKeys.commentKey);

  RoadMovementStatusPageState({
    Key? key,
    required this.viewModel,
  });

  @override
  initState() {
    super.initState();
    if (widget.viewModel.roadMovementCreate != RoadMovementCreate.initial() &&
        widget.viewModel.roadMovementCreate.state != "") {
      setState(() {
        _selectedStatus = widget.viewModel.roadMovementCreate.state;
        _commentController.text = widget.viewModel.roadMovementCreate.comment;
      });
    }

    if (widget.viewModel.attachedImages != null &&
        widget.viewModel.attachedImages!.isNotEmpty) {
      attachedImages = widget.viewModel.attachedImages!;
      rebuildTableRows();
    }
  }

  @override
  void dispose() {
    _commentController.dispose();
    super.dispose();
  }

  void rebuildTableRows() {
    setState(() {
      imageTableRows = <TableRow>[];
    });
    for (var attachedImage in attachedImages) {
      addImageRow(attachedImage);
    }
  }

  void _imagePicker() {
    var dropDownItems = <DropdownItem>[];
    dropDownItems.add(const DropdownItem("Camera", 0, ImageSource.camera));
    dropDownItems.add(const DropdownItem("Gallery", 0, ImageSource.gallery));

    showBottomDropdown(
        context,
        'Image',
        'Where would you like to retrieve the images from?',
        dropDownItems,
        0,
        _processImage,
        false);
  }

  Future<void> _processImage(ImageSource imageSource) async {
    final ImagePicker picker = ImagePicker();
    XFile? image = await picker.pickImage(source: imageSource);
    if (image != null) {
      setImage(image);
    }
  }

  void setImage(XFile? image) {
    if (image != null) {
      final bytes = File(image.path).readAsBytesSync();
      String name = image.name;
      String base64Content = base64Encode(bytes);
      ImageCreate imageCreate = ImageCreate(
        name: name,
        base64Content: base64Content,
        kind: null,
      );

      setState(() {
        attachedImages.add(imageCreate);
        addImageRow(imageCreate);
      });
    }
  }

  void addImageRow(ImageCreate attachedImage) {
    var displayName = imageDisplayName(attachedImage.name);
    setState(() {
      imageTableRows.add(buildTableRow(context, buildTableTitle(displayName),
          buildTableTitle(""), delete(attachedImage)));
    });
  }

  String imageDisplayName(String imageName) {
    return imageName.length > 6
        ? imageName.substring(imageName.length - 5)
        : imageName;
  }

  TextButton delete(ImageCreate imageCreate) {
    return TextButton.icon(
        onPressed: () {
          var displayName = imageDisplayName(imageCreate.name);
          showMessageWithActions(
              context,
              "Remove image #$displayName",
              "Are you sure you want to remove this image?",
              () {
                Navigator.pop(context);
                setState(() {
                  attachedImages.remove(imageCreate);
                });
                rebuildTableRows();
              },
              "Yes",
              () {
                Navigator.pop(context);
              },
              "No");
        },
        icon: const Icon(
          FontAwesomeIcons.remove,
          size: 20,
          color: BrandingColors.cadiGreen,
        ),
        label: const Text(""));
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> moveEventWidgets = <Widget>[];
    for (var cadiEnum in widget.viewModel.roadCargoMovementStates) {
      var color = _selectedStatus != cadiEnum.id
          ? BrandingColors.white
          : BrandingColors.cadiGrey;

      moveEventWidgets.add(buildContainerCard(
        context,
        "",
        () => {
          setState(() {
            _selectedStatus = cadiEnum.id;
            _commentRequired =
                _selectedStatus != CadiCargoMovementStatus.goodOrder;
          }),
          // setStatus(),
        },
        color,
        cadiEnum.description,
      ));
    }

    List<SettingTile> allOptions = <SettingTile>[
      SettingTile(
        0,
        'Active Account',
        Container(
          child: Column(
            children: <Widget>[
              Text(
                  "Road Movement: ${getRoadMovementRoleDescription(widget.viewModel.roadMovementRole)}",
                  style: TextStyle(
                      fontSize: PlatformSizing().normalTextSize(context),
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),
      SettingTile(
        1,
        'Account Operations',
        Container(
          margin: const EdgeInsets.only(
              top: 10.0, bottom: 15), // Add your own value here
          child: Column(
            children: <Widget>[
              Text("Complete Status Information",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      fontWeight: FontWeight.w300,
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),
      SettingTile(
          2,
          "Container List",
          Material(
              color: BrandingColors.background,
              child: buildMoveEventList(context, moveEventWidgets))),
      SettingTile(
          3,
          'COMMENTS',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text('Enter additional comments:',
                    style: TextStyle(
                        fontSize: PlatformSizing().smallTextSize(context),
                        fontWeight: FontWeight.w300,
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(
          4,
          'comment',
          Padding(
              padding:
                  const EdgeInsets.only(top: 0, bottom: 20, left: 5, right: 5),
              child: PlatformTextField(
                cursorColor: BrandingColors.cadiGrey,
                controller: _commentController,
                key: _commentControllerKey,
                maxLines: 5,
                hintText: "* Enter additional comments",
                material: (_, __) => CommentBox().textFieldData(''),
                cupertino: (_, __) =>
                    CupertinoAssets().textFieldData('Add Comments', null),
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    color: BrandingColors.cadiBlack,
                    fontSize: PlatformSizing().inputTextFieldSize(context)),
                keyboardType: TextInputType.text,
              ))),
      SettingTile(
        5,
        '',
        Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              BaseGhostButton(
                onPressed: () {
                  _imagePicker();
                },
                textColor: BrandingColors.cadiBlack,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Icon(
                      FontAwesomeIcons.paperclip,
                      size: 20,
                      color: BrandingColors.cadiBlack,
                    ),
                    const SizedBox(width: 8),
                    Text(
                      'Add Photos',
                      style: TextStyle(
                        color: BrandingColors
                            .cadiBlack, // Set your desired text color here
                        fontSize: PlatformSizing().mediumTextSize(context),
                      ),
                    ),
                  ],
                ), // Pass the text color here
              ),
              if (attachedImages.isNotEmpty)
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Text(
                    '${attachedImages.length} Image(s) Attached',
                    style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      color: BrandingColors.cadiBlackOpacity,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              if (attachedImages.isNotEmpty)
                Material(
                    color: BrandingColors.background,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20, left: 10),
                      child: buildScrollableDynamicTable(
                          buildSummaryTable(context, imageTableRows)),
                    ))
            ],
          ),
        ),
      ),
      const SettingTile(
          6,
          '',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text(''),
              ))),
      SettingTile(
          7,
          '',
          Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 50.0, vertical: 0.0),
              child: BaseButton(
                  color: widget.viewModel.universalBranding
                      .getButtonPrimaryColor(),
                  child: Text(
                    'Next',
                    style: TextStyle(
                        fontSize: PlatformSizing().normalTextSize(context)),
                  ),
                  onPressed: () {
                    if (_commentRequired &&
                        _commentController.text.trim() == "") {
                      viewModel.showErrorAlert(
                          context, "Comment is required for selected status.");
                    } else if (_selectedStatus == "") {
                      viewModel.showErrorAlert(
                          context, "Please select a status.");
                    } else {
                      viewModel.createRoadMovement(context, _selectedStatus,
                          _commentController.text, attachedImages);
                    }
                  }))),
    ];

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          itemCount: allOptions.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          itemBuilder: (BuildContext context, int index) {
            return allOptions[index].body;
          }),
    );
  }
}

Widget buildMoveEventList(BuildContext context, List<Widget> moveEvents) {
  return LayoutBuilder(
    builder: (context, constraints) {
      double mainAxisSpacing = constraints.maxWidth > 600 ? 20 : 10; // Adjust spacing based on screen width
      double crossAxisSpacing = constraints.maxWidth > 600 ? 20 : 10; // Adjust spacing based on screen width
      double childAspectRatio = constraints.maxWidth > 600 ? 6 : 2.8; // Adjust aspect ratio based on screen width

      return GridView(
        padding: const EdgeInsets.only(top: 10, bottom: 20, left: 5, right: 5),
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: mainAxisSpacing,
          crossAxisSpacing: crossAxisSpacing,
          childAspectRatio: childAspectRatio,
        ),
        children: moveEvents,
      );
    },
  );
}

Widget buildContainerCard(
  BuildContext context,
  String menuName,
  Function() navFunction,
  Color color,
  String eventStatus,
) {
  return BaseMoveEventSelect(
    color: color,
    onPressed: navFunction,
    child: Center(
      child: Text(
        eventStatus,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: PlatformSizing().superSmallTextSize(context),
          color: BrandingColors.cadiBlack,
        ),
      ),
    ),
  );
}
