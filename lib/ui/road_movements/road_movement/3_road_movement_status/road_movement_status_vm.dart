import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/image/image_create.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'road_movement_status_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class RoadMovementStatusScreenBuilder extends StatelessWidget {
  const RoadMovementStatusScreenBuilder({super.key});

  static const String route = route_helper.roadMovementStatusScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(context, "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, RoadMovementStatusVM>(
        converter: RoadMovementStatusVM.fromStore,
        builder: (context, vm) {
          return RoadMovementStatusPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class RoadMovementStatusVM {
  bool isLoading;
  AuthState authState;
  Account account;
  String roadMovementRole;
  RoadMovementCreate roadMovementCreate;
  UniversalBranding universalBranding;
  List<CadiEnum> roadCargoMovementStates;
  List<ImageCreate>? attachedImages;

  final Function(BuildContext, String) onNavigatePressed;
  final Function(String token) setToken;
  final Function(BuildContext context, String message) showErrorAlert;
  final Function(BuildContext context, String status, String inputComment,
      List<ImageCreate>? attachedImages) createRoadMovement;

  RoadMovementStatusVM(
      {
        required this.isLoading,
        required this.authState,
        required this.account,
        required this.roadMovementRole,
        required this.roadMovementCreate,
        required this.universalBranding,
        required this.roadCargoMovementStates,
        required this.attachedImages,
        required this.onNavigatePressed,
        required this.setToken,
        required this.showErrorAlert,
        required this.createRoadMovement,
      });

  static RoadMovementStatusVM fromStore(Store<AppState> store) {
    return RoadMovementStatusVM(
        isLoading: store.state.isLoading,
        authState: store.state.authState,
        account: store.state.accountState.account,
        roadMovementRole: store.state.uiState.roadMovementRole,
        roadMovementCreate: store.state.roadMovementState.roadMovementCreate,
        universalBranding:
            UniversalBranding(store.state.accountState.activeService),
        roadCargoMovementStates:
            store.state.referenceState.roadCargoMovementStates,
        attachedImages: store.state.roadMovementState.roadMovementCreate.images,
        onNavigatePressed: (BuildContext context, String route) {
          if (store.state.isLoading) {
            return;
          }
          navigation_helper.navigateRoute(route, store, null);
        },
        setToken: (String token) {},
        showErrorAlert: (BuildContext context, String message) {
          showOkMessage(
              // FontAwesomeIcons.warning,
              context,
              "Submit Error",
              message);
        },
        createRoadMovement: (BuildContext context, String status, String inputComment,
            List<ImageCreate>? attachedImages) {
          store.dispatch(
              SetRoadStatusVariables(status, inputComment, attachedImages));
          //if (store.state.uiState.roadMovementRole == CadiRoadMovementRole.delivery) {
            store.dispatch(NavigateToAction(route_helper.roadMovementSignatureScreenBuilder));
          // } else {
          //     store.dispatch(NavigateToAction(_route_helper.roadMovementSummaryScreenBuilder));
          // }
        });
  }
}
