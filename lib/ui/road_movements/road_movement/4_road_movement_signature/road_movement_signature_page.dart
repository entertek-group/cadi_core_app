import 'dart:io';

import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/keys/keys.dart';
import 'package:cadi_core_app/redux/models/image/image_create.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/base/base_table.dart';
import 'package:cadi_core_app/ui/shared/components/bottom_dropdown.dart';
import 'package:cadi_core_app/ui/shared/components/setting_tile.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'road_movement_signature_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';
// IMPORT PACKAGE
import 'package:signature/signature.dart';
import 'dart:convert';

class RoadMovementSignaturePage extends StatefulWidget {
  final RoadMovementSignatureVM viewModel;
  const RoadMovementSignaturePage({
    super.key,
    required this.viewModel,
  });

  @override
  State<RoadMovementSignaturePage> createState() {
    return RoadMovementSignaturePageState(viewModel: viewModel);
  }
}

class RoadMovementSignaturePageState extends State<RoadMovementSignaturePage> {
  final RoadMovementSignatureVM viewModel;
  final SignatureController _controller = SignatureController(
    penStrokeWidth: 5,
    penColor: BrandingColors.cadiBlack,
    exportBackgroundColor: BrandingColors.cadiWhite,
  );

  final _inputSignatoryFullNameController = TextEditingController();

  static const Key _inputSignatoryFullNameKey =
      Key(RoadMovementKeys.inputSignatoryFullNameKey);

  List<TableRow> documentTableRows = <TableRow>[];
  List<ImageCreate> attachedDocuments = <ImageCreate>[];
  String documentTypeCode = "";

  RoadMovementSignaturePageState({
    Key? key,
    required this.viewModel,
  });

  @override
  initState() {
    super.initState();
    if (widget.viewModel.roadMovementCreate.signatoryFullName != null) {
      _inputSignatoryFullNameController.text =
          widget.viewModel.roadMovementCreate.signatoryFullName!;
    }

    buildTableHeader();

    if (widget.viewModel.attachedDocuments != null &&
        widget.viewModel.attachedDocuments!.isNotEmpty) {
      attachedDocuments = widget.viewModel.attachedDocuments!;
      rebuildTableRows();
    }
  }

  @override
  void dispose() {
    super.dispose();
    _inputSignatoryFullNameController.dispose();
  }

  void addImage() {
    var dropDownItem = <DropdownItem>[];
    for (var element in widget.viewModel.documentTypes) {
      dropDownItem.add(DropdownItem(element.description, 0, element.code));
    }

    showBottomDropdown(
        context,
        'Document Type',
        'What document type do you want to upload?',
        dropDownItem,
        0,
        _imagePicker,
        false);
  }

  void _imagePicker(String documentType) {
    setState(() {
      documentTypeCode = documentType;
    });

    var dropDownItems = <DropdownItem>[];
    dropDownItems.add(const DropdownItem("Camera", 0, ImageSource.camera));
    dropDownItems.add(const DropdownItem("Gallery", 0, ImageSource.gallery));

    if (Platform.isAndroid) {
      /*
       Showing
        */
      Navigator.pop(context, "");
      showBottomDropdown(
          context,
          'Document',
          'Where would you like to retrieve the document from?',
          dropDownItems,
          0,
          _processImage,
          false);
    }

    showBottomDropdown(
        context,
        'Document',
        'Where would you like to retrieve the document from?',
        dropDownItems,
        0,
        _processImage,
        false);
  }

  void buildTableHeader() {
    setState(() {
      documentTableRows = <TableRow>[];
      documentTableRows.add(buildTableRow(context, buildTableTitle("DOCUMENT"),
          buildTableTitle("TYPE"), buildTableTitle("ACTION")));
    });
  }

  void rebuildTableRows() {
    buildTableHeader();
    for (var attachedDocument in attachedDocuments) {
      addDocumentRow(attachedDocument);
    }
  }

  void addDocumentRow(ImageCreate attachedImage) {
    var displayName = imageDisplayName(attachedImage.name);
    setState(() {
      documentTableRows.add(buildTableRow(context, buildTableTitle(displayName),
          buildTableTitle(attachedImage.kind ?? ""), delete(attachedImage)));
    });
  }

  String imageDisplayName(String imageName) {
    return imageName.length > 6
        ? imageName.substring(imageName.length - 5)
        : imageName;
  }

  Future<void> _processImage(ImageSource imageSource) async {
    final ImagePicker picker = ImagePicker();
    XFile? image = await picker.pickImage(source: imageSource);
    if (image != null) {
      setImage(image);
    }
  }

  void setImage(XFile? image) {
    if (image != null) {
      final bytes = File(image.path).readAsBytesSync();
      String name = image.name;
      String base64Content = base64Encode(bytes);
      ImageCreate imageCreate = ImageCreate(
          name: name, base64Content: base64Content, kind: documentTypeCode);

      setState(() {
        attachedDocuments.add(imageCreate);
        addDocumentRow(imageCreate);
      });
    }
  }

  TextButton delete(ImageCreate imageCreate) {
    return TextButton.icon(
        onPressed: () {
          var displayName = imageDisplayName(imageCreate.name);
          showMessageWithActions(
              context,
              "Remove document #$displayName",
              "Are you sure you want to remove this document?",
              () {
                Navigator.pop(context);
                setState(() {
                  attachedDocuments.remove(imageCreate);
                });
                rebuildTableRows();
              },
              "Yes",
              () {
                Navigator.pop(context);
              },
              "No");
        },
        icon: const Icon(
          FontAwesomeIcons.xmark,
          size: 20,
          color: BrandingColors.cadiGreen,
        ),
        label: const Text(""));
  }

  void goNext(BuildContext context) async {
    if (_inputSignatoryFullNameController.text.trim() == "") {
      viewModel.showErrorAlert(context, "Please add signatory full name");
      return; // Prevent further execution
    } else if (_controller.isEmpty &&
        widget.viewModel.roadMovementCreate.signature == null) {
      viewModel.showErrorAlert(context, "Please provide signature");
      return;
    }

    var bytes = await _controller.toPngBytes();
    if (bytes != null) {
      String base64Content = base64Encode(bytes.toList());
      String name =
          '${_inputSignatoryFullNameController.text.replaceAll(" ", "").trim()}_SIGNATURE.png';
      ImageCreate imageCreate = ImageCreate(
          name: name, base64Content: base64Content, kind: "SIGNATURE");
      if (context.mounted) {
        viewModel.onNavigatePressed(
            context,
            _inputSignatoryFullNameController.text.trim(),
            imageCreate,
            attachedDocuments);
      }
    } else {
      if (context.mounted) {
        viewModel.showErrorAlert(
            context, "Please try again. Signature not captured");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;

    var signatureCanvas = Container(
      decoration: BoxDecoration(
        border: Border.all(color: BrandingColors.cadiBlack.withOpacity(0.2)),
        color: Colors.white,
      ),
      child: ClipRect(
        child: Signature(
          controller: _controller,
          width: screenWidth,
          height: screenHeight * 0.35,
          backgroundColor: Colors.white,
        ),
      ),
    );

    List<SettingTile> allOptions = <SettingTile>[
      SettingTile(
        0,
        'Active Account',
        Column(
          children: <Widget>[
            Text(
                "Road Movement: ${getRoadMovementRoleDescription(widget.viewModel.roadMovementRole)}",
                style: TextStyle(
                    fontSize: PlatformSizing().normalTextSize(context),
                    color: BrandingColors.cadiBlack)),
            // Any additional widget/codes
          ],
        ),
      ),
      SettingTile(
        1,
        'Account Operations',
        Container(
          margin: const EdgeInsets.only(
              top: 10.0, bottom: 15), // Add your own value here
          child: Column(
            children: <Widget>[
              Text("Complete delivery signatory",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: PlatformSizing().smallTextSize(context),
                      fontWeight: FontWeight.w300,
                      color: BrandingColors.cadiBlack)),
              // Any additional widget/codes
            ],
          ),
        ),
      ),
      SettingTile(
          2,
          'Signatory',
          Material(
              color: BrandingColors.cadiWhite,
              child: ListTile(
                title: Text('Full Name',
                    style: TextStyle(
                        fontSize: PlatformSizing().mediumTextSize(context),
                        fontWeight: FontWeight.w300,
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(
          3,
          '',
          PlatformTextField(
            cursorColor: BrandingColors.cadiGrey,
            textCapitalization: TextCapitalization.characters,
            controller: _inputSignatoryFullNameController,
            key: _inputSignatoryFullNameKey,
            autocorrect: false,
            material: (_, __) =>
                MaterialAssetsDark().textFieldData("", FontAwesomeIcons.magnifyingGlass),
            cupertino: (_, __) =>
                CupertinoAssets().textFieldData("Full Name", null),
            style: TextStyle(
                color: BrandingColors.cadiBlack,
                fontSize: PlatformSizing().inputTextFieldSize(context)),
            //maxLength: 11,
            keyboardType: TextInputType.text,
            onEditingComplete: () {},
          )),
      SettingTile(
          3,
          'Signature',
          Material(
              color: BrandingColors.cadiWhite,
              child: ListTile(
                title: Text('Signature',
                    style: TextStyle(
                        fontSize: PlatformSizing().mediumTextSize(context),
                        fontWeight: FontWeight.w300,
                        color: BrandingColors.cadiBlack)),
              ))),
      SettingTile(4, 'Signature', signatureCanvas),
      SettingTile(
        5,
        '',
        Material(
          color: BrandingColors.background,
          child: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                BaseGhostButton(
                  onPressed: () {
                    addImage();
                  },
                  textColor: BrandingColors.cadiBlack,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Icon(
                        FontAwesomeIcons.paperclip,
                        size: 20,
                        color: BrandingColors.cadiBlack,
                      ),
                      const SizedBox(width: 8),
                      Text(
                        'Add',
                        style: TextStyle(
                          color: BrandingColors
                              .cadiBlack, // Set your desired text color here
                          fontSize: PlatformSizing().mediumTextSize(context),
                        ),
                      ),
                    ],
                  ), // Pass the text color here
                ),
                BaseGhostButton(
                  onPressed: () {
                    _controller.clear();
                  },
                  textColor: BrandingColors.cadiBlack,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Icon(
                        FontAwesomeIcons.trash,
                        size: 20,
                        color: BrandingColors.cadiBlack,
                      ),
                      const SizedBox(width: 8),
                      Text(
                        'Clear',
                        style: TextStyle(
                          color: BrandingColors
                              .cadiBlack, // Set your desired text color here
                          fontSize: PlatformSizing().mediumTextSize(context),
                        ),
                      ),
                    ],
                  ), // Pass the text color here
                ),
              ],
            ),
          ),
        ),
      ),
      SettingTile(
          6,
          'Summary',
          Material(
              color: BrandingColors.background,
              child: Padding(
                padding: const EdgeInsets.only(top: 20, left: 10),
                child: buildScrollableDynamicTable(
                    buildSummaryTable(context, documentTableRows)),
              ))),
      const SettingTile(
          7,
          '',
          Material(
              color: BrandingColors.background,
              child: ListTile(
                title: Text(''),
              ))),
      SettingTile(
          8,
          '',
          Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 50.0, vertical: 0.0),
              child: BaseButton(
                  color: widget.viewModel.universalBranding
                      .getButtonPrimaryColor(),
                  child: Text('Next',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: PlatformSizing().normalTextSize(context))),
                  onPressed: () {
                    goNext(context);
                  }))),
    ];

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
          itemCount: allOptions.length,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          itemBuilder: (BuildContext context, int index) {
            return allOptions[index].body;
          }),
    );
  }
}
