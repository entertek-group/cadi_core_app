import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/image/image_create.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'road_movement_signature_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

class RoadMovementSignatureScreenBuilder extends StatelessWidget {
  const RoadMovementSignatureScreenBuilder({super.key});

  static const String route = route_helper.roadMovementSignatureScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(
                  // FontAwesomeIcons.warning,
                  context,
                  "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, RoadMovementSignatureVM>(
        converter: RoadMovementSignatureVM.fromStore,
        builder: (context, vm) {
          return RoadMovementSignaturePage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class RoadMovementSignatureVM {
  bool settingsAutoDisplayNextDropdownOrInput;
  bool settingsAutoSubmitPage;
  bool isLoading;
  AuthState authState;
  Account account;
  String roadMovementRole;
  RoadMovementCreate roadMovementCreate;
  UniversalBranding universalBranding;
  List<ImageCreate>? attachedDocuments;
  List<AccountDocumentType> documentTypes;

  final Function(BuildContext context, String message) showErrorAlert;
  final Function(BuildContext, String signatoryFullName, ImageCreate imageCreate, List<ImageCreate>? attachedDocuments) onNavigatePressed;

  RoadMovementSignatureVM({
    required this.isLoading,
    required this.settingsAutoDisplayNextDropdownOrInput,
    required this.settingsAutoSubmitPage,
    required this.authState,
    required this.account,
    required this.roadMovementRole,
    required this.roadMovementCreate,
    required this.universalBranding,
    required this.attachedDocuments,
    required this.documentTypes,
    required this.showErrorAlert,
    required this.onNavigatePressed,
  });

  static RoadMovementSignatureVM fromStore(Store<AppState> store) {
    return RoadMovementSignatureVM(
        settingsAutoDisplayNextDropdownOrInput:
            store.state.uiState.uiDropDownOrInputAutoContinue,
        settingsAutoSubmitPage: store.state.uiState.uiAutoSubmitPage,
        isLoading: store.state.isLoading,
        authState: store.state.authState,
        account: store.state.accountState.account,
        roadMovementRole: store.state.uiState.roadMovementRole,
        roadMovementCreate: store.state.roadMovementState.roadMovementCreate,
        universalBranding:
            UniversalBranding(store.state.accountState.activeService),
      attachedDocuments: store.state.roadMovementState.roadMovementCreate.documents,
      documentTypes: store.state.accountState.filteredAccountDocumentTypes,
        onNavigatePressed: (BuildContext context, String signatoryFullName, ImageCreate imageCreate, List<ImageCreate>? attachedDocuments) {
          if (store.state.isLoading) {
            return;
          }

          store.dispatch(SetRoadMovementSignature(signatoryFullName, imageCreate, attachedDocuments));
          store.dispatch(NavigateToAction.push(
              route_helper.roadMovementSummaryScreenBuilder));
        },
        showErrorAlert: (BuildContext context, String message) {
          showOkMessage(
              context,
              "Submit Error",
              message);
        },
    );
  }
}
