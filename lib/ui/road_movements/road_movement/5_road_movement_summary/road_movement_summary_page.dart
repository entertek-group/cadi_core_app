import 'dart:core';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/helpers/validation_helper.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/ui/shared/components/app/setting_tile.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:cadi_core_app/ui/shared/components/input.dart';
import 'package:cadi_core_app/ui/shared/components/popup_menu.dart';
import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'road_movement_summary_vm.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';

class RoadMovementSummaryPage extends StatefulWidget {
  final RoadMovementSummaryVM viewModel;
  const RoadMovementSummaryPage({
    super.key,
    required this.viewModel,
  });

  @override
  State<RoadMovementSummaryPage> createState() {
    return RoadMovementSummaryPageState(viewModel: viewModel);
  }
}

class RoadMovementSummaryPageState extends State<RoadMovementSummaryPage> {
  final RoadMovementSummaryVM viewModel;
  String movementStatus = "";
  String movementType = "";
  var dateTime = DateFormat.yMMMEd().format(DateTime.now()).toString();
  var containerWeightUnitList = <PopMenuItem>[];
  RoadMovementSummaryPageState({
    Key? key,
    required this.viewModel,
  });

  final TextEditingController _textEditingController = TextEditingController();

  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var time = widget.viewModel.roadMovementCreate.expectedArrivalAt.toString().substring(10, 20);

    List<Widget> roadSummary = <Widget>[
      buildSummaryRow(context, "Waybill", widget.viewModel.roadMovementCreate.waybillNumber, false, null),
      buildSummaryRow(context, "Leg", widget.viewModel.roadMovementCreate.legNumber, false, null),
      if (widget.viewModel.roadMovementCreate.isBreakBulk)
        buildSummaryRow(context, "Break Bulk", "True", false, null),
      if (widget.viewModel.roadMovementCreate.containerData != null && !widget.viewModel.roadMovementCreate.isBreakBulk)
        for (var container in widget.viewModel.roadMovementCreate.containerData!)
          buildSummaryRow(context, "Container #", container.containerNumber, true, () {
            _textEditingController.text = container.containerNumber;
            showMessageWithInputActions(
              context,
              "Amend Container",
              "Enter new container number",
              () {
                if (isContainerInputValid(context, _textEditingController.text, true)) {
                  widget.viewModel.updateContainerNumber(container.containerNumber, _textEditingController.text);
                  Navigator.pop(context);
                } else {
                  Navigator.pop(context);
                  showOkMessage(context, "Update Error", "Container number is not in a valid format. Try again.");
                }
              },
              "Save",
              () {
                Navigator.pop(context);
              },
              "Cancel",
              inputWidget(context, _textEditingController, "", false),
            );
          }),
      buildSummaryRow(context, "Status", widget.viewModel.roadMovementCreate.state, false, null),
      buildSummaryRow(context, "Comments", widget.viewModel.roadMovementCreate.comment, false, null),
      if (widget.viewModel.roadMovementCreate.documents != null)
        buildSummaryRow(context, "Documents", '${widget.viewModel.roadMovementCreate.documents!.length} attached', false, null),
      buildSummaryRow(context, "", "", false, null),
        if (widget.viewModel.roadMovementCreate.role == CadiRoadMovementRole.delivery) ...[
        if (widget.viewModel.roadMovementCreate.signature != null)
          buildSummaryRow(context, "Signature Attached", "YES", true, () => viewModel.onNavigateToSignaturePage(context))
        else
          buildSummaryRow(context, "Signature Attached", "NO", true, () => viewModel.onNavigateToSignaturePage(context)),
        buildSummaryRow(context, "Signatory", widget.viewModel.roadMovementCreate.signatoryFullName ?? "", false, null),       
      ],
      buildSummaryRow(context, "Port", widget.viewModel.accountPortDepot.unLocode, false, null),
      buildSummaryRow(context, "Location", widget.viewModel.accountPortDepot.depotName, false, null),
      buildSummaryRow(context, "Driver", widget.viewModel.accountVehicleDriver.fullName, false, null),
    ];
    if(widget.viewModel.roadMovementCreate.accountVehicleOperatorId != null) {
      roadSummary.add(buildSummaryRow(context, "Operator", widget.viewModel.accountVehicleOperator.fullName, false, null));
    }

    roadSummary.addAll([
      buildSummaryRow(context, "Vehicle", widget.viewModel.accountVehicle.model, false, null),
      buildSummaryRow(context, "Date", dateTime, false, null),
      buildSummaryRow(context, "Time In", time, false, null),
      buildSummaryRow(context, "Profile", widget.viewModel.account.name, false, null)
    ]);


    List<SettingTile> allOptions = <SettingTile>[
      SettingTile(
        0,
        'Active Account',
        Column(
          children: <Widget>[
            Text(
              "Road Movement: ${getRoadMovementRoleDescription(widget.viewModel.roadMovementRole)}",
              style: TextStyle(fontSize: PlatformSizing().normalTextSize(context), color: BrandingColors.cadiBlack),
            ),
          ],
        ),
      ),
      SettingTile(
        1,
        'Account Operations',
        Container(
          margin: const EdgeInsets.only(top: 10.0, bottom: 15),
          child: Column(
            children: <Widget>[
              Text(
                "Summary: Check, Update and Submit",
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: PlatformSizing().smallTextSize(context), color: BrandingColors.cadiBlack),
              ),
            ],
          ),
        ),
      ),
      SettingTile(2, 'Summary', buildSummaryCard(context, roadSummary)),
    ];

    allOptions.add(SettingTile(
      7,
      '',
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 50.0, vertical: 10.0),
        child: BaseButton(
          color: widget.viewModel.universalBranding.getButtonPrimaryColor(),
          child: Text('Submit', style: TextStyle(fontSize: PlatformSizing().normalTextSize(context))),
          onPressed: () {
            viewModel.confirmRoadMovement(context);
          },
        ),
      ),
    ));

    if (widget.viewModel.isLoading) {
      return const AppLoading(message: "Loading...");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: ListView.builder(
        physics: const ClampingScrollPhysics(),
        itemCount: allOptions.length,
        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
        itemBuilder: (BuildContext context, int index) {
          return allOptions[index].body;
        },
      ),
    );
  }

  Widget buildSummaryCard(BuildContext context, List<Widget> roadSummary) {
    return Card(
      elevation: 2.0,
      color: Colors.white,
      shape: RoundedRectangleBorder(
        side: const BorderSide(color: BrandingColors.cadiGrey, width: 0.5),
        borderRadius: BorderRadius.circular(5),
      ),
      margin: const EdgeInsets.only(left: 5, right: 5, bottom: 20),
      child: ListView.builder(
        physics: const ClampingScrollPhysics(),
        shrinkWrap: true,
        padding: const EdgeInsets.all(15),
        itemCount: roadSummary.length,
        itemBuilder: (BuildContext context, int index) {
          return roadSummary[index];
        },
      ),
    );
  }

  Widget buildSummaryRow(BuildContext context, String textDisplay, String textValue, bool hasAction, Function? buttonAction) {
    return ListTile(
      dense: true,
      visualDensity: const VisualDensity(horizontal: 0, vertical: -2),
      minVerticalPadding: 4,
      contentPadding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 2),
      tileColor: Colors.transparent,
      leading: Text(
        textDisplay,
        style: TextStyle(fontWeight: FontWeight.w500, fontSize: PlatformSizing().smallTextSize(context), color: BrandingColors.cadiBlack),
      ),
      title: Text(
        textValue,
        style: TextStyle(fontSize: PlatformSizing().smallTextSize(context), color: BrandingColors.cadiBlack),
        textAlign: TextAlign.end,
      ),
      trailing: hasAction
          ? IconButton(
              padding: const EdgeInsets.only(bottom: 10, left: 25),
              icon: const Icon(FontAwesomeIcons.penToSquare, color: BrandingColors.cadiBlack),
              onPressed: buttonAction != null ? () => buttonAction() : () {},
            )
          : const IconButton(icon: Icon(Icons.edit_outlined, color: Colors.transparent), onPressed: null),
    );
  }
}
