import 'package:cadi_core_app/factory/cadi_enum_factory.dart';
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/redux/models/reference/cadi_enum.dart';
import 'package:cadi_core_app/services/data_processing_service.dart';
import 'package:cadi_core_app/theme/theme.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'road_movement_summary_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class RoadMovementSummaryScreenBuilder extends StatelessWidget {
  const RoadMovementSummaryScreenBuilder({super.key});

  static const String route = route_helper.roadMovementSummaryScreenBuilder;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(context, "Action Blocked",
                  "You cannot change account while performing this action on summary screen.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, RoadMovementSummaryVM>(
        converter: RoadMovementSummaryVM.fromStore,
        builder: (context, vm) {
          return RoadMovementSummaryPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class RoadMovementSummaryVM {
  bool isLoading;
  AuthState authState;
  Account account;
  AccountPortDepot accountPortDepot;
  AccountVehicleDriver accountVehicleDriver;
  AccountVehicleDriver accountVehicleOperator;
  AccountVehicle accountVehicle;
  AccountCarrier accountCarrier;
  String roadMovementRole;
  RoadMovementCreate roadMovementCreate;
  UniversalBranding universalBranding;
  UiCargoTruckDetails uiCargoTruckDetails;
  List<CadiEnum> weightUnits;

  FileStore tempStorage;
  final Function(BuildContext, String) onNavigatePressed;
  final Function(BuildContext context) onNextPressed;
  final Function(String previousContainerNumber, String containerNumber) updateContainerNumber;

  final Function(BuildContext context) confirmRoadMovement;
  final Function(BuildContext context) onNavigateToSignaturePage;

  RoadMovementSummaryVM({
    required this.isLoading,
    required this.authState,
    required this.account,
    required this.accountPortDepot,
    required this.accountVehicleDriver,
    required this.accountVehicleOperator,
    required this.accountVehicle,
    required this.accountCarrier,
    required this.roadMovementRole,
    required this.weightUnits,
    required this.roadMovementCreate,
    required this.universalBranding,
    required this.onNavigatePressed,
    required this.onNextPressed,
    required this.updateContainerNumber,
    required this.confirmRoadMovement,
    required this.uiCargoTruckDetails,
    required this.tempStorage,
    required this.onNavigateToSignaturePage, 

  });

  static RoadMovementSummaryVM fromStore(Store<AppState> store) {
    return RoadMovementSummaryVM(
      isLoading: store.state.isLoading,
      authState: store.state.authState,
      account: store.state.accountState.account,
      uiCargoTruckDetails: store.state.uiState.uiCargoTruckDetails,
      accountPortDepot: store.state.accountState.accountPortDepots
          .where((element) =>
              element.id ==
              store.state.roadMovementState.roadMovementCreate.accountPortLocationId)
          .first,
      accountVehicleDriver:
          store.state.accountState.accountVehicleDrivers.firstWhere(
        (element) =>
            element.id ==
            store.state.roadMovementState.roadMovementCreate
                .accountVehicleDriverId,
        orElse: () => AccountVehicleDriver.initial(),
      ),
      accountVehicleOperator:
      store.state.accountState.accountVehicleOperators.firstWhere(
            (element) =>
        element.id ==
            store.state.roadMovementState.roadMovementCreate
                .accountVehicleOperatorId,
        orElse: () => AccountVehicleDriver.initial(),
      ),
      accountVehicle:
      store.state.accountState.accountVehicles.firstWhere(
            (element) =>
        element.id ==
            store.state.roadMovementState.roadMovementCreate
                .accountVehicleId,
        orElse: () => AccountVehicle.initial(),
      ),
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      accountCarrier: AccountCarrier.initial(),
      roadMovementRole: store.state.uiState.roadMovementRole,
      roadMovementCreate: store.state.roadMovementState.roadMovementCreate,
      weightUnits: CadiEnumFactory().createWeightMeasurements(),
      tempStorage: store.state.storageState.tempStorage,
      onNavigatePressed: (BuildContext context, String route) {
        if (store.state.isLoading) {
          return;
        }
        navigation_helper.navigateRoute(route, store, null);
      },
      onNextPressed: (BuildContext context) {
        store.dispatch(NavigateToAction.push(
            route_helper.roadMovementCompletionScreenBuilder));
      },
      updateContainerNumber: (String previousContainerNumber, String containerNumber) {
        store.dispatch(UpdateRoadContainerNumber(previousContainerNumber,containerNumber));
      },

      confirmRoadMovement: (BuildContext context) {
        store.dispatch(QueueRoadMovement(
            store.state.roadMovementState.roadMovementCreate));
        if (store.state.connectivityState.connectionStatus ==
                ConnectivityConstants.none ||
            store.state.connectivityState.connectionStatus ==
                ConnectivityConstants.ethernet) {
          showMessageWithActions(context, 'Queued',
              "Road movement has scheduled for submission when device is online.",
              () {
            Navigator.pop(context);
            store.dispatch(NavigateToAction.pushNamedAndRemoveUntil(
                route_helper.roadMovementCompletionScreenBuilder,
                    (route) => false));
          }, "Ok", null, "");
        } else {
          store.dispatch(NavigateToAction.pushNamedAndRemoveUntil(
              route_helper.roadMovementCompletionScreenBuilder,
              (route) => false));

          processRoadMovementQueue(store);
        }
      } ,
       onNavigateToSignaturePage: (BuildContext context) {
        store.dispatch(NavigateToAction.push(
            route_helper.roadMovementSignatureScreenBuilder));
      },
    );
  }
}
