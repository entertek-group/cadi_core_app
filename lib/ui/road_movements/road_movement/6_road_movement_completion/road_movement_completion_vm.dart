// ignore_for_file: prefer_function_declarations_over_variables
import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/models.dart';
import 'package:cadi_core_app/theme/theme.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_bar_buttons/app_bar_buttons.dart';
import 'package:cadi_core_app/ui/shared/components/alerts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/states.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'road_movement_completion_page.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart'
    as navigation_helper;

class RoadMovementCompletionScreenBuilder extends StatelessWidget {
  const RoadMovementCompletionScreenBuilder({super.key});

  static const String route = route_helper.roadMovementCompletionScreenBuilder;
  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      backgroundColor: BrandingColors.cadiWhite,
      appBar: appBar(
          context,
          appBarUserAccount(
              context,
              () => showOkMessage(
                  context,
                  "Action Blocked",
                  "You cannot change account while performing this action.")),
          null,
          <Widget>[
            appBarHomeButton(context, true),
          ]),
      body: StoreConnector<AppState, RoadMovementCompletionVM>(
        converter: RoadMovementCompletionVM.fromStore,
        builder: (context, vm) {
          return RoadMovementCompletionPage(
            viewModel: vm,
          );
        },
      ),
    );
  }
}

class RoadMovementCompletionVM {
  bool isLoading;
  AuthState authState;
  Account account;
  UniversalBranding universalBranding;
  final Function(BuildContext, String) onNavigatePressed;
  final Function(String token) setToken;
  final Function(BuildContext context) newMovement;

  RoadMovementCompletionVM({
    required this.isLoading,
    required this.authState,
    required this.account,
    required this.universalBranding,
    required this.onNavigatePressed,
    required this.setToken,
    required this.newMovement,
  });

  static RoadMovementCompletionVM fromStore(Store<AppState> store) {
    return RoadMovementCompletionVM(
      isLoading: store.state.isLoading,
      authState: store.state.authState,
      account: store.state.accountState.account,
      universalBranding:
          UniversalBranding(store.state.accountState.activeService),
      onNavigatePressed: (BuildContext context, String route) {
        if (store.state.isLoading) {
          return;
        }
        navigation_helper.navigateRoute(route, store, null);
      },
      setToken: (String token) {},
      newMovement: (BuildContext context) {
        store.dispatch(ResetRoadMovementCreate());
        store
            .dispatch(SetHomeIndex(HomeNavigationConstants.roadMovementIndex));
        store.dispatch(NavigateToAction.replace(route_helper.homeScreen));
      },
    );
  }
}
