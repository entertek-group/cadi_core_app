import 'package:cadi_core_app/theme/branding.dart';
import 'package:cadi_core_app/ui/shared/components/base/base.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

void showOkMessage(
  BuildContext context,
  String title,
  String text,
) {
  showPlatformDialog(
    context: context,
    builder: (context) {
      return PlatformAlertDialog(
        title: Text(
          title,
        ),
        content: Text(
          text,
        ),
        actions: <Widget>[
          PlatformDialogAction(
            child: BaseButton(
              color: BrandingColors.cadiGreen,
              child: Text('Ok',
                  style: TextStyle(
                      fontSize: PlatformSizing().normalTextSize(context))),
              onPressed: () => Navigator.pop(context),
            ),
          ),
        ],
        material: (_, __) => MaterialAlertDialogData(
          backgroundColor: BrandingColors.cadiWhite,
        ),
        cupertino: (_, __) => CupertinoAlertDialogData(
          // CupertinoAlertDialog does not support background color,
          // so this will only affect the MaterialAlertDialog.
        ),
      );
    },
  );
}

void showMessageWithActions(
  BuildContext context,
  String title,
  String text,
  Function okAction,
  String okText,
  Function? cancelAction,
  String cancelText,
) {
  List<Widget> widgets = [];
  widgets.add(PlatformDialogAction(
    child: BaseGhostButton(
      onPressed: () => okAction(),
      textColor: BrandingColors.cadiGreen,
      child: Text(
        okText,
        style: TextStyle(
          color: BrandingColors.cadiGreen,
          fontSize: PlatformSizing().normalTextSize(context),
        ),
      ),
    ),
  ));

  if (cancelAction != null) {
    widgets.add(PlatformDialogAction(
      child: BaseGhostButton(
        onPressed: () => cancelAction(),
        textColor: BrandingColors.cadiBlackOpacity,
        child: Text(
          cancelText,
          style: TextStyle(
            color: BrandingColors.cadiBlackOpacity,
            fontSize: PlatformSizing().normalTextSize(context),
          ),
        ),
      ),
    ));
  }

  showPlatformDialog(
    context: context,
    builder: (context) {
      return PlatformAlertDialog(
        title: Text(title),
        content: Text(text),
        actions: widgets,
        material: (_, __) => MaterialAlertDialogData(
          backgroundColor: BrandingColors.cadiWhite,
        ),
        cupertino: (_, __) => CupertinoAlertDialogData(
          // CupertinoAlertDialog does not support background color,
          // so this will only affect the MaterialAlertDialog.
        ),
      );
    },
  );
}


void showMessageWithInputActions(
  BuildContext context,
  String title,
  String text,
  Function okAction,
  String okText,
  Function? cancelAction,
  String cancelText,
  Widget customWidget,
) {
  List<Widget> widgets = [];
  widgets.add(PlatformDialogAction(
    child: BaseButton(
      color: BrandingColors.cadiGreen,
      child: Text(
        okText,
        style: TextStyle(
          fontSize: PlatformSizing().normalTextSize(context),
        ),
      ),
      onPressed: () => okAction(),
    ),
  ));

  if (cancelAction != null) {
    widgets.add(PlatformDialogAction(
      child: BaseGhostButton(
        onPressed: () => cancelAction(),
        textColor: BrandingColors.cadiBlackOpacity,
        child: Text(
          cancelText,
          style: TextStyle(
            fontSize: PlatformSizing().normalTextSize(context), color: BrandingColors.cadiBlackOpacity,
          ),
        ),
      ),
    ));
  }

  showPlatformDialog(
    context: context,
    builder: (context) {
      return PlatformAlertDialog(
        title: Text(
          title,
          style: const TextStyle(color: BrandingColors.cadiBlack),
        ),
        content: Container(
          color: BrandingColors.cadiWhite,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Material(
                color: BrandingColors.cadiWhite,
                child: ListTile(
                  title: Text(
                    text,
                    style: TextStyle(
                      fontSize: PlatformSizing().mediumTextSize(context),
                      color: BrandingColors.cadiBlack,
                    ),
                  ),
                ),
              ),
              customWidget,
            ],
          ),
        ),
        actions: widgets,
        material: (_, __) => MaterialAlertDialogData(
          backgroundColor: BrandingColors.cadiWhite,
        ),
        cupertino: (_, __) => CupertinoAlertDialogData(
          // CupertinoAlertDialog does not support background color,
          // so this will only affect the MaterialAlertDialog.
        ),
      );
    },
  );
}
