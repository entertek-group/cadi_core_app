import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

PlatformAppBar appBar(
  BuildContext context,
  Widget? title,
  Widget? leading,
  List<Widget>? trailingActions,
) {
  var cupertinoNavBarData = CupertinoNavigationBarData(
    padding: const EdgeInsetsDirectional.all(0.0),
    border: Border.all(
        width: 0.0, style: BorderStyle.none, color: BrandingColors.cadiWhite),
    backgroundColor:
        BrandingColors.cadiWhite, // Set background color for Cupertino
  );
  var dynamicSize = MediaQuery.of(context).size.width * 0.2;
  var materialNavBarData = MaterialAppBarData(
    leadingWidth: dynamicSize,
    toolbarHeight: 80,
    shadowColor: BrandingColors.white,
    elevation: 0,
    scrolledUnderElevation: 1,
    surfaceTintColor: BrandingColors.cadiLightGreen,
    iconTheme: const IconThemeData(
      size: 30,
      color: BrandingColors.cadiBlack,
    ),
  );

  const backgroundColor = BrandingColors.cadiWhite;

  if (leading == null && trailingActions != null) {
    return PlatformAppBar(
      title: title,
      cupertino: (_, __) => cupertinoNavBarData,
      backgroundColor: backgroundColor,
      material: (_, __) => materialNavBarData,
      trailingActions: trailingActions,
    );
  } else if (leading != null && trailingActions == null) {
    return PlatformAppBar(
      title: title,
      cupertino: (_, __) => cupertinoNavBarData,
      material: (_, __) => materialNavBarData,
      leading: leading,
      backgroundColor: backgroundColor,
    );
  } else if (leading == null && trailingActions == null) {
    return PlatformAppBar(
      title: title,
      cupertino: (_, __) => cupertinoNavBarData,
      material: (_, __) => materialNavBarData,
      backgroundColor: backgroundColor,
    );
  } else {
    return PlatformAppBar(
      title: title,
      cupertino: (_, __) => cupertinoNavBarData,
      material: (_, __) => materialNavBarData,
      leading: leading,
      trailingActions: trailingActions,
      backgroundColor: backgroundColor,
    );
  }
}

class IconFontAwesomeIcons {}
