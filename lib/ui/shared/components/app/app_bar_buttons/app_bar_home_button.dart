import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

Widget appBarHomeButton(BuildContext context, bool replace) {
  final store = StoreProvider.of<AppState>(context);
  var dynamicSize = MediaQuery.of(context).size.width * 0.065;
  var topHeight = MediaQuery.of(context).size.width * 0.10;
  return GestureDetector(
      onTap: () {
        store.dispatch(SetHomeIndex(HomeNavigationConstants.homeScreenIndex));
        if (replace) {
          store.dispatch(NavigateToAction.replace(route_helper.homeScreen));
        }
      },
      child: RichText(
        text: TextSpan(children: [
          WidgetSpan(
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                FaIcon(BrandIcons.home,
                    size: dynamicSize, color: BrandingColors.cadiBlack),
                Padding(
                  padding: EdgeInsets.only(
                    top: topHeight,
                    right: dynamicSize,
                  ),
                ),
              ],
            ),
          ),
        ]),
      ));
}
