import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Widget appBarLogoutButton(Function() function, BuildContext context) {
  var dynamicSize = MediaQuery.of(context).size.width * 0.065;
  var topHeight = MediaQuery.of(context).size.width * 0.10;
  return GestureDetector(
      onTap: function,
      child: RichText(
        text: TextSpan(children: [
          WidgetSpan(
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                FaIcon(BrandIcons.logout,
                    size: dynamicSize, color: BrandingColors.cadiBlack),
                Padding(
                  padding: EdgeInsets.only(
                    top: topHeight,
                    right: dynamicSize,
                    bottom: 10,
                  ),
                )
              ],
            ),
          ),
        ]),
      ));
}
