import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_redux/flutter_redux.dart';

Widget appBarUserAccount(BuildContext context, Function() function) {
  final store = StoreProvider.of<AppState>(context);
  var dynamicSize = MediaQuery.of(context).size.width * 0.093;

  final String connectionStatus =
      store.state.connectivityState.connectionStatus;

  return GestureDetector(
    onTap: function,
    child: Padding(
      padding: const EdgeInsets.only(top: 16.0), // Add top padding here
      child: Padding(
        padding: EdgeInsets.only(left: dynamicSize),
        child: RichText(
          text: TextSpan(
            children: [
              WidgetSpan(
                child: Column(
                  children: [
                    (connectionStatus == ConnectivityConstants.mobile ||
                            connectionStatus == ConnectivityConstants.wifi)
                        ? const FaIcon(
                            BrandIcons.userCircle,
                            color: BrandingColors.cadiGreen,
                          )
                        : const FaIcon(
                            BrandIcons.userCircle,
                            color: Colors.red,
                          ),
                    Padding(
                      padding: EdgeInsets.only(left: dynamicSize, bottom: 38),
                    ),
                  ],
                ),
              ),
              WidgetSpan(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 32.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        store.state.authState.user!.name.length > 14
                            ? store.state.authState.user!.name.substring(0, 15)
                            : store.state.authState.user!.name,
                        style: TextStyle(
                          fontSize: PlatformSizing().smallTextSize(context),
                          fontWeight: FontWeight.w500,
                          color: BrandingColors.cadiBlack,
                        ),
                      ),
                      Text(
                        store.state.accountState.account.name,
                        style: TextStyle(
                          fontSize: PlatformSizing().mediumTextSize(context),
                          fontWeight: FontWeight.w400,
                          color: BrandingColors.cadiBlack,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
