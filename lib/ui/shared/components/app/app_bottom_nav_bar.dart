import 'package:cadi_core_app/redux/actions/app_actions.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:cadi_core_app/theme/theme.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

import 'bottom_bar/bottom_bar_builder.dart';

PlatformNavBar appBottomNavBar(BuildContext context, bool replace,
    BottomNavigationBarType fixed, String activeService) {
  var dynamicSize = MediaQuery.of(context).size.width * 0.055;
  final store = StoreProvider.of<AppState>(context);

  return PlatformNavBar(
    backgroundColor: BrandingColors.cadiWhite,
    itemChanged: (index) {
      store.dispatch(SetHomeIndex(index + 1));
      if (replace) {
        store.dispatch(NavigateToAction.replace(route_helper.homeScreen));
      }
    },
    items: buildBottomNavBarItems(activeService, store.state.homeIndex, dynamicSize),

    material: (_, __) => MaterialNavBarData(
      fixedColor: BrandingColors.cadiWhite, // Text color
      backgroundColor: BrandingColors.cadiWhite, // Background color
      type: BottomNavigationBarType.fixed,
    ),

    material3: (_, __) => MaterialNavigationBarData(
      indicatorColor: store.state.homeIndex == 0
          ? Colors.transparent
          : BrandingColors.cadiWhite,
      backgroundColor: store.state.homeIndex == 0
          ? BrandingColors.background
          : BrandingColors.cadiWhite, // Background color
      selectedIndex: store.state.homeIndex > 0
          ? store.state.homeIndex - 1
          : store.state.homeIndex,
      overlayColor: WidgetStateProperty.all<Color>(Colors.transparent),
    ),

    cupertino: (_, __) => CupertinoTabBarData(
      border: Border.all(width: 0, color: BrandingColors.cadiWhite),
    ),
  );
}