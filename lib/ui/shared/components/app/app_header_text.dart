import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';

AutoSizeText appHeaderText(String text, double maxFontSize, Color color) {
  return AutoSizeText(
    text,
    style: TextStyle(
        fontSize: maxFontSize, fontFamily: "abeatbyKai", color: color),
    minFontSize: 14,
    stepGranularity: 0.2,
    maxLines: 2,
  );
}
