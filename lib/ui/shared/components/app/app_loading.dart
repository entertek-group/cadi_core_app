import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/theme.dart';
import 'app_logo.dart';
import 'package:auto_size_text/auto_size_text.dart';

class AppLoading extends StatefulWidget {
  final String? message;

  const AppLoading({super.key, this.message});

  @override
  // ignore: unnecessary_this
  _AppLoading createState() => _AppLoading(message: this.message);
}

class _AppLoading extends State<AppLoading> with TickerProviderStateMixin {
  AnimationController? _controller; //Todo test if this works
  final String? message;
//Key key,
  _AppLoading({required this.message});

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
    _controller!.repeat();
  }

  @override
  void dispose() {
    _controller!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var maxWidth = MediaQuery.of(context).size.width * 0.4;
    var maxHeight = MediaQuery.of(context).size.height * 0.03;
    return Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
          image: AssetImage(BrandingImages.backgroundLogin),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(
            BrandingColors.cadiBlackOpacity,
            BlendMode.srcOver,
          ),
        )),
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            const AppLogo(
              maxFontSize: 60,
            ),
            const Text(""),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 20),
              width: 150,
              height: 20,
              child: ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(10)),
                child: LinearProgressIndicator(
                  value: 0.9,
                  valueColor: _controller!.drive(ColorTween(
                      begin: BrandingColors.cadiGreen,
                      end: BrandingColors.cadiBlue)),
                  backgroundColor: BrandingColors.cadiWhiteOpacity,
                ),
              ),
            ),
            // Padding(
            //     padding: const EdgeInsets.all(15.0),
            //     child: Center(
            //       child: LinearProgressIndicator(
            //         minHeight: 20,
            //         valueColor: _controller!.drive(ColorTween(
            //             begin: BrandingColors.cadiGreen,
            //             end: BrandingColors.cadiBlue)),
            //       ),
            // )), //#Todo Hannah.
            const Text(""),
            const Text(""),
            SizedBox(
                height: maxHeight,
                width: maxWidth,
                child: Center(
                    child: AutoSizeText(
                  widget.message ?? "",
                  style: const TextStyle(
                      // fontFamily: "abeatbyKai",
                      color: BrandingColors.cadiWhite,
                      fontSize: 25),
                  minFontSize: 8,
                  stepGranularity: 0.1,
                )))
          ],
        )));
  }
}
