import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/theme.dart';

class AppLogo extends StatelessWidget {
  final double maxFontSize;

  const AppLogo({super.key, required this.maxFontSize});

  @override
  Widget build(BuildContext context) {
    var maxWidth = MediaQuery.of(context).size.width * 0.25;
    var maxHeight = MediaQuery.of(context).size.height * 0.05;
    return SizedBox(
        height: maxHeight,
        width: maxWidth,
        child: Container(
            alignment: Alignment.centerLeft,
            decoration: const BoxDecoration(
                image: DecorationImage(
              image: AssetImage(BrandingImages.logoWhite),
              fit: BoxFit.scaleDown,
            ))));

    //  Center(
    //     child:
    //         // ignore: prefer_const_constructors
    //         AutoSizeText.rich(
    //   const TextSpan(children: [
    //     TextSpan(text: "Ca", style: TextStyle(color: BrandingColors.black)),
    //     TextSpan(
    //       text: "Di",
    //       style: TextStyle(color: BrandingColors.cadiGreen),
    //     )
    //   ]),
    //   style: TextStyle(fontFamily: "abeatbyKai", fontSize: maxFontSize),
    //   minFontSize: 10,
    //   stepGranularity: 0.2,
    // )));
  }
}
