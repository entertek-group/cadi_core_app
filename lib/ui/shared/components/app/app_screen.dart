import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
//import 'app_nav_bar.dart';
import 'app_bar.dart';

PlatformScaffold appScreenScaffold(BuildContext context, Widget appBarTitle,
    Widget leading, List<Widget> trailing, Widget body) {
  return PlatformScaffold(
    appBar: appBar(context, appBarTitle, leading, trailing),
    //drawer: AppDrawerBuilder(),
    backgroundColor: BrandingColors.cadiGreen,
    //bottomNavBar: appNavBar(context),
    body: body,
  );
}
