import 'package:cadi_core_app/helpers/constants.dart';
import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';


// Function to build BottomNavigationBar items based on active service and home index
List<BottomNavigationBarItem> buildBottomNavBarItems(
    String activeService, int activeHomeIndex, double dynamicSize) {
  List<BottomNavigationBarItem> bottomNavBarItems = [];

  switch (activeService) {
    case CadiAppServices.portMovements:
      bottomNavBarItems = [
        _buildBarItem(activeHomeIndex, BrandIcons.cargoMovement,
            HomeNavigationConstants.cargoMovementIndex, dynamicSize),
        _buildBarItem(activeHomeIndex, BrandIcons.cargoUpdate,
            HomeNavigationConstants.portCargoUpdateIndex , dynamicSize),
        _buildBarItem(activeHomeIndex, BrandIcons.reports,
            HomeNavigationConstants.reportsIndex , dynamicSize),
        _buildBarItem(activeHomeIndex, BrandIcons.settings,
            HomeNavigationConstants.settingsScreenIndex , dynamicSize),
      ];
      break;

    case CadiAppServices.portEvents:
      bottomNavBarItems = [
        _buildBarItem(activeHomeIndex, BrandIcons.vessel, 1 , dynamicSize),
        _buildBarItem(activeHomeIndex, BrandIcons.settings, 2 , dynamicSize),
      ];
      break;

    case CadiAppServices.roadMovements:
      bottomNavBarItems = [
        _buildBarItem(activeHomeIndex, BrandIcons.truck,
            HomeNavigationConstants.roadMovementIndex , dynamicSize),
        _buildBarItem(activeHomeIndex, BrandIcons.reports, 2 , dynamicSize),
        _buildBarItem(activeHomeIndex, BrandIcons.settings, 3 , dynamicSize),
      ];
      break;

    case CadiAppServices.roadEvents:
      bottomNavBarItems = [
        _buildBarItem(activeHomeIndex, BrandIcons.settings,
            HomeNavigationConstants.settingsScreenIndex , dynamicSize),
      ];
      break;

    default:
      bottomNavBarItems = [
        _buildBarItem(
            activeHomeIndex,
            BrandIcons.settings,
            HomeNavigationConstants.emptySettingsScreenIndex , dynamicSize),
      ];
      break;
  }
  return bottomNavBarItems;
}

// Helper function to build a BottomNavigationBarItem with proper styling
BottomNavigationBarItem _buildBarItem(
    int activeHomeIndex, IconData iconData, int targetIndex, double dynamicSize) {
  return BottomNavigationBarItem(
      label: "",
      icon: Padding(
  padding: const EdgeInsets.only(top: 20.0, bottom: 2.0), // Added top padding
  child: FaIcon(
    iconData,
    size: dynamicSize,
    color: activeHomeIndex == targetIndex
        ? BrandingColors.cadiGrey
        : BrandingColors.cadiBlack,
  ),
)
);
}
