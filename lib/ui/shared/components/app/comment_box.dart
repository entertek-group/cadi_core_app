import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

class CommentBox {
  dynamic textFieldData(String placeHolder) {
    return MaterialTextFieldData(
        style: const TextStyle(
          color: BrandingColors.cadiBlack,
        ),
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: BrandingColors.cadiGrey),
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: BrandingColors.cadiGreen),
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
          ),
          labelText: placeHolder,
          labelStyle: const TextStyle(color: BrandingColors.cadiBlack),
        ));
  }
}
