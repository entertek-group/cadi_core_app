import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CupertinoAssets {
  dynamic textFieldData(String placeHolder, IconData? icon) {
    return CupertinoTextFieldData(
        prefix: Padding(
            padding: icon != null
                ? const EdgeInsets.only(left: 20)
                : const EdgeInsets.only(left: 0),
            child: FaIcon(icon, color: BrandingColors.cadiGrey) // #todo
            ),
        padding: const EdgeInsets.all(12), //#todo
        placeholder: placeHolder,
        decoration: BoxDecoration(
            color: BrandingColors.white,
            borderRadius: BorderRadius.circular(30.0),
            border: Border.all(color: BrandingColors.grey)));
  }
}
