import 'dart:io';

import 'package:cadi_core_app/theme/theme.dart';
import 'package:flutter/material.dart';

errorSnack(BuildContext context, String errorMessage) {
  /*
  Need to wrap this in Android specific logic because of
  the platform aware widgets set at the App Level
  PlatformApp();
  iOS does not have access to Material.
   */
  if (Platform.isAndroid) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(errorMessage),
      duration: const Duration(seconds: 2),
      backgroundColor: BrandingColors.red,
    ));
  }
}