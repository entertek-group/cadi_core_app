import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MaterialAssets {
  dynamic textFieldData(String placeHolder, IconData? icon) {
    return MaterialTextFieldData(
        style: const TextStyle(
          color: BrandingColors.cadiWhite,
        ),
        decoration: InputDecoration(
          enabledBorder: const OutlineInputBorder(
            borderSide:
                BorderSide(width: 1, color: BrandingColors.cadiWhiteOpacity),
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: BrandingColors.cadiGreen),
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
          ),
          prefixIcon: Padding(
              padding: icon != null
                  ? const EdgeInsets.only(left: 20, top: 10, right: 20)
                  : const EdgeInsets.only(right: 20),
              child: FaIcon(icon, color: BrandingColors.cadiGrey) // #todo
              ),
          labelText: placeHolder,
          labelStyle: const TextStyle(
            color: BrandingColors.cadiWhite,
          ),
        ));
  }
}
