import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MaterialAssetsDark {
  dynamic textFieldData(String placeHolder, IconData? icon) {
    return MaterialTextFieldData(
      style: const TextStyle(
        color: BrandingColors.cadiBlackOpacity,
      ),
      decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          enabledBorder: const OutlineInputBorder(
            borderSide:
                BorderSide(width: 1, color: BrandingColors.cadiBlackOpacity),
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
          ),
          disabledBorder: const OutlineInputBorder(
            borderSide:
                BorderSide(width: 1, color: BrandingColors.cadiBlackOpacity),
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: BrandingColors.cadiGreen),
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
          ),
          prefixIcon: Padding(
              padding: icon != null
                  ? const EdgeInsets.only(left: 20, top: 10, right: 10)
                  : const EdgeInsets.only(right: 10),
              child: FaIcon(icon, color: BrandingColors.cadiGrey) // #todo
              ),
          labelText: placeHolder,
          labelStyle: const TextStyle(
            color: BrandingColors.cadiBlack,
          )),
    );
  }
}
