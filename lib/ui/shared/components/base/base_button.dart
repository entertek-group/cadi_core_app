import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

class BaseButton extends StatelessWidget {
  final Widget child;
  final Function() onPressed;
  final Color color;

  const BaseButton({
    super.key,
    required this.child,
    required this.onPressed,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    var paddedWidth = MediaQuery.of(context).size.width * 0.05;

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: paddedWidth,
      ),
      child: PlatformElevatedButton(
        color: color,
        padding: const EdgeInsets.all(14),
        onPressed: onPressed,
        child: child,
        material: (_, __) => MaterialElevatedButtonData(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all(color),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
            ),
            foregroundColor: WidgetStateProperty.all<Color>(BrandingColors.cadiWhite),
            textStyle: WidgetStateProperty.all<TextStyle>(
              const TextStyle(color: BrandingColors.cadiWhite),
            ),
          ),
        ),
        cupertino: (_, __) => CupertinoElevatedButtonData(
          color: color,
          borderRadius: BorderRadius.circular(30),
        ),
      ),
    );
  }
}
