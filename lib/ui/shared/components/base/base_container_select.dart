import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

class BaseContainerSelect extends StatelessWidget {
  final Widget child;
  final Function() onPressed;

  const BaseContainerSelect({super.key, 
    required this.child,
    required this.onPressed,

  });

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10),
        child:  PlatformElevatedButton(
          //Todo change tghis to new method
          color: BrandingColors.cadiGrey,
          padding: const EdgeInsets.fromLTRB(10, 20, 30, 20),
          onPressed: onPressed,
          child: child,
          material: (_, __) => MaterialElevatedButtonData(
            style: ButtonStyle( foregroundColor: WidgetStateProperty.all<Color>(BrandingColors.cadiGrey), textStyle: WidgetStateProperty.all<TextStyle>(const TextStyle(color: BrandingColors.cadiWhite))),
          ),
          cupertino: (_, __) =>
              CupertinoElevatedButtonData(borderRadius: BorderRadius.circular(20)),
        ));
  }
}
