import 'package:flutter/material.dart';
import 'package:cadi_core_app/theme/branding.dart';

class BaseGhostButton extends StatelessWidget {
  final Widget child;
  final Function() onPressed;
  final Color textColor;

  const BaseGhostButton({
    super.key,
    required this.child,
    required this.onPressed,
    required this.textColor,
  });

  @override
  Widget build(BuildContext context) {
    var paddedWidth = MediaQuery.of(context).size.width * 0.05;
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: paddedWidth,
      ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          shape: const StadiumBorder(
            side: BorderSide(color: BrandingColors.cadiBlackOpacity, width: 1),
          ),
          padding: const EdgeInsets.all(14),
          backgroundColor: BrandingColors.cadiWhite,
          textStyle: TextStyle(color: textColor),
        ),
        onPressed: onPressed,
        child: child,
      ),
    );
  }
}
