import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';

class BaseGhostButtonDark extends StatelessWidget {
  final Widget child;
  final Function() onPressed;
  // final Color color;
  // final Color textColor;
  // final Color splashColor;

  const BaseGhostButtonDark({super.key, 
    required this.child,
    required this.onPressed,
    // required this.color,
    // required this.textColor,
    // required this.splashColor
  });

  @override
  Widget build(BuildContext context) {
    // const border = BorderRadius.all(Radius.circular(8.0));
    var paddedWidth = MediaQuery.of(context).size.width * 0.05;
//    var maxHeight = MediaQuery.of(context).size.height * 0.10 ;
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: paddedWidth,
      ),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            shape: const StadiumBorder(
                side: BorderSide(color: BrandingColors.cadiWhite, width: 1)),
            padding: const EdgeInsets.all(14),
            backgroundColor: BrandingColors.cadiBlackOpacity
          ),
          onPressed: onPressed,
          child: child,
         ),
    );
  }
}
