import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

class BaseInput extends StatelessWidget {
  final TextEditingController textEditingController;
  final Key textKey;
  final String labelText;
  final String placeholder;
  final TextInputType textInputType;
  final int maxLines;

  const BaseInput(
      {super.key,
      required this.textEditingController,
      required this.textKey,
      required this.labelText,
      required this.placeholder,
      required this.textInputType,
      required this.maxLines});

  @override
  Widget build(BuildContext context) {
//    var maxWidth  =  MediaQuery.of(context).size.width * 0.65;
//    var maxHeight = MediaQuery.of(context).size.height * 0.10 ;
    return PlatformTextField(
        cursorColor: BrandingColors.cadiGrey,
        controller: textEditingController,
        key: textKey,
        autocorrect: false,
        maxLines: maxLines,
        style: TextStyle(
            color: BrandingColors.cadiBlack,
            fontSize: PlatformSizing().normalTextSize(context)),
        material: (_, __) => MaterialTextFieldData(
            decoration: InputDecoration(labelText: labelText)),
        cupertino: (_, __) => CupertinoTextFieldData(
            placeholder: placeholder,
            decoration: BoxDecoration(
                color: BrandingColors.white,
                border: Border.all(color: BrandingColors.cadiGrey))),
//                          style: TextStyle(fontSize: 20),
        keyboardType: textInputType
        //                        validator: (val) => val.isEmpty || val.trim().length == 0  || !(RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(val))
        //                            ? 'Please enter a valid email address'
        //                            : null,
        );
  }
}
