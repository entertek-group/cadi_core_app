import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';

class BaseMoveEventSelect extends StatelessWidget {
  final Widget child;
  final Function() onPressed;
  final Color color;
  // final Color textColor;
  // final Color splashColor;

  const BaseMoveEventSelect({super.key, 
    required this.child,
    required this.onPressed,
    required this.color,
    // required this.textColor,
    // required this.splashColor
  });

  get primary => null;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        border: Border.all(color: BrandingColors.cadiBlackOpacity, width: 1),
        color: Colors.transparent,
      ),
      padding: const EdgeInsets.all(0),
      child: ElevatedButton(
        onPressed: onPressed,
        style: ButtonStyle(

          backgroundColor: WidgetStateProperty.resolveWith<Color>(
              (Set<WidgetState> states) {
            return color;
            // if (states.contains(MaterialState.pressed)) return Colors.grey;
          }),
        ),
        child: child,
      ),
    );

    // const border = BorderRadius.all(Radius.circular(8.0));
    // var paddedWidth = MediaQuery.of(context).size.width * 0.15;
//    var maxHeight = MediaQuery.of(context).size.height * 0.10 ;
    // return Padding(
    //     padding: EdgeInsets.all(0),
    //     child: PlatformButton(
    //       //Todo change tghis to new method
    //       color: color,
    //       padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
    //       onPressed: onPressed,
    //       child: child,

    //       material: (_, __) => MaterialRaisedButtonData(
    //         textColor: BrandingColors.cadiWhite,
    //         splashColor: BrandingColors.cadiGrey,
    //       ),
    //       cupertino: (_, __) => CupertinoButtonData(
    //         borderRadius: BorderRadius.circular(15),
    //       ),
    //     ));
  }
}
