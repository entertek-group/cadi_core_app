import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/material.dart';

Container buildTableText(String text) {
  return Container(
    child: Padding(
      padding: const EdgeInsets.only(bottom: 4, left: 5),
      child: Text(text,
          style: const TextStyle(
              color: BrandingColors.cadiBlack,
              fontWeight: FontWeight.w300,
              fontSize: 15)),
    ),
  );
}

Container buildTableTitle(String text) {
  return Container(
    child: Padding(
      padding: const EdgeInsets.only(bottom: 8, top: 10, left: 5),
      child: Text(text,
          style: const TextStyle(
              color: BrandingColors.cadiBlack,
              fontWeight: FontWeight.w800,
              fontSize: 15)),
    ),
  );
}

TableRow buildTableRow(BuildContext context, Widget rowItemOne,
    Widget rowItemTwo, Widget rowItemThree) {
  return TableRow(children: [rowItemOne, rowItemTwo, rowItemThree]);
}

TableRow buildTableRowWithKey(BuildContext context, ValueKey key, Widget rowItemOne,
    Widget rowItemTwo, Widget rowItemThree) {
  return TableRow(key: key, children: [rowItemOne, rowItemTwo, rowItemThree]);
}

Table buildSummaryTable(BuildContext context, List<TableRow> tableRows) {
  return Table(
    columnWidths: const {
      0: FlexColumnWidth(3),
      1: FlexColumnWidth(2),
      2: FlexColumnWidth(2),
    },
    children: tableRows,
  );
}

Container buildScrollableDynamicTable(Table table) {
  return Container(
      child: Scrollbar(
          child: SingleChildScrollView(
              scrollDirection: Axis.vertical, child: table)));
}

Container buildScrollableTable(Table table, double screenHeight) {
  return Container(
      //height: screenHeight,
      child: Scrollbar(
          child: SingleChildScrollView(
              scrollDirection: Axis.vertical, child: table)));
}
