import 'package:bottom_picker/bottom_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:cadi_core_app/theme/branding.dart';

class DropdownItem {
  const DropdownItem(this.text, this.index, this.value);
  final String text;
  final int index;
  final dynamic value;
}

void showBottomDropdown(
    BuildContext context,
    String title,
    String subText,
    List<DropdownItem> items,
    int? selectedIndex,
    Function onSubmit,
    bool submitIndex) {
  showPlatformModalSheet(
    context: context,
    builder: (_) => PlatformWidget(
      material: (_, __) => _androidBottomDropdown(
          context, title, items, selectedIndex, onSubmit, submitIndex),
      cupertino: (_, __) => _cupertinoSheetContent(
          context, title, subText, items, onSubmit, submitIndex),
    ),
  );
}

Widget _androidBottomDropdown(
    BuildContext context,
    String title,
    List<DropdownItem> items,
    int? selectedIndex,
    Function onSubmit,
    bool submitIndex) {

  List<Widget> displayItems = items.map((item) => Center(child: Text(item.text))).toList();

  // Use MediaQuery to get the screen size
  var screenWidth = MediaQuery.of(context).size.width;

  // Adjust font sizes based on screen size
  var normalFontSize = screenWidth > 600 ? 26.0 : 23.0;
  var buttonWidth  = screenWidth > 600 ? screenWidth * 0.2 : screenWidth * 0.3;

  return BottomPicker(
    pickerTitle: Text(
      title,
      style: TextStyle(
        fontSize: normalFontSize,
        color: BrandingColors.cadiGrey,
        fontWeight: FontWeight.bold,
      ),
    ),
    items: displayItems,
    selectedItemIndex: selectedIndex ?? 0,
    dismissable: true,
    buttonSingleColor: BrandingColors.cadiGreen,
    buttonContent: Container(
      alignment: Alignment.center,
      child: Text(
        'Confirm',
        style: TextStyle(
            color: BrandingColors.cadiWhite,
            fontSize: screenWidth > 600 ? 26.0 : normalFontSize),
      ),
    ),
    buttonPadding: screenWidth > 600 ? 7.0 : 5.0,
    buttonWidth: buttonWidth,
    pickerTextStyle: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: normalFontSize,
        color: BrandingColors.cadiBlack),
    onSubmit: (index) {
      if (submitIndex) {
        onSubmit(items[index].index);
      } else {
        onSubmit(items[index].value);
      }
    },
    onChange: (index) {},
    onClose: () {
      Navigator.pop(context);
    },
    displayCloseIcon: true,
    closeIconColor: BrandingColors.cadiGreen,
    closeIconSize: 36,
  );
}


Widget _cupertinoSheetContent(
    BuildContext context,
    String title,
    String subText,
    List<DropdownItem> items,
    Function onSubmit,
    bool submitIndex) {
  var itemWidgets = <Widget>[];
  for (final item in items) {
    itemWidgets.add(CupertinoActionSheetAction(
      child: Text(item.text),
      onPressed: () {
        Navigator.pop(context, item.text);
        if (submitIndex) {
          onSubmit(item.index);
        } else {
          onSubmit(item.value);
        }
      },
    ));
  }
  return CupertinoActionSheet(
    title: Text(title),
    message: Text(subText),
    actions: itemWidgets,
    cancelButton: CupertinoActionSheetAction(
      isDefaultAction: true,
      onPressed: () {
        Navigator.pop(
          context,
          'Cancel',
        );
      },
      child: const Text('Cancel', style: TextStyle()),
    ),
  );
}
