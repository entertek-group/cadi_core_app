import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Future<DateTime?> showPlatformDatePicker({
  required BuildContext context,
  required DateTime initialDate,
  required DateTime firstDate,
  required DateTime lastDate,
  TransitionBuilder? builder,
  bool useRootNavigator = true,
  RouteSettings? routeSettings,
  Key? key,
  CupertinoDatePickerMode mode = CupertinoDatePickerMode.date,
  Color? backgroundColor,
  double? height,
  bool showCupertino = false,
  bool showMaterial = false,
}) async {
  if ((Theme.of(context).platform == TargetPlatform.iOS) || showCupertino) {
    DateTime now = DateTime.now();
    DateTime? keep;
    await showModalBottomSheet(
      context: context,
      builder: (BuildContext builder) {
        return ConstrainedBox(
          constraints: BoxConstraints(
            maxHeight:
                height ?? MediaQuery.of(context).copyWith().size.height / 3,
            maxWidth: MediaQuery.of(context).copyWith().size.width,
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    child: const Text(""),
                    onPressed: () => {},
                  ),
                  TextButton(
                    child: const Text("Done"),
                    onPressed: () => {
                      Navigator.pop(
                        context,
                        'Done',
                      )
                    },
                  ),
                ],
              ),
              Flexible(
                child: Container(
                  color: BrandingColors.cadiGreen,
                  child: CupertinoDatePicker(
                    key: key,
                    mode: mode,
                    onDateTimeChanged: (date) => keep = date,
                    backgroundColor: backgroundColor,
                    initialDateTime: DateTime(
                      initialDate.year,
                      initialDate.month,
                      initialDate.day,
                      now.hour,
                      now.minute,
                    ),
                    minimumDate: firstDate,
                    maximumDate: lastDate,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
    return keep;
  } else {
    return await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: firstDate,
      lastDate: lastDate,
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: const ColorScheme.light(
              primary: BrandingColors.cadiGreen,
              onSurface: BrandingColors.cadiBlackOpacity,
            ),
          ),
          child: child!,
        );
      },
      routeSettings: routeSettings,
      useRootNavigator: useRootNavigator,
    );
  }
}
