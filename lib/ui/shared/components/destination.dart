import 'package:flutter/cupertino.dart';

class Destination {
  const Destination(this.index, this.title, this.titleWidget, this.leading,
      this.trailing, this.body);
  final int index;
  final String title;
  final Widget? titleWidget;
  final Widget? leading;
  final List<Widget>? trailing;
  final Widget body;
}
