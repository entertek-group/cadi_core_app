import 'package:cadi_core_app/theme/theme.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_platform_assets.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

Widget inputWidget(BuildContext context,
    TextEditingController textFieldController, String title, bool isNum) {
  return PlatformTextField(
    cursorColor: BrandingColors.cadiGrey,
    textCapitalization: TextCapitalization.characters,
    onChanged: (value) {},
    controller: textFieldController,
    material: (_, __) => MaterialAssetsDark().textFieldData(title, null),
    cupertino: (_, __) => CupertinoAssets().textFieldData(title, null),
    inputFormatters: [
      isNum
          ? FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}'))
          : FilteringTextInputFormatter(RegExp("[a-zA-Z0-9]"), allow: true)
    ],
    style: TextStyle(
        color: BrandingColors.cadiBlack,
        fontSize: PlatformSizing().inputTextFieldSize(context)),
    keyboardType: isNum
        ? const TextInputType.numberWithOptions(decimal: true)
        : TextInputType.text,
  );
}
