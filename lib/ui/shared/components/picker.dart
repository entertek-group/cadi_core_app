import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void showPlatformPicker(BuildContext context, List<PickerItem> items,
    int selectedIndex, Function refresh) {
  if (defaultTargetPlatform == TargetPlatform.android) {
    showCupertinoModalPopup<void>(
        context: context,
        builder: (BuildContext context) {
          return PlatformPicker(
            refresh: refresh,
            selectedIndex: selectedIndex,
            items: items,
            key: null,
          );
        });
  } else if (defaultTargetPlatform == TargetPlatform.iOS) {
    showCupertinoModalPopup<void>(
        context: context,
        builder: (BuildContext context) {
          return PlatformPicker(
            refresh: refresh,
            selectedIndex: selectedIndex,
            items: items,
            key: null,
          );
        });
  }
}

class PickerItem {
  const PickerItem(this.text, this.value);
  final String text;
  final dynamic value;
}

class PlatformPicker extends StatefulWidget {
  final Function refresh;
  final List<PickerItem> items;
  final int selectedIndex;

  const PlatformPicker({
    required super.key,
    required this.refresh,
    required this.items,
    required this.selectedIndex,
  });

  @override
  _PlatformPickerState createState() => _PlatformPickerState();
}

class _PlatformPickerState extends State<PlatformPicker> {
  int value = 0;
  Color color = BrandingColors.cadiGreen;
  //TODO BRANDING CLASS ACTIVE SERVICE

  void _itemTapped(index) {
    // _selectedIndex = index;
    widget.refresh(index);
  }

  Widget _buildBottomPicker(Widget picker) {
    var dynamicSize = MediaQuery.of(context).size.width * 0.1;
    final double kPickerSheetHeight = MediaQuery.of(context).size.width * 0.65;
    return Container(
      height: kPickerSheetHeight,
      padding: const EdgeInsets.only(top: 5.0),
      color: CupertinoColors.white,
      child: DefaultTextStyle(
        style: const TextStyle(
          color: CupertinoColors.black,
          fontSize: 22.0,
        ),
        child: Stack(
          children: [
            GestureDetector(
              // Blocks taps from propagating to the modal sheet and popping.
              onTap: () {},
              child: SafeArea(
                top: false,
                child: picker,
              ),
            ),
            Positioned(
                left: 0,
                right: 0,
                child: Material(
                    child: IconButton(
                  highlightColor: color,
                  alignment: Alignment.center,
                  iconSize: dynamicSize,
                  onPressed: () => Navigator.of(context).pop(),
                  icon: const Icon(
                    Icons.clear,
                    color: BrandingColors.cadiGreen,
                  ),
                ))),
          ],
        ),
      ),
    );
  }

  Widget _buildCupertinoPicker(List<PickerItem> items, int selectedIndex) {
    return Container(
      child: CupertinoPicker(
          magnification: 1.5,
          backgroundColor: Colors.white,
          itemExtent: 50, //height of each item
          looping: false,
          scrollController:
              FixedExtentScrollController(initialItem: selectedIndex),
          onSelectedItemChanged: _itemTapped,
          children: items
              .map((item) => Center(
                    child: Text(
                      item.text,
                      style: TextStyle(
                          fontSize: PlatformSizing().normalTextSize(context),
                          color: BrandingColors.cadiBlack),
                    ),
                  ))
              .toList()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildBottomPicker(
      _buildCupertinoPicker(widget.items, widget.selectedIndex),
    );
  }
}
