import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:cadi_core_app/theme/branding.dart';

class PopMenuItem {
  const PopMenuItem(this.text, this.onTap);
  final String text;
  final Function onTap;
}

void showPopupMenu(BuildContext context, String title, String searchText,
    List<PopMenuItem> items) {
  showPlatformModalSheet(
    context: context,
    builder: (_) => PlatformWidget(
      material: (_, __) => _androidPopupContent(context, title, items),
      cupertino: (_, __) =>
          _cupertinoSheetContent(context, title, searchText, items),
    ),
  );
}

Widget _androidPopupContent(
    BuildContext context, String text, List<PopMenuItem> items) {
  var dynamicSize = MediaQuery.of(context).size.width * 0.1;
  final double kPickerSheetHeight = MediaQuery.of(context).size.width * 0.75;
  Color color = BrandingColors.cadiGreen;
  var screenWidth = MediaQuery.of(context).size.width;
  var fontSize = screenWidth * 0.048; 

  var itemWidgets = <Widget>[];
  for (final item in items) {
    itemWidgets.add(GestureDetector(
      child: Container(
        color: BrandingColors.background, 
        padding: const EdgeInsets.all(15),
        child: Text(
          item.text,
          style: TextStyle(
              fontSize: fontSize, color: BrandingColors.cadiBlackOpacity),
          textAlign: TextAlign.center,
        ),
      ),
      onTap: () {
        item.onTap();
        Navigator.pop(context);
      },
    ));
  }

  return Container(
  decoration: const BoxDecoration(
    color: BrandingColors.background,
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(100.0),
      topRight: Radius.circular(100.0),
    ),
  ),
  height: kPickerSheetHeight,
  padding: const EdgeInsets.only(top: 5.0),
  child: Column(
    children: [
      Align(
        alignment: Alignment.center,
        child: IconButton(
          highlightColor: color,
          iconSize: dynamicSize,
          onPressed: () => Navigator.of(context).pop(),
          icon: const Icon(
            Icons.clear,
            color: BrandingColors.cadiGreen,
          ),
        ),
      ),
      Expanded(
        child: ListView(
          shrinkWrap: true,
          children: itemWidgets,
        ),
      ),
    ],
  ),
);

}


Widget _cupertinoSheetContent(BuildContext context, String title,
    String searchText, List<PopMenuItem> items) {
  var itemWidgets = <Widget>[];
  for (final item in items) {
    itemWidgets.add(CupertinoActionSheetAction(
      child: Text(item.text),
      onPressed: () {
        item.onTap();
        Navigator.pop(context, item.text);
      },
    ));
  }
  return CupertinoActionSheet(
    title: Text(title, textAlign: TextAlign.center),
    message: Text(searchText, textAlign: TextAlign.center),
    actions: itemWidgets,
    cancelButton: CupertinoActionSheetAction(
      isDefaultAction: true,
      onPressed: () {
        Navigator.pop(
          context,
          'Cancel',
        );
      },
      child: const Text('Cancel', style: TextStyle()),
    ),
  );
}
