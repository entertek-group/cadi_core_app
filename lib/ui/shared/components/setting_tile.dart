import 'package:flutter/cupertino.dart';

class SettingTile {
  const SettingTile(this.index, this.title, this.body);
  final int index;
  final String title;
  final Widget body;
}
