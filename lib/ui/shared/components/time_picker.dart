import 'package:cadi_core_app/theme/branding.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


Future<TimeOfDay?> showPlatformTimePicker({
  required BuildContext context,
  required TimeOfDay initialTime,
  TransitionBuilder? builder,
  bool useRootNavigator = true,
  RouteSettings? routeSettings,
  Key? key,
  CupertinoDatePickerMode mode = CupertinoDatePickerMode.time,
  int minuteInterval = 1,
  bool use24hFormat = false,
  Color? backgroundColor,
  double? height,
  bool showCupertino = false,
  bool showMaterial = false,
}) async {
  if ((Theme.of(context).platform == TargetPlatform.iOS) || showCupertino) {
    DateTime now = DateTime.now();
    TimeOfDay? keep;
    await showModalBottomSheet(
      context: context,
      builder: (BuildContext builder) {
        return ConstrainedBox(
          constraints: BoxConstraints(
            maxHeight: height ?? MediaQuery.of(context).copyWith().size.height / 3,
            maxWidth: MediaQuery.of(context).copyWith().size.width,
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    child: const Text(""),
                    onPressed: () => {},
                  ),
                  TextButton(
                    child: const Text("Done"),
                    onPressed: () => {
                      Navigator.pop(
                        context,
                        'Done',
                      )
                    },
                  ),
                ],
              ),
              Flexible(
                child: Container(
                  color: BrandingColors.cadiGreen, // Custom background color
                  child: CupertinoDatePicker(
                    key: key,
                    mode: mode,
                    onDateTimeChanged: (date) => keep = TimeOfDay.fromDateTime(date),
                    backgroundColor: backgroundColor,
                    initialDateTime: DateTime(
                      now.year,
                      now.month,
                      now.day,
                      initialTime.hour,
                      initialTime.minute,
                    ),
                    minuteInterval: minuteInterval,
                    use24hFormat: use24hFormat,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
    return keep;
  } else {
    return await showTimePicker(
      context: context,
      initialTime: initialTime,
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: const ColorScheme.light(
              primary: BrandingColors.cadiGreen, 
              onSurface: BrandingColors.cadiBlackOpacity, 
            ),
            timePickerTheme: TimePickerThemeData(
              dayPeriodColor: WidgetStateColor.resolveWith((states) => 
                states.contains(WidgetState.selected)
                  ? BrandingColors.cadiGreen 
                  : Colors.transparent,
              ),
              dayPeriodTextColor: WidgetStateColor.resolveWith((states) => 
                states.contains(WidgetState.selected)
                  ? BrandingColors.cadiWhite 
                  : BrandingColors.cadiGreen, 
              ),
            ),
          ),
          child: child!,
        );
      },
      routeSettings: routeSettings,
      useRootNavigator: useRootNavigator,
    );
  }
}
