import 'dart:async';
import 'package:cadi_core_app/redux/models/api_response.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:cadi_core_app/redux/state/app_state.dart';
import 'package:after_layout/after_layout.dart';
import 'package:cadi_core_app/ui/shared/components/app/app_loading.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;
import 'package:cadi_core_app/ui/shared/navigation/navigators.dart' as navigator;
import 'package:cadi_core_app/services/loader_service.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

class LoginLoaderScreen extends StatefulWidget {
  static const String route = route_helper.loaderLogin;

  const LoginLoaderScreen({super.key});

  @override
  LoginLoaderState createState() => LoginLoaderState();
}

class LoginLoaderState extends State<LoginLoaderScreen>
    with AfterLayoutMixin<LoginLoaderScreen> {
  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
        body: Container(
      color: Colors.white,
      child: Stack(
        children: <Widget>[
          Container(),
          const AppLoading(message: "Signing in...")
        ],
      ),
    ));
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // Calling the same function "after layout" to resolve the issue.
    loadUserInfo();
  }

  void loadUserInfo() {
    final store = StoreProvider.of<AppState>(context);
    final Completer<ApiResponse> completer = Completer<ApiResponse>();
    loadLogin(store, completer);

    completer.future.then((_) {
      var error = store.state.authState.error;
      if (error.length > 1) {
        navigator.navigateRoute(route_helper.logoutScreen, store, null);
      } else {
        navigator.navigateRoute(route_helper.homeScreen, store, null);
      }
    });
  }
}
