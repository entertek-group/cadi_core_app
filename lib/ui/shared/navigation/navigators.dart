import 'dart:async';
import 'package:cadi_core_app/redux/actions/actions.dart';
import 'package:cadi_core_app/redux/models/reference/reference.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:cadi_core_app/helpers/routes.dart' as route_helper;

void navigateRoute(String route, Store store, Object? object) {
  var useCompleter = false;
  var replace = false;
  final Completer<Null> completer = Completer<Null>();
  switch (route) {
    case route_helper.cargoMovementStatusScreenBuilder:
      if (store.state.referenceState.cargoMovementStates ==
              [MovementStatus.initial()] ||
          store.state.referenceState.cargoMovementStates.length < 2) {
        store.dispatch(StartLoading());
        completer.future.then((_) {
          store.dispatch(StopLoading());
        });
      }
      break;
  }

  if (replace) {
    store.dispatch(NavigateToAction.replace(route));
    replace = false;
  } else if (useCompleter) {
    completer.future.then((_) {
      store.dispatch(NavigateToAction.push(route));
    });
  } else {
    if (route == route_helper.logoutScreen) {
      route = route_helper.loginScreen;
      store.dispatch(CompleteSignOut());
      store.dispatch(NavigateToAction.replace(route));
    } else {
      store.dispatch(NavigateToAction.push(route));
    }
  }
}
